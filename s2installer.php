<?php 
/*
 * This script contains a BitBucket client which downloads a zipped tagged 
 * release of a repository, unpacks it and installs it over any existing code.
 */
$repo = new BitBucketClient('Lunarfish','s2main');
$call = $_GET['call'];
switch($call) {
    case 'tags': {
        $tags = $repo->getTags();
        echo json_encode($tags);
    } break;
    case 'latest': {
        $tag = $repo->getRecentTag();
        echo json_encode($tag);
    } break;
    case 'installed': {
        $tag = $repo->getInstalledVersion();
        echo json_encode(array('tag'=>$tag));
    } break;
    case 'install': {
        $tag = $repo->getRecentTag();
        $repo->downloadTag($tag);
        $repo->unpackTag($tag);
        $repo->installTag($tag);
        $chk = $repo->getInstalledVersion();
        echo json_encode(array('tag'=>$chk));
    } break;
    default: {
        s2InstallerInterface();
    } break;
}
            


class BitBucketClient {
    
    protected $user;
    protected $repo;
    
    public function __construct($user,$repo) {
        $this->user = $user;
        $this->repo = $repo;
    }
    
    public function getTags() {
        
        $tags = array();
        $url = "https://api.bitbucket.org/1.0/repositories/{$this->user}/{$this->repo}/tags";
        //echo $url;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));
        $result = json_decode(curl_exec($curl));
        
        curl_close($curl);
        
        foreach($result as $tag => $contents) {
            //$result->$tag = $contents->node;
            $tags[] = array('name'=>$tag, 'node'=>$contents->node, 'timestamp'=>$contents->utctimestamp);
        }
        usort($tags, array($this,"_tagSortByTimestamp"));
        
        return $tags;
        
    }
    
    protected function _tagSortByTimestamp($a,$b) {
        return strcmp($a->timestamp, $b->timestamp);
    }
    
    public function getRecentTag() {

        $tags = $this->getTags();
    
        $tag = array_pop($tags);
        
        return $tag;
        
    }
    
    public function downloadTag($tag) {
        
        $url = "https://bitbucket.org/{$this->user}/{$this->repo}/get/{$tag['name']}.zip";
        
        if (!file_exists('s2versions')) mkdir('s2versions');
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));
    
        $st = curl_exec($curl);
        $fd = fopen("s2versions/{$tag['name']}.zip", 'w');
        fwrite($fd, $st);
        fclose($fd);

        curl_close($curl);
        
    }
    
    public function unpackTag($tag) {
    
        try {
            $zip = new ZipArchive;
            $res = $zip->open("s2versions/{$tag['name']}.zip");
            if ($res === TRUE) {
                $zip->extractTo("s2versions/");
            }
            rename("s2versions/{$this->user}-{$this->repo}-{$tag['node']}","s2versions/{$tag['name']}");
            unlink("s2versions/{$tag['name']}.zip");
            
        } catch (Exception $e) {
            
            // what to do here?
        }
    }
    
    public function installTag($tag) {
        
        if (!file_exists('s2stash')) {
            mkdir('s2stash/s2',0644,true);
        }
        
        if (file_exists("s2/settings.conf")) {
            copy("s2/settings.conf","s2stash/s2/settings.conf");
        }
        
        if (file_exists("s2/config.ini")) {
            copy("s2/config.ini","s2stash/s2/config.ini");
        }
        
        if (!file_exists('s2')) {
            mkdir('s2',0644);
        }
        $this->_recursiveCopy("s2versions/{$tag['name']}/s2", "s2");
        
        //if (file_exists("s2stash/s2/settings.conf")) {
        //    copy("s2stash/s2/settings.conf","s2/settings.conf");
        //}
        if (file_exists("s2stash/s2/config.ini")) {
            copy("s2stash/s2/config/ini", "s2/config.ini");
        }
        
        file_put_contents("s2/tag.txt",$tag['name']);
        
    }
    
    public function getInstalledVersion() {
        if (file_exists("s2/tag.txt")) {
            $tag = file_get_contents("s2/tag.txt");
        }
        return (isset($tag))?$tag:null;
    }
    
    protected function _recursiveCopy($source, $dest, $mode=0755) {
        // recursive function to copy
        // all subdirectories and contents:
        if(is_dir($source)) {
            $dir_handle=opendir($source);
            while($file=readdir($dir_handle)){
                if($file!="." && $file!=".."){
                    if(is_dir($source."/".$file)){
                        if(!is_dir($dest."/".$file)){
                            mkdir($dest."/".$file);
                        }
                        $this::_recursiveCopy($source."/".$file, $dest."/".$file);
                    } else {
                        copy($source."/".$file, $dest."/".$file);
                        chmod($dest."/".$file,$mode);
                    }
                }
            }
            closedir($dir_handle);
        } else {
            copy($source, $dest);
        }
    }
}
function s2InstallerInterface() {
?>    
<!doctype html>
<html>
    <head>
        <title>Second Site installer</title>
        
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
        <script type="text/javascript">
            var s2installer;
            $(document).ready(function() {
                s2installer.getInstalledVersion();
                s2installer.getLatestVersion();
            });
            
            s2installer = {
                current: null,
                latest: null,
                getInstalledVersion: function() {
                    var url = location.href + '?call=installed';
                    $.getJSON(url,this.updateInstalledVersion);
                },
                updateInstalledVersion: function(version) {
                    //console.log(version);
                    this.current = version.tag;
                    $('#s2-current-version>*').remove();
                    $('#s2-current-version').append($("<p></p>").text((version.tag)?version.tag:'Not installed'));
                }, 
                getLatestVersion: function() {
                    var url = location.href + '?call=latest';
                    $.getJSON(url,this.updateLatestVersion);
                },
                updateLatestVersion: function(version) {
                    //console.log(version);
                    this.latest = version;
                       
                    console.log(this.latest);
                    console.log(this.current);
                    $('#s2-latest-version>*').remove();
                    if (!this.current || this.latest.name > this.current) {
                        $('#s2-latest-version').append($("<button></button>").text('Install').click(function() {s2installer.installLatest();}));
                    }
                    
                    $('#s2-latest-version').append($("<p></p>").text(version.name));   
                },
                installLatest: function() {
                    var url = location.href + '?call=install';
                    $.getJSON(url,this.updateInstalledVersion);
                }
            
            };
            
            
            
        </script>
        <style type="text/css">
            * {
                font-family: Arial, sans-serif;
                color: #669;
            }
            h1,h2,h3 {
                font-weight: normal;
                color: #557;
            }
            #outer {
                text-align: center;
            }
            #page {
                margin: 1em 0;
                text-align: left;
                display: inline-block;
                width: 964px;
                border: 1px solid #ddd;
                background-color: #ddf;
            }
            #container {
                padding: 1em 2em;
                line-height: 1.5em;
            }
            .col-wrapper {
                width: 900px;
                margin: 0;
                border-width: 0;
                padding: 0;
            }
            .cols-2 {
                display: inline-block;
                width: 447px;
                padding: 0;
                margin: 0;
                border-width: 0;
            }
            .cols-2 .liner {
                margin: 1em;
                padding: 1em;
                border: 1px solid #ddd;
                background-color: #cce;
            }
            .cols-2 .liner h3 {
                font-weight: normal;
                font-size: 1.2em;
            }
            .cols-2 .liner button {
                float: right;
            }
            .cols-2:nth-child(odd) .liner {
                margin-left: 0;
            }
            .cols-2:nth-child(even) .liner {
                margin-right: 0;
            }
            
        </style>
    </head>
    <body>
        <div id="outer">
            <div id="page">
                <div id="container">
                    <h1>Second Site installer</h1>
                    <p>This page installs:</p>
                    <ul>
                        <li>Second Site</li>
                        <li>the Second Site BGS linked data API and browser</li>
                    </ul>
                    
                    <h2>Install &amp; Update</h2>
                    <div class="col-wrapper">
                        <div class="cols-2">
                            <div class="liner">
                            <h3>Current installed version</h3>
                            <div id="s2-current-version"></div>

                            </div>
                        </div>
                        <div class="cols-2">
                            <div class="liner">
                                <h3>Latest released version</h3>
                                <div id="s2-latest-version"></div>
                            </div>
                        </div>
                    </div>
                    
                    <h2>Setup</h2>
                    <div>
                        
                        
                    </div>
                </div>
            </div>
            
        </div>
    </body>
</html>
<?php
}
?>