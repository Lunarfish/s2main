/*
   IMPORT PROCESS
   
      Choose file type - Return file type
      Choose file(s) - Return file id(s)
      Match columns -
         Choose type of match for each column
            Column in input matches column in table
            Column in input matches a column in linked table
            Same value select from available
            Static value enter a value for all  
         Set match for each column  
         Returns interface with columns matched
         
     The server interactions are to collate the data from the import file(s),
     execute the import and review the success of the process.            
*/
function s2_showimportinterface(id) {
   var args = s2_disectid(id);
   var id = 'Form-'+args.Form+'_Interface';
   var ifaceobj = JSON.parse(document.getElementById(id).value);
   s2_drawimportform_choosetype(ifaceobj);
}

function s2_getimporttypenode(id,prop) {
   var td, select, option, oi;
   var options = new Array('CSV','XLS','MIF');
   td = document.createElement('TD');
   select = document.createElement('SELECT');
   select.id = id;
   select.name = prop;
   for (oi=0;oi<options.length;oi++) {
      option = document.createElement('OPTION');
      option.value = options[oi];
      option.appendChild(document.createTextNode(options[oi]));
      select.appendChild(option);        
   }
   td.appendChild(select);
   return td;   
}

function s2_initimportuploader(upload) {
   var uploader,node;
   node = document.getElementById('file-uploader-'+upload);
   if (node) {
      uploader = new qq.FileUploader({
         element: node,
         action: '../echofile_v1_1.php',
         allowedExtensions: ["csv","xls","mid","mif"],
         debug: false,
         onComplete: function(id, fileName, responseJSON){s2_linkimportuploader(node.id,fileName,responseJSON);}
      });           
   }  
}

function s2_linkimportuploader(id,file,jsonobj) {
   var div,a,img,node,url,p,input,name,button;
   node = document.getElementById(id);
   div = document.createElement('DIV');
   name = id.replace(/file\-uploader\-/,'');
   div.id = id;
   url = unescape(jsonobj.url);
   var args = s2_disectid(name);
   a = document.createElement('A');
   a.className = 's2_txtlink';
   a.target = jsonobj.file;
   a.href = url;
   a.appendChild(document.createTextNode('Download File'));
   a.style.marginRight = '10px';
   div.appendChild(a);
   button = document.createElement('BUTTON');
   button.className = 's2iconbutton';
   button.id = id+'_ShowHide';
   button.appendChild(document.createTextNode('Show'));
   button.onclick = function () {s2_showhidelink(this.id);};
   div.appendChild(button);

   input = document.createElement('INPUT');
   input.id = name;
   input.name = name;
   input.value = url;
   input.type = 'hidden';
   div.appendChild(input); 
   node.parentNode.replaceChild(div,node);

   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);     
   var iset = JSON.parse(iidnode.value);
   if (!iset.Files) iset.Files = new Object();
   iset.Files[args.Type] = jsonobj.id;
   iidnode.value = JSON.stringify(iset);
   //alert(JSON.stringify(jsonobj));
   // this should create the url and replace the uploader with a url parameter 
   // for the database. 
   return true;
}
function s2_drawimportform_linkfiles(args) {
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);     
   var iset = JSON.parse(iidnode.value);
   if (!iset.Files) iset.Files = new Object();
   var type = iset.ImportType.toLowerCase();
   var node,url,nid,fid,qs;
   switch(type) {
   case 'csv': {
      nid = 'Form-'+args.Form+'_IType-'+args.IType+'_Property-CSV_File_Type-csv_Id';
      fid = document.getElementById(nid).value;
      iset.Files['csv'] = fid;
   }break;
   case 'xls': {
      nid = 'Form-'+args.Form+'_IType-'+args.IType+'_Property-Excel_File_Type-xls_Id';
      fid = document.getElementById(nid).value;
      iset.Files['xls'] = fid;
   }break;
   case 'mif': {
      nid = 'Form-'+args.Form+'_IType-'+args.IType+'_Property-MIF_File_Type-mif_Id';
      fid = document.getElementById(nid).value;
      iset.Files['mif'] = fid;
      nid = 'Form-'+args.Form+'_IType-'+args.IType+'_Property-MID_File_Type-mid_Id';
      fid = document.getElementById(nid).value;
      iset.Files['mid'] = fid;
   }break;
   }
   iidnode.value = JSON.stringify(iset);
   return true;
}

function s2_drawimportform_choosetype(jobj) {
   forms++;
   var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input,datatype;
   var table,thead,tbody,tr,th,td,node,text,id,lid,action,data,params,uploaders;
   var interfacetype,
   action = 'Import';
   dialog = document.getElementById('dialog-liner');
   if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
   if (!jobj.Data || jobj.Interim == true) {
      var h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode(action+' '+jobj.DisplayName));
      dialog.appendChild(h2);
   } 
   table = document.createElement('TABLE');
   table.id = action+'_'+jobj.IType;
   table.className = 'addform';
   tbody = document.createElement('TBODY');
   uploaders = new Array();
   
   prop = 'Import_Type';
   id = 'Form-'+forms+'_IType-'+jobj.IType+'_Property-'+prop;
   td = s2_getimporttypenode(id,prop)
   tr = document.createElement('TR')
   th = document.createElement('TH');
   th.className = 'left';
   th.appendChild(document.createTextNode(prop.replace(/\_/g,' ')));
   tr.appendChild(th);
   tr.appendChild(td);
   tbody.appendChild(tr);
   
   table.appendChild(tbody);                  
   dialog.appendChild(table);

   delete jobj.ListData;
   delete jobj.Page;
   delete jobj.Pages;
   delete jobj.Versions;
   delete jobj.Count;
   delete jobj.IsAllowedTo;
   for (i in jobj.Interface) {
      if (jobj.Interface[i].Current) delete jobj.Interface[i].Current;
   }
         
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_ImportSettings';
   node.type = 'hidden';
   node.value = JSON.stringify(jobj);
   dialog.appendChild(node);
   
   button = document.createElement('BUTTON');
   button.onclick = function() {s2_drawimportform_loaddata(this.id);};
   button.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Action-Import_Stage-Type';
   button.appendChild(document.createTextNode('Choose Files'));
   dialog.appendChild(button);  
    
   showdialog();
   for (var i in uploaders) s2_initimportuploader(uploaders[i]);
   
   //s2_debugjson(node.value);   
   return true;
}                      

function s2_drawimportform_loaddata(buttonid) {
   //forms++;
   var args = s2_disectid(buttonid);
   var typeid = 'Form-'+args.Form+'_IType-'+args.IType+'_Property-Import_Type';
   var type = document.getElementById(typeid).value.toLowerCase();
   
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);     
   var iset = JSON.parse(iidnode.value);
   iset.ImportType = type;
   iidnode.value = JSON.stringify(iset);
   
   var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input,datatype,div,p,text;
   var table,thead,tbody,tr,th,td,node,text,id,lid,action,data,params,uploaders;
   var interfacetype,
   action = 'Import';
   dialog = document.getElementById('dialog-liner');
   dialog.removeChild(document.getElementById(buttonid));
   var h2 = document.createElement('H2');
   h2.appendChild(document.createTextNode('Choose Import Files'));
   dialog.appendChild(h2);
   table = document.createElement('TABLE');
   table.id = action+'_'+args.IType;
   table.className = 'addform';
   tbody = document.createElement('TBODY');
   uploaders = new Array();
   switch (type) {
   case 'csv': {
      prop = 'CSV_File';
      id = 'Form-'+forms+'_IType-'+args.IType+'_Property-'+prop+'_Type-csv';
      params = JSON.parse('{"DataType":"File Path","Mandatory":1,"Inline":1}');
      datatype = 'File Path';
      td = s2_getnode(id,prop,params,data,'View')
      tr = document.createElement('TR')
      th = document.createElement('TH');
      th.className = 'left';
      th.appendChild(document.createTextNode(prop.replace(/\_/g,' ')));
      tr.appendChild(th);
      tr.appendChild(td);
      tbody.appendChild(tr);
      //uploaders.push(id);
   }break;
   case 'xls': {
      prop = 'Excel_File';
      id = 'Form-'+forms+'_IType-'+args.IType+'_Property-'+prop+'_Type-xls';
      params = JSON.parse('{"DataType":"File Path","Mandatory":1,"Inline":1}');
      datatype = 'File Path';
      td = s2_getnode(id,prop,params,data,'View')
      tr = document.createElement('TR')
      th = document.createElement('TH');
      th.className = 'left';
      th.appendChild(document.createTextNode(prop.replace(/\_/g,' ')));
      tr.appendChild(th);
      tr.appendChild(td);
      tbody.appendChild(tr);
      //uploaders.push(id);
   }break;
   case 'mif': {
      div = s2_ihelpdiv();
      text = new Array();
      text.push('Please ensure that your MID MIF file projection is Latitude Longitude WGS84 before importing.');
      text.push('To do this open the table and choose File - Save Copy As');
      text.push('Then choose a different name eg <Table Name>_WGS84.TAB and click projection to change the projection.');
      text.push('Choose Latitude Longitude and then scroll to the bottom of the list and choose WGS84.');
      text.push('Finally open the new table and choose Table Export to Export to MID MIF.');
      for(i in text) {
         p = document.createElement('P');
         p.appendChild(document.createTextNode(text[i]));
         div.appendChild(p);
      }
      dialog.appendChild(div);
      
      prop = 'MIF_File';
      id = 'Form-'+forms+'_IType-'+args.IType+'_Property-'+prop+'_Type-mif';
      params = JSON.parse('{"DataType":"File Path","Mandatory":1,"Inline":1}');
      datatype = 'File Path';
      td = s2_getnode(id,prop,params,data,'View')
      tr = document.createElement('TR')
      th = document.createElement('TH');
      th.className = 'left';
      th.appendChild(document.createTextNode(prop.replace(/\_/g,' ')));
      tr.appendChild(th);
      tr.appendChild(td);
      tbody.appendChild(tr);
      uploaders.push(id);
   
      prop = 'MID_File';
      id = 'Form-'+forms+'_IType-'+args.IType+'_Property-'+prop+'_Type-mid';
      params = JSON.parse('{"DataType":"File Path","Mandatory":1,"Inline":1}');
      datatype = 'File Path';
      td = s2_getnode(id,prop,params,data,'View')
      tr = document.createElement('TR')
      th = document.createElement('TH');
      th.className = 'left';
      th.appendChild(document.createTextNode(prop.replace(/\_/g,' ')));
      tr.appendChild(th);
      tr.appendChild(td);
      tbody.appendChild(tr);
      uploaders.push(id);
   }break;   
   }
   table.appendChild(tbody);
   dialog.appendChild(table);
   button = document.createElement('BUTTON');
   button.onclick = function() {s2_drawimportform_getheadings(this.id);};
   button.id = 'Form-'+args.Form+'_IType-'+args.IType+'_Action-Import_Stage-Match';
   button.appendChild(document.createTextNode('Match Columns'));
   dialog.appendChild(button);   
      
   //s2_debugjson(iidnode.value);   
   //showdialog();
   for (var i in uploaders) s2_initimportuploader(uploaders[i]);
   return true;
}
function s2_importcheckfiles(jobj) {
   
}

function s2_imatchselect(id,headings,params) {
   var td,select,option,value,ih,hx;
   td = document.createElement('TD');
   var dtype = (params.DataType)?params.DataType:params;
   var args = s2_disectid(id);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);
   var jobj = JSON.parse(iidnode.value);
   var match;
   switch(dtype) {
   case 'Unique Key':   {td = null;} break;
   case 'LINKEDTOM':    {td = null;} break;
   case 'LINKEDTO': {
      select = document.createElement('SELECT');
      select.id = id + '_TType-' + params.TargetType;
      select.className = 'MatchType_IColumn';
      select.onchange = function() {s2_isetmatchoption(this)};
      if (!jobj.Matches) jobj.Matches = new Object();
      if (!jobj.Matches[args.Property]) match = new Object();
      else match = jobj.Matches[args.Property];
      match.Type = 'Link';       
      var first = true;
      for(ih in headings) {
         hx = headings[ih];
         option = document.createElement('OPTION');
         option.value = hx;
         option.appendChild(document.createTextNode(hx));
         select.appendChild(option);
         if (first) {
            option.selected = 'selected';
            first = false;
            //s2_isetmatchoption(select);
            match.IColumn = hx;
         }
               
      }
      option = document.createElement('OPTION');
      option.value = 'Choose';
      option.appendChild(document.createTextNode('* Choose Existing'));
      select.appendChild(option);
      option = document.createElement('OPTION');
      option.value = 'Derive';
      option.appendChild(document.createTextNode('* Derive'));
      select.appendChild(option);
      option = document.createElement('OPTION');
      option.value = 'None';
      option.appendChild(document.createTextNode('* None'));
      select.appendChild(option);
      td.appendChild(select);
      // get columns from linked table to match
      
      var req = new Object();
      var par = new Object();
      par.Action = 'List';
      par.IType = params.TargetType;
      par.CurrentSettings = s2_getcurrent();
      req.target = 'S2_query.php';
      req.sync = true;
      req.request = par;
      var resp = snc_send(req);
      completed();
      showdialog();
      var tint = JSON.parse(resp);
      delete tint.Alternatives;
      delete tint.Views;
      delete tint.ViewName;
      delete tint.Current;
      delete tint.IsAllowedTo;
      delete tint.UserPermissions;
      
      if (!jobj.Links) jobj.Links = new Object();
      jobj.Links[params.TargetType] = tint;
      
      select = document.createElement('SELECT');
      select.id = id;
      select.className = 'MatchType_TColumn';
      select.onchange = function() {s2_isetmatchoption(this)};
      var first = true;
      for (ih in tint.Interface) {
         hx = tint.ListData[ih];
         option = document.createElement('OPTION');
         option.value = ih;
         option.appendChild(document.createTextNode(ih));
         select.appendChild(option);
         if (first) {
            option.selected = 'selected';
            first = false;
            //s2_isetmatchoption(select);
            match.TColumn = ih;
         }      
      }
      td.appendChild(select);  
      jobj.Matches[args.Property] = match;
      iidnode.value = JSON.stringify(jobj);
   }break;
   default: {
      select = document.createElement('SELECT');
      select.id = id;
      select.className = 'MatchType_Column';
      select.onchange = function() {s2_isetmatchoption(this)};
      var first = true;
      if (dtype == 'Version' || dtype == 'Count') {
         first = false;
         option = document.createElement('OPTION');
         option.value = 'Auto';
         option.appendChild(document.createTextNode('* Auto'));
         select.appendChild(option);
         option.selected = 'selected';
         s2_isetmatchoption(select);
      }
      for(ih in headings) {
         hx = headings[ih];
         option = document.createElement('OPTION');
         option.value = hx;
         option.appendChild(document.createTextNode(hx));
         select.appendChild(option);
         if (first) {
            option.selected = 'selected';
            first = false;
            s2_isetmatchoption(select);
         }      
      }
      option = document.createElement('OPTION');
      option.value = 'Static';
      option.appendChild(document.createTextNode('* Enter Text'));
      select.appendChild(option);
      td.appendChild(select);   
   }break;
   }
   return td;
}
function s2_isetmatchoption(node) {
   var id = node.id;
   var value = node.value;
   var args = s2_disectid(id);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);
   var jobj = JSON.parse(iidnode.value);
   if (!jobj.Matches) jobj.Matches = new Object();
   
   switch(value) {
   case 'Choose': {
      var td = node.parentNode;
      var tint = jobj.Links[args.TType];
      while (td.childNodes.length > 0) {
         td.removeChild(td.lastChild);
      }
      var select,option,ih,hx;
      select = document.createElement('SELECT');
      select.id = id;
      select.className = 'MatchType_SameForAll';
      select.onchange = function() {s2_isetmatchoption(this);};
      var tidcol;
      for (ih in tint.Interface) {
         if (tint.Interface[ih] == 'Unique Key') {
            tidcol = ih;            
         } 
      }
      var first = true;
      for (ih in tint.ListData) {
         hx = tint.ListData[ih];
         option = document.createElement('OPTION');
         option.value = hx[tidcol];
         option.appendChild(document.createTextNode(hx.Name));
         select.appendChild(option);
         if (first) {
            option.selected = 'selected';
            first = false;
            s2_isetmatchoption(select);
         }      
      }
      td.appendChild(select);                                            
   }break;
   case 'Derive': {
      var td = node.parentNode;
      var tr = td.parentNode;
      var tb = tr.parentNode;
      var req = new Object();
      var par = new Object();
      par.Action = 'GetDeriveReqs';
      par.IType = args.TType;
      req.target = 'S2_query.php';
      req.sync = true;
      req.request = par;
      var resp = snc_send(req);
      completed();
      showdialog();
      var tint = JSON.parse(resp);
//s2_debugjson(resp);
      delete tint.IsAllowedTo;
      delete tint.UserPermissions;
      var crumbs = s2_getcrumbs();
      var tname,ii,ic,ct,crumb,cset,tdo,tro,tho,select,option;
      for (ii in tint) {
         cset = false;
         tdo = null;
         tname = tint[ii].DataType;
         id = 'Form-'+forms+'_IType-'+jobj.IType+'_DataType-LINKEDTO_TargetType-'+tname+'_Property-'+tname+'_IsCrumb-1';
         for (ic in crumbs) {
            crumb = crumbs[ic];
            ct = crumb.IType;
            if (tname == ct) {
               tdo = document.createElement('TD');
               select = document.createElement('SELECT');
               select.className = 'MatchType_SameForAll';
               select.id = id;
               option = document.createElement('OPTION');
               option.value = crumb.Current;
               option.appendChild(document.createTextNode(crumb.Name));
               select.appendChild(option); 
               option = document.createElement('OPTION');
               option.value = 'Match';
               option.appendChild(document.createTextNode('* Match Data'));
               select.appendChild(option);
               option = document.createElement('OPTION');
               option.value = 'Choose';
               option.appendChild(document.createTextNode('* Choose Existing'));
               select.appendChild(option);
               select.onchange = function() {s2_isetmatchoption(this);};
               tdo.appendChild(select);
            } 
         }
         if (!tdo) {
            var headings = (jobj.Worksheet)?jobj.Headings[jobj.Worksheet]:jobj.Headings;
            var params = s2_disectid(id);
            tdo = document.createElement('TD');
            select = document.createElement('SELECT');
            select.className = 'MatchType_SameForAll';
            select.id = id;
            option = document.createElement('OPTION');
            option.value = '';
            option.appendChild(document.createTextNode('-- choose --'));
            select.appendChild(option); 
            option = document.createElement('OPTION');
            option.value = 'None';
            option.appendChild(document.createTextNode('* None'));
            select.appendChild(option); 
            option = document.createElement('OPTION');
            option.value = 'Match';
            option.appendChild(document.createTextNode('* Match Data'));
            select.appendChild(option);
            select.onchange = function() {s2_isetmatchoption(this);};
            tdo.appendChild(select);
            
         }
         tro = document.createElement('TR')
         tho = document.createElement('TH');
         tho.className = 'left';
         if (tname.match(/_ID$/)) {
            tname = tname.replace(/^[^_]+_/,'');
            tname = tname.replace(/_ID$/,'');
         }
         tho.appendChild(document.createTextNode(tname.replace(/\_/g,' ')));
         tro.appendChild(tho);
         tro.appendChild(tdo);
         tb.insertBefore(tro,tr);
         if (select) {
            s2_isetmatchoption(select);
            jobj = JSON.parse(iidnode.value);
         }
      }
      var td1,match;
      td1 = document.createElement('TD');
      td1.className = 'autodata';
      td1.appendChild(document.createTextNode('Derived Data'));
      tr.replaceChild(td1,td);
      match = new Object();
      match.Type = 'Derived';
      jobj.Matches[args.Property] = match;
      iidnode.value = JSON.stringify(jobj);
      //if (!jobj.Links) jobj.Links = new Object();
      //jobj.Links[args.TType] = tint;      
   }break;
   case 'None': {
      var td = node.parentNode;
      var tr = td.parentNode;
      var tb = tr.parentNode;
      tb.removeChild(tr);
      delete jobj.Matches[args.Property];
      iidnode.value = JSON.stringify(jobj);
   }break;
   case 'Match': {
      var headings = (jobj.Worksheet)?jobj.Headings[jobj.Worksheet]:jobj.Headings;
      var params = s2_disectid(node.id);
      var td = node.parentNode;
      var td2 = s2_imatchselect(node.id,headings,params); 
      var tr = td.parentNode;
      tr.replaceChild(td2,td);
   }break;
   case 'Static': {
      var nnode = document.createElement('INPUT');
      nnode.id = id;
      nnode.className = 'MatchType_Static';
      nnode.onblur = function() {s2_isetmatchoption(this);};
      node.parentNode.replaceChild(nnode,node);
      nnode.focus();            
      s2_isetmatchoption(nnode);
   }break;
   case '': {
      delete jobj.Matches[args.Property];
      iidnode.value = JSON.stringify(jobj);
   }break;
   default: {
      var match = (jobj.Matches[args.Property]!=null)?jobj.Matches[args.Property]:new Object();
      var bits = node.className.split(/\_/);
      var type = bits.pop();
      switch(type) {
      case 'IColumn': {
         match.Type = 'Link'; 
         match.IColumn = value;      
      }break;
      case 'TColumn': {
         match.Type = 'Link'; 
         match.TColumn = value;      
      }break;
      default: {
         if (match.IColumn) delete match.IColumn;
         if (match.TColumn) delete match.TColumn;
         match.Type = type; 
         match.Value = value;      
      }break;
      }
      if (args.IsCrumb != null) {
         match.IsCrumb = true;
         //match.Type = 'Crumb';
      } 
      jobj.Matches[args.Property] = match;
      iidnode.value = JSON.stringify(jobj);   
   }break;
   } 
//s2_debugjson(iidnode.value);
   return true;  
}

function s2_imatchworksheetselect(id,headings) {
   var td,select,option,value,ih,hx;
   td = document.createElement('TD');
   select = document.createElement('SELECT');
   select.id = id;
   select.className = 'MatchType_Worksheet';
   select.onchange = function() {s2_isetmatchworksheetoption(this)};
   var first = true;
   for(ih in headings) {
      option = document.createElement('OPTION');
      option.value = ih;
      option.appendChild(document.createTextNode(ih));
      select.appendChild(option);
      if (first) {
         option.selected = 'selected';
         first = false;
         s2_isetmatchworksheetoption(select);
      }      
   }
   td.appendChild(select);
   return td;
}
function s2_isetmatchworksheetoption(node) {
   var id = node.id
   var value = node.value;
   var args = s2_disectid(id);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);
   var jobj = JSON.parse(iidnode.value);
   jobj.Worksheet = value;
   //if (jobj.Headings[value]) jobj.Headings = jobj.Headings[value];
   iidnode.value = JSON.stringify(jobj);    
   //s2_debugjson(iidnode.value);  
}

function s2_drawimportform_getheadings(id) {
   var args = s2_disectid(id);
   s2_drawimportform_linkfiles(args);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);     
   var jobj = JSON.parse(iidnode.value);
   // add file check
   // get import file headings
   var fi;
   var req = new Object;
   req.sync = true;
   req.target = '../echofile_v1_1.php?a=getcolumnheadings&i=' + jobj.Files[jobj.ImportType];
   var iheadings = JSON.parse(snc_send(req));
   completed();
   showdialog();
   jobj.Headings = iheadings.Headings;
   iidnode.value = JSON.stringify(jobj);
   if (jobj.ImportType == 'xls') {
      s2_ichooseworksheet(id,jobj.Headings);
   } else {
      s2_drawimportform_matchdata(id);
   }  
}

function s2_ichooseworksheet(id,headings) {
   var args = s2_disectid(id);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);     
   var jobj = JSON.parse(iidnode.value);
   var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input,datatype;
   var table,thead,tbody,tr,th,td,node,text,id,lid,action,data,params,uploaders;
   var interfacetype,
   action = 'Import';
   dialog = document.getElementById('dialog-liner');
   dialog.removeChild(document.getElementById(id));
   
   if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
   if (!jobj.Data || jobj.Interim == true) {
      var h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode(action+' '+jobj.DisplayName));
      dialog.appendChild(h2);
   } 
   table = document.createElement('TABLE');
   table.id = action+'_'+jobj.IType;
   table.className = 'addform';
   tbody = document.createElement('TBODY');
   
   prop = 'Worksheet';
   id = 'Form-'+forms+'_IType-'+jobj.IType+'_Property-'+prop;
   td = s2_imatchworksheetselect(id,headings);
   tr = document.createElement('TR')
   th = document.createElement('TH');
   th.className = 'left';
   th.appendChild(document.createTextNode(prop.replace(/\_/g,' ')));
   tr.appendChild(th);
   tr.appendChild(td);
   tbody.appendChild(tr);
   
   table.appendChild(tbody);
   dialog.appendChild(table);
   button = document.createElement('BUTTON');
   button.onclick = function() {s2_drawimportform_matchdata(this.id);};
   button.id = 'Form-'+args.Form+'_IType-'+args.IType+'Action-Import_Stage-Review';
   button.appendChild(document.createTextNode('Match Columns'));
   dialog.appendChild(button); 
   
   //s2_debugjson(iidnode.value);     
}

function s2_drawimportform_crumbs() {
   var crumbs = s2_getcrumbs();
   var prop,crumb,select,option,ctype;
   if (!jobj.Matches) jobj.Matches = new Array();
   for (prop in crumbs) {
      crumb = crumbs[prop]; 
      id = 'Form-'+forms+'_IType-'+jobj.IType+'_DataType-LINKEDTO_TargetType-'+crumb.IType+'_Property-'+crumb.IType+'_IsCrumb-1';
      td = document.createElement('TD');
      select = document.createElement('SELECT');
      select.className = 'MatchType_SameForAll';
      select.id = id;
      option = document.createElement('OPTION');
      option.value = crumb.Current;
      option.appendChild(document.createTextNode(crumb.Name));
      select.appendChild(option); 
      option = document.createElement('OPTION');
      option.value = 'Match';
      option.appendChild(document.createTextNode('* Match Data'));
      select.appendChild(option);
      select.onchange = function() {s2_isetmatchoption(this);};
      td.appendChild(select); 
      tr = document.createElement('TR')
      th = document.createElement('TH');
      th.className = 'left';
      ctype = crumb.IType;
      if (ctype.match(/_ID$/)) {
         ctype = ctype.replace(/^[^_]+_/,'');
         ctype = ctype.replace(/_ID$/,'');
      }
      th.appendChild(document.createTextNode(ctype.replace(/\_/g,' ')));
      tr.appendChild(th);
      tr.appendChild(td);
      tbody.appendChild(tr);
      s2_isetmatchoption(select);      
   }            
}

function s2_drawimportform_matchdata(id) {
   var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input,datatype;
   var div,table,thead,tbody,tr,th,td,node,text,id,lid,action,data,params,uploaders;
   var interfacetype;
   
   var args = s2_disectid(id);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);     
   var jobj = JSON.parse(iidnode.value);
   jobj.Form = args.Form;
   action = 'Import';
   dialog = document.getElementById('dialog-liner');
   dialog.removeChild(document.getElementById(id));
   
   if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
   if (!jobj.Data || jobj.Interim == true) {
      var h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode(action+' '+jobj.DisplayName));
      dialog.appendChild(h2);
   }
   table = document.createElement('TABLE');
   table.id = action+'_'+jobj.IType;
   table.className = 'addform';
   tbody = document.createElement('TBODY');
   uploaders = new Array();
   
   var headings = (jobj.Worksheet)?jobj.Headings[jobj.Worksheet]:jobj.Headings;
   for(prop in jobj.Interface) {
      id = 'Form-'+forms+'_IType-'+jobj.IType+'_Property-'+prop;
      params = jobj.Interface[prop];
      datatype = (typeof params == 'object')?params.DataType:params.replace(/:.+/,'');
      if (!jobj.Data && datatype == 'File Path') uploaders.push(id);
      if (typeof params != 'object' || !params.Hidden) {
      
         if (!jobj.Data && jobj.Versions && prop == jobj.Versions.Column) data = (jobj.Versions.Maximum)?(Math.floor((jobj.Versions.Maximum + 0.1)*10)/10):1;
         else if (!jobj.Data && datatype == 'Count') data = (jobj.Count)?parseInt(jobj.Count)+1:1;       
         else data = (jobj.Data && jobj.Data[prop])?jobj.Data[prop]:null;
         td = s2_imatchselect(id,headings,params);
         if (td) {
            tr = document.createElement('TR')
            th = document.createElement('TH');
            th.className = 'left';
            if (prop.match(/_ID$/)) {
               prop = prop.replace(/^[^_]+_/,'');
               prop = prop.replace(/_ID$/,'');
            }
            th.appendChild(document.createTextNode(prop.replace(/\_/g,' ')));
            tr.appendChild(th);
            tr.appendChild(td);
            tbody.appendChild(tr);
         }
      }
   }
   table.appendChild(tbody);
   dialog.appendChild(table);
   button = document.createElement('BUTTON');
   button.onclick = function() {s2_drawimportform_collatedata(this.id);};
   button.id = 'Form-'+args.Form+'_IType-'+args.IType+'Action-Import_Stage-Collate';
   button.appendChild(document.createTextNode('Collate Data'));
   dialog.appendChild(button);   
   
   //s2_debugjson(iidnode.value);
   //showdialog();
   for (var i in uploaders) s2_inituploader(uploaders[i]);
   return true;
}
function s2_drawimportform_sendidata(id) {
   var args = s2_disectid(id);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);     
   var jobj = JSON.parse(iidnode.value);
   delete jobj.UserPermissions;
   delete jobj.Views;
   delete jobj.Alternatives;
   delete jobj.Domain;   
   var dialog,user,settings;
   dialog = document.getElementById('dialog-liner');
   dialog.removeChild(document.getElementById(id));   
   //alert(iidnode.value);
   //s2_debugjson(iidnode.value);
   var req = new Object();
   req.target = 'S2_query.php';
   var par = new Object();
   par.IType = jobj.IType;
   par.Action = 'Import';
   par.ImportSettings = jobj;
   //user = s2_getcookie('u_info');
   user = s2_getstorage('u_info');
   settings = s2_getcurrent();
   if (user) par.UInfo = user;
   if (settings) par.CurrentSettings = settings;
   req.request = par;
   switch(jobj.Stage) {
   case 'Collate': req.responder = s2_drawimportform_previewdata;break;
   case 'Import': req.responder = s2_drawimportform_reviewdata;break; 
   }    
   snc_send(req);    
   return true;   
}
function s2_drawimportform_collatedata(id) {
   var args = s2_disectid(id);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);
   jobj = JSON.parse(iidnode.value);
   jobj.Stage = 'Collate';
   jobj.Form = args.Form;   
   iidnode.value = JSON.stringify(jobj);
   s2_drawimportform_sendidata(id);   
}
function s2_drawimportform_previewdata(json) {
   var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input,datatype;
   var table,thead,tbody,tr,th,td,node,text,id,lid,action,data,params,uploaders;
   var interfacetype,jobj;
   if (json) {
      jobj = JSON.parse(json);
      cleardialog('dialog-liner');      
      dialog = document.getElementById('dialog-liner');
      
      node = document.createElement('INPUT');
      node.id = 'Form-'+jobj.Form+'_ImportSettings';
      node.type = 'hidden';
      node.value = JSON.stringify(jobj);
      dialog.appendChild(node);     
      action = 'Import';
         
      if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
      
      node = s2_searchbylinks(jobj);
      dialog.appendChild(node);
      h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode('Preview Data'));
      dialog.appendChild(h2);
      div = document.createElement('DIV');
      div.className = 's2_iImportTable'; 
      table = document.createElement('TABLE');
      table.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Table';
      table.className = 'listform';
      thead = document.createElement('THEAD');
      tr = document.createElement('TR');
      for(prop in jobj.Interface) {
         if (!jobj.Interface[prop].Hidden) {
            params = jobj.Interface[prop];
            params.Show = true;
            th = s2_getnode(null,prop,params,null,'ListHeader');
            if (th) tr.appendChild(th);
         }
      }
      thead.appendChild(tr);
      
      table.appendChild(thead);
      tbody = document.createElement('TBODY');
      for(index in jobj.ListData) {
         var listitem = jobj.ListData[index];
         tr = document.createElement('TR');
         var id,name=null,interfacetype;
         for(prop in jobj.Interface) {
            if (jobj.Interface[prop] == 'Unique Key') id = listitem[prop];
            if (!jobj.Interface[prop].Hidden) {
               nodeid = 'Form-'+jobj.Form+'_IType-'+jobj.IType+'_Property-'+prop+'_Current-'+id;
               params = jobj.Interface[prop];
               params.Show = true;
               td = s2_getnode(nodeid,prop,params,listitem[prop],'List');
               if (td) {
                  if (td.className == 'currentversion' || td.className == 'autodata') tr.className = td.className;
                  tr.appendChild(td);
               }
            }
         }
         //td = gen_getlistoptionsnode(jobj,listitem);
         //if (td) tr.appendChild(td);
         tbody.appendChild(tr);
      }
      table.appendChild(tbody);
      //table.onload = function() {table_split(this.id);};
      div.appendChild(table);
      dialog.appendChild(div);
      
      button = document.createElement('BUTTON');
      button.id = 'Form-'+jobj.Form+'_IType-'+jobj.IType+'_Action-ImportInterface';
      button.onclick = function() {s2_drawimportform_importdata(this.id);};
      button.appendChild(document.createTextNode('Import'));
      dialog.appendChild(button);
      
      gen_appendhiddenfields(jobj,dialog,forms);
   }
   
   //s2_debugjson(json);
   completed();
   showdialog();
   return true;   
}
function s2_drawimportform_importdata(id) {
   var args = s2_disectid(id);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);
   jobj = JSON.parse(iidnode.value);
   jobj.Stage = 'Import';   
   jobj.Form = args.Form;   
   iidnode.value = JSON.stringify(jobj);
   s2_drawimportform_sendidata(id);   
}
function s2_drawimportform_reviewdata(json) {
   var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input,datatype;
   var table,thead,tbody,tr,th,td,node,text,id,lid,action,data,params,uploaders;
   var interfacetype;
   if (json) {
   
      cleardialog('dialog-liner');
      jobj = JSON.parse(json);
      action = 'Import';
      dialog = document.getElementById('dialog-liner');
         
      if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
      
      node = s2_searchbylinks(jobj);
      dialog.appendChild(node);
      h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode('Review Import'));
      dialog.appendChild(h2);
      div = document.createElement('DIV');
      div.className = 's2_iImportTable'; 
      table = document.createElement('TABLE');
      table.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Table';
      table.className = 'listform';
      thead = document.createElement('THEAD');
      tr = document.createElement('TR');
      for(prop in jobj.Interface) {
         if (!jobj.Interface[prop].Hidden) {
            th = s2_getnode(null,prop,jobj.Interface[prop],null,'ListHeader');
            if (th) tr.appendChild(th);
         }
      }
      th = document.createElement('TH');
      th.appendChild(document.createTextNode('Status'));
      tr.appendChild(th);
      thead.appendChild(tr);
      
      table.appendChild(thead);
      tbody = document.createElement('TBODY');
      for(index in jobj.ListData) {
         var listitem = jobj.ListData[index];
         tr = document.createElement('TR');
         tr.className = 's2_i'+listitem.Status;
         var id,name=null,interfacetype;
         for(prop in jobj.Interface) {
            if (jobj.Interface[prop] == 'Unique Key') id = listitem[prop];
            if (!jobj.Interface[prop].Hidden) {
               nodeid = 'Form-'+forms+'_IType-'+jobj.IType+'_Property-'+prop+'_Current-'+id;          
               td = s2_getnode(nodeid,prop,jobj.Interface[prop],listitem[prop],'List');
               if (td) {
                  if (td.className == 'currentversion' || td.className == 'autodata') tr.className = td.className;
                  tr.appendChild(td);
               }
            }
         }
         td = document.createElement('TD');
         td.appendChild(document.createTextNode(listitem.Status));
         tr.appendChild(td);
         tbody.appendChild(tr);
      }
      table.appendChild(tbody);
      div.appendChild(table);
      dialog.appendChild(div);      
   }   
   //s2_debugjson(json);
   completed();
   showdialog();
   return true;   
}

function s2_debugjson(json) {
   if (json) {
      var dialog;
      dialog = document.getElementById('dialog-liner');
      var pre = snc_get_json_html(json);
      pre.style.height = '200px';
      pre.style.overflow = 'auto';
      var n = document.getElementById(pre.id);
      if (n) dialog.removeChild(n);
      dialog.appendChild(pre);
   }
   return true;
}
