/* 
   Move storage from cookies into JS object to avoid unnecessary things being 
   transmitted in new XMLHttpRequest model
*/
var s2storage = new Object();
function s2_getexpiry(mins) {
   var d = new Date();
   d.setMinutes ( d.getMinutes() + mins );
   return d.valueOf();
}
function s2_getstorageobject(value,expires) {
   var o,defexp;
   defexp = 60; // set default expiry to 1 hour
   if (!expires) expires = defexp;
   o = new Object();
   o.value = value;
   o.expiry = expires;
   o.expires = s2_getexpiry(expires);
   return o;
}
function s2_setstorage(name,value,expires) {
   s2storage[name] = s2_getstorageobject(value,expires);
   return true;   
}
function s2_getstorage(name) {
   var o,e,d,v;
   try {
      d = new Date();
      o = s2storage[name];
      if (o) {
         if (d.valueOf() <= o.expires) {
            v = o.value;
            o.expires = s2_getexpiry(o.expiry);
            s2storage[name] = o;
         } else {
            delete s2storage[name] 
            v = null;
         }
      } 
   } catch (e) { /* do I need to do something with this */ }
   return v;
}
function s2_delstorage(name) {
   delete s2storage[name];
}
function s2_emptystorage() {
   s2storage = new Object();
}
function s2_loadstorage() {
   var s2s,s2sc,s2e,s2ec,e,s2encrypt,prop;
   try {
      s2s = s2_getcookie('s2s');
      s2sc = cdm_decrypt(s2s,'AB');
      s2storage = JSON.parse(s2sc);
      s2_delcookie('s2s');
   } catch (e) { /* do I need to do something with this */ }
   try {
      s2e = s2_getcookie('s2e');
      s2ec = cdm_decrypt(s2e,'AB');
      s2encrypt = JSON.parse(s2ec);
      for(prop in s2encrypt) s2storage[prop] = s2_getstorageobject(s2encrypt[prop]); 
      s2_delcookie('s2e');         
   } catch(e) { /* do I need to do something with this */ }
}
function s2_savestorage() {
   var s2s,e,s2se;
   try {
      s2s = JSON.stringify(s2storage)
      s2se = cdm_encrypt(s2s,'AB');
      s2_setcookie('s2s',s2se,60);
   } catch(e) {
      alert('Unable to save settings');
   }
}
function s2_movedomain() {
   s2_delstorage('S2Interim');
   s2_savestorage();
   return true;
}

function s2_flip_encryption(buttonid) {            
   var txt,img,onoff,button;
   if (!buttonid) {
      onoff = (!s2_getstorage('encrypt'))?'encrypted':'open';
      button = document.getElementById('flip_encrypt');   
   } else {
      button = document.getElementById(buttonid);
      onoff = button.firstChild.alt;
   }
   switch (onoff) {
   case 'encrypted': {
      txt = 'open';
      img = new Image();
      img.src = '../images/padlock_open.gif'
      img.alt = txt;
      button.replaceChild(img,button.firstChild);
      // set encryption settings to last 2 weeks
      s2_setstorage('encrypt',0,20160);
   }break;
   case 'open': {
      txt = 'encrypted';
      img = new Image();
      img.src = '../images/padlock_closed.gif';
      img.alt = txt;
      button.replaceChild(img,button.firstChild); 
      cdm_clientinit();
   }break;     
   }
   return true;   
}
function s2_ihelpdiv() {
   div = document.createElement('DIV');
   div.className = 's2_iHelp';
   button = document.createElement('BUTTON');
   button.onclick = function() {s2_ihelpshowhide(this);};
   img = document.createElement('IMG');
   img.src = '../images/plus14.gif';
   img.alt = 'Show help';
   button.appendChild(img);
   div.appendChild(button);
   return div;   
}
function s2_ihelpshowhide(el) {
   var par,x,cx;
   par = el.parentNode;
   if (par.className == 's2_iHelp') {
      par.className = 's2_iHelpV';
      el.firstChild.src = '../images/minus14.gif';
      el.firstChild.alt = 'Hide help';
   } else {
      par.className = 's2_iHelp';
      el.firstChild.src = '../images/plus14.gif';
      el.firstChild.alt = 'Show help';
   }
}
// the start function establishes the session / encryption settings / sets up 
// the screen and performs the first query to the server to populate the 
// interface.
window.onresize = function() {s2_resize();};
function s2_resize() {
   placescreen();
   setscreenres();
}
function s2_getusername() {
/* this needs changing */
   var uinfo;
   uinfo = s2_getcookie('u_info');
   uinfo = s2_decrypt(uinfo,'AB');
   alert(uinfo);
}
function s2_start() {
   var parameters,encrypt,drawn,jp,qs,par,pc;
   s2_loadstorage();
   parameters = s2_drawsettings();      
   s2_resize();
   encrypt = parseInt(s2_getstorage('encrypt'));
   if (encrypt) syncauth();
   drawn = s2_setupdomaininterface(); // FIX POSTGIS VERSION
   jp = s2_getstorage('S2Parameters');
   qs = s2_getqsparams();
   if (jp) {
      parameters = jp;
      s2_delstorage('S2Parameters'); 
   } else if (qs) parameters = qs;
   if (!drawn) s2_query(parameters);     
   else s2_query({"Action":"CheckIn"});
   completed();  
}
function s2_getqsparams(param) {
   var query,vars,pair,params;
   query = window.location.search.substring(1);
   vars = query.split("&");
   params = new Object();   
   for (var i=0;i<vars.length;i++) {
      pair = vars[i].split("=");
      if (pair[0] && pair[1]) params[pair[0]] = pair[1];      
   }
   var rval = (param && params[param])?params[param]:(!param && i>=1)?params:null;
   return rval;
}
// the query function is used to request data from the server. 
// the query can add / delete / update or list data. data for an item 
// is returned with the data for linked items. so the data for the current 
// partnership comes back with the associated meetings and site list. 
function s2_getcurrentpath() {
   var path = window.location.pathname;
   path = path.replace(/^\/+/,'');
   path = path.replace(/\/$/,'');
   var paths = path.split(/\//);
   var path = paths.join('/');
   path = (path.match(/^\/+/))?path:'/'+path; 
   var url = 'http://'+window.location.host + path;
   return url; 
}
function s2_getcurrentdomain() {
   var path = window.location.pathname.split(/\//);
   var dom;
   var s2path = s2_getcookie('S2Path');
   while (dom != s2path) dom = path.shift(); // FIX THIS FOR INSTALLATIONS OUTSIDE /s2/
   dom = path.shift();
   return dom;
}
function s2_query(parameters) {
   inprogress();
   var req = new Object();
   var encrypt = s2_getstorage('encrypt');
   var i,obj,settings,user;

   //if(encrypt) req.target = 'http://'+unescape(s2_getcookie('cdm_server'))+'/sites/S2_query.php'; 
   //else req.target = './S2_query.php';
   req.target = s2_getcurrentpath()+'/S2_query.php';
   settings = s2_getcurrent();
   if (parameters) {
      var s2interim = s2_getinterim(parameters.IType);
      if (s2interim && (parameters.Action == 'Add' || parameters.Action == 'View' || parameters.Action == 'MSelect')) {
         s2_updateinterim(parameters.IType);
      } 
   } else if (settings) {
      parameters = new Object();
      for (i in settings) {
         obj = settings[i];
         if (obj.IType && obj.Domain == s2_getcurrentdomain() && obj.Current && obj.Name != null) {
            parameters.IType = obj.IType;
            parameters.Current = obj.Current;
         }
      }
      if (parameters.IType && parameters.Current!='Multiple') parameters.Action = 'View';
   }
   qs = s2_getqsparams();
   if (qs) for(i in qs) parameters[i] = qs[i];
   user = s2_getcookie('u_info');
   if (user) parameters.UInfo = user; 
   parameters.CurrentSettings = settings;
   
   req.request = parameters;
   req.responder = s2_draw;
   snc_send(req);   
}
var forms = 0;
// the draw function creates the interface based on the data returned from the 
// server and (if present) the interim cookie. 
// the interim cookie allows you to fill out some fields in a form and then go 
// and enter or select linked data and return to the form. 
function s2_draw(json) {
//snc_report_json_obj(json);
   var i,jobj,sobj,prop,iprop,nprop,link,add,id,name,obj,err;
   try {
      jobj = JSON.parse(json);
   } catch(err) {
      alert('Action Failed:\nServer Communication Error');
      completed();
      return null;   
   }
   if (jobj.UInfo) {
//alert(JSON.stringify(jobj.UInfo));
      s2_setstorage(jobj.UInfo.Name,jobj.UInfo.Cookie,jobj.UInfo.Expiry);
      alert(jobj.Message);
   } 
   if (jobj.UserPermissions) {
      s2_updateusermenu(jobj.UserPermissions);
      s2_enableextender(jobj.UserPermissions); // FIX POSTGIS VERSION
   } //else s2_updateusermenu();
   if (jobj.Status == 0) {
      alert('Action Failed:\n'+jobj.Message);                                       
      completed();
      //s2_query(); 
   } else if (jobj.NoDraw) {
      completed();
      if (jobj.Refresh) s2_refresh();
   } else {
      snc_clearMenus('incontentdialog');
      forms = 0;
      if (s2_drawinterim(jobj.IType)) {
         var s2interim = s2_getinterim(jobj.IType);
         for (prop in s2interim.Data) {
            var type = s2interim.Data[prop];
            if (type && typeof type == 'object' && type.IsInterim) {
               var inttype = type.IType;
               if (inttype == jobj.IType) { 
                  sobj = new Object;
                  sobj.IType = jobj.IType;
                  sobj.DisplayName = jobj.DisplayName;
                  if (jobj.Current == 'Multiple' || type.DataType == 'LINKEDTO') {
                     sobj.Name = jobj.ItemName;
                     sobj.Id = jobj.Current;
                     sobj.ListData = jobj.ListData;
                  } else {
                     obj = new Object();
                     obj.Name = jobj.ItemName;
                     obj.Id = jobj.Current;
                     jobj.ListData = new Array();
                     jobj.ListData.push(obj);
                  }
                  if (type.DataType == 'LINKEDTOM') {
                     for (lprop in jobj.ListData) {
                        add = true;
                        obj = new Object();
                        obj.Id = (jobj.ListData[lprop].Id)?jobj.ListData[lprop].Id:jobj.ListData[lprop][jobj.Key];
                        obj.Name = jobj.ListData[lprop]['Name'];
                        for (iprop in type.IData) {
                           if (obj.Id == type.IData[iprop].Id) add = false;
                        }
                        
                        if (add) {
                           if (!type.IData) type.IData = new Array();
                           type.IData.push(obj);
                        }
                     }
                     sobj.ListData = type.IData;
                  } 
                  sobj.Key = jobj.Key;
                  s2interim.Data[prop] = sobj;
                  s2interim.Interim = true;
                  //s2_updateinterim(jobj.IType);
                  s2_unsetinterim(jobj.IType); 
//alert(JSON.stringify(jobj));
               }
            }
         }
         jobj = s2interim;                  
      } //else alert('Draw Original Interface');
      hidedialog();
      completed();
      if (jobj.Form) {
         s2_drawalternatives(jobj);
         s2_drawform(jobj);
         s2_drawsettings();
      } else {
         if (jobj.Current) {
            sobj = new Object();
            sobj.IType = jobj.IType;
            sobj.DisplayName = jobj.DisplayName; 
            sobj.Name = jobj.ItemName;
            sobj.Current = jobj.Current;
            sobj.Domain = jobj.Domain;
            s2_setcurrent(sobj);
            s2_drawsettings();
            if (jobj.ManagePermissions) s2_drawpermissionslink(jobj.ManagePermissions);
            s2_drawalternatives(jobj);
            if (jobj.Current != 'Multiple') s2_drawheader(sobj);
            if (jobj.Views) s2_drawviews(jobj);
            
            if (!jobj.ViewName) s2_drawaddform(jobj);
            else s2_drawhiddenparent(jobj);
            s2_setstorage('S2ViewName',jobj.ViewName);
         } else if (jobj.ListData) {
            s2_drawalternatives(jobj);
            s2_drawlisttable(jobj); 
         } else {
            s2_drawalternatives(jobj);
            s2_drawaddform(jobj);
         } 
         if (jobj.LinkedData && jobj.LinkedData.length > 0) {
            for (i in jobj.LinkedData) {
               link = jobj.LinkedData[i];
               if (link.Data) {
                  s2_drawheader(link);
                  s2_drawaddform(link);
               } else if (link.Type == 'Links') {
                  s2_drawheader(link);
                  s2_drawlinks(link);
               }            
               else s2_drawlisttable(link,jobj.IType);   
            }
         }
         if (jobj.External) {
            s2_getexternaldata(jobj);
         }
         if (jobj.PostProcess && jobj.Current) {
            if ((jobj.Data.PP_Step+1) < jobj.PostProcess.length && !jobj.ViewName) s2_postprocess(jobj);
            s2_postprocesslink(jobj);
         }
         //table_setupmanager();
         s2_intenablereturnbutton();         
      }  
   }
//snc_report_json_obj(json);
//snc_report_json_obj(JSON.stringify(s2storage));
}
function s2_postprocesslink(jobj) {
   var e,sid,node,a,btn,input;
   try {
      sid = 'Setting_IType-'+jobj.IType;
      node = document.getElementById(sid);
      btn = document.createElement('BUTTON');
      btn.name = 'Continue '+jobj.DisplayName.toLowerCase()+ ' processing';
      //btn.appendChild(document.createTextNode('\u03A6'));
      btn.appendChild(document.createTextNode('~'));
      btn.id = 'Setting_IType-'+jobj.IType+'_Action-Process_Current-'+jobj.Current;
      //a.href = '#';
      btn.onclick = function() {
         s2_postprocess(jobj);
         return false;
      };
      s2_addtooltip(btn);
      node.appendChild(btn);
   } catch (e) {}  
}
function s2_updateusermenu(perms) {
   var domain, label;
   var div, node, a, text, url, insert, ibutton;
   div = document.getElementById('s2menubar');
   node = div.lastChild;
   
   while (node.id != 's2menuheadings') {
      if (node.id == 's2_interfacebutton') ibutton = node;
      div.removeChild(div.lastChild);
      node = div.lastChild;
   }
   var u = s2_getcookie('u_info');
   if (u && perms && perms.UserName) {
      if (perms.users.list_users) {
         a = document.createElement('A');
         a.href = '../users' + window.location.search;//?a=List&i=USR_Users';
         a.onclick = function() {
            s2_setstorage('S2Parameters','{"Action":"List","IType":"USR_Users"}');
         };
         a.className = 'buttonLink';
         a.appendChild(document.createTextNode('[ List Users ]'));
         div.appendChild(a);                    
      }
      if (perms.users.edit_self) {
         a = document.createElement('A');
         a.href = '../users' + window.location.search;//?a=View&i=USR_Users';
         a.onclick = function() {
            s2_setstorage('S2Parameters','{"Action":"View","IType":"USR_Users"}');
         };
         a.className = 'buttonLink';
         a.appendChild(document.createTextNode('[ My Settings ]'));
         div.appendChild(a);                 
      }
      a = document.createElement('A');
      a.href = '../users';//?a=Logout&i=USR_Users';
      a.className = 'buttonLinkRight';
      a.onclick = function() {
         s2_delcookie('u_info');
         s2_emptystorage();
         s2_setstorage('S2Parameters','{"Action":"Logout","IType":"USR_Users"}');
         s2_unsetcurrent();
      };
      a.appendChild(document.createTextNode('[ Logout ]'));
      div.appendChild(a);
      a = document.createElement('A');
      a.href = '../users';//?a=View&i=USR_Users';
      a.onclick = function() {
         s2_setstorage('S2Parameters','{"Action":"View","IType":"USR_Users"}');
      };
      a.className = 'buttonLinkRight';
      a.appendChild(document.createTextNode('[ Login: '+ perms.UserName +' ]'));
      div.appendChild(a);
   } else {
      a = document.createElement('A');
      a.href = '../users';//?a=Register&i=USR_Users';
      a.onclick = function() {
         s2_setstorage('S2Parameters','{"Action":"GetForm","IType":"USR_Users","Form":"Register"}');
      };
      a.className = 'buttonLinkRight';
      a.appendChild(document.createTextNode('[ Register ]'));
      div.appendChild(a);  
      a = document.createElement('A');
      a.href = '../users';//?a=GetForm&i=USR_Users';
      a.onclick = function() {
         s2_setstorage('S2Parameters','{"Action":"GetForm","IType":"USR_Users"}');
      };
      a.className = 'buttonLinkRight';
      a.appendChild(document.createTextNode('[ Login ]'));
      div.appendChild(a);
      div.appendChild(document.createTextNode('...'));
   }
   if (ibutton) div.appendChild(ibutton);
   node = document.createElement('HR');
   node.style.clear = 'both';
   div.appendChild(node);    
}
function s2_drawpermissionslink(lobj) {
   var prop,value,div,node,li,h4,a,json,domain,denc;
   div = document.getElementById('left-menus');
   node = div.firstChild;
   var path = s2_getcurrentpath();
   path = path.replace(/\/[^\/]+$/,'/'+lobj.ToDomain);
   domain = new Object();
   for (prop in lobj.Parameters) {
      domain[prop] = lobj.Parameters[prop];
   }
   denc = getencrypted(JSON.stringify(domain),'AB');
   path += '?d='+denc;  
   while(node && node.nodeName != 'UL') node = node.nextSibling;
   if (node) {
      json = JSON.stringify(lobj);
      li = document.createElement('LI');
      h4 = document.createElement('H4');
      h4.appendChild(document.createTextNode('Manage Permissions'));                                                   
      li.appendChild(h4);
      a = document.createElement('A');
      a.href = path;
      a.className = 'ListEntry';
      a.appendChild(document.createTextNode(lobj.Parameters.alias));
      a.name = 'Manage user permissions';
      s2_addtooltip(a);
      li.appendChild(a);
      node.appendChild(li);
   }      
}
function s2_itypeselect(id) {
   var args = s2_disectid(id);
      //s2_removecrumbs(id); WHAT WAS THIS HERE FOR?
   s2_query(args);
   return true;
}
function s2_removecrumbs(id) {
   var args = s2_disectid(id);
   var settings = s2_getcurrent();
   var set,cur = false;
   var dom = s2_getcurrentdomain();
   //alert(JSON.stringify(settings));
   for(prop in settings) {
      set = settings[prop];
      if (args.IType == set.IType) cur = true;
      if (cur && dom == set.Domain) s2_unsetcurrent(prop);
   }   
}
function s2_getcrumbs() {
   var dom = s2_getcurrentdomain();
   var settings = s2_getcurrent();
   //alert(JSON.stringify(settings));
   var crumbs = new Array();
   for(prop in settings) {
      set = settings[prop];
      if (dom == set.Domain) crumbs.push(set);
   }
   // the last crumb should be the current item
   crumbs.pop();
   return crumbs;   
}
function s2_itypeselectall(id) {
   var args,table,tbody,rownum,tr,td,tick,val,current,targs;
   args = s2_disectid(id);
   table = document.getElementById('Form-'+args.Form+'_IType-'+args.IType+'_Table');
   tbody = table.tBodies[0];
   args.Current = new Array();
   for (rownum=0;rownum<tbody.childNodes.length;rownum++) {
      tr = tbody.childNodes[rownum];
      td = tr.lastChild;
      tick = td.firstChild;
      while (tick && tick.className != 'tick') tick = tick.nextSibling;
      if (tick.checked) {
         targs = s2_disectid(tick.id);
         args.Current.push(targs.Current);
      }
   }
   if (args.Current.length == 1) {
      args.Action = 'View';
      args.Current = args.Current[0];
      s2_query(args);      
   } else if (args.Current.length > 0) {
      args.Action = 'MSelect';
      s2_query(args);      
   } else {
      alert('Nothing selected.\nNo boxes have been ticked.');
   }   
   return true;      
}
// form fields are given and id with key value pairs separated by underscores (_)
// with the key and value separated by a hyphens (-)
// forms are numbered sequencially on the page using the global forms variable
// eg Form-1_IType-Site_Action-Edit_Current-2
// the equivalent get method query string would be
// ?Form=1&IType=Site&Action=Edit&Current=2
// the get syntax has not been used as it contains characters which don't meet 
// the standard for HTML element ids   
function s2_disectid(id) {
   var args = new Object();
   if (id) {
      var pairs = id.split('_');
      var i,keyvals;
      for(i=0;i<pairs.length;i++) {
         if (!pairs[i].match('-')) {
            pairs[i] = pairs[i-1] + '_' + pairs[i];
            pairs = (pairs.slice(0,i-1)).concat(pairs.slice(i));
            i--;
         }
      }
      for(i in pairs) {
         keyvals = pairs[i].split('-');
         args[keyvals[0]] = keyvals[1];   
      }
   }
   return args; 
}
function s2_buildid(args) {
   var arg,id,col,val,pairs;
   pairs = new Array();
   for(col in args) {
      val = args[col];
      pairs.push(col+'-'+val); 
   }
   id = pairs.join('_');
   return id;
}


// the settings consist of objects with an IType, Name and Id. 
// these are stored as json in a settings cookie and are drawn to the left menu 
// giving the user clickable links to all the currently selected table entries.
function s2_removesetting(id) {
   var args = s2_disectid(id);
   s2_unsetcurrent(args.IType);
   s2_drawsettings();
   s2_hidetooltip();
   //args.Action = 'List';
   //s2_query(args);
   s2_query();
}

function s2_drawsettings() {
   var div,i,obj,cdom,cobj;
   //var table,thead,tbody,tr,th,td,node,a;
   var ul,li,a,button,h2,h3,h4,text,span,ipt;
   //forms++;
   snc_clearMenus('left-menus');
   div = document.getElementById('left-menus');
   h2 = document.createElement('H2');
   h2.appendChild(document.createTextNode('Settings'));
   div.appendChild(h2);
   ul = document.createElement('UL');
   var settings = s2_getcurrent();
   for (i in settings) {
      obj = settings[i];
      cdom = s2_getcurrentdomain();
      if (obj.Name && obj.Current) {
         if (!obj.Domain || obj.Domain == cdom) {
            li = document.createElement('LI');
            li.id = 'Setting_IType-'+obj.IType;
            h4 = document.createElement('H4');
            h4.appendChild(document.createTextNode(obj.DisplayName));  
            li.appendChild(h4);
            
            text = obj.Name;//.replace(/@/,' @ ');
            if (text.match(/@/)) {
               a = document.createElement('A');
               a.className = 'ListEntry';
               a.href = 'mailto:'+text;
               a.appendChild(document.createTextNode(text));
               li.appendChild(a);
               li.appendChild(document.createTextNode(' '));
            } else if (text.match(/http:\/+/)) {
               a = document.createElement('A');
               a.className = 'ListEntry';
               a.href = text;
               a.appendChild(document.createTextNode(text));
               li.appendChild(a);
               li.appendChild(document.createTextNode(' '));
            } else {
               span = document.createElement('SPAN');
               span.appendChild(document.createTextNode(text));
               li.appendChild(span);
            }
            li.appendChild(document.createElement('BR'));
            
            button = document.createElement('BUTTON');
            button.appendChild(document.createTextNode('\xD7'));
            button.name = 'Remove '+obj.DisplayName.toLowerCase()+' setting';
            button.id = 'Form-'+forms+'_IType-'+obj.IType+'_Action-Unset_Current-'+obj.Current;
            button.onclick = function() {s2_removesetting(this.id);};
            s2_addtooltip(button);
            li.appendChild(button);
            
            button = document.createElement('BUTTON');
            button.name = 'View current '+obj.DisplayName.toLowerCase();
            button.appendChild(document.createTextNode('\u2192'));
            button.id = 'Form-'+forms+'_IType-'+obj.IType+'_Action-View_Current-'+obj.Current;
            button.onclick = function() {s2_itypeselect(this.id);return false;};
            s2_addtooltip(button);
            li.appendChild(button);
            /*
            if (jobj.PostProcess && jobj.Current && !jobj.ViewName) {
               button = document.createElement('BUTTON');
               button.name = 'View current '+obj.DisplayName.toLowerCase();
               button.appendChild(document.createTextNode('\u221E'));
               button.id = 'Form-'+forms+'_IType-'+obj.IType+'_Action-Process_Current-'+obj.Current;
               button.onclick = function() {
                  var jobj,json,args;
                  args = s2_disectid(this.id);
                  json = document.getElementById('Form-'+args.Form+'_IType-'+args.IType+'_Definition-Process');
                  jobj = JSON.parse(jobj);
                  s2_postprocess(jobj);
                  return false;
               };
               s2_addtooltip(button);
               li.appendChild(button);
               ipt = document.createElement('INPUT');
               ipt.type = 'hidden';
               ipt.value = JSON.stringify(jobj);
               ipt.id = 'Form-'+forms+'_IType-'+obj.IType+'_Definition-Process';
               li.appendChild(ipt);  
            }
            */
            ul.appendChild(li);
            cobj = obj;
         }
      }
   }
   div.appendChild(ul);
   var d = s2_getqsparams('d');
   if (d) {
      d = cdm_decrypt(d,'AB');
      d = JSON.parse(d);
      h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode('Permissions For'));
      div.appendChild(h2);
      ul = document.createElement('UL');
      li = document.createElement('LI');
      h4 = document.createElement('H4');
      h4.appendChild(document.createTextNode(d.alias));  
      button = document.createElement('BUTTON');
      //button.innerHTML = '&times;';
      button.appendChild(document.createTextNode('\xD7'));
      button.name = 'Remove effective domain setting';
      button.onclick = function() {s2_removeqs(this);};
      s2_addtooltip(button);
      h4.appendChild(button);
      li.appendChild(h4);
      ul.appendChild(li);
      div.appendChild(ul);
   }   
   if (cobj) cobj.Action = 'View';
   return cobj; 
}
function s2_removeqs(el) {
   window.location.search = null;
   // ul>li>h4>button
   var ul = el.parentNode.parentNode.parentNode;
   var h2 = ul.previousSibling;
   var div = ul.parentNode;
   div.removeChild(ul);
   div.removeChild(h2);   
}

function s2_addtooltip(el) {
   el.onmouseover = function(e) {s2_showtooltip(this,e);};
   el.onmouseout = function(e)  {s2_hidetooltip();};
}
function s2_showtooltip(el,e) {
   var x,y,div,node;
   var evt=window.event || e;
   x = (Math.floor(evt.clientX/10)-1) * 10;
   x = (x<10)?10:x;
   x += document.body.scrollLeft;
   y = (Math.floor(evt.clientY/10)+2) * 10;
   y = (y<10)?10:y;
   y += document.body.scrollTop;   
   div = document.createElement('DIV');
   div.style.position = 'absolute';
   div.style.left = x + 'px';
   div.style.top = y + 'px';
   div.className = 's2tooltip';
   div.id = 's2tooltip';
   div.appendChild(document.createTextNode(el.name)); 
   div.onmouseout = function(e) {s2_hidetooltip();};      
   document.getElementById('main').appendChild(div);
   return true;  
}
function s2_hidetooltip() {
   // only hide tooltip if there is one. 
   try {
      var node = document.getElementById('s2tooltip'); 
      if (node) node.parentNode.removeChild(node);
   } catch (e) {}
   return false;
}

// cookie alias functions
String.prototype.chunk = function(n) {
   if (typeof n=='undefined') n=2;
   return this.match(RegExp('.{1,'+n+'}','g'));
};

var csize = 2048;
function s2_setcookiechunks(name,value,expires) {
   var x,chunks,cname,rem,last;
   chunks = value.chunk(csize);
   for(x=0;x<chunks.length;x++) {
      cname = name + '_ck_' + (x+1);
      createCookie(cname,chunks[x],expires);
   }
   return chunks.length;    
}
function s2_getcookiechunks(name,chunks) {
   var ckx = parseInt(chunks.replace(/ck/,''));
   var val,cval,cname,x;
   for(x=1;x<=ckx;x++) {
      cname = name + '_ck_' + x;
      cval = readCookie(cname);
      if (val) val = val + cval;
      else val = cval;
   }
   return val;  
}
function s2_delcookiechunks(name,chunks) {
   var ckx = parseInt(chunks.replace(/ck/,''));
   var val,cval,cname,x;
   for(x=1;x<=ckx;x++) {
      cname = name + '_ck_' + x;
      cval = eraseCookie(cname);
      if (val) val = val + cval;
      else val = cval;
   }
   return val;  
}
function s2_setcookie(name,value,expires) {
   var ck,escval;
   escval = escape(value);
   if (escval.length > csize) {
      ck = s2_setcookiechunks(name,escval,expires);
      createCookie(name,'ck'+ck,expires);
   } else {
      createCookie(name,escval,expires);
   }    
   return true;
}
function s2_getcookie(name) {
   var c,chunks;
   c = readCookie(name);
   if (c && c.match(/ck\d+/)) c = s2_getcookiechunks(name,c); 
   if (c) c = unescape(c);
   return c;
}
function s2_delcookie(name) {
   var c; 
   c = readCookie(name);
   if (c && c.match(/ck\d+/)) c = s2_delcookiechunks(name,c);
   eraseCookie(name);
   return true;
}

function s2_inarray(needle, haystack) {
   var i,length;
   length = haystack.length;
   for (i = 0;i < length;i++) {
      if(haystack[i] == needle) return true;
   }
   return false;                     
}


// settings are stored in a cookie called S2Settings as json with the 
// itype, name field and id 
// for tables which only contain link data, the getname method is overridden on 
// the server end to calculate a name field for the entry. 
function s2_getcurrent(itype) {
   var s2settings = s2_getstorage('S2Settings');
   if (s2settings) {
      if (itype) return s2settings[itype];
      else return s2settings;
   } else return new Object(); 
}
function s2_setcurrent(itypeobj) {
   var s2settings = s2_getcurrent();
   if (itypeobj.DisplayName && itypeobj.Current) {
      var obj = new Object();
      obj.IType = itypeobj.IType;
      obj.DisplayName = itypeobj.DisplayName;
      obj.Current = itypeobj.Current;
      obj.Name = itypeobj.Name;
      obj.Domain = itypeobj.Domain;
      s2settings[itypeobj.IType] = obj;
      s2_setstorage('S2Settings',s2settings);
   }
   return true;   
}
function s2_unsetcurrent(itype) {
   if (itype) {
      var s2settings = s2_getcurrent();
      var prop,set = false;
      delete s2settings[itype];
      s2_setstorage('S2Settings',s2settings);
   } else s2_delstorage('S2Settings');
   return true;   
}

function s2_setpage(itype,page) {
   var itypeobj,cdom;
   var s2settings = s2_getcurrent();
   cdom = s2_getcurrentdomain();
   if (s2settings[itype]) {
      itypeobj = s2settings[itype];
      itypeobj.Page = page;
   } else {
      itypeobj = new Object();
      itypeobj.Domain = cdom;
      itypeobj.IType = itype;
      itypeobj.Page = page;
   }
   s2settings[itype] = itypeobj;
   s2_setstorage('S2Settings',s2settings);
   s2_refresh();   
}   
function s2_refresh() {
   var vtype,vcurr,vview,args;
   var s2settings = s2_getcurrent();   
   vtype = (document.getElementById('Form-1_ViewIType'))?document.getElementById('Form-1_ViewIType').value:null;
   vview = (document.getElementById('Form-1_ViewName'))?document.getElementById('Form-1_ViewName').value:'';
   vcurr = (s2settings[vtype] && s2settings[vtype]['Current'])?s2settings[vtype]['Current']:null;
   if (!vtype) s2_query();
   else {
      args = new Object();
      args.IType = vtype;
      if (vcurr) { 
         args.Action = 'View';
         args.Current = vcurr;
         args.ViewName = vview;
      } else {
         args.Action = 'List';
      }
      s2_query(args);
   }
   return true;
}
function s2_searchby(id) {
   var sby,sfor,itypeobj,cdom;
   args = s2_disectid(id);
   id = id.replace(/\-Exec/,'');
   sby = document.getElementById(id+'-By').value;
   sfor = document.getElementById(id+'-For').value;
   cdom = s2_getcurrentdomain();
      
   var s2settings = s2_getcurrent();
   if (s2settings[itypeobj]) {
      itypeobj = s2settings[itype];
      itypeobj.SearchBy = sby;
      itypeobj.SearchFor = sfor;
   } else {
      itypeobj = new Object();
      itypeobj.Domain = cdom;
      itypeobj.IType = args.IType;
      itypeobj.SearchBy = sby;
      itypeobj.SearchFor = sfor;
   }
   s2settings[args.IType] = itypeobj;
   s2_setstorage('S2Settings',s2settings);
   s2_refresh();
   return true;
}

// when add forms contain linked data these have to be set / added and then 
// the user gets returned to the form they were filling out.

// when the users clicks set on a linked data element in a form the current form
// data is saved to an "interim" cookie. when the server returns from adding or 
// setting the link data the form is re-populated with the contents from the
// cookie and the cookie is deleted.

// each time a server query returns, or an existing element is selected, the 
// draw function checks for an interim cookie and draws that interface if there 
// is one or the returned interface if there isn't 

function s2_drawinterim(target) {
   var s2interim = s2_getinterim(target);
   var drawinterim;
   drawinterim = (s2interim)?(s2interim.Status == 'Redraw'):false;
   //s2_updateinterim(target);
   return drawinterim;
}

function s2_saveinterim(id)  {
   var s2interim = s2_getinterim();
   var obj,node,id,name,cid,coldata;
   var args = s2_disectid(id);
   var target;
   var iface = JSON.parse(document.getElementById('Form-'+args.Form+'_Interface').value);
   var interfacetype,parameters,obj,id,name,iobj;
   parameters = new Object();
   for(prop in iface.Interface) {
      if (prop == args.Property) {
         if (typeof iface.Interface[prop] == 'object') itype = iface.Interface[prop].TargetType; 
         else itype = iface.Interface[prop].replace(/LINKEDTO[M]*:([^\.]+)\..+/,"$1");
         //parameters[prop] = '#INTERIM#'+itype;
         iface.Target = (itype)?itype:args.IType;
         id = 'Form-'+args.Form+'_IType-'+iface.IType+'_Property-'+prop;
         obj = new Object();
         obj.IsInterim = true;
         obj.IType = itype;
         obj.DataType = iface.Interface[prop].DataType;
         iobj = s2_getnode(id,prop,iface.Interface[prop],null,'Interim');
         if (iobj && iobj.ListData) obj.IData = iobj.ListData; 
         parameters[prop] = obj;
      } else {
         if (typeof iface.Interface[prop] == 'object') interfacetype = iface.Interface[prop].DataType;
         else interfacetype = iface.Interface[prop].replace(/([^\:]+).*/,'$1');
         switch (interfacetype) {
         case 'Unique Key': if(iface.Data) cid = iface.Data[prop]; break;
         default: {
            id = 'Form-'+args.Form+'_IType-'+iface.IType+'_Property-'+prop; 
            parameters[prop] = s2_getnode(id,prop,iface.Interface[prop],null,'Interim');
         }break;            
         }   
      }
   }
   if (cid) iface.Current = cid;
   iface.Data = parameters;
   iface.Status = 'Setting';
   delete iface.UserPermissions;
   var s2inum;
   s2inum = s2_deleteexpiredinterimdefs(); 
   s2interim[iface.Target] = s2inum;
   s2_saveinterim_x(iface,s2inum);
//alert(s2inum + '\n' + JSON.stringify(iface));
   s2_setstorage('S2Interim',s2interim);
   s2_savestorage();
//alert(JSON.stringify(s2interim));
   return true;   
}

function s2_saveinterim_x(iface,number) {
   var cname = 'S2Int'+number;
   s2_setstorage(cname,iface);
   return true;   
}
function s2_deleteexpiredinterimdefs() {
   var s2interim = s2_getinterim();
   var type,s2int,s2inum,s2imax=0;
   if (s2interim) {
      for (type in s2interim) {
         s2inum = s2interim[type];
         s2int = s2_getinterim(type);
         if (s2int && s2int.Status == 'Delete') {
            s2_delstorage('S2Int'+s2inum);
            delete s2interim[type];
            s2imax = Math.max(s2imax,s2inum);
         }
      }
   }
   return (s2imax+1);
}

function s2_updateinterim(target)  {
   var s2interim = s2_getinterim();
   var s2int;
   var drawinterim = false;
   if (s2interim && s2interim[target]) {
      var s2inum;
      s2inum = s2interim[target];
      s2int = s2_getinterim(target);
      if (s2int.Status) {
         switch (s2int.Status) { 
         case 'Setting': {
            s2int.Status = 'Redraw';
            drawinterim = false;
            s2_setstorage('S2Int'+s2inum,s2int);
         }break;
         case 'Redraw': {
            s2int.Status = 'Delete';
            drawinterim = true;
            s2_setstorage('S2Int'+s2inum,s2int);
         }break;
         case 'Delete': {
            s2_unsetinterim(target);
            drawinterim = false;         
         }break
         }
      }      
   }
   return drawinterim;
}
function s2_unsetinterim(target) {
   if (!target) s2_delstorage('S2Interim');
   else {
      var s2interim = s2_getinterim();
      var s2inum = s2interim[target];
      if (s2inum) { 
         delete s2interim[target];
         s2_delstorage('S2Int'+s2inum);
      }
      s2_setstorage('S2Interim',s2interim);
   }
   return true;   
}
function s2_getinterim(target) {
   var s2interim = s2_getstorage('S2Interim');
   var s2return,type,s2int,s2inum;
   if (s2interim) {
      if (target) {
         s2inum = s2interim[target];
         if (s2inum) {
            s2int = s2_getstorage('S2Int'+s2inum);
         }
         s2int = (s2int)?s2int:new Object();
      } else s2int = s2interim;
      return s2int;
   } else return new Object(); 
}


function s2_showaddinterface(id) {
   var args = s2_disectid(id);
   var id = 'Form-'+args.Form+'_Interface';
   var ifaceobj = JSON.parse(document.getElementById(id).value);
   //s2_drawaddform(ifaceobj);
   if (ifaceobj.ListData) delete ifaceobj.ListData;
   var iface = JSON.stringify(ifaceobj);
   s2_draw(iface);
   //snc_report_json_obj(iface);
}
function s2_passwordmatches(cid) {
   var bid,pid,cval,pval,encval,matches;
   bid = cid.replace(/\_Confirm.+$/,'');
   pid = bid+'_Confirm-1';
   cval = document.getElementById(cid).value;
   pval = document.getElementById(pid).value;
   matches = (pval == cval);
   if(!matches) alert('The passwords do not match');
   else s2_passwordencrypt(pval,pid);  
   return matches;   
}
function s2_passwordencrypt(val,pid) {
   var bid = pid.replace(/\_Confirm.+$/,'');
   encval = getencrypted(val,'AB');
   document.getElementById(bid).value = encval;
}
var s2ystart;
function s2_stretch() {}
var mapname,mappoly; 
var datetarget;
function s2_today(target) {
   datetarget = target;
   var date = new Date();
   var currentday,currentmonth,currentyear;
   currentday     = date.getUTCDate();
   currentmonth   = date.getUTCMonth();
   currentyear    = date.getUTCFullYear();
   var datetext = ((currentday<10)?'0'+currentday:currentday) + '/' + ((currentmonth<9)?'0'+(currentmonth+1):(currentmonth+1)) + '/' + currentyear;   
   var dnode = document.getElementById(datetarget);  
   dnode.focus();
   dnode.value = datetext;
   dnode.blur();
   if (dnode.onchange) dnode.onchange();
}

function useDate() {
   var datetext = ((currentday<10)?'0'+currentday:currentday) + '/' + ((currentmonth<9)?'0'+(currentmonth+1):(currentmonth+1)) + '/' + currentyear;
   var dnode = document.getElementById(datetarget);  
   dnode.focus();
   dnode.value = datetext;
   dnode.blur();
   if (dnode.onchange) dnode.onchange();
   hidedialog(); 
}
function s2_showcalendardialog(target) {
   var dialog,cal,cont,cent;
   datetarget = target;
   cleardialog('dialog-liner');
   dialog = document.getElementById('dialog-liner');
   cent = document.createElement('CENTER');
   cal = document.createElement('DIV');
   cal.id = 'calendar';
   cal.className = 'calendar';
   cent.appendChild(cal);
   dialog.appendChild(cent);
   loadCalendar();
   //var close = document.createElement('BUTTON');
   //close.appendChild(document.createTextNode('close'));
   //close.onclick = hidedialog;
   //dialog.appendChild(close);
   showdialog();
}
function s2_drawheader(jobj) {
   var dialog,h2,text,button,input;
   dialog = document.getElementById('incontentdialog');
   h2 = document.createElement('H2');
   text = jobj.DisplayName;
   if (jobj.Name) text += ' : ' + jobj.Name 
   h2.appendChild(document.createTextNode(text));
   dialog.appendChild(h2);
   return true;
}
function s2_searchbylinks(jobj) {
   var node,a,id,span,prop,interfacetype,input,select,option,button,maxl;
   var parenttype;
   maxl = 20;
   var s2settings = s2_getcurrent(jobj.IType);
   node = document.createElement('DIV');
   node.className = 'searchbydiv';
   if (s2settings && s2settings.SearchBy) {
      span = document.createElement('SPAN');
      span.appendChild(document.createTextNode('Filter: '));
      node.appendChild(span);
      span = document.createElement('SPAN');
      span.appendChild(document.createTextNode(s2settings.SearchBy));
      span.style.marginLeft = '5px';
      span.style.marginRight = '5px';
      span.style.fontWeight = 'bold';
      node.appendChild(span);
      span = document.createElement('SPAN');
      span.appendChild(document.createTextNode(' = '));
      span.style.marginLeft = '5px';
      span.style.marginRight = '5px';
      node.appendChild(span);
      span = document.createElement('SPAN');
      prop = s2settings.SearchFor;
      prop = prop.replace(/\s*\,\s*/,' , ');
      if (prop.length > maxl) prop = prop.substring(0,(maxl-2))+'..'; 
      span.appendChild(document.createTextNode(prop));
      span.style.fontWeight = 'bold';
      node.appendChild(span);
      button = document.createElement('BUTTON');
      //button.innerHTML = '&times;';
      button.appendChild(document.createTextNode('\xD7'));
      button.className = 'lmtype';
      button.onclick = function() {s2_removefilter(jobj.IType);};
      node.appendChild(button);            
   } else {
      span = document.createElement('SPAN');
      span.appendChild(document.createTextNode('Search For'));
      span.style.marginRight = '5px';
      node.appendChild(span);
      input = document.createElement('INPUT');
      input.type = 'text';
      input.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Search-For'; 
      node.appendChild(input);
      
      select = document.createElement('SELECT');
      select.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Search-By';
      for(prop in jobj.Interface) {
         if (typeof jobj.Interface[prop] == 'object') interfacetype = jobj.Interface[prop].DataType;
         else interfacetype = jobj.Interface[prop].replace(/([^\:]+).*/,'$1');
         switch (interfacetype) {
         case 'Unique Key': break;
         case 'LINKEDTO': break;
         case 'LINKEDTOM': break;
         default: {
            text = prop.replace(/^[^_]+_/,'');
            text = text.replace(/_[^_]+$/,'');
            option = document.createElement('OPTION');
            option.appendChild(document.createTextNode(text));
            option.value = prop;
            select.appendChild(option);
         }break;
         }
      }
      span = document.createElement('SPAN');
      span.appendChild(document.createTextNode('In'));
      span.style.marginLeft = '5px';
      span.style.marginRight = '5px';
      node.appendChild(span);
      node.appendChild(select);
      
      a = document.createElement('A');
      a.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Search-Exec';
      a.href = window.location.search + '#';
      a.appendChild(document.createTextNode('Go'));
      a.onclick = function() {s2_searchby(this.id);};
      a.className = 'listnav';
      node.appendChild(a);
   }
   return node;
}
function s2_pagelinks(jobj) {
   var node,a,id,span;
   node = document.createElement('DIV');
   node.className = 'pagelinksdiv';
   if (jobj.Page == 1) {
      span = document.createElement('SPAN');
      span.appendChild(document.createTextNode('<'));
      span.style.color = '#999';
      span.className = 'listnav';
      node.appendChild(span);
   } else { 
      a = document.createElement('A');
      a.href = window.location.search + '#';
      a.appendChild(document.createTextNode('<'));
      a.onclick = function() {s2_setpage(jobj.IType,(jobj.Page-1));};
      a.className = 'listnav';
      node.appendChild(a);
   }
   span = document.createElement('SPAN');
   span.appendChild(document.createTextNode('Page '+jobj.Page+' of '+jobj.Pages));
   span.style.marginLeft = '5px';
   span.style.marginRight = '5px';
   node.appendChild(span);
   if (jobj.Page == jobj.Pages) {
      span = document.createElement('SPAN');
      span.appendChild(document.createTextNode('>'));
      span.style.color = '#999';
      span.className = 'listnav';
      node.appendChild(span);
   } else {
      a = document.createElement('A');
      a.href = window.location.search + '#';
      a.appendChild(document.createTextNode('>'));
      a.onclick = function() {s2_setpage(jobj.IType,(jobj.Page+1));};
      a.className = 'listnav';
      node.appendChild(a);
   }
   return node;
}
function s2_removefilter(itype) {
   var s2settings = s2_getcurrent();
   if (s2settings[itype]) {
      var itypeobj = s2settings[itype]; 
      delete itypeobj.SearchBy;
      delete itypeobj.SearchFor
      s2settings[itype] = itypeobj; 
      s2_setstorage('S2Settings',s2settings);
      s2_refresh();
   }
}
function s2_drawlisttable(jobj,parent) {
   forms++;
   var dialog,i,j,indent,tabs,extratab,prop,index,type,button,title;
   var table,thead,tbody,tr,th,td,node,a,id,lid,span,h2,nodeid,prefix;
   var interfacetype;
   dialog = document.getElementById('incontentdialog');
   if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
   if (jobj.Page) {
      node = s2_pagelinks(jobj);
      dialog.appendChild(node);
   }
   node = s2_searchbylinks(jobj);
   dialog.appendChild(node);
   h2 = document.createElement('H2');
   //prefix = (jobj.IsAllowedTo.View)?'Select ':null;
   title = (jobj.IsAllowedTo.View)?'Select '+jobj.DisplayName:jobj.DisplayName;
   h2.appendChild(document.createTextNode(title));
   dialog.appendChild(h2);
   table = document.createElement('TABLE');
   table.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Table';
   table.className = 'listform';
   thead = document.createElement('THEAD');
   tr = document.createElement('TR');
   if (jobj.Versions && jobj.Versions.Column) {
      th = document.createElement('TH');
      th.appendChild(document.createTextNode('Current'));
      tr.appendChild(th);
   } 
   for(prop in jobj.Interface) {
      if (!jobj.Interface[prop].Hidden) {
         th = s2_getnode(null,prop,jobj.Interface[prop],null,'ListHeader');
         if (th) tr.appendChild(th);
      }
   }
   if (jobj.IsAllowedTo.View) {
      th = document.createElement('TH');
      th.className = 's2_optionsnode';
      if (jobj.IsAllowedTo.Add) {
         button = document.createElement('BUTTON');
         button.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Action-AddInterface';
         button.onclick = function() {s2_showaddinterface(this.id);};
         button.appendChild(document.createTextNode('Add'));
         th.appendChild(button);
      }
      if (jobj.IsAllowedTo.Import) {
         button = document.createElement('BUTTON');
         button.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Action-ImportInterface';
         button.onclick = function() {s2_showimportinterface(this.id);};
         button.appendChild(document.createTextNode('Import'));
         th.appendChild(button);
      }
      tr.appendChild(th);
   }   
   thead.appendChild(tr);
   
   table.appendChild(thead);
   tbody = document.createElement('TBODY');
   for(index in jobj.ListData) {
      var listitem = jobj.ListData[index];
      tr = document.createElement('TR');
      if (jobj.Versions && jobj.Versions.Column) {
         td = gen_getlistversionnode(jobj,listitem);                   
         tr.appendChild(td);
      }
      var id=null,name=null,interfacetype;
      for(prop in jobj.Interface) {
         if (jobj.Interface[prop] == 'Unique Key') id = listitem[prop];
         if (!jobj.Interface[prop].Hidden) {
            nodeid = 'Form-'+forms+'_IType-'+jobj.IType+'_Property-'+prop+'_Current-'+((id)?id:index);          
            td = s2_getnode(nodeid,prop,jobj.Interface[prop],listitem[prop],'List');
            if (td) {
               if (td.className == 'currentversion' || td.className == 'autodata') tr.className = td.className;
               tr.appendChild(td);
            }
         }
      }
      td = gen_getlistoptionsnode(jobj,listitem);
      if (td) tr.appendChild(td);
      tbody.appendChild(tr);
   }
   table.appendChild(tbody);
   //table.onload = function() {table_split(this.id);};
   dialog.appendChild(table);
   
   if (jobj.IsAllowedTo.View) gen_appendchooseselected(jobj,dialog,forms);
   if (jobj.Versions && jobj.Versions.Current) { 
      var vid = 'Form-'+forms+'_IType-'+jobj.IType+'_Action-SetVersion_Version-'+jobj.Versions.Current;
      var vnode = document.getElementById(vid); 
      if (vnode) vnode.checked = 'checked';
   }
   /*
   // add extender button
   ext = s2_getextenderbutton(jobj,'edit');
   if (ext) dialog.appendChild(ext);
   */
   gen_appendhiddenfields(jobj,dialog,forms);
   
   return true;
}
function s2_drawlinks(jobj) {
   forms++;
   var dialog,i,j,indent,tabs,extratab,prop,index,type,button,title;
   var node,a,id,lid,lname,span,h2,nodeid,prefix,ol,ul,li;
   var interfacetype;
   dialog = document.getElementById('incontentdialog');
   if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
   //if (jobj.IsAllowedTo.View) {
      ol = document.createElement('OL');
      for(index in jobj.ListData) {
         var listitem = jobj.ListData[index];
         li = document.createElement('LI');
         a = document.createElement('A');
         lname = (listitem['Name'])?('View Report: '+listitem['Name'].replace(/\_/g,' ').replace(/\.html/,'')):'View Report';
         a.appendChild(document.createTextNode(lname));
         a.href = listitem.URL; 
         a.target = 'S2Reports';
         li.appendChild(a);
         
         if (jobj.IsAllowedTo.Delete) {
            a = document.createElement('A');
            a.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Action-Delete_Current-'+listitem.Current;
            a.href = window.location.search + '#';
            a.style.marginLeft = '20px';
            a.appendChild(document.createTextNode('Delete'));
            a.onclick = function() {
               if (s2_confirmdelete()) s2_itypeselect(this.id);
            };
            li.appendChild(a);
         }
         
         ol.appendChild(li);         
      }
      dialog.appendChild(ol);
   //}
   return true;
}
function s2_drawviews(jobj) {
   var ext;
   if (jobj.Current) {
      var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input;
      var div,node,text,id,lid,action,viewname;
      dialog = document.getElementById('incontentdialog');
      div = document.createElement('DIV');
      div.className = 'viewmenu';
      div.id = 'Menu_IType-'+jobj.IType;
      for(i in jobj.Views) {
         viewname = jobj.Views[i];
         if (jobj.ViewName == viewname) {
            node = document.createElement('SPAN');
            node.className = 'viewmenulink';
            node.appendChild(document.createTextNode(viewname));
         } else { 
            //alert(viewname);
            node = document.createElement('A');
            node.href = window.location.search + '#';
            node.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Action-View_Current-'+jobj.Current+'_ViewName-'+viewname;
            node.className = 'viewmenulink';
            node.style.marginRight = '5px';
            node.appendChild(document.createTextNode(viewname));
            node.onclick = function() {s2_itypeselect(this.id);};
         }
         div.appendChild(node);
         div.appendChild(document.createTextNode(' '));
      }
      ext = s2_getextenderbutton(jobj,'view');
      if (ext) div.appendChild(ext);            
      dialog.appendChild(div);
   } 
   return true;
}  

function s2_drawalternatives(jobj) {
   //if (!jobj.Current) {
      var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input;
      var div,node,text,id,lid,action,viewname,classname;
      dialog = document.getElementById('incontentdialog');
      div = document.createElement('DIV');
      div.className = 'viewmenu';
      if (jobj.DisplayName && (jobj.Current || !jobj.ListData)) {
         node = document.createElement('A');
         node.href = window.location.search + '#';
         node.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Action-List';
         node.style.marginRight = '10px';
         node.appendChild(document.createTextNode(jobj.DisplayName));
         node.onclick = function() {s2_itypeselect(this.id);};
         div.appendChild(node);
      }
      for(viewname in jobj.Alternatives) {
         classname = jobj.Alternatives[viewname];
         //alert(viewname);
         node = document.createElement('A');
         node.href = window.location.search + '#';
         node.id = 'Form-'+forms+'_IType-'+classname+'_Action-List';
         node.style.marginRight = '10px';
         node.appendChild(document.createTextNode(viewname));
         node.onclick = function() {s2_itypeselect(this.id);};
         div.appendChild(node);
      }
      dialog.appendChild(div);
   //} 
   return true;
}  

function s2_drawform(jobj) {
   //cleardialog('dialog-liner');
   forms++;
   var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input;
   var field,fieldname,fielddisplay,fieldtype,nodeid,link;
   var table,thead,tbody,tr,th,td,node,text,id,lid,action,ul,li;
   action = jobj.Form.Action;
   dialog = document.getElementById('incontentdialog');
   if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
   if (!jobj.Data || jobj.Interim == true) {
      var h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode(jobj.Form.Title));
      dialog.appendChild(h2);
   } 
   table = document.createElement('TABLE');
   table.id = action+'_'+jobj.Form.IType;
   table.className = 'addform';
   tbody = document.createElement('TBODY');
   for(prop in jobj.Form.Fields) {
      field = jobj.Form.Fields[prop];
      fieldname = field.Name;
      fielddisplay = field.DisplayName;
      params = field.Params;
      fieldtype = field.Params.DataType;
      
      tr = document.createElement('TR')
      th = document.createElement('TH');
      th.className = 'left';
      th.appendChild(document.createTextNode(fielddisplay));
      tr.appendChild(th);
      nodeid = 'Form-'+forms+'_IType-'+jobj.Form.IType+'_Property-'+fieldname;
      var data = s2_getqsparams(fieldname);
      td = s2_getnode(nodeid,fieldname,params,data,'View');
      if (td) {
         if (td.className == 'currentversion' || td.className == 'autodata') tr.className = td.className;
         tr.appendChild(td);
      }
      tbody.appendChild(tr);
   }
   table.appendChild(tbody);
   dialog.appendChild(table);
   if (jobj.Form.Links) {
      ul = document.createElement('UL');
      for (prop in jobj.Form.Links) {
         link = jobj.Form.Links[prop];
         li = document.createElement('LI');
         a = document.createElement('A');
         a.className = 'ListEntry';
         a.appendChild(document.createTextNode(link.DisplayName));
         a.href = (link.URL.match(/^http\:\/+/))?link.URL:'../'+link.URL;
         if (link.Target) a.target = link.Target;
         li.appendChild(a);         
         ul.appendChild(li);         
      }      
      dialog.appendChild(ul);
   }
   button = document.createElement('BUTTON');
   button.onclick = function() {s2_submitform(this.id);};
   button.id = 'Form-'+forms+'_Action-'+action;
   button.appendChild(document.createTextNode(jobj.Form.Title));
   dialog.appendChild(button);   
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_IType';
   node.type = 'hidden';
   node.value = jobj.Form.IType;
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_ViewIType';
   node.type = 'hidden';
   node.value = jobj.Form.IType;
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_Action';
   node.type = 'hidden';
   node.value = action;
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_FormInterface';
   node.type = 'hidden';
   node.value = JSON.stringify(jobj.Form);
   dialog.appendChild(node);
   //showdialog();
   return true;
}

function s2_drawaddform(jobj) {
   //cleardialog('dialog-liner');
   forms++;
   var dialog,container,i,j,indent,tabs,extratab,prop,propname,type,button,input,datatype;
   var table,thead,tbody,tr,th,td,node,text,id,lid,action,data,params,uploaders,cols,includecol;
   var interfacetype,
   action = (jobj.Current || (jobj.Data && !jobj.Interim))?'Edit':'Add';
   dialog = document.getElementById('incontentdialog');
   if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
   if (!jobj.Data || jobj.Interim == true) {
      var h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode(action+' '+jobj.DisplayName));
      dialog.appendChild(h2);
   } 
   table = document.createElement('TABLE');
   table.id = action+'_'+jobj.IType;
   table.className = 'addform';
   tbody = document.createElement('TBODY');
   
   for(prop in jobj.Interface) {
      id = 'Form-'+forms+'_IType-'+jobj.IType+'_Property-'+prop;
      params = jobj.Interface[prop];
      datatype = (typeof params == 'object')?params.DataType:params.replace(/:.+/,'');
      
      includecol = (!jobj.Columns || s2_inarray(prop,jobj.Columns));
      if (typeof params != 'object' || !(params.Hidden || params.NoView)) {
      
         if (!jobj.Data && jobj.Versions && prop == jobj.Versions.Column) data = (jobj.Versions.Maximum)?(Math.floor((jobj.Versions.Maximum + 0.1)*10)/10):1;
         else if (!jobj.Data && datatype == 'Count') data = (jobj.Count)?parseInt(jobj.Count)+1:1;       
         else data = (jobj.Data && jobj.Data[prop])?jobj.Data[prop]:null;
         
         td = s2_getnode(id,prop,params,data,'View');
         if (td && includecol) {
            tr = document.createElement('TR')
            th = document.createElement('TH');
            th.className = 'left';
            if (prop.match(/_ID$/)) {
               prop = prop.replace(/^[^_]+_/,'');
               prop = prop.replace(/_ID$/,'');
            }
            th.appendChild(document.createTextNode(prop.replace(/\_/g,' ')));
            tr.appendChild(th);
            tr.appendChild(td);
            tbody.appendChild(tr);
         }
      }
   }
   table.appendChild(tbody);
   dialog.appendChild(table);
   if (jobj.Current && jobj.IsAllowedTo && jobj.IsAllowedTo.Edit) {
      button = document.createElement('BUTTON');
      button.onclick = function() {s2_submitdialog(this.id);};
      button.id = 'Form-'+forms+'_Action-Edit_Current-'+jobj.Current;
      button.appendChild(document.createTextNode('Save'));
      dialog.appendChild(button);   
   } else if (jobj.IsAllowedTo && jobj.IsAllowedTo.Add) {
      button = document.createElement('BUTTON');
      button.onclick = function() {s2_submitdialog(this.id);};
      button.id = 'Form-'+forms+'_Action-Add';
      button.appendChild(document.createTextNode('Save'));
      dialog.appendChild(button);   
   }
   if (jobj.Current && jobj.IsAllowedTo && jobj.IsAllowedTo.Delete) {
      button = document.createElement('BUTTON');
      button.onclick = function() {
         if (s2_confirmdelete()) s2_submitdialog(this.id);
      };
      button.id = 'Form-'+forms+'_Action-Delete_Current-'+jobj.Current;
      button.appendChild(document.createTextNode('Delete'));
      button.style.marginLeft = '5px';
      dialog.appendChild(button);   
   }
   /*
   // add extender button
   ext = s2_getextenderbutton(jobj,'edit');
   if (ext) dialog.appendChild(ext);
   */
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_ViewIType';
   node.type = 'hidden';
   node.value = jobj.IType;
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_ActionIType';
   node.type = 'hidden';
   node.value = jobj.IType;
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_Action';
   node.type = 'hidden';
   node.value = action;
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_Interface';
   node.type = 'hidden';
   node.value = JSON.stringify(jobj);
   dialog.appendChild(node);
   if (jobj.ViewName) {
      node = document.createElement('INPUT');
      node.id = 'Form-'+forms+'_ViewName';
      node.type = 'hidden';
      node.value = jobj.ViewName;
      dialog.appendChild(node);
   }
   s2_derivefromlinks();
   s2_onloadnode();
   return true;
}

function s2_drawhiddenparent(jobj) {
   //cleardialog('dialog-liner');
   forms++;
   var dialog,container,i,j,indent,tabs,extratab,prop,propname,type,button,input,datatype;
   var table,thead,tbody,tr,th,td,node,text,id,lid,action,data,params,uploaders;
   var interfacetype;
   action = (jobj.Current || (jobj.Data && !jobj.Interim))?'Edit':'Add';
   dialog = document.getElementById('incontentdialog');
   container = document.createElement('DIV');
   container.className = 's2hiddenparent';
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_ViewIType';
   node.type = 'hidden';
   node.value = jobj.IType;
   container.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_ActionIType';
   node.type = 'hidden';
   node.value = jobj.IType;
   container.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_Action';
   node.type = 'hidden';
   node.value = action;
   container.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_Interface';
   node.type = 'hidden';
   node.value = JSON.stringify(jobj);
   container.appendChild(node);
   if (jobj.ViewName) {
      node = document.createElement('INPUT');
      node.id = 'Form-'+forms+'_ViewName';
      node.type = 'hidden';
      node.value = jobj.ViewName;
      container.appendChild(node);
   }
   dialog.appendChild(container);
   return true;
}

function s2_showlist(id,target) {
   var args = s2_disectid(id);
   var pars = new Object();
   pars.IType = args.IType;
   pars.Action = 'List';
   s2_query(pars);
   return true;
}

function s2_submitform(id) {
   hidedialog();
   var args = s2_disectid(id);
   var prefix = 'Form-'+args.Form+'_';
   var jobj = JSON.parse(document.getElementById(prefix+'FormInterface').value);
   var action = jobj.Action;
   var itype = jobj.IType;
   var parameters = new Object();
   parameters.IType = itype;
   parameters.Action = action;
   var field;
   for(prop in jobj.Fields) {
      field = jobj.Fields[prop];
      switch (field.Type) {
      case 'Unique Key': break;
      default: parameters[field.Name] = document.getElementById(prefix+'IType-'+itype+'_Property-'+field.Name).value;break;
      }
   }
   s2_query(parameters);
}
function s2_submitdialog(id) {
   hidedialog();
   var node,id;
   var args = s2_disectid(id);
   var prefix = 'Form-'+args.Form+'_';
   var itype = document.getElementById(prefix+'ActionIType').value;
   var action = (args.Action)?args.Action:document.getElementById(prefix+'Action').value;
   var jobj = JSON.parse(document.getElementById(prefix+'Interface').value);
   var parameters = new Object();
   parameters.IType = itype;
   parameters.Action = action;
   if (jobj.Current && action == 'Delete') s2_unsetcurrent(itype); 
   if (jobj.Current && jobj.Current > 0) parameters.Current = jobj.Current;
   var datatype;
   var alldataisvalid,propisvalid,invalidprops; 
   alldataisvalid = true;
   invalidprops = new Array();
   
   for(prop in jobj.Interface) {
      if (typeof jobj.Interface[prop] == 'object') datatype = jobj.Interface[prop].DataType;
      else datatype = jobj.Interface[prop].replace(/([^\:]+).*/,'$1');
      switch (datatype) {
      case 'Unique Key': break;
      case 'File Path': {
         //node = document.getElementById(prefix+'IType-'+itype+'_Property-'+prop); 
         //if (node) parameters[prop] = escape(node.value);
         id = prefix+'IType-'+itype+'_Property-'+prop;
         node = s2_getnode(id,prop,jobj.Interface[prop],null,'Submit');
         parameters[prop] = escape(node);          
      }break;
      case 'URL': {
         //node = document.getElementById(prefix+'IType-'+itype+'_Property-'+prop); 
         //if (node) parameters[prop] = escape(node.value);
         id = prefix+'IType-'+itype+'_Property-'+prop;
         node = s2_getnode(id,prop,jobj.Interface[prop],null,'Submit'); 
         parameters[prop] = escape(node);
      }break;
      default: {
         //node = document.getElementById(prefix+'IType-'+itype+'_Property-'+prop); 
         //if (node) parameters[prop] = node.value;
         id = prefix+'IType-'+itype+'_Property-'+prop;
         node = s2_getnode(id,prop,jobj.Interface[prop],null,'Submit'); 
         parameters[prop] = node;
      }break;
      }
      // data validation 
      propisvalid = s2_propertyvalueisvalid(prop,datatype,parameters[prop]);
      if (!propisvalid) {
         invalidprops.push(prop + ' = ' + parameters[prop]);
         //alert(prop + ' ' + parameters[prop]);
      } 
   }
   alldataisvalid = (invalidprops.length == 0);
   if (alldataisvalid) s2_query(parameters);
   else alert('Invalid entries:\n' + invalidprops.join('\n')); 
}

function s2_propertyvalueisvalid(property,datatype,value) {
   // assume it's fine. 
   var isvalid = true;
   // it's fine if it's null (add mandatory test)
   if (value) {
   // otherwise test based on datatype.
      switch(datatype) {
      case 'Short Text':      {isvalid = (value.length <= 100);}break;
      case 'Medium Text':     {isvalid = (value.length <= 200);}break;
      case 'Long Text':       {isvalid = (value.length <= Math.pow(2,16));}break;
      case 'Password':        {isvalid = (value.length <= 100);}break;
      case 'Encrypted Text':  {isvalid = (value.length <= Math.pow(2,16));}break;
      case 'Number':          {isvalid = (value.match(/^\d+$/));}break;
      case 'Percentage':      {isvalid = (value.match(/^[\d,\.]+$/));}break;
      case 'Count':           {isvalid = (value.match(/^\d+$/));}break;
      case 'Decimal':         {isvalid = (value.match(/^[\d,\.]+$/));}break;
      case 'Version':         {isvalid = (value.match(/^[\d,\.]+$/));}break;
      case 'True or False':   {isvalid = ((value == 0)||(value == 1));}break;
      case 'Date':            {isvalud = (value.match(/^\d{2}\/\d{2}\/\d{4}$/));}break;
      case 'Time':            {isvalid = (value.match(/^[\d,\:]+$/));}break;
      //case 'Date and Time':   {isvalid = (value.match(/^\d{2}\/\d{2}\/\d{4}\s[\d,\:]+$/));}break;
      //case 'Map Data':        {isvalid = (value.match(/^POLYGON\(\([\d,\.\,]+\)\)$/));}break;
      //case 'Taxa':            {/* Taxa */}break;
      case 'URL':             {isvalid = (value.length <= 500);}break;
      case 'Email':           {isvalid = (value.length <= 100);}break;
      case 'File Path':       {isvalid = (value.length <= 500);}break;
      //case 'File':            {/* File */}break;
      }
   }
   return isvalid; 
}

function s2_setversion(id) {
   var args = s2_disectid(id);
   var prefix = 'Form-'+args.Form+'_';
   var jobj = JSON.parse(document.getElementById(prefix+'Interface').value);
   var parameters = new Object();
   parameters.Action = args.Action;
   if (jobj.Current && jobj.Current > 0) parameters.Current = jobj.Current;
   var bits,dtype,classname,column;
   for(prop in jobj.Interface) {
      dtype = jobj.Interface[prop].DataType;
      //bits = jobj.Interface[prop].split(/\:/);
      //dtype = bits[0];
      switch (dtype) {
      case 'Version': {
         //parameters.IType = bits[1];
         parameters.IType = jobj.Interface[prop].VersionOfType; 
         parameters.VersionOf = args.IType; 
         parameters.Version = args.Version;
      }break;
      }
   }
   s2_query(parameters);   
}
function cleardialog(dlinerid) {
   var dliner = document.getElementById(dlinerid);
   while(dliner.lastChild.id != 'dialog-title') {
      dliner.removeChild(dliner.lastChild);
   }
   return true;
}
function snc_send(r) {   
   inprogress();   
   r = cdm_wrapper(r);
   var ac = new AjaxClass();
   ac.url = r.target;
   ac.parameters = r.request;
   if (r.responder) ac.responder = r.responder;
   if (r.method) ac.Method = r.method;
   if (r.sync) ac.Async = !r.sync;
   ac.init();
   var resp = ac.send();
   if (r.sync) return resp;
}

function enableinterface() {
   var settings = s2_getstorage('Settings');
   //if (!settings) {
      var json = new Object();
      if (settings) json.Settings = settings;
      var action = new Object();
      action.Task = 'startsession';
      json.Action = action;
      snc_executeTask(json,true);
   //}             
}

function snc_executeTask(json) {
   var server = s2_getstorage('cdm_server');
   var r = new Object();
   r.target = 'http://'+server+'/snc_sqlitetasks2.php';
   var parameters = new Object();
   parameters['json'] = json;
   r.request = parameters;
   r.responder = snc_actionStatus;
   snc_send(r);    
}
function snc_actionStatus(json) {
   var json_obj = JSON.parse(json);
   if (json_obj.Settings) {
      var settings = json_obj.Settings;
      s2_setstorage('Settings',settings);
   }
   /*
   if (json_obj.Status) {
      alert(json_obj.Status.Message);
   }
   */
   if (json_obj.Choices) {
      var newchoices = json_obj.Choices;
      var choices = s2_getstorage('Choices');
      if (!choices) choices = new Object();
      for (prop in newchoices) choices[prop] = newchoices[prop];
      s2_setstorage('Choices',choices);
   }
   if (json_obj.Action) snc_actionDialog(json_obj.Action);
   if (json_obj.Options) snc_report_json_obj(json);
   s2_query();
   completed();
}
function snc_clearMenus(menuid) {
   var menudiv = document.getElementById(menuid);
   while(menudiv.childNodes.length > 0) {
      menudiv.removeChild(menudiv.lastChild);
   }
   return true;
}
function snc_get_json_html(json) {
   var pre = document.createElement('PRE');
   indent = 0;
   tabs = '';
   for (i=0;i<json.length;i++) {
      switch(json.substr(i,1)) {
      case ',': {
         tabs = '\n';
         for(j=0;j<indent;j++) tabs = tabs + '\t';          
         json = json.substr(0,i+1) + tabs + json.substr(i+1);
         i += tabs.length;      
      }break;
      case '{': {
         indent++;
         tabs = '\n';
         for(j=0;j<indent;j++) tabs = tabs + '\t';          
         json = json.substr(0,i+1) + tabs + json.substr(i+1);
         i += tabs.length;         
      }break;
      case '[': {
         indent++;
         tabs = '\n';
         for(j=0;j<indent;j++) tabs = tabs + '\t';          
         json = json.substr(0,i+1) + tabs + json.substr(i+1);
         i += tabs.length;
      }break;
      case '}': {
         if (json.substr(i+1,1) == ',') {
            indent--;
            tabs = '\n';
            for(j=0;j<indent;j++) tabs = tabs + '\t';          
            json = json.substr(0,i) + tabs + json.substr(i,2) + tabs + json.substr(i+2);
            i += tabs.length * 2;
         } else {
            indent-=2;
            tabs = '\n';
            extratab = (indent >= 0)?'\t':''; 
            for(j=0;j<indent;j++) tabs = tabs + '\t';
            json = json.substr(0,i) + tabs + extratab + json.substr(i,1) + tabs + json.substr(i+1);
            i += (tabs.length + 1) * 2; // extra 2 for the extra \t tab
         }      
      }break;
      case ']': {
         if (json.substr(i+1,1) == ',') {
            indent--;
            tabs = '\n';
            for(j=0;j<indent;j++) tabs = tabs + '\t';          
            json = json.substr(0,i) + tabs + json.substr(i,2) + tabs + json.substr(i+2);
            i += tabs.length * 2;
         } else {
            indent-=2;
            tabs = '\n';
            extratab = (indent >= 0)?'\t':''; 
            for(j=0;j<indent;j++) tabs = tabs + '\t';
            json = json.substr(0,i) + tabs + extratab + json.substr(i,1) + tabs + json.substr(i+1);
            i += (tabs.length + 1) * 2; // extra 2 for the extra \t tab
         }
      }break;
      }      
   }   
   pre.appendChild(document.createTextNode(json));
   pre.id = 'json_response';
   return pre;
}

function snc_report_json_obj(json) {
/*
   debugging code to print the returned json readably formatted to the screen     
   for some reason the last close bracket indent doesn't get removed.
*/
   var dialog,i,j,indent,tabs,extratab;
   //snc_clearMenus('incontentdialog');   
   dialog = document.getElementById('incontentdialog');
   var pre = snc_get_json_html(json);   
   dialog.appendChild(pre);      
   return true;
}

/*
   Interfaces provide the interaction between separate databases or separate 
   domains in S2. 
   
   To allow users to specify an existing contacts database rather than the 
   integrated S2 contacts database, S2 separates contacts into a separate 
   domain. 
   
   Data from an external source or from another domain is stored as the JSON 
   necessary to retreive the data from that domain with a mandatory name and 
   value element allowing the data to be rendered on the interface. 
   
   So if your site has an owner then you need to define an interface column 
   for the owner with the Domain "contacts" and the DataType "Contact" 
   when prompted to enter this information the user will be linked to the 
   contacts domain to retrieve the information which will be returned as JSON 
   and populated into the interface as the "Name" property.
   
   The big question is whether to post the request or to just store it in a 
   cookie. POST would be the obvious choice except that it isn't available to 
   javascript which would mean writing the post data back into the server 
   generated HTML which is ugly. GET is messy looking and old fashioned so 
   cookies it is.
   
   if you're using encryption you can encrypt the json into the cookie.
   can use get to set the datatype to be looked up from the cookie?        
*/
function s2_ifaceselect(json,noedit) {
   var params = JSON.parse(json);
   if (typeof params == 'object') {
      var s2int = new S2DomainInterface();
      var todom = s2_getcurrentdomain();
      if (!noedit) s2_updateinterim(params.IType);
      else s2_unsetinterim(params.IType);
      s2int.Init(params.From_Type,params.In_Domain,todom,params.ValidTypes,params.Display_Name);
      s2int.Edit = !noedit;
      if (params.Name) s2int.Set(params.Data_Type,params.Name,params.Value);
      s2int.Save();
      s2int.Send();
   }
}
function s2_ifaceclear(element) {
   var jsonnode,linknode;
   linknode = element.previousSibling;
   jsonnode = element.nextSibling;
   jsonnode.value = '';
   
   element.parentNode.removeChild(linknode);
   element.parentNode.removeChild(element);
}
function S2DomainInterface() {
// cookie name  = 'S2Interface';
   this.Display_Name = null;
   this.In_Domain = null;
   this.To_Domain = null
   this.Data_Type = null;
   this.ValidTypes = null;
   this.From_Type = null;
   this.Edit = true;
   this.Name = null;
   this.Value = null;
   this.Draw = false;
   this.Init = function(fromtype,indomain,todomain,types,display) {
      this.From_Type = fromtype;
      this.In_Domain = indomain;
      this.To_Domain = todomain;
      this.ValidTypes = types;
      this.Display_Name = display;
   }
   this.Set = function(type,name,value) {
      this.Data_Type = type;
      this.Name = name;
      this.Value = value;
   };
   this.Save = function() {
      var json = JSON.stringify(this);          
      s2_setstorage('S2Interface',json);
      s2_savestorage();
   }                
   this.Send = function() {
      var path = s2_getcurrentpath();
      path = path.replace(/\/[^\/]+$/,'/'+this.In_Domain);
      
      vtypes = this.ValidTypes;
      if (!this.Value) {
         var txt = 'Select one of: ';
         for(i in vtypes) {
            vtypes[i] = vtypes[i].replace(/^[^\_]+\_/,'');
            vtypes[i] = vtypes[i].replace(/\_/, ' ');
         }
         txt += vtypes.join(', ');
         txt += '\nThen press the "'+this.Display_Name+'" button in the menu bar';
         alert(txt);
      }
      window.location.href = path;      
   };
   this.AddButton = function () {
      var dom,menu,mnuid,btnid,button,img,txt;
      mnuid = 's2menubar';
      btnid = 's2_interfacebutton'; 
      dom = s2_getcurrentdomain();      
      if (this.In_Domain == dom) {
         menu = document.getElementById(mnuid);
         button = document.getElementById(btnid);
         if (!button) {
            button = document.createElement('BUTTON');
            button.id = btnid;
            txt = (this.Display_Name)?'Set '+this.Display_Name:'Use selected';
            button.name = 'Returns the current value into the previous form.';
            s2_addtooltip(button);
            button.appendChild(document.createTextNode(txt));
            button.onclick = function() {
               var s2int = new S2DomainInterface();
               s2int.Return();
            };
            menu.appendChild(button);
         } 
      }   
   };
   this.IsValidType = function(type) {
      var cobj,i,vtype,isvalid;
      isvalid = false;
      for(i in this.ValidTypes) {
         if (type == this.ValidTypes[i]) isvalid = true;         
      }
      return isvalid;
   }
   this.Return = function () {
      this.Read();
      var cobj,i,type;
      var types = this.ValidTypes;
      type = document.getElementById('Form-1_ActionIType').value;
      cobj = s2_getcurrent(type);
      if (cobj) {
         var path = s2_getcurrentpath();
         this.Data_Type = cobj.IType;
         this.Name = cobj.Name;
         this.Value = cobj.Current;
         this.Save();
         path = path.replace(/\/[^\/]+$/,'/'+this.To_Domain);
         window.location.href = path;
      } else {
         type = types.pop();
         type = types.join(", ") + " or " + type;         
         alert('No '+type+' is currently selected.');
      }
   };
   this.Delete = function() {
      s2_delstorage('S2Interface');
   };
   this.Read = function () {
      var thisdom,dom,prop,params;
      thisdom = false;
      var json = s2_getstorage('S2Interface');
      if (json) {
         s2int = JSON.parse(json);
         dom = s2_getcurrentdomain();
         thisdom = (s2int.In_Domain == dom);       
         for(prop in s2int) {
            this[prop] = s2int[prop];   
         }
      }
      return thisdom;             
   };
   this.Load = function () {
      var thisdom = this.Read();
      var drawn = false;
      if (this.From_Type) {
         var s2obj;
         var s2obj = s2_getinterim(this.From_Type);
         if (this.To_Domain == s2_getcurrentdomain() && s2obj) {
            //s2obj = s2obj[this.From_Type];
            for (prop in s2obj.Data) {
               val = s2obj.Data[prop];
               if (val && typeof val == 'object' && val.IsInterim) {
                  s2obj.Data[prop] = this;
                  s2obj.Interim = true;
               }
            }
            var json = JSON.stringify(s2obj); 
            s2_draw(json);
            s2_unsetinterim(s2obj.IType);
            drawn = true;
            this.Delete();         
         } else if (thisdom) {
            if(this.Edit) this.AddButton();
            if(this.Value && this.Data_Type && !this.Draw) {
               params = 'IType-'+this.Data_Type+'_Action-View_Current-'+this.Value;
               s2_itypeselect(params);
               drawn = true;
            } 
         }
      }
      return drawn;       
   };
}
function s2_intenablereturnbutton() {
   var i,vtypes,txt,buttontxt;
   var s2int = new S2DomainInterface();
   var thisdom = s2int.Read();
   if (thisdom) {
      var itype = document.getElementById('Form-1_ActionIType').value;
      var setting = s2_getcurrent(itype);
      var current = (setting)?setting.Current:false;
      var button = document.getElementById('s2_interfacebutton');
      var valid = s2int.IsValidType(itype);
      var enabled = false;
      if (button) {
         buttontxt = button.firstChild.nodeValue;
         enabled = (itype && current && valid);
         button.disabled = (enabled)?null:'disabled';
         button.className = (enabled)?'enabled':'disabled';   
      } 
   }     
}
function s2_setupdomaininterface() {
   var s2int = new S2DomainInterface();
   var drawn = s2int.Load();
   if (s2int.In_Domain != s2_getcurrentdomain() && !s2int.Edit) drawn = false;
   return drawn;   
}
function s2_redirect_with_post(path, params, method) {
   method = method || "post"; // Set method to post by default, if not specified.
   // The rest of this code assumes you are not using a library.
   // It can be made less wordy if you use one.
   var form = document.createElement("form");
   form.setAttribute("method", method);
   form.setAttribute("action", path);

   for(var key in params) {
      var hiddenField = document.createElement("input");
      hiddenField.setAttribute("type", "hidden");
      hiddenField.setAttribute("name", key);
      hiddenField.setAttribute("value", params[key]);
      form.appendChild(hiddenField);
   }
   //document.body.appendChild(form);    // Not entirely sure if this is necessary
   form.submit();
}

function s2_getextcredentials(auth,source) {
   var sid = s2_getcookie('PHPSESSID');
   var prop;
   var req = new Object();
   var req = new Object();
   req.target = s2_getcurrentpath()+'/S2_EDS.php';
   var params = new Object();
   params.UInfo = s2_getcookie('u_info');
   params.Authority = auth;
   params.Source = source;
   req.request = params;
   req.etype = 'AB';
   req.sid = sid;
   req.sync = true;
   var res = snc_send(req);
   var dec = cdm_decrypt(res,'AB');
   //var dec = res;
   var obj = JSON.parse(dec); 
   completed();
   return obj;
}

function gen_getdialog() {
   var dialog = document.getElementById('incontentdialog');
   return dialog;
}

var s2derivefrom = new Array();
function s2_derivefromlinks() {
   var nodeid,derivedata;
   for(nodeid in s2derivefrom) {
      node = document.getElementById(nodeid);
      derivedata = JSON.parse(s2derivefrom[nodeid]);
      s2_derive(node,derivedata);
      delete s2derivefrom[nodeid];
   }
   return true;
}
var s2onload = new Array();
function s2_onloadnode() {
   var nodeid,dtype;
   for (nodeid in s2onload) {
      node = document.getElementById(nodeid);
      dtype = s2onload[nodeid];
      switch(dtype) {
      case 'Long Text': s2_resizelongtext(node,true);break
      }
      delete s2onload[nodeid];
   }
   return true;
}
function s2_getnode(nodeid,name,params,data,headerlistviewsubmit) {
   var itype,dtype,node,gcd;
   if (data && typeof data == 'string') data = htmlentities(data);
//alert(JSON.stringify(params));
   if (typeof params == 'object') dtype = params.DataType;
   else dtype = params.replace(/\:.+/,'');
   switch(dtype) {
   case 'Long Text':       gcd = new Gen_LongTextColumnData(nodeid,name,params,data);break;
   case 'Password':        gcd = new Gen_PasswordColumnData(nodeid,name,params,data);break;
   case 'Encrypted Text':  gcd = new Gen_EncryptedColumnData(nodeid,name,params,data);break;
   case 'Number':          gcd = new Gen_NumberColumnData(nodeid,name,params,data);break;
   case 'Percentage':      gcd = new Gen_PercentageColumnData(nodeid,name,params,data);break;
   case 'Count':           gcd = new Gen_CountColumnData(nodeid,name,params,data);break;
   case 'Decimal':         gcd = new Gen_DecimalColumnData(nodeid,name,params,data);break;
   case 'Version':         gcd = new Gen_VersionColumnData(nodeid,name,params,data);break;
   case 'True or False':   gcd = new Gen_LogicalColumnData(nodeid,name,params,data);break;
   case 'Date':            gcd = new Gen_DateColumnData(nodeid,name,params,data);break;
   case 'Time':            gcd = new Gen_TimeColumnData(nodeid,name,params,data);break;
   case 'Date and Time':   gcd = new Gen_TimestampColumnData(nodeid,name,params,data);break;
   case 'Map Data':        gcd = new Gen_MapColumnData(nodeid,name,params,data);break;
   case 'Taxa':            gcd = new Gen_TaxaColumnData(nodeid,name,params,data);break;
   case 'URL':             gcd = new Gen_URLColumnData(nodeid,name,params,data);break;
   case 'Email':           gcd = new Gen_EmailColumnData(nodeid,name,params,data);break;
   case 'File Path':       gcd = new Gen_FilePathColumnData(nodeid,name,params,data);break;
   case 'File':            gcd = new Gen_FileColumnData(nodeid,name,params,data);break;
   case 'LINKEDTO':        gcd = new Gen_LTColumnData(nodeid,name,params,data);break;
   case 'LINKEDTOM':       gcd = new Gen_LTMColumnData(nodeid,name,params,data);break;
   case 'INTERFACE':       gcd = new Gen_InterfaceColumnData(nodeid,name,params,data);break;
   default:                gcd = new Gen_ColumnData(nodeid,name,params,data);break;
   }
   switch(headerlistviewsubmit) {
   case 'ListHeader':   node = gcd.getListHeaderNode();break;
   case 'List':         node = gcd.getListDataNode();break;
   case 'View':         node = gcd.getDataNode();break;
   case 'Submit':       node = gcd.getSubmissionData();break;
   case 'Interim':      node = gcd.getInterim();break; 
   }
   if (node && !node.className) node.className = dtype.replace(/\s+/g,'_');
   if (nodeid && dtype == 'LINKEDTO' && params.Derives) s2derivefrom[nodeid] = JSON.stringify(params.Derives);
   if (nodeid && dtype == 'Long Text') s2onload[nodeid] = dtype; 
   return node;
}
function gen_getlistversionnode(jobj,listitem) {
   var td,node,c;
   c = false;
   td = document.createElement('TD');
   node = document.createElement('INPUT');
   node.type = 'radio';
   node.name = 'V';
   node.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Action-SetVersion_Version-'+listitem[jobj.Versions.Column];
   node.value = listitem[jobj.Versions.Column]; 
   if (listitem[jobj.Versions.Column] == jobj.Versions.Current) {
      node.checked = 'checked';
      //c = true;
      td.className = 'currentversion';
   } else {
      td.className = 'autodata';
   }
   node.onclick = function() {s2_setversion(this.id);}; 
   td.appendChild(node);
   //if (c) node.checked = 'checked';
   return td;
}
function gen_getlistoptionsnode(jobj,listitem) {
   var td,input,button,form,prop,id,a,type;
   for (prop in jobj.Interface) {
      if (jobj.Interface[prop] == 'Unique Key') id = listitem[prop]; 
   }
   form = forms;
   td = null;
   if (jobj.IsAllowedTo.View) {
      td = document.createElement('TD');
      td.className = 's2_optionsnode';
      if (jobj.IsAllowedTo.View) {
         input = document.createElement('INPUT');
         input.id = 'Form-'+form+'_IType-'+jobj.IType+'_Action-Tick_Current-'+id;
         input.className = 'tick';
         input.type = 'checkbox';
         td.appendChild(input);
         a = document.createElement('A');
         a.id = 'Form-'+form+'_IType-'+jobj.IType+'_Action-View_Current-'+id;
         a.href = window.location.search + '#';
         a.appendChild(document.createTextNode('Select')); // View | Select
         a.onclick = function() {s2_itypeselect(this.id);};
         td.appendChild(a);
      }
      if (jobj.IsAllowedTo.Delete) {
         a = document.createElement('A');
         a.id = 'Form-'+form+'_IType-'+jobj.IType+'_Action-Delete_Current-'+id;
         a.href = window.location.search + '#';
         a.style.marginLeft = '5px';
         a.appendChild(document.createTextNode('Delete'));
         a.onclick = function() {
            if (s2_confirmdelete()) s2_itypeselect(this.id);
         };
         td.appendChild(document.createTextNode(' '));
         td.appendChild(a);
      }
   }
   return td;
}
function gen_appendhiddenfields(jobj) {
   var node,form,dialog;
   form = forms;
   dialog = gen_getdialog();
   node = document.createElement('INPUT');
   node.id = 'Form-'+form+'_ViewIType';
   node.type = 'hidden';
   node.value = jobj.IType;
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+form+'_ActionIType';
   node.type = 'hidden';
   node.value = jobj.IType;
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+form+'_Action';
   node.type = 'hidden';
   node.value = 'Add';
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+form+'_Interface';
   node.type = 'hidden';
   node.value = JSON.stringify(jobj);
   dialog.appendChild(node);
   return true;
}
function gen_appendchooseselected(jobj) {
   var button,form,dialog;
   form = forms;
   dialog = gen_getdialog();   
   button = document.createElement('BUTTON');
   button.id = 'Form-'+form+'_IType-'+jobj.IType+'_Action-SelectAll';
   button.appendChild(document.createTextNode('Ticked'));
   button.onclick = function() {s2_itypeselectall(this.id);};
   dialog.appendChild(button);
   return true;
}
function gen_def_getinterim() {
   var node,form;
   node = document.getElementById(this.NodeID); 
   if (node) return node.value;
   else return null;            
}
function gen_def_getlistheadernode() {
   var pk,th,text;
   if (typeof this.Params == 'object') pk = (this.Params.DataType == 'Unique Key');
   else pk = (this.Params.replace(/\:.+/,'') == 'Unique Key');
   if (!pk && !this.Params.NoList) {
      text = this.Name;
      if (text.match(/_ID$/)) {
         text = text.replace(/^[^_]+_/,'');         
         text = text.replace(/_ID$/,'');
      }         
      text = text.replace(/\_/g,' ')         
      th = document.createElement('TH');
      th.appendChild(document.createTextNode(text));
   }
   return th;
}
function gen_def_getlistdatanode() {
   var td,text,pk;
   // IE writes null to the screen if the input value is set to null
   if (this.Data == null) this.Data = ''; 
   if (typeof this.Params == 'object') pk = (this.Params.DataType == 'Unique Key');
   else pk = (this.Params.replace(/\:.+/,'') == 'Unique Key');
   if (!pk && !this.Params.NoList) {
      td = document.createElement('TD');
      text = this.Data;
      if (text && text.length > 50) {
         text = text.substring(0,46)+'...';
      }
      td.appendChild(document.createTextNode(text));
   }
   return td;
}
function gen_def_getdatanode() {
   var td,node,width,pk,opt,oi;
   // IE writes null to the screen if the input value is set to null
   if (this.Data == null) this.Data = ''; 
   if (typeof this.Params == 'object') pk = (this.Params.DataType == 'Unique Key');
   else pk = (this.Params.replace(/\:.+/,'') == 'Unique Key');
   if (!pk) {
      td = document.createElement('TD');
      if(!this.Params.ReadOnly) {
         if (this.Params.Options) { 
            node = document.createElement('SELECT');
            for (oi in this.Params.Options) {
               opt = document.createElement('OPTION');
               opt.value = this.Params.Options[oi];
               opt.appendChild(document.createTextNode(this.Params.Options[oi]));
               if (this.Data && this.Data == this.Params.Options[oi]) opt.selected = 'selected';
               node.appendChild(opt); 
            }
         } else {
            node = document.createElement('INPUT');
            node.type = 'text';
            if (this.Data) node.value = this.Data;
         }
         node.id = this.NodeID;
         node.name = this.NodeID;
         if (this.Params && this.Params.Derives) {
            var derives = this.Params.Derives;
            node.onchange = function() {s2_derive(this,derives);};
         }
         td.appendChild(node);
      } else {
         node = document.createTextNode(this.Data);
         td.className = 'autodata';
         td.appendChild(node);
         node = document.createElement('INPUT');
         node.type = 'hidden';
         node.id = this.NodeID;
         node.name = this.NodeID;
         if (this.Data) node.value = this.Data;
         if (this.Params && this.Params.Derives) {
            var derives = this.Params.Derives;
            node.onchange = function() {s2_derive(this,derives);};
         }
         td.appendChild(node);
      }
   }
   return td;
}
function gen_def_getsubmissiondata() {
   var node = document.getElementById(this.NodeID);
   if (node) return node.value;
   else return null;
}
function s2_confirmdelete() {
   var text,confirmed;
   text = "Are you sure?\n"+
   "Deleting will delete not only this entry\n"+
   "but all entries in other tables which depend upon it.";
   confirmed = confirm(text);
   return confirmed; 
}
function s2_editlink(button) {
   var node = button.previousSibling;
   var link = node.previousSibling;
   var parent = link.parentNode;
   var text = link.firstChild.nodeValue;
   node.type = 'text';
   node.onblur = function() {s2_createlink(this);};
   parent.removeChild(link);
   parent.removeChild(button);
   return true;   
}
function s2_createlink(node) {
   var text = node.value;
   var parent = node.parentNode;
   var link = document.createElement('A');
   var button = document.createElement('BUTTON');
   var href;
   if (text.match(/@/) && !text.match(/^mailto:/)) href = 'mailto:'+text;
   else if (!text.match(/^http:\/+/)) text = 'http://'+href;
   else href = text;
   link.href = href;
   link.appendChild(document.createTextNode(text));
   link.className = 's2_txtlink';
   link.style.marginRight = '10px';
   parent.insertBefore(link,node);
   button.onclick = function() {s2_editlink(this);};
   button.appendChild(document.createTextNode('Edit'));
   parent.appendChild(button);      
   node.type = 'hidden';
   node.value = text;
   return true;      
}
function s2_sendpassword(id) {
   var params = s2_disectid(id);
   var user = s2_getcurrent(params.IType);
   var ui;
   for (ui in user) params[ui] = user[ui];
   s2_query(params);
}                
function Gen_ColumnData(id,name,params,data) {
/*
   Type definitions
   INTERFACE:contacts:S2_Person,S2_Organisation
   LINKEDTO:Table.Column
   LINKEDTOM:Table.Column
   
   Appends 
      :Current
      :Mandatory
*/
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_ColumnData.prototype.getInterim = gen_def_getinterim;
Gen_ColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_ColumnData.prototype.getListDataNode = gen_def_getlistdatanode;
Gen_ColumnData.prototype.getDataNode = gen_def_getdatanode;
Gen_ColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_LongTextColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_LongTextColumnData.prototype.getInterim = gen_def_getinterim;
Gen_LongTextColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_LongTextColumnData.prototype.getListDataNode = gen_def_getlistdatanode;
Gen_LongTextColumnData.prototype.getDataNode = function() {
   var td,width,div,node;
   td = document.createElement('TD');
   // IE writes null to the screen if the input value is set to null
   if (this.Data == null) this.Data = ''; 
   if (!this.Params.ReadOnly) {
      node = document.createElement('TEXTAREA');
      node.onblur = function() {s2_resizelongtext(this,true);};
      node.onchange = function() {s2_resizelongtext(this,true);};
      node.onfocus = function() {s2_resizelongtext(this,true);}; 
      node.id = this.NodeID;
      node.name = this.NodeID;
      node.value = this.Data
      td.appendChild(node);
   } else {
      node = document.createTextNode(this.Data);
      td.className = 'autodata';
      td.appendChild(node);
      node = document.createElement('input');
      node.type = 'hidden';
      node.id = this.NodeID;
      node.name = this.NodeID;
      node.value = this.Data
      td.appendChild(node);
   }
   return td;
}
function s2_resizelongtext(textarea,auto) {
   if (textarea) {
      textarea.style.height = 'auto';
      textarea.style.height = (textarea.scrollHeight + 20) + 'px';
   } 
   return true;
}  

Gen_LongTextColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_PasswordColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_PasswordColumnData.prototype.getInterim = gen_def_getinterim;
Gen_PasswordColumnData.prototype.getListHeaderNode = function() {
   /* can user see passwords if not they shouldn't be sent */
   var th,text;
   if (!this.Params.NoList) {
      th = document.createElement('TH');
      text = this.Name;
      th.appendChild(document.createTextNode(text));
   }
   return th;
}
Gen_PasswordColumnData.prototype.getListDataNode = function() {
   var node,div,button,img,td;
   // IE writes null to the screen if the input value is set to null
   if (!this.Params.NoList) {
      if (this.Data == null) this.Data = ''; 
      var enc = this.Data;
      var clr = (enc)?cdm_decrypt(enc,'AB'):null;
      td = document.createElement('TD');
      div = document.createElement('DIV');
      var unconfirmed = !this.Params.Confirmed;
      node = document.createElement('INPUT');
      node.type = 'password';
      node.id = this.NodeID+'_Confirm-2';
   
      if (this.Data) node.value = clr;
      node.disabled = 'disabled';
      div.appendChild(node);
   
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.id = this.NodeID;
      node.name = this.NodeID;
      if (this.Data) node.value = enc;
      div.appendChild(node);
      td.appendChild(div);
   }
   return td;
}
Gen_PasswordColumnData.prototype.getDataNode = function() {
   var td,node,div,button,img;
   // IE writes null to the screen if the input value is set to null
   if (this.Data == null) this.Data = ''; 
   div = document.createElement('DIV');
   var unconfirmed = !this.Params.Confirmed;
   var enc = this.Data;
   var clr = (enc)?cdm_decrypt(enc,'AB'):null;
   if (unconfirmed) {
      node = document.createElement('INPUT');
      node.type = 'password';
      node.id = this.NodeID+'_Confirm-2';
      if (this.Data) node.value = clr;
      node.onchange = function() {s2_passwordencrypt(this.value,this.id);};
      div.appendChild(node);
   } else { 
      node = document.createElement('LABEL');
      node.appendChild(document.createTextNode('1:'));
      div.appendChild(node); 
      node = document.createElement('INPUT');
      node.type = 'password';
      node.id = this.NodeID+'_Confirm-1';
      node.value = clr;
      div.appendChild(node);
      div.appendChild(document.createElement('BR'));
      node = document.createElement('LABEL');
      node.appendChild(document.createTextNode('2:'));
      div.appendChild(node); 
      node = document.createElement('INPUT');
      node.type = 'password';
      node.id = this.NodeID+'_Confirm-2';
      if (this.Data) node.value = clr;
      node.onchange = function() {s2_passwordmatches(this.id);};
      div.appendChild(node); 
   }
   node = document.createElement('INPUT');
   node.type = 'hidden';
   node.id = this.NodeID;
   node.name = this.NodeID;
   if (this.Data) node.value = enc;
   div.appendChild(node);
   if (this.Data) {
      node = document.createElement('BUTTON');
      node.className = 's2iconbutton';
      node.appendChild(document.createTextNode('Send'));
      node.id = 'IType-'+this.Args.IType+'_Action-SendPassword';
      node.onclick = function() {s2_sendpassword(this.id);};
      div.appendChild(node);
   }
   td = document.createElement('TD');
   td.appendChild(div);
   return td;
}
Gen_PasswordColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_EncryptedColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_EncryptedColumnData.prototype.getInterim = gen_def_getinterim;
Gen_EncryptedColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_EncryptedColumnData.prototype.getListDataNode = gen_def_getlistdatanode;
Gen_EncryptedColumnData.prototype.getDataNode = gen_def_getdatanode;
Gen_EncryptedColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_NumberColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_NumberColumnData.prototype.getInterim = gen_def_getinterim;
Gen_NumberColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_NumberColumnData.prototype.getListDataNode = gen_def_getlistdatanode;
Gen_NumberColumnData.prototype.getDataNode = gen_def_getdatanode;
Gen_NumberColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_PercentageColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_PercentageColumnData.prototype.getInterim = gen_def_getinterim;
Gen_PercentageColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_PercentageColumnData.prototype.getListDataNode = function() {
   var td,node,width;
   node = document.createElement('INPUT');
   node.type = 'text';
   node.id = this.NodeID;
   node.name = this.NodeID;
   node.value = Math.floor(this.Data*100)+'%';
   node.disabled = 'disabled';
   td = document.createElement('TD');
   td.className = 'autodata';
   td.appendChild(node);
   return td;
}

Gen_PercentageColumnData.prototype.getDataNode = function() {
   var td,node,width;
   node = document.createElement('INPUT');
   node.type = 'text';
   node.id = this.NodeID;
   node.name = this.NodeID;
   node.value = Math.floor(this.Data*100)+'%';
   node.disabled = 'disabled';
   td = document.createElement('TD');
   td.className = 'autodata';
   td.appendChild(node);
   return td;
}
Gen_CountColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;


function Gen_CountColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_CountColumnData.prototype.getInterim = gen_def_getinterim;
Gen_CountColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_CountColumnData.prototype.getListDataNode = function() {
   var td,node,width;
   node = document.createElement('INPUT');
   node.type = 'text';
   node.id = this.NodeID;
   node.name = this.NodeID;
   node.value = this.Data;
   node.disabled = 'disabled';
   td = document.createElement('TD');
   td.className = 'autodata';
   td.appendChild(node);
   return td;
}

Gen_CountColumnData.prototype.getDataNode = function() {
   var td,node,width;
   node = document.createElement('INPUT');
   node.type = 'text';
   node.id = this.NodeID;
   node.name = this.NodeID;
   node.value = this.Data;
   node.disabled = 'disabled';
   td = document.createElement('TD');
   td.className = 'autodata';
   td.appendChild(node);
   return td;
}
Gen_CountColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_DecimalColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_DecimalColumnData.prototype.getInterim = gen_def_getinterim;
Gen_DecimalColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_DecimalColumnData.prototype.getListDataNode = gen_def_getlistdatanode;
Gen_DecimalColumnData.prototype.getDataNode = gen_def_getdatanode;
Gen_DecimalColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_VersionColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_VersionColumnData.prototype.getInterim = gen_def_getinterim;
Gen_VersionColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_VersionColumnData.prototype.getListDataNode = function() {
   var td;
   td = document.createElement('TD');
   td.className = 'autodata';
   if (typeof this.Data == 'object') td.appendChild(document.createTextNode(this.Data.Name)); 
   else td.appendChild(document.createTextNode(this.Data));
   return td;
}
Gen_VersionColumnData.prototype.getDataNode = function() {
   var td,node,width,vval;
   node = document.createElement('INPUT');
   node.type = 'text';
   node.id = this.NodeID;
   node.name = this.NodeID;
   vval = (this.Data)?this.Data:1.0; 
   node.value = vval;
   node.disabled = 'disabled';
   td = document.createElement('TD');
   td.className = 'autodata';
   td.appendChild(node);
   return td;
}
Gen_VersionColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_LogicalColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_LogicalColumnData.prototype.getInterim = gen_def_getinterim;
Gen_LogicalColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_LogicalColumnData.prototype.getListDataNode = function () {
   var td,text,pk;
   if (!this.Params.NoList) {
      td = document.createElement('TD');
      switch(parseInt(this.Data)) {
      case 1: text='True';break;
      default: text='False';break;
      }
      td.appendChild(document.createTextNode(text));
   }
   return td;
};

Gen_LogicalColumnData.prototype.getDataNode = function () {
   var td,node,width,pk,option,options,io;
   var options = new Array('False','True');
   /*
   switch (typeof this.Data) {
   case 'string': this.Data = (this.Data == 'true')?1:0; break;
   case 'boolean': this.Data = (this.Data)?1:0; break;
   }
   */
   td = document.createElement('TD');
   if (!this.Params.ReadOnly) {
      node = document.createElement('SELECT');
      node.id = this.NodeID;
      node.name = this.NodeID;
      for(io in options) {
         option = document.createElement('OPTION');
         option.appendChild(document.createTextNode(options[io]));
         option.value = io;
         if (io == this.Data) option.selected = 'selected';
         node.appendChild(option);
      }
      td.appendChild(node);
   } else {
      node = document.createTextNode(options[this.Data]);
      td.className = 'autodata';
      td.appendChild(node);
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.id = this.NodeID;
      node.name = this.NodeID;
      if (this.Data) node.value = this.Data;
      td.appendChild(node);
   }
   return td;
};

Gen_LogicalColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_DateColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}                                 
Gen_DateColumnData.prototype.getInterim = gen_def_getinterim;
Gen_DateColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_DateColumnData.prototype.getListDataNode = function() {
   var node,div,button,img,td;
   // IE writes null to the screen if the input value is set to null
   if (this.Data == null) this.Data = '';
   if (!this.Params.NoList) { 
      td = document.createElement('TD');
      div = document.createElement('DIV');
      node = document.createElement('INPUT');
      node.type = 'text';
      node.id = this.NodeID;
      node.name = this.NodeID;
      node.value = this.Data;
      node.style.width = '100px';
      node.style.border = 'none';
      node.style.height = '38px'
      node.style.verticalAlign = 'top';
      node.disabled = 'disabled';
      div.appendChild(node);
      td.appendChild(div);
   }
   return td;
}
Gen_DateColumnData.prototype.getDataNode = function() {
   var td,node,div,button,img;
   // IE writes null to the screen if the input value is set to null
   if (this.Data == null) this.Data = ''; 
   div = document.createElement('DIV'); 
   node = document.createElement('INPUT');
   node.type = 'text';
   node.id = this.NodeID;
   node.name = this.Name;
   node.value = this.Data;
   if (this.Params && this.Params.Derives) {
      var derives = this.Params.Derives;
      node.onchange = function() {s2_derive(this,derives);};
   } 
   node.style.width = '100px';
   node.style.border = 'none';
   node.style.height = '38px'
   node.style.verticalAlign = 'top';
   div.appendChild(node);
   button = document.createElement('BUTTON');
   button.onclick = function() { s2_showcalendardialog(this.previousSibling.id);};
   button.name = 'Calendar';
   s2_addtooltip(button);
   img = new Image;
   img.src = '../images/dateicon.php?dm=m';
   img.alt = 'cal';
   button.appendChild(img);
   button.className = 's2iconbutton';
   div.appendChild(button);
   button = document.createElement('BUTTON');
   button.onclick = function() {s2_today(this.previousSibling.previousSibling.id);};
   button.name = 'Today';
   s2_addtooltip(button);
   img = new Image;
   img.src = '../images/dateicon.php?dm=d';
   img.alt = 'tdy';
   button.appendChild(img);
   button.className = 's2iconbutton';
   div.appendChild(button);
   td = document.createElement('TD');
   td.appendChild(div);
   return td;
}
Gen_DateColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_TimeColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_TimeColumnData.prototype.getInterim = gen_def_getinterim;
Gen_TimeColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_TimeColumnData.prototype.getListDataNode = gen_def_getlistdatanode;
Gen_TimeColumnData.prototype.getDataNode = gen_def_getdatanode;
Gen_TimeColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_TimestampColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_TimestampColumnData.prototype.getInterim = gen_def_getinterim;
Gen_TimestampColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_TimestampColumnData.prototype.getListDataNode = gen_def_getlistdatanode;
Gen_TimestampColumnData.prototype.getDataNode = gen_def_getdatanode;
Gen_TimestampColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_MapColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_MapColumnData.prototype.getInterim = gen_def_getinterim;
Gen_MapColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_MapColumnData.prototype.getListDataNode = function() {
   var td,node;
   var poly = s2_wkt_to_gmappoly(this.Data);
   var name = s2_poly_getname(poly); 
   var propname = this.NodeID; 
   td = document.createElement('TD');
   node = document.createElement('A');
   node.id = 'Link_'+propname;
   node.href = window.location.search + '#';
   node.onclick = function() {s2_drawmap(this.id);return false;}; 
   node.appendChild(document.createTextNode(name));
   td.appendChild(node);
   node = document.createElement('INPUT');
   node.type = 'hidden';
   node.id = propname
   node.value = this.Data;
   td.appendChild(node);
   return td;
}
Gen_MapColumnData.prototype.getDataNode = function() {
   var td,node,div,button,img;
   td = document.createElement('TD');
   if (this.Data) {
      var poly = s2_wkt_to_gmappoly(this.Data);
      var name = s2_poly_getname(poly); 
      var propname = this.NodeID;
      node = document.createElement('A');
      node.id = 'Link_'+propname
      node.style.marginLeft = '5px';
      node.href = window.location.search + '#';
      node.onclick = function() {s2_drawmap(this.id);return false;}; 
      node.appendChild(document.createTextNode(name));
      td.appendChild(node);
      node = document.createElement('INPUT');
      // changed from hidden input to text input with display:none
      node.type = 'hidden';
      /*
      node.type = 'text';
      node.style.display = 'none';
      */
      node.id = propname
      node.value = this.Data;
      if (this.Params && this.Params.Derives) {
         var derives = this.Params.Derives;
         node.onchange = function() {s2_derive(this,derives);};
      }
      td.appendChild(node);
   } else {
      div = document.createElement('DIV'); 
      node = document.createElement('INPUT');
      // changed from hidden input to text input with display:none
      node.type = 'hidden';
      /*
      node.type = 'text';
      node.style.display = 'none';
      */
      node.id = this.NodeID;
      node.name = this.Name;
      if (this.Params && this.Params.Derives) {
         var derives = this.Params.Derives;
         node.onchange = function() {s2_derive(this,derives);};
      }
      div.appendChild(node);
      node = document.createElement('INPUT');
      node.type = 'text';
      node.id = this.NodeID+'_Name';
      node.name = this.Name+'_name';
      node.style.width = '170px';
      node.style.border = 'none';
      div.appendChild(node);
      button = document.createElement('BUTTON');
      button.onclick = function() {
         s2_drawmap(this.id);
         return false;
      };
      button.appendChild(document.createTextNode('Map'));
      button.className = 's2iconbutton';
      button.id = 'MAP_'+this.NodeID;
      div.appendChild(button);
      td.appendChild(div);
   }
   return td;
}
Gen_MapColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_URLColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_URLColumnData.prototype.getInterim = gen_def_getinterim;
Gen_URLColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_URLColumnData.prototype.getListDataNode = function() {
   var td,node;
   td = document.createElement('TD');
   node = document.createElement('A');
   node.href = this.Data;
   node.target = 'S2ExternalLink';  
   node.appendChild(document.createTextNode(this.Data));
   //node.appendChild(document.createTextNode('[ link ]'));
   td.appendChild(node);
   return td;
}
Gen_URLColumnData.prototype.getDataNode = gen_def_getdatanode;
Gen_URLColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_EmailColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_EmailColumnData.prototype.getInterim = gen_def_getinterim;
Gen_EmailColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_EmailColumnData.prototype.getListDataNode = gen_def_getlistdatanode;
Gen_EmailColumnData.prototype.getDataNode = function() {
   var td,node,width,pk,a,div;
   if(this.Data) {                                         
      div = document.createElement('DIV');
      node = document.createElement('A');
      node.href = 'mailto:'+this.Data;
      node.className = 's2_txtlink';
      node.style.marginRight = '10px';
      node.appendChild(document.createTextNode(this.Data));
      div.appendChild(node);
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.id = this.NodeID;
      node.name = this.NodeID;
      node.value = this.Data;
      div.appendChild(node);
      node = document.createElement('BUTTON');
      node.className = 's2iconbutton';
      node.onclick = function() {s2_editlink(this);};
      node.appendChild(document.createTextNode('Edit'));
      div.appendChild(node);
      td = document.createElement('TD');
      td.appendChild(div);         
   } else {
      node = document.createElement('INPUT');
      node.type = 'text';
      node.id = this.NodeID;
      node.name = this.NodeID;
      node.value = this.Data;
      td = document.createElement('TD');
      td.appendChild(node);
   }
   return td;
}
Gen_EmailColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_FilePathColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_FilePathColumnData.prototype.getInterim = gen_def_getinterim;
Gen_FilePathColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_FilePathColumnData.prototype.getListDataNode = function() {
   var td,width,div,node,p,input,cont,button,img,fid,fdata,isimg;
   if (!this.Params.NoList) {
      td = document.createElement('TD');
      if (this.Data) {
         fid = this.Data.replace(/.+\=/,'');
         if (fid) fdata = s2_getfiledetails(fid);
         isimg = s2_isimageurl((fid)?fdata.Name:this.Data);
         a = document.createElement('A');
         a.target = 's2doc';
         a.href = unescape(this.Data);
         a.style.marginRight = '10px';
         if (!isimg) {
            a.appendChild(document.createTextNode((fdata.Name)?fdata.Name:'Download File'));
         } else {
            img = new Image();
            img.src = this.Data;
            img.className = 's2_thumbnail';
            img.alt = fdata.Name;
            a.appendChild(img);   
         }
         td.appendChild(a);
         button = document.createElement('BUTTON');
         button.className = 's2iconbutton';
         button.id = this.NodeID+'_ShowHide';
         button.appendChild(document.createTextNode('Show'));
         button.onclick = function () {s2_showhidelink(this.id);};
         td.appendChild(button);  
      }
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.id = this.NodeID;
      node.value = this.Data;
      td.appendChild(node);
   } 
   return td;         
}
Gen_FilePathColumnData.prototype.getDataNode = function() {
   var td,width,div,node,p,input,cont,a,button,img,fid,fdata,isimg;
   td = document.createElement('TD');
   if (this.Data && this.Data != 'null') {
      fid = this.Data.replace(/.+\=/,'');
      if (fid) fdata = s2_getfiledetails(fid);
      isimg = s2_isimageurl((fid)?fdata.Name:this.Data);
      a = document.createElement('A');
      a.target = 's2doc';
      a.href = unescape(this.Data);
      a.style.marginRight = '10px';
      if (!isimg) {
         a.appendChild(document.createTextNode(fdata.Name));
      } else {
         img = new Image();
         img.src = this.Data;
         img.className = 's2_medsize';
         img.alt = fdata.Name;
         a.appendChild(img);   
      }
      td.appendChild(a);
      button = document.createElement('BUTTON');
      button.className = 's2iconbutton';
      button.id = this.NodeID+'_ShowHide';
      button.appendChild(document.createTextNode('Show'));
      button.onclick = function () {s2_showhidelink(this.id);};
      td.appendChild(button);    
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.id = this.NodeID;
      node.value = this.Data;
      td.appendChild(node);
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.id = this.NodeID+'_FileName';
      node.value = fdata.Name;
      td.appendChild(node); 
   } else {
      var inline = this.Params.Inline; 
      button = document.createElement('BUTTON');
      button.className = 's2iconbutton';
      button.id = this.NodeID+'_Choose';
      button.appendChild(document.createTextNode('Find'));
      button.onclick = function () {s2_showfiledialog(this,inline);};
      td.appendChild(button);    
   }
   return td;         
}
Gen_FilePathColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_FileColumnData() {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_FileColumnData.prototype.getInterim = gen_def_getinterim;
Gen_FileColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_FileColumnData.prototype.getListDataNode = gen_def_getlistdatanode;
Gen_FileColumnData.prototype.getDataNode = gen_def_getdatanode;
Gen_FileColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_LTColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_LTColumnData.prototype.getInterim = function() {
   var itype,obj;
   itype = this.Params.TargetType;
   obj = new Object;
   obj.IType = itype;
   id = document.getElementById('Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name).value;
   if (id && id > 0) {
      node = document.getElementById('Form-'+this.Args.Form+'_IType-'+itype+'_Action-View_Current-'+id);
      obj = new Object;
      obj.IType = itype;
      obj.Id = id;
      if (node) obj.Name = node.firstChild.nodeValue;
      return obj;
   } else return null;          
}
Gen_LTColumnData.prototype.getListHeaderNode = function() {
   var itype,th,prop;
   
   itype = this.Params.TargetType;
   var pnode,pobj,parent;
   pnode = document.getElementById('Form-1_Interface');
   if (pnode) {
      pobj = JSON.parse(pnode.value);
      parent = pobj.IType;
   }  
   var ismandatory = this.Params.Mandatory;
   var usecurrent = this.Params.Current;
   var setting;
   
   //if (this.Params.Show || (itype != this.Args.IType && (!(usecurrent || (itype == parent))))) {
   if (this.Params.Show || (itype != this.Args.IType && ((!usecurrent || !(itype == parent))))) {
      th = document.createElement('TH');
      prop = this.Name;
      if (prop.match(/_ID$/)) {
         prop = prop.replace(/^[^_]+_/i,'');
         prop = prop.replace(/_ID$/i,'');
      }
      prop = prop.replace(/\_/g,' ');
      th.appendChild(document.createTextNode(prop));
      return th;            
   } else return null;
}
Gen_LTColumnData.prototype.getListDataNode = function() {
   var node,td,text,lid;
   var itype,rules;
   itype = this.Params.TargetType;
   var pnode,pobj,parent;
   pnode = document.getElementById('Form-1_Interface');
   if (pnode) {
      pobj = JSON.parse(pnode.value);
      parent = pobj.IType;  
   }
      
   var ismandatory = this.Params.Mandatory;
   var usecurrent = this.Params.Current;
   var setting;
   var dialog = gen_getdialog();
   if (!(this.Params.Show || (itype != this.Args.IType && ((!usecurrent || !(itype == parent)))))) {
   //if (!this.Params.Show && itype == this.Args.IType) {
      node = document.createElement('INPUT');
      node.type = 'hidden';
      setting = s2_getcurrent(itype);      
      if (setting) node.value = setting.Current;
      node.id = this.NodeID;
      dialog.appendChild(node); 
   /*} else if (!this.Params.Show && (usecurrent ||(parent == itype))) {
      node = document.createElement('INPUT');
      node.type = 'hidden';
      setting = s2_getcurrent(itype);
      if (setting) node.value = setting.Current;
      node.id = this.NodeID;
      dialog.appendChild(node);*/
   } else {
      text = (typeof this.Data == 'object')?this.Data.Name:this.Data;
      lid = (typeof this.Data == 'object')?this.Data.Id:this.Data;
      td = document.createElement('TD');
      if (lid>0) {
         node = document.createElement('A');
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Params.TargetType+'_Action-View_Current-'+lid;
         node.href = window.location.search + '#';
         node.appendChild(document.createTextNode(text));
         node.onclick = function() {s2_itypeselect(this.id);};
         td.appendChild(node);
      } else {
         td.appendChild(document.createTextNode(''));
      }
   }
   return td;
}
   
Gen_LTColumnData.prototype.getDataNode = function() {
   var td,width;
   var node = document.createElement('INPUT');
   node.type = 'text';
   node.id = this.NodeID;
   node.name = this.NodeID;
   node.value = this.Data;
   var dialog = gen_getdialog();
   var itype;
   itype = this.Params.TargetType;
   var ismandatory = this.Params.Mandatory;
   var usecurrent = this.Params.Current;
   var setting = s2_getcurrent(itype);
   if (itype == this.Args.IType) {
      setting = s2_getcurrent(this.Args.IType);
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.value = setting.Current;
      node.id = this.NodeID;
      dialog.appendChild(node); 
   } else if (usecurrent && setting && setting.Current) {
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.value = setting.Current;
      node.id = this.NodeID;
      dialog.appendChild(node);
   } else if (this.Data && typeof this.Data == 'object' && this.Data.Id != 0) {
      
      td = document.createElement('TD');
      switch (this.Data.Id) {
      case 'Multiple': {
         var l,listdata;
         var ids = new Array();
         for(l in this.Data.ListData) {
            listdata = this.Data.ListData[l];
            text = listdata['Name'];
            lid = listdata[this.Data.Key];
            ids.push(lid);
            node = document.createElement('A');
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Params.TargetType+'_Action-View_Number-'+l+'_Current-'+lid;
            node.href = window.location.search + '#';
            node.style.marginRight = '10px';
            node.appendChild(document.createTextNode(text));
            node.onclick = function() {s2_itypeselect(this.id);};
            td.appendChild(node);
         }                  
         node = document.createElement('INPUT');
         node.type = 'hidden';
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name;
         node.value = JSON.stringify(ids);
         td.appendChild(node);
         
         node = document.createElement('BUTTON');
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Params.TargetType+'_Action-List_Property-'+this.Name;
         node.appendChild(document.createTextNode('Set'));
         node.className = 's2iconbutton';
         node.style.marginRight = '5px';
         node.onclick = function() {
            s2_saveinterim(this.id);
            s2_showlist(this.id);
         };
         td.appendChild(node);
      }break;
      default: {
         text = (typeof this.Data == 'object')?this.Data.Name:this.Data;
         lid = (typeof this.Data == 'object')?this.Data.Id:this.Data;
         node = document.createElement('A');
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Params.TargetType+'_Action-View_Current-'+lid;
         node.href = window.location.search + '#';
         node.style.marginRight = '10px';
         node.style.marginLeft = '4px';
         //node.innerHTML = text;
         node.appendChild(document.createTextNode(text));
         node.onclick = function() {s2_itypeselect(this.id);};
         td.appendChild(node);
         node = document.createElement('INPUT');
         node.type = 'hidden';
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Args.Property;
         node.name = text;
         node.value = lid;
         /*
         //FIGURE OUT HOW TO CALL DERIVES METHODS 
         if (this.Params.Derives) {
            var derives = this.Params.Derives;
            //node.onchange = function() {s2_derive(this,derives);};
            derived[node] = derives;
         }
         //*/
         td.appendChild(node);
         
         node = document.createElement('BUTTON');
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Params.TargetType+'_Action-List_Property-'+this.Name;
         node.appendChild(document.createTextNode('Set'));
         node.className = 's2iconbutton';
         node.onclick = function() {
            s2_saveinterim(this.id);
            s2_showlist(this.id);
         };
         td.appendChild(node);
      }break;
      }     
   } else {
      td = document.createElement('TD');
      node = document.createElement('BUTTON');
      node.id = 'Form-'+this.Args.Form+'_IType-'+this.Params.TargetType+'_Action-List_Property-'+this.Name;
      node.appendChild(document.createTextNode('Set'));
      node.className = 's2iconbutton';
      node.onclick = function() {
         s2_saveinterim(this.id);
         s2_showlist(this.id);
      };
      td.appendChild(node);
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name;
      node.name = this.Name;
      td.appendChild(node);
      
   }
   return td;
}
Gen_LTColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_LTMColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_LTMColumnData.prototype.getInterim = function() {
   var node,form;
   node = document.getElementById(this.NodeID);
//alert(this.Name + '\n' + node.value);
   if (node && node.value) return JSON.parse(node.value);
   else return null;            
          
}
Gen_LTMColumnData.prototype.getListHeaderNode = function() {
   return null;
}
Gen_LTMColumnData.prototype.getListDataNode = function() {
   return null;
}
Gen_LTMColumnData.prototype.getDataNode = function() {
   var node,text,lid,mobj;
   var itype,rules,li,nodes;
   itype = this.Params.TargetType;
   var ismandatory = this.Params.Mandatory;
   var usecurrent = this.Params.Current;
   var setting = s2_getcurrent(itype);
   
   td = document.createElement('TD');
   if (typeof this.Data == 'object') {
//alert('getDataNode\n' + JSON.stringify(this.Data));
      var ldata,lobj,ul,li,ids,lind;
      mobj = this.Data;
      ldata = new Array();
      if (mobj && mobj.ListData && mobj.ListData.length > 0) {
         ldata = mobj.ListData
      } else if (mobj && mobj.Id) {
         text = mobj.Name;
         lid = mobj.Id;   
         lobj = new Object();
         lobj.Name = text;
         lobj.Id = lid;
         ldata = new Array();
         ldata.push(lobj); 
      }
      ids = new Array();
      ul = document.createElement('UL');
      ul.className = 's2ul_intable'; 
      for(lind in ldata) {
         if (ldata[lind].Id > 0) {
            li = document.createElement('LI');
            li.className = 's2li_intable';
            text = ldata[lind].Name;
            lid = (ldata[lind].Id)?ldata[lind].Id:ldata[lind][mobj.Key];
            //ids.push(lid);
            node = document.createElement('A');
            node.id = 'Form-'+this.Args.Form+'_IType-'+itype+'_Action-View_Current-'+lid;
            node.href = window.location.search + '#';
            node.style.marginRight = '5px';
            //node.innerHTML = text;
            node.appendChild(document.createTextNode(text));
            node.onclick = function() {s2_itypeselect(this.id);};
            li.appendChild(node);
            button = document.createElement('BUTTON');
            button.className = 's2delbutton';
            //button.innerHTML = '&times;';
            button.appendChild(document.createTextNode('\xD7'));
            button.name = 'Remove '+text;
            button.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name+'_Action-Unset_Current-'+lid;
            button.onclick = function() {s2_removeltmdata(this);};
            li.appendChild(button);
            ul.appendChild(li);
         }
      }
      button = document.createElement('BUTTON');
      //button.innerHTML = 'Add';
      button.appendChild(document.createTextNode('Add'));
      button.className = 's2addbutton';
      button.name = 'Add data';
      button.id = 'Form-'+this.Args.Form+'_IType-'+itype+'_Property-'+this.Name+'_Action-List';
      button.onclick = function() {
         s2_saveinterim(this.id);
         s2_showlist(this.id);
      };
      td.appendChild(button);td.appendChild(ul);      
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name;
      node.name = this.Name;
      //node.value = JSON.stringify(ids);
      node.value = JSON.stringify(mobj);
      td.appendChild(node);
   }
   return td;
}
function s2_removeltmdata(node) {
   var id = node.id;
   var args = s2_disectid(id);
   var ul,li,ind,obj,ldata;
   li = node.parentNode;
   ul = li.parentNode;
   ul.removeChild(li);
   
   id = 'Form-'+args.Form+'_IType-'+args.IType+'_Property-'+args.Property;
   node = document.getElementById(id);
   
   obj = JSON.parse(node.value);
   ldata = obj.ListData;
   var newdata = new Array();
   for (ind in ldata) if (ldata[ind].Id!=parseInt(args.Current)) newdata.push(ldata[ind]);
   obj.ListData = newdata;
   node.value = JSON.stringify(obj);
   
   return true;    
}
Gen_LTMColumnData.prototype.getSubmissionData = function() {
   var node,obj,data,ind,val;
   node = document.getElementById(this.NodeID);
   obj = JSON.parse(node.value);

   if (obj) data = obj.ListData;
//alert('getSubmissionData\n' + JSON.stringify(data));
   if (data) {
      for (ind in data) {
         val = data[ind];
         data[ind] = (typeof val == 'object')?val.Id:val;    
      }
      data = JSON.stringify(data) 
   } else data = null
   return data;   
}

function Gen_InterfaceColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_InterfaceColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_InterfaceColumnData.prototype.getListDataNode = function() {
   var td,node;
   var text,json,obj;
   td = document.createElement('TD');
   if (this.Data) {
      if(typeof this.Data == 'object') {
         text = (this.Data.Name)?this.Data.Name:null;
         json = this.Data.JSON;
         if (text && json) {
            node = document.createElement('A');
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Args.Property+'_Current-'+this.Data.Value;
            node.href = window.location.search + '#';
            node.appendChild(document.createTextNode(text));
            node.onclick = function() {
               var args = s2_disectid(this.id);
               delete args.Action;
               var id = s2_buildid(args);
               var json = document.getElementById(id).value;
               s2_ifaceselect(json,true);
            };
            td.appendChild(node);
            node = document.createElement('INPUT');
            node.type = 'hidden';
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name+'_Current-'+this.Data.Value;
            node.value = json;
            td.appendChild(node);
         } /// ? should I add a set button here               
      } else if (typeof this.Data == 'string' && this.Data != '') {   
         obj = JSON.parse(this.Data);
         text = (obj.Name)?obj.Name:null;
         json = obj.JSON;
         if (text && json) {
            node = document.createElement('A');
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Args.Property+'_Current-'+obj.Value;
            node.href = window.location.search + '#';
            node.appendChild(document.createTextNode(text));
            node.onclick = function() {
               var args = s2_disectid(this.id);
               delete args.Action;
               var id = s2_buildid(args);
               var json = document.getElementById(id).value;
               s2_ifaceselect(json,true);
            };
            td.appendChild(node);
            node = document.createElement('INPUT');
            node.type = 'hidden';
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name+'_Current-'+obj.Value;
            node.value = json;
            td.appendChild(node);
         } /// ? should I add a set button here                                 
      }
   }
   return td;
}
Gen_InterfaceColumnData.prototype.getDataNode = function() {
   var td,node,div,button;
   var text,json,obj;
   td = document.createElement('TD');
   div = document.createElement('DIV');
   div.style.padding = '0 5px 0 5px';
   if (this.Data) {
      if (typeof this.Data == 'object') {
         text = (this.Data.Name)?this.Data.Name:null;
         json = JSON.stringify(this.Data);
         if (text && json) {
            node = document.createElement('A');
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Args.Property;
            node.href = window.location.search + '#';
            node.appendChild(document.createTextNode(text));
            node.onclick = function() {
               var args = s2_disectid(this.id);
               delete args.Action;
               var id = s2_buildid(args);
               var json = document.getElementById(id).value;
               s2_ifaceselect(json,true);
            };
            div.appendChild(node);
            button = document.createElement('BUTTON');
            button.className = 's2delbutton';
            //button.innerHTML = '&times;';
            button.appendChild(document.createTextNode('\xD7'));
            button.name = 'Remove '+text;
            button.onclick = function() {s2_ifaceclear(this);};
            div.appendChild(button);
            node = document.createElement('INPUT');
            node.type = 'hidden';
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name;
            node.value = json;
            div.appendChild(node);
         } /// ? should I add a set button here
         
         var s2int = new S2DomainInterface();
         s2int.Init(this.Args.IType,this.Params.Domain,s2_getcurrentdomain(),this.Params.ValidTypes,this.Name.replace(/_/,' '));
         json = JSON.stringify(s2int);
         node = document.createElement('BUTTON');
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-Set_Property-'+this.Name;
         node.appendChild(document.createTextNode('Set'));
         node.className = 's2iconbutton';
         node.onclick = function() {
            s2_saveinterim(this.id);
            s2_ifaceselect(json);
         };
         div.appendChild(node);
      } else if (typeof this.Data == 'string' && this.Data != '') {   
         obj = JSON.parse(this.Data);
         text = (obj.Name)?obj.Name:null;
         json = this.Data;
         if (text && json) {
            node = document.createElement('A');
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Name;
            node.href = window.location.search + '#';
            node.appendChild(document.createTextNode(text));
            node.onclick = function() {
               var args = s2_disectid(this.id);
               delete args.Action;
               var id = s2_buildid(args);
               var json = document.getElementById(id).value;
               s2_ifaceselect(json,true);
            };
            div.appendChild(node);
            node = document.createElement('INPUT');
            node.type = 'hidden';
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name;
            node.value = json;
            div.appendChild(node);
         } /// ? should I add a set button here                                 
      }
   } else {
      var s2int = new S2DomainInterface();
      
      s2int.Init(this.Args.IType,this.Params.Domain,s2_getcurrentdomain(),this.Params.ValidTypes,this.Name.replace(/_/,' '));
      json = JSON.stringify(s2int);
      node = document.createElement('BUTTON');
      node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-Set_Property-'+this.Name;
      node.appendChild(document.createTextNode('Set'));
      node.className = 's2iconbutton';
      node.onclick = function() {
         s2_saveinterim(this.id);
         s2_ifaceselect(json);
      };
      div.appendChild(node);
   }
   td.appendChild(div);
   return td;
}
Gen_InterfaceColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;
Gen_InterfaceColumnData.prototype.getInterim = function() {
   var node,form;
   node = document.getElementById(this.NodeID);
   if (node && node.value) return JSON.parse(node.value);
   else return null;                      
}

function Gen_TaxaColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_TaxaColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_TaxaColumnData.prototype.getListDataNode = function() {
   var td,node;
   var text,json,obj;
   td = document.createElement('TD');
   if (this.Data) {
      if (typeof this.Data == 'string' && this.Data != '') this.Data = JSON.parse(this.Data);
      switch (typeof this.Data) {
      case 'object': {
         text = (this.Data.Name)?this.Data.Name:null;
         json = this.Data.JSON;
         if (text && json) {
            node = document.createElement('A');
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Args.Property;
            node.href = window.location.search + '#';
            node.appendChild(document.createTextNode(text));
            node.onclick = function() {
               s2_shownbntaxadialog(this);
            };
            td.appendChild(node);
         } /// ? should I add a set button here               
      } break;
      case 'array': {
         var ti,tobj,sel,opt;
         for(ti in this.Data) {
            tobj = this.Data[ti];
            text = (tobj.Name)?tobj.Name:null;
            json = tobj.JSON;
            sel = document.createElement('SELECT');
            sel.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Args.Property;
            if (text && json) {
               opt = document.createElement('OPTION');
               opt.appendChild(document.createTextNode(text));
               opt.value = json;
               sel.appendChild(opt);
            }
            td.appendChild(sel);
         } /// ? should I add a set button here               
      } break;
      /*
      case 'string': {
         if (this.Data != '') {   
            obj = JSON.parse(this.Data);
            text = (obj.Name)?obj.Name:null;
            json = this.Data.JSON;
            if (text && json) {
               node = document.createElement('A');
               node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Args.Property;
               node.href = window.location.search + '#';
               node.appendChild(document.createTextNode(text));
               node.onclick = function() {
                  s2_shownbntaxadialog(this);
               };
               td.appendChild(node);
            }
         } /// ? should I add a set button here                                 
      } break;
      */
      }                                             
   }
   return td;
}
Gen_TaxaColumnData.prototype.getDataNode = function() {
   var td,node,div;
   var text,json,obj,lid;
   td = document.createElement('TD');
   div = document.createElement('DIV');
   div.style.padding = '0 5px 0 5px';
   if (this.Data) {
      if (typeof this.Data == 'object') {
         text = (this.Data.Name)?this.Data.Name:null;
         json = JSON.stringify(this.Data);
   //alert(json);
         if (text && json) {
            lid = this.Data.Value;
            node = document.createElement('A');
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Args.Property;
            node.href = window.location.search + '#';
            node.appendChild(document.createTextNode(text));
            node.onclick = function() {
               s2_shownbntaxadialog(this,this.firstChild.nodeValue);
            };
            div.appendChild(node);
            button = document.createElement('BUTTON');
            button.className = 's2delbutton';
            //button.innerHTML = '&times;';
            button.appendChild(document.createTextNode('\xD7'));
            button.name = 'Remove '+text;
            button.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name+'_Action-Unset_Current-'+lid;
            button.onclick = function() {s2_ifaceclear(this);};
            div.appendChild(button);
            node = document.createElement('INPUT');
            node.type = 'hidden';
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name;
            node.value = json;
            div.appendChild(node);
         } /// ? should I add a set button here
         node = document.createElement('BUTTON');
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-Set_Property-'+this.Name;
         node.appendChild(document.createTextNode('Set'));
         node.className = 's2iconbutton';
         node.onclick = function() {
            s2_shownbntaxadialog(this);
         };
         div.appendChild(node);
      } else if (typeof this.Data == 'string' && this.Data != '') {   
         obj = JSON.parse(this.Data);
         text = (obj.Name)?obj.Name:null;
         json = this.Data;
         if (text && json) {
            lid = this.Data.Value
            node = document.createElement('A');
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Name;
            node.href = window.location.search + '#';
            node.appendChild(document.createTextNode(text));
            node.onclick = function() {
               s2_shownbntaxadialog(this,this.firstChild.nodeValue);
            };
            div.appendChild(node);
            node = document.createElement('INPUT');
            node.type = 'hidden';
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name;
            node.value = json;
            div.appendChild(node);
         } /// ? should I add a set button here
         node = document.createElement('BUTTON');
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-Set_Property-'+this.Name;
         node.appendChild(document.createTextNode('Set'));
         node.className = 's2iconbutton';
         node.onclick = function() {
            s2_shownbntaxadialog(this);
         };
         div.appendChild(node);                                 
      }
   } else {
      node = document.createElement('BUTTON');
      node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-Set_Property-'+this.Name;
      node.appendChild(document.createTextNode('Set'));
      node.className = 's2iconbutton';
      node.onclick = function() {
         s2_shownbntaxadialog(this);
      };
      div.appendChild(node);
   }
   td.appendChild(div);
   return td;
}
Gen_TaxaColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;
Gen_TaxaColumnData.prototype.getInterim = function() {
   var node,form;
   node = document.getElementById(this.NodeID);
   if (node) return JSON.parse(node.value);
   else return null;            
          
}                                                                                                                                        