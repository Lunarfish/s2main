<?php
/*
   The Cache stores data definitions for cross-domain data. 
    
   The interface tables cross between domains and return a JSON definition 
   describing how to identify the linked data in the other domain. So a contact
   interface returns the Contact name, database id and the table from which the 
   data has been retrieved. 
   
   Likewise a taxa from the NHM dictionary NBN WebService returns the 
   taxon name as well as the TaxonVersionKey for that species.
   
   Each time in interface is populated the cache entry is entered or updated and                       
   the cache id is returned and stored in the interface column. 

   ALTERED PGSQL VERSION   
*/
class G_Cache extends G_Cache_Gen {
   function G_Cache() {
      parent::G_Cache_Gen();
   }
   function getcacheid() {
      $id = null;
      if (isset($this->data['Data_Type'])&&isset($this->data['Value'])) { 
         $query = $this->dbhandle->select();
         $query->from($this->tablename);
         $query->where('Data_Type = ?',$this->data['Data_Type']);
         $query->where('Value = ?',strval($this->data['Value']));
         $result = $this->dbhandle->fetchAll($query);
//print "<pre>"; print_r($result); print "</pre>";
         if (count($result)>0) $id = $result[0]['G_Cache_ID'];
      }
      return $id;
   }
   function insert() {
      // select where Data_Type and Value match if no match insert / else update
      $id = $this->getcacheid();
      $status = 0;
      if (isset($id)) {
         $this->update($this->getpk(),$id);
         $this->data[$pk] = $id;
         $this->id = $id;
         $status = $id;
      } else {
         if (!$this->validated) $this->validate();
         $this->dbhandle->insert($this->tablename, $this->data);
         $pk = $this->getpk();
         $id = $this->dbhandle->lastInsertId();
         if ($id) {
            $this->data[$pk] = $id;
            $this->id = $id;
            $status = $id;   
         }
      }
      return $status;            
   }         
}
?>
