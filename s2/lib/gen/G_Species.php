<?php
/* 
   GENERATE MetaData SCHEMA Based on UK GEMINI 2.1
*/

include_once('settings.conf');
include_once('snc/S2_DBT.php');

class G_Dataset extends DBT {
   protected $tablename = 'G_Dataset';
   protected $displayname = 'Species Dataset';
   protected $user;
   protected $alts = array('Records'=>'G_Record','Species Lists'=>'G_Species_List');
   protected $columns = array(
      'G_Dataset_ID'    => 'Unique Key',
      'Name'            => 'Short Text',
      'From_Date'       => 'Date',
      'To_Date'         => 'Date',
      'Coverage'        => 'Map Data',
      'Contact'         => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"]}'
   );
   protected $domain = 'species';
      protected $view = '[
      {
         "Name":"Records",
         "Data": [
            {
               "Table":"G_Record"
            }
         ]
      },
      {
         "Name":"MetaData",
         "Data": [
            {
               "Table":"G_Dataset_Description"
            },
            {
               "Table":"G_Dataset_File"
            }            
         ]
      }
   ]'; 
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $primingdata = array(
   );
      protected $defaultpermissions = array(
      'USR_Permissions' => '
"User_Id","Domain","Label","Value"
1,"species","Registered User",1
1,"species","Species Access",1
1,"species","Species Administrator",1
1,"species","System Administrator",1',
      'USR_Default_Permissions' => '
"Domain","Label","Description","Value","Anon" 
"species","System Administrator","Full control",0,0
"species","Species Administrator","Add and edit datasets and records",0,0
"species","Species Access","View all species data",0,0
"species","Registered User","View unprotected species data",1,0'   
   );

}
class G_Dataset_Description extends DBT {
   protected $tablename = 'G_Dataset_Description';
   protected $displayname = 'Dataset Description';
   protected $user;
   protected $columns = array(
      'G_Dataset_Description_ID'     => 'Unique Key',
      'G_Dataset_ID'    => '{"DataType":"LINKEDTO","TargetType":"G_Dataset","TargetField":"G_Dataset_ID","Mandatory":1,"Current":1}',
      'Description'     => 'Long Text'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $primingdata = array(
   );   
}
class G_Record extends DBT {
   protected $tablename = 'G_Record';
   protected $displayname = 'Species Record';
   protected $perpage = 25;
   protected $user;
   protected $alts = array('Datasets'=>'G_Dataset','Species Lists'=>'G_Species_List');
   protected $columns = array(
      'G_Record_ID'     => 'Unique Key',
      'G_Dataset_ID'    => '{"DataType":"LINKEDTO","TargetType":"G_Dataset","TargetField":"G_Dataset_ID","Mandatory":1,"Current":1}',
      'Species'         => 'Taxa',
      'Date'            => 'Date',
      'Location'        => '{"DataType":"Map Data","Derives":[{"Column":"Grid_Reference","Method":"s2_getgr","Params":[{"Type":"Value"}]}]}',
      'Grid_Reference'  => '{"DataType":"Short Text","Derives":[{"Column":"Location","Method":"s2_getgrsquare","Params":[{"Type":"Value"}]}]}',
      'Recorder'        => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person"],"Mandatory":1}'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $primingdata = array(
   );   
}
class G_Dataset_File extends DBT {
   protected $tablename = 'G_Dataset_File';
   protected $displayname = 'Dataset Files';
   protected $user;
   protected $columns = array(
      'G_Dataset_File_ID'     => 'Unique Key',
      'G_Dataset_ID'          => '{"DataType":"LINKEDTO","TargetType":"G_Dataset","TargetField":"G_Dataset_ID","Mandatory":1,"Current":1}',
      'File'            => 'File Path'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $primingdata = array(
   );   
} 
class G_Species_List extends DBT {
   protected $tablename = 'G_Species_List';
   protected $displayname = 'Species List';
   protected $perpage = 10;
   protected $user;
   protected $alts = array('Datasets'=>'G_Dataset','Records'=>'G_Record');
   protected $show = array('G_List_Member');
   protected $view = '[
      {
         "Name":"List Members",
         "Data": [
            {
               "Table":"G_List_Member",
               "Show":"All"
            }                                                                                                    
         ]
      }      
   ]';
   protected $columns = array(
      'G_Species_List_ID'     => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text","Mandatory":1}',
      'Abbreviation'          => '{"DataType":"Short Text","Mandatory":1}',
      'Description'           => 'Long Text',
      'G_List_Type_ID'        => '{"DataType":"LINKEDTO","TargetType":"G_List_Type","TargetField":"G_List_Type_ID","Mandatory":1,"Current":1}',
      'G_List_Class_ID'       => '{"DataType":"LINKEDTO","TargetType":"G_List_Class","TargetField":"G_List_Class_ID","Mandatory":1,"Current":1}',
      'G_List_Extent_ID'      => '{"DataType":"LINKEDTO","TargetType":"G_List_Extent","TargetField":"G_List_Extent_ID","Current":1,"NoList":1}'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Species Administrator'
   );
}

class G_List_Member extends DBT {
   protected $tablename = 'G_List_Member';
   protected $displayname = 'List Member';
   protected $perpage = 10;
   protected $user;
   protected $alts = array('Species Lists'=>'G_Species_List','Datasets'=>'G_Dataset','Records'=>'G_Record');
   protected $columns = array(
      'G_List_Member_ID'         => 'Unique Key',
      'G_Species_List_ID'        => '{"DataType":"LINKEDTO","TargetType":"G_Species_List","TargetField":"G_Species_List_ID","Mandatory":1,"Current":1}',
      'Species'                  => '{"DataType":"Taxa","Mandatory":1}',
      'Score'                    => '{"DataType":"Decimal"}',
      'G_List_Scoring_Group_ID'  => '{"DataType":"LINKEDTO","TargetType":"G_List_Scoring_Group","TargetField":"G_List_Scoring_Group_ID","Current":1}'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Species Administrator'
   );
   function getmembership($type,$value) {
      $membership = array();
      $sl = new G_Species_List();
      $lists = $sl->select("${type}_ID",$value,false);
      $obj = new stdClass();
      $obj->Lists = $lists;
      foreach($lists as $lx => $list) {
         $abbrev = $list['Abbreviation'];
         $members = $this->select('G_Species_List_ID',$list['G_Species_List_ID']);
         foreach($members as $mx => $mem) {
            $tvk = $mem['Species']['Value'];
            if (!isset($membership[$tvk])) $membership[$tvk] = array();
            if (!in_array($abbrev,$membership[$tvk])) $membership[$tvk][] = $abbrev;               
         }         
      }
      $obj->Membership = $membership;
      return $obj;
   }
}

class G_List_Type extends DBT {
   protected $tablename = 'G_List_Type';
   protected $displayname = 'Species List Type';
   protected $show = array('G_Species_List');
   protected $columns = array(
      'G_List_Type_ID'    => 'Unique Key',
      'Name'              => '{"DataType":"Short Text","Mandatory":1}',
      'Description'       => 'Long Text' 
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'    => 'Species Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'G_List_Type' => '
"Name","Description"
"Protection","Protected and priority species legislation."
"Indicator","Species indicative of habitat or environmental character."
"Sensitive","Species for which data is considered sensitive."
"Custom","Allows users to define their own search criteria."'
   );   
}

class G_List_Class extends DBT {
   protected $tablename = 'G_List_Class';
   protected $displayname = 'Species List Class';
   protected $show = array('G_Species_List');
   protected $columns = array(
      'G_List_Class_ID'   => 'Unique Key',
      'Name'              => '{"DataType":"Short Text","Mandatory":1}',
      'Code'              => '{"DataType":"Short Text","Mandatory":1}',
      'Description'       => 'Long Text' 
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'    => 'Species Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'G_List_Class' => '
"Name","Code","Description"
"UK Legislative Protection","A","Species protected in UK law."
"UK Biodiversity Action Plan","B","Species prioritised in the UK BAP."
"Conservation","C","Species identified as at risk in conservation lists such as red data books."
"Local BAP Species","D","Species identified in local biodiversity action plans limited by the list extent."
"Extinct","E","Extict locally, regionally or nationally."
"Other","F","Other legislation."
"Non-native","G","Imports and invasives."
"Migrants","H","Migrants and vagrants."
"Indicators","I","Indicators, habitats, Ellenberg.."'
   );   
}

class G_List_Extent extends DBT {
   protected $tablename = 'G_List_Extent';
   protected $displayname = 'Species List Extent';
   protected $show = array();
   protected $columns = array(
      'G_List_Extent_ID'  => 'Unique Key',
      'Name'              => '{"DataType":"Short Text","Mandatory":1}',
      'Boundary'          => '{"DataType":"Map Data","Mandatory":1}',
      'Description'       => 'Long Text' 
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'    => 'Species Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}

class G_List_Scoring_Group extends DBT {
   protected $tablename = 'G_List_Scoring_Group';
   protected $displayname = 'Species List Scoring Group';
   protected $show = array();
   protected $columns = array(
      'G_List_Scoring_Group_ID'  => 'Unique Key',
      'G_Scoring_Group_Type_ID'  => '{"DataType":"LINKEDTO","TargetType":"G_Scoring_Group_Type","TargetField":"G_Scoring_Group_Type_ID","Mandatory":1,"Current":1}',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Description'              => 'Long Text' 
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'    => 'Species Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}

class G_Scoring_Group_Type extends DBT {
   protected $tablename = 'G_Scoring_Group_Type';
   protected $displayname = 'Scoring Group Type';
   protected $show = array();
   protected $columns = array(
      'G_Scoring_Group_Type_ID'  => 'Unique Key',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Description'              => 'Long Text' 
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'    => 'Species Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'G_Scoring_Group_Type' => '
"Name","Description"
"Total","Add up the scores for all the recorded species in the group."
"Maximum","Report the highest single species score for recorded species in the group."
"Average","Average of the scores for all recorded species in the group (Ellenberg)."'
   );   
}

/*
class G_Dataset_Species extends DBT_Analysis {
   protected $tablename = 'S2_Dataset_Species';
   protected $displayname = 'Dataset Species';
   protected $domain = 'species';
   protected $selectto = 'G_Dataset';
   protected $columns = array(
      'S2_Authority_ID'          => '{"DataType":"LINKEDTO","TargetType":"S2_Authority","TargetField":"S2_Authority_ID","Mandatory":1,"Current":1,"Hidden":1,"Inherit":1}',
      'Authority'                => '{"DataType":"Short Text","Hidden":1}',
      'S2_Designation_ID'        => '{"DataType":"LINKEDTO","TargetType":"S2_Designation","TargetField":"S2_Designation_ID","Mandatory":1,"Current":1}',
      'Designation'              => '{"DataType":"Short Text","Hidden":1}',
      'Sites'                    => '{"DataType":"Number"}',
      'Appropriate_Management'   => '{"DataType":"Number"}',
      'Percentage_In_Management' => '{"DataType":"Percentage"}',
      'Candidates'               => '{"DataType":"Number"}',
      'Designated'               => '{"DataType":"Number"}',
      'Deleted'                  => '{"DataType":"Number"}',                    
   ); 
   protected $permissions = array(
      'Def'    => 'NOONE'
   );
   function S2_Designation_Overview() {
      parent::DBT_Analysis();
   }
   function create() {
      $select = "SELECT G_Dataset_ID,G_Record_ID,"; 
      $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
      $this->dbhandle->exec($statement);
      unset($pending[$this->tablename]);
   }
   function getuserpermissions() {
      return false;      
   }   
}
*/
?>