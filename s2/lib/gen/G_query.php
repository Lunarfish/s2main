<?php
error_reporting(E_ERROR);
include_once('proj4php/proj4php.php');
include_once('spatial/coordinateFunctions.php');
include_once('spatial/spatialFunctions.php');
include_once('gen/G_Process.php');
//ini_set('memory_limit','64M');
//set_time_limit(0);

//print $itype;exit;
//print "<pre>"; print_r($_REQUEST); print "</pre>"; 
//exit;

$dbt = new $itype();
if (!isset($_REQUEST['Action']) && !isset($_REQUEST['IType']) && !$dbt->exists()) $dbt->initialise();
$sid = $dbt->getsid(); 
if (!isset($sid) && isset($_REQUEST['SID'])) $dbt->setsid($_REQUEST['SID']);
if ($dbt->isallowed($_CURRENT_USER,$action)) {
   if (!$dbt->exists()) $dbt->create();
   switch($action) {
   case 'GetForm': {
      include_once('gen/G_User_Forms.php');
      $form = (isset($_REQUEST['Form']))?$_REQUEST['Form']:'Login';
      $obj = new stdClass();
      eval("\$fobj = \$G_User_Forms->Forms->$form;");
      $obj->Form = $fobj;
      $obj->IType = $fobj->IType; 
   }break;
   case 'Login': {
//print_r($_REQUEST);exit;
      include_once('gen/G_User_Forms.php');
      $user = $_REQUEST['Email'];
      $pass = $_REQUEST['Password'];
//print "$user $pass";
      $status = $dbt->login($user,$pass);
      switch ($status) {
      case 'unrecognised': {
         $form = 'Register';
         $obj = new stdClass();
         eval("\$fobj = \$G_User_Forms->Forms->$form;");
         $obj->Form = $fobj;
         $obj->Status = 0;
         $obj->Message = 'User or password not recognised';
      }break;
      case 'unconfirmed': {
         $form = 'Confirm';
         $obj = new stdClass();
         eval("\$fobj = \$G_User_Forms->Forms->$form;");
         $obj->Form = $fobj;
         $obj->Status = 0;
         $obj->Message = 'User requires confirmation';
      }break;
      default: {
         // already loaded by login function
         //$dbt->loaddata($current);
         //$currentdata = $dbt->getdata();
         $view = (isset($_REQUEST['ViewName']))?$_REQUEST['ViewName']:null;
         $obj = $dbt->getview($settings,$view);
         $obj->UInfo = $dbt->getcookieobj();
         $obj->Status = 1;
         $obj->Message = 'User logged in successfully';
         $_CURRENT_USER->setid($dbt->getid());
         $_CURRENT_USER->load_from_db();
         $_CURRENT_USER->is_anonymous = 0;
      }break;
      }
   }break;
   case 'Logout': {
      if (!$_CURRENT_USER->is_anonymous) $status = $dbt->logout();
      include_once('gen/G_User_Forms.php');
      $obj = new stdClass();
      $obj->Form = $G_User_Forms->Forms->Login;
      $obj->Status = 1;
      $obj->Message = 'Logged out successfully';
      $_CURRENT_USER = new User();
      $_CURRENT_USER->is_anonymous = 1; 
   }break;
   case 'Register': {
      $cols = $dbt->getcolumns();
      $data = array();
      foreach($cols as $name => $type) {
         if (isset($_REQUEST[$name])) {
            $data[$name] = $_REQUEST[$name];
         }
      }
      $obj = $dbt->register($data);
   }break;
   case 'Confirm': {
      include_once('gen/G_User_Forms.php');
      $token = $_REQUEST['Token'];
      $password = $_REQUEST['Password'];
      $obj = $dbt->confirm($token,$password);
      $obj->Form = $G_User_Forms->Forms->Login;
   }break;
   case 'Google_Confirm': {
      include_once('gen/G_User_Forms.php');
      $token = $_REQUEST['Token'];
      $password = $_REQUEST['Password'];
      $obj = $dbt->googleconfirm($token,$password);
      $obj->Form = $G_User_Forms->Forms->Login;
   }break;
   case 'SendPassword': {
      $dbt->loaddata($current);
      $currentdata = $dbt->getdata();
      $view = (isset($_REQUEST['ViewName']))?$_REQUEST['ViewName']:null;
      $obj = $dbt->getview($settings,$view);
      $status = $dbt->send_password();
      $message = ($status==0)?'Email could not be sent.':'The password has been sent.';
      $obj->Status = $status;
      $obj->Message = $message;
   }break;
   case 'List': {
      $rows = $dbt->selectAllRowCount($settings);
      $obj = $dbt->getview($settings);
      $perpage = $dbt->getperpage();
      if ($rows > $perpage && !isset($_REQUEST['NoPaging'])) {
         $page = (isset($settings->$itype) && isset($settings->$itype->Page))
            ?$settings->$itype->Page
            :1;
         $dbts = $dbt->selectAll($settings,$page);
         $obj->Page = $page;
         $obj->Pages = ($rows>0)?ceil($rows/$perpage):1;
      } else {
         $dbts = $dbt->selectAll($settings);
      }
      $obj->ListData = $dbts;      
   }break;
   case 'View': {
//print_r($settings);exit;
      $dbt->loaddata($current);
      $currentdata = $dbt->getdata();
      $view = (isset($_REQUEST['ViewName']))?$_REQUEST['ViewName']:null;
      $obj = $dbt->getview($settings,$view);
      if (isset($view)) $obj->ViewName = $view;
   }break;
   case 'One': {
      $ptype = $_REQUEST['ParentType'];
      $pid = $_REQUEST['ParentID'];
      $met = $_REQUEST['Method'];
      list($scol,$tcol) = $dbt->getlinkedcolumn($ptype);
      $dbt->loadone($pid,$scol,$met);
      $obj = $dbt->getview($settings,$view);
   }break;
   case 'MSelect': {
      if (!is_array($current)) $current = json_decode($current);
      //print_r($current);
      $dbt->noshow();
      $obj = $dbt->getview();
      $obj->Current = 'Multiple';
      $obj->ListData = array();
      $obj->Key = $dbt->getpk();
      foreach($current as $count => $val) {
         $dbt->loaddata($val);
         $obj->ListData[] = $dbt->getdata();
      }
   }break;
   case 'ColumnValue': {
      $dbt->loaddata($current);
      $col = $_REQUEST['Column']; 
      $val = $dbt->selectcolumnval($col);
      $obj->IType = $itype;
      $obj->Current = $current;
      $obj->Column = $col;
      $obj->Value = $val;
   }break;
   case 'GetDeriveReqs': {
      $obj = $dbt->getderiverequirements();         
   }break;
   case 'GetParameters': {
      $params = json_decode(stripslashes($_REQUEST['Parameters']));
      $settings = json_decode(stripslashes($_REQUEST['CurrentSettings']));
      $obj = array();
      foreach($params as $param) {
         $name = $param->Name;
         $paramtype = $param->From;
         $prop = $param->Property;
         //eval("\$lcurrent = (isset(\$settings->${paramtype}))?\$settings->${paramtype}->Current:null;");
         $lcurrent = (isset($settings->$paramtype))?$settings->$paramtype->Current:null;
         if ($paramtype == $itype) $link = $dbt;
         //else eval("\$link = new $paramtype();");
         else $link = new $paramtype();
         if (isset($link)) {
            if (isset($lcurrent)) { 
               $link->loaddata($lcurrent);
               $obj[$name] = $link->selectcolumnval($prop);
            } else {
               list($scol,$tcol) = $link->getlinkedcolumn($itype);
               $hasdate = false;
               $hasversion = false;
               $hascount = false;
               $usecol = null;
               foreach($link->getinterface() as $col => $def) {
                  if (is_object($def)) $type = $def->DataType;
                  else $type = preg_replace('/\:.+/','',$def);
                  switch ($type) {
                  case 'Date': {
                     $hasdate = true;
                  }break;
                  case 'Version': {
                     $hasversion = true;
                  }break;
                  case 'Count': {
                     $hascount = true;
                  }break;
                  } 
               }
               if ($hasversion) {
                  $usecol = $link->getdisplayname();
                  $usecol = preg_replace('/\s+/','_',$usecol);
                  $link->loadone($current,$scol,'Current',$dbt->data[$usecol]);               
               } else if ($hascount) {
                  $link->loadone($current,$scol,'Last');            
               } else if ($hasdate) {
                  $link->loadone($current,$scol,'Previous');   
               }               
               if ($link->getid()) {$obj[$name] = $link->selectcolumnval($prop);}
            }
         }
      }
   }break;
   case 'Add': {
      $data = array();
      $cols = $dbt->getcolumns();
      $max = $dbt->getcount();
      $counts = $dbt->getcolumnsbytype('Count');
      foreach($cols as $name => $type) {
         if (isset($_REQUEST[$name])) $data[$name] = $_REQUEST[$name];
         if (in_array($name,$counts)) $data[$name] = $dbt->reserve($name,$max);//$data[$name]);
      }
      $dbt->setdata($data,true);
      $status = $dbt->insert();
      if ($status) $dbt->unreserve();
      $message = ($status==0)?'Insert data failed':'Data inserted successfully';
      $obj = $dbt->getview($settings);
      $obj->Status = $status;
      $obj->Message = $message;
      if (isset($_REQUEST['NoDraw'])) $obj->NoDraw = 1; 
      if (isset($_REQUEST['Refresh'])) $obj->Refresh = 1; 
      
   }break;
   case 'Edit': {
      $cols = $dbt->getcolumns();
      $data = array();
      foreach($cols as $name => $type) {
         $type = preg_replace('/\:.+/','',$type);
         if (isset($_REQUEST[$name])) $data[$name] = $_REQUEST[$name];
      }
      $dbt->setdata($data);
      $status = $dbt->update($dbt->getpk(),$current);
      if ($status) $dbt->unreserve();
      $message = ($status==0)?'Update failed':'Data updated successfully';
      $obj = $dbt->getview($settings);
      $obj->Status = $status;
      $obj->Message = $message;
   }break;
   case 'SetVersion': {
      $settings = json_decode(stripslashes($_REQUEST['CurrentSettings']));
      //eval("\$current = \$settings->${itype}->Current;");
      $current = $settings->${itype}->Current;
      $status = $dbt->updateversion($current,$_REQUEST['VersionOf'],$_REQUEST['Version']);
      $message = ($status==0)?'Update failed':'Data updated successfully';
      $dbt->loaddata($current);
      $obj->Status = $status;
      $obj->Message = $message;
      $obj->NoDraw = 1;
      $obj->Refresh = 1;
   }break;      
   case 'Delete': {
      if (isset($current)) {
         $dbt->loaddata($current);
         $status = $dbt->delete();
      } else if (isset($_REQUEST['Column']) && isset($_REQUEST['Value'])) {
         $col = $_REQUEST['Column'];
         $val = $_REQUEST['Value'];
         $status = $dbt->delete($col,$val);  
      } else {
         $status = $dbt->deleteAll();
      }
      $message = ($status==0)?'Delete failed':'Data deleted successfully';
      $parent = $dbt->getparent();
      if (isset($parent) && isset($parent->IType)) {         
         $ptype = $parent->IType;
         $pdbt = new $ptype();
         $pdbt->loaddata($parent->Data);
         $obj = $pdbt->getview($settings);
      } else {
         $dbt->cleardata();
         $dbts = $dbt->selectAll($settings);
         $obj = $dbt->getview($settings);
         //$obj->ListData = $dbts;
         if (count($dbts) > $perpage && !isset($_REQUEST['NoPaging'])) {
            $pages = array_chunk($dbts,$perpage);
            $page = (isset($settings->$itype) && isset($settings->$itype->Page))
               ?$settings->$itype->Page
               :1;
            $obj->ListData = $pages[$page-1];
            $obj->Page = $page;
            $obj->Pages = count($pages);
         } else $obj->ListData = $dbts;
      }
      $obj->Status = $status;
      $obj->Message = $message;
   }break;
   case 'Create': {
      $status = $dbt->create();
      $message = ($status==0)?'Create table failed':'Table created successfully';
      $obj = new stdClass();
      $obj->Status = $status;
      $obj->Message = $message;
   }break;
   case 'Drop': {
      $status = $dbt->drop();
      $message = ($status==0)?'Drop table failed':'Table dropped successfully';
      $obj = new stdClass();
      $obj->Status = $status;
      $obj->Message = $message;
   }break;
   case 'Import': {
      set_time_limit(300);
      /* 
         Import Settings are defined in a JSON object passed back and forth 
         between the server and client. Mostly this is defined by JavaScript 
         to the point where the import is ready to execute but the user then 
         has a chance to review the data to be executed before the import is 
         carried out. 
         
         The import action has several steps defined by the stage variable.
         
         On the client side the stages are: 
         Choose Type 
         Choose File(s)
         Match Columns
          
         For the server side these stages are:
            
         Collate - Process the import file(s) into the data to be exported 
         Import - Actually carry out the import of the data into the database 
         Review - Report on the success of the import        
      */
      if (isset($_REQUEST['ImportSettings'])) {
      //print $_REQUEST['ImportSettings'];exit;
      
         $iset = json_decode($_REQUEST['ImportSettings']);
      //print_r($iset);
         $settings = json_decode(stripslashes($_REQUEST['CurrentSettings']));
         $coldefs = $dbt->getcolumns();
         switch($iset->Stage) {
         case 'Collate': {
            // get data 
            // http or move get data into S2_File class
            $dirs = preg_split('/\//',$_SERVER['PHP_SELF']);
            array_pop($dirs);array_pop($dirs);
            $p = join('/',$dirs).'/';
            $url = 'http://'.$_SERVER['HTTP_HOST'].$p.'echofile_v1_1.php';
            switch($iset->ImportType) {
            case 'csv': {
               $post = array(
                  'a' => 'getdata',
                  'i' => $iset->Files->csv 
               );
               $qs = 'a=getdata&i='.$iset->Files->csv;               
            }break;
            case 'xls': {
               $post = array(
                  'a' => 'getdata',
                  'i' => $iset->Files->xls,
                  's' => $iset->Worksheet 
               );             
               $qs = 'a=getdata&i='.$iset->Files->xls.'&s='.$iset->Worksheet;               
            }break;
            case 'mif': {
               $post = array(
                  'a' => 'getdata',
                  'i' => $iset->Files->mif,
                  'd' => $iset->Files->mid 
               );             
               $qs = 'a=getdata&i='.$iset->Files->mif.'&d='.$iset->Files->mid;
                         
            }break;               
            }
            $curl = curl_init($url);
            //print_r($post);
         
            curl_setopt($curl, CURLOPT_POST, true);
   	      //curl_setopt ($curl, CURLOPT_POSTFIELDS, $post);
   	      curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            // Make the call
            $resp = curl_exec($curl);
            $data = json_decode($resp);
            
            curl_close($curl);             
            //$iset->Data = $data;
            
            $idata = array();
            // for each column in the table to be imported to there are 5 match 
            // types Column,Static,Auto,Link,SameForAll
            // need to figure out how to enter version and count data.
            $autodata = array();
            foreach ($data->Data as $en => $entry) {
               $ientry = array(); 
               $entry = (array)$entry;
               $importable = true;
               $icrumbs = array();
               foreach($iset->Matches as $col => $match) {
                  if (!isset($match->IsCrumb) || !$match->IsCrumb) {
                     if ($match->Value == 'Auto') {
                        $iparams = $dbt->getcolumndef($col); 
                        if (!isset($autodata[$col])) $autodata[$col] = $iparams;
                     } else {
                        switch($match->Type) {
                        case 'Column': {
                           $ientry[$col] = $entry[$match->Value];   
                        }break;
                        case 'Static': {
                           $ientry[$col] = $match->Value;   
                        }break;
                        case 'SameForAll': {
                           $iparams = $dbt->getcolumndef($col); 
                           $ttype = $iparams->TargetType;
                           $link = new $ttype();
                           $id = $link->loaddata($match->Value);
                           $importable &= ($id>0);
                           $ientry[$col] = $link->getnameid();
                        }break;
                        case 'Link': {
                           $iparams = $dbt->getcolumndef($col); 
                           $ttype = $iparams->TargetType;
                           $link = new $ttype();
                           $icrumbs = array();
                           foreach($settings as $stype => $setting) {
                              if ($stype!= $ttype) {
                                 $smatch = new stdClass();
                                 $smatch->IType = $stype;
                                 $smatch->Type = 'SameForAll';
                                 $smatch->Value = $setting->Current;
                                 $icrumbs[$stype] = $smatch;
                              }      
                           }
                           $c = $match->TColumn;
                           $v = $entry[$match->IColumn];
                           $id = $link->loadderiveddata($icrumbs,$c,$v);
                           $importable &= ($id>0);
                           $ientry[$col] = $link->getnameid();
                        }break;
                        case 'Derived': {
                           $iparams = $dbt->getcolumndef($col); 
                           $ttype = $iparams->TargetType;
                           $link = new $ttype();
                           $icrumbs = array(); 
                           foreach($iset->Matches as $ccol => $cmatch) {
                              if (isset($cmatch->IsCrumb) && $cmatch->IsCrumb) {
                                 $cmatch->IType = $ccol;
                                 if ($cmatch->Type == 'Link') $cmatch->Value = $entry[$cmatch->IColumn];
                                 $icrumbs[$ccol] = $cmatch;
                              }
                           }
                           $link = new $ttype();
                           $id = $link->loadderiveddata($icrumbs); 
                           $importable &= ($id>0);
                           $ientry[$col] = $link->getnameid();                        
                        }break;
                        }
                     }
                  } 
               }
               foreach($ientry as $col => $val) {
                  $params = $dbt->getcoldef($col);
                  if (isset($params)) $ientry[$col] = $dbt->preformatterm($col,$params->DataType,$val);
               }
               if ($importable) $idata[] = $ientry;
            }
            if (count($autodata)>0) {
               foreach($idata as $ik => $ientry) {
                  foreach($autodata as $col => $ad) {
                     switch ($ad->DataType) {
                     case 'Count': {
                        $cot = $ad->CountOfType;
                        $cof = $ad->CountOfField;
                        if (!isset($ad->Data)) $ad->Data = array();
                        $val = (is_object($ientry[$cof]))?$ientry[$cof]->Id:$ientry[$cof];
                        if (!isset($ad->Data[$val])) $ad->Data[$val] = $dbt->getcount($val);   
                        else $ad->Data[$val] += 1;
                        $ientry[$col] = $ad->Data[$val];
                     }break;
                     case 'Version': {
                        $cot = $iparams->VersionOfType;
                        $cof = $iparams->VersionOfField;
                        if (!isset($ad->Data)) $ad->Data = array();
                        $val = (is_object($ientry[$cof]))?$ientry[$cof]->Id:$ientry[$cof];
                        if (!isset($ad->Data[$val])) {
                           $vers = $dbt->getversions($val);
                           $ad->Data[$val] = (isset($vers['Maximum']))?($vers['Maximum'] + 0.01):1.0;
                        } else $ad->Data[$val] += 0.01;
                        $ientry[$col] = $ad->Data[$val];                                                
                     }break;                     
                     default: {
                        $ientry[$col] = '';
                     }break;
                     }
                     $autodata[$col] = $ad;
                  }
                  $idata[$ik] = $ientry;
               }
            }
            unset($iset->Links);
            $iset->Stage = 'Preview';
            $iset->ListData = $idata;
         }break;
         case 'Import': {
            foreach($iset->ListData as $i => $entry) {
               $dbt->setdata((array)$entry,true);
               $dup = $dbt->isduplicate();
               if (!$dup) $status = $dbt->insert();
               $entry->Status = ($dup)?'Duplicate':(($status)?'Imported':'Failed');
               $iset->ListData[$i] = $entry;
            }
         }break;
         }                           
         $obj = $iset;
      }      
   }break;
   case 'CheckIn': {
      $obj = new stdClass();
      $obj->Status = 1;
      $obj->Message = 'Checked In';
      $obj->NoDraw = 1;
   }break;
   case 'Calculate': {
      $calc = $_REQUEST['Calculation'];
      $res = null;
      $msg = null;
      switch($calc) {
      case 'Buffer': {
         // Buffers can only be calculated on PGSQL database platform
         // MySQL doesn't support this function.
         global $dbPlatform;
         if ($dbPlaform == 'PGSQL') {
            $poly = $_REQUEST['Polygon'];
            $buffer = $_REQUEST['Buffer'];
            $proj = (isset($_REQUEST['Native']))?$_REQUEST['Native']:27700;
            $expr = new Zend_Db_Expr("st_astext(st_transform(st_buffer(st_transform(st_geomfromtext('$poly',4326),$proj),$buffer),4326))");break;
            $sel = "SELECT $expr AS buffer";
            $dbpg = SnC_getPGNativeConnection();
            $res = pg_query($dbpg,$sel);
            $res = pg_fetch_array($res);
            $intersects = $res['buffer'];
         } else {
            $res = null;
            $msg = 'The MySQL version of S2 does not support automatic buffering.';
         }         
      }break;
      case 'Site_Code': {
         $type = $_REQUEST['IType'];
         $col = $_REQUEST['Column'];
         $lcol = strtolower($col);
         $table = strtolower($type);
         $zone = $_REQUEST['Zone'];
         $expr = new Zend_Db_Expr("max($lcol)");
         $dbh = SnC_getDatabaseConnection();
         $q = $dbh->select();
         $q->from($table,array($col=>$expr));
         $q->where("$lcol like '$zone%'");
         $res = $dbh->fetchOne($q);
         $res = preg_replace('/[^\-]+\-/','',$res);
         $res = $zone . sprintf('-%1$03d', (intval($res) + 1));
      }break;
      case 'File_Code': {
         $dbh = SnC_getDatabaseConnection();
         $dbp = SnC_getPlatform();
         $dom = $dbt->getdomain();
         $uid = $dbt->getuser()->getid();
         $type = $_REQUEST['IType'];
         $col = $_REQUEST['Column'];
         $lcol = strtolower($col);
         $prefix = $_REQUEST['Prefix'];
         
         $q = $dbh->select();
         switch($dbp) {
         case 'PGSQL': {
            $table = strtolower($type);
            $expr = new Zend_Db_Expr("max($lcol)");
            $q->from($table,array($col=>$expr));
            $q->where("$lcol like '$prefix%'");
         }break;
         case 'MySQL': {
            $table = $type;
            $expr = new Zend_Db_Expr("Max($col)");
            $q->from($table,array($col=>$expr));
            $q->where("$col like '$prefix%'");
         }break;
         }
         $max = $dbh->fetchOne($q);
         $max = intval(preg_replace("/$prefix/",'',$max));
         
         $grn = new G_Reserve_Number();             
         $res = $grn->reserve($dom,$type,$prefix,$max);
         $res = $prefix . sprintf('%1$04d', intval($res));
      }break;
      }
      if (isset($res)) { 
         $obj = new stdClass();
         $obj->Status = 1;
         $obj->Message = "Calculation of $calc succeeded";
         $obj->Result = $res;
         $obj->NoDraw = 1;
      } else {
         $obj = new stdClass();
         $obj->Status = 0;
         $obj->Message = ($msg)?$msg:'Action failed';
         $obj->NoDraw = 1;
      } 
   }break;
   case 'Call': {
      $method = $_REQUEST['Method'];
      $args = json_decode($_REQUEST['Args']);
      $call = call_user_func_array(array($dbt,$method),$args);
      $obj = new stdClass();
      $obj->Status = ($call != false);
      $obj->Response = $call;       
   }break;
   case 'ProcessStatus': {
      $current = $_REQUEST['Current'];
      $dbt->loaddata($current);
      $process = new G_Post_Processor(); 
      $process->loadmyprocessor($dbt->getid(),$dbt->gettablename());
      $obj = $process->getstatus();      
   }break;
   case 'PostProcess': {
      set_time_limit(0);
      $process = $dbt->getpostprocess();
   //print_r($process);exit;
      $step = $_REQUEST['Step'];
      $pstep = $process[$step];
      $name = $pstep->Name;
      $method = $pstep->Method;
      $current = $_REQUEST['Current'];
   //print("$step $name $method");exit;
      $dbt->loaddata($current);
      eval("\$res = \$dbt->$method();");
      //$res = $dbt->$method();
      if (isset($res)) {
         $dbt->setdata(array('PP_Step'=>$step));
         $dbt->update($dbt->getpk(),$dbt->getid());
         $obj = $dbt->getview();
         $obj->Status = 1;
         $obj->Executing = 1;    
         $obj->Message = 'Step Completed';
      } else {
         $obj = new stdClass();
         $obj->Status = 0;
         $obj->Message = 'Step Failed';      
         $obj->NoDraw = 1;
      } 
   }break;
   }
   if (isset($obj)) {
      if ($dbt->getorigdomain() != $dbt->getdomain()) {
         if ($_CURRENT_USER->can_set_perm()) {
            $link = new stdClass();
            $link->ToDomain = 'users';
            $link->Parameters = array(
               'domain'    => $dbt->getorigdomain(),
               'instance'  => $dbt->getdomain(),
               'alias'     => $dbt->getdomaininstancename()
            );                          
            if (is_object($obj)) $obj->ManagePermissions = $link;
            //else $obj['ManagePermissions'] = $link;   
         }
      } 
      /* 
         Add permissions granted to current user      
      */
      if (is_object($obj)) {
         if (!$_CURRENT_USER->is_anonymous) {
            $obj->UserPermissions = get_permissions_for_user($_CURRENT_USER->getid(),$dbt->getorigdomain());
            $obj->UserPermissions['UserName'] = $_CURRENT_USER->getusername();
            $obj->UserPermissions['UserId'] = $_CURRENT_USER->getid(); 
         } else $obj->UserPermissions = get_default_permissions();
      }
      echo(json_encode($obj));
   }
} else {
   $obj = new stdClass();
   $obj->Status = 0;
   $obj->Message = "You do not have permission to $action ".$dbt->getdisplayname()." data.";
   if (is_object($obj)) {
      if (!$_CURRENT_USER->is_anonymous) {
         $obj->UserPermissions = get_permissions_for_user($_CURRENT_USER->getid(),$dbt->getorigdomain());
         $obj->UserPermissions['UserName'] = $_CURRENT_USER->getusername();
      } else $obj->UserPermissions = get_default_permissions();
   }
   echo(json_encode($obj));
}

?>