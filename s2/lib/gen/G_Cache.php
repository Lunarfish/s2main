<?php
/*
   The Cache stores data definitions for cross-domain data. 
    
   The interface tables cross between domains and return a JSON definition 
   describing how to identify the linked data in the other domain. So a contact
   interface returns the Contact name, database id and the table from which the 
   data has been retrieved. 
   
   Likewise a taxa from the NHM dictionary NBN WebService returns the 
   taxon name as well as the TaxonVersionKey for that species.
   
   Each time in interface is populated the cache entry is entered or updated and 
   the cache id is returned and stored in the interface column. 
*/
include("gen/G_Cache_Gen.php");
include("gen/G_Cache_${dbPlatform}.php");
?>