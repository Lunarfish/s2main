<?php
/*
   The Cache stores data definitions for cross-domain data. 
    
   The interface tables cross between domains and return a JSON definition 
   describing how to identify the linked data in the other domain. So a contact
   interface returns the Contact name, database id and the table from which the 
   data has been retrieved. 
   
   Likewise a taxa from the NHM dictionary NBN WebService returns the 
   taxon name as well as the TaxonVersionKey for that species.
   
   Each time in interface is populated the cache entry is entered or updated and                       
   the cache id is returned and stored in the interface column. 

   ALTERED PGSQL VERSION   
*/
class G_Cache_Gen extends DBT {
   protected $tablename = 'G_Cache';
   protected $displayname = 'Interface Cache';
   protected $columns = array(
      'G_Cache_ID'         => 'Unique Key',
      'Data_Type'          => 'Short Text',
      'Name'               => 'Short Text',
      'Value_Type'         => 'Short Text', 
      'Value'              => 'Short Text',
      'Domain'             => 'Short Text',
      'JSON'               => 'Long Text' 
   ); 
   protected $domain = 'genera';
   protected $permissions = array(
      'Def'    => 'Registered User',
      'Delete' => 'System Administrator'
   );
   function G_Cache_Gen() {
      parent::DBT();
      if (!$this->exists()) $this->create();
   }
   function getcacheid() {
      $query = $this->dbhandle->select();
      $query->from(strtolower($this->tablename));
      $query->where('data_type = ?',$this->data['Data_Type']);
      $query->where('value = ?',strval($this->data['Value']));
      $result = $this->dbhandle->fetchAll($query);
      $id = null;
      if (count($result)>0) $id = $result[0]['g_cache_id'];
      return $id;
   }
   function insert() {
      // select where Data_Type and Value match if no match insert / else update
      $id = $this->getcacheid();
      $status = 0;
      if (isset($id)) {
         $this->update($this->getpk(),$id);
         $this->data[$pk] = $id;
         $this->id = $id;
         $status = $id;
      } else {
         if (!$this->validated) $this->validate();
         //$this->dbhandle->insert(strtolower($this->tablename),array_change_key_case($this->data,LOWER_CASE));
         $pk = $this->getpk();
         $sq = strtolower($this->tablename).'_'.strtolower($pk).'_seq';
         $id = $this->dbhandle->nextSequenceId($sq);
         $r = $this->pgnativeinsert($id,array_change_key_case($this->data,CASE_LOWER));
         //$id = $this->dbhandle->lastInsertId();
         if ($r) {
            $this->data[$pk] = $id;
            $this->id = $id;
            $status = $r;   
         }
      }
      return $status;            
   }         
}
?>