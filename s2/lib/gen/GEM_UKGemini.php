<?php
/*
   UK GEMINI Implementation
*/
error_reporting(E_ERROR);
include_once('settings.conf');
include_once('snc/S2_DBT.php');

class GEM_Dataset extends DBT {
   protected $tablename = 'GEM_Dataset';
   protected $displayname = 'Dataset Metadata';
   protected $user;
   protected $view = '[
      {
         "Name":"Geography",
         "Data": [
            {
               "Table":"GEM_Potential_Geographic_Scope"
            },
            {
               "Table":"GEM_Actual_Geographic_Extent"
            }
         ]
      },
      {
         "Name":"Temporal",
         "Data": [
            {
               "Table":"GEM_Dataset_Temporal_Extent"
            }
         ]
      },
      {
         "Name":"History",
         "Data": [
            {
               "Table":"GEM_Dataset_Revision"
            },
            {
               "Name":"Digital Files",
               "Table":"GEM_Dataset_File"
            }
         ]
      },
      {
         "Name":"Quality",
         "Data": [
            {
               "Table":"GEM_Dataset_Lineage"
            },
            {
               "Table":"GEM_Data_Collection"
            },
            {
               "Table":"GEM_Data_Validation"
            },
            {
               "Table":"GEM_Data_Verification"
            },
            {
               "Table":"GEM_Record_Pending_Verification"
            }
         ]
      },
      {
         "Name":"Access",
         "Data": [
            {
               "Table":"GEM_Dataset_Conditions"
            },
            {
               "Table":"GEM_Dataset_Public_Access"
            }
         ]
      },
      {
         "Name":"Ownership",
         "Data": [
            {
               "Table":"GEM_Dataset_Role"
            },
            {  
               "Table":"GEM_Dataset_Agreement"
            }
         ]
      },
      {
         "Name":"Review",
         "Data": [
            {
               "Table":"GEM_Meta_Data_Review"
            }
         ]
      },
      {
         "Name":"Conformity",
         "Data": [
            {
               "Table":"GEM_Dataset_Conformity"
            }
         ]
      }
   ]';
   protected $columns = array(
      'GEM_Dataset_ID'              => 'Unique Key',
      'Title'                       => 'Short Text',
      'Alternative_Titles'          => '{"DataType":"LINKEDTOM","TargetType":"GEM_Alternative_Title","TargetField":"GEM_Alternative_Title_ID","Property":"Alternative_Titles"}',
      'Abstract'                    => 'Long Text',
      'Resource_Type'               => '{"DataType":"LINKEDTO","TargetType":"GEM_Resource_Type","TargetField":"GEM_Resource_Type_ID","Mandatory":0,"Current":1}',
      'GEM_Data_Supplier_ID'        => '{"DataType":"LINKEDTO","TargetType":"GEM_Data_Supplier","TargetField":"GEM_Data_Supplier_ID","Mandatory":0,"Current":1,"Derives":[{"Column":"File_Code","Method":"s2_nextfilecode","Params":[{"Type":"Static","Value":"GEM_Dataset"},{"Type":"Static","Value":"File_Code"},{"Type":"Column","Value":"GEM_Data_Supplier_ID","IType":"GEM_Data_Supplier","Property":"File_Code"},{"Type":"Column","Value":"Title","For":1}]}]}',
      'File_Code'                   => '{"DataType":"Short Text","ReadOnly":1}',
      'UUID'                        => '{"DataType":"UUID","ReadOnly":1,"NoList":1}',
      'Unique_Resource_Identifiers' => '{"DataType":"LINKEDTOM","TargetType":"GEM_Unique_Resource_Identifier","TargetField":"GEM_Unique_Resource_Identifier_ID","Property":"Resource_IDs"}',
      'Coupled_Resources'           => '{"DataType":"LINKEDTOM","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Property":"Linked_Resources"}',
      'Topic_Categories'            => '{"DataType":"LINKEDTOM","TargetType":"GEM_Topic_Category","TargetField":"GEM_Topic_Category_ID","Property":"Resource_Categories"}',
      'Keywords'                    => '{"DataType":"LINKEDTOM","TargetType":"GEM_Keyword","TargetField":"GEM_Keyword_ID","Property":"Resource_Categories"}'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $defaultpermissions = array(
      'USR_Permissions' => '
"User_Id","Domain","Label","Value"
1,"metadata","Registered User",1
1,"metadata","System Administrator",1',
      'USR_Default_Permissions' => '
"Domain","Label","Description","Value","Anon" 
"metadata","System Administrator","Full control",0,0
"metadata","Registered User","View unprotected species data",1,0'   
   );
}
class GEM_Data_Supplier extends DBT {
   protected $tablename = 'GEM_Data_Supplier';
   protected $displayname = 'Data Supplier';
   protected $columns = array(
      'GEM_Data_Supplier_ID'  => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text","Derives":[{"Column":"File_Code","Method":"s2_nextfilecode","Params":[{"Type":"Static","Value":"GEM_Data_Supplier"},{"Type":"Static","Value":"File_Code"},{"Type":"Value","For":1}]}]}',
      'Contact_Details'       => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"],"NoList":1}',
      'File_Code'             => '{"DataType":"Short Text","ReadOnly":1}',
      'Description'           => 'Long Text',
      'Is_Public'             => 'True or False',
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}
class GEM_Alternative_Title extends DBT {
   protected $tablename = 'GEM_Alternative_Title';
   protected $displayname = 'Alternative Title';
   protected $columns = array(
      'GEM_Alternative_Title_ID' => 'Unique Key',
      'Datasets'                 => '{"DataType":"LINKEDTOM","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Property":"Alternative_Titles"}',
      'Title'                    => '{"DataType":"Short Text","Mandatory":1}'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      return $this->derivename(array('Title')); 
   }
}
class GEM_Resource_Type extends DBT {
   protected $tablename = 'GEM_Resource_Type';
   protected $displayname = 'Resource Type';
   protected $columns = array(
      'GEM_Resource_Type_ID'  => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text","Mandatory":1}',
      'Code'                  => 'Short Text',
      'Description'           => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GEM_Resource_Type' => '
"Name","Code","Description"
"Spatial data set series","series",""
"Spatial data set","dataset",""
"Spatial data services","services",""'
   );   
}
class GEM_Resource_Locator extends DBT {
   protected $tablename = 'GEM_Resource_Locator';
   protected $displayname = 'Resource Locator';
   protected $columns = array(
      'GEM_Resource_Locator_ID'  => 'Unique Key',
      'GEM_Dataset_ID'           => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":0,"Current":1}',
      'URL'                      => '{"DataType":"URL","Mandatory":1}',
      'Description'              => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'Species Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}
class GEM_Species extends DBT {
   protected $tablename = 'GEM_Species';
   protected $displayname = 'Species';
   protected $columns = array(
      'GEM_Species_ID'  => 'Unique Key',
      'Latin_Name'      => '{"DataType":"Short Text","Interface":"Taxa","Use":"Name"}',
      'Common_Name'     => '{"DataType":"Short Text","ReadOnly":true}',
      'Authority'       => '{"DataType":"Short Text","ReadOnly":true,"NoList":true}',
      'Rank'            => '{"DataType":"Short Text","ReadOnly":true}',
      'TVK'             => '{"DataType":"Short Text","ReadOnly":true,"NoList":true}',
      'EOL_Taxa'        => '{"DataType":"Number","ReadOnly":true,"NoList":true}',
      'EOL_Page'        => '{"DataType":"Number","ReadOnly":true,"NoList":true}'
   );
   protected $domain = 'metadata';
   protected $permissions = array('Delete'=>'NO-ONE');
   protected $withtaxonomy = true;
   protected $ranks = array('kingdom','division','phylum','class','order','family','genus','species');
   function insert() {
      // allow population of json data from Taxa interface.
      if (preg_match('/^\{/',$this->data['Latin_Name'])) {
         $nbntaxa = json_decode($this->data['Latin_Name']);
         $latin = $nbntaxa->Name;
         $tvk = $nbntaxa->Value;
         
      } else $latin = $this->data['Latin_Name'];
      // deal with NHM Bombus (Bombus) terrestris style names
      $latin = preg_replace('/\s*\([^\)]+\)\s*/',' ',$latin);
      $trank = $this->data['Rank'];
      //$tdata = $this->data;
      $this->loaddata($latin,'Latin_Name');
      
      // check if species already exists in database using latin name
      if (!isset($this->id)) {
         $this->data['Latin_Name'] = $latin;
         $this->data['TVK'] = (isset($tvk))?$tvk:null;
         $this->data['Rank'] = $trank;
         parent::insert();
      }
      $tx = new GEM_Taxonomy();
      if (!$tx->exists()) $tx->create();
      $tx->loaddata($this->id,'Recorded_As');
      $xid = $tx->getid();
      if (!isset($xid) && $this->withtaxonomy) {
         // use nbn api and eol api to populate other fields from latin name
         include_once('eol/eolapi.php');
         $pgs = eolgetpagesforspecies($latin);
         if (isset($pgs)) {
            list($pg1,$tx1) = eolgettaxonidforpages($pgs);
            if (isset($tx1)) {
               $sh = eolgethierarchyfortaxa($tx1,$latin);
               $vn = eolgetvernacularname($latin,$sh);
               $this->data['Common_Name'] = $vn;
               $this->data['EOL_Taxa'] = $tx1;
               $this->data['EOL_Page'] = $pg1;
               $species_ranks = eolgetranklist($tx1,$latin);
               $rdata = array();
               $t = null;
               foreach($species_ranks as $rank => $lname) {
                  if ($latin == $lname) {
                     $this->data['Rank'] = $rank;
                     $this->data['Latin_Name'] = $latin;
                     $this->data['TVK'] = $tvk;
                     $this->update($this->getpk(),$this->getid());
                     $tid = $this->getid();
                     switch($rank) {
                     case 'phylum':   $rdata['Rank_Phylum_or_Division'] = $tid; break;
                     case 'division':  $rdata['Rank_Phylum_or_Division'] = $tid; break;
                     case 'subspecies':   break; // ignore subspecies 
                     case 'infraspecies': break; // ignore subspecies 
                     case 'variety':      break; // ignore subspecies 
                     case '':             break; // ignore missing rank 
                     default:          $rdata['Rank_'.ucwords($rank)] = $tid; break;                
                     }
                     $rdata['Recorded_As'] = $tid;
                  } else {
                     $t = new GEM_Species();
                     $td = array('Latin_Name' => $lname,'Rank' => $rank);
                     $t->setdata($td);
                     $t->inctaxonomy(false);
                     $tid = $t->insert();
                     switch($rank) {
                     case 'phylum':   $rdata['Rank_Phylum_or_Division'] = $tid; break;
                     case 'division':  $rdata['Rank_Phylum_or_Division'] = $tid; break;
                     case 'subspecies':   break; // ignore subspecies 
                     case 'infraspecies': break; // ignore subspecies 
                     case 'variety':      break; // ignore subspecies 
                     case '':             break; // ignore missing rank 
                     default:          $rdata['Rank_'.ucwords($rank)] = $tid; break;                
                     } 
                  }
               }
               $tx = new GEM_Taxonomy();
               $tx->setdata($rdata);
               $tx->insert();
            }
         }
      } else {
         $this->data['TVK'] = $tvk;
         $this->update($this->getpk(),$this->getid());
      }
      return $this->getid();      
   }
   function inctaxonomy($val) {
      $this->withtaxonomy = $val;
   }
   function delete() {
      // delete any references to relevant rank from taxonomy 
      // if not specific level then entries can't be deleted as other specific 
      // entries may rely on current
      // alternatively - just refuse to delete altogether.
      return 0;  
   }
   function getname() {
      $common = $this->data['Common_Name'];
      $latin = $this->data['Latin_Name'];
      return (isset($common) && $common != "")?"$common ($latin)":$latin; 
   }
}
class GEM_Taxonomy extends DBT {
   protected $tablename = 'PUB_Taxonomy';
   protected $displayname = 'Taxonomy';
   protected $alts = array('Contributors' => 'PUB_Contributor','Events' => 'PUB_Survey','Species' => 'PUB_Species');
   protected $columns = array(
      'GEM_Taxonomy_ID'             => 'Unique Key',
      'Recorded_As'                 => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}', 
      'Rank_Kingdom'                => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","NoList":1}',
      'Rank_Phylum_or_Division'     => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","NoList":1}',
      'Rank_Class'                  => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","NoList":1}',
      'Rank_Order'                  => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'Rank_Family'                 => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'Rank_Genus'                  => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'Rank_Species'                => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}'
   );
   protected $domain = 'metadata';
   protected $permissions = array('Delete'=>'NO-ONE');
}

class GEM_Unique_Resource_Identifier extends DBT {
   protected $tablename = 'GEM_Unique_Resource_Identifier';
   protected $displayname = 'Unique Resource Identifier';
   protected $columns = array(
      'GEM_Unique_Resource_Identifier_ID' => 'Unique Key',
      'Datasets'                          => '{"DataType":"LINKEDTOM","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Property":"Resource_IDs"}',
      'Unique_ID'                         => '{"DataType":"Short Text","Mandatory":1}',
      'Description'                       => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      return $this->derivename(array('Unique_ID')); 
   }
}

class GEM_Resource_Language extends DBT {
   protected $tablename = 'GEM_Resource_Language';
   protected $displayname = 'Resource Language';
   protected $columns = array(
      'GEM_Resource_Language_ID' => 'Unique Key',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Code'                     => '{"DataType":"Short Text","Mandatory":1}'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GEM_Resource_Language' => '
"Name","Code","Description"
"English","eng"
"Welsh","cym"
"Gaelic (Irish)","gle"
"Gaelic (Scottish)","gla"
"Cornish","cor"
"Ulster Scots","sco"'
   );   
}
class GEM_Topic_Category extends DBT {
   protected $tablename = 'GEM_Topic_Category';
   protected $displayname = 'Topic Category';
   protected $columns = array(
      'GEM_Topic_Category_ID' => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text","Mandatory":1}',
      'Code'                  => 'Short Text',
      'Description'           => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GEM_Topic_Category' => '
"Name","Code","Description"
"farming","001","rearing of animals and/or cultivation of plants Examples: agriculture, plantations, herding, pests and diseases affecting crops and livestock."
"biota","002","flora and/or fauna in natural environment Examples: wildlife, vegetation, biological sciences, ecology, sea-life, habitat"
"boundaries","003","legal land descriptions Examples: political and administrative boundaries"
"climatology Meteorology Atmosphere","004","processes and phenomena of the atmosphere Examples: weather, climate, atmospheric conditions"
"economy","005","economic activities, conditions and employment Examples: production, labour, revenue, commerce, industry"
"elevation","006","height above or below sea level Examples: altitude, bathymetry, digital elevation models, slope"
"environment","007","environmental resources, protection and conservation Examples: environmental pollution, waste storage and treatment, environmental impact assessment, monitoring environmental risk, nature reserves, landscape"
"geoscientificInformation","008","information pertaining to earth sciences Examples: geophysical features and processes, geology, minerals, soils"
"health","009","health, health services, human ecology, and safety Examples: disease and illness, factors affecting health, health services"
"imageryBasemapsEarthCover","010","base maps Examples: land cover, topographic maps, imagery, unclassified images"
"intelligenceMilitary","011","military bases, structures, activities Examples: barracks, training grounds, military transportation"
"inlandWaters","012","inland water features, drainage systems and their characteristics Examples: rivers, salt lakes, dams, floods, water quality, hydrographic charts"
"location","013","positional information and services Examples: addresses, geodetic networks, control points, postal zones and services, place names"
"oceans","014","features and characteristics of salt water bodies (excluding inland waters) Examples: tides, tidal waves, coastal information, reefs"
"planningCadastre","015","information used for appropriate actions for future use of the land Examples: land use maps, zoning maps, cadastral surveys, land ownership"
"society","016","characteristics of society and cultures Examples: settlements, anthropology, archaeology, education, demographic data, recreational areas and activities, social impact assessments, crime and justice, census information"
"structure","017","man-made construction Examples: buildings, museums, churches, factories, housing, monuments, shops"
"transportation","018","means and aids for conveying persons and/or goods Examples: roads, airports/airstrips, shipping routes, tunnels, nautical charts, vehicle or vessel location, aeronautical charts, railways"
"utilitiesCommunication","019","energy, water and waste systems and communications infrastructure and services Examples: sources of energy, water purification and distribution, sewage collection and disposal, electricity and gas distribution, data communication, telecommunication, radio"'
   );   
}
/*
Geographic coverage and spatial service type

Spatial Service Type
   Discovery Service (discovery)
   View Service (view)
   Download Service (download)
   Transformation Service (transformation)
   Invoke Spatial Data Service (invoke)
   Other Service (other)

Boundaries (x) associated with types
Bounding box calculated from boundary geographies. 

Calculated bounding box of data from Recorder?

Geographic Coverage
*/
class GEM_Potential_Geographic_Scope extends DBT {
   protected $tablename = 'GEM_Potential_Geographic_Scope';
   protected $displayname = 'Potential Geographic Scope';
   protected $user;
   protected $columns = array(
      'GEM_Potential_Geographic_Scope_ID' => 'Unique Key',
      'GEM_Dataset_ID'                    => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":0,"Current":1}',
      'GEM_Known_Site_ID'                 => '{"DataType":"LINKEDTO","TargetType":"GEM_Known_Site","TargetField":"GEM_Known_Site_ID"}'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
}
class GEM_Known_Site extends DBT {
   protected $tablename = 'GEM_Known_Site';
   protected $displayname = 'Known Site';
   protected $user;
   protected $columns = array(
      'GEM_Known_Site_ID'        => 'Unique Key',
      'GEM_Known_Site_Type_ID'   => '{"DataType":"LINKEDTO","TargetType":"GEM_Known_Site_Type","TargetField":"GEM_Known_Site_Type_ID","Property":"Resource_IDs"}',
      'Name'                     => 'Short Text',
      'Reference'                => 'Short Text',
      'Boundary'                 => '{"DataType":"Map Data"}'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
}
class GEM_Known_Site_Type extends DBT {
   protected $tablename = 'GEM_Known_Site_Type';
   protected $displayname = 'Known Site Type';
   protected $columns = array(
      'GEM_Known_Site_Type_ID'   => 'Unique Key',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Code'                     => 'Short Text',
      'Description'              => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GEM_Resource_Type' => '
"Name","Code","Description"
"Administrative Area","administrative",""
"Vice County","vicecounty",""
"LRC coverage","lrc",""
"Society Recording Area","society",""
"Nature Improvement Area","nia",""
"Local Nature Partnership","lnp",""'
   );   
}
class GEM_Actual_Geographic_Extent extends DBT {
   protected $tablename = 'GEM_Actual_Geographic_Extent';
   protected $displayname = 'Actual Geographic Extent';
   protected $user;
   protected $columns = array(
      'GEM_Actual_Geographic_Extent_ID'   => 'Unique Key',
      'GEM_Dataset_ID'                    => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":0,"Current":1}',
      'North' => 'Decimal',
      'South' => 'Decimal',
      'East'  => 'Decimal',
      'West'  => 'Decimal'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
}

/*
Temporal constraints 
Date of publication
Date of last revision = Greatest mod date
Date of creation = Least mod date

Records from
Records to
*/
class GEM_Dataset_Temporal_Extent extends DBT {
   protected $tablename = 'GEM_Dataset_Temporal_Extent';
   protected $displayname = 'Temporal Extent';
   protected $user;
   protected $columns = array(
      'GEM_Dataset_Temporal_Extent_ID' => 'Unique Key',
      'GEM_Dataset_ID'                 => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":0,"Current":1}',
      'Date_Of_Publication'            => 'Date',
      'Date_Of_Last_Revision'          => 'Date',
      'Date_Of_Creation'               => 'Date',
      'Data_From'                      => 'Date',
      'Data_Until'                     => 'Date'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
}


class GEM_Keyword_Category extends DBT {
   protected $tablename = 'GEM_Keyword_Category';
   protected $displayname = 'Keyword Category';
   protected $columns = array(
      'GEM_Keyword_Category_ID'  => 'Unique Key',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Code'                     => 'Short Text',
      'Description'              => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}
class GEM_Keyword extends DBT {
   protected $tablename = 'GEM_Keyword';
   protected $displayname = 'Keyword';
   protected $columns = array(
      'GEM_Keyword_ID'           => 'Unique Key',
      'GEM_Keyword_Category_ID'  => '{"DataType":"LINKEDTO","TargetType":"GEM_Keyword_Category","TargetField":"GEM_Keyword_Category_ID","Mandatory":0,"Current":1}',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}

class GEM_Dataset_Lineage extends DBT {
   protected $tablename = 'GEM_Dataset_Lineage';
   protected $displayname = 'Lineage';
   protected $columns = array(
      'GEM_Dataset_Lineage_ID'   => 'Unique Key',
      'GEM_Dataset_ID'           => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":0,"Current":1}',
      'Statement'                => 'Long Text'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}

class GEM_Dataset_Revision extends DBT {
   protected $tablename = 'GEM_Dataset_Revision';
   protected $displayname = 'Revision';
   protected $columns = array(
      'GEM_Dataset_Revision_ID'  => 'Unique Key',
      'GEM_Dataset_ID'           => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":0,"Current":1}',
      'GEM_Revision_Type_ID'     => '{"DataType":"LINKEDTO","TargetType":"GEM_Revision_Type","TargetField":"GEM_Revision_Type_ID","Mandatory":0,"Current":1}',
      'Received'                 => 'Date',
      'Loaded'                   => 'Date',
      'Files'                    => '{"DataType":"LINKEDTOM","TargetType":"GEM_Dataset_File","TargetField":"GEM_Dataset_File_ID","Property":"Dataset_Files"}'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}
class GEM_Revision_Type extends DBT {
   protected $tablename = 'GEM_Revision_Type';
   protected $displayname = 'Revision Type';
   protected $columns = array(
      'GEM_Revision_Type_ID'  => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text","Mandatory":1}',
      'Code'                  => 'Short Text',
      'Description'           => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GEM_Revision_Type' => '
"Name","Code","Description"
"Addition","add","New data received to be added to existing dataset."
"Replacement","rep","New data received which replaces existing copy of dataset."
"Correction","cor","Additional data received which represents corrections to data in the existing dataset"'
   );   
}

class GEM_Dataset_Conformity extends DBT {
   protected $tablename = 'GEM_Dataset_Conformity';
   protected $displayname = 'Conformity';
   protected $columns = array(
      'GEM_Dataset_Conformity_ID'   => 'Unique Key',
      'GEM_Dataset_ID'              => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":0,"Current":1}',
      'GEM_Specification_ID'        => '{"DataType":"LINKEDTO","TargetType":"GEM_Specification","TargetField":"GEM_Specification_ID","Mandatory":0,"Current":1}',
      'GEM_Degree_ID'               => '{"DataType":"LINKEDTO","TargetType":"GEM_Degree","TargetField":"GEM_Degree_ID","Mandatory":0,"Current":1}',
      'Comments'                    => 'Long Text'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}
class GEM_Specification extends DBT {
   protected $tablename = 'GEM_Specification';
   protected $displayname = 'Specification';
   protected $columns = array(
      'GEM_Specification_ID'  => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text","Mandatory":1}',
      'Code'                  => 'Short Text',
      'Description'           => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GEM_Specification' => '
"Name","Code","Description"
"UK Gemini","gem",""
"INSPIRE","ins,""'
   );   
}
class GEM_Degree extends DBT {
   protected $tablename = 'GEM_Degree';
   protected $displayname = 'Degree';
   protected $columns = array(
      'GEM_Degree_ID'         => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text","Mandatory":1}',
      'Code'                  => 'Short Text',
      'Description'           => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GEM_Degree' => '
"Name","Code","Description"
"Partial","par",""
"Complete","com",""'
   );   
}


class GEM_Dataset_Conditions extends DBT {
   protected $tablename = 'GEM_Dataset_Conditions';
   protected $displayname = 'Conditions applying to access and use';
   protected $columns = array(
      'GEM_Dataset_Conditions_ID'   => 'Unique Key',
      'GEM_Dataset_ID'              => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":0,"Current":1}',
      'Statement'                   => 'Long Text'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}
/* 
   Custodianship constraints
*/
class GEM_Dataset_Public_Access extends DBT {
   protected $tablename = 'GEM_Dataset_Public_Access';
   protected $displayname = 'Limitations on public access';
   protected $columns = array(
      'GEM_Dataset_Public_Access_ID'   => 'Unique Key',
      'GEM_Dataset_ID'                 => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":0,"Current":1}',
      'Statement'                      => 'Long Text'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}
/*
   EIR exemptions
*/
class GEM_Dataset_Role extends DBT {
   protected $tablename = 'GEM_Dataset_Role';
   protected $displayname = 'Dataset Role';
   protected $columns = array(
      'GEM_Dataset_Role_ID'   => 'Unique Key',
      'GEM_Dataset_ID'        => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":0,"Current":1}',
      'GEM_Role_Type_ID'      => '{"DataType":"LINKEDTO","TargetType":"GEM_Role_Type","TargetField":"GEM_Role_Type_ID","Mandatory":0,"Current":1}',
      'Contact'               => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"]}',
      'Notes'                 => 'Long Text'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}

class GEM_Role_Type extends DBT {
   protected $tablename = 'GEM_Role_Type';
   protected $displayname = 'Role Type';
   protected $columns = array(
      'GEM_Role_Type_ID'         => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text","Mandatory":1}',
      'Code'                  => 'Short Text',
      'Description'           => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GEM_Role_Type' => '
"Code","Name","Definition"
"1","Resource","provider party that supplies the resource"
"2","Custodian","party that accepts accountability and responsibility for the data and ensures appropriate care and maintenance of the resource"
"3","Owner","party that owns the resource"
"4","User","party who uses the resource"
"5","Distributor","party who distributes the resource"
"6","Originator","party who created the resource"
"7","Point of Contact","party who can be contacted for acquiring knowledge about or acquisition of the resource"
"8","Principle Investigator","key party responsible for gathering information about or acquisition of the resource"
"9","Processor","party who has processed the data in a manner such that the resource has been modified"
"10","Publisher","party who published the resource"
"11","Author","party who authored the resource"'
   );   
}

class GEM_Dataset_File extends DBT {
   protected $tablename = 'GEM_Dataset_File';
   protected $displayname = 'Dataset File';
   protected $columns = array(
      'GEM_Dataset_File_ID'      => 'Unique Key',
      'GEM_Dataset_ID'           => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":1,"Current":1}',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1,"Derives":[{"Column":"File_Code","Method":"s2_nextfilecode","Params":[{"Type":"Static","Value":"GEM_Dataset_File"},{"Type":"Static","Value":"File_Code"},{"Type":"Column","Value":"GEM_Dataset_ID","IType":"GEM_Dataset","Property":"File_Code"},{"Type":"Value","For":1}]}]}',
      'File_Code'                => '{"DataType":"Short Text","ReadOnly":1}',
      'Received'                 => '{"DataType":"Date","Mandatory":0}',
      'GEM_Physical_Format_ID'   => '{"DataType":"LINKEDTO","TargetType":"GEM_Physical_Format","TargetField":"GEM_Physical_Format_ID","Mandatory":0}',
      'GEM_Digital_Format_ID'    => '{"DataType":"LINKEDTO","TargetType":"GEM_Digital_Format","TargetField":"GEM_Digital_Format_ID","Mandatory":0}',
      'Location'                 => 'Long Text',
      'File'                     => '{"DataType":"File Path","NoList":1,"Mandatory":0}'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}

class GEM_Physical_Format extends DBT {
   protected $tablename = 'GEM_Physical_Format';
   protected $displayname = 'Physical Format';
   protected $columns = array(
      'GEM_Physical_Format_ID'   => 'Unique Key',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Description'              => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GEM_Physical_Format' => '
"Name","Description"
"Book",""
"Bound Copy",""
"Paper Document",""
"CD","",
"DVD",""
"Hard Disk Drive",""'
   );   
}
class GEM_Digital_Format extends DBT {
   protected $tablename = 'GEM_Digital_Format';
   protected $displayname = 'Digital Format';
   protected $columns = array(
      'GEM_Digital_Format_ID'    => 'Unique Key',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Description'              => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GEM_Digital_Format' => '
"Name","Description"
"PDF","Adobe PDF original document"
"PDF (scanned)","Adobe PDF original document"
"Image","Image file in standard format"
"Word Doc(x)","Microsoft Word .doc or .docx file"
"Excel(x)","Microsoft Excel .xls or .xlsx file"
"GIS (MapInfo)","TAB or MIF file format",
"GIS (ESRI)","SHP or E000 file"
"CSV","Comma-separated values"
"Tabbed","Tab separated values"'
   );   
}

class GEM_Meta_Data_Review extends DBT {
   protected $tablename = 'GEM_Meta_Data_Review';
   protected $displayname = 'Meta Data Review';
   protected $columns = array(
      'GEM_Meta_Data_Review_ID'  => 'Unique Key',
      'GEM_Dataset_ID'           => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":0,"Current":1}',
      'GEM_Review_Status_ID'     => '{"DataType":"LINKEDTO","TargetType":"GEM_Review_Status","TargetField":"GEM_Review_Status_ID","Mandatory":0,"Current":1}',
      'Reviewed'                 => 'Date',
      'Description'              => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}

class GEM_Review_Status extends DBT {
   protected $tablename = 'GEM_Review_Status';
   protected $displayname = 'Review Status';
   protected $columns = array(
      'GEM_Review_Status_ID'     => 'Unique Key',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Description'              => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GEM_Review_Status' => '
"Name","Definition"
"No Change",""
"Update Needed",""
"Update Completed",""
"To Be Deleted",""'
   );   
}

class GEM_Data_Collection extends DBT {
   protected $tablename = 'GEM_Data_Collection';
   protected $displayname = 'Data Collection';
   protected $columns = array(
      'GEM_Data_Collection_ID'      => 'Unique Key',
      'GEM_Dataset_ID'              => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":0,"Current":1}',
      'Purpose'                     => 'Long Text',
      'GEM_Survey_Methodology_ID'   => '{"DataType":"LINKEDTO","TargetType":"GEM_Survey_Methodology","TargetField":"GEM_Survey_Methodology_ID","Mandatory":0,"Current":1}',
      'Rigour'                      => 'True or False',
      'Casual'                      => 'True or False',
      'Amateur'                     => 'True or False',
      'Professional'                => 'True or False',
      'Comments'                    => 'Long Text'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}

class GEM_Survey_Methodology extends DBT {
   protected $tablename = 'GEM_Survey_Methodology';
   protected $displayname = 'Survey_Methodology';
   protected $columns = array(
      'GEM_Survey_Methodology_ID'   => 'Unique Key',
      'Name'                        => '{"DataType":"Short Text","Mandatory":1}',
      'Description'                 => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GEM_Review_Status' => '
"Name","Definition"
"Casual Records","General type for all non-scientifically recorded data."
"Phase 1",""
"NVC",""
"NVC Rapid",""
"Transect",""
"HSI",""'
   );   
}

class GEM_Data_Validation extends DBT {
   protected $tablename = 'GEM_Data_Validation';
   protected $displayname = 'Data Validation';
   protected $columns = array(
      'GEM_Data_Validation_ID'   => 'Unique Key',
      'GEM_Dataset_ID'           => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":0,"Current":1}',
      'Updated'                  => 'Date',
      'GEM_Validation_Status_ID' => '{"DataType":"LINKEDTO","TargetType":"GEM_Validation_Status","TargetField":"GEM_Validation_Status_ID","Mandatory":0,"Current":1}', 
      'Comments'                 => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}
class GEM_Validation_Status extends DBT {
   protected $tablename = 'GEM_Validation_Status';
   protected $displayname = 'Validation Status';
   protected $columns = array(
      'GEM_Validation_Status_ID' => 'Unique Key',
      'Number'                   => 'Number',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Description'              => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GEM_Validation_Status' => '
"Number","Name","Description"
1,"Location",""
2,"Species",""
3,"Recorder",""
4,"Date",""'
   );   
}

class GEM_Data_Verification extends DBT {
   protected $tablename = 'GEM_Data_Verification';
   protected $displayname = 'Data Verification';
   protected $columns = array(
      'GEM_Data_Verification_ID'    => 'Unique Key',
      'GEM_Dataset_ID'              => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":0,"Current":1}',
      'Updated'                     => 'Date',
      'GEM_Verification_Status_ID'  => '{"DataType":"LINKEDTO","TargetType":"GEM_Verification_Status","TargetField":"GEM_Verification_Status_ID","Mandatory":0,"Current":1}', 
      'Comments'                    => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}

class GEM_Verification_Status extends DBT {
   protected $tablename = 'GEM_Verification_Status';
   protected $displayname = 'Verification Status';
   protected $columns = array(
      'GEM_Verification_Status_ID'  => 'Unique Key',
      'Name'                        => '{"DataType":"Short Text","Mandatory":1}',
      'Description'                 => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GEM_Verification_Status' => '
"Name","Description"
"Correct",""
"Considered correct",""
"Requires confirmation",""
"Considered incorrect",""
"Incorrect",""
"Unchecked",""'
   );
}

class GEM_Dataset_Spatial_Resolution extends DBT {
   protected $tablename = 'GEM_Dataset_Spatial_Resolution';
   protected $displayname = 'Spatial Resolution';
   protected $columns = array(
      'GEM_Dataset_Spatial_Resolution_ID' => 'Unique Key',
      'GEM_Dataset_ID'                    => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":0,"Current":1}',
      'GEM_Spatial_Type_ID'               => '{"DataType":"LINKEDTO","TargetType":"GEM_Spatial_Type","TargetField":"GEM_Spatial_Type_ID","Mandatory":0,"Current":1}',
      'GEM_Spatial_Resolution_Type_ID'    => '{"DataType":"LINKEDTO","TargetType":"GEM_Spatial_Resolution_Type","TargetField":"GEM_Spatial_Resolution_Type_ID","Mandatory":0,"Current":1}',
      'Statement'                         => 'Long Text'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );   
}

class GEM_Spatial_Type extends DBT {
   protected $tablename = 'GEM_Spatial_Type';
   protected $displayname = 'Spatial Type';
   protected $columns = array(
      'GEM_Spatial_Type_ID'   => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text","Mandatory":1}',
      'Description'           => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GEM_Spatial_Type' => '
"Name","Definition"
"Raster",""
"Polygon",""
"Point",""
"Line",""
"Square",""
"Grid",""'
   );   
}
class GEM_Spatial_Resolution_Type extends DBT {
   protected $tablename = 'GEM_Spatial_Resolution_Type';
   protected $displayname = 'Spatial Resolution Type';
   protected $columns = array(
      'GEM_Spatial_Resolution_Type_ID' => 'Unique Key',
      'Name'                           => '{"DataType":"Short Text","Mandatory":1}',
      'Description'                    => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GEM_Spatial_Resolution_Type' => '
"Name","Definition"
"GPS",""
"10m",""
"100m",""
"1km",""
"2km",""
"10km",""
"100km",""
"Freehand",""
"Snapped OS MasterMap",""
"Snapped Other Vector",""'
   );   
}


/*
   verification queried records   
*/
class GEM_Record_Pending_Verification extends DBT {
   protected $tablename = 'GEM_Record_Pending_Verification';
   protected $displayname = 'Record Pending Verification';
   protected $columns = array(
      'GEM_Record_Pending_Verification_ID'   => 'Unique Key',
      'GEM_Dataset_ID'                       => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":0,"Current":1}',
      'GEM_Verification_Query_Type_ID'       => '{"DataType":"LINKEDTO","TargetType":"GEM_Verification_Query_Type","TargetField":"GEM_Verification_Query_Type_ID","Mandatory":0,"Current":1}',
      'Record_Key'                           => 'Short Text',
      'Referred_To'                          => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"]}',
      'Identified'                           => 'Date',
      'Referred'                             => 'Date',
      'Received'                             => 'Date',
      'GEM_Verification_Response_Type_ID'    => '{"DataType":"LINKEDTO","TargetType":"GEM_Verification_Response_Type","TargetField":"GEM_Verification_Response_Type_ID","Mandatory":0,"Current":1}',
      'Notes'                                => 'Long Text'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}
class GEM_Verification_Query_Type extends DBT {
   protected $tablename = 'GEM_Verification_Query_Type';
   protected $displayname = 'Verification Query Type';
   protected $columns = array(
      'GEM_Verification_Query_Type_ID'    => 'Unique Key',
      'Name'                              => '{"DataType":"Short Text","Mandatory":1}',
      'Description'                       => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}
class GEM_Verification_Response_Type extends DBT {
   protected $tablename = 'GEM_Verification_Response_Type';
   protected $displayname = 'Verification Response Type';
   protected $columns = array(
      'GEM_Verification_Response_Type_ID' => 'Unique Key',
      'Name'                              => '{"DataType":"Short Text","Mandatory":1}',
      'Description'                       => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}

class GEM_Dataset_Agreement extends DBT {
   protected $tablename = 'GEM_Dataset_Agreement';
   protected $displayname = 'Dataset Agreement';
   protected $show = array('GEM_Agreement_Release_To');
   protected $columns = array(
      'GEM_Dataset_Agreement_ID'    => 'Unique Key',
      'GEM_Dataset_ID'              => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset","TargetField":"GEM_Dataset_ID","Mandatory":0,"Current":1}',
      'GEM_Agreement_Type_ID'       => '{"DataType":"LINKEDTO","TargetType":"GEM_Agreement_Type","TargetField":"GEM_Agreement_Type_ID","Mandatory":0,"Current":1}',
      'Signatory'                   => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"]}',
      'Applies_From'                => 'Date',
      'Expires'                     => 'Date',
      'Notes'                       => 'Long Text'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}
class GEM_Agreement_Type extends DBT {
   protected $tablename = 'GEM_Agreement_Type';
   protected $displayname = 'Agreement Type';
   protected $columns = array(
      'GEM_Agreement_Type_ID' => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text","Mandatory":1}',
      'Description'           => 'Long Text' 
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GEM_Agreement_Type' => '
"Name","Definition"
"Legacy",""
"Statement",""
"DEA (Individual)",""
"DEA (Organisation)",""'
   );   
}
class GEM_Agreement_Release_To extends DBT {
   protected $tablename = 'GEM_Agreement_Release_To';
   protected $displayname = 'Agreement Release To 3rd Parties';
   protected $columns = array(
      'GEM_Agreement_Release_To_ID' => 'Unique Key',
      'GEM_Dataset_Agreement_ID'    => '{"DataType":"LINKEDTO","TargetType":"GEM_Dataset_Agreement","TargetField":"GEM_Dataset_Agreement_ID","Mandatory":0,"Current":1}',
      'Release_To'                  => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"]}',
      'Notes'                       => 'Long Text'
   );
   protected $domain = 'metadata';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}


?> 
