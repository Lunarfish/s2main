<?php
class G_DBConfig {
   protected $platform;// MySQL, PGSQL etc
   protected $conn;// array of connection string settings
   function G_DBConfig($platform,$conn) {
      $this->platform   = $platform;
      $this->conn       = $conn;
   }
   function getconn() {
      return $this->conn;
   }
   function getplatform() {
      return $this->platform;
   }
}
?>