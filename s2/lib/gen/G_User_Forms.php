<?php 
$G_User_Forms = json_decode(
'{
   "Forms": {      
      "Login": {
         "Action": "Login",
         "Title": "Login",
         "IType": "USR_Users",
         "Fields": [
            {
               "Name": "Email",
               "DisplayName": "Email",
               "Params": {
                  "DataType": "Short Text"
               }
            },
            {
               "Name": "Password",
               "DisplayName": "Password",
               "Params": {
                  "DataType": "Password",
                  "Confirmed": 0
               }
            }
         ],
         "Links": [
            {
               "Name": "GLogin",
               "DisplayName": "Or login via Google Account",
               "URL": "openid/"
            }            
         ]
      },
      "Register": {
         "Action": "Register",
         "Title": "Register",
         "IType": "USR_Users",
         "Fields": [
            {
               "Name": "Name",
               "DisplayName": "Your Name",
               "Params": {
                  "DataType": "Short Text"
               }
            },
            {
               "Name": "Email",
               "DisplayName": "Email",
               "Params":{
                  "DataType": "Short Text"
               }
            },
            {
               "Name": "Google_Account",
               "DisplayName": "Google Account",
               "Params":{
                  "DataType": "Short Text"
               }
            },
            {
               "Name": "Password",
               "DisplayName": "Password",
               "Params":{
                  "DataType": "Password",
                  "Confirmed": 1
               }
            }
         ]
      },
      "Confirm": {
         "Action": "Confirm",
         "Title": "Confirm User",
         "IType": "USR_Users",
         "Fields": [
            {
               "Name": "Token",
               "DisplayName": "Verification ID",
               "Params": {
                  "DataType": "Short Text"
               }
            },
            {
               "Name": "Password",
               "DisplayName": "Password",
               "Params":{
                  "DataType": "Password",
                  "Confirmed": 0
               }
            }
         ]
      },
      "Google_Confirm": {
         "Action": "Google_Confirm",
         "Title": "Confirm Google Account",
         "IType": "USR_Users",
         "Fields": [
            {
               "Name": "Token",
               "DisplayName": "Verification ID",
               "Params": {
                  "DataType": "Short Text"
               }
            },
            {
               "Name": "Password",
               "DisplayName": "Password",
               "Params":{
                  "DataType": "Password",
                  "Confirmed": 0
               }
            }
         ]
      }    
   }
}'
);
/* 
      Have a form type of grid which takes an X and Y array and populates a 
      table with X as top headings, Y as left headings and a grid of check 
      boxes
*/ 
?>