<?php
/* 
   GENERATE MetaData SCHEMA Based on UK GEMINI 2.1
*/

include_once('settings.conf');
include_once('snc/S2_DBT.php');
include_once('eol/eolapi.php');
         
class GN_Taxon_Revision extends DBT {
   protected $tablename = 'GN_Taxon_Revision';
   protected $displayname = 'Taxon Revision';
   protected $perpage = 25;
   protected $user;
   protected $alts = array('Datasets'=>'GN_Dataset','Species Lists'=>'GN_Species_List');
   protected $revises = array('GN_Record'=>'Taxa'); // array of key(table) = value(column)
   protected $columns = array(
      'GN_Taxon_Revision_ID'  => 'Unique Key',
      'Old_Taxa'              => '{"DataType":"Taxon Version Key","Interface":"TVK","Use":"Name","Revise":0}',
      'New_Taxa'              => '{"DataType":"Taxon Version Key","Interface":"TVK","Use":"Name","Revise":0}',
      'Date'                  => 'Date'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $primingdata = array(
   );
   function insert() {
      if (!$this->isduplicate()) parent::insert();
      $otvk = $this->data['Old_Taxa'];
      $ntvk = $this->data['New_Taxa'];
      foreach($this->revises as $type => $col) {
         $dbti = new $type();
         $recs = $dbti->select($col,$otvk);
         foreach($recs as $num => $data) {
            $data[$col] = $otvk;
            $dbti->setdata($data);
            $dbti->addrevision('Taxa',$ntvk,'Taxonomic Revision');
         }  
      } 
   }   
}


class GN_Dataset extends DBT {
   protected $tablename = 'GN_Dataset';
   protected $displayname = 'Species Dataset';
   protected $user;
   protected $alts = array('Records'=>'GN_Record','Species Lists'=>'GN_Species_List','Taxonomy'=>'GN_Taxonomy');
   protected $columns = array(
      'GN_Dataset_ID'   => 'Unique Key',
      'Name'            => 'Short Text',
      'From_Date'       => 'Date',
      'To_Date'         => 'Date',
      'Coverage'        => 'Map Data',
      'Contact'         => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"]}'
   );
   protected $domain = 'species';
      protected $view = '[
      {
         "Name":"Records",
         "Data": [
            {
               "Table":"GN_Record"
            }
         ]
      },
      {
         "Name":"MetaData",
         "Data": [
            {
               "Table":"GN_Dataset_Description"
            },
            {
               "Table":"GN_Dataset_File"
            }            
         ]
      }
   ]'; 
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $primingdata = array(
   );
   protected $defaultpermissions = array(
      'USR_Permissions' => '
"User_Id","Domain","Label","Value"
1,"species","Registered User",1
1,"species","Species Access",1
1,"species","Species Administrator",1
1,"species","System Administrator",1',
      'USR_Default_Permissions' => '
"Domain","Label","Description","Value","Anon" 
"species","System Administrator","Full control",0,0
"species","Species Administrator","Add and edit datasets and records",0,0
"species","Species Access","View all species data",0,0
"species","Registered User","View unprotected species data",1,0'   
   );

}
class GN_Dataset_Description extends DBT {
   protected $tablename = 'GN_Dataset_Description';
   protected $displayname = 'Dataset Description';
   protected $user;
   protected $columns = array(
      'GN_Dataset_Description_ID'     => 'Unique Key',
      'GN_Dataset_ID'    => '{"DataType":"LINKEDTO","TargetType":"GN_Dataset","TargetField":"GN_Dataset_ID","Mandatory":1,"Current":1}',
      'Description'     => 'Long Text'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $primingdata = array(
   );   
}

class GN_Record extends DBT {
   protected $tablename = 'GN_Record';
   protected $displayname = 'Species Record';
   protected $perpage = 25;
   protected $user;
   protected $revision_tables = array('Taxa'=>'GN_Record_Taxon_Revision');
   protected $alts = array('Datasets'=>'GN_Dataset','Species Lists'=>'GN_Species_List');
   protected $show = array('GN_Record_Taxon_Revision');
   
   protected $implements = array('gridify');
   protected $tileby = array('Taxa');
   
   protected $columns = array(
      'GN_Record_ID'    => 'Unique Key',
      'GN_Dataset_ID'   => '{"DataType":"LINKEDTO","TargetType":"GN_Dataset","TargetField":"GN_Dataset_ID","Mandatory":1,"Current":1}',
      'Recorder'        => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person"]}',
      'Taxa'            => '{"DataType":"Taxon Version Key","Interface":"TVK","Use":"Name","Revise":1}',
      'Date'            => 'Date',
      'Location'        => '{"DataType":"Map Data","Derives":[{"Column":"Grid_Reference","Method":"s2_getgr","Params":[{"Type":"Value"}]}]}',
      'Grid_Reference'  => '{"DataType":"Short Text","Derives":[{"Column":"Location","Method":"s2_getgrsquare","Params":[{"Type":"Value"}]}]}'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $primingdata = array(
   );  
   function addrevision($col,$newval,$reason) {
      $id = $this->getid();
      if(isset($this->revision_tables) && isset($this->revision_tables[$col])) {
         $type = $this->revision_tables[$col]; 
         $oldval = $this->data[$col];
         $dbti = new $type();
         $dbti->autorevision($id,$oldval,$newval,$reason);                                 
      }
      //$this->data[$col] = $newval;
      //$this->setdata(array($col => $newval));
      //$this->update($this->getpk(),$id);
   }
   function getname() {
      //return $this->data['Surveyor']->Name;
      return $this->derivename(array('GN_Dataset_ID','GN_Record_ID'));
   }  
}

class GN_Record_Taxon_Revision extends DBT {
   protected $tablename = 'GN_Record_Taxon_Revision';
   protected $displayname = 'Record Taxon Revision';
   protected $perpage = 25;
   protected $user;
   protected $parent = 'GN_Record';
   protected $alts = array('Datasets'=>'GN_Dataset','Species Lists'=>'GN_Species_List');
   protected $columns = array(
      'GN_Record_Taxon_Revision_ID' => 'Unique Key',
      'GN_Record_ID'    => '{"DataType":"LINKEDTO","TargetType":"GN_Record","TargetField":"GN_Record_ID","Mandatory":1,"Current":1}',
      'Date'            => 'Date',
      'Reason'          => '{"DataType":"Short Text"}',
      'New_Taxa'        => '{"DataType":"Taxon Version Key","Interface":"TVK","Use":"Name","Revise":0}',
      'Old_Taxa'        => '{"DataType":"Taxon Version Key","Interface":"TVK","Use":"Name","Revise":0}',
      'Recorder'        => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person"]}'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $primingdata = array(
   );
   function autorevision($rid,$oldval,$newval,$reason) {
      $data = array(
         'GN_Record_ID' => $rid,
         'Old_Taxa'     => $oldval,
         'New_Taxa'     => $newval,
         'Date'         => new Zend_Db_Expr('NOW()'),
         'Reason'       => $reason
      );
      $this->setdata($data);
      return $this->insert();
   } 
   function insert() {
      $inserted = parent::insert();
      if ($inserted) {
         $rec = new GN_Record();
         //$rec->loaddata($this->data['GN_Record_ID']);
         $rec->setdata(array('Taxa'=>$this->data['New_Taxa']));
         $rec->update('GN_Record_ID',$this->data['GN_Record_ID']);
      }
   }  
}


class GN_Dataset_File extends DBT {
   protected $tablename = 'GN_Dataset_File';
   protected $displayname = 'Dataset Files';
   protected $user;
   protected $columns = array(
      'GN_Dataset_File_ID'     => 'Unique Key',
      'GN_Dataset_ID'          => '{"DataType":"LINKEDTO","TargetType":"GN_Dataset","TargetField":"GN_Dataset_ID","Mandatory":1,"Current":1}',
      'File'            => 'File Path'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $primingdata = array(
   );   
} 
class GN_Species_List extends DBT {
   protected $tablename = 'GN_Species_List';
   protected $displayname = 'Species List';
   protected $perpage = 10;
   protected $user;
   protected $alts = array('Datasets'=>'GN_Dataset','Records'=>'GN_Record');
   protected $show = array('GN_List_Member');
   protected $view = '[
      {
         "Name":"List Members",
         "Data": [
            {
               "Table":"GN_List_Member",
               "Show":"All"
            }                                                                                                    
         ]
      }      
   ]';
   protected $columns = array(
      'GN_Species_List_ID'     => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text","Mandatory":1}',
      'Abbreviation'          => '{"DataType":"Short Text","Mandatory":1}',
      'Description'           => 'Long Text',
      'GN_List_Type_ID'        => '{"DataType":"LINKEDTO","TargetType":"GN_List_Type","TargetField":"GN_List_Type_ID","Mandatory":1,"Current":1}',
      'GN_List_Class_ID'       => '{"DataType":"LINKEDTO","TargetType":"GN_List_Class","TargetField":"GN_List_Class_ID","Mandatory":1,"Current":1}',
      'GN_List_Extent_ID'      => '{"DataType":"LINKEDTO","TargetType":"GN_List_Extent","TargetField":"GN_List_Extent_ID","Current":1,"NoList":1}'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Species Administrator'
   );
}

class GN_List_Member extends DBT {
   protected $tablename = 'GN_List_Member';
   protected $displayname = 'List Member';
   protected $perpage = 10;
   protected $user;
   protected $alts = array('Species Lists'=>'GN_Species_List','Datasets'=>'GN_Dataset','Records'=>'GN_Record');
   protected $columns = array(
      'GN_List_Member_ID'         => 'Unique Key',
      'GN_Species_List_ID'        => '{"DataType":"LINKEDTO","TargetType":"GN_Species_List","TargetField":"GN_Species_List_ID","Mandatory":1,"Current":1}',
      'GN_Species_ID'             => '{"DataType":"Taxon Version Key","Mandatory":1}',
      'Score'                    => '{"DataType":"Decimal"}',
      'GN_List_Scoring_Group_ID'  => '{"DataType":"LINKEDTO","TargetType":"GN_List_Scoring_Group","TargetField":"GN_List_Scoring_Group_ID","Current":1}'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Species Administrator'
   );
   function getmembership($type,$value) {
      $membership = array();
      $sl = new GN_Species_List();
      $lists = $sl->select("${type}_ID",$value,false);
      $obj = new stdClass();
      $obj->Lists = $lists;
      foreach($lists as $lx => $list) {
         $abbrev = $list['Abbreviation'];
         $members = $this->select('GN_Species_List_ID',$list['GN_Species_List_ID']);
         foreach($members as $mx => $mem) {
            $tvk = $mem['Species']['Value'];
            if (!isset($membership[$tvk])) $membership[$tvk] = array();
            if (!in_array($abbrev,$membership[$tvk])) $membership[$tvk][] = $abbrev;               
         }         
      }
      $obj->Membership = $membership;
      return $obj;
   }
   function insert() {
      $sids = array();
      if (preg_match('/^\[/',$this->data['GN_Species_ID'])) {
         $svals = json_decode($this->data['GN_Species_ID']);
         foreach ($svals as $x => $sx) {
            $s = new GN_Species();
            $s->setdata(array('Latin_Name' => json_encode($sx)));
            $s->insert();
            $this->data['GN_Species_ID'] = $s->getid();
            parent::insert();
            $sids[] = $this->getid();
         }
      } else if (preg_match('/\{/',$this->data['GN_Species_ID'])) {
         $s = new GN_Species();
         $s->setdata(array('Latin_Name' => $this->data['GN_Species_ID']));
         $s->insert();
         $this->data['GN_Species_ID'] = $s->getid();
         parent::insert();
         $sids[] = $this->getid();
      } else {
         parent::insert();
         $sids = $this->getid();
      }
      return $sids;
   }
}

class GN_List_Type extends DBT {
   protected $tablename = 'GN_List_Type';
   protected $displayname = 'Species List Type';
   protected $show = array('GN_Species_List');
   protected $columns = array(
      'GN_List_Type_ID'    => 'Unique Key',
      'Name'              => '{"DataType":"Short Text","Mandatory":1}',
      'Description'       => 'Long Text' 
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'    => 'Species Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GN_List_Type' => '
"Name","Description"
"Protection","Protected and priority species legislation."
"Indicator","Species indicative of habitat or environmental character."
"Sensitive","Species for which data is considered sensitive."
"Custom","Allows users to define their own search criteria."'
   );   
}

class GN_List_Class extends DBT {
   protected $tablename = 'GN_List_Class';
   protected $displayname = 'Species List Class';
   protected $show = array('GN_Species_List');
   protected $columns = array(
      'GN_List_Class_ID'   => 'Unique Key',
      'Name'              => '{"DataType":"Short Text","Mandatory":1}',
      'Code'              => '{"DataType":"Short Text","Mandatory":1}',
      'Description'       => 'Long Text' 
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'    => 'Species Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GN_List_Class' => '
"Name","Code","Description"
"UK Legislative Protection","A","Species protected in UK law."
"UK Biodiversity Action Plan","B","Species prioritised in the UK BAP."
"Conservation","C","Species identified as at risk in conservation lists such as red data books."
"Local BAP Species","D","Species identified in local biodiversity action plans limited by the list extent."
"Extinct","E","Extict locally, regionally or nationally."
"Other","F","Other legislation."
"Non-native","G","Imports and invasives."
"Migrants","H","Migrants and vagrants."
"Indicators","I","Indicators, habitats, Ellenberg.."'
   );   
}

class GN_List_Extent extends DBT {
   protected $tablename = 'GN_List_Extent';
   protected $displayname = 'Species List Extent';
   protected $show = array();
   protected $columns = array(
      'GN_List_Extent_ID'  => 'Unique Key',
      'Name'              => '{"DataType":"Short Text","Mandatory":1}',
      'Boundary'          => '{"DataType":"Map Data","Mandatory":1}',
      'Description'       => 'Long Text' 
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'    => 'Species Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}

class GN_List_Scoring_Group extends DBT {
   protected $tablename = 'GN_List_Scoring_Group';
   protected $displayname = 'Species List Scoring Group';
   protected $show = array();
   protected $columns = array(
      'GN_List_Scoring_Group_ID'  => 'Unique Key',
      'GN_Scoring_Group_Type_ID'  => '{"DataType":"LINKEDTO","TargetType":"GN_Scoring_Group_Type","TargetField":"GN_Scoring_Group_Type_ID","Mandatory":1,"Current":1}',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Description'              => 'Long Text' 
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'    => 'Species Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}

class GN_Scoring_Group_Type extends DBT {
   protected $tablename = 'GN_Scoring_Group_Type';
   protected $displayname = 'Scoring Group Type';
   protected $show = array();
   protected $columns = array(
      'GN_Scoring_Group_Type_ID'  => 'Unique Key',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Description'              => 'Long Text' 
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'    => 'Species Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'GN_Scoring_Group_Type' => '
"Name","Description"
"Total","Add up the scores for all the recorded species in the group."
"Maximum","Report the highest single species score for recorded species in the group."
"Average","Average of the scores for all recorded species in the group (Ellenberg)."'
   );   
}

class GN_Species extends DBT {
   protected $tablename = 'GN_Species';
   protected $displayname = 'Species';
   protected $alts = array('Taxonomy' => 'GN_Taxonomy','Datasets' => 'GN_Dataset');
   protected $columns = array(
      'GN_Species_ID'    => 'Unique Key',
      'Latin_Name'      => '{"DataType":"Short Text","Interface":"Taxa","Use":"Name"}',
      'Common_Name'     => '{"DataType":"Short Text","ReadOnly":true}',
      'Authority'       => '{"DataType":"Short Text","ReadOnly":true,"NoList":true}',
      'Rank'            => '{"DataType":"Short Text","ReadOnly":true}',
      'TVK'             => '{"DataType":"Short Text","ReadOnly":true,"NoList":true}',
      'EOL_Taxa'        => '{"DataType":"Number","ReadOnly":true,"NoList":true}',
      'EOL_Page'        => '{"DataType":"Number","ReadOnly":true,"NoList":true}'
   );
   protected $domain = 'species';
   protected $permissions = array('Delete'=>'NO-ONE');
   protected $withtaxonomy = true;
   protected $ranks = array('kingdom','division','phylum','class','order','family','genus','species');
   function insert() {
      // allow population of json data from Taxa interface.
      if (preg_match('/^\{/',$this->data['Latin_Name'])) {
         $nbntaxa = json_decode($this->data['Latin_Name']);
         $latin = $nbntaxa->Name;
         $tvk = $nbntaxa->Value;
         
      } else $latin = $this->data['Latin_Name'];
      // deal with NHM Bombus (Bombus) terrestris style names
      $latin = preg_replace('/\s*\([^\)]+\)\s*/',' ',$latin);
      $trank = $this->data['Rank'];
      //$tdata = $this->data;
      $this->loaddata($latin,'Latin_Name');
      
      // check if species already exists in database using latin name
      if (!isset($this->id)) {
         $this->data['Latin_Name'] = $latin;
         $this->data['TVK'] = (isset($tvk))?$tvk:null;
         $this->data['Rank'] = $trank;
         parent::insert();
      }
      $tx = new GN_Taxonomy();
      if (!$tx->exists()) $tx->create();
      $tx->loaddata($this->id,'Recorded_As');
      $xid = $tx->getid();
      if (!isset($xid) && $this->withtaxonomy) {
         // use nbn api and eol api to populate other fields from latin name
         include_once('eol/eolapi.php');
         $pgs = eolgetpagesforspecies($latin);
         if (isset($pgs)) {
            list($pg1,$tx1) = eolgettaxonidforpages($pgs);
            if (isset($tx1)) {
               $sh = eolgethierarchyfortaxa($tx1,$latin);
               $vn = eolgetvernacularname($latin,$sh);
               $this->data['Common_Name'] = $vn;
               $this->data['EOL_Taxa'] = $tx1;
               $this->data['EOL_Page'] = $pg1;
               $species_ranks = eolgetranklist($tx1,$latin);
               $rdata = array();
               $t = null;
               foreach($species_ranks as $rank => $lname) {
                  if ($latin == $lname) {
                     $this->data['Rank'] = $rank;
                     $this->data['Latin_Name'] = $latin;
                     $this->data['TVK'] = $tvk;
                     $this->update($this->getpk(),$this->getid());
                     $tid = $this->getid();
                     switch($rank) {
                     case 'kingdom':      $rdata['Rank_'.ucwords($rank)] = $tid; break;                
                     case 'phylum':       $rdata['Rank_Phylum_or_Division'] = $tid; break;
                     case 'division':     $rdata['Rank_Phylum_or_Division'] = $tid; break;
                     case 'class':        $rdata['Rank_'.ucwords($rank)] = $tid; break;                
                     case 'order':        $rdata['Rank_'.ucwords($rank)] = $tid; break;                
                     case 'family':       $rdata['Rank_'.ucwords($rank)] = $tid; break;                
                     case 'genus':        $rdata['Rank_'.ucwords($rank)] = $tid; break;                
                     case 'species':      $rdata['Rank_'.ucwords($rank)] = $tid; break;                
                     /*
                     case 'subclass':     break; // ignore subclass 
                     case 'subphylum':    break; // ignore subphylum 
                     case 'superfamily':  break; // ignore superfamily 
                     case 'sp.':          break; // ignore subspecies 
                     case 'subspecies':   break; // ignore subspecies 
                     case 'infraspecies': break; // ignore subspecies 
                     case 'variety':      break; // ignore subspecies 
                     case '':             break; // ignore missing rank 
                     default:          $rdata['Rank_'.ucwords($rank)] = $tid; break;
                     */                
                     }
                     $rdata['Recorded_As'] = $tid;
                  } else {
                     $t = new GN_Species();
                     $td = array('Latin_Name' => $lname,'Rank' => $rank);
                     $t->setdata($td);
                     $t->inctaxonomy(false);
                     $tid = $t->insert();
                     switch($rank) {
                     case 'kingdom':      $rdata['Rank_'.ucwords($rank)] = $tid; break;                
                     case 'phylum':       $rdata['Rank_Phylum_or_Division'] = $tid; break;
                     case 'division':     $rdata['Rank_Phylum_or_Division'] = $tid; break;
                     case 'class':        $rdata['Rank_'.ucwords($rank)] = $tid; break;                
                     case 'order':        $rdata['Rank_'.ucwords($rank)] = $tid; break;                
                     case 'family':       $rdata['Rank_'.ucwords($rank)] = $tid; break;                
                     case 'genus':        $rdata['Rank_'.ucwords($rank)] = $tid; break;                
                     case 'species':      $rdata['Rank_'.ucwords($rank)] = $tid; break;                
                     /*
                     case 'subclass':     break; // ignore subclass 
                     case 'subphylum':    break; // ignore subphylum 
                     case 'superfamily':  break; // ignore superfamily 
                     case 'sp.':          break; // ignore subspecies 
                     case 'subspecies':   break; // ignore subspecies 
                     case 'infraspecies': break; // ignore subspecies 
                     case 'variety':      break; // ignore subspecies 
                     case '':             break; // ignore missing rank 
                     default:          $rdata['Rank_'.ucwords($rank)] = $tid; break;
                     */                
                     } 
                  }
               }
               $tx = new GN_Taxonomy();
               $tx->setdata($rdata);
               $tx->insert();
            }
         }
      } else {
         $this->data['TVK'] = $tvk;
         $this->update($this->getpk(),$this->getid());
      }
      return $this->getid();      
   }
   function inctaxonomy($val) {
      $this->withtaxonomy = $val;
   }
   function delete() {
      // delete any references to relevant rank from taxonomy 
      // if not specific level then entries can't be deleted as other specific 
      // entries may rely on current
      // alternatively - just refuse to delete altogether.
      return 0;  
   }
   function getname() {
      $common = $this->data['Common_Name'];
      $latin = $this->data['Latin_Name'];
      return (isset($common) && $common != "")?"$common ($latin)":$latin; 
   }
}


class GN_Taxonomy extends DBT {
   protected $tablename = 'GN_Taxonomy';
   protected $displayname = 'Taxonomy';
   protected $alts = array('Species' => 'GN_Species','Datasets' => 'GN_Dataset');
   protected $columns = array(
      'GN_Taxonomy_ID'             => 'Unique Key',
      'Recorded_As'                 => '{"DataType":"LINKEDTO","TargetType":"GN_Species","TargetField":"GN_Species_ID"}', 
      'Rank_Kingdom'                => '{"DataType":"LINKEDTO","TargetType":"GN_Species","TargetField":"GN_Species_ID","NoList":1}',
      'Rank_Phylum_or_Division'     => '{"DataType":"LINKEDTO","TargetType":"GN_Species","TargetField":"GN_Species_ID","NoList":1}',
      'Rank_Class'                  => '{"DataType":"LINKEDTO","TargetType":"GN_Species","TargetField":"GN_Species_ID","NoList":1}',
      'Rank_Order'                  => '{"DataType":"LINKEDTO","TargetType":"GN_Species","TargetField":"GN_Species_ID"}',
      'Rank_Family'                 => '{"DataType":"LINKEDTO","TargetType":"GN_Species","TargetField":"GN_Species_ID"}',
      'Rank_Genus'                  => '{"DataType":"LINKEDTO","TargetType":"GN_Species","TargetField":"GN_Species_ID"}',
      'Rank_Species'                => '{"DataType":"LINKEDTO","TargetType":"GN_Species","TargetField":"GN_Species_ID"}'
   );
   protected $domain = 'species';
   protected $permissions = array('Delete'=>'NO-ONE');
}
/*
class G_Dataset_Species extends DBT_Analysis {
   protected $tablename = 'S2_Dataset_Species';
   protected $displayname = 'Dataset Species';
   protected $domain = 'species';
   protected $selectto = 'G_Dataset';
   protected $columns = array(
      'S2_Authority_ID'          => '{"DataType":"LINKEDTO","TargetType":"S2_Authority","TargetField":"S2_Authority_ID","Mandatory":1,"Current":1,"Hidden":1,"Inherit":1}',
      'Authority'                => '{"DataType":"Short Text","Hidden":1}',
      'S2_Designation_ID'        => '{"DataType":"LINKEDTO","TargetType":"S2_Designation","TargetField":"S2_Designation_ID","Mandatory":1,"Current":1}',
      'Designation'              => '{"DataType":"Short Text","Hidden":1}',
      'Sites'                    => '{"DataType":"Number"}',
      'Appropriate_Management'   => '{"DataType":"Number"}',
      'Percentage_In_Management' => '{"DataType":"Percentage"}',
      'Candidates'               => '{"DataType":"Number"}',
      'Designated'               => '{"DataType":"Number"}',
      'Deleted'                  => '{"DataType":"Number"}',                    
   ); 
   protected $permissions = array(
      'Def'    => 'NOONE'
   );
   function S2_Designation_Overview() {
      parent::DBT_Analysis();
   }
   function create() {
      $select = "SELECT G_Dataset_ID,G_Record_ID,"; 
      $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
      $this->dbhandle->exec($statement);
      unset($pending[$this->tablename]);
   }
   function getuserpermissions() {
      return false;      
   }   
}


class G_Record extends DBT {
   protected $tablename = 'G_Record';
   protected $displayname = 'Species Record';
   protected $perpage = 25;
   protected $user;
   protected $alts = array('Datasets'=>'G_Dataset','Species Lists'=>'G_Species_List');
   protected $columns = array(
      'G_Record_ID'     => 'Unique Key',
      'G_Dataset_ID'    => '{"DataType":"LINKEDTO","TargetType":"G_Dataset","TargetField":"G_Dataset_ID","Mandatory":1,"Current":1}',
      'Species'         => 'Taxa',
      'Date'            => 'Date',
      'Location'        => '{"DataType":"Map Data","Derives":[{"Column":"Grid_Reference","Method":"s2_getgr","Params":[{"Type":"Value"}]}]}',
      'Grid_Reference'  => '{"DataType":"Short Text","Derives":[{"Column":"Location","Method":"s2_getgrsquare","Params":[{"Type":"Value"}]}]}',
      'Recorder'        => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person"],"Mandatory":1}'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $primingdata = array(
   );   
}

class G_Record_Determination extends DBT {
   protected $tablename = 'G_Record_Determination';
   protected $displayname = 'Species Determination';
   protected $perpage = 25;
   protected $user;
   protected $alts = array('Datasets'=>'G_Dataset','Species Lists'=>'G_Species_List');
   protected $show = array('G_Record_Determination');
   protected $columns = array(
      'G_Record_Determination_ID'   => 'Unique Key',
      'G_Determination_Type_ID'     => '{"DataType":"LINKEDTO","TargetType":"G_Determination_Type","TargetField":"G_Determination_Type_ID","Mandatory":1,"Current":1}', 
      'G_Record_ID'     => '{"DataType":"LINKEDTO","TargetType":"G_Record","TargetField":"G_Record_ID","Mandatory":1,"Current":1}',
      'G_Species_ID'    => '{"DataType":"LINKEDTO","TargetType":"G_Species","TargetField":"G_Species_ID","Mandatory":1,"Current":1}',
      'Date'            => 'Date',
      'Recorder'        => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person"],"Mandatory":1}'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $primingdata = array(
   );   
}

class G_Record_Determination extends DBT {
   protected $tablename = 'G_Record_Determination';
   protected $displayname = 'Species Determination';
   protected $perpage = 25;
   protected $user;
   protected $alts = array('Datasets'=>'G_Dataset','Species Lists'=>'G_Species_List');
   protected $columns = array(
      'G_Record_Determination_ID'   => 'Unique Key',
      'G_Determination_Type_ID'     => '{"DataType":"LINKEDTO","TargetType":"G_Determination_Type","TargetField":"G_Determination_Type_ID","Mandatory":1,"Current":1}', 
      'G_Record_ID'     => '{"DataType":"LINKEDTO","TargetType":"G_Record","TargetField":"G_Record_ID","Mandatory":1,"Current":1}',
      'G_Species_ID'    => '{"DataType":"LINKEDTO","TargetType":"G_Species","TargetField":"G_Species_ID","Mandatory":1,"Current":1}',
      'Date'            => 'Date',
      'Recorder'        => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person"],"Mandatory":1}'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $primingdata = array(
   );   
}

class G_Determination_Type extends DBT {
   protected $tablename = 'G_Determination_Type';
   protected $displayname = 'Determination Type';
   protected $columns = array(
      'G_Determination_Type_ID'    => 'Unique Key',
      'Name'              => '{"DataType":"Short Text","Mandatory":1}',
      'Description'       => 'Long Text' 
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'    => 'Species Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'G_Determination_Type' => '
"Name","Description"
"Transcription Error","Replacing errors created when entering the data"
"Taxonomy Update","Corrections to current preferred latin names and taxonomy"
"Photo ID","Expert review of species photo eg iSpot"
"Specimen ID","Result of expert dissection / microscopy / specimen analysis"'
   );   
}

class G_Determination_Status_Type extends DBT {
   protected $tablename = 'G_Determination_Status_Type';
   protected $displayname = 'Determination Status';
   protected $columns = array(
      'G_Determination_Status_Type_ID'    => 'Unique Key',
      'Name'              => '{"DataType":"Short Text","Mandatory":1}',
      'Description'       => 'Long Text' 
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'    => 'Species Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'G_Determination_Status_Type' => '
"Name","Description"
"Pending","New determination submitted but not reviewed"
"Accepted","New determination has been accepted as preferred determination"
"Disputed","New determination which has not been accepted due to disagreement"'
   );   
}
*/
?>