<?php
/*
*/
class G_Reserve_Number extends DBT {
   protected $tablename = 'G_Reserve_Number';
   protected $displayname = 'Reserve Number';
   protected $columns = array(
      'G_Reserve_Number_ID'   => 'Unique Key',
      'Domain'                => 'Short Text',
      'Class_Name'            => 'Short Text',
      'Property'              => 'Short Text',
      'User_Id'               => '{"DataType":"LINKEDTO","TargetType":"USR_Users","TargetField":"User_Id","Current":1,"Inherit":1}',
      'Reserve'               => 'Number',
      'Expiry'                => 'Date and Time'
   ); 
   protected $domain = 'genera';
   protected $permissions = array(
      'Def'    => 'Registered User'
   );
   function G_Reserve_Number() {
      parent::DBT();
      if (!$this->exists()) $this->create();
   }
   function maxreserved($dom,$tab,$prop) {
      $max = 0;
      $query = $this->dbhandle->select();
      switch($this->dbp) {
      case 'PGSQL': {
         $query->from(strtolower($this->tablename), array('MaxCount' => new Zend_Db_Expr("Max(reserve)")));
         $query->where("domain = ?",$dom);
         $query->where("class_name = ?",$tab);
         $query->where("property = ?",$prop);
         $query->where("expiry >= ?",new Zend_Db_Expr('NOW()'));
      }break;
      case 'MySQL': {
         $query->from($this->tablename, array('MaxCount' => new Zend_Db_Expr("Max(Reserve)")));
         $query->where("Domain = ?",$dom);
         $query->where("Class_Name = ?",$tab);
         $query->where("Property = ?",$prop);
         $query->where("Expiry >= ?",new Zend_Db_Expr('NOW()'));   
      }break;
      }
      $result = $this->dbhandle->fetchRow($query);
      if (isset($result['MaxCount'])) $max = $result['MaxCount'];
      return $max;
   }
   function myreserved($dom,$tab,$prop) {
      $uid = $this->user->getid();
      $myid = 0;
      $query = $this->dbhandle->select();
      switch($this->dbp) {
      case 'PGSQL': {
         $query->from(strtolower($this->tablename), array('MyCount' => new Zend_Db_Expr("Max(reserve)")));
         $query->where("domain = ?",$dom);
         $query->where("class_name = ?",$tab);
         $query->where("property = ?",$prop);
         $query->where("user_id = ?",$uid);
         $query->where("expiry >= ?",new Zend_Db_Expr('NOW()'));
      }break;
      case 'MySQL': {
         $query->from($this->tablename, array('MyCount' => new Zend_Db_Expr("Max(Reserve)")));
         $query->where("Domain = ?",$dom);
         $query->where("Class_Name = ?",$tab);
         $query->where("Property = ?",$prop);
         $query->where("User_Id = ?",$uid);
         $query->where("Expiry >= ?",new Zend_Db_Expr('NOW()'));
      }break;
      }
      $result = $this->dbhandle->fetchRow($query);
      if (isset($result['MyCount'])) $myid = $result['MyCount'];
      return $myid;
   }
   function reserve($dom,$tab,$prop,$maxinuse) {
      $myres = $this->myreserved($dom,$tab,$prop);
      if ($myres == 0) {
         $uid = $this->user->getid();
         $maxres = $this->maxreserved($dom,$tab,$prop);
         $myres = max($maxinuse,$maxres)+1;
         $inter = $this->getinterval('1H');
         $expiry = new Zend_Db_Expr('NOW() + '.$inter);
         $this->setdata(array('Domain'=>$dom,'Class_Name'=>$tab,'Property'=>$prop,'User_Id'=>$uid,'Reserve'=>$myres,'Expiry'=>$expiry));
         $this->insert();
      }      
      return $myres;   
   }
   function unreserve($dom,$tab) {
      $pk = $this->getpk();
      $uid = $this->user->getid();
      $query = $this->dbhandle->select();
      switch($this->dbp) {
      case 'PGSQL': {
         $pk = strtolower($pk);
         $query->from(strtolower($this->tablename), array($pk));
         $query->where("domain = ?",$dom);
         $query->where("class_name = ?",$tab);
         $query->where("user_id = ?",$uid);
      }break;
      case 'MySQL': {
         $query->from($this->tablename, array($pk));
         $query->where("Domain = ?",$dom);
         $query->where("Class_Name = ?",$tab);
         $query->where("User_Id = ?",$uid);
      }break;
      }
      $result = $this->dbhandle->fetchAll($query);
      foreach($result as $rownum => $row) {$this->expire($row[$pk]);}
   }
   function expire($id) {
      if ($id) $status = $this->delete($this->getpk(),$id); 
      else {
         switch($this->dbp) {
         case 'PGSQL': $status = $this->dbhandle->delete($this->tablename,"expiry > ?",new Zend_Db_Expr('NOW();')); break;
         case 'MySQL': $status = $this->dbhandle->delete($this->tablename,"Expiry > ?",new Zend_Db_Expr('NOW();')); break;
         }
      }
      return $status;
   }
}
?>