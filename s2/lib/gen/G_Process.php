<?php
class G_Post_Processor extends DBT { 
   protected $tablename = 'G_Post_Processor';
   protected $displayname = 'Post Processing';
   protected $domain = 'gen';
   function G_Post_Processor() {
      parent::DBT();
      if (!$this->exists()) $this->create();
   }
   protected $columns = array(
      'G_Post_Processor_ID'      => 'Unique Key',
      'Type'                     => '{"DataType":"Short Text"}',
      'Type_ID'                  => '{"DataType":"Number"}',
      'Last_Step'                => '{"DataType":"Number"}',
      'Current_Step'             => '{"DataType":"LINKEDTO","TargetType":"G_Post_Process_Step","TargetField":"G_Post_Process_Step_ID","Current":1,"Inherit":1}',
      'Out_Of'                   => '{"DataType":"Number"}',
      'Status'                   => '{"DataType":"Short Text"}',
      'Percentage_Complete'      => '{"DataType":"Percentage"}'
   );
   protected $permissions = array(
      'Def'    => 'Registered User'
   );
   function loadmyprocessor($id,$type) {
      $where = array('Type'=>$type,'Type_ID'=>$id);
      $id = $this->selectvalwhere($this->getpk(),$where);
      if (isset($id)) $this->loaddata($id);
   }
   function createstep($name,$step,$outof,$parent=0) {
      $gppsc = 'G_Post_Process_Step';
      $status = "$step: $name started.";
      $pid = $this->getid(); 
      $stepdata = array(
         'G_Post_Processor_ID'   => $pid,
         'Name'                  => $name,
         'Parent_Step'           => $parent,
         'Number'                => $step,      
         'Last_Step'             => 0,
         'Out_Of'                => $outof,
         'Status'                => $status,
         'Percentage_Complete'   => 0
      );
      $pstep = new $gppsc();
      $pstep->setdata($stepdata);
      $pstep->insert();
      $sid = $pstep->getid();
      if ($parent > 0) {
         $this->data['Current_Step'] = $sid;
         $this->update($this->getpk(),$this->getid());
      }
      return $pstep;
   }
   function completestep($number,$status=null) {
      $of = $this->data['Out_Of'];
      $this->data['Last_Step'] = $number;
      $this->data['Percentage_Complete'] = ($number / $of);
      $this->data['Status'] = (isset($status))?$status:"Step $number of $of completed";
      $this->update($this->getpk(),$this->getid());   
   }
   function failedstep($number,$status) {
      $this->data['Last_Step'] = $number-1;
      $this->data['Percentage_Complete'] = (($number-1) / $this->data['Out_Of']);
      $this->data['Status'] = (isset($status))?$status:"Step $number failed";   
      $this->update($this->getpk(),$this->getid());   
   }
   function getnextstep() {
      return ($this->data['Last_Step']) + 1;
   }
   function complete() {
      $this->delete($this->getpk(),$this->getid());
   }
   function getstatus() {
      $obj = new stdClass();
      $obj->Process = $this->getdata(); 
      $step = $this->getcurrentstep();
      if (isset($step)) $obj->Step = $step->getdata();
      return $obj;               
   }
   function setstep($stepid) {
      $this->data['Current_Step'] = $stepid;
      $this->update($this->getpk(),$this->getid());          
   }
   function getcurrentstep() {
      $pps = null;
      $step = $this->data['Current_Step'];
      if (is_object($step))$step = $step->Id;
      if (isset($step) && ($step > 0)) {
         $ppsc = 'G_Post_Process_Step';
         $pps = new $ppsc();
         $pset = new stdClass();
         $pset->Current = $this->getid();
         $cstep = $this->data['Current_Step'];
         if (is_object($cstep)) $cstep = $cstep->Id;
         $pps->loaddata($cstep);
      }
      return $pps;
   }     
   function getname() {
      return $this->derivename(array('Type','Type_ID'));
   }
   function updatestatus($percent,$status) {
      $this->data['Percentage_Complete'] = $percent;
      $this->data['Status'] = $status;
      unset($this->data['Name']);
      $this->update($this->getpk(),$this->getid());
   }
}

class G_Post_Process_Step extends DBT { 
   protected $tablename = 'G_Post_Process_Step';
   protected $displayname = 'Post Process Step';
   protected $domain = 'gen';
   function G_Post_Process_Step() {
      parent::DBT();
      if (!$this->exists()) $this->create();
   }
   protected $columns = array(
      'G_Post_Process_Step_ID'   => 'Unique Key',
      'G_Post_Processor_ID'      => '{"DataType":"LINKEDTO","TargetType":"G_Post_Processor","TargetField":"G_Post_Processor_ID","Current":1,"Inherit":1}',
      'Name'                     => '{"DataType":"Short Text"}',
      'Parent_Step'              => '{"DataType":"LINKEDTO","TargetType":"G_Post_Process_Step","TargetField":"G_Post_Process_Step_ID","Current":1,"Inherit":1}',
      'Number'                   => '{"DataType":"Number"}',
      'Last_Step'                => '{"DataType":"Number"}',
      'Out_Of'                   => '{"DataType":"Number"}',
      'Status'                   => '{"DataType":"Short Text"}',
      'Percentage_Complete'      => '{"DataType":"Percentage"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Registered User'
   );
   function completestep($number,$status=null) {
      $of = $this->data['Out_Of'];
      $this->data['Last_Step'] = $number;
      $percent = ($number / $of); 
      $this->data['Percentage_Complete'] = $percent;
      $this->data['Status'] = "Step $number completed";
      $this->update($this->getpk(),$this->getid());
      if (!isset($status)) $status = $this->data['Name'] + ": Step $number of $of completed";
      $this->updateparent($percent,$status);   
   }
   function failedstep($number,$status=null) {
      $this->data['Last_Step'] = $number-1;
      $this->data['Percentage_Complete'] = (($number-1) / $this->data['Out_Of']);
      $this->data['Status'] = (isset($status))?$status:"Step $number failed";   
      $this->update($this->getpk(),$this->getid());   
   }
   function getnextstep() {
      return $this->data['Last_Step'] + 1;
   }
   function complete() {
      $pc  = 'G_Post_Processor';
      $thistype = $this->tablename;
      if (isset($this->data['Parent_Step'])) { 
         $parent = new $thistype();        
         $parent->loaddata($this->data['Parent_Step']);
         $pdata = $parent->getdata();
      } else {
         $process = new $pc();
         list($incol,$tocol) = $this->getlinkedcolumn($pc);
         $pci = $this->data[$incol];
         $process->loaddata($pci);
         $pdata = $process->getdata();
      }
      $this->delete($this->getpk(),$this->getid());
   }
   function updatestatus($percent,$status) {
      $this->data['Percentage_Complete'] = $percent;
      $this->data['Status'] = $status;
      $this->update($this->getpk(),$this->getid());
      $this->updateparent($percent,$status);
   }
   function updateparent($percent,$status) {
      $pc = 'G_Post_Processor';
      $name = $this->data['Name'];
      $thistype = $this->tablename;
      if (isset($this->data['Parent_Step']) && ($this->data['Parent_Step'] > 0)) { 
         $parent = new $thistype();
         $pci = $this->data['Parent_Step'];
         if (is_object($pci)) $pci = $pci->Id;
         $parent->loaddata($pci);
         $pdata = $parent->getdata();
         $pname = $pdata['Name'];
         $pcomp = (isset($pdata['Last_Step']))?$pdata['Last_Step']:0;
         $pfrom = $pdata['Out_Of'];
         $core = floatval($pcomp/$pfrom);
         $incr = floatval(floatval(1/$pfrom) * $percent);
//print "<br/>$pname ($name): ( $pcomp / $pfrom ) + ((1 / $pfrom) * $percent) = " . floatval($core+$incr);
         $pass = $parent->updatestatus(floatval($core+$incr),$status);
//if (!$pass) print "<br/>$pname update failed: ".pg_last_error();
      } else {
         $proc = new $pc();
         list($incol,$tocol) = $this->getlinkedcolumn($pc);
         $pci = $this->data[$incol];
         if (is_object($pci)) $pci = $pci->Id;
         $proc->loaddata($pci);
         $pdata = $proc->getdata();
         $pcomp = (isset($pdata['Last_Step']))?$pdata['Last_Step']:0;
         $pfrom = $pdata['Out_Of'];
         $core = floatval($pcomp/$pfrom);
         $incr = floatval(floatval(1/$pfrom) * $percent);
//print "<br/>Process ($name): ( $pcomp / $pfrom ) + ((1 / $pfrom) * $percent) = " . floatval($core+$incr);
         $pass = $proc->updatestatus(floatval($core+$incr),"$name : $status");   
//if (!$pass) print "<br/>Process update failed: ".pg_last_error();
      }      
   }
}
?>