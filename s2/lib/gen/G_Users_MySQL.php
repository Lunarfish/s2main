<?php
/*
$paths = array(
   '../',
   '../../',
   '../../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
*/
include_once('settings.conf');
include_once('snc/S2_DBT.php');

/*
CREATE TABLE IF NOT EXISTS `usr_users` (
  `userId` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `seclev` int(10) NOT NULL DEFAULT '1',
  `receiveReminders` enum('yes','no') NOT NULL DEFAULT 'yes',
  `status` enum('unconfirmed','disabled','enabled') NOT NULL DEFAULT 'unconfirmed',
  `createdDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`userId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 PACK_KEYS=1 AUTO_INCREMENT=4 ;
*/
class USR_Users extends DBT {
   protected $tablename = 'USR_Users';
   protected $displayname = 'User';
   protected $user;
   //protected $show = array('USR_Permissions');
   protected $alts = array('Default Permissions'=>'USR_Default_Permissions');
   protected $show = array('USR_Effective_Permissions');
   protected $columns = array(
      'User_Id'            => 'Unique Key',
      'Name'               => 'Short Text',
      'Email'              => 'Medium Text',
      'Google_Account'     => '{"DataType":"Medium Text","NoList":1}',
      'Password'           => '{"DataType":"Password","Confirmed":1,"NoList":1}',
      'Status'             => '{"DataType":"Short Text","NoList":1,"Options":["enabled","disabled"]}',
      'GStatus'            => '{"DataType":"Short Text","Hidden":1}',
      'Seclev'             => '{"DataType":"Number","Hidden":1}' 
   );
   protected $domain = 'users';
   protected $permissions = array(
      'List'   => 'list_users',
      'View'   => 'list_users',
      'Delete' => 'NOONE'
   );
   function getname() {
      return $this->data['Name'];
   }
   function getstatus() {
      return $this->selectcolumnval('Status');
   }
   function getgstatus() {
      return $this->selectcolumnval('GStatus');
   }
   function googleauthenticate($email) {
      $this->loaddata($email,'Email');
      $status = 'unrecognised';
      if (!isset($this->id)) {
         $this->loaddata($email,'Google_Account');
         $status = $this->getgstatus();
      } else {
         $status = $this->getstatus();
      }
      if (isset($this->id)) {
         if ($status == 'enabled') { 
            $this->setcookie();
            $cookie = $this->getcookie();
            $status = 'authenticated';
         } else {
            $status = 'unconfirmed';
         }      
      }
      return $status;
   }
   function autologin($username) {
      $this->loaddata($username,'Email');
      $id = $this->getid();
      $cookie = null;
      if (isset($id)) {
         $this->setcookie();
         $cookie = $this->getcookie();
      } 
      return $cookie;  
   }
   function login($username,$password) {
      $this->loaddata($username,'Email');
      $status = 'unrecognised';
      if ($this->data['Password'] == $password && $this->getstatus() == 'enabled') {
         $status =  $this->getstatus();
         $this->setcookie();
         $cookie = $this->getcookie();
      } 
      return $status;  
   }
   function logout() {
      global $USER_COOKIE,$COOKIE_PATH,$COOKIE_DOMAIN,$COOKIE_KEY;
      setcookie($USER_COOKIE,"",-1,$COOKIE_PATH,$COOKIE_DOMAIN,0);      
   }
   function register($data) {
      global $USERS_TABLES;
      $data['Seclev'] = 9000;
      $data['Status'] = 'unconfirmed';
      $data['GStatus'] = 'unconfirmed';
      $this->setdata($data);
      $status = $this->insert();
      $message = ($status==0)?'Account could not be created.':'Account created.';
      
      $obj = $this->getview($settings);
      $obj->Status = $status;
      $obj->Message = $message;
      // use user class methods to create account and send email
      $userdata = $this->getdata();
      $user_id = $userdata[$this->getpk()];
      $token = time() . "::$user_id";
      $ltable = $USERS_TABLES['Login_Tokens'];
      $link = new $ltable();
      if (!$link->exists()) $link->create();
      $data = array();
      $data['User_Id'] = $user_id;
      $data['Token'] = $token;
      $link->setdata($data);
      $link->insert();
      //$u = new User();
      //$u->set_defaultpermissions($user_id);
      $this->set_defaultpermissions($user_id);
      $status1 = true;$status2 = true;
      if (isset($userdata['Email'])) $status1 = $this->send_confirmation($token,'default');
      if (isset($userdata['Google_Account']) && $userdata['Google_Account'] != "") $status2 = $this->send_confirmation($token,'Google');
      if (!$status1 || !$status2) {
         $obj->Status = 0;
         $obj->Message .= "\nEmail could not be sent. ".error_get_last();
      }     
      return $obj;
   }
   function gettoken() {
      $link = null;
      $link = new USR_Login_Tokens();
      $link->loaddata($user_id,'User_Id');
      $data = $link->getdata();
      $token = $data['Token'];
      return $token;   
   }
   function insert() {
      if (!isset($this->data['Status'])) $this->data['Status'] = 'enabled';
      $s = parent::insert();
      $this->set_defaultpermissions($this->id);
      /*
      $t = $this->gettoken();
      $p = $this->data['Password'];
      $o = $this->confirm($t,$p);
      */
      return $s;
   }
   function confirm($token,$password) {
      global $USERS_TABLES;
      $ltable = $USERS_TABLES['Login_Tokens'];
      $lt = null;
      $token = (preg_match('/\=+/',$token))?$token:"$token==";
      $token = base64_decode($token);
      eval ("\$lt = new $ltable();");
      $lt->loaddata($token,'Token');
      $ltdata = $lt->getdata();
      $user_id = (is_object($ltdata['User_Id']))?$ltdata['User_Id']->Id:$ltdata['User_Id'];
      $this->loaddata($user_id);
      $status = false;
      if ($this->data['Password'] == $password) {
         $this->data['Status'] = 'enabled';
         $status = $this->update($this->getpk(),$user_id);
      }
      $message = ($status==0)?'Confirm account failed':'Account enabled';
      
      $obj = new stdClass();
      $obj->Status = $status;
      $obj->Message = $message;
      return $obj;        
   }
   function googleconfirm($token,$password) {
      global $USERS_TABLES;
      $ltable = $USERS_TABLES['Login_Tokens'];
      $lt = null;
      $token = (preg_match('/\=+/',$token))?$token:"$token==";
      $token = base64_decode($token);
      eval ("\$lt = new $ltable();");
      $lt->loaddata($token,'Token');
      $ltdata = $lt->getdata();
      $user_id = (is_object($ltdata['User_Id']))?$ltdata['User_Id']->Id:$ltdata['User_Id'];
      $this->loaddata($user_id);
      $status = false;
      if ($this->data['Password'] == $password) {
         $this->data['GStatus'] =  'enabled';
         $status = $this->update($this->getpk(),$user_id);
      }
      $message = ($status==0)?'Confirm account failed':'Account enabled';
      
      $obj = new stdClass();
      $obj->Status = $status;
      $obj->Message = $message;
      return $obj;        
   }
   function send_password() {
      global $COMPANY_NAME,$WEB_MASTER,$WEB_MASTER_EMAIL;
      $email = $this->data['Email'];
      $k = new KLib($this->getsid());
      $keys = json_decode($k->getkeys());
      $password = decrypt($this->data['Password'],$keys->k1,$keys->k2);      
      $status = mail(
         "$email",
         "$COMPANY_NAME requested email", 
         "Here is your password for $COMPANY_NAME: $password.\n\nThanks,\n$WEB_MASTER",
         "From: $WEB_MASTER <$WEB_MASTER_EMAIL>\n"
      );
   }
   function send_confirmation($token,$type) {
      global $COMPANY_NAME,$USERS_BASE_URL,$WEB_MASTER,$WEB_MASTER_EMAIL;
      switch ($type) {
      case 'Google': {
         $gmail = $this->data['Google_Account'];
         $email = $this->data['Email'];
         $name = $this->data['Name'];
         $status = mail(
            "$email, $WEB_MASTER_EMAIL", 
            "Linked Google account for: $name @ $COMPANY_NAME", 
            "Thank you for registering your Google ID with $COMPANY_NAME. " .
            "This is an automatic email to confirm the Google account linked to your account. " . 
            "Please click the link below to verify your account so you can login using your Google ID.\n\n" . 
            "$USERS_BASE_URL/?Action=GetForm&Form=Google_Confirm&Token=".base64_encode($token)."\n\n" .
            "Email: $email\nGoogle Account: $gmail\nToken: ".base64_encode($token)."\n\n". 
            "Thanks,\n$WEB_MASTER","From: $WEB_MASTER <$WEB_MASTER_EMAIL>\n"
         );
      }break;
      default: {
         $email = $this->data['Email'];      
         $status = mail(
            "$email, $WEB_MASTER_EMAIL", 
            "New account: $COMPANY_NAME", 
            "Thank you for registering with $COMPANY_NAME. " .
            "This is an automatic email to confirm your identity. " . 
            "Please click the link below to verify your account so you can login.\n\n" . 
            "$USERS_BASE_URL/?Action=GetForm&Form=Confirm&Token=".base64_encode($token)."\n\n" .
            "Email: $email\nToken: ".base64_encode($token)."\n\n". 
            "Thanks,\n$WEB_MASTER","From: $WEB_MASTER <$WEB_MASTER_EMAIL>\n"
         );
      }break;
      }
      return $status;        
   }
   function getcookie() {
      $cookie = "email=".urlencode($this->data['Email'])."&name=".$this->data['Name']."&uid=".$this->getid()."&seclev=".$this->data['Seclev'];
      $cookie = uenc($cookie);
      return $cookie;
   }
   function getcookieobj() {
      global $USER_COOKIE;
      $u = new stdClass();
      $u->Name = $USER_COOKIE;
      $u->Cookie = $this->getcookie();
      $u->Expiry = $this->getexpiry();
      return $u;  
   }
   function getexpiry() {
      // this is set to 5 years for some reason !
      //return (time() + (3600 * 24 * 365 * 5));
      return (time() + (3600 * 3));
   }
   function setcookie() {
      global $USER_COOKIE,$COOKIE_PATH,$COOKIE_DOMAIN;
      $cookie = $this->getcookie();
      $expire = $this->getexpiry();
      setcookie($USER_COOKIE,$cookie,$expire,$COOKIE_PATH,$COOKIE_DOMAIN,0);
   }
   function set_defaultpermissions($user_id) {
      $dp = new USR_Default_Permissions();
      $p = new USR_Permissions();
      $dps = $dp->selectAll();
      foreach($dps as $i => $dpi) {
         $domain = $dpi['Domain'];
         $label = $dpi['Label'];
         $value = $dpi['Value'];
         $d = array(
            'User_Id'   => $user_id,
            'Domain'    => $domain,
            'Label'     => $label,
            'Value'     => $default
         );
         $p->setdata($d);
         $p->insert();
      }  
   }  
   function isallowed($user,$action) {
      // overrided to allow you to view and edit your own data
      if (isset($_REQUEST['Current']) && ($user->getid() == $_REQUEST['Current'])) $d = 1; 
      else $d = parent::isallowed($user,$action);
      return $d;
   } 
   protected $primingdata = array(
      'USR_Users' => '
"Name", "Email","Password","Seclev","Status"
"Administrator","root@localhost","changeme",0,"enabled"',
      'USR_Permissions' => '
"User_Id","Domain","Label","Value"
1,"users","change_other_pass",1
1,"users","edit_perm",1
1,"users","set_perm",1
1,"users","edit_users",1
1,"users","edit_self",1
1,"users","list_users",1',
      'USR_Default_Permissions' =>
'"Domain","Label","Description","Value","Anon" 
"users","list_users","View existing user accounts",0,0
"users","edit_self","Edit personal info including password",1,0
"users","edit_users","Edit other users",0,0
"users","set_perm","Set permissions",0,0
"users","edit_perm","Edit permissions",0,0
"users","change_other_pass","Change others user password",0,0'
   );   
}
/*
CREATE TABLE IF NOT EXISTS `usr_permissions` (
  `user_id` int(10) NOT NULL DEFAULT '0',
  `domain` varchar(35) NOT NULL DEFAULT '',
  `label` varchar(35) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT '',
  `value` int(1) DEFAULT NULL,
  `lastModified` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`domain`,`label`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=1;
*/
class USR_Permissions extends DBT {
   protected $tablename = 'USR_Permissions';
   protected $displayname = 'Permissions';
   protected $user;
   //protected $show = array();
   protected $order = array('domain','label');
   protected $columns = array(
      'USR_Permissions_ID' => 'Unique Key',
      'User_Id'            => '{"DataType":"LINKEDTO","TargetType":"USR_Users","TargetField":"User_Id","Current":1,"Mandatory":1}',
      'Domain'             => 'Short Text',
      'Label'              => 'Short Text',
      'Description'        => 'Long Text',
      'Value'              => 'True or False'
   );
   protected $domain = 'users';
   protected $permissions = array(
      'Def'    => 'set_perm',
      'List'   => 'edit_perm',
      'View'   => 'edit_perm'
   );
   function getname() {
      return $this->data['Label'];
   }    
}

/*
CREATE TABLE IF NOT EXISTS `usr_default_permissions` (
  `USR_Default_Permissions_Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `domain` varchar(35) NOT NULL DEFAULT '',
  `label` varchar(35) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT '',
  `value` int(1) DEFAULT NULL,
  `lastModified` datetime DEFAULT NULL,
  PRIMARY KEY (`USR_Default_Permissions_Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 PACK_KEYS=1 AUTO_INCREMENT=16 ;
*/
class USR_Default_Permissions extends DBT {
   protected $tablename = 'USR_Default_Permissions';
   protected $displayname = 'Default Permissions';
   protected $user;
   //protected $show = array();
   protected $alts = array('Users'=>'USR_Users');
   protected $order = array('domain','label');
   protected $columns = array(
      'USR_Default_Permissions_Id'  => 'Unique Key',
      'Domain'                      => 'Short Text',
      'Label'                       => 'Short Text',
      'Description'                 => 'Long Text',
      'Value'                       => 'True or False',
      'Anon'                        => 'True or False'
   );
   protected $domain = 'users';
   protected $permissions = array(
      'Def'    => 'set_perm',
      'List'   => 'edit_perm',
      'View'   => 'edit_perm'
   );
   function getperms() {
      $data = $this->selectAll();
      $perms = array();      
      foreach ($data as $row) {
         $perms[$row['Domain']][$row['Label']] = $row['Value'];
      }
      return $perms;
   }  
   function getanonperms() {
      $data = $this->selectAll();
      $perms = array();      
      foreach ($data as $row) {
         $perms[$row['Domain']][$row['Label']] = $row['Anon'];
      }
      return $perms;
   }
   function getname() {
      return $this->data['Label'];
   }
}
/*
CREATE TABLE IF NOT EXISTS `usr_login_tokens` (
  `userId` int(10) NOT NULL DEFAULT '0',
  `token` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=1;
*/
class USR_Login_Tokens extends DBT {
   protected $tablename = 'USR_Login_Tokens';
   protected $displayname = 'Login Tokens';
   protected $user;
   //protected $show = array();
   protected $alts = array('Users'=>'USR_Users');
   protected $columns = array(
      'USR_Login_Tokens_Id'  => 'Unique Key',
      'User_Id'                     => '{"DataType":"LINKEDTO","TargetType":"USR_Users","TargetField":"User_Id","Mandatory":1}',
      'Token'                      => 'Short Text' 
   );
   protected $domain = 'users';
   protected $permissions = array(
      'Def'    => 'set_perm',
      'List'   => 'edit_perm',
      'View'   => 'edit_perm'
   );
}

/*
CREATE TABLE IF NOT EXISTS `usr_profiles` (
  `userId` int(10) NOT NULL DEFAULT '0',
  `department` varchar(100) DEFAULT '',
  `firstName` varchar(50) DEFAULT '',
  `lastName` varchar(50) DEFAULT '',
  `title` varchar(35) DEFAULT '',
  `im_type` varchar(20) DEFAULT '',
  `im_userid` varchar(100) DEFAULT '',
  `phone1` varchar(20) DEFAULT NULL,
  `phone1Type` enum('home','mobile','pager','work') DEFAULT NULL,
  `phone2Type` enum('home','mobile','pager','work') DEFAULT NULL,
  `phone2` varchar(20) DEFAULT NULL,
  `phone3Type` enum('home','mobile','pager','work') DEFAULT NULL,
  `phone3` varchar(20) DEFAULT NULL,
  `lastModified` datetime DEFAULT NULL,
  `biography` text,
  `startDate` date DEFAULT NULL,
  `photoUrl` varchar(100) DEFAULT NULL,
  `photoWidth` int(10) DEFAULT NULL,
  `photoHeight` int(10) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=1;
*/

class USR_Effective_Permissions extends DBT_Analysis {
   protected $tablename = 'USR_Effective_Permissions';
   protected $displayname = 'Effective Permissions';
   protected $user;
   protected $def = 'USR_Default_Permissions';
   protected $per = 'USR_Permissions';
   protected $usr = 'USR_Users';
   protected $rootdomain;
   protected $instance;
   protected $alias;
   protected $columns = array(
      'USR_Permissions_ID' => 'Unique Key',
      'User_Id'            => '{"DataType":"LINKEDTO","TargetType":"USR_Users","TargetField":"User_Id","Current":1,"Mandatory":1}',
      'Domain'             => '{"DataType":"Short Text","ReadOnly":1}',
      'Label'              => '{"DataType":"Short Text","ReadOnly":1}',
      'Description'        => '{"DataType":"Long Text","ReadOnly":1}',
      'Default'            => '{"DataType":"True or False","ReadOnly":1}',   
      'Effective'          => '{"DataType":"True or False","ReadOnly":0}'   
   );
   function USR_Effective_Permissions() {
      parent::DBT_Analysis();
      if (isset($_REQUEST['d'])) {
         $denc = $_REQUEST['d'];
         $k = new KLib($this->getsid());
         $keys = json_decode($k->getkeys());
         $dclr = decrypt($denc,$keys->k1,$keys->k2);
         $dobj = json_decode($dclr);
         $this->rootdomain = $dobj->domain;
         $this->instance = $dobj->instance;
         $this->alias = $dobj->alias;
      }
   }
   function selectRowCount($col,$val,$settings=null,$where=null) {
      $data = $this->select($col,$val,$settings,$where);
      return count($rows);
   }
   function select($col,$val,$settings=null,$where=null,$page=null) {
      if (isset($settings)) {
         $itype = $this->gettablename();
         $isettings = $settings->$itype;
         $sby = (isset($isettings->SearchBy))?'def.'.$isettings->SearchBy:null;
         $sfor = (isset($isettings->SearchFor))?$isettings->SearchFor:null;
      }
      
      $query = $this->dbhandle->select();
      $query->from(array('def' => $this->def),array('Domain','Label','Description','Default'=>'Value'));
      $join = "def.Domain = per.Domain And def.Label = per.Label";
      $query->joinLeft(array('per' => $this->per),$join,array('USR_Permissions_ID','Effective'=>'Value'));
      $join = "usr.User_Id = per.User_Id";
      $query->join(array('usr' => $this->usr),$join,array('User_Id'));
      // you need to be able to set permissions for users who haven't had 
      // permissions on the domain before so you need to use a LEFT JOIN to 
      // allow the default permissions to be returned where the user permissions
      // are not yet set. 
      $query->where("usr.$col = ?",$val);
      if (isset($sby)) {
         $stype = "".$this->columns[$sby];
         if (preg_match('/^\{/',$stype)) {
            $params = json_decode($stype);
            $stype = $params->DataType;
         } else {
            $bits = preg_split('/\:/',$stype);
            $stype = $bits[0];
         }
         switch($stype) {
         case 'INTERFACE': {
            $cache = new G_Cache();
            $ints = $cache->select('Name',"%$sfor%");
            $sfors = array();
            foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
            $query->where("$sby IN(?)",$sfors);
         }break;
         case 'Taxa': {
            $cache = new G_Cache();
            $ints = $cache->select('Name',"%$sfor%");
            $sfors = array();
            foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
            $query->where("$sby IN(?)",$sfors);
         }break;
         case 'Date': {
            if (preg_match('/([>,<]\=*)/',$sfor,$m)) $sop = $m[1];
            else $sop = '=';
            if ($sop == '') $sop = '=';
            $sfor = preg_replace('/[>,<]\=*\s*/','',$sfor);
            if (preg_match('/(\d{2})\/(\d{2})\/(\d{4})/',$sfor,$m)) $sfor = $m[3]."-".$m[2]."-".$m[1];
            if (preg_match('/^\d{4}$/',$sfor)) $sby = new Zend_Db_Expr("YEAR($sby)");
            $scol = (preg_match('/^\d{4}$/',$sfor))?new Zend_Db_Expr("YEAR($sby)"):$sby;
            $query->where("$scol $sop ?",$sfor);
         }break;
         /* linked data is handled separately */
         case 'LINKEDTO':break;
         case 'LINKEDTOM':break;
         default: {
            if (preg_match('/\,/',$sfor)) {
               $sfors = preg_split('/\s*\,\s*/',$sfor); 
               $query->where("$sby IN(?)",$sfors);
            } else {
               $query->where("$sby LIKE ?","%$sfor%");
            }
         }break;
         }  
      }
      if (isset($this->instance)) {
         $query->where("def.domain = ?",$this->rootdomain);
      // if you just select where the user_id is set then you again lose the 
      // default permissions where the user's haven't yet been set so you have 
      // to add an orWhere there is no user_id
         $query->orWhere("usr.$col IS NULL");  
         $result = $this->dbhandle->fetchAll($query);
         $query = $this->dbhandle->select();
         $query->from($this->per,array('USR_Permissions_ID','User_Id','Label','Value'));
         $query->where("$col = ?",$val);
         $query->where("Domain = ?",$this->instance);
//print $query->__toString();exit;
         $inst = $this->dbhandle->fetchAll($query);
         foreach($result as $drow => $dpermission) {
            $perid = null;
            foreach($inst as $irow => $ipermission) {
               if ($dpermission['Label'] == $ipermission['Label']) {
                  $result[$drow]['Default'] = max($dpermission['Default'],$dpermission['Effective']);
                  $result[$drow]['Effective'] = max($dpermission['Effective'],$ipermission['Value']);
                  $perid = $ipermission['USR_Permissions_ID'];
               }
            }
            if (!isset($perid)) {
               $data = array(
                  'User_Id'   => $val, 
                  'Domain'    => $this->instance, 
                  'Label'     => $dpermission['Label'],
                  'Effective' => $dpermission['Default']
               );
               $this->setdata($data);
               $this->insert();
               $perid = $this->getid();
            }
            $result[$drow]['Domain'] = $this->alias;
            $result[$drow]['USR_Permissions_ID'] = $perid;
         }
      } else {
//echo $query->__toString();
      // if you just select where the user_id is set then you again lose the 
      // default permissions where the user's haven't yet been set so you have 
      // to add an orWhere there is no user_id
         //$lcol = strtolower($col);    
         $query->orWhere("usr.$col IS NULL");  
         $result = $this->dbhandle->fetchAll($query);
         foreach($result as $drow => $dpermission) {
            $perid = $dpermission['USR_Permissions_ID'];
            if (!isset($perid)) {
               $data = array(
                  'User_Id'   => $val, 
                  'Domain'    => $dpermission['Domain'], 
                  'Label'     => $dpermission['Label'],
                  'Effective' => $dpermission['Default']
               );
               $this->setdata($data);
               $this->insert();
               $perid = $this->getid();
               $result[$drow]['USR_Permissions_ID'] = $perid;
            } else {
               $result[$drow]['Effective'] = $this->postprocessterm('Effective','True or False',$dpermission['Effective']);
               $result[$drow]['Default'] = $this->postprocessterm('Default','True or False',$dpermission['Default']);                            
            }
         }
      }      
      return $result;   
   }
   function loaddata($id,$col=null,$innerdata=true) {
      $query = $this->dbhandle->select();
      $query->from(array('def' => $this->def),array('Domain','Label','Description','Default'=>'Value'));
      if (isset($this->rootdomain)) $join = "def.Domain = '$this->rootdomain' And def.Label = per.Label";
      else $join = "def.Domain = per.Domain And def.Label = per.Label"; 
      $query->joinLeft(array('per' => $this->per),$join,array('USR_Permissions_ID','User_Id','Effective'=>'Value'));
      $query->where("per.USR_Permissions_Id = ?",$id);
      $this->data = $this->dbhandle->fetchRow($query);
      $this->data['Effective'] = $this->postprocessterm('Effective','True or False',$this->data['Effective']);
      $this->data['Default'] = $this->postprocessterm('Default','True or False',$this->data['Default']);
      if (isset($this->alias)) $this->data['Domain'] = $this->alias;
   }
   function update($col,$val) {
      $this->data['Value'] = $this->data['Effective'];
      if (isset($this->instance)) $this->data['Domain'] = $this->instance;
      unset($this->data['Default']);
      unset($this->data['Effective']);
      unset($this->data['USR_Permissions_ID']);
      $where = $this->dbhandle->quoteInto("$col = ?",$val);
      $status = $this->dbhandle->update($this->per,$this->data,$where);
      $this->loaddata($val,$col);
      $this->data[$col] = $val;
      return $status;
   }
   function insert() {
      $this->data['Value'] = $this->data['Effective'];
      $this->data['CreatedBy'] = $this->user->getid();
      unset($this->data['Default']);
      unset($this->data['Effective']);
      $status = $this->dbhandle->insert($this->per,$this->data);
      return $status;   
   } 
   function exists() {
      return true;
   }
   function create() {
      return true;
   }
   protected $domain = 'users';
   protected $permissions = array(
      'Def'    => 'set_perm',
      'List'   => 'edit_perm',
      'View'   => 'edit_perm',
      'Add'    => 'NO-ONE'
   );  
}
?>