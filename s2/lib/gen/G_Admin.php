<?php
/*
GAdm_Project - Costed Project Work ie Project Tender

GAdm_Job - Costed Work ie Data Search

GAdm_Cost
GAdm_Cost_Standard_Element - Hourly Rate / Day Rate / Data Search Element
GAdm_Cost_Custom_Element - Description and amount - One off costs .

GAdm_Quote

GAdm_Invoice
*/
class GAdm_Job extends DBT {
   protected $tablename = 'GAdm_Job';
   protected $displayname = 'Job';
   protected $columns = array(
      'GAdm_Job_ID'     => 'Unique Key',
      'Name'            => 'Short Text', 
      'Ref'             => 'Short Text',   
      'Client'          => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"]}',
      'Dated'           => '{"DataType":"Date","Mandatory":1,"Derives":[{"Column":"Target_Date","Method":"s2_targetdate","Params":[{"Type":"Value"},{"Type":"Static","Value":"+5WD"}]}]}',
      'Target_Date'     => '{"DataType":"Date","Derives":[{"Column":"Completed","Method":"s2_getcompletionstatus","Params":[{"Type":"Value"},{"Type":"Column","Value":"Completion_Date"}]}]}',
      'Completion_Date' => '{"DataType":"Date","Derives":[{"Column":"Completed","Method":"s2_getcompletionstatus","Params":[{"Type":"Column","Value":"Target_Date"},{"Type":"Value"}]}]}',
      'Is_Assigned'     => 'Short Text',
      'Assigned_To'     => '{"DataType":"INTERFACE","Domain":"users","ValidTypes":["S2_User"]}',
      'Completed'       => 'Short Text'
   );
   protected $domain = 'admin';
   protected $permissions = array(
      'Def'    => 'Registered User'
   );
}

/*
class GAdm_Job_Assignment extends DBT {
   protected $tablename = 'GAdm_Job_Assignment';
   protected $displayname = 'Job Assignment';
   protected $columns = array(
      'GAdm_Job_ID'     => 'Unique Key',
      'Name'            => 'Short Text', 
      'Ref'             => 'Short Text',   
      'Client'          => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"]}',
      'Dated'           => '{"DataType":"Date","Mandatory":1,"Derives":[{"Column":"Target_Date","DerivedUsing":"s2_targetdate","DParams":[{"Type":"Value"},{"Type":"Static","Value":"+5WD"}]}]}',
      'Target_Date'     => '{"DataType":"Date"}',
      'Completion_Date' => '{"DataType":"Date"}',
      'Is_Assigned'     => 'Short Text',
      'Assigned_To'     => '{"DataType":"INTERFACE","Domain":"users","ValidTypes":["S2_User"]}',
      'Completed'       => 'Short Text'
   );
   protected $domain = 'admin';
   protected $permissions = array(
      'Def'    => 'Registered User'
   );
}
*/   
?>