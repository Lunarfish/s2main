var key;
var iv;
function enc_makekey(len) {
   var k,seed,ran,characters;
   if (!len) len = 8;
   characters = new Array( 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 
                           'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 
                           'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 
                           'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                           'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                           'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 
                           'w', 'x', 'y', 'z', '0', '1', '2', '3', 
                           '4', '5', '6', '7', '8', '9');
   k = '';
   while (k.length<len) {
      ran = (Math.floor(Math.random()*(characters.length+1)) % characters.length);
      k += characters[ran];
   }
   if (k.length > len) k = k.substring(0,len);
   return k;  
}
function enc_getexternaldatakeys() {
   var k = new S2AB();
   k.cd(readCookie('PHPSESSID'));
   return JSON.parse(k.bd());
}
function enc_getencrypted(clear) {
   var ko,key,iv;
   ko = enc_getexternaldatakeys();
   key = ko.ab;
   iv =  ko.bc;
   var encoded = des(key,clear,1,1,iv,0);
   encoded = stringToHex(encoded);
   return encoded;
}
function enc_decryptit(encoded) {
   var ko,key,iv;
   ko = enc_getexternaldatakeys();
   key = ko.ab;
   iv =  ko.bc;
   var clear = des(key,hexToString(encoded),0,1,iv,0);
   /* if it is json remove any trailing characters */
   if (clear.match(/\}/)) clear = clear.substring(0,clear.lastIndexOf('}')+1);
   return clear;
}
function enc_getauthtest() {
   var k1,k2,k3;
   k1 = enc_makekey();
   k2 = enc_getencrypted(k1);
   k3 = enc_plait(k1,k2);
   return k3;     
}
function enc_unravel(k3) {
   var k1,k2,i,k2d;
   i = 0;k1='';k2='';
   while(i < k3.length) {
      k1 += k3.substr((i*3),1);
      k2 += k3.substr(((i*3)+1),2);
      i++; 
   }
   k2d = enc_decryptit(k2);
   return (k1 == k2d);
}
function enc_plait(k1,k2) {
   var i,k3;
   k3 = '';
   for(i=0;i<k1.length;i++) k3 += k1.substr(i,1) + k2.substr((i*2),2);
   return k3;   
}
