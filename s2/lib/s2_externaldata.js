function s2_getparamlist(params) {
   var p,param,props,val;
   props = new Object();
   for(p in params) {
      param = params[p];
      val = param.Value;
      val = (param.Encrypt)?cdm_decrypt(val,"AB"):val;      
      props[param.Name] = val;
   }
   return props;   
}

function s2_getexternaldata(jobj) {
   var text = 'Retrieving data from an external source.\nThis may take a little more time.';
   alert(text);
   var exts = jobj.External;
   var sid = s2_getcookie('PHPSESSID');
   var exti,i,j,props,svr,url,req,authority,source,p,param,value;
   for(i in exts) {
      exti = exts[i];
      req = new Object();
      svr = exti.Server;
      url = exti.Target;
      req.target = 'http://'+svr+url;
      req.request = s2_getparamlist(exti.Parameters);
      if (exti.Encrypt) req.etype = "AB";
      req.sync = true;
      req.svr = svr;
      req.sid = sid;
      //req.responder = s2_drawexternaldata;
      var res = snc_send(req);
      var dec = (req.etype)?cdm_decrypt(res,req.etype):res;
      if (dec) {
         var ed = JSON.parse(dec);
         delete jobj.LinkedData;
         delete jobj.External;
         //s2_draw(JSON.stringify(jobj));
         s2_drawexternaldata(ed,exti.Name);
      }
      completed();
   }
}
function s2_drawexternaldata(jobj,section) {
   forms++
   var dialog,i,j,prop,tabname,colname,coldefs,text,ftext,input;
   var table,thead,tbody,tr,th,td,node,a,id,lid,span,h3,listitem;
   dialog = document.getElementById('incontentdialog');
   if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
   /*
   h2 = document.createElement('H2');
   text = (jobj.Title)?jobj.Title:section;
   h2.appendChild(document.createTextNode(text));
   dialog.appendChild(h2);
   */
//alert(JSON.stringify(jobj.Tables));
   shrinknodes = new Array();
   if (jobj.Status == 0) alert(unescape(jobj.Message));
   else {
      for (tabname in jobj.Tables) {
      
         text = tabname.replace(/\_/g,' ');
         h2 = document.createElement('H2');
         h2.appendChild(document.createTextNode(text));
         dialog.appendChild(h2);
         
         if (jobj.Columns[tabname]) coldefs = jobj.Columns[tabname];
         else {
            coldefs = new Array();
            for(prop in jobj.Tables[tabname][0]) coldefs[prop] = prop;
         }
         table = document.createElement('TABLE');
         table.id = 'Form-'+forms+'_IType-R6_Table-'+tabname;
         table.className = 'extform';
         thead = document.createElement('THEAD');
         tr = document.createElement('TR');
         for (prop in coldefs) {
            colname = coldefs[prop];
            th = document.createElement('TH');
            th.appendChild(document.createTextNode(colname));
            tr.appendChild(th);      
         }
         thead.appendChild(tr);
         table.appendChild(thead);
         tbody = document.createElement('TBODY');
         for (i in jobj.Tables[tabname]) {
            listitem = jobj.Tables[tabname][i];
            tr = document.createElement('TR');
            for (prop in coldefs) {
               td = document.createElement('TD');
               //td.appendChild(document.createTextNode(listitem[prop]));
               switch (typeof listitem[prop]) {
               case 'object': {
                  text = null;
                  for (j in listitem[prop]) {
                     if (!text) text = listitem[prop][j];
                     else text = text + '<br/>' + listitem[prop][j];
                  }
                  td.innerHTML = text;
               }break;
               case 'array': {
                  text = listitem[prop].join('<br/>');
                  td.innerHTML = text;                           
               }break;
               default: {
                  if (listitem[prop]) {
                     if (typeof listitem[prop] == 'string') {
                        Encoder.EncodeType = "entity";
                        text = Encoder.htmlDecode(listitem[prop]);
                     } else text = listitem[prop];
                     td.innerHTML = text;
                  }
               }break;
               }
               tr.appendChild(td);      
            }   
            tbody.appendChild(tr);  
         }
         table.appendChild(tbody);
         dialog.appendChild(table);
      }
   }
}