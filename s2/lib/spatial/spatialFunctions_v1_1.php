<?php
include_once('coordinateFunctions.php');
class BoundingBox {
   var $north;
   var $east;
   var $south;
   var $west;
   var $area;
   var $centroid;
   function BoundingBox ($north=null,$east=null,$south=null,$west=null) {
      $this->north = $north;
      $this->east = $east;
      $this->south = $south;
      $this->west = $west;
   }
   function fromWKT ($wkt) {
      $xs = array();
      $ys = array();
      $wkt = str_replace(array('POLYGON((','))'),'',$wkt);
      $coords = preg_split('/\,/',$wkt);
      unset($wkt);
      while ($c = array_shift($coords)) {
         $xy = preg_split('/\s+/', $c);
         $this->north = ($this->north!=null&&$this->north>$xy[1])?$this->north:$xy[1];
         $this->south = ($this->south!=null&&$this->south<$xy[1])?$this->south:$xy[1];
         $this->east =  ($this->east!=null&&$this->east >$xy[0])?$this->east :$xy[0];
         $this->west =  ($this->west!=null&&$this->west <$xy[0])?$this->west :$xy[0];
         
         unset($xy);
         unset($c);
      }
      unset($coords);
   }
   function printItem() {
      print "<table>";
      print "<tr><td/><td><b>N</b><input type='text' class='grid' id='bbnorth' value='" . floor($this->north) . "'/></td>";
     /* print "<td/>"; */print "<td style='text-align: center;'><em>North East</em> <img src='./images/maps/question.gif' onclick='findGR(\"north\",\"east\");'/></td>";
      print "</tr>";
      print "<tr><td><b>W</b><input type='text' class='grid' id='bbwest' value='" . floor($this->west) . "'/></td><td/><td><b>E</b><input type='text' class='grid' id='bbeast' value='" . floor($this->east) . "'/></td></tr>";
      print "<tr>";
     /* print "<td/>"; */print "<td style='text-align: center;'><em>South West</em> <img src='./images/maps/question.gif' onclick='findGR(\"south\",\"west\");'/></td>";
      print "<td><b>S</b><input type='text' class='grid' id='bbsouth' value='" . floor($this->south) . "'/></td><td/>";
      print "</tr>";
      print "</table>";
   }
   function asWKT() {
      $wkt = "POLYGON(($this->west $this->north,$this->east $this->north," .
            "$this->east $this->south,$this->west $this->south," .
            "$this->west $this->north))";
      return $wkt;
   }
   function asGridReferenceWKT() {
      $sw = wgs84_to_osgb36(new DegreesDecimalCoordinate($this->south,$this->west));
      $nw = wgs84_to_osgb36(new DegreesDecimalCoordinate($this->north,$this->west));
      $se = wgs84_to_osgb36(new DegreesDecimalCoordinate($this->south,$this->east));
      $ne = wgs84_to_osgb36(new DegreesDecimalCoordinate($this->north,$this->east));
      $wkt = "POLYGON(($nw->easting $nw->northing,$ne->easting $ne->northing," .
            "$se->easting $se->northing,$sw->easting $sw->northing," .
            "$nw->easting $nw->northing))";
      return $wkt;
   }
   function asLatLongWKT() {
      $sw = conv_uk_ings_to_ll(new GridReference($this->west,$this->south));
      $nw = conv_uk_ings_to_ll(new GridReference($this->west,$this->north));
      $se = conv_uk_ings_to_ll(new GridReference($this->east,$this->south));
      $ne = conv_uk_ings_to_ll(new GridReference($this->east,$this->north));
      $wkt = "POLYGON(($nw->longitude $nw->latitude,$ne->longitude $ne->latitude," .
            "$se->longitude $se->latitude,$sw->longitude $sw->latitude," .
            "$nw->longitude $nw->latitude))";
      return $wkt;
   }
   function calculateSpatial() {
      $wkt = $this->asWKT();
      $loc = new Location();
      $loc->polygon = $wkt;
      $loc->calculateSpatial();
      $this->area = $loc->area;
      $this->centroid = $loc->centroid;
   }
   function bbintersects($bb2) {
      /* checks for an intersection between two bounding boxes. */
      $boxes = array($this,$bb2);
      $total = unionBoxes($boxes);
      /*
         either x or y must overlap somewhere for an intersect which means that 
         the combined height or the combined width must be less than the total 
         of adding the heights or widths together.
      */
      $intersects = (($this->height + $bb2->height) < $total->height());
      $intersects = $intersects || (($this->width + $bb2->width) < $total->width());
      return $intersects;
   }
   function area() {    
      return ($this->north - $this->south) 
               * ($this->east - $this->west); 
   }
   function height() {  return $this->north -   $this->south;  }
   function width()  {  return $this->east -    $this->west;   }
}
function getWKT($lid) {
   $query = "SELECT AsText(box) as box from locations WHERE id='$lid';";
   $result = mysql_query($query);
   while ($row = fetch_associative_row($result)) {
      $wkt = $row['box'];
   }
   return $wkt;
}
function wktToGMLCoordinates($wkt,$src=null,$dst=null) {
   $gml = "<gml:coordinates decimal='.' cs=',' ts=' ' >";
      $cstring = $wkt;
      $cstring = str_replace("POLYGON((","",$cstring);
      $cstring = str_replace("))","",$cstring);
      $cstring = str_replace(",",":",$cstring);
      $cstring = str_replace(" ",",",$cstring);
      $cstring = str_replace(":"," ",$cstring);
   if ($src && $dst && $src!=$dst) {
      $points = split(" ",$cstring);
      $newpoints = array();
      foreach ($points as $point) {
         list($x,$y) = split(",",$point);
         if ($src == 'OSGB36') {
            $gr = new GridReference($x,$y);
            $ll = conv_uk_ings_to_ll($gr);
            $newpoints[] = "$ll->longitude,$ll->latitude";
         } else {
            $ll = new DegreesDecimalCoordinate($y,$x);
            $gr = wgs84_to_osgb36($ll);
            $newpoints[] = "$gr->easting,$gr->northing";
         }
      }
      $cstring = join(" ",$newpoints);
   }
   $gml .= $cstring;
   $gml .= "</gml:coordinates>";
   return $gml;
}
function gmlCoordinatesToWKT($gml) {
   $wkt = $gml;
   $wkt = str_replace(",",":",$wkt);
   $wkt = str_replace(" ",",",$wkt);
   $wkt = str_replace(":"," ",$wkt);
   $wkt = "POLYGON((" . $wkt . "))";
   return $wkt;
}
function convertPolygon($src,$infmt,$outfmt,$inprj=null,$outprj=null) {
   $gmlin = ($infmt == 'gml');
   $gmlout = ($outfmt == 'gml');
   $top = ($gmlout)?"<gml:coordinates decimal='.' cs=',' ts=' ' >":"POLYGON((";
      $cstring = $src;
      $intop = ($gmlin)?"<gml:coordinates decimal='.' cs=',' ts=' ' >":"POLYGON((";
      $inbtm = ($gmlin)?"</gml:coordinates>":"))";
      $cstring = str_replace($intop,"",$cstring);
      $cstring = str_replace($inbtm,"",$cstring);
   if ($infmt != $outfmt) {
      $cstring = str_replace(",",":",$cstring);
      $cstring = str_replace(" ",",",$cstring);
      $cstring = str_replace(":"," ",$cstring);
   }   
   if ($inprj && $outprj && $inprj!=$outprj) {
      $cosep = ($gmlout)?",":" ";
      $ptsep = ($gmlout)?" ":",";
      $points = ($gmlin)?split(",",$cstring):split(" ",$cstring);
      $newpoints = array();
      foreach ($points as $point) {
         list($x,$y) = ($gmlin)?split(" ",$point):split(",",$point);
         if ($inprj == 'OSGB36') {
            $gr = new GridReference($x,$y);
            $ll = conv_uk_ings_to_ll($gr);
            $newpoints[] = $ll->longitude . $cosep . $ll->latitude;
         } else {
            $ll = new DegreesDecimalCoordinate($y,$x);
            $gr = wgs84_to_osgb36($ll);
            $newpoints[] = $gr->easting . $cosep . $gr->northing;
         }
      }
      $cstring = join($ptsep,$newpoints);
   }
   $btm = ($outfmt == 'gml')?"</gml:coordinates>":"))";
   $poly = $top . $cstring . $btm;
   return $poly;
}
function gmlBBOXToWKT($gml) {
   $c1 = split(" ",$gml);
   $sw = split(",",$c1[0]);
   $ne = split(",",$c1[1]);
   $bbox = new BoundingBox($ne[1],$ne[0],$sw[1],$sw[0]);
   return $bbox->asWKT();
}

function getBinary($lid) {
   $query = "SELECT AsBinary(box) as box from locations WHERE id='$lid';";
   $result = mysql_query($query);
   while ($row = fetch_associative_row($result)) {
      $binary = $row['box'];
   }
   return $binary;
}
function getBoundingBox($wkt) {
   $bb = new BoundingBox();
   $bb->fromWKT($wkt);
   return $bb;
}

function getBoxCoordinates($lid) {
   $query = "SELECT AsText(box) as box from locations WHERE id='$lid';";
   $result = mysql_query($query);
   $bb = new BoundingBox();
   while ($row = fetch_associative_row($result)) {
      $bb->fromWKT($row['box']);
   }
   $bb->calculateSpatial();
   return $bb;
}
function unionBoxes($boxes) {
   $union = new BoundingBox();
   for ($i=0;$i<count($boxes);$i++) {
      $current = $boxes[$i];
      $union->north = ($union->north==null)
                        ?$current->north
                        :max($union->north,$current->north);
      $union->east = ($union->east==null)
                        ?$current->east
                        :max($union->east,$current->east);
      $union->south = ($union->south==null)
                        ?$current->south
                        :min($union->south,$current->south);
      $union->west = ($union->west==null)
                        ?$current->west
                        :min($union->west,$current->west);
   }
   return $union;
}
function epsgFromCoordSysString($csys) {
   $epsg = 0;
   switch ($csys) {
   // WGS 84 do nothing
   case 'CoordSys Earth Projection 1, 104': {
      $epsg = 4326;
   }break;
   // UK NGR EPSG:27700 convert to WGS 84
   case 'CoordSys Earth Projection 8, 79, "m", -2, 49, 0.9996012717, 400000, -100000 Bounds (-7845061.1011, -15524202.1641) (8645061.1011, 4470074.53373)': {
      $epsg = 27700;
   }break;
   }
   return $epsg;
}
function convertMIFRegionsToWKT($mifarray) {
   $polygons = array();
   $epsg = 0;
   for ($i=0;$i<=count($mifarray);$i++) {
      $mifline = $mifarray[$i];
      if (preg_match("/CoordSys/",$mifline)) $epsg = epsgFromCoordSysString(trim($mifline));
      if (preg_match("/Region/",$mifline)) {
         $bits = preg_split('/\s+/',trim($mifline));
         $pathcount = array_pop($bits);
         $paths = array();
         $i++;
         for($p=0;$p<$pathcount;$p++) {
            //$i++;
            $mifline = $mifarray[$i];
            $mifline = trim($mifline);
            $vertices = $mifline;
            $path = "(";
            $current = ++$i;
            $endOfRegion = $current+$vertices;
            for ($i;$i<$endOfRegion;$i++) {
               $mifarray[$i] = trim($mifarray[$i]);
               if ($i > $current) {$path .= ",";}
               $path .= $mifarray[$i];
               /*
               switch($epsg) {
               case 4326:  $path .= $mifarray[$i]; break;
               case 27700: {
                  $xy = preg_split('/\s+/',$mifarray[$i]);
                  $ll = conv_uk_ings_to_ll(new GridReference($xy[0],$xy[1]));
                  $path .= "$ll->longitude $ll->latitude";                   
               }break;
               }
               */ 
            }
            $path .= ")";
            $paths[] = $path;
         }
         $polygon = "POLYGON(".join(",",$paths).")";
         if ($epsg != 4326) $polygon = "SRID=$epsg;$polygon";
         $polygons[] =  $polygon;
      }
   }
   $ret = new stdClass();
   $ret->status = ($epsg > 0);
   $ret->epsg = $epsg;
   $ret->polygons = $polygons;
   return $ret;
}
function convertE00RegionsToWKT($arcarray) {
/*
ARC  3
         1         1         1         1         1         2         6
 4.48676103380609E+05 4.39055690871048E+05
 4.56852681004203E+05 4.39566724589167E+05
 4.60685447127416E+05 4.34520239130603E+05
 4.49570412178001E+05 4.25321602213031E+05
 4.42607557018855E+05 4.31198509965686E+05
 4.48676103380609E+05 4.39055690871048E+05
         2         2         2         2         1         3         4
 4.59790775547336E+05 4.40968553319867E+05
 4.67264676083650E+05 4.39754840741479E+05
 4.61962673052465E+05 4.33111372414506E+05
 4.59790775547336E+05 4.40968553319867E+05
         3         3         3         3         1         4        11
 4.41041053125074E+05 4.44450346666889E+05
 4.46662733665270E+05 4.40106310134412E+05
 4.40529991257783E+05 4.30907103379954E+05
 4.49729170829502E+05 4.22474557171197E+05
 4.61228128804029E+05 4.31418157092350E+05
 4.61739190671320E+05 4.29629469098962E+05
 4.69660715574813E+05 4.40106310134412E+05
 4.74260338340917E+05 4.25285432566485E+05
 4.42574238726945E+05 4.15064118387234E+05
 4.33119528221581E+05 4.30140522811359E+05
 4.41041053125074E+05 4.44450346666889E+05
        -1         0         0         0         0         0         0
*/
   $polygons = array();
   for ($i=0;$i<=count($arcarray);$i++) {
      $arcline = $arcarray[$i];
//print "<p>$arcline</p>";
      if (preg_match("/ARC/",$arcline)) {
//print "<h3>$arcline</h3>";
         $i++;
         $arcline = $arcarray[$i];
         $arcline = preg_split("/\s+/",trim($arcline));
         while ($arcline[0] > 0) {
            $vertices = $arcline[count($arcline)-1];
            $polygon = "POLYGON((";
            $current = ++$i;
            $endOfRegion = $current+$vertices;
            for ($i;$i<$endOfRegion;$i++) {
               $arcline = trim($arcarray[$i]);
               $coords = preg_split("/\s+/",$arcline);
               $x = $coords[0]; $y = $coords[1];
               $x = split("E",$x);
               $x = $x[0] * pow(10,$x[1]);
               $x = floor(10*$x)/10;
               $y = split("E",$y);
               $y = $y[0] * pow(10,$y[1]);
               $y = floor(10*$y)/10;
               if ($i > $current) {
                  $polygon .= ",";
               }
               $polygon .= "$x $y";
            }
            $polygon .= "$x $y";
            $polygon .= "))";
            $polygons[] =  $polygon;
            $arcline = $arcarray[$i];
            $arcline = preg_split("/\s+/",trim($arcline));
         }
      }
   }
   return $polygons;
}

function cloneAttribute($a) {
   $a2 = new Attribute($a->name,$a->type,$a->value);
   $a2->region = $a->region;
   $a2->id = $a->id;
   return $a2;
}
function getCSVValues($string) {
   //$string = preg_replace("/^\"/","",$string);
   //$string = preg_replace("/\"$/","",$string);
   $elements = array();
   if (preg_match("/\"/",$string)) {
      $bits = split('"',$string);
      for ($i=0;$i<count($bits);$i++) {
         $bit = $bits[$i];
         $bit = preg_replace("/^\s+$/","",$bit);
         if ($i%2==1) {
            $elements[] = $bit;
/*
print "<p>inside quotes %$bit%<br/>";
print_r($elements);
print "</p>";
*/
         } else if ($bit != ',' && strlen($bit)>0) {
            $s = $bit;
            $s = preg_replace("/^,/","",$s);
            $s = preg_replace("/,$/","",$s);
            $elements = array_merge($elements,split(",",$s));
/*
print "<p>outside quotes %$bit%<br/>";
print_r($elements);
print "</p>";
*/
         }
      }
   } else {
      $elements = split(",",$string);
   }
   return $elements;
}
class Layer {
   var $table = 'region_layers';
   var $id;
   var $layername;
   var $bb;
   var $tablename;
   var $resource;
   var $regions;
   var $category;
   var $tier;
   var $statutory;
   function Layer($layername,$tablename) {
      $this->layername = $layername;
      $this->tablename = $tablename;
   }
   function insert() {
      $bb = $this->bb;
      $wkt = $bb->asWKT();
      $query = "INSERT INTO $this->table (id,layername,tablename,bb,scope,statutory,tier,category) 
         VALUES ('$this->resource','$this->layername','$this->tablename',GeomFromText('$wkt'),'$this->scope',$this->statutory,$this->tier,$this->category);";
      $result = mysql_query($query);
      return $result;
   }
   function createTable() {
      $query = "CREATE TABLE $this->tablename (
         id varchar(36) NOT NULL,
         name text,
         region geometry NOT NULL,
         box geometry NOT NULL,
         PRIMARY KEY(id),
         SPATIAL INDEX(box)
         ) ENGINE=MyISAM;";
      $result = mysql_query($query);
//print "<p><em>$query</em> $result</p>";
      return $result;
   }
}
class Region {
   var $table = 'mif_regions';
   var $id;
   var $name;
   var $shape;
   var $box;
   var $attributes;
   function attributeTable($exclude=null) {
      $exclude = split(",",$exclude);
      $cols = 5;
      $lines = ceil(count($this->attributes)/$cols);
      $htm = "<table>";
      for($i=0;$i<$lines;$i++) {
         $start = $i * $cols;
         $htm .= $this->attributeHeadings($start,$cols,$exclude);
         $htm .= $this->attributeData($start,$cols,$exclude);
      }
      $htm .= "</table>";
      return $htm;
   }
   function attributeTable2($exclude=null) {
      $exclude = split(",",$exclude);
      $cols = 2;
      $htm = "<div class='searchsection'><h4>attribute table</h4><table><tr>";
      $count = 0;
      for ($i=0;$i<count($this->attributes);$i++) {
         if (!in_array($i,$exclude)) {
            $a = $this->attributes[$i];
            $name = preg_replace("/_/"," ",$a->name);
            $style = (strlen($a->value)>20)?" style='font-size: smaller;'":"";
            $htm .= "<th>$name</th><td $style>$a->value</td>";
            $count++;
         }
         if (($count%$cols == 0) && (count($this->attributes)>5)) $htm.= "</tr><tr>";
      }
      $htm.= "</tr></table></div>";
      return $htm;
   }
   function attributeHeadings($start=0,$cols=null,$exclude=null) {
      $htm = "<tr>";
      $atts = $this->attributes;
      $end = ($start+$cols < count($atts))?$start+$cols:count($atts);
      for ($att=$start;$att<$end;$att++) {
         //if (!in_array($att,$exclude)) {
            $a = $atts[$att];
            $htm .= "<th>$a->name</th>";
         //}
      }
      $htm .= "</tr>";
      return $htm;
   }
   function attributeData($start=0,$cols=null,$exclude=null) {
      $htm = "<tr>";
      $atts = $this->attributes;
      $end = ($start+$cols < count($atts))?$start+$cols:count($atts);
      for ($att=$start;$att<$end;$att++) {
         //if (!in_array($att,$exclude)) {
            $a = $atts[$att];
            $style = (strlen($a->value)>20)?" style='font-size: smaller;'":"";
            $htm .= "<td class='attribute' $style>$a->value</td>";
         //}
      }
      $htm .= "</tr>";
      return $htm;
   }
   function insert() {
      $this->id = getUUID();
      $query = "INSERT INTO $this->table (id,name,region,box) 
         VALUES ('$this->id','$this->name',GeomFromText('$this->shape'),Envelope(region));";
      $result = mysql_query($query);
      for ($i=0;$i<count($this->attributes);$i++) {
         $aid = getUUID();
         $a = $this->attributes[$i];
         $a->id = $aid;
         $a->region = $this->id;
         $a->insert();
      }
   }
   function getAttributes() {
      $a = new Attribute('temp');
      $this->attributes = $a->getAttributesForRegion($this->id);
   }
   function simplify() {
      $maxv = 200;
      $wkt = $this->shape;
      $wkt = substr($wkt,9);
      $wkt = substr($wkt,0,strlen($wkt)-2);
//print "<p>$wkt</p>";
      $oldvertices = split(",",$wkt);
      if (count($oldvertices)>$maxv) {
         $newvertices = array();
         $gap = floor(count($oldvertices)/$maxv);
         for ($i=0;$i<count($oldvertices);$i+=$gap) $newvertices[] = $oldvertices[$i];
         $wkt = "POLYGON((" . join(",",$newvertices) . "))";
//print "<p>$wkt</p>";
      } else $wkt = $this->shape;
      return $wkt;
   }
}
class Attribute {
   var $table = 'mif_attributes';
   var $region;
   var $id;
   var $name;
   var $type;
   var $value;
   function Attribute($name,$type=null,$value=null) {
      $this->name = $name;
      $this->type = $type;
      $this->value = $value;
   }
   function htm() {
      $htm = "<h4>$this->name</h4>";
      $htm .= "<p>$this->value ( $this->type )</p>";
      return $htm;
   }
   function tr() {
      return "<tr><td>$this->name</td><td>$this->type</td><td>$this->value</td></tr>";
   }
   function th() {
      return "<tr><th>name</th><th>type</th><th>value</th></tr>";
   }
   function insert() {
      $query = "INSERT INTO $this->table (region,name,attribute,type) 
         VALUES ('$this->region','$this->name','$this->id','$this->type');";
      $result = mysql_query($query);
      $value = $this->value;
      if ($this->type == 'text') $value = "'$value'";
      $query = "INSERT INTO mif_$this->type (attribute,value) 
            VALUES ('$this->id',$value);";
      $result = mysql_query($query);
   }
   function getAttributesForRegion($region) {
      $query = "SELECT name,attribute,type FROM $this->table WHERE region='$region';";
      $result = mysql_query($query);
      $attributes = array();
      while ($row = mysql_fetch_assoc($result)) {
         $name = $row['name'];
         $type = $row['type'];
         $attribute = $row['attribute'];
         $value = '';
         if ($type) {
            $query = "SELECT value FROM mif_$type WHERE attribute='$attribute';";
            $vresult = mysql_query($query);
            if ($vresult){
            while ($vrow = mysql_fetch_assoc($vresult)) {
               $value = $vrow['value'];
            }
            }
         }
         $a = new Attribute($name,$type,$value);
         $attributes[] = $a;
      }
      return $attributes;
   }
}
function getMIFColumnHeadings($mifarray) {
   $headings = array();
   for ($i=0;$i<=count($mifarray);$i++) {
      $mifline = $mifarray[$i];
      if (preg_match("/Columns/",$mifline)) {
         $bits = preg_split("/\s+/", trim($mifline));
         $vertices = $bits[count($bits)-1];
         $current = ++$i;
         $endOfRegion = $current+$vertices;
         for ($i;$i<$endOfRegion;$i++) {
            $mifarray[$i] = trim($mifarray[$i]);
            $nandt = split(" ", $mifarray[$i]);
            $headings[] = $nandt[0];
         }
      }
   }
   return $headings;
}
function getMIFAttributes($mifarray) {
   $atts = array();
   for ($i=0;$i<=count($mifarray);$i++) {
      $mifline = $mifarray[$i];
      if (preg_match("/Columns/",$mifline)) {
         $bits = split(" ", $mifline);
         $vertices = $bits[count($bits)-1];
         $current = ++$i;
         $endOfRegion = $current+$vertices;
         for ($i;$i<$endOfRegion;$i++) {
            $mifarray[$i] = trim($mifarray[$i]);
            $nandt = split(" ", $mifarray[$i]);
            $name = $nandt[0];
            $type = $nandt[1];
            $type = strtolower($type);
            if (preg_match("/decimal/",$type)) $type = "float";
            if (preg_match("/char/",$type)) $type = "text";
            if (preg_match("/int/",$type)) $type = "int";
            $atts[] = new Attribute($name,$type);
         }
      }
   }
   return $atts;
}
function getE00Attributes($filename,$arcarray) {
/*
IFO  3
E00TEST_REGIO.AAT               XX  10  10  62         3
FNODE#            4-1   14-1   5-1 50-1  -1  -1-1                   1-
TNODE#            4-1   54-1   5-1 50-1  -1  -1-1                   2-
LPOLY#            4-1   94-1   5-1 50-1  -1  -1-1                   3-
RPOLY#            4-1  134-1   5-1 50-1  -1  -1-1                   4-
LENGTH            8-1  174-1  18 5 60-1  -1  -1-1                   5-
E00TEST_REGIO#    4-1  254-1   5-1 50-1  -1  -1-1                   6-
E00TEST_REGIO-ID  4-1  294-1   5-1 50-1  -1  -1-1                   7-
FIELD1           10-1  334-1  10-1 20-1  -1  -1-1                   8-
FIELD2           10-1  434-1  10-1 20-1  -1  -1-1                   9-
FIELD3           10-1  534-1  10-1 20-1  -1  -1-1                  10-
          1          1          1          2 4.79965746453394020E+04          1 
         1Value1    Value2    Value3    
          2          2          1          3 2.42234628432665010E+04          2 
         2Row2V1    Row2V2    Row2V3    
          3          3          1          4 1.43166130348484990E+05          3 
         3Row3V1    Row3V2    Row3V3 
*/
   $atts = array();
   $names = array();
   for ($i=0;$i<=count($arcarray);$i++) {
      $arcline = $arcarray[$i];
      if (preg_match("/IFO/",$arcline)) {
         $arcline = preg_split("/\s+/",trim($arcarray[++$i]));
         $polygons = $arcline[count($arcline)-1];
         $previousline = split(".",$filename);
         $previousline = strtoupper($previousline[0])."-ID";
         while (!preg_match("/$previousline/",$arcline)) {
            $arcline = $arcarray[++$i];
         }
         $arcline = $arcarray[++$i];
         while (!preg_match("/^\s+/",$arcline)) {
            $fields = preg_split("/\s+/",$arcline);
            $name = $fields[0];
            $type = 'text';
            $atts[] = new Attribute($name,$type);
            $arcline = $arcarray[++$i];
         }
         break;
      }
   }
   return $atts;
}
function getE00AttributeValues($region,$arcarray) {
   $vals = array();
    for ($i=0;$i<=count($arcarray);$i++) {
      $arcline = $arcarray[$i];
      if (preg_match("/IFO/",$arcline)) {
         $arcline = preg_split("/\s+/",trim($arcarray[++$i]));
         $polygons = $arcline[count($arcline)-1];
         while (!preg_match("/^\s+/",$arcline)) {
            $i++;
            $arcline = $arcarray[$i];
         }
         $arcline = $arcarray[$i];
         while (!preg_match("/^\s+$region/",$arcline)) {
            $i++;
            $arcline = $arcarray[$i];
         }
         $i++;
         $next = $region + 1;
         $arcline = $arcarray[$i];
         $i++;
         while ((!preg_match("/^\s+$next/",$arcarray[$i])) && (!preg_match("/.BND/",$arcarray[$i]))) {
            $arcline .= $arcarray[$i];
            $i++;
         }
         
         $vals = preg_split("/\s\s+/",trim($arcline));
         $vals[0] = substr($vals[0],strlen($region));
         break;
      }
   }
   return $vals;
}

class LocationRelationship {
   var $table = 'locations_cv';
   var $id;
   var $name;
   var $type;
   var $components;
   function LocationRelationship ($name,$type) {
      $this->name = $name;
      $this->type = $type;
   }
   function setId($id=0) {
      $this->id = ($id==0)? getUUID() : $id;
   }
   function create() {
      $table = $this->table;
      $result = true;
      for ($i=0;$i<count($this->components);$i++) {
         $comp = $this->components[$i];
         $id = $comp->id;
         $rel = $this->id;
         $name = $this->name;
         $gen = $comp->tier;
         $cid = $comp->component;
         $ctype = $this->type;
         $query = "INSERT INTO $table " .
                  "(id,relationship_id,name,generation," .
                  "component_id,component_type) " .
                  "VALUES ('$id','$rel','$name'," .
                  "$gen,'$cid','$ctype');";
         $result &= mysql_query($query);
      }
      return $result;    
   }
   function deleteComp($id) {
      $table = $this->table;
      $query = "SELECT relationship_id FROM $table " .
               "WHERE component_id = '$id'";
      $rresult = mysql_query($query);
      $result = true;
      while ($row = fetch_associative_row($rresult)) {
         $rel = $row['relationship_id'];
         $query = "SELECT COUNT(*) FROM $table " .
                  "WHERE relationship_id='$rel'";
         $cresult = mysql_query($query);
         if ($row = fetch_associative_row($cresult) && $row['COUNT(*)'] > 2) {
            $query = "DELETE FROM $table WHERE component_id='$id';";
         } else {
            $query = "DELETE FROM $table WHERE relationship_id='$rel';";
         }
         $result &= mysql_query($query);
      }
      return $result; 
   }
   function deleteRel() {
      $query = "DELETE FROM $table WHERE relationship_id='$rel';";
      $result = mysql_query($query);
      return $result;
   }
}
class LocationRelationshipComponent {
   var $id;
   var $component;
   var $tier;
   function LocationRelationshipComponent($component,$tier) {
      $this->component = $component;
      $this->tier = $tier;
   }
   function setId($id=null) {
      $this->id = ($id==null)?getUUID():$id;
   }
}
class Location {
   var $table = 'locations';
   var $name;
   var $id;
   var $polygon;
   var $box;
   var $area;
   var $centroid;
   function Location($name=null) {
      $this->name = $name;
   }
   function setId($id=null) {
      $this->id = ($id==null)?getUUID():$id;
   }
   function populate($id) {
      $this->id = $id;
      $query = "SELECT name,AsText(polygon) AS polygon, AsText(box) AS box, " .
               "Area(polygon) AS area, AsText(Centroid(polygon)) AS centroid " .
               "FROM $this->table WHERE id='$id';";
      $result = mysql_query($query);
      while ($row = fetch_associative_row($result)) {
         $this->name = $row['name'];
         $this->polygon = $row['polygon'];
         $this->box = $row['box'];
         $this->area = $row['area'];
         $this->centroid = $row['centroid'];
      }
   }
   function calculateSpatial() {
      $query = "SELECT Envelope(GeomFromText('$this->polygon')) AS box, " .
               "Area(GeomFromText('$this->polygon')) AS area, " .
               "AsText(Centroid(GeomFromText('$this->polygon'))) AS centroid;";
      $result = mysql_query($query);
      while ($row = fetch_associative_row($result)) {
         $this->box = $row['box'];
         $this->area = $row['area'];
         $c = new Point($row['centroid']);
         $c->getCoordsFromWKT();
         $this->centroid = $c;
      }
   }
}
class Point {
   var $wkt;
   var $e;
   var $n;
   function Point($wkt=null) {
      $this->wkt = $wkt;
   }
   function getCoordsFromWKT() {
      $wkt = $this->wkt;
      $wkt = str_replace(array('POINT(',')'),'',$wkt);
      $coords = split(' ',$wkt);
      $this->e = floor($coords[0]);
      $this->n = floor($coords[1]);
   }
   function getWKTFromCoords() {
      $wkt = "POINT($this->e,$this->n)";
      $this->wkt = $wkt;
   }
   function getDistance($p1,$p2) {
      $de = $p1->e - $p2->e;
      $dn = $p1->n - $p2->n;
      $dist = sqrt((pow($de,2) + pow($dn,2)));
//print "<p>$de $dn <em>$dist</em></p>";
      return floor($dist);
   }
}
function areaMToKM($m) {
   $area = ($m / (1000000));// m to km
   $area = floor($area*100) / 100;// round to 2 decimal places
   return $area;
}
function distMToKM($m) {
   $dist = ($m / (1000));// m to km
   $dist = floor($dist*100) / 100;// round to 2 decimal places
   return $dist;
}
class GeorefPoint {
   var $px;
   var $py;
   var $gx;
   var $gy;
   function GeorefPoint($px=null,$py=null,$gx=null,$gy=null) {
      $this->px = $px;
      $this->py = $py;
      $this->gx = $gx;
      $this->gy = $gy;
   }
   function htm() {
      return "<p>$this->gx ($this->px), $this->gy ($this->py)</p>";
   }
}
class GeorefImage {
   var $path;
   var $tab;
   var $name;
   var $image;
   var $width;
   var $height;
   var $xmperpix;
   var $ymperpix;
   var $realsx;
   var $realsy;
   var $pixelx;
   var $pixely;
   var $gpoints;
   var $realbb;
   function GeorefImage($tabfile=null,$name=null) {
      $path = split("/",$tabfile);
      $this->tab = array_pop($path);
      $this->path = join("/",$path);
      $this->name = $name;
   }
   function readTab() {
      $tabfile = $this->getTabPath();
      $ffile = fopen($tabfile, "rb");
      if ($ffile) {
      $tabsource = array();
      $realsx = array();
      $realsy = array();
      $pixelx = array();
      $pixely = array();

      $rxy;
      $pxy;
      $gpoints = array();
      while(!feof($ffile)) {
         //read file line by line into a new array element
         $line = fgets($ffile, 4096);
         $tabsource[] = $line;
         if (preg_match("/File/",$line)) {
            $words = preg_split("/\s+/",$line);
            for ($i=0;$i<count($words);$i++) {
               if ($words[$i] == "File") {
                  $imagefile = $words[$i+1];
                  $imagefile = str_replace("\"", "",$imagefile);
               }
            }
         } else if (preg_match("/^\s*\(/",$line)) {
            $words = preg_split("/\s+/",$line);
            $rxy = $words[1];
            $rxy = preg_replace("/[\(\)]/","",$rxy);
            $rbits = split(",",$rxy);
            if (!in_array($rbits[0],$realsx)) $realsx[] = $rbits[0];
            if (!in_array($rbits[1],$realsy)) $realsy[] = $rbits[1];
            $pxy = $words[2];
            $pxy = preg_replace("/[\(\)]/","",$pxy);
            $pbits = split(",",$pxy);
            if (!in_array($pbits[0],$pixelx)) $pixelx[] = $pbits[0];
            if (!in_array($pbits[1],$pixely)) $pixely[] = $pbits[1];
            $gpoints[] = new GeorefPoint($pbits[0],$pbits[1],$rbits[0],$rbits[1]);
         }
      }
      fclose ($ffile);
      } else {
         print "<h1>couldn&#39;t open $tabfile</h1>";
         exit;
      }
      $this->realsx = $realsx;
      $this->realsy = $realsy;
      $this->pixelx = $pixelx;
      $this->pixely = $pixely;
      $this->gpoints = $gpoints;
      $this->image = $imagefile;
   }
   function getImageDimensions() {
      $imagepath = $this->getImagePath();
      $dims = getimagesize($imagepath);
      /*for ($i=0;$i<count($dims);$i++) {
         print "<p>$i: $dims[$i]</p>";
      }*/
      $this->width = $dims[0];
      $this->height = $dims[1];
   }
   function getBoundingBox() {
      $gdx = abs(max($this->realsx) - min($this->realsx));
      $gdy = abs(max($this->realsy) - min($this->realsy));
      $pdx = abs(max($this->pixelx) - min($this->pixelx));
      $pdy = abs(max($this->pixely) - min($this->pixely));
      $xMetresPerPixel = $gdx / $pdx;
      $yMetresPerPixel = $gdy / $pdy;
      $bbmingx = (min($this->pixelx) > 0)? min($this->realsx) - ($xMetresPerPixel * min($this->pixelx)): min($this->realsx);
      $bbmaxgx = (max($this->pixelx) < $this->width)? max($this->realsx) + ($xMetresPerPixel * ($this->width - max($this->pixelx))): max($this->realsx); 
      /* y axes are reversed */
      $bbmingy = (max($this->pixely) < $this->height)? min($this->realsy) - ($yMetresPerPixel * ($this->height - max($this->pixely))): min($this->realsy);
      $bbmaxgy = (min($this->pixely) > 0)? max($this->realsy) + ($yMetresPerPixel * min($this->pixely)): max($this->realsy);

      //print "<p>e $bbmingx n $bbmingy , e $bbmaxgx n $bbmaxgy</p>";
      $this->realbb = new BoundingBox(floor($bbmaxgy),floor($bbmaxgx),floor($bbmingy),floor($bbmingx));
      $this->xmperpix = $xMetresPerPixel;
      $this->ymperpix = $yMetresPerPixel;
   }
   function getImagePath() {
      if ($this->path) {
         $path = $this->path . "/" . $this->image;
      } else {
         $path = $this->image;
      }
      //$path = str_replace("./","http://www.humber-edc.org.uk/",$path);
      return $path;
   }
   function getTabPath() {
      $path = $this->path . "/" . $this->tab;
      //$path = str_replace("./","http://www.humber-edc.org.uk/",$path);
      return $path;
   }
   function imgTagHTM($id=null) {
      $src = $this->getImagePath();
      $size = getimagesize($src);
      $width = $size[0];
      $height = $size[1];
      return "<img id='$id' class='map' src='$src' alt='$this->name' onclick='js_map_click(this,event);' style='position: absolute; width: ${width}px; height: ${height}px;'/>";
   }
}
class Background {
   var $table = 'location_backgrounds';
   var $join = 'locations';
   var $name;
   var $id;
   var $tabfile;
   function getSmallestBackground($box,$size) {
      $query = "SELECT l.name,b.id,b.w700,b.w350 from $this->table AS b " .
               "LEFT JOIN $this->join as l ON l.id = b.id " . 
               "WHERE MBRContains(l.box,GeomFromText('$box')) " .
               "ORDER BY Area(l.box);";
//print "<p>$query</p>";
      $result = mysql_query($query);
      if ($row = mysql_fetch_assoc($result)) {
         switch ($size) {
            case 'w700': $gi = new GeorefImage($row['w700'],$row['name']); break;
            case 'w350': $gi = new GeorefImage($row['w350'],$row['name']); break;
         }
         $gi->readTab();
         $gi->getImageDimensions();
         $gi->getBoundingBox();

//print "<p>" . $row['name'] . $row['background'] . "</p>";
      }
      return $gi;
   }
} 


function get_path_center($path) {
   $vx = count($path)-1;
   $tx = 0;$ty = 0;
   for ($i=0;$i<$vx;$i++) {
      $ptx = $path[$i];
      $cx = $ptx[0];
      $cy = $ptx[1];
      $tx+= $cx;
      $ty+= $cy;
   }
   $ax = $tx/$vx;
   $ay = $ty/$vx;
   return array($ax,$ay);
}

function wkt_to_gml_polygon($wkt,$wfsver=1.0) {
   $coordsaslist = true;
   $wkt = preg_replace('/POLYGON\({2}/','',$wkt);
   $wkt = preg_replace('/\){2}$/','',$wkt);
   $rings = preg_split('/\)\,\(/',$wkt);
   $gml = '<gml:Polygon srsName="http://www.opengis.net/gml/srs/epsg.xml#4326">';
   foreach($rings as $ring) {
      $path = array();
      $ring = preg_replace('/^\(/','',$ring);
      $ring = preg_replace('/\)$/','',$ring);
      $coords = preg_split('/\s*\,\s*/',$ring);
      //if ($coords[0] == $coords[count($coords)-1]) array_pop($coords);
      foreach($coords as $coord) {
         $pair = preg_split('/\s+/',$coord);
         $path[] = $pair;
      }
      switch($wfsver) {
      case 1.1: {
         $lring = ($coordsaslist)
            ?'<gml:LinearRing><gml:posList dimension="2">'.join(' ',$coords).'</gml:posList></gml:LinearRing>'
            :'<gml:LinearRing><gml:pos>'.join('</gml:pos><gml:pos>',$coords).'</gml:pos></gml:LinearRing>';
         $nothole = path_is_clockwise($path);
         $gml .= ($nothole)?'<gml:exterior>':'<gml:interior>';
         $gml .= $lring;
         $gml .= ($nothole)?'</gml:exterior>':'</gml:interior>';
      }break;
      default: {
         $lring = ($coordsaslist)
            ?'<gml:LinearRing><gml:coordinates decimal="." cs=" " ts=",">'.join(',',$coords).'</gml:coordinates></gml:LinearRing>'
            :'<gml:LinearRing><gml:coord>'.join('</gml:coord><gml:coord>',$coords).'</gml:coord></gml:LinearRing>';
         $nothole = path_is_clockwise($path);
         $gml .= ($nothole)?'<gml:outerBoundaryIs>':'<gml:innerBoundaryIs>';
         $gml .= $lring;
         $gml .= ($nothole)?'</gml:outerBoundaryIs>':'</gml:innerBoundaryIs>';
      }break;
      }
   }
   $gml.= '</gml:Polygon>';
   return $gml;
}

function path_is_clockwise($path) {
   $ptc = get_path_center($path);
   $vx = count($path)-1;
   $thetat = 0;
   for ($i=0;$i<$vx;$i++) {
      $ptm = $path[$i];
      $ptn = $path[(($i+1)%$vx)];
      if($ptm != $ptn) {
         $thetam = get_angle_from_center($ptc,$ptm);
         $thetan = get_angle_from_center($ptc,$ptn);
         $theta = $thetan - $thetam;
         // each pair of vertices is joined by a straight line to form a polygon
         // so if you have an angle > 180 degrees you need the other side of the 
         // circle.
         if (abs($theta)>180) $theta = ($theta>0)?($theta-360):($theta+360);
         $thetat += $theta;
      }
   }
   return ($thetat > 0); 
}

function get_angle_from_center($ptc,$ptn) {
   $r2d = 180/pi();
   $cx = $ptc[0];$cy = $ptc[1];
   $nx = $ptn[0];$ny = $ptn[1];
   $dx = $cx - $nx;
   $dy = $ny - $cy;
   if ($dx == 0 && $dy == 0) $theta = 0; 
   else if ($dy == 0) $theta = ($dx > 0)?180:0;
   else if ($dx == 0) $theta = ($dy > 0)?90:-90;
   else {
      $theta = atan($dy/$dx) * $r2d;
      if ($dx<0) $theta += 180;
   }
   return $theta;
}

?>
