<?php
class DegreesDecimalCoordinate {
   var $latitude;
   var $longitude;
   function DegreesDecimalCoordinate ($lat, $lon) {
      $this->latitude = $lat;
      $this->longitude = $lon;
   }
   function __destruct() {
      unset($this->latitude,$this->longitude);
   }
}
class GridReference {
   var $easting;
   var $northing;
   var $accuracy;
   function GridReference ($eing, $ning) {
      $this->easting = $eing;
      $this->northing = $ning;
      $this->accuracy = 1;
   }
   function __destruct() {
      unset($this->easting,$this->northing,$this->accuracy);
   }
}
class NationalGridReference {
   var $square;
   var $easting;
   var $northing;
   var $accuracy;
   function NationalGridReference ($sq=null, $eing=null, $ning=null) {
      $this->square = strtoupper($sq);
      $this->easting = $eing;
      $this->northing = $ning;
      $this->accuracy = 1;
   }
   function fromString($grid) {
      $this->square = substr($grid,0,2);
      $length = strlen($grid);
      $frag = floor(($length - 2) / 2);
      $acc = pow(10,5-$frag);
      $e = substr($grid,2,$frag) * $acc;
      $n = substr($grid,($frag+2),$frag) * $acc;
      if (($length % 2) == 1) {
         $tetrads = tetradArray();
         $tetrad = substr($grid,$length-1);
         for ($x=0;$x<=5;$x++) {
            for ($y=0;$y<=5;$y++) {
               if ($tetrad == $tetrads[$x][$y]) {
                  $e += ($x * 2000);
                  $n += ($y * 2000);
                  $acc = 2000;
               }
            }
         }
      }
      $this->easting = $e;
      $this->northing = $n;
      $this->accuracy = $acc;
   }
   function toString() {
      $e = $this->easting;
      $n = $this->northing;
      if ($this->accuracy == 2000) {
         $tetrads = tetradArray();
         $tx = substr($e,1,1)/2;
         $ty = substr($n,1,1)/2;
         $e = substr($e,0,1);
         $n = substr($n,0,1);
         $t = $tetrads[$tx][$ty];
         $grid = $this->square . $e . $n . $t;
      } else {
         while (($e%10==0)&&($n%10==0)) {
            $e /= 10;
            $n /= 10;
         }
         $grid = $this->square . $e . $n;
      }
      return $grid;
   }
   function __destruct() {
      unset($this->square,$this->easting,$this->northing,$this->accuracy);
   }

}
class CartesianCoordinate {
   var $x;
   var $y;
   var $z;
   function CartesianCoordinate ($x, $y, $z) {
      $this->x = $x;
      $this->y = $y;
      $this->z = $z;
   }
   function __destruct() {
      unset($this->x,$this->y,$this->z);
   }
}

function conv_ngr_to_ings($ngr_obj) {
    $sq = $ngr_obj->square;
    $north = $ngr_obj->northing;
    $east = $ngr_obj->easting;

    $t1 = ord(substr($sq,0,1)) - 65;
    
    if ($t1 > 8) $t1 = $t1 -1;
    $t2 = floor($t1 / 5);
    $north = $north + 500000 * (3 - $t2);
    $east = $east + 500000 * ($t1 - 5 * $t2 - 2);

    $t1 = ord(substr($sq,1,1)) - 65;
    if ($t1 > 8) $t1 = $t1 - 1;
    $t2 = floor($t1 / 5);
    $north = $north + 100000 * ( 4 - $t2);
    $east = $east + 100000 * ( $t1 - 5 * $t2);
    $gr_obj = new GridReference($east,$north);
    $gr_obj->accuracy = $ngr_obj->accuracy;
    return $gr_obj;
}

function find_gridsquare ($gr_obj) {
    $east = $gr_obj->easting;
    $north = $gr_obj->northing;
    $eX = $east / 500000;
    $nX = $north / 500000;
    $tmp = floor($eX)-5.0 * floor($nX)+17.0;
    $nX = 5 * ($nX - floor($nX));
    $eX = 20 - 5.0 * floor($nX) + floor(5.0 * ($eX - floor($eX)));
    if ($eX > 7.5)
      $eX = $eX + 1;
    if ($tmp > 7.5)
      $tmp = $tmp + 1;
    $eing = $east;
    $ning = $north;
    $lnth = strlen($eing);
    $eing = substr($eing,$lnth - 5, $lnth);
    $lnth = strlen($ning);
    $ning = substr($ning,$lnth - 5, $lnth);
    $sq = chr($tmp + 65) . chr($eX + 65);
//print "<p>$sq</p>";
    $ngr_obj = new NationalGridReference($sq, $eing, $ning);
    $ngr_obj->accuracy = $gr_obj->accuracy;
    return $ngr_obj;
}

function cartesian_to_ll($x, $y, $z, $ecc, $axis) {
    $lon = atan($y / $x);
    $p = sqrt(($x * $x) + ($y * $y));
    $lat = atan($z / ($p * (1 - $ecc)));
    $v = $axis / (sqrt(1 - $ecc * (sin($lat) * sin($lat))));
    $errvalue = 1.0;
    $lat0 = 0;
    while ($errvalue > 0.001) {
      $lat0 = atan(($z + $ecc * $v * sin($lat)) / $p);  
      $errvalue = abs($lat0 - $lat);
      $lat=$lat0;
    }
    $height = $p / cos($lat) - $v;
    $geo = new DegreesDecimalCoordinate($lat, $lon);
    return $geo;
}

function conv_uk_ings_to_ll($gr_obj) {
   $deg2rad = pi()/180;
   $rad2deg = 1/$deg2rad;
   $east = $gr_obj->easting;
   $north = $gr_obj->northing;
   $wgs_axis = 6378137;
   $wgs_eccent = 0.00669438;
   $latorig = 49 * $deg2rad;
   $lonorig = -2 * $deg2rad;
   $scale = 0.9996012717;
   $falseeast = 400000;
   $falsenorth = -100000;
   $axis = 6377563.396;
   $eccent = 0.00667054;
   $ep = $east;
   $np = $north;
   $p = $eccent / 8;
   $a = $axis * (1 - (2 * $p) - (3 * $p * $p) - (10 * $p * $p * $p));
   $b = $axis * ((6 * $p) + (12 * $p * $p) + (45 * $p * $p * $p)) / 2;
   $c = $axis * ((15 * $p * $p) + (90 * $p * $p * $p)) / 4;
   $mo = $a * $latorig - $b * sin(2 * $latorig) + $c * sin(4 * $latorig);
   $mp = $mo + (($np - $falsenorth) / $scale);
   $phidash = $mp / $a;
   $phif = $phidash + (($b * sin(2 * $phidash)) - ($c * sin(4 * $phidash))) / ($a - (2 * $b * cos(2 * $phidash)));

   $uf = $axis / sqrt(1 - ($eccent * (sin($phif) * sin($phif))));
   $h = ($ep - $falseeast) / ($scale * $uf);

   $nsqd = $eccent * (cos($phif) * cos($phif)) / (1 - $eccent);
   $tsqd = pow(sin($phif) / cos($phif), 2);

   $lambdap = $lonorig + ( 1 / cos($phif)) * (($h - (($h * $h * $h) / 6) * (1 + (2 * $tsqd) + $nsqd)));
   $phip = $phif - ((1 + $nsqd) * (sin($phif)/cos($phif)) * ((($h * $h) / 2) - (($h * $h * $h * $h) / 24) * (5 + 3 * $tsqd))); 

   $cart = ll_to_cartesian($phip, $lambdap, $axis, $eccent, 0);

   $x = $cart->x + 371;
   $y = $cart->y - 112;
   $z = $cart->z + 434;

   $geo = cartesian_to_ll($x, $y, $z, $wgs_eccent, $wgs_axis);
   $lat = $geo->latitude * $rad2deg;
   $lon = $geo->longitude * $rad2deg;
   $lat = formatToXPlaces($lat,2,6);
   $lon = formatToXPlaces($lon,2,6);
   $ddc = new DegreesDecimalCoordinate ($lat,$lon);
   
   /*unset($deg2rad,$rad2deg,
         $geo,$lat,$lon,$east,$north,
         $wgs_axis,$wgs_eccent,$latorig,$lonorig,
         $scale,$falseeast,$falsenorth,$axis,$eccent,
         $ep,$np,$p,$a,$b,$c,$mo,$mp,
         $phidash,$phif,$uf,$h,$nsqd,$tsqd,
         $lambdap,$phip,$cart,$x,$y,$z);*/
   return $ddc;
}
function ll_to_cartesian($lat, $lon, $axis, $ecc, $height) {
    $v = $axis / (sqrt (1 - $ecc * (pow (sin($lat), 2))));
    $x = ($v + $height) * cos($lat) * cos($lon);
    $y = ($v + $height) * cos($lat) * sin($lon);
    $z = ((1 - $ecc) * $v + $height) * sin($lat);
    $cart = new CartesianCoordinate ($x, $y, $z );
    return $cart;
}
function switch_ellipsoids  ($dd_coord_obj) {
    $lat = $dd_coord_obj->latitude;
    $lon = $dd_coord_obj->longitude;
    $lat = deg2rad($lat);  // GRS80/WGS84 input in radians
    $lon = deg2rad($lon);  // ditto
    $axis = 6378137.0;         // GRS80/WGS84 major axis
    $ecc = 0.00669438;         // ditto eccentricity  
    $height = 0;               // height above datum
    $cart = ll_to_cartesian($lat, $lon, $axis, $ecc, $height);
    $x = $cart->x - 371;
    $y = $cart->y + 112;
    $z = $cart->z - 434;
    $axis = 6377563.396;
    $ecc = 0.00667054;
    $geo = cartesian_to_ll($x, $y, $z, $ecc, $axis);
    $lat = rad2deg($geo->latitude); 
    $lon = rad2deg($geo->longitude);
    $dd_coord = new DegreesDecimalCoordinate($lat,$lon);
    return ($dd_coord);
}

function wgs84_to_osgb36($dd_coord_obj) {
    $dd_coord_obj = switch_ellipsoids($dd_coord_obj);
    $lat = $dd_coord_obj->latitude;
    $lon = $dd_coord_obj->longitude;
    $phip = deg2rad($lat);      // convert latitude to radians
    $lambdap = deg2rad($lon);   // convert longitude to radians
    $AXIS = 6377563.396;        // the Airy major axis
    $ECCENTRIC = 0.00667054;    // Airy eccentricity
    $LAT_ORIG = deg2rad(49);   
    $lon_ORIG = deg2rad(-2);
    $FALSE_EAST = 400000.0;
    $FALSE_NORTH = -100000.0;
    $SCALE = 0.9996012717;
    $east = '';                 // variable to hold NGR Eastings
    $north = '';                 // variable to hold NGR Northings
    //
    // The five uncommented constants relate to the British National Grid. This has a
    // reference point (origin) based on 49 degrees north, 2 degrees west (west is negative). 
    // The FALSE_EAST and FALSE_NORTH are just offsets to ensure that the grid reference is
    // always positive.
    // Now we convert the latitude and longitude to National Grid Northings and Eastings.
    //
    $j = ($lambdap - $lon_ORIG) * cos($phip);
    $nsqd = $ECCENTRIC * (cos($phip) * cos($phip)) / (1 - $ECCENTRIC);
    $tsqd = pow((sin($phip) / cos($phip)), 2);
    $up = $AXIS / sqrt(1 - ($ECCENTRIC * (sin($phip) * sin($phip))));
    $p = $ECCENTRIC / 8.0;
    $a = $AXIS * (1 - (2 * $p) - (3 * $p * $p) - (10 * $p * $p * $p));
    $b = $AXIS * ((6 * $p) + (12 * $p * $p) + (45 * $p * $p * $p)) / 2;
    $c = $AXIS * ((15 * $p * $p) + (90 * $p * $p * $p)) / 4;
    $mo = ($a * $LAT_ORIG) - ($b * sin(2 * $LAT_ORIG)) + ($c * sin(4 * $LAT_ORIG));
    $mp = ($a * $phip) - ($b * sin(2 * $phip)) + ($c * sin(4 * $phip));
    $east = 0.5 + $FALSE_EAST + (($SCALE * $up) * ($j + (($j * $j * $j) / 6) * (1 - $tsqd + $nsqd)));
    $north = 0.5 + $FALSE_NORTH + ($SCALE * ($mp - $mo)) + ($SCALE * ($up) * ((sin($phip)) / (cos($phip)))) * ((($j * $j)/ 2) + (($j * $j * $j * $j) / 24 * (5 - $tsqd)));
    $east = floor($east);       // round to whole number
    $north = floor($north);     // round to whole number
    $gr_obj = new GridReference ($east, $north);
    return $gr_obj;
}

function addZeros($number,$zeros) {
   for ($i=1;$i<=$zeros;$i++) {
      $number *= 10;
   }
   return $number;
}

function llbb_from_grbb($bb) {
   $mingr = new GridReference($bb->west,$bb->south);
   $maxgr = new GridReference($bb->east,$bb->north);
   $minll = conv_uk_ings_to_ll($mingr);
   $maxll = conv_uk_ings_to_ll($maxgr);
   /*
   $maxlat = max($maxll->latitude,$minll->latitude);
   $maxlon = max($maxll->longitude,$minll->longitude);
   $minlat = min($maxll->latitude,$minll->latitude);
   $minlon = min($maxll->longitude,$minll->longitude);
   */
   $llbb = new BoundingBox($maxll->latitude,$maxll->longitude,$minll->latitude,$minll->longitude);
   return $llbb;
}
function tetradArray() {
   $tk[0][4] = 'E';$tk[1][4] = 'J';$tk[2][4] = 'P';$tk[3][4] = 'U';$tk[4][4] = 'Z';
   $tk[0][3] = 'D';$tk[1][3] = 'I';$tk[2][3] = 'N';$tk[3][3] = 'T';$tk[4][3] = 'Y';
   $tk[0][2] = 'C';$tk[1][2] = 'H';$tk[2][2] = 'M';$tk[3][2] = 'S';$tk[4][2] = 'X';
   $tk[0][1] = 'B';$tk[1][1] = 'G';$tk[2][1] = 'L';$tk[3][1] = 'R';$tk[4][1] = 'W';
   $tk[0][0] = 'A';$tk[1][0] = 'F';$tk[2][0] = 'K';$tk[3][0] = 'Q';$tk[4][0] = 'V';
   return $tk;
}

?>
