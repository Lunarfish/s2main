function s2_enableextender(perms) {
   var dom = s2_getcurrentdomain();
   var perm = perms[dom]['System Administrator'];
   var extendable = s2_getcookie('s2_extender');
   var encbutton,extbutton,img;
   extbutton = document.getElementById('flip_extender');
   if (!extbutton && perm) { 
      encbutton = document.getElementById('flip_encrypt');
      extbutton = document.createElement('BUTTON');   
      extbutton.className = 'flipextend';
      extbutton.id = 'flip_extender';
      extbutton.onclick = function() {s2_flip_extender(this.id);};
      img = document.createElement('IMG');
      if (extendable>0) {
         img.src = '../images/leaf.gif' 
         img.alt = 'extendable';
      } else {
         img.src = '../images/leaf-gs.gif' 
         img.alt = 'fixed';
      }
      extbutton.appendChild(img);
      encbutton.parentNode.insertBefore(extbutton,encbutton);
   }      
}
function s2_flip_extender(buttonid) {            
   var txt,img,onoff,button;
   if (!buttonid) {
      onoff = (!s2_getcookie('s2_extender'))?'extendable':'fixed';
      button = document.getElementById('flip_extender');   
   } else {
      button = document.getElementById(buttonid);
      onoff = button.firstChild.alt;
   }
   switch (onoff) {
   case 'extendable': {
      txt = 'fixed';
      img = new Image();
      img.src = '../images/leaf-gs.gif'
      img.alt = txt;
      button.replaceChild(img,button.firstChild);
      s2_setcookie('s2_extender',0);
   }break;
   case 'fixed': {
      txt = 'extendable';
      img = new Image();
      img.src = '../images/leaf.gif';
      img.alt = txt;
      button.replaceChild(img,button.firstChild); 
      s2_setcookie('s2_extender',1);
   }break;                    
   }
   s2_refresh();
   return true;   
}

function s2_getextenderbutton(jobj,part) {
//alert('extender button');
   var dom = s2_getcurrentdomain();
//alert(JSON.stringify(jobj.UserPermissions));
//alert(perm);
   var button,img;
   if (s2_getcookie('s2_extender')>0) {
      button = document.createElement('BUTTON');
      button.className = 'inlineextender';
      button.onclick = function() {s2_getclassdefinition(jobj.IType,part);};
      img = document.createElement('IMG');
      img.src = '../images/leaf.gif';
      img.alt = 'Extend ' + jobj.IType + ' ' + part;
      button.appendChild(img);
   }
   return button;
}
function s2_callextender(req) {
   req.target = '../custom/S2_Extender.php';
   snc_send(req);
}
function s2_getclassdefinition(itype,part) {
   var req,par;
   req = new Object();
   par = new Object();
   par.Action = 'Define';
   par.IType = itype;
   par.Part = part;
   req.request = par;
   req.responder = s2_drawclassdefinition;
   s2_callextender(req);
}
function s2_drawclassdefinition(json) {
   //alert(json);
   var jobj = JSON.parse(json);
   
   var dialog,node,h2;
   snc_clearMenus('incontentdialog');   
   dialog = document.getElementById('incontentdialog');
   h2 = s2_extendheader('S2 Extender Customisation');
   dialog.appendChild(h2);
   
   node = s2_drawclasscontext(jobj);
   if(node) dialog.appendChild(node);
   
   node = s2_drawclasscolumndefs(jobj);
   if(node) dialog.appendChild(node);
   
   node = s2_drawclasspermissions(jobj);
   if(node) dialog.appendChild(node);
   
   node = s2_drawclassviewmenu(jobj);
   if(node) dialog.appendChild(node);
   else {
      node = s2_drawclassshowlist(jobj);
      if(node) dialog.appendChild(node);
   }
   
   
//snc_report_json_obj(json);
   completed();
}
function s2_extendheader(name) {
   var h2 = document.createElement('H2');
   h2.appendChild(document.createTextNode(name));
   return h2;
}
function s2_getvtable(headings) {
   var table,thead,tr,th,i;
   table = document.createElement('TABLE');
   thead = document.createElement('THEAD');
   tr = document.createElement('TR');
   for (i in headings) {
      th = document.createElement('TH');
      th.appendChild(document.createTextNode(headings[i]));
      tr.appendChild(th);
   }
   thead.appendChild(tr);
   table.appendChild(thead);
   return table;   
}
function s2_getselect(options,current) {
   var select,option,opt,val,oname;
   select = document.createElement('SELECT');
   for (opt in options) {
      val = options[opt];
      option = document.createElement('OPTION');
      option.appendChild(document.createTextNode(val));
      option.value = val;
      if (current == val) option.selected = 'selected';
      select.appendChild(option);
   }   
   return select;                         
}
function s2_drawclasscontext(jobj) {
   var rnode,h2,div,node,table,tbody,tr,th,td,input,tnode,prop;
   var props = new Array();
   props['IType'] = 'Type Name';
   props['DisplayName'] = 'Display As';
   //props['TableName'] = 'Database Table Name';
   props['Domain'] = 'Permissions Domain';
   props['InstanceOf'] = 'Instance Of';
   rnode = document.createElement('DIV');
   h2 = s2_extendheader('Context');
   rnode.appendChild(h2);
   table = document.createElement('TABLE');
   tbody = document.createElement('TBODY');
   for (prop in props) {
      tr = document.createElement('TR');
      th = document.createElement('TH');
      th.appendChild(document.createTextNode(props[prop]));
      tr.appendChild(th);
      td = document.createElement('TD');
      input = document.createElement('INPUT');
      input.type = 'text';
      input.value = jobj.Current[prop];
      td.appendChild(input);
      tr.appendChild(td);
      tbody.appendChild(tr);
   }
   table.appendChild(tbody);
   rnode.appendChild(table);
   node = document.createElement('HR');
   node.style.clear = 'both';
   rnode.appendChild(node);            
   return rnode;                                                                                                                        
}

function s2_drawclasscolumndefs(jobj) {
   var rnode,h2,table,tbody,tr,td,i,input,select,col,def,dtype,dtypeval;
   var coldefs = (jobj.Current && jobj.Current.Columns)?jobj.Current.Columns:null;
   var datatypes = jobj.DataTypes;
   rnode = document.createElement('DIV');
   h2 = s2_extendheader('Columns');
   rnode.appendChild(h2);
   table = s2_getvtable(new Array('Column Name','Data Type','Options'));
   tbody = document.createElement('TBODY'); 
   for (col in coldefs) {
      def = coldefs[col];
      dtype = (typeof def == 'object')?def.DataType:def.replace(/\:+/,'');
      switch(dtype) {
      case 'Unique Key': dtypeval = null;break;
      case 'LINKEDTO': dtypeval = 'Linked Key';break;
      case 'LINKEDTOM': dtypeval = 'Linked Key';break;
      case 'INTERFACE': dtypeval = 'Interface';break;
      default: dtypeval = dtype;break;
      }
      if (dtypeval) {
         tr = document.createElement('TR');
         td = document.createElement('TD');
         input = document.createElement('INPUT');
         input.value = col;
         td.appendChild(input);
         tr.appendChild(td);
         td = document.createElement('TD');
         select = s2_getselect(datatypes,dtypeval);
         td.appendChild(select);
         tr.appendChild(td);
         
         td = document.createElement('TD');
         btn = document.createElement('BUTTON');
         btn.className = 'inlineextender';
         btn.innerHTML = '&times;';
         td.appendChild(btn);
         
         btn = document.createElement('BUTTON');
         btn.className = 'inlineextender';
         img = document.createElement('IMG');
         img.src = '../images/arrow_u.gif';
         img.alt = 'Move Up';
         btn.appendChild(img)
         td.appendChild(btn);
         
         btn = document.createElement('BUTTON');
         btn.className = 'inlineextender';
         img = document.createElement('IMG');
         img.src = '../images/arrow_d.gif';
         img.alt = 'Move Down';
         btn.appendChild(img)
         td.appendChild(btn);
   
         tr.appendChild(td);
         tbody.appendChild(tr);
      }                
   }
   table.appendChild(tbody);
   rnode.appendChild(table);                   
   node = document.createElement('HR');
   node.style.clear = 'both';
   rnode.appendChild(node);            
   return rnode;
}

function s2_drawclasspermissions(jobj) {
   var rnode,h2,table,tbody,tr,td,i,input,select,action,perm;
   var perms = (jobj.Current && jobj.Current.Permissions)?jobj.Current.Permissions:null;
   var defperms = jobj.DefaultPermissions;
   var actions = jobj.Actions;
   rnode = document.createElement('DIV');
   h2 = s2_extendheader('Permission Requirements');
   rnode.appendChild(h2);
   table = s2_getvtable(new Array('Action','Permission','Options'));
   tbody = document.createElement('TBODY'); 
   for (action in perms) {
      perm = perms[action];
      tr = document.createElement('TR');
      td = document.createElement('TD');
      select = s2_getselect(actions,action);
      td.appendChild(select);
      tr.appendChild(td);
      td = document.createElement('TD');
      select = s2_getselect(defperms,perm);
      td.appendChild(select);
      tr.appendChild(td);
      
      td = document.createElement('TD');
      btn = document.createElement('BUTTON');
      btn.className = 'inlineextender';
      btn.innerHTML = '&times;';
      td.appendChild(btn);
      
      btn = document.createElement('BUTTON');
      btn.className = 'inlineextender';
      img = document.createElement('IMG');
      img.src = '../images/arrow_u.gif';
      img.alt = 'Move Up';
      btn.appendChild(img)
      td.appendChild(btn);
      
      btn = document.createElement('BUTTON');
      btn.className = 'inlineextender';
      img = document.createElement('IMG');
      img.src = '../images/arrow_d.gif';
      img.alt = 'Move Down';
      btn.appendChild(img)
      td.appendChild(btn);

      tr.appendChild(td);

      
      tbody.appendChild(tr);                
   }
   table.appendChild(tbody);
   rnode.appendChild(table);                   
   node = document.createElement('HR');
   node.style.clear = 'both';
   rnode.appendChild(node);            
   return rnode;
}

function s2_drawclassviewmenu(jobj) {
   var rnode,h2,table,tbody,tr,td,i,input,select,ind,view,vname,sval,btn,img,div;
   var views = (jobj.Current && jobj.Current.Views)?jobj.Current.Views:null;
   var showopts = jobj.ShowOpts;
   rnode = document.createElement('DIV');
   
   for (ind in views) {
      view = views[ind];
      vname = view.Name;
      div = document.createElement('DIV');
      div.id = 'View_Number_'+ind;
      h2 = s2_extendheader('View: '+vname);
      h2.style.paddingRight = '11px';
      btn = document.createElement('BUTTON');
      btn.className = 'inlineextender';
      btn.innerHTML = '&times;';
      h2.appendChild(btn);
      
      btn = document.createElement('BUTTON');
      btn.className = 'inlineextender';
      img = document.createElement('IMG');
      img.src = '../images/arrow_u.gif';
      img.alt = 'Move Up';
      btn.appendChild(img)
      h2.appendChild(btn);
      btn = document.createElement('BUTTON');
      btn.className = 'inlineextender';
      img = document.createElement('IMG');
      img.src = '../images/arrow_d.gif';
      img.alt = 'Move Down';
      btn.appendChild(img)
      h2.appendChild(btn);
      
      div.appendChild(h2);
   
      table = s2_getvtable(new Array('Type','Name','Show','Options'));
      tbody = document.createElement('TBODY');
       
      for (i in view.Data) {
         
         tr = document.createElement('TR');
         td = document.createElement('TD');
         input = document.createElement('INPUT');
         input.value = view.Data[i].Table;
         td.appendChild(input);
         tr.appendChild(td);
         
         td = document.createElement('TD');
         input = document.createElement('INPUT');
         input.value = (view.Data[i].Name)?view.Data[i].Name:'-- default --';
         td.appendChild(input);
         tr.appendChild(td);
         
         td = document.createElement('TD');
         sval = (view.Data[i].Show)?view.Data[i].Show:'All';
         select = s2_getselect(showopts,sval);
         td.appendChild(select);
         tr.appendChild(td);
         
         td = document.createElement('TD');
         btn = document.createElement('BUTTON');
         btn.className = 'inlineextender';
         btn.innerHTML = '&times;';
         td.appendChild(btn);
         
         btn = document.createElement('BUTTON');
         btn.className = 'inlineextender';
         img = document.createElement('IMG');
         img.src = '../images/arrow_u.gif';
         img.alt = 'Move Up';
         btn.appendChild(img)
         td.appendChild(btn);
         
         btn = document.createElement('BUTTON');
         btn.className = 'inlineextender';
         img = document.createElement('IMG');
         img.src = '../images/arrow_d.gif';
         img.alt = 'Move Down';
         btn.appendChild(img)
         td.appendChild(btn);
         
         tr.appendChild(td);
         tbody.appendChild(tr);                
      }
   
      table.appendChild(tbody);
      div.appendChild(table);
      node = document.createElement('HR');
      node.style.clear = 'both';
      div.appendChild(node);            
      rnode.appendChild(div);                      
   }
   return rnode;
}
function s2_drawclassshowlist(jobj) {
   var rnode,h2,table,tbody,tr,td,i,input,select,tab;
   var show = (jobj.Current && jobj.Current.Show)?jobj.Current.Show:null;
   
   rnode = document.createElement('DIV');
   h2 = s2_extendheader('Show Types');
   rnode.appendChild(h2);
   table = s2_getvtable(new Array('Type'));
   tbody = document.createElement('TBODY'); 
   for (tab in show) {
      tr = document.createElement('TR');
      td = document.createElement('TD');
      input = document.createElement('INPUT');
      input.value = show[tab];
      td.appendChild(input);
      tr.appendChild(td);
      tbody.appendChild(tr);                
   }
   table.appendChild(tbody);
   rnode.appendChild(table);                   
   node = document.createElement('HR');
   node.style.clear = 'both';
   rnode.appendChild(node);            
   
   return rnode;
}
 