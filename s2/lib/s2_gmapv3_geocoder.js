var geocoder = new google.maps.Geocoder();
var infowindow = new google.maps.InfoWindow();
var marker;
function gm3_isCoordinate(val) {
   return (val.match(/^[\d,\.]+\s*\,\s*[\d,\.]+$/));
}
function gm3_isEasting(val) {
   return (val.length >= 6 && val.match(/^[\d,\.]+$/));
}
function gm3_codeAddress() {
   var lat,lng,latlng,coords,e,n,gr,ll;
   var address = document.getElementById('address').value;
   if (gm3_isCoordinate(address)) {
      coords = address.split(/\s*\,\s*/);
      e = parseFloat(coords.shift());
      n = parseFloat(coords.shift());
      ll = s2_gr_to_ll(new GridReference(e,n));
      latlng = ll.latitude + ',' + ll.longitude; 
      gm3_updateNGR(latlng);
   } else if (gm3_isEasting(address)) {
      document.getElementById('address').value = address + ', ';
      alert('Now paste Northing');
   } else {
      geocoder.geocode( { 'address': address}, function(results, status) {
         if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            if (marker) marker.setMap(null);
            marker = new google.maps.Marker({
               map: map,
               position: results[0].geometry.location
            });
            lat = marker.position.lat();
            lng = marker.position.lng();
            latlng = (Math.round(lat*100000)/100000) + ',' + (Math.round(lng*100000)/100000);
            gridref = gm3_updateNGR(latlng);
            gm3_updateLL(latlng);
            infowindow.setContent('GR: '+gridref+'<br/>LL: '+latlng);
            infowindow.open(map, marker);
         } else {
            alert('Geocode was not successful: ' + status);
         }
      });
   }
}

function gm3_codeLatLng() {
   var addnode;
   var input = document.getElementById('latlng').value;
   var latlngStr = input.split(',', 2);
   var lat = parseFloat(latlngStr[0]);
   var lng = parseFloat(latlngStr[1]);
   var latlng = new google.maps.LatLng(lat, lng);
   geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
         if (results[1]) {
            //map.setZoom(11);
            map.setCenter(latlng);
            if (marker) marker.setMap(null);
            marker = new google.maps.Marker({
               position: latlng,
               map: map
            });
            infowindow.setContent(results[1].formatted_address);
            infowindow.open(map, marker);
            addnode = document.getElementById('address');
            addnode.value = results[1].formatted_address;
         } else {
           alert('No results found');
         }
      } else {
         alert('Geocoder failed due to: ' + status);
      }
   });
}
function gm3_codeNGR(gridref) {
   var gr,ll,input;
   gr = s2_gr_from_ngrstring(gridref);
   ll = s2_gr_to_ll(gr);
   input = document.getElementById('latlng');
   input.value = ll.lat() + ',' + ll.lng();
   //codeLatLng();
   return true;  
}
function gm3_updateLL(llstr) {
   input = document.getElementById('latlng');
   input.value = llstr;
}
function gm3_updateNGR(latlng) {
   var gm,ll,gr,ngr,gridref,grnode,latlngstr,llnode;
   var latlngStr = latlng.split(',', 2);
   var lat = parseFloat(latlngStr[0]);
   var lng = parseFloat(latlngStr[1]);
   var gm = new google.maps.LatLng(lat, lng);
   ll = s2_gmp_to_ll(gm);
   gr = s2_ll_to_gr(ll);
   ngr = s2_gr_to_ngr(gr);
   gridref = ngr.asString();
   grnode = document.getElementById('ngr');
   grnode.value = gridref;
   s2_clickmap({latLng:gm});
   return gridref;
}
