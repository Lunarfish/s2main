<?php
class CAT_File extends DBT {
   protected $tablename = 'CAT_File';
   protected $displayname = 'Catalogue File';
   protected $user;
   protected $alts = array('Categories'=>'CAT_Category');
   protected $columns = array(
      'G_Dataset_ID'    => 'Unique Key',
      'Name'            => 'Short Text',
      'From_Date'       => 'Date',
      'To_Date'         => 'Date',
      'Coverage'        => 'Map Data',
      'Contact'         => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"]}'
   );
   protected $domain = 'species';
      protected $view = '[
      {
         "Name":"Records",
         "Data": [
            {
               "Table":"G_Record"
            }
         ]
      },
      {
         "Name":"MetaData",
         "Data": [
            {
               "Table":"G_Dataset_Description"
            },
            {
               "Table":"G_Dataset_File"
            }            
         ]
      }
   ]'; 
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $primingdata = array(
   );
      protected $defaultpermissions = array(
      'USR_Permissions' => '
"User_Id","Domain","Label","Value"
1,"species","Registered User",1
1,"species","Species Access",1
1,"species","Species Administrator",1
1,"species","System Administrator",1',
      'USR_Default_Permissions' => '
"Domain","Label","Description","Value","Anon" 
"species","System Administrator","Full control",0,0
"species","Species Administrator","Add and edit datasets and records",0,0
"species","Species Access","View all species data",0,0
"species","Registered User","View unprotected species data",1,0'   
   );

}
class G_Dataset_Description extends DBT {
   protected $tablename = 'G_Dataset_Description';
   protected $displayname = 'Dataset Description';
   protected $user;
   protected $columns = array(
      'G_Dataset_Description_ID'     => 'Unique Key',
      'G_Dataset_ID'    => '{"DataType":"LINKEDTO","TargetType":"G_Dataset","TargetField":"G_Dataset_ID","Mandatory":1,"Current":1}',
      'Description'     => 'Long Text'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $primingdata = array(
   );   
}

?>