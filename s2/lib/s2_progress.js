function s2_updateprogressbar(id,complete) {
   var pc,pb,ps,pt,percent;
   percent = Math.floor(complete * 100);
   pc = document.getElementById(id);
   pb = document.getElementById(id + '-bar');
   ps = document.getElementById(id + '-status');
   pt = document.getElementById(id + '-statustext');
   stext = '\u00a0' + percent + '%\u00a0~\u00a0';
   ps.style.width = percent + '%';
   pt.replaceChild(document.createTextNode(stext),pt.firstChild);
   if (percent >= 50) ps.appendChild(pt);
   else pb.insertBefore(pt,pb.firstChild);
}
function s2_completeprogressbar(id) {
   var pc,pnode;
   pc = document.getElementById(id);
   pnode = pc.parentNode;
   pnode.removeChild(pc);
}
function s2_makeprogressbar(parent,id,complete,width) {
   var pnode,c,b,s,sp,percent,stext;
   pnode = document.getElementById(parent);
   percent = Math.floor(complete * 100);
   c = document.createElement('DIV');
   c.id = id;
   c.className = 'pbar-container';
   if (width) c.style.width = width;
   b = document.createElement('DIV');
   b.id = id + '-bar';
   b.className = 'pbar-bar';        
   s = document.createElement('DIV');
   s.id = id + '-status';
   s.className = 'pbar-status';
   s.style.width = percent + '%';
   stext = percent + '%\u00a0~\u00a0';
   sp = document.createElement('SPAN');
   sp.id = id + '-statustext';
   sp.className = 'pbar-statustext';
   sp.appendChild(document.createTextNode(stext));
   if (percent >= 50) s.appendChild(sp);
   else b.appendChild(sp);
   b.appendChild(s);
   c.appendChild(b);
   pnode.appendChild(c);
   return true;
}
