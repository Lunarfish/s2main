<?php
function getJSONFiles($folder) {
   $files = glob("$folder/*.json");
   return $files;
}
function readJSONFile($file,$decode=true) {
   $file = fopen($file, "rb");
   $json = "";
   $lines = array();
   if ($file) {
      while(!feof($file)) {$lines[] = trim(fgets($file, 4096));}
      fclose($file);
      $json = join(" ",$lines);
      $json_obj = json_decode($json);
   }
   return ($decode)?$json_obj:$json;
}    
?>