/*
      function Coordinate (x, y) {
         this.x = x;
         this.y = y;
      }
*/
function LS_System () {
   this.id;
   this.name;
   this.runby;
   this.setup;
}

function LS_Site () {
   this.id;
   this.name;
   this.code;
   this.parish;
   this.district;
   this.gridref;
}

function LS_Meeting () {
   this.id;
   this.name;
   this.number;
   this.location;
   this.date;
   this.time;
}

function LS_Event () {
   this.id;
   this.site_id;
}

function LS_Document () {
   this.id;
   this.title;
   this.doclink;
   this.date;
}

function LS_MeetingAgenda () {
   this.id;
   this.meeting_id;
   this.document_id;
}

function LS_MeetingMinutes () {
   this.id;
   this.meeting_id;
   this.document_id;
}

