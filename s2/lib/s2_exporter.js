var svr = 'http://localhost/s2';
function s2_enableexporter() {
   var encbutton,expbutton,img,icn,div,i,id,ids;
   ids = ['page','main','report-page'];
   expbutton = document.getElementById('flip_export');
   if (!expbutton) { 
      expbutton = document.createElement('BUTTON');   
      expbutton.className = 'fliporder';
      expbutton.id = 'flip_export';
      expbutton.onclick = function() {s2_excelmedoc();};
      img = document.createElement('IMG');
      //img.src = svr+'/images/expicon-on.png'; 
      img.src = '../images/expicon-on.png'; 
      img.alt = 'Export data';
      expbutton.appendChild(img);
      encbutton = document.getElementById('flip_encrypt');
      if (encbutton) encbutton.parentNode.insertBefore(expbutton,encbutton);
      else {
         for(i=0;i<ids.length;i++) {
            div = document.getElementById(ids[i]);
            if (!id && div) {
               id = ids[i];
               div.insertBefore(expbutton,div.firstChild);
            } 
         }
      }
   }   
   s2_addtabexporters();      
}
function s2_excelmedoc() {
   var dat;
   dat = s2_getalltabledata();
   s2_sendexcelmedata(dat);
}
function s2_excelmetab(id) {
   var dat;
   dat = new Array(s2_gettabledatabyid(id));
   s2_sendexcelmedata(dat);   
}
function s2_sendexcelmedata(dat) {
   var hdn,frm,ta,json;
   json = JSON.stringify(dat);
   
   hdn = document.createElement('DIV');
   hdn.style.display = 'none';
   hdn.style.visibility = 'hidden';
   frm = document.createElement('FORM');
   frm.method = 'POST';
   frm.target = 's2excelme';
   frm.action = svr+'/htmlexcel.php';
   
   ta = document.createElement('TEXTAREA');
   ta.name = 'data'
   ta.value = json;
   ta.style.width = '100%';
   ta.style.height = '300px';
   frm.appendChild(ta);
   hdn.appendChild(frm);
   document.body.appendChild(hdn);
   frm.submit();
   document.body.removeChild(hdn);
}
function s2_getalltabledata() {
   var tabs,tab,t,dats,dat;
   tabs = document.getElementsByTagName('TABLE');
   dats = new Array();
   for (t=0;t<tabs.length;t++) {
      tab = tabs[t];
      if (!tab.className.match(/s2noexport/)) {
         dat = s2_gettabledata(tab);
         dats.push(dat);
      }
   }
   return dats;
}  
function s2_addtabexporters() {
   var tabs,tab,t;
   tabs = document.getElementsByTagName('TABLE');
   for (t=0;t<tabs.length;t++) {
      tab = tabs[t];
      if (!tab.className.match(/s2noexport/))
         s2_addtabexportbutton(tab,t);
   }
}
function s2_addtabexportbutton(tab,t) {
   var expbutton,img;
   if (!tab.id) tab.id = 's2table_'+t;
   if (!tab.className.match(/noexport/)&&(!tab.previousSibling||tab.previousSibling.className != 'inlineexport')) {
      expbutton = document.createElement('BUTTON');   
      expbutton.className = 'inlineexport';
      expbutton.id = 'flip_export_'+tab.id;
      expbutton.onclick = function() {s2_excelmetab(tab.id);};
      img = document.createElement('IMG');
      //img.src = svr+'/images/expicon-on.png'; 
      img.src = '../images/expicon-on.png'; 
      img.alt = 'Export data';
      expbutton.appendChild(img);
      tab.parentNode.insertBefore(expbutton,tab);
   }   
}
function s2_gettabledatabyid(id) {
   var tab,dat;
   tab = document.getElementById(id);
   dat = s2_gettabledata(tab);
   return dat;
}
function s2_gettabledata(tab) {
   var dat,dor,ttl,bx,bds,rx,rows,row,cx,cel,nde;
   dat = {};
   dat.rows = new Array();
   //dat.dim = tab.rows.length;
   ttl = s2_gettabletitle(tab);
   if (ttl) dat.title = ttl;
   //rows = tab.rows;
   if (tab.tHead) {
      rows = tab.tHead.rows;
      for (rx=0;rx<rows.length;rx++) {
         row = rows[rx];
         dor = s2_getrowdata(row); 
         dat.rows.push(dor);
      }
   }
   for (bx=0;bx<tab.tBodies.length;bx++) {
      rows = tab.tBodies[bx].rows;
      for (rx=0;rx<rows.length;rx++) {
         row = rows[rx];
         dor = s2_getrowdata(row); 
         dat.rows.push(dor);
      }
   }
   return dat;
}
function s2_getrowdata(row) {
   var bx,bds,cx,cel,nde;
   dor = new Array();
   for (cx=0;cx<row.cells.length;cx++) {
      cel = row.cells[cx];
      nde = s2_getcel(cel);
      dor.push(nde); 
   }
   return dor;
}
function s2_gettabletitle(tab) {
   var ttl;
   ttl = s2_getattval(tab,'title');
   // if the table doesn't have a title attribute then try to find a hx element 
   // associated with successive parent elements.
   if (!ttl) ttl = s2_getsibhx(tab);
   if (!ttl) ttl = s2_getparhx(tab);
   return ttl;
}
function s2_getattval(nde,nme) {
   var ttl,ax,att,attnme;
   for (ax in nde.attributes) {
      att = nde.attributes[ax];
      attnme = att.nodeName;
      if (attnme == nme) tll = att.nodeValue;
   }
   return ttl;
}
function s2_getcel(cel) {
   var nde;
   nde = {};
   nde.t = (cel.nodeName == 'TH')?'h':'d';
   nde.v = s2_getndeval(cel);
   return nde;
}
function s2_getndeval(nde) {
   var val,cld,cx,ccld,ccx,frms;
   val = '';
   for(cx=0;cx<nde.childNodes.length;cx++) {
      cld = nde.childNodes[cx];
      switch(cld.nodeName) {
      case 'INPUT':     if (cld.type != 'hidden') val += cld.value; break;
      case 'TEXTAREA':  val += cld.value; break;
      default:          val += ((cld.nodeValue)?cld.nodeValue:''); break;
      }
      if (cld.childNodes) val += s2_getndeval(cld);
   }
   val = val.replace(/[^\x00-\x7F]/g,'');
   val = val.replace(/[\x60,\x27]/g,''); // replace backticks
   return val;
}
function s2_getsibhx(nde) {
   var ttl;
   while (!ttl && (nde = nde.previousSibling)) {
      if (nde.nodeName && nde.nodeName.match(/H\d/) && nde.childNodes) ttl = s2_getndeval(nde);//nde.firstChild.nodeValue;
      else if (nde.childNodes) ttl = s2_getcldhx(nde);
   }
   return ttl;
}
function s2_getcldhx(nde) {
   var ttl,cld,cx;
   for (cx in nde.childNodes) {
      cld = nde.childNodes[cx];
      if (!ttl && cld.nodeName && cld.nodeName.match(/H\d/) && cld.childNodes) ttl = s2_getndeval(cld);//cld.firstChild.nodeValue;
   }
   return ttl;
}
function s2_getparhx(nde) {
   var ttl,cx,cld;
   while (!ttl && (nde = nde.parentNode)) {
      ttl = s2_getsibhx(nde);
      if (!ttl) {
      for (cx in nde.childNodes) {
         cld = nde.childNodes[cx];
         if (!ttl && cld.nodeName && cld.nodeName.match(/H\d/) && cld.childNodes) ttl = s2_getndeval(cld);//cld.firstChild.nodeValue;
      }}
   }   
   return ttl;
}