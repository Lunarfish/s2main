<?php
/*
<fes:Filter 
   xsi:schemaLocation="http://www.opengis.net/fes/2.0    
      http://schemas.opengis.net/filter/2.0/filterAll.xsd    
      http://www.opengis.net/gml/3.2    
      http://schemas.opengis.net/gml/3.2.1/gml.xsd">
   <fes:Overlaps>
      <fes:ValueReference>Geometry</fes:ValueReference>
      <gml:Polygon gml:id="P1" srsName="urn:ogc:def:crs:EPSG::4326">
         <gml:exterior>
            <gml:LinearRing>
               <gml:posList>10 10 20 20 30 30 40 40 10 10</gml:posList>
            </gml:LinearRing>
         </gml:exterior>
      </gml:Polygon>
   </fes:Overlaps>   
</fes:Filter>

*/
class OGCFilter {
   function define($type,$values) {
      $comp = new stdClass();
      switch($type) {
      case 'Filter': {
         $comp->Method = $type;
         $comp->Args = array();
         foreach($values as $value) $comp->Args[] = $value;
      }break;
      case 'BBOX': {
         $comp->Method = 'BBOX';
         $comp->Args = array();
         $comp->Args[] = $this->define('PropertyName',array($values[0]));
         $comp->Args[] = $this->define('Box',array($values[1]));      
      }break;
      case 'Overlaps': {
         $comp->Method = 'Overlaps';
         $comp->Args = array();
         $comp->Args[] = $this->define('PropertyName',array($values[0]));
         $comp->Args[] = $this->define('Polygon',array($values[1]));      
      }break;
      case 'Contains': {
         $comp->Method = 'Contains';
         $comp->Args = array();
         $comp->Args[] = $this->define('PropertyName',array($values[0]));
         $comp->Args[] = $this->define('Polygon',array($values[1]));      
      }break;
      case 'PropertyIsGreaterThan': {
         $comp->Method = 'PropertyIsGreaterThan';
         $comp->Args = array();
         $comp->Args[] = $this->define('PropertyName',array($values[0]));
         $comp->Args[] = $this->define('Literal',array($values[1]));      
      }break;
      case 'PropertyIsLessThan': {
         $comp->Method = 'PropertyIsLessThan';
         $comp->Args = array();
         $comp->Args[] = $this->define('PropertyName',array($values[0]));
         $comp->Args[] = $this->define('Literal',array($values[1]));      
      }break;
      case 'PropertyIsBetween': {
         $comp->Method = 'PropertyIsBetween';
         $comp->Args = array();
         $comp->Args[] = $this->define('PropertyName',array($values[0]));
         $comp->Args[] = $this->define('LowerBoundary',array($values[1]));      
         $comp->Args[] = $this->define('UpperBoundary',array($values[2]));      
      }break;
      case 'LowerBoundary': {
         $comp->Method = 'LowerBoundary';
         $comp->Args = array();
         $comp->Args[] = $this->define('Literal',array($values[0]));
      }break;
      case 'UpperBoundary': {
         $comp->Method = 'UpperBoundary';
         $comp->Args = array();
         $comp->Args[] = $this->define('Literal',array($values[0]));
      }break;
      case 'PropertyName': {
         $comp->Method = 'PropertyName';
         $comp->Value = $values[0];
      }break;
      case 'Literal': {
         $comp->Method = 'Literal';
         $comp->Value = $values[0];
   	}break;
      case 'Box': {
         $comp->Method = 'Box';
         $comp->Value = $values[0];
      }break;
      case 'Polygon': {
         $comp->Method = 'Polygon';
         $comp->Value = $values[0];
      }break;
      default: {
         $comp->Method = $type;
         $comp->Args = array();
         foreach($values as $value) $comp->Args[] = $value;
      }break;
      }
      return $comp;
   }
   function getxml($comp) {
      $xml = '';
      $nspc = (isset($comp->NameSpace))?$comp->NameSpace:'ogc';
      $mthd = $comp->Method;
      $type = (isset($comp->Type))?$comp->Type:'default';
      $args = (isset($comp->Args))?$comp->Args:null;
      switch($mthd) {
      case 'Filter': {
         $xml ='<ogc:Filter xmlns:ogc="http://www.opengis.org/ogc" xmlns:gml="http://www.opengis.net/gml">';
         foreach($args as $arg) $xml.= $this->getxml($arg);
         $xml.= '</ogc:Filter>';
      }break;
      case 'Box': {
         $wkt  = $comp->Value;
         $bb = new BoundingBox();
         /* there is a function to translate wkt into gml bbox coordinates which 
         should really replace this bit */
         $bb->fromWKT($wkt);
         $xml = '<gml:Box srsName="http://www.opengis.net/gml/srs/epsg.xml#4326">';                                    
         $xml.=   "<gml:coordinates>$bb->west,$bb->south $bb->east,$bb->north</gml:coordinates>";
         $xml.= '</gml:Box>';
      }break;
      case 'PropertyName': {
         $xml = "<ogc:PropertyName>$comp->Value</ogc:PropertyName>";
      }break;
      case 'Literal': {
         $xml = "<ogc:Literal>$comp->Value</ogc:Literal>";
		}break;
      case 'Polygon': {
         $wkt  = $comp->Value;
         $xml = wkt_to_gml_polygon($wkt);
      }break;
      default: {
         $xml = "<$nspc:$mthd>";
         foreach($args as $arg) $xml.= $this->getxml($arg);
         $xml.= "</$nspc:$mthd>";
      }break;
      }
      return $xml;
   }
}
?>