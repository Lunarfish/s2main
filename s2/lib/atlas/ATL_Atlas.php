<?php
/*
   species       varchar(100) NOT NULL,
  recorder      varchar(100) DEFAULT NULL,
  location      varchar(100) DEFAULT NULL,
  gridref       varchar(14) DEFAULT NULL,
  start_date    date DEFAULT NULL,
  end_date      date DEFAULT NULL,
  comments      text,
*/
class ATL_Raw_Data extends DBT {
   protected $tablename = 'ATL_Raw_Data';
   protected $displayname = 'Atlas Raw Data';
   protected $domain = 'atlas';
   protected $columns = array(
      'ATL_Raw_Data_ID'   => 'Unique Key',
      'Species'                        => '{"DataType":"Taxon Version Key"}',
      'Recorder'                       => '{"DataType":"Short Text"}',
      'Location'                       => '{"DataType":"Short Text"}',
      'Gridref'                        => '{"DataType":"Short Text"}',
      'Start_Date'                     => '{"DataType":"Date"}',
      'End_Date'                       => '{"DataType":"Date"}',
      'Comments'                       => '{"DataType":"Long Text","NoList":1}'
   ); 
   protected $permissions = array(
      'Def'    => 'Atlas Administrator',
      'List'   => 'Atlas View'
   );
   protected $defaultpermissions = array(
      'USR_Permissions' => '
"User_Id","Domain","Label","Value"
1,"atlas","Atlas View",1
1,"atlas","Atlas Full",1
1,"atlas","Atlas Administrator",1',
      'USR_Default_Permissions' => '
"Domain","Label","Description","Value","Anon" 
"atlas","Atlas Administrator","Administer atlas data",0,0
"atlas","Atlas Full","View all atlas data",0,0
"atlas","Atlas View","View atlas data to public level",0,0'   
   );
}

class ATL_Records extends DBT_Analysis {
   protected $tablename = 'S2_Designation_Overview';
   protected $displayname = 'Designation Overview';
   //protected $show = array('S2_Site_Management_Agreement');
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $indexon = 'S2_Designation_ID';  
   protected $selectto = 'S2_Designation';
   protected $columns = array(
      'S2_Authority_ID'          => '{"DataType":"LINKEDTO","TargetType":"S2_Authority","TargetField":"S2_Authority_ID","Mandatory":1,"Current":1,"Hidden":1,"Inherit":1}',
      'Authority'                => '{"DataType":"Short Text","Hidden":1}',
      'S2_Designation_ID'        => '{"DataType":"LINKEDTO","TargetType":"S2_Designation","TargetField":"S2_Designation_ID","Mandatory":1,"Current":1}',
      'Designation'              => '{"DataType":"Short Text","Hidden":1}',
      'Sites'                    => '{"DataType":"Number"}',
      'Appropriate_Management'   => '{"DataType":"Number"}',
      'Percentage_In_Management' => '{"DataType":"Percentage"}',
      'Candidates'               => '{"DataType":"Number"}',
      'Designated'               => '{"DataType":"Number"}',
      'Deleted'                  => '{"DataType":"Number"}',                    
   ); 
   protected $relieson = array(  
      "S2_Authority",
      "S2_Designation",
      "S2_Site",
      "S2_Site_Designation",
      "S2_Site_Appropriate_Management_Decision",
      "S2_Designation_Status",
      "S2_Site_Designation_Status"
   );
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
   function S2_Designation_Overview() {
      parent::DBT_Analysis();
   }
   function create() {
      global $pending;
      $prereqsmet = true;
      foreach($this->relieson as $lclass) {
         $link = new $lclass();
         if (!$link->exists()) $prereqsmet = false;  
      }
      if ($prereqsmet) {
         $select = "select 
   raw_data.raw_data_id AS raw_data_id,
   raw_data.species AS species,
   raw_data.recorder AS recorder,
   raw_data.location AS location,
   raw_data.gridref AS gridref,
   yhedn_getgrprecision(raw_data.gridref) AS precision,
   raw_data.start_date AS start_date,
   raw_data.end_date AS end_date,
   date_part('year',raw_data.start_date) AS year,
   raw_data.comments AS comments,
   raw_data.created AS created,
   yhedn_derivegr(raw_data.gridref,10000) AS gr10k,
   yhedn_derivegr(raw_data.gridref,2000) AS gr2k,
   yhedn_derivegr(raw_data.gridref,1000) AS gr1k,
   yhedn_derivegr(raw_data.gridref,100) AS gr100m 
from raw_data;"; 
         $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
         $this->dbhandle->exec($statement);
         unset($pending[$this->tablename]);
      }
   }
   function getuserpermissions() {
      return false;      
   }   
}
?>