<?php
error_reporting(E_ALL);
/* 
   PUB = Public 
   
   Set of classes for conducting public engagement recording programmes 
   including BioBlitzes, Recording at reserves or wildlife sites 
   
   These classes link to the bioblitz.yhedn.org.uk pages to create recording 
   applications and QRCoded location badges.
*/

include_once('settings.conf');
include_once('snc/S2_DBT.php');
class PUB_Survey extends DBT {
   protected $tablename = 'PUB_Survey';
   protected $displayname = 'Survey Event';
   protected $alts = array('Contributors' => 'PUB_Contributor','Species' => 'PUB_Species','Taxonomy' => 'PUB_Taxonomy');
   protected $show = array('Zones' => 'PUB_Recording_Zone','Bingo Forms' => 'PUB_Bingo_Form');
   protected $view = '[
      {
         "Name":"People",
         "Data": [
            {
               "Table":"PUB_Contributor"
            }
         ]
      },
      {
         "Name":"Geography",
         "Data": [
            {
               "Table":"PUB_Survey_Location"
            },
            {
               "Table":"PUB_Recording_Zone"
            }
         ]
      },
      {
         "Name":"Bingo",
         "Data": [
            {
               "Table":"PUB_Bingo_Form"
            },
            {
               "Table":"PUB_Bingo_Record"
            }
         ]
      },
      {
         "Name":"Records",
         "Data": [
            {
               "Table":"PUB_Species_Record"
            },
            {
               "Table":"PUB_Contributor_Species"
            },
            {
               "Table":"PUB_Zone_Species"
            }
         ]
      },
      {
         "Name":"Stats",
         "Data": [
            {
               "Table":"PUB_Species_By_Hour"
            },
            {
               "Table":"PUB_Species_First_Record"
            },
            {
               "Table":"PUB_Records_By_Hour"
            },
            {
               "Table":"PUB_Recorded_Species"
            },
            {
               "Table":"PUB_Recorded_Genus"
            },
            {
               "Table":"PUB_Recorded_Family"
            },
            {
               "Table":"PUB_Recorded_Order"
            },
            {
               "Table":"PUB_Recorded_Class"
            },
            {
               "Table":"PUB_Recorded_Phylum"
            },
            {
               "Table":"PUB_Recorded_Kingdom"
            }
         ]
      },
      {
         "Name":"Species Database",
         "Data": [
            {
               "Name":"Species",
               "Table":"PUB_Species_Filter",
               "Show":"Every"
            },
            {
               "Name":"All Taxa",
               "Table":"PUB_Species",
               "Show":"Every"
            },
            {
               "Name":"Hierarchy",
               "Table":"PUB_Taxonomy",
               "Show":"Every"
            }
         ]
      }
   ]'; 
   protected $columns = array(
      'PUB_Survey_ID'      => 'Unique Key',
      'PUB_Survey_Type_ID' => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey_Type","TargetField":"PUB_Survey_Type_ID","Inherit":1}',
      'Name'               => 'Short Text',
      'From_Date'          => 'Date',
      'To_Date'            => 'Date',
      'Lead_Contact'       => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"]}'
   );
   protected $domain = 'public';
   protected $permissions = array(
      'Edit'   => 'Registered User',
      'Add'    => 'Registered User'
   );
   protected $defaultpermissions = array(
      'USR_Permissions' => '
"User_Id","Domain","Label","Value"
1,"public","Registered User",1
1,"public","System Administrator",1',
      'USR_Default_Permissions' => '
"Domain","Label","Description","Value","Anon" 
"public","System Administrator","Add and edit partnerships",0,0
"public","Registered User","View unprotected data",1,0'   
   );
}

class PUB_Survey_Type extends DBT {
   protected $tablename = 'PUB_Survey_Type';
   protected $displayname = 'Survey Type';
   protected $alts = array('Events' => 'PUB_Survey');
   protected $columns = array(
      'PUB_Survey_Type_ID' => 'Unique Key',
      'Name'               => '{"DataType":"Short Text","Mandatory":1}',
      'Description'        => 'Long Text' 
   );
   protected $domain = 'public';
   protected $permissions = array(
      'Edit'   => 'Registered User',
      'Add'    => 'Registered User',
      'Delete' => 'NO-ONE'
   );
   protected $primingdata = array(
      'PUB_Survey_Type' => '
"Name","Description"
"BioBlitz","Public engagement activities to find as many species as possible in 24 hours."
"Monitoring Survey","Formal ongoing recording for a number of known sites."
"Informal","Ongoing unstructured recording like gardens and nature reserves."'
   );   
}
class PUB_Survey_Location extends DBT {
   protected $tablename = 'PUB_Survey_Location';
   protected $displayname = 'Location';
   protected $alts = array('Events' => 'PUB_Survey');
   protected $columns = array(
      'PUB_Survey_Location_ID'   => 'Unique Key',
      'PUB_Survey_ID'            => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Current":1,"Inherit":1}',
      'Post_Code'                => 'Short Text',
      'Sector'                   => 'Short Text',
      'Grid_Reference'           => '{"DataType":"Short Text","Derives":[{"Column":"On_Map","Method":"s2_getgrsquare","Params":[{"Type":"Value"}]}]}',
      'On_Map'                   => '{"DataType":"Map Data","Derives":[{"Column":"Grid_Reference","Method":"s2_getgr","Params":[{"Type":"Value"}]}]}'
   );
   protected $domain = 'public';
   protected $permissions = array(
      'Edit'   => 'Registered User',
      'Add'    => 'Registered User',
      'Delete' => 'NO-ONE'
   );

}
class PUB_Bingo_Form extends DBT {
   protected $tablename = 'PUB_Bingo_Form';
   protected $displayname = 'Bingo Form';
   protected $alts = array();
   protected $show = array('Bingo Species' => 'PUB_Bingo_Species');
   protected $columns = array(
      'PUB_Bingo_Form_ID'  => 'Unique Key',
      'PUB_Survey_ID'      => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Current":1,"Inherit":1}',
      'Name'               => 'Short Text'
   );
   protected $domain = 'public';
   protected $permissions = array(
      'Edit'   => 'Registered User',
      'Add'    => 'Registered User'
   );
}
class PUB_Bingo_Species extends DBT {
   protected $tablename = 'PUB_Bingo_Species';
   protected $displayname = 'Bingo Species';
   protected $alts = array();
   protected $columns = array(
      'PUB_Bingo_Species_ID'  => 'Unique Key',
      'PUB_Bingo_Form_ID'     => '{"DataType":"LINKEDTO","TargetType":"PUB_Bingo_Form","TargetField":"PUB_Bingo_Form_ID","Current":1,"Inherit":1}',
      'PUB_Species_ID'        => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}'
   );
   protected $domain = 'public';
   protected $permissions = array(
      'Edit'   => 'Registered User',
      'Add'    => 'Registered User'
   );   
}
class PUB_Recording_Zone extends DBT {
   protected $tablename = 'PUB_Recording_Zone';
   protected $displayname = 'Recording Zone';
   protected $alts = array();
   protected $columns = array(
      'PUB_Recording_Zone_ID' => 'Unique Key',
      'PUB_Survey_ID'         => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Current":1,"Inherit":1}',
      'Name'                  => 'Short Text',
      'Grid_Reference'        => '{"DataType":"Short Text","Derives":[{"Column":"On_Map","Method":"s2_getgrsquare","Params":[{"Type":"Value"}]}]}',
      'On_Map'                => '{"DataType":"Map Data","Derives":[{"Column":"Grid_Reference","Method":"s2_getgr","Params":[{"Type":"Value"}]}]}'
   );
   protected $domain = 'public';
   protected $permissions = array(
      'Edit'   => 'Registered User',
      'Add'    => 'Registered User'
   );
}
/*
   Sessions, Contributors and Records can be entered without a login.
   Records are retrospectively checked with the contributor by email. 
   
*/
class PUB_Session extends DBT {
   protected $tablename = 'PUB_Session';
   protected $displayname = 'Session';
   protected $alts = array();
   protected $columns = array(
      'PUB_Session_ID'        => 'Unique Key',
      'PUB_Survey_ID'         => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Current":1,"Inherit":1}',
      'PUB_Contributor_ID'    => '{"DataType":"LINKEDTO","TargetType":"PUB_Contributor","TargetField":"PUB_Contributor_ID","Current":1,"Inherit":1}',
      'Name'                  => 'Short Text',
      'Grid_Reference'        => '{"DataType":"Short Text","Derives":[{"Column":"On_Map","Method":"s2_getgrsquare","Params":[{"Type":"Value"}]}]}',
      'On_Map'                => '{"DataType":"Map Data","Derives":[{"Column":"Grid_Reference","Method":"s2_getgr","Params":[{"Type":"Value"}]}]}'
   );
   protected $domain = 'public';
   protected $permissions = array();
}
/* 
   Recording statement - 
  "The records you make today are your property. 
  
   By ticking the box below you grant us permission to pass your records on to 
   the Local Record Centre who will provide data to help inform decisions about 
   conservation and planning and to produce stats to help understand things 
   like climate change.
   
   If you prefer you can choose not to tick the box and we will just treat your 
   records as a contribution to today's event."
*/
class PUB_Contributor extends DBT {
   protected $tablename = 'PUB_Contributor';
   protected $displayname = 'Contributor';
   protected $alts = array();
   protected $columns = array(
      'PUB_Contributor_ID'  => 'Unique Key',
      'PUB_Survey_ID'       => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Current":1,"Inherit":1}',
      'Name'                => 'Short Text',
      'Email'               => 'Email',
      'Phone'               => 'Short Text',
      'Use_My_Data'         => 'True or False',
      'Is_Trusted_Recorder' => 'True or False'     
   );
   protected $domain = 'public';
   protected $permissions = array();
}
class PUB_Species_Record extends DBT {
   protected $tablename = 'PUB_Species_Record';
   protected $displayname = 'Species Record';
   protected $alts = array();
   protected $columns = array(
      'PUB_Species_Record_ID' => 'Unique Key',
      'PUB_Survey_ID'         => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Current":1,"Inherit":1}',
      'PUB_Contributor_ID'    => '{"DataType":"LINKEDTO","TargetType":"PUB_Contributor","TargetField":"PUB_Contributor_ID","Current":1,"Inherit":1}',
      'PUB_Species_ID'        => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'PUB_Recording_Zone_ID' => '{"DataType":"LINKEDTO","TargetType":"PUB_Recording_Zone","TargetField":"PUB_Recording_Zone_ID","Derives":[{"Column":"Location","Method":"s2_getcolumnvalue","Params":[{"Type":"Static","Value":"PUB_Recording_Zone"},{"Type":"Value"},{"Type":"Static","Value":"Name"}]},{"Column":"Grid_Reference","Method":"s2_getcolumnvalue","Params":[{"Type":"Static","Value":"PUB_Recording_Zone"},{"Type":"Value"},{"Type":"Static","Value":"Grid_Reference"}]}]}',
      'Location'              => '{"DataType":"Short Text","NoList":true}',
      'Grid_Reference'        => '{"DataType":"Short Text","NoList":true,"Derives":[{"Column":"On_Map","Method":"s2_getgrsquare","Params":[{"Type":"Value"}]}]}',
      'On_Map'                => '{"DataType":"Map Data","Derives":[{"Column":"Grid_Reference","Method":"s2_getgr","Params":[{"Type":"Value"}]}]}',
      'On_Date'               => '{"DataType":"Date","NoList":true}',
      'At_Time'               => '{"DataType":"Time","NoList":true}',
      'Comment'               => '{"DataType":"Long Text","NoList":true}'
   );
   protected $domain = 'public';
   protected $permissions = array();
   function insert() {
      $sids = array();
      if (preg_match('/^\[/',$this->data['PUB_Species_ID'])) {
         $svals = json_decode($this->data['PUB_Species_ID']);
         foreach ($svals as $x => $sx) {
            $s = new PUB_Species();
            $s->setdata(array('Latin_Name' => json_encode($sx)));
            $s->insert();
            $this->data['PUB_Species_ID'] = $s->getid();
            parent::insert();
            $sids[] = $this->getid();
         }
      } else if (preg_match('/\{/',$this->data['PUB_Species_ID'])) {
         $s = new PUB_Species();
         $s->setdata(array('Latin_Name' => $this->data['PUB_Species_ID']));
         $s->insert();
         $this->data['PUB_Species_ID'] = $s->getid();
         parent::insert();
         $sids[] = $this->getid();
      } else {
         parent::insert();
         $sids = $this->getid();
      }
      return $sids;
   } 
}
/*
   Bingo Record is triggered when someone completes the bingo and 
*/
class PUB_Bingo_Record extends DBT {
   protected $tablename = 'PUB_Bingo_Record';
   protected $displayname = 'Bingo Record';
   protected $alts = array();
   protected $columns = array(
      'PUB_Bingo_Species_ID'  => 'Unique Key',
      'PUB_Survey_ID'         => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Current":1,"Inherit":1}',
      'PUB_Bingo_Form_ID'     => '{"DataType":"LINKEDTO","TargetType":"PUB_Bingo_Form","TargetField":"PUB_Bingo_Form_ID","Current":1,"Inherit":1}',
      'PUB_Contributor_ID'    => '{"DataType":"LINKEDTO","TargetType":"PUB_Contributor","TargetField":"PUB_Contributor_ID","Current":1,"Inherit":1}',
      'On_Date'               => 'Date'
   );
   protected $domain = 'public';
   protected $permissions = array();
}
class PUB_Species extends DBT {
   protected $tablename = 'PUB_Species';
   protected $displayname = 'Species';
   protected $alts = array('Contributors' => 'PUB_Contributor','Events' => 'PUB_Survey','Taxonomy' => 'PUB_Taxonomy');
   protected $columns = array(
      'PUB_Species_ID'  => 'Unique Key',
      'Latin_Name'      => '{"DataType":"Short Text","Interface":"Taxa","Use":"Name"}',
      'Common_Name'     => '{"DataType":"Short Text","ReadOnly":true}',
      'Authority'       => '{"DataType":"Short Text","ReadOnly":true,"NoList":true}',
      'Rank'            => '{"DataType":"Short Text","ReadOnly":true}',
      'TVK'             => '{"DataType":"Short Text","ReadOnly":true,"NoList":true}',
      'EOL_Taxa'        => '{"DataType":"Number","ReadOnly":true,"NoList":true}',
      'EOL_Page'        => '{"DataType":"Number","ReadOnly":true,"NoList":true}'
   );
   protected $domain = 'public';
   protected $permissions = array('Delete'=>'NO-ONE');
   protected $withtaxonomy = true;
   protected $ranks = array('kingdom','division','phylum','class','order','family','genus','species');
   function insert() {
      // allow population of json data from Taxa interface.
      if (preg_match('/^\{/',$this->data['Latin_Name'])) {
         $nbntaxa = json_decode($this->data['Latin_Name']);
         $latin = $nbntaxa->Name;
         $tvk = $nbntaxa->Value;
         
      } else $latin = $this->data['Latin_Name'];
      // deal with NHM Bombus (Bombus) terrestris style names
      $latin = preg_replace('/\s*\([^\)]+\)\s*/',' ',$latin);
      $trank = $this->data['Rank'];
      //$tdata = $this->data;
      $this->loaddata($latin,'Latin_Name');
      
      // check if species already exists in database using latin name
      if (!isset($this->id)) {
         $this->data['Latin_Name'] = $latin;
         $this->data['TVK'] = (isset($tvk))?$tvk:null;
         $this->data['Rank'] = $trank;
         parent::insert();
      }
      $tx = new PUB_Taxonomy();
      if (!$tx->exists()) $tx->create();
      $tx->loaddata($this->id,'Recorded_As');
      $xid = $tx->getid();
      if (!isset($xid) && $this->withtaxonomy) {
         // use nbn api and eol api to populate other fields from latin name
         include_once('eol/eolapi.php');
         $pgs = eolgetpagesforspecies($latin);
         if (isset($pgs)) {
            list($pg1,$tx1) = eolgettaxonidforpages($pgs);
            if (isset($tx1)) {
               $sh = eolgethierarchyfortaxa($tx1,$latin);
               $vn = eolgetvernacularname($latin,$sh);
               $this->data['Common_Name'] = $vn;
               $this->data['EOL_Taxa'] = $tx1;
               $this->data['EOL_Page'] = $pg1;
               $species_ranks = eolgetranklist($tx1,$latin);
               $rdata = array();
               $t = null;
               foreach($species_ranks as $rank => $lname) {
                  if ($latin == $lname) {
                     $this->data['Rank'] = $rank;
                     $this->data['Latin_Name'] = $latin;
                     $this->data['TVK'] = $tvk;
                     $this->update($this->getpk(),$this->getid());
                     $tid = $this->getid();
                     switch($rank) {
                     case 'phylum':   $rdata['Rank_Phylum_or_Division'] = $tid; break;
                     case 'division':  $rdata['Rank_Phylum_or_Division'] = $tid; break;
                     case 'subspecies':   break; // ignore subspecies 
                     case 'infraspecies': break; // ignore subspecies 
                     case 'variety':      break; // ignore subspecies 
                     case '':             break; // ignore missing rank 
                     default:          $rdata['Rank_'.ucwords($rank)] = $tid; break;                
                     }
                     $rdata['Recorded_As'] = $tid;
                  } else {
                     $t = new PUB_Species();
                     $td = array('Latin_Name' => $lname,'Rank' => $rank);
                     $t->setdata($td);
                     $t->inctaxonomy(false);
                     $tid = $t->insert();
                     switch($rank) {
                     case 'phylum':   $rdata['Rank_Phylum_or_Division'] = $tid; break;
                     case 'division':  $rdata['Rank_Phylum_or_Division'] = $tid; break;
                     case 'subspecies':   break; // ignore subspecies 
                     case 'infraspecies': break; // ignore subspecies 
                     case 'variety':      break; // ignore subspecies 
                     case '':             break; // ignore missing rank 
                     default:          $rdata['Rank_'.ucwords($rank)] = $tid; break;                
                     } 
                  }
               }
               $tx = new PUB_Taxonomy();
               $tx->setdata($rdata);
               $tx->insert();
            }
         }
      } else {
         $this->data['TVK'] = $tvk;
         $this->update($this->getpk(),$this->getid());
      }
      return $this->getid();      
   }
   function inctaxonomy($val) {
      $this->withtaxonomy = $val;
   }
   function delete() {
      // delete any references to relevant rank from taxonomy 
      // if not specific level then entries can't be deleted as other specific 
      // entries may rely on current
      // alternatively - just refuse to delete altogether.
      return 0;  
   }
   function getname() {
      $common = $this->data['Common_Name'];
      $latin = $this->data['Latin_Name'];
      return (isset($common) && $common != "")?"$common ($latin)":$latin; 
   }
}
class PUB_Species_Filter extends DBT_FilterView {
   protected $sourcetable = 'PUB_Species';
   protected $tablename = 'PUB_Species_Filter';
   protected $filters = array('Rank' => 'species');
   protected $orders = array('Latin_Name' => 'ASC');
   protected $domain = 'public';
   function PUB_Species_Filter() {
      parent::DBT_FilterView();
   }
}

class PUB_Taxonomy extends DBT {
   protected $tablename = 'PUB_Taxonomy';
   protected $displayname = 'Taxonomy';
   protected $alts = array('Contributors' => 'PUB_Contributor','Events' => 'PUB_Survey','Species' => 'PUB_Species');
   protected $columns = array(
      'PUB_Taxonomy_ID'             => 'Unique Key',
      'Recorded_As'                 => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}', 
      'Rank_Kingdom'                => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","NoList":1}',
      'Rank_Phylum_or_Division'     => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","NoList":1}',
      'Rank_Class'                  => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","NoList":1}',
      'Rank_Order'                  => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'Rank_Family'                 => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'Rank_Genus'                  => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'Rank_Species'                => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}'
   );
   protected $domain = 'public';
   protected $permissions = array('Delete'=>'NO-ONE');
}
class PUB_Recorded_Species extends DBT_ReportView {
   protected $tablename = 'PUB_Recorded_Species';
   protected $displayname = 'Recorded Species';
   protected $domain = 'public';
   protected $instancedomain = false;
   protected $indexon = 'PUB_Species';  
   protected $selectto = 'PUB_Species';
   protected $perpage = 5;
   protected $columns = array(
      'PUB_Survey_ID'   => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Mandatory":1,"Current":1,"Hidden":1,"Inherit":1}',
      'PUB_Species_ID'  => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","Mandatory":1,"Current":1}',
      'Records'         => 'Number',
      'Kingdom'         => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","Mandatory":1,"NoList":1}',
      'Phylum'          => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","Mandatory":1,"NoList":1}',
      'Class'           => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","Mandatory":1,"NoList":1}',
      'Order'           => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","Mandatory":1}',
      'Family'          => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","Mandatory":1}',
      'Genus'           => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","Mandatory":1}',
      'Species'         => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","Mandatory":1}',                                         
   ); 
   protected $relieson = array(
      "PUB_Survey",  
      "PUB_Species",
      "PUB_Species_Record",
      "PUB_Taxonomy"
   );
   function create() {
      foreach($this->relieson as $lclass) {
         $link = new $lclass();
         if ($link && !$link->exists()) $link->create();
      }
      $select = "SELECT
         SR.PUB_Survey_ID,
         SR.PUB_Species_ID,
         COUNT(*) AS `Records`, 
         TX.Rank_Kingdom AS `Kingdom`,
         TX.Rank_Phylum_or_Division AS `Phylum`,
         TX.Rank_Class AS `Class`,
         TX.Rank_Order AS `Order`,
         TX.Rank_Family AS `Family`,
         TX.Rank_Genus AS `Genus`,
         TX.Rank_Species AS `Species`
      FROM PUB_Species_Record AS SR
      INNER JOIN PUB_Taxonomy AS TX 
      ON TX.Recorded_As = SR.PUB_Species_ID
      GROUP BY PUB_Survey_ID,PUB_Species_ID;"; 
      $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
      $this->dbhandle->exec($statement);
      unset($pending[$this->tablename]);
   }
}
class PUB_Recorded_Genus extends DBT_ReportView {
   protected $tablename = 'PUB_Recorded_Genus';
   protected $displayname = 'Recorded Genus';
   protected $domain = 'public';
   protected $instancedomain = false;
   protected $indexon = 'PUB_Species';  
   protected $selectto = 'PUB_Species';
   protected $perpage = 5;
   protected $columns = array(
      'PUB_Survey_ID'   => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Mandatory":1,"Current":1,"Hidden":1,"Inherit":1}',
      'Genus'           => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'Records'         => 'Number',
      'Species'         => 'Number',
      'Kingdom'         => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","NoList":1}',
      'Phylum'          => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","NoList":1}',
      'Class'           => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","NoList":1}',
      'Order'           => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'Family'          => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}'
   ); 
   protected $relieson = array(
      "PUB_Survey",  
      "PUB_Species",
      "PUB_Species_Record",
      "PUB_Taxonomy"
   );
   function create() {
      foreach($this->relieson as $lclass) {
         $link = new $lclass();
         if ($link && !$link->exists()) $link->create();
      }
      $select = "SELECT 
            RS.PUB_Survey_ID, 
            COUNT( * ) AS `Species` , 
            SUM( RS.Records ) AS `Records` , 
            RS.Kingdom AS `Kingdom` , 
            RS.Phylum AS `Phylum` , 
            RS.Class AS `Class` , 
            RS.Order AS `Order` , 
            RS.Family AS `Family` , 
            RS.Genus AS `Genus`
         FROM PUB_Recorded_Species AS RS
         WHERE Genus IS NOT NULL
         GROUP BY PUB_Survey_ID, Genus;"; 
      $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
      $this->dbhandle->exec($statement);
      unset($pending[$this->tablename]);
   }
}
class PUB_Recorded_Family extends DBT_ReportView {
   protected $tablename = 'PUB_Recorded_Family';
   protected $displayname = 'Recorded Family';
   protected $domain = 'public';
   protected $instancedomain = false;
   protected $indexon = 'PUB_Species';  
   protected $selectto = 'PUB_Species';
   protected $perpage = 5;
   protected $columns = array(
      'PUB_Survey_ID'   => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Mandatory":1,"Current":1,"Hidden":1,"Inherit":1}',
      'Family'          => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'Records'         => 'Number',
      'Genera'          => 'Number',
      'Kingdom'         => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","NoList":1}',
      'Phylum'          => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","NoList":1}',
      'Class'           => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'Order'           => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}'
   ); 
   protected $relieson = array(
      "PUB_Survey",  
      "PUB_Species",
      "PUB_Species_Record",
      "PUB_Taxonomy"
   );
   function create() {
      foreach($this->relieson as $lclass) {
         $link = new $lclass();
         if ($link && !$link->exists()) $link->create();
      }
      $select = "SELECT 
            RS.PUB_Survey_ID, 
            COUNT( RS.Genus ) AS `Genera` , 
            SUM( RS.Records ) AS `Records` , 
            RS.Kingdom AS `Kingdom` , 
            RS.Phylum AS `Phylum` , 
            RS.Class AS `Class` , 
            RS.Order AS `Order` , 
            RS.Family AS `Family`
         FROM PUB_Recorded_Species AS RS
         WHERE Family IS NOT NULL
         GROUP BY PUB_Survey_ID, Family;"; 
      $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
      $this->dbhandle->exec($statement);
      unset($pending[$this->tablename]);
   }
}
class PUB_Recorded_Order extends DBT_ReportView {
   protected $tablename = 'PUB_Recorded_Order';
   protected $displayname = 'Recorded Order';
   protected $domain = 'public';
   protected $instancedomain = false;
   protected $indexon = 'PUB_Species';  
   protected $selectto = 'PUB_Species';
   protected $perpage = 5;
   protected $columns = array(
      'PUB_Survey_ID'   => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Mandatory":1,"Current":1,"Hidden":1,"Inherit":1}',
      'Order'           => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'Records'         => 'Number',
      'Families'        => 'Number',
      'Kingdom'         => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID","NoList":1}',
      'Phylum'          => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'Class'           => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}'
   ); 
   protected $relieson = array(
      "PUB_Survey",  
      "PUB_Species",
      "PUB_Species_Record",
      "PUB_Taxonomy"
   );
   function create() {
      foreach($this->relieson as $lclass) {
         $link = new $lclass();
         if ($link && !$link->exists()) $link->create();
      }
      $select = "SELECT 
            RS.PUB_Survey_ID, 
            COUNT( RS.Family ) AS `Families` , 
            SUM( RS.Records ) AS `Records` , 
            RS.Kingdom AS `Kingdom` , 
            RS.Phylum AS `Phylum` , 
            RS.Class AS `Class` , 
            RS.Order AS `Order`
         FROM PUB_Recorded_Species AS RS
         WHERE `Order` IS NOT NULL
         GROUP BY PUB_Survey_ID, `Order`;"; 
      $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
      $this->dbhandle->exec($statement);
      unset($pending[$this->tablename]);
   }
}
class PUB_Recorded_Class extends DBT_ReportView {
   protected $tablename = 'PUB_Recorded_Class';
   protected $displayname = 'Recorded Class';
   protected $domain = 'public';
   protected $instancedomain = false;
   protected $indexon = 'PUB_Species';  
   protected $selectto = 'PUB_Species';
   protected $perpage = 5;
   protected $columns = array(
      'PUB_Survey_ID'   => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Mandatory":1,"Current":1,"Hidden":1,"Inherit":1}',
      'Class'           => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'Records'         => 'Number',
      'Orders'          => 'Number',
      'Kingdom'         => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'Phylum'          => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}'
   ); 
   protected $relieson = array(
      "PUB_Survey",  
      "PUB_Species",
      "PUB_Species_Record",
      "PUB_Taxonomy"
   );
   function create() {
      foreach($this->relieson as $lclass) {
         $link = new $lclass();
         if ($link && !$link->exists()) $link->create();
      }
      $select = "SELECT 
            RS.PUB_Survey_ID, 
            COUNT( RS.Order ) AS `Orders` , 
            SUM( RS.Records ) AS `Records` , 
            RS.Kingdom AS `Kingdom` , 
            RS.Phylum AS `Phylum` , 
            RS.Class AS `Class`
         FROM PUB_Recorded_Species AS RS
         WHERE Class IS NOT NULL
         GROUP BY PUB_Survey_ID, Class;"; 
      $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
      $this->dbhandle->exec($statement);
      unset($pending[$this->tablename]);
   }
}
class PUB_Recorded_Phylum extends DBT_ReportView {
   protected $tablename = 'PUB_Recorded_Phylum';
   protected $displayname = 'Recorded Phylum';
   protected $domain = 'public';
   protected $instancedomain = false;
   protected $indexon = 'PUB_Species';  
   protected $selectto = 'PUB_Species';
   protected $perpage = 5;
   protected $columns = array(
      'PUB_Survey_ID'   => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Mandatory":1,"Current":1,"Hidden":1,"Inherit":1}',
      'Phylum'          => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'Records'         => 'Number',
      'Classes'         => 'Number',
      'Kingdom'         => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}'
   ); 
   protected $relieson = array(
      "PUB_Survey",  
      "PUB_Species",
      "PUB_Species_Record",
      "PUB_Taxonomy"
   );
   function create() {
      foreach($this->relieson as $lclass) {
         $link = new $lclass();
         if ($link && !$link->exists()) $link->create();
      }
      $select = "SELECT 
            RS.PUB_Survey_ID, 
            COUNT( RS.Class ) AS `Classes` , 
            SUM( RS.Records ) AS `Records` , 
            RS.Kingdom AS `Kingdom` , 
            RS.Phylum AS `Phylum`
         FROM PUB_Recorded_Species AS RS
         WHERE Phylum IS NOT NULL
         GROUP BY PUB_Survey_ID, Phylum;"; 
      $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
      $this->dbhandle->exec($statement);
      unset($pending[$this->tablename]);
   }
}
class PUB_Recorded_Kingdom extends DBT_ReportView {
   protected $tablename = 'PUB_Recorded_Kingdom';
   protected $displayname = 'Recorded Kingdom';
   protected $domain = 'public';
   protected $instancedomain = false;
   protected $indexon = 'PUB_Species';  
   protected $selectto = 'PUB_Species';
   protected $perpage = 5;
   protected $columns = array(
      'PUB_Survey_ID'   => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Mandatory":1,"Current":1,"Hidden":1,"Inherit":1}',
      'Kingdom'         => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'Records'         => 'Number',
      'Phyla'           => 'Number'
   ); 
   protected $relieson = array(
      "PUB_Survey",  
      "PUB_Species",
      "PUB_Species_Record",
      "PUB_Taxonomy"
   );
   function create() {
      foreach($this->relieson as $lclass) {
         $link = new $lclass();
         if ($link && !$link->exists()) $link->create();
      }
      $select = "SELECT 
            RS.PUB_Survey_ID, 
            COUNT( RS.Phylum ) AS `Phyla` , 
            SUM( RS.Records ) AS `Records` , 
            RS.Kingdom AS `Kingdom`
         FROM PUB_Recorded_Species AS RS
         WHERE Kingdom IS NOT NULL
         GROUP BY PUB_Survey_ID, Kingdom;"; 
      $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
      $this->dbhandle->exec($statement);
      unset($pending[$this->tablename]);
   }
}
class PUB_Records_By_Hour extends DBT_ReportView {
   protected $tablename = 'PUB_Records_By_Hour';
   protected $displayname = 'Records By Hour';
   protected $domain = 'public';
   protected $instancedomain = false;
   protected $indexon = 'PUB_Species';  
   protected $selectto = 'PUB_Species';
   protected $perpage = 5;
   protected $columns = array(
      'PUB_Survey_ID'   => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Mandatory":1,"Current":1,"Hidden":1,"Inherit":1}',
      'On_Date'         => '{"DataType":"Date"}',
      'At_Hour'         => 'Number',
      'Records'         => 'Number'
   ); 
   protected $relieson = array(
      "PUB_Survey",  
      "PUB_Species",
      "PUB_Species_Record"
   );
   function create() {
      foreach($this->relieson as $lclass) {
         $link = new $lclass();
         if ($link && !$link->exists()) $link->create();
      }
      $select = "SELECT 
            SR.PUB_Survey_ID, 
            COUNT( * ) AS `Records`,
            SR.On_Date AS `On_Date`, 
            HOUR( SR.At_Time ) AS `At_Hour`
         FROM PUB_Species_Record AS SR
         GROUP BY SR.PUB_Survey_ID, SR.On_Date, HOUR(SR.At_Time)"; 
      $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
      $this->dbhandle->exec($statement);
      unset($pending[$this->tablename]);
   }
}
class PUB_Species_First_Record extends DBT_ReportView {
   protected $tablename = 'PUB_Species_First_Record';
   protected $displayname = 'Species First Record';
   protected $domain = 'public';
   protected $instancedomain = false;
   protected $indexon = 'PUB_Species';  
   protected $selectto = 'PUB_Species';
   protected $perpage = 5;
   protected $columns = array(
      'PUB_Survey_ID'   => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Mandatory":1,"Current":1,"Hidden":1,"Inherit":1}',
      'PUB_Species_ID'  => '{"DataType":"LINKEDTO","TargetType":"PUB_Species","TargetField":"PUB_Species_ID"}',
      'First_Recorded'  => '{"DataType":"Datea and Time"}',
      'On_Date'         => '{"DataType":"Date"}',
      'At_Hour'         => 'Number'
   ); 
   protected $order = array("First_Recorded DESC");
   protected $relieson = array(
      "PUB_Survey",  
      "PUB_Species",
      "PUB_Species_Record"
   );
   function create() {
      foreach($this->relieson as $lclass) {
         $link = new $lclass();
         if ($link && !$link->exists()) $link->create();
      }
      $select = "SELECT 
            SR.PUB_Survey_ID, 
            SR.PUB_Species_ID, 
            MIN(CONCAT(SR.On_Date, ' ', SR.At_Time)) As First_Recorded, 
            DATE(MIN(CONCAT(SR.On_Date, ' ', SR.At_Time))) As On_Date, 
            HOUR(MIN(CONCAT(SR.On_Date, ' ', SR.At_Time))) As At_Hour 
         FROM `pub_species_record` AS SR 
         GROUP BY SR.PUB_Survey_ID , SR.PUB_Species_ID
         ORDER BY On_Date, At_Hour"; 
      $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
      $this->dbhandle->exec($statement);
      unset($pending[$this->tablename]);
   }
}
class PUB_Species_By_Hour extends DBT_ReportView {
   protected $tablename = 'PUB_Species_By_Hour';
   protected $displayname = 'Species By Hour';
   protected $domain = 'public';
   protected $instancedomain = false;
   protected $indexon = 'PUB_Species';  
   protected $selectto = 'PUB_Species';
   protected $perpage = 5;
   protected $columns = array(
      'PUB_Survey_ID'   => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Mandatory":1,"Current":1,"Hidden":1,"Inherit":1}',
      'On_Date'         => '{"DataType":"Date"}',
      'At_Hour'         => 'Number',
      'Species'         => 'Number'
   ); 
   protected $relieson = array(
      "PUB_Survey",  
      "PUB_Species",
      "PUB_Species_Record",
      "PUB_Species_First_Record"
   );
   function create() {
      foreach($this->relieson as $lclass) {
         $link = new $lclass();
         if ($link && !$link->exists()) $link->create();
      }
      $select = "SELECT 
            FR.PUB_Survey_ID, 
            COUNT( * ) AS `Species`,
            FR.On_Date AS `On_Date`, 
            FR.At_Hour AS `At_Hour`
         FROM PUB_Species_First_Record AS FR
         GROUP BY FR.PUB_Survey_ID, FR.On_Date, FR.At_Hour"; 
      $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
      $this->dbhandle->exec($statement);
      unset($pending[$this->tablename]);
   }
}
class PUB_Contributor_Species extends DBT_ReportView {
   protected $tablename = 'PUB_Contributor_Species';
   protected $displayname = 'Contributor Species';
   protected $domain = 'public';
   protected $instancedomain = false;
   protected $indexon = 'PUB_Contributor';  
   protected $selectto = 'PUB_Contributor';
   protected $perpage = 5;
   protected $columns = array(
      'PUB_Survey_ID'   => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Mandatory":1,"Current":1,"Hidden":1,"Inherit":1}',
      'PUB_Contributor_ID'    => '{"DataType":"LINKEDTO","TargetType":"PUB_Contributor","TargetField":"PUB_Contributor_ID","Current":1,"Inherit":1}',
      'Species'         => 'Number',
      'Records'         => 'Number'
   ); 
   protected $relieson = array(
      "PUB_Survey",  
      "PUB_Contributor",
      "PUB_Species_Record"
   );
   function create() {
      foreach($this->relieson as $lclass) {
         $link = new $lclass();
         if ($link && !$link->exists()) $link->create();
      }
      $select = "SELECT 
            SR.PUB_Survey_ID, 
            SR.PUB_Contributor_ID, 
            COUNT( DISTINCT PUB_Species_ID ) AS `Species`,
            COUNT( * ) AS `Records`
         FROM PUB_Species_Record AS SR
         GROUP BY SR.PUB_Survey_ID, SR.PUB_Contributor_ID 
         ORDER BY Species DESC"; 
      $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
      $this->dbhandle->exec($statement);
      unset($pending[$this->tablename]);
   }
}
class PUB_Zone_Species extends DBT_ReportView {
   protected $tablename = 'PUB_Zone_Species';
   protected $displayname = 'Zone Species';
   protected $domain = 'public';
   protected $instancedomain = false;
   protected $indexon = 'PUB_Recording_Zone';  
   protected $selectto = 'PUB_Recording_Zone';
   protected $perpage = 5;
   protected $columns = array(
      'PUB_Survey_ID'         => '{"DataType":"LINKEDTO","TargetType":"PUB_Survey","TargetField":"PUB_Survey_ID","Mandatory":1,"Current":1,"Hidden":1,"Inherit":1}',
      'PUB_Recording_Zone_ID' => '{"DataType":"LINKEDTO","TargetType":"PUB_Recording_Zone","TargetField":"PUB_Recording_Zone_ID","Current":1,"Inherit":1}',
      'Species'               => 'Number',
      'Records'               => 'Number'
   ); 
   protected $relieson = array(
      "PUB_Survey",  
      "PUB_Recording_Zone",
      "PUB_Species_Record"
   );
   function create() {
      foreach($this->relieson as $lclass) {
         $link = new $lclass();
         if ($link && !$link->exists()) $link->create();
      }
      $select = "SELECT 
            SR.PUB_Survey_ID, 
            SR.PUB_Recording_Zone_ID,
            COUNT( DISTINCT SR.PUB_Species_ID )  AS `Species`,
            COUNT( * ) AS `Records`
         FROM PUB_Species_Record AS SR
         WHERE SR.PUB_Recording_Zone_ID > 0 
         GROUP BY SR.PUB_Survey_ID, SR.PUB_Recording_Zone_ID
         ORDER BY Species DESC"; 
      $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
      $this->dbhandle->exec($statement);
      unset($pending[$this->tablename]);
   }
}
?>
