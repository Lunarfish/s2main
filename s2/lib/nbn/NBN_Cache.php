<?php
// function points to base of the NBN API
function nbn_getapibaseurl() {
   $url = 'https://data.nbn.org.uk/api/';
   return $url;
}
// function to turn camel case property: taxonVersionKey
// into capitalised column name style: Taxon_Version_Key
// will need equivalent in JS.  
function nbn_colnamefromjsonprop($prop) {
   $prop = preg_replace('/([A-Z])/','_$1',$prop);
   $prop = preg_replace_callback('/^([a-z])/',function($matches){return strtoupper($matches[1]);},$prop);
   return $prop;
}

class NBN_Cache extends DBT {
   protected $tablename = 'NBN_Cache';
   protected $displayname = 'NBN Cache';
   protected $expiry = '6M';
   protected $perpage = 25;
   protected $user;
   protected $alts = array();
   protected $columns = array(
      'NBN_Cache_ID'    => 'Unique Key',
      'Endpoint'        => 'Medium Text',
      'JSON'            => 'Long Text', 
      'Expiry'          => 'Date'
   );
   protected $domain = 'nbn';
   protected $permissions = array();
   protected $primingdata = array();
   function get($endpoint,$exp=null) { 
      $exp = ($exp == null)?$this->expiry:$exp;
      if (!$this->exists()) $this->create();
      $data = $this->select('Endpoint',$endpoint);
      if (isset($data) && count($data) > 0) {
         $id = $data[0]['NBN_Cache_ID'];
         $expiry = $data[0]['Expiry'];
         $valid = $this->valid($expiry);
         if ($valid) $json = $data[0]['JSON'];
         else if ($id) $this->delete($this->getpk(),$id);                               
      } 
      if (!isset($json)) {
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, $endpoint);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         $json = curl_exec($ch);
         
         /*
         // Maybe necessary to remove some characters from the returned JSON
         $json = preg_replace('/\x60/',' - ',$json);
         $json = preg_replace('/\x93/',' - ',$json);
         $json = preg_replace('/\x94/',' - ',$json);
         //*/
         
         $ival = $this->getinterval($this->expiry);
         $data = array(
            'Endpoint'  => $endpoint,
            'JSON'      => $json,
            'Expiry'    => new Zend_Db_Expr("NOW() + $ival")  
         );
         $this->setdata($data);
         $this->insert();
      }
      return $json;      
   }
   function valid($date) {
      // 0 = expired , 1 = valid
      $expiry = preg_replace('/(\d{2})\/(\d{2})\/(\d{4})/','$3-$2-$1',$date);
      $today = date("Y-m-d",gmmktime());
      $valid = ($expiry >= $today);
      return $valid; 
   }   
}
class NBN_TVK_Revision extends DBT {
   protected $tablename = 'NBN_TVK_Revision';
   protected $displayname = 'TVK Revision';
   protected $cascadeto = array('NBN_Record');
   protected $perpage = 25;
   protected $user;
   protected $columns = array(
      'NBN_TVK_Revision_ID'   => 'Unique Key',
      'Old_TVK'               => 'Taxon Version Key',
      'New_TVK'               => 'Taxon Version Key',
      'On_Date'               => 'Date'
   );
   protected $domain = 'nbn';
   protected $permissions = array();
   protected $primingdata = array();  
    
   function addrevision($otvk,$ntvk) {
      if (!$this->exists()) $this->create();
      $idata = $this->select('Old_TVK',$otvk);
      $entered = false;
      foreach($idata as $row => $data) {
         if ($data['New_TVK'] == $ntvk) $entered = true;
         else $this->delete('Old_TVK',$otvk);
      }
      if (!$entered) {
         $data = array(  
            'Old_TVK' => $otvk,
            'New_TVK' => $ntvk,
            'On_Date' => new Zend_Db_Expr('NOW()')
         );              
         $this->setdata($data);
         $this->insert();
      }
      $this->cascadeupdates($otvk,$ntvk);
   }
   function cascadeupdates($otvk,$ntvk) {
      foreach($this->cascadeto as $type) {
         $idbt = new Type();
         $pk = $idbt->getpk();
         $cols = $idbt->getcolumnsbytype('Taxon Version Key');
         foreach ($cols as $col) {
            $idata = $idbt->select($col,$otvk);
            foreach($idata as $row => $data) {
               $id = $data[$pk];
               // some entries may record the history of taxonomic revisions
               // others may want to store only the current value
               // to record the change the class has to implement the addtaxonrevision method
               if (method_exists($idbt,'addtaxonrevision')) 
                  $idbt->addtaxonrevision($id,$otvk,$ntvk,'Taxonomic Revision');
               $idbt->setdata(array($col => $ntv));
               $idbt->update($pk,$id);                                                                     
            }
         }
      }
   }
}

class NBN_Taxa extends DBT {
   protected $tablename = 'NBN_Taxa';
   protected $displayname = 'NBN Taxa';
   protected $perpage = 25;
   protected $user;
   protected $columns = array(
      'NBN_Taxa_ID'              => 'Unique Key',
      'Taxon_Version_Key'        => 'Short Text',
      'Name'                     => 'Short Text',
      'Common_Name'              => 'Short Text',
      'Taxon_Output_Group_Key'   => 'Short Text',
      'Taxon_Output_Group_Name'  => 'Short Text',
      'Organism_Key'             => 'Short Text',// sort order
      'Rank'                     => 'Short Text',//"Class"
      'Name_Status'              => 'Short Text',//"Recommended"
      'Version_Form'             => 'Short Text',//"Well-formed"
      'Preferred_Version_Key'    => 'Short Text' // preferred taxon version key       
   );
   protected $domain = 'species';
   protected $permissions = array();
   protected $primingdata = array();
   function fromjson($json) {
      $data = array();
      $taxa = json_decode($json);
      foreach((array)$taxa as $prop => $val) {
         $icol = nbn_colnamefromjsonprop($prop);
         $icol = ($icol == 'Ptaxon_Version_Key')?'Preferred_Version_Key':$icol;
         if (isset($this->columns[$icol])) $this->data[$icol] = $val;
      }
   } 
   function confirmtvk($itvk) {
      $json = $this->gettaxajson($itvk);
      $this->fromjson($json);
      $taxa = $this->getdata(); 
      if ($taxa['Taxon_Version_Key'] == $taxa['Preferred_Version_Key']) $otvk = $itvk;
      else {
         $otvk = $taxa['Preferred_Version_Key'];
         $nbnr = new NBN_TVK_Revision();
         $nbnr->addrevision($itvk,$otvk);
      }
      return $otvk;
   }
   function gettaxajson($tvk) {
      $turl = nbn_getapibaseurl();
      $url = $turl.'taxa/'.$tvk;
      $nbnc = new NBN_Cache();
      $json = $nbnc->get($url);
      return $json;
   }
   function getpreferredtaxajson($itvk) {
      $otvk = $this->confirmtvk($itvk);
      $json = $this->gettaxajson($otvk);
      return $json;
   }  
}

class NBN_Record extends DBT {
   protected $tablename = 'NBN_Record';
   protected $displayname = 'Species Record';
   protected $perpage = 25;
   protected $user;
   protected $columns = array(
      'NBN_Record_ID'   => 'Unique Key',
      //'NBN_Dataset_ID'  => '{"DataType":"LINKEDTO","TargetType":"NBN_Dataset","TargetField":"NBN_Dataset_ID","Current":1}',
      'Species'         => 'Taxon Version Key',
      'On_Date'         => 'Date',
      'Location'        => '{"DataType":"Map Data","Derives":[{"Column":"Grid_Reference","Method":"s2_getgr","Params":[{"Type":"Value"}]}]}',
      'Grid_Reference'  => '{"DataType":"Short Text","Derives":[{"Column":"Location","Method":"s2_getgrsquare","Params":[{"Type":"Value"}]}]}',
      'Recorder'        => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person"],"Mandatory":1}'
   );
   protected $domain = 'nbn';
   protected $permissions = array();
   protected $primingdata = array();
   protected $taxonrevisiontablename = 'NBN_Record_Taxon_Revision'; 
   function addtaxonrevision($id,$oldtvk,$newtvk,$reason) {
      // add the revision first
      $tr = new $taxonrevisiontablename();
      $data = array(
         $this->tablename => $id,
         'Old_Taxa'  => $oldtvk,
         'New_Taxa'  => $newtvk,
         'On_Date'   => new Zend_Db_Expr('NOW()'),
         'Reason'    => $reason 
      );
      $tr->setdata($data);
      $tr->insert();
      // then update existing value
      $this->setdata(array('Species'=>$newtvk));
      $this->setid($id);
      $this->update($this->getpk(),$id);   
   }  
}
class NBN_Record_Taxon_Revision extends DBT {
   protected $tablename = 'NBN_Record_Taxon_Revision';
   protected $displayname = 'Species Record Taxon Revision';
   protected $perpage = 25;
   protected $user;
   protected $columns = array(
      'NBN_Record_Taxon_Revision_ID'   => 'Unique Key',
      'NBN_Record_ID'   => '{"DataType":"LINKEDTO","TargetType":"NBN_Record","TargetField":"NBN_Record_ID","Mandatory":1,"Current":1}',
      'Old_Taxa'        => 'Taxon Version Key',
      'New_Taxa'        => 'Taxon Version Key',
      'On_Date'         => 'Date',
      'Reason'          => 'Short Text',
      'Recorder'        => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person"]}'
   );
   protected $domain = 'nbn';
   protected $permissions = array();
   protected $primingdata = array();   
}
?>