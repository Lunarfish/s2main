<?php

require_once('soap/nusoap.php');

function querynbntaxa($property,$value) {
   $client = new nusoapclient('http://www.nbnws.net/ws_3_5/GatewayWebService?wsdl', 'wsdl');
   if($client->fault){
       echo "FAULT:  <p>Code: {$client->faultcode} <br/>";
       echo "String: {$client->faultstring} </p>";
   }
   
   $yhedn_ods = '4d5a98202df71b779399b89b80b187edaa5bbb32';
   
   $query1 = "<TaxonomySearchRequest 
      xmlns='http://www.nbnws.net/Taxon/Taxonomy'
      xmlns:tax='http://www.nbnws.net/Taxon' 
      registrationKey='$yhedn_ods'>
   <$property>$value</$property>
   </TaxonomySearchRequest>";
   
   $response = $client->call('GetTaxonomySearch', $query1);
   
   if($client->fault){
      $obj = new stdClass();
      $obj->Status = $client->faultcode;
      $obj->Message = $client->faultstring;
      echo json_encode($obj); 
   }
   $taxonlist = array();
   if ($response['Taxa']['Taxon']) {
      $response = $response['Taxa']['Taxon'];
      $taxonlist = (!isset($response['TaxonName']))?$response:array($response);
      unset($response);
      foreach ($taxonlist as $ti => $taxa) {
         $taxonlist[$ti] = readTaxa($taxa); 
      }
   }
   return $taxonlist;
}
function getnbntaxa($col,$val) {
   global $client, $folder;
   $taxa = null;
   $type = (preg_match('/^[A-Z]{6}[0-9]{10}$/',$val))?'tax:TaxonVersionKey':'SearchTerm';
   $tarray = querynbntaxa($type,$val);
   foreach($tarray as $ti => $taxa) $tarray[$ti] = formatnbntaxa($col,$taxa); 
   switch (count($tarray)) {
   case 0: $taxa = null; break;
   case 1: $taxa = $tarray[0]; break;
   default: $taxa = $tarray; break;
   }            
   return $taxa;
}
function formatnbntaxa($col,$nbntaxa) {
//print "<pre>"; print_r($nbntaxa); print "</pre>"; 
   $obj = new stdClass();
   $obj->Display_Name = preg_replace('/\_/',' ',$col);
   $obj->To_Domain = 'Inline';
   $obj->Data_Type = 'NBNTaxa';
   $obj->Name = $nbntaxa['Name'];
   $obj->Value = $nbntaxa['TaxonVersionKey'];
//print "<pre>"; print_r($obj); print "</pre>"; exit;
   $json = json_encode($obj);
   unset($obj->To_Domain);
   unset($obj->Display_Name);
   $obj->Value_Type = 'Short Text';
   $obj->JSON = $json;
   return $obj;
}
function readTaxa($taxa) {
   $taxa['Name'] = $taxa['TaxonName']['!'];
   unset($taxa['TaxonName']);
   $taxa['Group'] = $taxa['TaxonReportingCategory']['!'];
   unset($taxa['TaxonReportingCategory']);
   $taxa = readSynonymy($taxa);
   $taxa = readInner($taxa);
   ksort($taxa);
   return $taxa;
}
function readSynonymy($taxa) {
   if (isset($taxa['SynonymList'])) {
      $synonyms = (!isset($taxa['SynonymList']['Taxon']['TaxonName']))?$taxa['SynonymList']['Taxon']:array($taxa['SynonymList']['Taxon']);
      $synos = array();
      $names = array();
      foreach ($synonyms as $syno) {
         if ($syno['TaxonName']['!isScientific'] == 'false') {
            if (!($taxa['Name'] == $syno['TaxonName']['!'])&&!in_array($syno['TaxonName']['!'],$names)) $names[] = $syno['TaxonName']['!'];
         } else {
            if (!in_array($syno['TaxonName']['!'],$synos)) $synos[] = $syno['TaxonName']['!']; 
         }
      }
      unset($taxa['SynonymList']);
      if (count($synos)) $taxa['Synonyms'] = $synos;
      if (count($names)) $taxa['Common_Names'] = $names;
   }
   return $taxa;
}
function readInner($taxa) {
   if (isset($taxa['LowerTaxaList'])) {
      $inner = (!isset($taxa['LowerTaxaList']['Taxon']['TaxonName']))?$taxa['LowerTaxaList']['Taxon']:array($taxa['LowerTaxaList']['Taxon']);
      foreach ($inner as $ii => $inneri) $inner[$ii] = readTaxa($inneri);
      $taxa['LowerTaxa'] = $inner;
      unset($taxa['LowerTaxaList']);
   }
   return $taxa;
}
?>
