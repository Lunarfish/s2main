<?php

// rest wrapper to mimic the old soap taxonSearch services 
$nbnproxy = "http://${server}/${folder}/taxa/nbnapi.php";
function nbnrestget($url) {
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
   $res = curl_exec($ch);
   return $res;
}
function getnbnresttaxabytvk($tvk) {
   global $nbnproxy;
   $url = "${nbnproxy}/taxa/${tvk}";
   $res = nbnrestget($url);
   return $res;  
}
function getnbnresttaxabysearchterm($term) {
   global $nbnproxy;
   $url = "${nbnproxy}/taxa/?q=${term}";
   $res = nbnrestget($url);
   return $res;
}
function querynbntaxa($property,$value) {
   if (!isset($property)) $property = (preg_match('/^[A-Z]{6}[0-9]{10}$/',$val))?'tax:TaxonVersionKey':'SearchTerm'; 
   switch($property) {
   case 'tax:TaxonVersionKey': $response = getnbnresttaxabytvk($value); break;
   case 'SearchTerm': $response = getnbnresttaxabysearchterm($value); break;
   }
   $taxonlist = array();
   if ($response['results']) {
      foreach ($response['results'] as $ti => $taxa) {
         $taxonlist[$ti] = readTaxa($taxa); 
      }
   }
   return $taxonlist;
}
function getnbntaxa($col,$val) {
   global $client, $folder;
   $taxa = null;
   $type = (preg_match('/^[A-Z]{6}[0-9]{10}$/',$val))?'tax:TaxonVersionKey':'SearchTerm';
   $tarray = querynbntaxa($type,$val);
   foreach($tarray as $ti => $taxa) $tarray[$ti] = formatnbntaxa($col,$taxa); 
   switch (count($tarray)) {
   case 0: $taxa = null; break;
   case 1: $taxa = $tarray[0]; break;
   default: $taxa = $tarray; break;
   }            
   return $taxa;
}
function formatnbntaxa($col,$nbntaxa) {
//print "<pre>"; print_r($nbntaxa); print "</pre>";
   $nbntaxa = readSynonymy($nbntaxa);
     
   $obj = new stdClass();
   $obj->Display_Name = preg_replace('/\_/',' ',$col);
   $obj->To_Domain = 'Inline';
   $obj->Data_Type = 'NBNTaxa';
   $obj->Name = $nbntaxa['Name'];
   $obj->Common_Name = (isset($nbntaxa['Common_Names']))?urlencode($nbntaxa['Common_Names'][0]):null;
   $obj->Group = $nbntaxa['Group'];
   $obj->Value = $nbntaxa['TaxonVersionKey'];
//print "<pre>"; print_r($obj); print "</pre>"; exit;
   $json = json_encode($obj);
   unset($obj->To_Domain);
   unset($obj->Display_Name);
   unset($obj->Common_Name);
   unset($obj->Group);
   $obj->Value_Type = 'Short Text';
   $obj->JSON = $json;
   return $obj;
}
function readTaxa($taxa) {
   $soaptaxa = stdClass();
   $soaptaxa['Name'] = $taxa['name'];
   $soaptaxa['Group'] = $taxa['taxonOutputGroupName'];
   if (isset($taxa['commonName'])) $soaptaxa['Common_Names'] = array($taxa['commonName']);
    
   $soaptaxa['TaxonVersionKey'] = $taxa['pTaxonVersionKey'];
   if ($taxa['taxonVersionKey'] != $taxa['pTaxonVersionKey']) {
      $ptaxa = querynbntaxa('tax:TaxonVersionKey',$taxa['pTaxonVersionKey'])
      $soaptaxa = readTaxa($ptaxa);
      switch($taxa['languageKey']) {
      case 'en': $soaptaxa['Common_Names']   = array($taxa['name']); break;  
      case 'la': $soaptaxa['Synonyms']       = array($taxa['name']); break; 
      }
   }   
   //$soaptaxa = readSynonymy($taxa);
   //$soaptaxa = readInner($taxa);
   //ksort($taxa);
   return $soaptaxa;
}
function readSynonymy($taxa) {
   if (isset($taxa['SynonymList'])) {
      $synonyms = (!isset($taxa['SynonymList']['Taxon']['TaxonName']))?$taxa['SynonymList']['Taxon']:array($taxa['SynonymList']['Taxon']);
      $synos = array();
      $names = array();
      foreach ($synonyms as $syno) {
         if ($syno['TaxonName']['!isScientific'] == 'false') {
            if (!($taxa['Name'] == $syno['TaxonName']['!'])&&!in_array($syno['TaxonName']['!'],$names)) $names[] = $syno['TaxonName']['!'];
         } else {
            if (!in_array($syno['TaxonName']['!'],$synos)) $synos[] = $syno['TaxonName']['!']; 
         }
      }
      unset($taxa['SynonymList']);
      if (count($synos)) $taxa['Synonyms'] = $synos;
      if (count($names)) $taxa['Common_Names'] = $names;
   }
   return $taxa;
}
function readInner($taxa) {
   if (isset($taxa['LowerTaxaList'])) {
      $inner = (!isset($taxa['LowerTaxaList']['Taxon']['TaxonName']))?$taxa['LowerTaxaList']['Taxon']:array($taxa['LowerTaxaList']['Taxon']);
      foreach ($inner as $ii => $inneri) $inner[$ii] = readTaxa($inneri);
      $taxa['LowerTaxa'] = $inner;
      unset($taxa['LowerTaxaList']);
   }
   return $taxa;
}
?>
