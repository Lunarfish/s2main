-- EXEC sp_addlinkedserver @server='MySQL', @srvproduct='MySQL', @provider='MSDASQL', @datasrc='MySQL'
-- EXEC sp_addlinkedsrvlogin @rmtsrvname='MySQL', @useself='false', @locallogin=NULL, @rmtuser='root', @rmtpassword='ztd7sfzr'
-- EXEC sp_helplinkedsrvlogin @rmtsrvname='MySQL'
-- SELECT * FROM OPENQUERY(MySQL, 'SELECT * FROM TEST')
-- INSERT INTO OPENQUERY(MySQL, 'SELECT Column1,Column2 FROM test WHERE Column1<0') (Column1, Column2) VALUES (NULL, 'blahh')
INSERT INTO OPENQUERY(MySQL, 'SELECT scientificname,tli_key,tv_key,taxonomic_group,species_list_name FROM recorder6designations WHERE tli_key IS NOT NULL') (scientificname, tli_key, tv_key, taxonomic_group, species_list_name) 
SELECT DISTINCT 
itn.ACTUAL_NAME,
ns.RECOMMENDED_TAXON_LIST_ITEM_KEY,
ns.RECOMMENDED_TAXON_VERSION_KEY,
vd.TAXON_GROUP,
vd.LONG_NAME
FROM VW_DESIGNATIONS AS [vd]
INNER JOIN TAXON_LIST_ITEM AS [tli] 
ON vd.TAXON_LIST_ITEM_KEY = tli.TAXON_LIST_ITEM_KEY 
INNER JOIN NAMESERVER AS [ns] 
ON tli.TAXON_VERSION_KEY = ns.INPUT_TAXON_VERSION_KEY 
INNER JOIN INDEX_TAXON_NAME AS [itn] 
ON ns.RECOMMENDED_TAXON_LIST_ITEM_KEY = itn.TAXON_LIST_ITEM_KEY
ORDER BY vd.LONG_NAME,vd.TAXON_GROUP,itn.ACTUAL_NAME
