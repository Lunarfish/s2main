<!--
var wtimeoutid;
/*
<button class='lmtype' id='s2nbnbackbutton' onclick='nbngetprevsearch();'>&larr;</button>
<input type='text' class='text' id='searchfor' onkeypress='nbntaxasearch(this);' style='width:300px;'/>
<div id='speciescontainer'></div>
<input type='hidden' id='nbnsearchhistory'/>
*/
var taxatarget;
function s2_shownbntaxadialog(target,species) {
   var button,input,div,table,tbody,tr,th,td,span,h2;
   taxatarget = target;
   cleardialog('dialog-liner');
   var dialog = document.getElementById('dialog-liner');
   h2 = document.createElement('H2');
   h2.appendChild(document.createTextNode('Species browser'));
   dialog.appendChild(h2);
   span = document.createElement('SPAN');
   span.style.fontWeight = 'bold';
   span.appendChild(document.createTextNode('Search For: '));
   dialog.appendChild(span);
   input = document.createElement('INPUT');
   input.type = 'text';
   input.className = 'text';
   input.style.border = '1px solid #999';
   input.style.padding = '5px';
   input.id = 'searchfor';
   if (species) input.value = species;
   input.onkeypress = function() {nbntaxasearch(this);};
   input.style.width = '250px';
   dialog.appendChild(input);
   button = document.createElement('BUTTON');
   button.innerHTML = '&larr;';
   button.className = 'lmtype';
   button.id = 's2nbnbackbutton';
   button.onclick = function() {nbngetprevsearch();};
   button.name = 'Previous search';
   s2_addtooltip(button);
   dialog.appendChild(button);
   input = document.createElement('INPUT');
   input.type = 'hidden';
   input.id = 'nbnsearchhistory';
   if (species) input.value = species
   dialog.appendChild(input)
   div = document.createElement('DIV');
   div.id = 'speciescontainer';
   dialog.appendChild(div)
   /*   
   var close = document.createElement('BUTTON');
   close.appendChild(document.createTextNode('close'));
   close.onclick = hidedialog;
   dialog.appendChild(close);
   */
   showdialog();
   if (species) nbnsendsearch(species);
}
function nbnusetaxa(elid) {
//alert(elid);
   var args,targs,obj,node,input,a,text,button;
   if (taxatarget) {
      node = taxatarget.parentNode;
      
      args = s2_disectid(elid);
      targs = s2_disectid(taxatarget.id);
      /*
      obj = new Object();
      obj.Display_Name = targs.Property;
      obj.To_Domain = 'Inline';
      obj.Data_Type = 'NBNTaxa';
      obj.Name = args.NBNSpecies;
      obj.Value = args.NBNCode;
      */
      taxatarget = node.lastChild;
      while (node.childNodes.length > 1) node.removeChild(node.firstChild);
      a = document.createElement('A');
      a.appendChild(document.createTextNode(args.NBNSpecies));
      a.href = '#';
      a.id = 'Form-'+targs.Form+'_IType-'+targs.IType+'_Action-View_Property-'+targs.Property; 
      a.onclick = function() {s2_shownbntaxadialog(this,this.firstChild.nodeValue)};       
      node.insertBefore(a,taxatarget);
      button = document.createElement('BUTTON');
      button.className = 's2delbutton';
      button.innerHTML = '&times;';
      button.name = 'Remove '+text;
      button.id = 'Form-'+targs.Form+'_IType-'+targs.IType+'_Property-'+targs.Property+'_Action-Unset_Current-'+args.NBNCode;
      button.onclick = function() {s2_ifaceclear(this);};
      node.appendChild(button);
         
      input = document.createElement('INPUT');
      input.type = 'hidden';
      input.id = 'Form-'+targs.Form+'_IType-'+targs.IType+'_Property-'+targs.Property;
      input.value = args.NBNCode;//JSON.stringify(obj);
      node.insertBefore(input,taxatarget); 

      cleardialog('dialog-liner');
      hidedialog();
   }
//alert(JSON.stringify(args));
}
 
function nbnload() {
   placescreen();
   completed();     
   var el = document.getElementById('searchfor');
   nbntaxasearch(el);
   nbnaddtosearchhistory(el.value); 
}
function nbntaxasearch(searchfor) {
   window.clearTimeout(wtimeoutid);
   if (searchfor.value.length >= 3) {
      wtimeoutid = window.setTimeout(nbnsendsearch,500,searchfor.value);
   } 
}
function nbnsendsearch(searchfor) { 
   window.clearTimeout(wtimeoutid);
   var req,par;
   req = new Object();
   req.target = '../nbntaxondict.php';
   par = new Object();
   par.searchfor = searchfor;
   req.request = par;
   req.responder = nbnshowtaxa;
   snc_send(req);
}
function nbnshowtaxa(response) {
   //alert(response);
   var div = document.getElementById('speciescontainer');
   while (div.childNodes.length > 0) div.removeChild(div.lastChild);
   var taxonlist = JSON.parse(response);
   var ul;
   if (taxonlist.Status) {
      ul = document.createElement('UL');
      for(var i in taxonlist.Matches) {
         li = nbncreatetaxalistitem(taxonlist.Matches[i]);
         ul.appendChild(li);
      }
      div.appendChild(ul);
   } 
   completed();
}
function nbncreatetaxalistitem(taxa) {
   var ul,li,li2,a,button,span,text,b;
   li = document.createElement('LI');
   //li.appendChild(document.createTextNode(taxa.Name));
   a = document.createElement('A');
   a.appendChild(document.createTextNode(taxa.Name));
   a.href = '#';
   a.onclick = function() {nbnusetaxa(this.id);};
   a.id = 'NBNSpecies-'+taxa.Name+'_NBNCode-'+taxa.TaxonVersionKey;
   a.name = 'Use this species';
   s2_addtooltip(a);
   li.appendChild(a);
   
   button = document.createElement('BUTTON');
   button.className = 'lmtype';
   button.innerHTML = '&rarr;';
   button.id = 'NBNSearchBy-'+taxa.Name;
   button.name = 'Search by this';
   button.onclick = function() {nbnsearchby(this.id);};
   s2_addtooltip(button);
   li.appendChild(button);
   /*if (taxa.Authority) {
      span = document.createElement('SPAN');
      b = document.createElement('B');
      b.appendChild(document.createTextNode('Authority: '));
      span.appendChild(b);
      span.appendChild(document.createTextNode(taxa.Authority));
      li.appendChild(document.createElement('BR'));
      li.appendChild(span);
   }*/
   if (taxa.Synonyms) {
      span = document.createElement('SPAN');
      b = document.createElement('B');
      b.appendChild(document.createTextNode('Synonyms: '));
      span.appendChild(b);
      text = taxa.Synonyms.join(', '); 
      span.appendChild(document.createTextNode(text));
      li.appendChild(document.createElement('BR'));
      li.appendChild(span);
   }
   if (taxa.Common_Names) {
      span = document.createElement('SPAN');
      b = document.createElement('B');
      b.appendChild(document.createTextNode('Common Names: '));
      span.appendChild(b);
      text = taxa.Common_Names.join(', '); 
      span.appendChild(document.createTextNode(text));
      li.appendChild(document.createElement('BR'));
      li.appendChild(span);
   }
   
   if (taxa.LowerTaxa) {
      ul = document.createElement('UL');
      for(var i in taxa.LowerTaxa) {
         li2 = nbncreatetaxalistitem(taxa.LowerTaxa[i]);
         ul.appendChild(li2);
      }
      li.appendChild(ul);
   }
   return li;
}
function nbnsearchby(elid) {
   var args = s2_disectid(elid);
   var node = document.getElementById('searchfor');
   node.value = args.NBNSearchBy;
   nbnsendsearch(args.NBNSearchBy);
   nbnaddtosearchhistory(args.NBNSearchBy)
   return true;
}
function nbnaddtosearchhistory(term) {
   if (term && term != '') {
      var histnode = document.getElementById('nbnsearchhistory');
      if (histnode.value && histnode.value !='') hist = JSON.parse(histnode.value);
      else hist = new Array();
      hist.push(term);
      histnode.value = JSON.stringify(hist);
   }   
}
function nbngetprevsearch() {
   var sf = document.getElementById('searchfor'); 
   var current = sf.value;
   var histnode = document.getElementById('nbnsearchhistory');
   var retval;
   if (histnode.value && histnode.value !='') {
      hist = JSON.parse(histnode.value);
      for (var i=1;i<=hist.length;i++) {
         if (hist[i] == current) retval = hist[i-1];  
      }
   }
   if (retval) {
      sf.value = retval;
      nbnsendsearch(retval);
      hist.pop();
      histnode.value = JSON.stringify(hist);
   }    
}
function nbngetnextsearch() {
   var sf = document.getElementById('searchfor'); 
   var current = sf.value;
   var histnode = document.getElementById('nbnsearchhistory');
   var retval;
   if (histnode.value && histnode.value !='') {
      hist = JSON.parse(histnode.value);
      for (var i=0;i<hist.length;i++) {
         if (hist[i] == current) retval = hist[i+1];   
      }
   }
   if (retval) {
      sf.value = retval;
      nbnsendsearch(retval);
   }   
}

function s2_browsefrom() {
   var searchfor = document.getElementById('s2speciessearch').value;
   if (searchfor.length > 3) {
      s2_browsespecies('getbylatinname',searchfor);
   } else {
      s2_browsespecies();
   }
}
function s2_searchspecies(value) {
   window.clearTimeout(wtimeoutid);
   if (value.length >= 3) {
      wtimeoutid = window.setTimeout(s2_browsespecies,500,'getbyname',value);
   }
}
function s2_browsespecies(action,value) {
//alert(action + ' ' + value);
   var req,par;
   req = new Object();
   req.target = './dictionary.php';
   par = new Object();
   par.a = action;
   par.v = value;
   req.request = par;
   req.responder = s2_updatespecieslist;
   snc_send(req);      
}
function s2_updatespecieslist(json) {
   var jobj = JSON.parse(json);
   var list = s2_parsespecies(jobj)
   var node = document.getElementById('s2specieslist');
   while(node.childNodes.length > 0) {
      node.removeChild(node.lastChild);
   }
   node.appendChild(list);
   completed();
}
function s2_parsespecies(item,inner) {
   var citem,ul,li,pli,cul,pul,button,node,text,names,span,i;
   if (typeof item == 'object' && item.TAXON_LIST_ITEM_KEY) {
      names = new Array();
      li = document.createElement('LI');
      li.className = 's2taxondictionary';
      if (item.COMMON_NAME) names.push(document.createTextNode(item.COMMON_NAME));
      if (item.ACTUAL_NAME) {
         span = document.createElement('SPAN');
         span.className = 'latin';
         span.appendChild(document.createTextNode(' ('+item.ACTUAL_NAME+')'));
         names.push(span);
      }
      for (i in names) li.appendChild(names[i]);
      button = document.createElement('BUTTON');
      button.className = 'lmtype';
      button.innerHTML = '&rarr;';
      button.id = item.TAXON_LIST_ITEM_KEY;
      button.onclick = function() {s2_browsespecies('getself',this.id);};
      li.appendChild(button);
      if (item.CHILDREN) {
         cul = s2_parsespecies(item.CHILDREN,true);
         li.appendChild(cul);
      } 
      if (!inner) {
         ul = document.createElement('UL');
         ul.className = 's2taxondictionary';
         ul.appendChild(li);
         if (item.PARENT && item.PARENT != 'NULL') {
            pul = document.createElement('UL');
            pul.className = 's2taxondictionary';
            pli = s2_parsespecies(item.PARENT,true);
            pli.appendChild(ul);                  
            pul.appendChild(pli);
            node = pul;
         } else node = ul;
      } else node = li;
   } else if (typeof item == 'object') {
      ul = document.createElement('UL');
      ul.className = 's2taxondictionary';
      var i;
      for (i in item) {
         citem = item[i];
         li = s2_parsespecies(citem,true);
         ul.appendChild(li); 
      }
      node = ul;           
   } else {
      alert(JSON.stringify(item));
   }
   return node;
}
//-->