<?php
function putverb($url,$data=null,$type=null,$auth=null,$authtype=null) {
   //error_reporting(E_ALL);      
   $curl = curl_init($url);
   curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
   $post = (array)$data;
   foreach ($post as $key => $val)  {
      switch (gettype($val)) {
      case 'object': $post[$key] = json_encode($val);break;
      case 'array': $post[$key] = json_encode($val);break;
      }
   }
   if ($type) {
      curl_setopt($curl, CURLOPT_HTTPHEADER, Array("Content-Type: $type",'Content-Length: '.strlen($data)));
   }
   if ($auth) {
      switch(strtolower($authtype)) {
      case 'basic': {
         $authconst = CURLAUTH_BASIC;
      }break;
      case 'digest': {
         $authconst = CURLAUTH_DIGEST;
      }break;
      }
      curl_setopt($curl, CURLOPT_HTTPAUTH, $authconst);  
      curl_setopt($curl, CURLOPT_USERPWD, $auth);
   }
   curl_setopt($curl, CURLOPT_POST, isset($data));
   if (isset($data)) curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
   curl_setopt($curl, CURLOPT_HEADER, false);
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
   
   
   // Make the call
   $res = curl_exec($curl);
   /*
   $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
   $header = substr($res, 0, $header_size);
   $res = substr($res, $header_size);                     
   //*/
   curl_close($curl);
   return $res;
}
?>