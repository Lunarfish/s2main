function s2_targetdate(idate,interval) {
   var mats,odate,y,m,d,dobj,p,n,t;
   mats = idate.match(/(\d{2})\/(\d{2})\/(\d{4})/);
   d = mats[1]*1;                  
   m = mats[2]*1;
   m -= 1;
   y = mats[3]*1;
   mats = interval.match(/([+,-])(\d+)(\w+)/);
   p = mats[1];
   n = mats[2]*1;
   t = mats[3].toLowerCase();
   if (p != '+') n = -n;
   switch(t) {
   case 'd': {
      d += n;
      dobj = new Date(y,m,d);
   }break;
   case 'wd': {
      dobj = new Date(y,m,d);
      dobj.addBusDays(n);   
   }break;
   case 'w': {
      d += (n*7);
      dobj = new Date(y,m,d);
   }break;
   case 'm': {
      m += n;
      dobj = new Date(y,m,d);
   }break;
   case 'y': {
      y += n;            
      dobj = new Date(y,m,d);
   }break;
   }
   odate = s2_padnum(dobj.getDate(),2)+"/"+s2_padnum(dobj.getMonth()+1,2)+"/"+s2_padnum(dobj.getFullYear(),4);
   return odate;          
}

//alert(s2_targetdate('17/08/2011','+5D'));

function s2_columnisnull(col) {
   return (col == null);
}

function s2_getcompletionstatus(tdate,cdate) {
   var status,tobj,dobj,cobj;
   dobj = new Date();            // Today's Date
   tobj = s2_readdate(tdate);    // Target Date
   if (cdate == null || cdate == "") {
      status = (tobj.getTime() >= dobj.getTime())?'In Progress':'Overdue';
   } else {
      cobj = s2_readdate(cdate); // Completion Date
      status = (cobj.getTime() <= tobj.getTime())?'Completed On Time':'Completed Late';
   }
   return status;
}
function s2_isvalid(sdate,edate) {
   var status,sobj,dobj,eobj;
   dobj = new Date();            // Today's Date
   sobj = s2_readdate(sdate);    // Target Date
   if (edate == null || edate == "") {
      status = (sobj.getTime() >= dobj.getTime())?'Future':'Valid';
   } else {
      eobj = s2_readdate(edate); // Completion Date
      status = (eobj.getTime() < dobj.getTime())?'Expired':'Valid';
   }
   return status;
}

function s2_getclosestresolution(area) {
   // areas are measured against equivalent square side length so the first
   // thing is to square root the area to get the equivalent square size.
   area = Math.sqrt(area);   
   var res = new Array(1,10,100,1000,2000,5000,10000,20000,50000,100000);
   var closest,smaller,bigger;
   var i = 0;
   while((i < res.length) && (res[i] < area)) i++;
   if (i >= res.length -1) closest = res[(res.length-1)];
   else {
      smaller = res[i-1];
      bigger = res[i];
      closest = (Math.abs(smaller - area) > Math.abs(bigger - area))?bigger:smaller;
   }   
   return closest;
}
function s2_getgr(wkt) {
//alert('GetGR: '+ wkt);
   var polygon,p,ddc,gr,ngr,ptext,pname,parea,zoom;
   try {
      polygon = s2_wkt_to_gmappoly(wkt);
      parea = polygon.getArea();
      p = polygon.getBounds().getCenter();
      ddc = new DegreesDecimalCoordinate(p.y,p.x);
      gr = wgs84_to_osgb36(ddc);
      gr.accuracy = s2_getclosestresolution(parea);
      gr.easting = Math.floor(gr.easting/gr.accuracy)*gr.accuracy;
      gr.northing = Math.floor(gr.northing/gr.accuracy)*gr.accuracy;
      ngr = find_gridsquare(gr);
      ptext = ngr.asString();      
   } catch(err) {
      ptext = 'Google Maps not available in offline mode'; 
   }
   return ptext;
}
function s2_getyear(gap,from) {
   if (!gap) gap = 0;
   if (from) {
      from = parseInt(from);
   } else { 
      var d = new Date();
      from = d.getFullYear();
   }
   var y = from + parseInt(gap);
   return y;
}

function s2_getcolumnvalue(type,id,col) {
   var req,res,dec,params,param,i,current,q,rval,jobj;
   if (id) {
      req = new Object();
      req.target = s2_getcurrentpath()+'/S2_query.php';
      params = new Object();
      params.Action = 'ColumnValue';
      params.IType = type;
      params.Current = id;
      params.Column = col;
      params.CurrentSettings = s2_getcurrent();
      req.request = params;
      req.sync = true;
      res = snc_send(req);    S
      res = cdm_decrypt(res);
      completed();      
      jobj = JSON.parse(res);
      rval = jobj.Value;
   }
   return rval;
}

function s2_derive(formel,derives) {
   var elid,args,id,ip,d,ps,p,pc,dcol,val,method,pars,cid,colnode,icolnode,rcol,rid,rtype,rval;
   var cname,update=true;
   if (formel) {
      elid = formel.id;
      args = s2_disectid(elid);
      for(id in derives) {
         val = null;
         d = derives[id];
         dcol = d.Column;
         cname = dcol.replace(/\_/,' ');
         args.Property = d.Column;
         cid = s2_buildid(args);
         colnode = document.getElementById(cid);
         if (colnode.value) {
            update = confirm('Do you want to replace existing '+cname+' value?');
         } 
         if (colnode && update) {   
            method = d.Method;
            ps = d.Params;
            pc = ps.length;
            pars = new Array();
            for(ip in ps) {
               p = ps[ip];
               switch(p.Type) {
               case 'Value': pars.push(formel.value);break;
               case 'Static': pars.push(p.Value);break;
               case 'Column': {
                  args.Property = p.Value;
                  cid = s2_buildid(args); 
                  icolnode = document.getElementById(cid);
                  if (colnode) {
                     if (p.IType) {
                        rval = s2_getcolumnvalue(p.IType,icolnode.value,p.Property);  
                        pars.push(rval);            
                     } else {
                        pars.push(icolnode.value);
                     }
                  } 
               }break;
               }                              
            }
            if (pars.length == pc) {
               var expr = "val = "+method+"('"+pars.join("','")+"')"; 
               eval(expr);
            }
            if (val) {
//alert(JSON.stringify(val));
               switch(typeof val) {
               case 'object': {
                  var namenode = document.getElementById(colnode.id + '_Name');
                  if (namenode) namenode.value = val.Name;
                  colnode.value = val.Value;
               }break;
               default: colnode.value = val; break;
               }
               if (colnode.onchange) colnode.onchange();
            }
         }                     
      }
   }        
}

function s2_getcolumnvalue(type,id,col) {
   var req,res,dec,params,param,i,current,q;
   if (id) {
      req = new Object();
      req.target = s2_getcurrentpath()+'/S2_query.php';
      params = new Object();
      params.Action = 'ColumnValue';
      params.IType = type;
      params.Current = id;
      params.Column = col;
      params.CurrentSettings = s2_getcurrent();
      req.request = params;
      req.sync = true;
      res = snc_send(req);
      res = cdm_decrypt(res);
      completed();      
      var jobj = JSON.parse(res);
      return jobj.Value;
   } else return null;
}
function s2_getdatecolumnvalue(type,id,col) {
   if (id) {
      var ymd = s2_getcolumnvalue(type,id,col);
      var dmy = ymd.replace(/(\d{4})\-(\d{2})\-(\d{2})/,'$3/$2/$1');
      return dmy;
   } else return null; 
}

function s2_buffergeom(wkt,rad,proj) {
   var poly,ptxt,wktout,jobj=null,pobj=null;
   if (wkt) {
      req = new Object();
      req.target = s2_getcurrentpath()+'/S2_query.php';
      params = new Object();
      params.Action = 'Calculate';
      params.Calculation = 'Buffer';
      params.Polygon = wkt;
      params.Buffer = rad;
      params.Native = proj;
      params.CurrentSettings = s2_getcurrent();
      req.request = params;
      req.sync = true;
      res = snc_send(req);
      res = cdm_decrypt(res);
      completed();      
      jobj = JSON.parse(res);
      if (jobj.Result) {
         wktout = jobj.Result;
         poly = s2_wkt_to_gmappoly(wktout);
         ptxt = s2_poly_getname(poly);
         pobj = new Object();
         pobj.Name = ptxt;
         pobj.Value = wktout;
      }
   }
   return pobj;
}
function s2_nextsitecode(type,col,zone) {
   var sitecode;
   if (zone) {
      req = new Object();
      req.target = s2_getcurrentpath()+'/S2_query.php';
      params = new Object();
      params.Action = 'Calculate';
      params.Calculation = 'Site_Code';
      params.IType = type;
      params.Column = col;
      params.Zone = zone;
      params.CurrentSettings = s2_getcurrent();
      req.request = params;
      req.sync = true;
      res = snc_send(req);
      res = cdm_decrypt(res);
      completed();      
      jobj = JSON.parse(res);
      if (jobj.Result) sitecode = jobj.Result;
   }
   return sitecode;   
}