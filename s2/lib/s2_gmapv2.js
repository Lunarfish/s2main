var map;
var mapbutton;
var infoWindow;
var overlays = new Array();
var mapdiv = 'gmap2';
var tool = 'recentre';
var points = new Array();
var vertices = 36;

function s2_fittomap() {
   var polygon;
   if (map && overlays['boundary']) {
      polygon = overlays['boundary'];
      map.checkResize();
      var bnd = polygon.getBounds();
      var ctr = bnd.getCenter();
      map.setCenter(ctr);
      map.setZoom(map.getBoundsZoomLevel(bnd));  
   }
}


function s2_eldims(el,dim) {
   var r,x,y;
   x = (el.cssWidth)?el.cssWidth:(el.style && parseInt(el.style.width)>0)?parseInt(el.style.width):(el.offsetWidth>0)?el.offsetWidth:el.clientWidth;   
   y = (el.cssHeight)?el.cssHeight:(el.style && parseInt(el.style.height)>0)?parseInt(el.style.height):(el.offsetHeight>0)?el.offsetHeight:el.clientHeight;
   switch(dim) {
   case 'width':  r = x; break;
   case 'height': r = y; break;
   default: r = new Array(x,y); break; 
   }
   return r;   
}

function s2_drawmap(el) {
   mapbutton = el.replace(/^[^_]+_/,'');
   var wktnode = document.getElementById(mapbutton);
   var polygon;
   if (wktnode && wktnode.value) polygon = s2_wkt_to_gmappoly(wktnode.value);
   points = polygon.getPath();
   cleardialog('dialog-liner');
   var dialog = document.getElementById('dialog-liner');
   var mapcont = document.createElement('DIV');
   mapcont.id = 'mapcontainer';
   
   dialog.appendChild(mapcont)
   showdialog();
   s2_creategmap('mapcontainer');
}  

function s2_creategmap(parent) {
   var pnode = document.getElementById(parent);
   var d = pnode.parentNode;
   var tools,map,div,toolw,mapw,button;
   pnode.className = 'mapcontainer';
   var w = s2_eldims(pnode,'width');
   toolw = 180;
   mapw = w - toolw - 30;
   
   tools = s2_getmaptoolstable();
   tools.style.width = toolw+'px';
   div = document.createElement('DIV');
   div.style.width = mapw+'px';
   div.className = 'mapdiv';
   div.id = mapdiv;
   pnode.appendChild(tools);
   pnode.appendChild(div);
   
   s2_initgmap();
   s2_setmaptool('recentre');
   s2_showlocation();

   d.appendChild(document.createElement('HR'));
   button = document.createElement('BUTTON'); 
   button.className = 'dbutton';
   button.onclick = function() {hidedialog();};
   button.appendChild(document.createTextNode('Cancel'));
   d.appendChild(button);             

   button = document.createElement('BUTTON');
   button.className = 'dbutton';
   button.onclick = function() {s2_assignmap();};
   button.appendChild(document.createTextNode('OK'));
   d.appendChild(button);
   d.appendChild(document.createElement('HR'));             
}
function s2_setmaptool(t) {
   switch(t) {
   case 'recentre': break;
   default: {
      var b = overlays['boundary'];
      if (b) {
         map.removeOverlay(b);
         delete overlays['boundary'];
      }
   }break;
   } 
   points = new Array();
   var x = document.getElementById(tool);
   x.className = 'tool';
   tool = t;
   x = document.getElementById(tool);
   x.className = 'toolx';
   s2_showtooloptions(t);
   s2_showlocation();
   document.getElementById('s2gmapcheck').checked = false;
   return true;
}
function s2_showtooloptions(t) {
   var o = document.getElementById('tooloptions');
   var mt = document.getElementById('maptools');
   var div,h4,input,table,tbody,tr,th,td,img;
   var div = document.createElement('DIV');
   div.id = 'tooloptions';
   h4 = document.createElement('H4');
   h4.appendChild(document.createTextNode('Options'));
   div.appendChild(h4);
   table = document.createElement('TABLE');
   tbody = document.createElement('TBODY');
   
   switch(t) {
   case 'circle': {
      tr = document.createElement('TR');
      td = document.createElement('TD');
      td.appendChild(document.createTextNode('Radius (m)'));
      tr.appendChild(td);
      td = document.createElement('TD');
      input = document.createElement('INPUT');
      input.id = 'circle_radius';
      input.type = 'text';
      input.value = 1000;
      td.appendChild(input);
      tr.appendChild(td);
      tbody.appendChild(tr);
   }break;
   case 'square': {
      tr = document.createElement('TR');
      td = document.createElement('TD');
      td.appendChild(document.createTextNode('Side (m)'));
      tr.appendChild(td);
      td = document.createElement('TD');
      input = document.createElement('INPUT');
      input.id = 'square_side';
      input.type = 'text';
      input.value = 1000;
      td.appendChild(input);
      tr.appendChild(td);
      tbody.appendChild(tr);
   }break;
   }

   tr = document.createElement('TR');
   td = document.createElement('TD');
   td.appendChild(document.createTextNode('NGR'));
   tr.appendChild(td);
   td = document.createElement('TD');
   input = document.createElement('INPUT');
   input.id = 'ngr';
   input.type = 'text';
   input.onchange = function() {s2_movetongr(this.value);};
   td.appendChild(input);
   tr.appendChild(td);
   tbody.appendChild(tr);

   tr = document.createElement('TR');
   td = document.createElement('TD');
   td.appendChild(document.createTextNode('LL'));
   tr.appendChild(td);
   td = document.createElement('TD');
   input = document.createElement('INPUT');
   input.id = 'latlng';
   input.type = 'text';
   input.onchange = function() {s2_movetoll(this.value);};
   td.appendChild(input);
   tr.appendChild(td);
   tbody.appendChild(tr);

   table.appendChild(tbody);
   div.appendChild(table);
   if (o) mt.replaceChild(div,o);
   else mt.appendChild(div);
}
function s2_getmaptoolstable() {
   var div,h4,table,tbody,div;
   div = document.createElement('DIV');
   div.className = 'maptools';
   div.id = 'maptools';
   h4 = document.createElement('H4');
   h4.appendChild(document.createTextNode('Tools'));
   div.appendChild(h4);
   table = document.createElement('TABLE');
   tbody = document.createElement('TBODY');
   tbody.appendChild(s2_getrecentre_tooltr());
   tbody.appendChild(s2_getcircle_tooltr());
   tbody.appendChild(s2_getgridsquare_tooltr());
   tbody.appendChild(s2_getpolygon_tooltr());
   tbody.appendChild(s2_geteditvertices_tooltr());
   table.appendChild(tbody);
   div.appendChild(table);
   return div; 
}
function s2_getrecentre_tooltr() {
   var tr,th,node,text,input,img,button;
   tr = document.createElement('TR');
   td = document.createElement('TD');
   td.className = 's2gmaptdbutton';
   button = document.createElement('BUTTON');
   button.className = 'toolx';
   img = document.createElement('IMG');
   img.src = '../images/recentre.gif';
   img.alt = 'Re-centre';
   button.appendChild(img);
   button.id = 'recentre'; 
   button.onclick = function() {s2_setmaptool(this.id);};
   td.appendChild(button);
   tr.appendChild(td);
   td = document.createElement('TD');
   td.appendChild(document.createTextNode('Recentre'));
   tr.appendChild(td);
   return tr;
}
function s2_getcircle_tooltr() {
   var tr,th,node,text,input,img,button;
   tr = document.createElement('TR');
   td = document.createElement('TD');
   td.className = 's2gmaptdbutton';
   button = document.createElement('BUTTON');
   button.className = 'tool';
   img = document.createElement('IMG');
   img.src = '../images/circle.gif';
   img.alt = 'Point and radius';
   button.appendChild(img);
   button.id = 'circle'; 
   button.onclick = function() {s2_setmaptool(this.id);};
   td.appendChild(button);
   tr.appendChild(td);
   td = document.createElement('TD');
   td.appendChild(document.createTextNode('Radius'));
   tr.appendChild(td);
   return tr;
}
function s2_getgridsquare_tooltr() {
   var tr,th,node,text,input,img,button;
   tr = document.createElement('TR');
   td = document.createElement('TD');
   td.className = 's2gmaptdbutton';
   button = document.createElement('BUTTON');
   button.className = 'tool';
   img = document.createElement('IMG');
   img.src = '../images/square.gif';
   img.alt = 'Grid Square';
   button.appendChild(img);
   button.id = 'square'; 
   button.onclick = function() {s2_setmaptool(this.id);};
   td.appendChild(button);
   tr.appendChild(td);
   td = document.createElement('TD');
   td.appendChild(document.createTextNode('Gridsquare'));
   tr.appendChild(td);
   return tr;
}
function s2_getpolygon_tooltr() {
   var tr,th,node,text,input,img,button;
   tr = document.createElement('TR');
   td = document.createElement('TD');
   td.className = 's2gmaptdbutton';
   button = document.createElement('BUTTON');
   button.className = 'tool';
   img = document.createElement('IMG');
   img.src = '../images/polygon.gif';
   img.alt = 'Polygon';
   button.appendChild(img);
   button.id = 'polygon'; 
   button.onclick = function() {s2_setmaptool(this.id);};
   td.appendChild(button);
   tr.appendChild(td);
   td = document.createElement('TD');
   td.appendChild(document.createTextNode('Polygon'));
   tr.appendChild(td);
   return tr;
}
function s2_geteditvertices_tooltr() {
   var tr,th,node,text,input;
   tr = document.createElement('TR');
   td = document.createElement('TD');
   td.className = 's2gmaptdbutton';
   input = document.createElement('INPUT');
   input.type = 'checkbox';
   input.id = 's2gmapcheck';
   input.className = 's2gmapcheck';
   input.onclick = function() {s2_editpoly(this.checked);};
   
   td.appendChild(input);
   tr.appendChild(td);
   td = document.createElement('TD');
   td.appendChild(document.createTextNode('Edit Poly'));
   tr.appendChild(td);
   return tr;
}

function s2_moveto(point) {
   map.setCenter(point);
   return true;
}
function s2_movetopolycenter() {
   var c = s2_getpolycenter();
   s2_moveto(c);
}
function s2_movetongr(gridref) {
   var gr,ll,ctr;
   if (gridref) {
      gr = s2_gr_from_ngrstring(gridref);
      ll = s2_gr_to_ll(gr);
      ctr = s2_ll_to_gmp(ll);
      s2_moveto(ctr);
   }
}
function s2_gr_from_ngrstring(gridref) {
   var bits,sq,en,e,n,sx,res,ngr,gr,ll,ctr;
   if (gridref) {
      gridref = gridref.toUpperCase();
      gridref = gridref.replace(/(\w{2})(\d+)(\w*)/,'$1-$2-$3');
      bits = gridref.split(/\-/);
      sq = bits[0];
      en = bits[1];
      e = parseInt(en.substr(0,Math.floor(en.length/2)));
      n = parseInt(en.substr(Math.floor(en.length/2),Math.floor(en.length/2)));
      sx = bits[2];
      res = Math.pow(10,(5 - Math.floor(en.length/2)));
      e *= res;
      n *= res;
      switch(sx.length) {
      case 1: {
         // DINTY tetrad notation 2K
         var tn,tx,ty;
         tn = sx.charCodeAt(0) - 65;
         tn = (tn > 14)?(tn-1):tn;
         ty = (tn%5) * 2 * (res/10);
         tx = Math.floor(tn/5) * 2 * (res/10);
         res /= 5;
         e += tx;
         n += ty;
      }break;
      case 2: {
         // NE,SE,SW,NW notation 5K
         var tx = (sx.substring(1,1) == 'E')?5:0;
         tx *= (res/10);
         var ty = (sx.substring(0,1) == 'N')?5:0;
         ty *= (res/10);
         res /= 2;
         e += tx;
         n += ty;
      }break;
      }
      e += (res/2);
      n += (res/2);
//alert(e + ' ' + n + ' ' + res);
      ngr = new NationalGridReference(sq,e,n,res);
      gr = s2_ngr_to_gr(ngr);      
   }
   return gr; 
}
   
function s2_movetoll(llstring) {
   var bits,lat,lng,ctr;
   if (llstring && llstring.match(/\-*\d+\s*\,\s*\-*\d+/)) {
      bits = llstring.split(/\s*\,\s*/);
      lng = parseInt(bits[0]);
      lat = parseInt(bits[1]);
      ctr = new GLatLng(lng,lat);
      s2_moveto(ctr);                                                  
   }
}

function s2_getpolycenter(pts) {
   var px = (pts)?pts:points;
   var p,c,cx,cy,maxx,maxy,minx,miny; 
   for (p in px) {
      c = px[p];
      maxx = (maxx)?Math.max(maxx,c.lng()):c.lng();
      minx = (minx)?Math.min(minx,c.lng()):c.lng();
      maxy = (maxy)?Math.max(maxy,c.lat()):c.lat();
      miny = (miny)?Math.min(miny,c.lat()):c.lat();
   }
   cx = minx + ((maxx-minx)/2);
   cy = miny + ((maxy-miny)/2);
   c = new GLatLng(cy,cx);
   return c; 
}
function s2_initgmap() {
   var md = document.getElementById(mapdiv); 
   try {
      var myLatLng = new GLatLng(53.38, -1.46);
      var z = 8;
      map = new GMap2(md);
      map.setCenter(myLatLng,8);
      map.addControl(new GSmallZoomControl3D());
      map.enableGoogleBar();
      map.enableContinuousZoom();
      map.getMapTypes().length = 0;
      map.addMapType(G_NORMAL_MAP);
      map.addMapType(G_HYBRID_MAP);
      map.addMapType(G_PHYSICAL_MAP);
      map.setMapType(G_NORMAL_MAP);
      map.addControl(new GMenuMapTypeControl());
      map.checkResize();
      var dims = s2_eldims(md);
      var gdim = new GSize(dims[0],dims[1]);
      map.size = gdim;
      GEvent.addListener(map, 'click', function(o,pt) {s2_clickmap(o,pt);});
      if (points.length > 1) {
         s2_drawpoly();
         s2_fittomap();
      }
      s2_showzoom();
   } catch (e) {
      md.appendChild(document.createTextNode('Google Maps: Initialise failed.'));
   } 
}
function s2_clickmap(o,pt) {
   var rad;
   if (pt) {
      switch(tool) {
      case 'recentre': {
         s2_moveto(pt);
         s2_showlocation();
      }break;
      case 'circle': {
         rad = parseInt(document.getElementById('circle_radius').value);
         s2_drawcircle(pt,rad);
      }break;
      case 'square': {
         rad = parseInt(document.getElementById('square_side').value);
         s2_drawgridsquare(pt,rad);
      }break;
      case 'polygon': {
         points.push(pt);
         s2_drawpoly(points);
      }break;
      }         
   }
   return true;
}  
function s2_showlocation(ctr) {
   var ll,gr,ngr,res,z,node;
   if (!ctr) ctr = map.getCenter();
   ll = s2_gmp_to_ll(ctr);
   gr = s2_ll_to_gr(ll);
   switch(tool) {
   case 'circle': {
      res = parseInt(document.getElementById('circle_radius').value);
      res *= 2;
   }break;
   case 'square': {
      res = parseInt(document.getElementById('square_side').value);
   }break;
   default: {
      z = map.getZoom();
      res = (z >= 12)?100:(z>=8)?1000:(z>=5)?10000:100000;
   }break;
   }
   ngr = s2_gr_to_ngr(gr);
   ngr.accuracy = res;
   ll.accuracy = res;
   
   node = document.getElementById('ngr');
   if (node) node.value = ngr.asString();
   node = document.getElementById('latlng');
   if (node) node.value = ll.asString();
}

function s2_drawpoly(pts) {
   if (!pts) pts = points;
   var p,c,vc,v1,vn;
   if (overlays['boundary']) {
      p = overlays['boundary'];
      map.removeOverlay(p);
   } 
   p = new GPolygon(pts, "#FF0000", 0.8, 0.5, "#FF0000", 0.35,{"clickable":true});
   GEvent.addListener(p, 'click', function(pt) {s2_clickmap(this,pt);});
   map.addOverlay(p);
   overlays['boundary'] = p;
}
function s2_editpoly(makeeditable) {
   var p,vc,v1,vn;
   if (overlays['boundary']) {
      p = overlays['boundary'];
      if (makeeditable) {
         vc = p.getVertexCount();
         v1 = p.getVertex(0);
         vn = p.getVertex(vc-1);
         if (v1 != vn) p.insertVertex(vc,v1); 
         p.enableEditing();
      }
      else p.disableEditing();
   }
}
function s2_drawcircle(ctr,rad) {
   var c;
   if (overlays['boundary']) {
      c = overlays['boundary'];
      map.removeOverlay(c);
   }
   points = s2_getcirclevertices(ctr,rad);
   c = new GPolygon(points, "#FF0000", 0.8, 0.5, "#FF0000", 0.35,{"clickable":true});
   c.click = function(pt) {s2_clickmap(this,pt);};
   map.addOverlay(c);
   
   overlays['boundary'] = c;
   s2_showlocation(ctr);
   s2_fittomap();
}
function s2_drawgridsquare(pnt,res) {
   var p,ll,gr,ngr;
   if (overlays['boundary']) {
      p = overlays['boundary'];
      map.removeOverlay(p);
   }
   ll = s2_gmp_to_ll(pnt);
   gr = s2_ll_to_gr(ll);
   ngr = s2_gr_to_ngr(gr);
   ngr.easting = ngr.easting - (ngr.easting % res) + (0.5 * res);
   ngr.northing = ngr.northing - (ngr.northing % res) + (0.5 * res);
   ngr.accuracy = res;
   points = s2_getgridsquarefromgr(gr,res);
   p = new GPolygon(points, "#FF0000", 0.8, 0.5, "#FF0000", 0.35,{"clickable":true});
   p.click = function(pt) {s2_clickmap(this,pt);};
   map.addOverlay(p);
   overlays['boundary'] = p;
   s2_showlocation(pnt);
   s2_fittomap();
}

var ztime;
function s2_showzoom() {
   GEvent.addListener(map, 'zoom_changed', function() {
      // Get the current bounds, which reflect the bounds before the zoom.
      var zrect = map.getBounds;
      map.addOverlay(zrect);
      overlays['zoom'] = zrect;
      ztime = window.setTimeout("s2_hidezoom();",1000);   
   });
}
function s2_hidezoom() {
   var zrect = overlays['zoom'];
   if (zrect) { 
      map.removeOverlay(zrect);
      zrect = null;
      ztime = null;
   }
   delete overlays['zoom'];
}
                                  
function s2_drawpointmarker(point,title,info) {
   var infowindow = new google.maps.InfoWindow({
      content: info
   });

   var marker = new google.maps.Marker({
      position: point,
      title: title
   });
   
   google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
   });
   map.addOverlay(marker);
   overlays.push(marker);
}

function s2_showoverlays() {
   if (overlays) {
      for (i in overlays) {
         //overlays[i].setMap(map);
         map.addOverlay(overlays[i]);
      }
   }
}

// Deletes all markers in the array by removing references to them
function s2_deleteoverlays() {
   if (overlays) {
      for (i in overlays) {
         //overlays[i].setMap(null);
         map.removeOverlay(overlays[i]);
      }
      overlays.length = 0;
   }
}

function s2_gmp_to_ll(gmp) {
   var ddc = new DegreesDecimalCoordinate(gmp.lat(),gmp.lng());
   return ddc;
}
function s2_ll_to_gr(ll) {
   var gr = wgs84_to_osgb36(ll);
   return gr;
}
function s2_gr_to_ngr(gr) {
   var ngr = find_gridsquare(gr);
   return ngr;
}
function s2_ngr_to_gr(ngr) {
   var gr = conv_ngr_to_ings(ngr);
   return gr; 
}
function s2_gr_to_ll(gr) {
   var ddc = conv_uk_ings_to_ll(gr);
   return ddc;
}
function s2_ll_to_gmp(ll) {
   var gmp = new GLatLng(ll.lat(),ll.lng());
   return gmp;
}

function s2_getgridsquarefromgr(gr,acc) {
   var x,y,xi,yi,ll;
   x = gr.easting - (gr.easting % acc);
   y = gr.northing - (gr.northing % acc);
   var points = new Array();   
   for (xi=0;xi<=1;xi++) {
      for (yi=0;yi<=1;yi++) {
         gr = new GridReference((x + (xi*acc)),y + (((xi+yi)%2)*acc));
         gr.accuracy = acc;
         ll = s2_gr_to_ll(gr);                  
         points.push(s2_ll_to_gmp(ll));                  
      }
   }
//alert(JSON.stringify(points));
   return points;            
}
function s2_getgridsquarecenter(gr,acc) {
   var x,y,xi,yi,ll;
   x = gr.easting - (gr.easting % acc);
   y = gr.northing - (gr.northing % acc);
   gr = new GridReference((x + (0.5*acc)),(y + (0.5*acc)));
   gr.accuracy = acc;
   ll = s2_gr_to_ll(gr);                  
   return s2_ll_to_gmp(ll);            
}
function s2_getcirclevertices(ctr,rad) {
   var seg,ll,gr,ngr,points,vertex;
   seg = Math.floor(360/vertices) * (Math.PI/180);
   points = new Array();
   ll = s2_gmp_to_ll(ctr);
   cgr = s2_ll_to_gr(ll);
   gr = new GridReference();
   for (vertex=0;vertex<=vertices;vertex++) {
      gr.easting = cgr.easting + (rad * Math.sin(seg*vertex));
      gr.northing = cgr.northing + (rad * Math.cos(seg*vertex));
      ll = s2_gr_to_ll(gr);
      points[vertex] = s2_ll_to_gmp(ll);
   }
   return points;      
} 

function s2_wkt_to_gmappoly(wkt) {
   var pairs,p,pt,xy,ll,polygon;
   polygon = null;
   try {
      ll = new GLatLng(0,0);  
      if (wkt) {
         points = new Array();
         wkt = wkt.replace(/^[^\(]+\(+/,'');
         wkt = wkt.replace(/\)+$/,'');
         pairs = wkt.split(/\,/);
         for(p in pairs) {
            pt = pairs[p];
            xy = pt.split(/\s+/);
            ll = new GLatLng(xy[1],xy[0]);
            points.push(ll);
         }
         //points.push(points[0]);
         polygon = new GPolygon(points, "#FF0000", 0.8, 0.5, "#FF0000", 0.35,{"clickable":true});
      }
   } catch(err) {
      // add something here.   
   }
   return polygon;
}
function s2_poly_getname(polygon) {
   var wkt,p,ddc,gr,ngr,ptext,pname,parea,zoom;
   try {
      p = polygon.getBounds().getCenter();
      //p = s2_getpolycenter(polygon);
      ddc = new DegreesDecimalCoordinate(p.y,p.x);
      gr = s2_ll_to_gr(ddc);
      gr.accuracy = 10;
      gr.easting = Math.floor(gr.easting/gr.accuracy)*gr.accuracy;
      gr.northing = Math.floor(gr.northing/gr.accuracy)*gr.accuracy;
      ngr = s2_gr_to_ngr(gr);
      parea = polygon.getArea();
      if (parea > 900000) ptext = (Math.round(parea/100000)/10)+' km� @ '+ngr.asString();
      else if (parea > 9000) ptext = (Math.round(parea/1000)/10)+' ha @ '+ngr.asString();  
      else ptext = (Math.round(parea*10)/10)+' m� @ '+ngr.asString();
   } catch(err) {
      if (polygon) ptext = 'Google Maps not available in offline mode'; 
   }
   return ptext;
}
function s2_getpolyvertices(p) {
   var vc,vi,pts;
   pts = new Array();
   vc = p.getVertexCount();
   for(vi=0;vi<vc;vi++) {
      pts.push(p.getVertex(vi));
   }
   return pts;
}
function s2_assignmap() {
   var wkt,p,ddc,gr,ngr,ptext,pname,parea,zoom,polygon;
   var mappoly = document.getElementById(mapbutton);        
   var mapname = document.getElementById(mapbutton+'_Name');
   if (!mapname) mapname = document.getElementById('Link_'+mapbutton); 
   zoom = map.getZoom();
   if (points.length == 0 && overlays['boundary']) points = s2_getpolyvertices(overlays['boundary']);
   
   if (points && points.length > 1) {
      wkt = 'POLYGON((';
      for (pcount=0;pcount<points.length;pcount++) {
         p = points[pcount];
         wkt += p.lng() + ' ' + p.lat() + ',';
      }
      p = points[0];
      wkt += p.lng() + ' ' + p.lat() + '))';
      polygon = new GPolygon(points, "#FF0000", 0.8, 0.5, "#FF0000", 0.35);
      ptext = s2_poly_getname(polygon);
   } else if (points && points.length > 0) {
      p = points[0];
      ddc = s2_gmp_to_ll(p);
      gr = s2_ll_to_gr(ddc);
      gr.accuracy = (zoom >= 12)?100:(zoom>=8)?1000:(zoom>=5)?10000:100000;
      gr.easting = Math.floor(gr.easting/gr.accuracy)*gr.accuracy;
      gr.northing = Math.floor(gr.northing/gr.accuracy)*gr.accuracy;
      ngr = s2_gr_to_ngr(gr);
      wkt = 'POINT('+ p.lng() + ' ' + p.lat() + ')';
      ptext = ngr.asString(); 
   }
   mappoly.value = wkt;
   // execute onchange if the function is defined. 
   // no catch action because it just means the onchange function is not defined
   try {mappoly.onchange();} catch (e) {}
   if (mapname) {
      if (mapname.nodeName == 'INPUT') mapname.value = ptext;
      else mapname.replaceChild(document.createTextNode(ptext),mapname.firstChild);
   }
   //points = new Array();
   hidedialog();
   return true;
}         
