var mapbutton;
function s2_wkt_to_gmappoly(wkt) {
   var pairs,p,pt,xy,ll,polygon;
   polygon = null;
   try {
      ll = new GPoint(0,0);  
      if (wkt) {
         points = new Array();
         wkt = wkt.replace(/^[^\(]+\(+/,'');
         wkt = wkt.replace(/\)+$/,'');
         pairs = wkt.split(/\,/);
         for(p in pairs) {
            pt = pairs[p];
            xy = pt.split(/\s+/);
            ll = new GPoint(xy[0],xy[1]);
            points.push(ll);
         }
         polygon = new GPolygon(points,'#330000',1,1,'#ff0000',0.5);
      }
   } catch(err) {
      // add something here.   
   }
   return polygon;
}
function s2_poly_getname(polygon) {
   var wkt,p,ddc,gr,ngr,ptext,pname,parea,zoom;
   try {
      p = polygon.getBounds().getCenter();
      ddc = new DegreesDecimalCoordinate(p.y,p.x);
      gr = wgs84_to_osgb36(ddc);
      gr.accuracy = 10;
      gr.easting = Math.floor(gr.easting/gr.accuracy)*gr.accuracy;
      gr.northing = Math.floor(gr.northing/gr.accuracy)*gr.accuracy;
      ngr = find_gridsquare(gr);
      parea = polygon.getArea();
      if (parea > 900000) ptext = Math.round(polygon.getArea()/100000)/10+' km� @ '+ngr.asString();
      else if (parea > 9000) ptext = Math.round(polygon.getArea()/1000)/10+' ha @ '+ngr.asString();  
      else ptext = Math.round(polygon.getArea()*10)/10+' m� @ '+ngr.asString();
   } catch(err) {
      ptext = 'Google Maps not available in offline mode'; 
   }
   return ptext;
}
function s2_assignmap() {
   var wkt,p,ddc,gr,ngr,ptext,pname,parea,zoom;
   var mappoly = document.getElementById(mapbutton);        
   var mapname = document.getElementById(mapbutton+'_Name');
   if (!mapname) mapname = document.getElementById('Link_'+mapbutton); 
   zoom = map.getZoom();
   if (points && points.length > 1) {
      wkt = 'POLYGON((';
      for (pcount=0;pcount<points.length;pcount++) {
         p = points[pcount];
         wkt += p.x + ' ' + p.y + ',';
      }
      p = points[0];
      wkt += p.x + ' ' + p.y + '))';
      var polypoints = points.concat(new Array(points[0]));
      polygon = new GPolygon(polypoints,'#330000',1,1,'#ff0000',0.5);
      ptext = s2_poly_getname(polygon);
   } else if (points && points.length > 0) {
      p = points[0];
      ddc = new DegreesDecimalCoordinate(p.y,p.x);
      gr = wgs84_to_osgb36(ddc);
      gr.accuracy = (zoom >= 12)?100:(zoom>=8)?1000:(zoom>=5)?10000:0;
      gr.easting = Math.floor(gr.easting/gr.accuracy)*gr.accuracy;
      gr.northing = Math.floor(gr.northing/gr.accuracy)*gr.accuracy;
      ngr = find_gridsquare(gr);
      wkt = 'POINT('+ p.x + ' ' + p.y + ')';
      ptext = ngr.asString(); 
   }
   mappoly.value = wkt;
   // execute onchange if the function is defined. 
   // no catch action because it just means the onchange function is not defined
   try {mappoly.onchange();} catch (e) {}
   if (mapname) {
      if (mapname.nodeName == 'INPUT') mapname.value = ptext;
      else mapname.replaceChild(document.createTextNode(ptext),mapname.firstChild);
   }
   points = new Array();
   hidedialog();
   return true;
}         
function s2_drawmap(el) {
   mapbutton = el.replace(/^[^_]+_/,'');
   var wktnode = document.getElementById(mapbutton);
   if (wktnode && wktnode.value) polygon = s2_wkt_to_gmappoly(wktnode.value);  
   var parent,div,span,h3,h4,mt,mc,mo,button,node,img;
   cleardialog('dialog-liner');
   var dialog = document.getElementById('dialog-liner');
   button = document.createElement('BUTTON');
   button.className ='vs_showhide';
   button.onclick= function() {mapShrinkOrExpand(this);};
   img = document.createElement('IMG');
   img.src = '../images/shrink2.gif'; 
   img.alt='shrink';
   button.appendChild(img);
   dialog.appendChild(button);
   
   input = document.createElement('INPUT');
   input.id = 'action';
   input.type = 'hidden';
   input.value = 'recentre';
   dialog.appendChild(input);
   mo = document.createElement('DIV');
   mo.id = 'mapoutline'; 
   
   mt = document.createElement('DIV');
   mt.id = 'maptools';
   
   h3 = document.createElement('H3');
   h3.appendChild(document.createTextNode('Tools'));
   mt.appendChild(h3);
   
   div = document.createElement('DIV');
   div.style.margin = '3px';
   button = document.createElement('BUTTON');
   button.className = 'tool';
   button.onclick = function() {document.getElementById("action").value="recentre";};
   img = document.createElement('IMG');
   img.src = '../images/recentre.gif';
   img.alt = 'recentre';
   button.appendChild(img);
   div.appendChild(button);
   button = document.createElement('BUTTON');
   button.className = 'tool';
   button.onclick = function() {document.getElementById("action").value="point";deletePolygon();};
   img = document.createElement('IMG');
   img.src='../images/point2.gif';
   img.alt = 'grid square';
   button.appendChild(img);
   div.appendChild(button);
   
   node = document.createElement('DIV');
   
   button = document.createElement('BUTTON');
   button.className = 'tool';
   button.onclick = function() {document.getElementById("action").value="circle";deletePolygon();};
   img = document.createElement('IMG');
   img.src = '../images/circle.gif';
   img.alt = 'buffered point';
   button.appendChild(img);
   node.appendChild(button);
   div.appendChild(node);
   button = document.createElement('BUTTON');
   button.className = 'tool';
   button.onclick = function() {document.getElementById("action").value="polygon";deletePolygon();};
   img = document.createElement('IMG');
   img.src = '../images/closed.gif';
   img.alt = 'polygon';
   button.appendChild(img);
   div.appendChild(button);

   button = document.createElement('BUTTON');
   button.className = 'tool'; 
   button.onclick = function() {document.getElementById("action").value="polyline";deletePolygon();};
   img = document.createElement('IMG');
   img.src = '../images/polygon.gif';
   img.alt = 'polyline';
   button.appendChild(img);
   div.appendChild(button);
   button = document.createElement('BUTTON');
   button.className = 'tool';
   button.onclick = function() {deletePolygon();};
   button.appendChild(document.createTextNode('del'));
   div.appendChild(button);
   
   mt.appendChild(div);
   
   mc = document.createElement('DIV');
   mc.id = 'mapcontainer'; 
   mc.style.margin = '5px';
   mc.style.border = '1px solid black'
   mc.style.backgroundColor = 'white'; 
   mc.style.padding = '5px';
   
   div = document.createElement('DIV');
   div.id = "map"; 
   div.className = 'gmap';
   mc.appendChild(div);
   
   div = document.createElement('DIV');
   div.id = 'maplabels';
   
   h4 = document.createElement('H4');
   h4.style.display = 'inline';
   h4.appendChild(document.createTextNode('Gridref'));
   div.appendChild(h4);
   
   input = document.createElement('INPUT');
   input.id = 'ngr';
   input.className = 'text'; 
   input.style.border = '1px solid #ddd';
   input.style.padding = '2px'; 
   input.style.width = '100px'; 
   input.value = '';
   input.onchange = function() {updatePoint();};
   div.appendChild(input);
   
   h4 = document.createElement('H4');
   h4.style.display = 'inline';
   h4.appendChild(document.createTextNode('LatLon'));
   div.appendChild(h4);
   
   input = document.createElement('INPUT');
   input.id = 'll';
   input.className = 'text'; 
   input.style.border = '1px solid #ddd'; 
   input.style.padding = '2px'; 
   input.style.width = '120px';
   input.onchange = function() {updateLLPoint();};
   div.appendChild(input);      
   
   h4 = document.createElement('H4');
   h4.style.display = 'inline';
   h4.appendChild(document.createTextNode('Accuracy'));
   div.appendChild(h4);
   
   input = document.createElement('INPUT');
   input.id = 'acc'; 
   input.className = 'text'; 
   input.style.border = '1px solid #ddd'; 
   input.style.padding = '2px'; 
   input.style.width = '40px';
   input.value = 1000
   div.appendChild(input);
   span = document.createElement('SPAN');
   span.appendChild(document.createTextNode('m'));
   div.appendChild(span);
   
   mc.appendChild(div);      
   mo.appendChild(mc);
   mo.appendChild(mt);   
   dialog.appendChild(mo);
   s2_mapload();
   node = document.createElement('HR');
   node.style.clear = 'both';
   node.style.display = 'block';
   dialog.appendChild(node);
   button = document.createElement('BUTTON');
   button.appendChild(document.createTextNode('OK'));
   button.onclick = s2_assignmap;
   dialog.appendChild(button);
   button = document.createElement('BUTTON');
   button.appendChild(document.createTextNode('Copy'));
   button.onclick = hidedialog;
   dialog.appendChild(button);
   button = document.createElement('BUTTON');
   button.appendChild(document.createTextNode('Cancel'));
   button.onclick = hidedialog;
   dialog.appendChild(button);
   showdialog();
   maplayout();
   return false;
}