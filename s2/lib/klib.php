<?php
class KLib {
   protected $k1;
   protected $k2;
   function KLib($sid) {
      $salpha = preg_split('//',$sid);
      array_shift($salpha);
      array_pop($salpha);
       
      $alphastring = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $alpha = preg_split('//',$alphastring);
      array_shift($alpha);
      array_pop($alpha);
      $numer = array_flip($alpha);
      $stotal = 0;
      for($i=0;$i<count($salpha);$i++) {
         $stotal += $numer[$salpha[$i]];
      }
//print $stotal;
      $dnum = floor(intval(gmdate('Ymd'))/intval(gmdate('dm')))%count($alpha);
//print "<p>" . $dnum . " Ymd: " . intval(gmdate('Ymd')) . " dm: " . intval(gmdate('dm')) . " alpha: " . count($alpha) . " floor: " . intval(gmdate('Ymd'))/intval(gmdate('dm')) . "</p>";
      $alpha = array_merge(array_slice($alpha,$dnum),array_slice($alpha,0,$dnum));
      $pos1 = 0;
      $pos2 = 0;
      $inter;
      for ($i=0;$i<count($alpha);$i++) {
         $pos2 = ($stotal*$numer[$alpha[$i]])%count($alpha); 
         $inter = $alpha[$pos2];
         $alpha[$pos2] = $alpha[$pos1];
         $alpha[$pos1] = $inter;
         $pos1 = ($pos2+1)%count($alpha);            
      }
//print "<pre>";print_r($alpha);print "</pre>";
      $numer = array_flip($alpha);
      
      $dnum = $dnum%count($salpha);
      $salpha = array_merge(array_slice($salpha,$dnum),array_slice($salpha,0,$dnum));
//print "<pre>";print_r($salpha);print "</pre>";
      $kl = 0;
      $k1 = '';
      $k2 = '';
      $z01 = null;
      $z02 = null;
      while($kl<8) {
         $z1 = new z($numer[$salpha[($kl*2)]],$numer[$salpha[(($kl*2)+1)]]);
//print "<p>" . $numer[$salpha[($kl*2)]] . " " .$numer[$salpha[(($kl*2)+1)]] . "</p>"; 
         if (isset($z01)) $z1->add($z01);
//print "<p>" . $z1->r . " " . $z1->i . "</p>";
         $z2 = new z($numer[$salpha[(count($salpha)-(($kl*2)+1))]],$numer[$salpha[(count($salpha)-(($kl*2)+2))]]);
//print "<p>" . $numer[$salpha[(count($salpha)-(($kl*2)+1))]] . " " .$numer[$salpha[(count($salpha)-(($kl*2)+2))]] . "</p>"; 
         if (isset($z02)) $z2->add($z02);
//print "<p>" . $z2->r . " " . $z2->i . "</p>";
         $k1 .= $alpha[(floor($z1->size())%count($alpha))];
         $k2 .= $alpha[(floor($z2->size())%count($alpha))];
         $powr = (($numer[$salpha[($kl*2)]]%3)+1);
         $oper = ($numer[$salpha[($kl*2+1)]]%2);
         if (isset($z01)) {
            switch($oper) {
            case 0: $z01 = $z1->add($z01);break;
            case 1: $z01 = $z1->sub($z01);break;
            }
         } else {
            //$z01 = $z1;
            switch($powr) {
            case 1: $z01 = $z1;break;
            case 2: $z01 = $z1->sqd();break;
            case 3: $z01 = $z1->cbd();break;
            }
         }
         $powr = ((($numer[$salpha[(count($salpha)-(($kl*2)+1))]])%3)+1);
         $oper = (($numer[$salpha[(count($salpha)-(($kl*2)+2))]])%2);
         if (isset($z02)) {
            switch($oper) {
            case 0: $z02 = $z2->add($z02);break;
            case 1: $z02 = $z2->sub($z02);break;
            }
         } else {
            //$z02 = $z2;
            switch($powr) {
            case 1: $z02 = $z2;break;
            case 2: $z02 = $z2->sqd();break;
            case 3: $z02 = $z2->cbd();break;
            }
         }
         $kl++;
//print "<p>" . floor($z1->size()) . " " . floor($z2->size()) . "</p>";               
      }
      $this->k1 = $k1;
      $this->k2 = $k2;
   }
   function getkeys() {         
      $j = new stdClass();
      $j->k1 = $this->k1;
      $j->k2 = $this->k2;
      return json_encode($j);
   }
}
class z {
   public $r;
   public $i;
   function z($r,$i) {
      $this->r = $r;
      $this->i = $i;
   }
   function get($part=null) {
      switch($part) {
      case 'r': return $this->r;break;
      case 'i': return $this->i;break;
      default: return array($this->r,$this->i);break;
      }
   }
   function sqd() {
      $r = $this->r*1;
      $this->r = ((pow($r,2)) - (pow($this->i,2)));
      $this->i = (2 * $r * $this->i);
   }
   function cbd() {
      $r = $this->r*1;
      $i = $this->i*1;
      $this->r = (pow($r,3) - ($r * pow($i,2)) - (2 * $r * pow($i,2)));
      $this->i = ((pow($r,2)*$i) - pow($i,3) + (2 * pow($r,2) * $i));     
   }
   function add($z1) {
      $this->r = ($this->r + $z1->r);
      $this->i = ($this->i + $z1->i);  
   }
   function sub($z1) {
      $this->r = ($this->r - $z1->r);
      $this->i = ($this->i - $z1->i);
   }
   function mult($z1) {
      $this->r = ($this->r * $z1->r)-($this->i * $z1->i);
      $this->i = ($this->r * $z1->i)+($this->i * $z1->r);
   }
   function dist($z1) {
      return sqrt(pow(($this->r - $z1->r),2) + pow(($this->i - $z1->i),2));
   }
   function size() {
      return sqrt(pow($this->r,2)+pow($this->i,2));
   }
}
function encrypt($clear,$k1,$k2) {
   // nothing encrypted is nothing so don't bother trying
   if (isset($clear) && $clear != "") {
      $cipher = mcrypt_module_open('des','','cbc','');
      mcrypt_generic_init($cipher, $k1, $k2);
      $encrypted = stringToHex(mcrypt_generic($cipher,$clear));
      mcrypt_generic_deinit($cipher);
      mcrypt_module_close($cipher);
   } else $encrypted = $clear; 
   return $encrypted;
   
}
function decrypt($encrypted,$k1,$k2) {
//print "$k1 $k2 $encrypted";
   if (isset($encrypted) && $encrypted != "") {
      $cipher = mcrypt_module_open('des','','cbc','');
      mcrypt_generic_init($cipher, $k1, $k2);
      $decrypted = rtrim(mdecrypt_generic($cipher,hexToString($encrypted)),"\0");
      mcrypt_generic_deinit($cipher);
      mcrypt_module_close($cipher);
   } else $decrypted = $encrypted; 
   return $decrypted;
}
?>