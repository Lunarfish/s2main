function s2_openclose(bt) {
   var bid = bt.id;
   var uid = bid.replace(/bt_/,'ul_');
   var ul = document.getElementById(uid);
   var li = bt.parentNode;
   switch(ul.className) {
   case 's2open': {
      ul.className = 's2closed';
      li.className = 's2closed';
   }break;
   default: {                           
      ul.className = 's2open';
      li.className = 's2open';
   }break;
   }
   return true;      
}

function s2_drawfilelist(files) {
   var div,a,node,y,m,d,i,yfiles,mfiles,dfiles,ifile,button,link;
   div = document.createElement('DIV');
   div.className = 's2filecontainer';
   ul = document.createElement('UL');
   for(y in files) {
      var yli,yul;
      yfiles = files[y];
      yli = document.createElement('LI');
      yli.className = 's2closed';
      a = document.createElement('A');
      a.id = 'bt_y'+y;
      a.href = '#';
      a.onclick = function () {s2_openclose(this);};
      a.appendChild(document.createTextNode(y));
      yli.appendChild(a);
      br = document.createElement('BR');
      yli.appendChild(br);
      yul = document.createElement('UL');
      yul.id = 'ul_y'+y;
      yul.className = 's2closed';
      yli.appendChild(yul);         
      for(m in yfiles) {
         var mli,mul;
         mfiles = yfiles[m];
         mli = document.createElement('LI');
         mli.className = 's2closed';
         a = document.createElement('A');
         a.id = 'bt_m'+y+''+m;
         a.href = '#';
         a.onclick = function () {s2_openclose(this);};
         a.appendChild(document.createTextNode(m));
         mli.appendChild(a);
         br = document.createElement('BR');
         mli.appendChild(br);
         mul = document.createElement('UL');
         mul.id = 'ul_m'+y+''+m; 
         mul.className = 's2closed';
         mli.appendChild(mul);
         for(d in mfiles) {
            var dli,dul;
            dfiles = mfiles[d];
            dli = document.createElement('LI');
            dli.className = 's2closed';
            a = document.createElement('A');
            a.id = 'bt_d'+y+''+m+''+d;
            a.href = '#';
            a.onclick = function () {s2_openclose(this);};
            a.appendChild(document.createTextNode(d));
            dli.appendChild(a);
            br = document.createElement('BR');
            dli.appendChild(br);
            dul = document.createElement('UL');
            dul.id = 'ul_d'+y+''+m+''+d;
            dul.className = 's2closed';
            dli.appendChild(dul);
            for(i in dfiles) {
               var ili,iul,url,name,ext;
               ifile = dfiles[i];
               url = ifile['Link'];
               fname = ifile['Name'];
               ext = fname.replace(/^.+\./,'');
               ext = ext.toLowerCase();
               /*
               switch(ext) {
               case 'csv': {fname = 'T:' +fname;}break;
               case 'xls': {fname = 'X:' +fname;}break;
               case 'mif': {fname = 'MI:'+fname;}break;
               case 'mid': {fname = 'MI:'+fname;}break;
               }
               */
               ili = document.createElement('LI');
               ili.className = 's2file';
               a = document.createElement('A');
               a.href = url;
               a.appendChild(document.createTextNode(fname));
               ili.appendChild(a);
               button = document.createElement('BUTTON');
               button.name = 'Use this file.';
               //button.innerHTML = '&rarr;';
               //button.appendChild(document.createTextNode('\u2192'));
               button.appendChild(document.createTextNode('Use'));
               button.onclick = function() {s2_returnfile(this.nextSibling.value);return false};
               s2_addtooltip(button);
               ili.appendChild(button);
               input = document.createElement('INPUT');
               input.type = 'hidden';
               input.value = JSON.stringify(ifile);
               ili.appendChild(input);
               dul.appendChild(ili);
            }
            mul.appendChild(dli);
         }
         yul.appendChild(mli)
      }
      ul.appendChild(yli);
   }
   div.appendChild(ul);
   return div;
}

var ftd;
var fbid;
function s2_getfilelist() {
   var req,par;
   var sid = s2_getcookie('PHPSESSID');
   req = new Object();
   par = {"a":"list"};
   req.target = s2_getcurrentpath()+'/../echofile.php';
   req.request = par;
   req.etype = 'AB';
   req.sync = true;
   req.sid = sid;
   var res = snc_send(req);
   var dec = cdm_decrypt(res,'AB');
   var job = JSON.parse(dec);
   completed();
   return job;         
}
function s2_getfiledetails(id) {
   var req,par;
   var sid = s2_getcookie('PHPSESSID');
   req = new Object();
   par = {"a":"details","i":id};
   req.target = s2_getcurrentpath()+'/../echofile.php';
   req.request = par;
   req.etype = 'AB';
   req.sync = true;
   req.sid = sid;
   var res = snc_send(req);
   var dec = cdm_decrypt(res,'AB');
   var job = JSON.parse(dec);
   completed();
   return job;         
}

function s2_showfiledialog(el,inline)  {
   var h2,node,e,divl,divr,divw;
   ftd = el.parentNode;
   fbid = el.id;
   // get the files 
   var files = s2_getfilelist(); 
   var div = s2_drawfilelist(files);
   
   var dc = document.getElementById('dialog-container');
   var dialog = document.getElementById('dialog-liner');
   
   if (inline) {
      try {
         node = document.getElementById('s2inlinefiledialog');
         while (node.childNodes.length > 0) node.removeChild(node.lastChild);
      } catch (e) {
         node = document.createElement('DIV');
         node.id = 's2inlinefiledialog';
         dialog.appendChild(node);
      }
      dialog = node;
      dialog.appendChild(document.createElement('BR'));
   } else cleardialog('dialog-liner');
   divw = document.createElement('DIV');
   divw.style.textAlign = 'center';
   
   divl = document.createElement('DIV');
   divl.style.display = 'inline-block';
   divl.style.width = '500px';
   divl.style.height = '250px';
   divl.style.overflow = 'auto';
   divl.style.border = '1px solid #ccc';
   divl.style.backgroundColor = '#eee';
   divl.style.padding = '5px';
   divl.style.margin = '2px';
      h4 = document.createElement('H4');
      h4.appendChild(document.createTextNode('Uploaded files by date'));
      divl.appendChild(h4);
      divl.appendChild(div);
   divw.appendChild(divl);
   divr = document.createElement('DIV');
   divr.style.display = 'inline-block';
   divr.style.width = '200px';
   divr.style.height = '250px';
   divr.style.overflow = 'auto';
   divr.style.border = '1px solid #ccc';
   divr.style.backgroundColor = '#eee';
   divr.style.padding = '5px';
   divr.style.margin = '2px';
      h4 = document.createElement('H4');
      h4.appendChild(document.createTextNode('Upload a new file'));
      divr.appendChild(h4);
      div = document.createElement('DIV');
      id = fbid.replace(/\_Choose/,'_DialogUploader');
      div.id = 'file-uploader-'+id;
      divr.appendChild(div);
   divw.appendChild(divr);
   dialog.appendChild(divw);
   s2_inituploader(id);
   showdialog();   
} 
function s2_returnfile(json) {
   var a,button,node,x,cx;
   
   var args = s2_disectid(fbid);
   while(ftd.childNodes.length > 0) ftd.removeChild(ftd.lastChild);
   var job = JSON.parse(json);
   var inline = false;
   var dc = document.getElementById('dialog-container');
   var dialog = document.getElementById('dialog-liner');
   try {
      var ifd = document.getElementById('s2inlinefiledialog');
      dialog.removeChild(ifd);
      inline = true;
   } catch (e) {
      hidedialog('dialog-liner');
   }
         
   a = document.createElement('A');
   a.target = 's2doc';
   a.href = unescape(job.Link);
   a.appendChild(document.createTextNode(job.Name));
   a.style.marginRight = '10px';
   ftd.appendChild(a);
   button = document.createElement('BUTTON');
   button.className = 's2iconbutton';
   button.id = fbid.replace(/\_Choose/,'_ShowHide');
   button.appendChild(document.createTextNode('Show'));
   button.onclick = function () {s2_showhidelink(this.id);};
   ftd.appendChild(button);    
   button = document.createElement('BUTTON');
   button.className = 's2iconbutton';
   button.id = fbid;
   button.appendChild(document.createTextNode('Find'));
   button.onclick = function () {s2_showfiledialog(this,inline);};
   ftd.appendChild(button);    
   node = document.createElement('INPUT');
   node.type = 'hidden';
   node.id = fbid.replace(/\_Choose/,'');
   node.name = args.Property; 
   node.value = job.Link;
   ftd.appendChild(node);
   node = document.createElement('INPUT');
   node.type = 'hidden';
   node.id = fbid.replace(/\_Choose/,'_FileName');
   node.value = job.Name;
   ftd.appendChild(node);
   node = document.createElement('INPUT');
   node.type = 'hidden';
   node.id = fbid.replace(/\_Choose/,'_Id');
   node.value = job.S2_File_ID;
   ftd.appendChild(node);
   s2_hidetooltip();   
}

var links = new Array();
function s2_setlink(href,node) {
   links[href] = node;
}
function s2_getlink(href) {
   var node;
   node = links[href];
   links[href] = null;
   return node;
}
function s2_showhidelink(id) {
   var button = document.getElementById(id);
   var a = button.previousSibling;
   if (button.firstChild.nodeValue == 'Show') {
      s2_setlink(a.href,a.firstChild);
      a.replaceChild(document.createTextNode(a.href),a.firstChild);
      button.replaceChild(document.createTextNode('Hide'),button.firstChild);
   } else {
      //var fnid = id.replace(/\_ShowHide/,'_FileName');
      //var fnode = document.getElementById(fnid);
      //var fname = (fnode)?fnode.value:'Download';  
      //a.replaceChild(document.createTextNode(fname),a.firstChild);
      a.replaceChild(s2_getlink(a.href),a.firstChild);
      button.replaceChild(document.createTextNode('Show'),button.firstChild);
   } 
   return true;
}
function s2_isimageurl(url) {
   var isimg = getQueryVariable('isimg',url);
   var ext,i;
   var imgtypes = ["jpg","jpeg","gif","png"];
   if (!isimg) {
      ext = url.replace(/.+\.(.+$)/,"$1");
      for (i in imgtypes) if (ext.toLowerCase() == imgtypes[i]) isimg = 1;    
   }
   return isimg;
}


function s2_inituploader(upload) {
   var uploader,node;
   node = document.getElementById('file-uploader-'+upload);
   if (node) {
      uploader = new qq.FileUploader({
         element: node,
         action: '../echofile.php',
         allowedExtensions: ["jpg","gif","png","doc","docx","csv","xls","xlsx","ppt","txt","pdf","eml","mid","mif"],
         debug: false,
         onComplete: function(id, fileName, responseJSON){s2_linkuploader(this.element.id,fileName,responseJSON);}
      });           
   }  
}
function s2_linkuploader(id,file,jsonobj) {
   var div,a,img,node,url,p,input,name,button;
   node = document.getElementById(id);
   var qqu = node.lastChild;
   var qqul = qqu.lastChild;
   var qqi = 0;
   var qqli,qqname;
   while (qqi < qqul.childNodes.length) {
      qqli = qqul.childNodes[qqi];
      qqi++;
      qqname = qqli.firstChild.firstChild.nodeValue;
      if (jsonobj.data.Name == qqname) {
         if (jsonobj.image && jsonobj.image != 'false') {
            a = document.createElement('A');
            a.className = 's2_imglink';
            a.target = 's2doc';
            a.href = jsonobj.data.Link;
            img = new Image();
            img.src = jsonobj.data.Link;
            img.style.width = '120px';
            a.appendChild(img);
            qqli.appendChild(a);
            p = document.createElement('P');
            p.appendChild(document.createTextNode(jsonobj.data.Link));
            qqli.appendChild(p);
         } else {
            a = document.createElement('A');
            a.className = 's2_txtlink';
            a.target = 's2doc';
            a.href = jsonobj.data.Link;
            a.appendChild(document.createTextNode(jsonobj.data.Link));
            a.style.marginRight = '10px';
            qqli.appendChild(a);
         }
         button = document.createElement('BUTTON');
         button.name = 'Use this file.';
         //button.innerHTML = '&rarr;';
         //button.appendChild(document.createTextNode('\u2192'));
         button.appendChild(document.createTextNode('Use'));
         button.onclick = function() {s2_returnfile(this.nextSibling.value);return false};
         s2_addtooltip(button);
         qqli.appendChild(button);
         input = document.createElement('INPUT');
         input.type = 'hidden';
         input.value = JSON.stringify(jsonobj.data);
         qqli.appendChild(input);
      }      
   }
   return true;
}
   