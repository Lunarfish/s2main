<?php
function deleteverb($url,$auth=null,$authtype=null) {
   //error_reporting(E_ALL);      
   $curl = curl_init($url);
   curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
   if ($auth) {
      switch(strtolower($authtype)) {
      case 'basic': {
         $authconst = CURLAUTH_BASIC;
      }break;
      case 'digest': {
         $authconst = CURLAUTH_DIGEST;
      }break;
      }
      curl_setopt($curl, CURLOPT_HTTPAUTH, $authconst);  
      curl_setopt($curl, CURLOPT_USERPWD, $auth);
   }
   curl_setopt($curl, CURLOPT_HEADER, false);
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
   
   // Make the call
   $res = curl_exec($curl);
   /*
   $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
   $header = substr($res, 0, $header_size);
   $res = substr($res, $header_size);                     
   //*/
   curl_close($curl);
   return $res;
}
?>