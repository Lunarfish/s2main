function setpage(page) {
   if (!page) page = 'sitesystem';
   else page = page.replace("#","");
   var pages = document.getElementsByTagName('DIV');
   var p;
   for (p in pages) {
      if (pages[p].className == 'tabbedpane' 
            && pages[p].id == 'tabbedpane_current') pages[p].id = "";   
   }
   p = document.getElementById(page).parentNode;
   p.id = 'tabbedpane_current';
   window.location.hash = page;
   window.scrollTo(0,0);
   return false;
}
function showHideAddInfo(button) {
   var item = button.parentNode;
   var addinfo = item.childNodes[0];
   while (addinfo.className != 'addinfo') {
      addinfo = addinfo.nextSibling;
   }
   var display = addinfo.style.display;
   addinfo.style.display = (display == 'block') ? 'none':'block';
   addinfo.style.height = (display == 'block') ? '0':'auto';
   var name = (button.childNodes[0].nodeValue == 'show...')?'hide...':'show...';
   button.replaceChild(document.createTextNode(name),button.childNodes[0]);
   button.blur();
   return true;
}