function s2_makecollapsible() {
   var nodes,nx,node,collapser,btn,img,expand; 
   nodes = document.getElementsByTagName('DIV');
    
   for (nx in nodes) {                              
      node = nodes[nx];
      if (node.className.match(/s2_collapsible/)) {
         expand = (node.className.match(/expand/));
         collapser = node.previousSibling;
         btn = document.createElement('BUTTON');
         btn.className = 's2_expander';
         img = new Image();
         img.src = (expand)?'../images/arrow_d.gif':'../images/arrow_r.gif';
         //img.src = (expand)?'../images/eye-open-20.gif':'../images/eye-closed-20.gif';
         img.className = 's2_expander';
         img.alt = (expand)?'Collapse':'Expand';
         btn.appendChild(img);
         btn.name = img.alt;
         s2_addtooltip(btn);
         btn.onclick = function () {s2_clickcollapser(this);};
         collapser.insertBefore(btn,collapser.firstChild);
         node.className = (expand)?'s2_visiblesection':'s2_hiddensection';
      } 
   }
}
function s2_clickcollapser(btn) {
   var collapser,node,img;
   collapser = btn.parentNode;
   node = collapser.nextSibling;
   img = btn.firstChild;    
   switch (node.className) {
   case 's2_hiddensection': {
      node.className = 's2_visiblesection';
      img.src = '../images/arrow_d.gif';
      //img.src = '../images/eye-open-20.gif';
      img.alt = 'Collapse';
      btn.name = 'Collapse';
   }break;
   default: {
      node.className = 's2_hiddensection';
      img.src = '../images/arrow_r.gif';
      //img.src = '../images/eye-closed-20.gif';
      img.alt = 'Expand';
      btn.name = 'Expand';
   }break;
   }
   return true;
}