function mswordit(element) {
   var cleaned = element.cloneNode(true);
   cleaned = removeNodes(cleaned);
   /*
   var url = "msword.php?element=" + escape(cleaned.innerHTML);
   var msword = window.open(url,"msword","status=1,width=350,height=150");
   */
   document.getElementById('wordcontent').value = cleaned.innerHTML;
   document.msword.submit();
   return false;
}
function removeNodes(element) {
   for (var e=0;e<element.childNodes.length;e++) {
      var el = element.childNodes[e];
      switch (el.nodeName) {
      case 'A': {
         if (!el.href) element.removeChild(el);
      } break;
      case 'BUTTON': {
         element.removeChild(el);
      } break;
      case 'INPUT': {
         var p = document.createElement('P');
         var tn = document.createTextNode(el.value);
         p.appendChild(tn);
         element.replaceChild(p,el);
      } break;
      case 'TEXTAREA': {
         var p = document.createElement('P');
         var tn = document.createTextNode(el.value);
         p.appendChild(tn);
         element.replaceChild(p,el);
      } break;
      case 'IMG': {
         var tn = document.createTextNode(el.alt);
         element.replaceChild(tn,el);
      } break;
      case 'TABLE': {
         el.style.border = 1;
         el.style.borderColor = "#ccc";
         el.border = 1;
         el.bordercolor = "#ccc";
      } break;
      }
      if (el.childNodes.length > 0) {
         el = removeNodes(el);
      } 
   }
   return element;
}
