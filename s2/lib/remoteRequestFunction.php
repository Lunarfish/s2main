<?php
$ch;
function remoteRequest($url,$raw=false,$auth=null,$authtype=null) {
   global $ch;
   if ($raw) {
      /* raw returns the xml WFS:FeatureCollection as an ascii string */
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      // RETURNTRANSFER means the result is returned into a variable instead of 
      // echoed to the screen.
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      if ($authtype) {
         $authconst = null;
         switch(strtolower($authtype)) {
         case 'basic': {
            $authconst = CURLAUTH_BASIC;
         }break;
         case 'digest': {
            $authconst = CURLAUTH_DIGEST;
         }break;
         }
         curl_setopt($ch, CURLOPT_HTTPAUTH, $authconst);  
         curl_setopt($ch, CURLOPT_USERPWD, $auth);
         //curl_setopt($ch, CURLOPT_HEADER, true);
      }
      $response = curl_exec($ch);
   } else {
      /* by default the XMLParser class is used to parse the response XML 
         WFS:FeatureCollection message into an object structure */
      $xml = null;
      if ($authtype) {
         /* see ./lib/XMLParser-1.1/DigestXMLParser.class.php for details of this class */
         include_once('./lib/XMLParser-1.1/DigestXMLParser.class.php');
         $xml = new DigestXMLParser();
         $response = $xml->parse_curl($url,$auth);
         unset($xml);
      } else {
         /* see ./lib/XMLParser-1.1/XMLParser.class.php for details of this class */
         include_once('./lib/XMLParser-1.1/XMLParser.class.php');
         $xml = new XMLParser();
         $response = $xml->parse($url);
         unset($xml);
      }
   }
   return $response;
}
function getReturnedContentType() {
   global $ch;
   return curl_getinfo($ch,CURL_INFO_CONTENT_TYPE);
}
function forwardXMLPost($url,$xmlpacket,$auth=null,$authtype=null) {
   $ch = curl_init(); //initiate the curl session
   $timeout = 90;
   //$auth = "$user:$pass";   
   curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:text/xml"));
   curl_setopt($ch, CURLOPT_URL, $url); //set to url to post to
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // tell curl to return data in
   if ($authtype == 'digest') {
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
      curl_setopt($ch, CURLOPT_USERPWD, $auth);
   }
   curl_setopt($ch, CURLOPT_POST, 1);
   curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlpacket); // post the xml
   curl_setopt($ch, CURLOPT_TIMEOUT, (int)$timeout); // set timeout in seconds
   return curl_exec($ch);
}
?>
