function bodyheight() {
   var D = document;
   return Math.max(
      Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
      Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
      Math.max(D.body.clientHeight, D.documentElement.clientHeight)
   );
}
function bodywidth() {
   var D = document;
   return Math.max(
      Math.max(D.body.scrollWidth, D.documentElement.scrollWidth),
      Math.max(D.body.offsetWidth, D.documentElement.offsetWidth),
      Math.max(D.body.clientWidth, D.documentElement.clientWidth)
   );
}
function getDocDimensions() {
   return new Array(bodywidth(),bodyheight());
} 
function placescreen() {
   var p,screen,e;
   try {
      p = getDocDimensions();
      screen = document.getElementById('screen');
      screen.style.width = p[0];
      screen.style.height = p[1];
   }catch (e) {}
   return true;
}
function setscreenres() {
   var e;
   try {
      var p = getDocDimensions();
      var page = document.getElementById('page');
      var d = document.getElementById('incontentdialog');
       
      if (p[0] <= 1100) {
         page.style.width = (p[0] - 40) + 'px';
         /*page.style.marginLeft = '8px';*/        
      } else {
         page.style.width = '1000px';
         /*page.style.marginLeft = '15%';*/        
      }
      var w = page.offsetWidth;
      d.style.width = (w-260)+'px';
   } catch (e) {}
   return true;
}