var map;
var mapbutton;
var infoWindow;
var overlays = new Array();
var mapdiv = 'gmap3';
var tool = 'recentre';
var points = new Array();
var vertices = 36;
var paths = new Array();

function gm3_drawmap(el) {
   var wktnode,polygon;
   mapbutton = el.replace(/^[^_]+_/,'');
   wktnode = document.getElementById(mapbutton);
   polygon;
   if (wktnode && wktnode.value) polygon = gm3_readwkt(wktnode.value);
   cleardialog('dialog-liner');
   var dialog = document.getElementById('dialog-liner');
   var mapcont = document.createElement('DIV');
   mapcont.id = 'mapcontainer';
   dialog.appendChild(mapcont)
   showdialog();
   gm3_creategmap('mapcontainer');
}  
function gm3_creategmap(parent) {
   var pnode = document.getElementById(parent);
   var d = pnode.parentNode;
   var tools,map,div,toolw,mapw,button;
   pnode.className = 'mapcontainer';
   var w = s2_eldims(pnode,'width');
   toolw = 180;
   mapw = w - toolw - 30;
   
   tools = s2_getmaptoolstable();
   tools.style.width = toolw+'px';
   div = document.createElement('DIV');
   div.style.width = mapw+'px';
   div.className = 'mapdiv';
   div.id = mapdiv;
   pnode.appendChild(tools);
   pnode.appendChild(div);
   
   gm3_initgmap();
   s2_setmaptool('recentre');
   s2_showlocation();

   d.appendChild(document.createElement('HR'));
   button = document.createElement('BUTTON'); 
   button.className = 'dbutton';
   button.onclick = function() {hidedialog();};
   button.appendChild(document.createTextNode('Cancel'));
   d.appendChild(button);             

   button = document.createElement('BUTTON');
   button.className = 'dbutton';
   button.onclick = function() {gm3_assignmap();};
   button.appendChild(document.createTextNode('OK'));
   d.appendChild(button);
   d.appendChild(document.createElement('HR'));             
}
function gm3_initgmap() {
   var md = document.getElementById(mapdiv); 
   try {
      var myLatLng = new google.maps.LatLng(53.38, -1.46);
      var myOptions = {
         zoom: 12,
         center: myLatLng,
         panControl: false,
         zoomControl: true,
         mapTypeControl: true,
         scaleControl: true,
         streetViewControl: true,
         overviewMapControl: true,
         mapTypeId: google.maps.MapTypeId.SATELLITE
      };
      map = new google.maps.Map(md,myOptions);
      google.maps.event.addListener(map, 'click', function(e) {s2_clickmap(e);});
      if (overlays['boundary']) {
         overlays['boundary'].setMap(map);
         s2_fittomap();
      }
      //s2_showzoom();
   } catch (e) {
      md.appendChild(document.createTextNode('Google Maps: Initialise failed.'));
   }
}
function gm3_readwkt(wkt) {
   var b;
   if (wkt.match(/POINT\(/)) b = gm3_wkt_to_point(wkt);
   else b = gm3_wkt_to_mpoly(wkt);
   overlays['boundary'] = b;
   return b; 
}
function gm3_getnamefromoverlay(overlay) {
   var name;
   if (overlay instanceof google.maps.Polygon) name = gm3_mpoly_getname(overlay);
   if (overlay instanceof google.maps.Marker) name = overlay.getTitle();
   return name;
}
function gm3_mpoly_getname(polygon) {
   var wkt,p,ddc,gr,ngr,ptext,pname,parea,path,zoom;
   try {
      if (polygon instanceof google.maps.Circle) {
         path = s2_getcirclevertices(polygon);
         polygon = new google.maps.Polygon({paths: path});
         polygon.setMap(map);
      }
      p = gm3_getmultipolycenter(polygon)
      ddc = new DegreesDecimalCoordinate(p.lat(),p.lng());
      gr = s2_ll_to_gr(ddc);
      gr.accuracy = 10;
      gr.easting = Math.floor(gr.easting/gr.accuracy)*gr.accuracy;
      gr.northing = Math.floor(gr.northing/gr.accuracy)*gr.accuracy;
      ngr = s2_gr_to_ngr(gr);
      parea = gm3_mpolygetarea(polygon);
      if (parea > 900000) ptext = Math.round(parea/100000)/10+' km� @ '+ngr.asString();
      else if (parea > 9000) ptext = Math.round(parea/1000)/10+' ha @ '+ngr.asString();  
      else ptext = Math.round(parea*10)/10+' m� @ '+ngr.asString();
   } catch(err) {
      ptext = 'Google Maps not available in offline mode'; 
   }
   return ptext;
}
function gm3_mpolygetarea(p) {
// split polygon into paths,
// get area for each path 
// add to total area or remove if path is a hole.
   var mps,par,i,a,ax,px;
   mps = gm3_mpoly_to_arrays(p);
   if (mps.length > 1) par = gm3_get_parents(mps);
   else par = null;
   a = 0;
   for (i in mps) {
      path = mps[i];
      px = gm3_poly_from_ring(path);
      ax = s2gmapv3getarea(px.getPath().getArray());
      // if it has an odd number of parents, it's a hole
      is_hole = (!par)?0:((par[i].length % 2) == 1);
      a = (is_hole)?a-ax:a+ax;
   }
   return a;
}
function gm3_mpoly_to_arrays(p) {
   var p,c,cx,cy,maxx,maxy,minx,miny,sw,ne,mps,mpp; 
   var path,paths,cxs,px,r,e,rs,rx,m;
   mps = [];
   try {
      paths = p.getPaths();
      cxs = [];
      rs = paths.getLength();
      for(rx=0;rx<rs;rx++) {
         path = paths.getAt(rx).getArray();
         mpp = [];
         for (p in path) {
            c = path[p];
            mpp.push([c.lng(),c.lat()]);
         }
         mps.push(mpp);
      }
      
   } catch (e) { /* add handler */ }
   return mps; 
}


function gm3_reorder_paths(mps) {
   var i,is_hole,is_clockwise,path,rpath,parents;
   parents = gm3_get_parents(mps);
   for (i in parents) {
      path = mps[i];
      // if it has an odd number of parents, it's a hole
      is_hole = ((parents[i].length % 2) == 1);
      is_clockwise = gm3_path_is_clockwise(path);
      if ((is_hole && is_clockwise)||(!is_hole && !is_clockwise)) path.reverse();
      mps[i] = path;
   }
   return mps;
}
function gm3_get_parents(mps) {
   var parents,polys,i,pi,j,pj;
   parents = new Array();
   polys = new Array();
   for(i in mps) {
      polys[i] = gm3_poly_from_ring(mps[i]);
   }
   for(i in polys) {
      parents[i] = new Array();
      pi = polys[i];
      for (j in polys) {
         pj = polys[j];
         if (gm3_p1_contains_p2(pj,pi)) parents[i].push(j);
      }
   }
   return parents;
}
function gm3_show_path_directions(mps) {
   var px,pathx,dir;
   for(px in mps) {
      pathx = mps[px];
      dir = (gm3_path_is_clockwise(pathx))?'clockwise':'anti-clockwise';
      alert('Path ' + px + ': ' + dir);
   }
}
function gm3_poly_from_ring(ring) {
   var p,path,i,xy,grp,llp,ll;
   path = new Array();
   for (i in ring) {
      xy = ring[i];
      ll = new google.maps.LatLng(xy[0],xy[1]);
      path.push(ll);
   }
   p = new google.maps.Polygon({
      paths: path,
      clickable: false
   });
   return p;
}
function gm3_p1_contains_p2(p1,p2) {
   // For a p1 to contain p2 
   //       all p2 vertices fall within p1 
   // and   all p1 vertices fall outside p2
   var ppath,i,pi,is_inside;
   ppath = p2.getPath().getArray();
   is_inside = true;
   for (i in ppath) {
      pi = ppath[i];
      is_inside &= (google.maps.geometry.poly.containsLocation(pi,p1));
   }
   ppath = p1.getPath().getArray();
   for (i in ppath) {
      pi = ppath[i];
      is_inside &= (!google.maps.geometry.poly.containsLocation(pi,p2));
   }
   return is_inside;
}
function gm3_path_is_clockwise(path) {
   var i,ptc,ptm,ptn,vx,theta,thetam,thetan,thetat;
   ptc = gm3_get_path_center(path);
   vx = path.length-1;
   thetat = 0;
   for (i=0;i<vx;i++) {
      ptm = path[i];
      ptn = path[((i+1)%vx)];
      if(ptm != ptn) {
         thetam = gm3_get_angle_from_center(ptc,ptm);
         thetan = gm3_get_angle_from_center(ptc,ptn);
         theta = thetan - thetam;
         // each pair of vertices is joined by a straight line to form a polygon
         // so if you have an angle > 180 degrees you need the other side of the 
         // circle.
         if (Math.abs(theta)>180) theta = (theta>0)?(theta-360):(theta+360);
         thetat += theta;
      }
   }
   return (thetat > 0); 
}
function gm3_get_angle_from_center(ptc,ptn) {
   var cx,cy,nx,ny,dx,dy,theta,r2d;
   r2d = 180/Math.PI;
   cx = ptc[0];cy = ptc[1];
   nx = ptn[0];ny = ptn[1];
   dx = cx - nx;
   dy = ny - cy;
   if (dx == 0 && dy == 0) theta = 0; 
   else if (dy == 0) theta = (dx > 0)?180:0;
   else if (dx == 0) theta = (dy > 0)?90:-90;
   else {
      theta = Math.atan(dy/dx) * r2d;
      if (dx<0) theta += 180;
   }
   return theta;
}
function gm3_get_path_center(path) {
   var i,cx,cy,ax,ay,tx,ty,ptx,vx;
   vx = path.length-1;
   tx = 0;ty = 0;
   for (i=0;i<vx;i++) {
      ptx = path[i];
      cx = ptx[0];
      cy = ptx[1];
      tx+= cx;
      ty+= cy;
   }
   ax = tx/vx;
   ay = ty/vx;
   return [ax,ay];
}
function gm3_arrays_from_mpwkt(wkt) {
   var pairs,p,pt,xy,ll,polygon,rings,r,rg,grp,llp,epsg,err,simp,simpby,simpto;
   simp = true;
   simpto = 200;
   try {epsg = parseInt(wkt.replace(/SRID\=(\d+)\;.*/,'$1'));} 
   catch (err) {epsg = 4326;}
   try {
      ll = new google.maps.LatLng(0,0);  
      if (wkt) {
         paths = new Array();
         points = new Array();
         wkt = wkt.replace(/^[^\(]+\(+/,'');
         wkt = wkt.replace(/\)+$/,'');
         rings = wkt.split(/\)+\,\(+/);
         for(r in rings) {
            rg = rings[r];
            pairs = rg.split(/\,/);
            if (simp && (pairs.length > 100)) simpby = parseInt(pairs.length/simpto);
            if (!simpby || simpby < 1) simpby = 1;
            for(p in pairs) { 
               if (!simp || (p%simpby==0)) { 
                  pt = pairs[p];
                  xy = pt.split(/\s+/);
                  switch(epsg) {
                  case 27700: {
                     llp = conv_uk_ings_to_ll(new GridReference(xy[0],xy[1]));
                     xy = new Array(llp.lng(),llp.lat());  
                  }break;
                  }
                  points.push(xy);
               }
            }
            // if not closed, close polygon by adding first point
            if (xy != points[0]) points.push(points[0]);
            paths.push(points);
            points = new Array();
         }
      }
   } catch(err) {/* add something here. */}
   return paths;         
}
function gm3_mpwkt_from_arrays(rings,epsg) {
   var r,vertices,v,coords,c,x,y,wkt,scords,svertices,srings,sv,sr;
   srings = new Array();
   for(r in rings) {
      vertices = rings[r];
      scoords = new Array();
      for (v in vertices) {
         coords = vertices[v];
         switch(epsg) {
         case 27700: {
            llp = conv_uk_ings_to_ll(new GridReference(coords[0],coords[1]));
            coords = new Array(llp.lng(),llp.lat());   
         }break;
         }
         scoords.push(coords.join(' '));
      }
      srings.push('(' + scoords.join(',') + ')');
   } 
   wkt = 'POLYGON(' + srings.join(',') + ')';
   return wkt;  
}
function gm3_wkt_to_mpoly(wkt) {
   var pairs,p,pt,xy,ll,polygon,rings,r,rg,grp,llp,epsg,err,mps;
   mps = gm3_arrays_from_mpwkt(wkt);
   if (mps.length > 1) mps = gm3_reorder_paths(mps);
   //wkt = gm3_mpwkt_from_arrays(mps);
   polygon = null;
   try {epsg = parseInt(wkt.replace(/SRID\=(\d+)\;.*/,'$1'));} 
   catch (err) {epsg = 4326;}
   try {
      ll = new google.maps.LatLng(0,0);  
      if(mps) {
         paths = new Array();
         points = new Array();
         for (r in mps) {
            ring = mps[r];
            for (p in ring) {
               xy = ring[p];
               ll = new google.maps.LatLng(xy[1],xy[0]);
               points.push(ll);
            }
            paths.push(points);
            points = new Array();
         }
         polygon = new google.maps.Polygon({
            paths: paths,
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 0.5,
            fillColor: "#FF0000",
            fillOpacity: 0.35,
            editable: false
         });
      }
   } catch(err) { /* add something here. */ }
   return polygon;   
}
function gm3_getmpolybounds(p) {
   var p,c,cx,cy,maxx,maxy,minx,miny,sw,ne; 
   var path,paths,cxs,px,r,e,rs,rx,m;
   try {
      paths = p.getPaths();
      cxs = new Array();
      rs = paths.getLength();
      for(rx=0;rx<rs;rx++) {
         path = paths.getAt(rx).getArray();
         for (p in path) {
            c = path[p];
            maxx = (maxx)?Math.max(maxx,c.lng()):c.lng();
            minx = (minx)?Math.min(minx,c.lng()):c.lng();
            maxy = (maxy)?Math.max(maxy,c.lat()):c.lat();
            miny = (miny)?Math.min(miny,c.lat()):c.lat();
         }
      }
      sw = new google.maps.LatLng(miny,minx);
      ne = new google.maps.LatLng(maxy,maxx);
      c = new google.maps.LatLngBounds(sw,ne);
   } catch (e) { /* add handler */ }
   return c; 
}
function gm3_getmultipolycenter(p) {
   var p,path,paths,cxs,px,r,c,e,rs,rx,m;
   try {
      paths = p.getPaths();
      cxs = new Array();
      rs = paths.getLength();
      for(rx=0;rx<rs;rx++) {
         path = paths.getAt(rx).getArray();
         r = new google.maps.Polygon({paths: path});
         cxs.push(s2_getpolycenter(r));
      } 
      r = new google.maps.Polygon({paths: cxs});
      c = s2_getpolycenter(r);
   } catch (e)  { /* what to do here */ }
   return c;
}
function s2_drawmultipoly(paths) {
   var p,c;
   if (overlays['boundary']) {
      p = overlays['boundary'];
      p.setMap(null);
   }
   p = new google.maps.Polygon({
      paths: paths,
      strokeColor: "#FF0000",
      strokeOpacity: 0.8,
      strokeWeight: 0.5,
      fillColor: "#FF0000",
      fillOpacity: 0.35,
      clickable: true
   });
   google.maps.event.addListener(p, 'click', function(e) {s2_clickmap(e);});
   p.setMap(map);
   overlays['boundary'] = p;
   c = gm3_getmultipolycenter(p);
   s2_showlocation(c);
   s2_fittomap();
}

function s2_fittomap() {
   var polygon;
   if (map && overlays['boundary']) {
      polygon = overlays['boundary'];
      var bnd = gm3_getmpolybounds(polygon);
      var ctr = bnd.getCenter();
      map.setCenter(ctr);
      map.fitBounds(bnd);  
   }
}

function s2_eldims(el,dim) {
   var r,x,y;
   x = (el.style && parseInt(el.style.width)>0)?parseInt(el.style.width):(el.offsetWidth>0)?el.offsetWidth:el.clientWidth;   
   y = (el.style && parseInt(el.style.height)>0)?parseInt(el.style.height):(el.offsetHeight>0)?el.offsetHeight:el.clientHeight;
   switch(dim) {
   case 'width':  r = x; break;
   case 'height': r = y; break;
   default: r = new Array(x,y); break; 
   }
   return r;   
}

function s2_drawmap(el) {
   mapbutton = el.replace(/^[^_]+_/,'');
   var wktnode = document.getElementById(mapbutton);
   var polygon;
   if (wktnode && wktnode.value) {
      polygon = s2_wkt_to_gmappoly(wktnode.value);
      points = polygon.getPath().getArray();
   }
   cleardialog('dialog-liner');
   var dialog = document.getElementById('dialog-liner');
   var mapcont = document.createElement('DIV');
   mapcont.id = 'mapcontainer';
   
   dialog.appendChild(mapcont)
   showdialog();
   s2_creategmap('mapcontainer');
}  

function s2_creategmap(parent) {
   var pnode = document.getElementById(parent);
   var d = pnode.parentNode;
   var tools,map,div,toolw,mapw,button;
   pnode.className = 'mapcontainer';
   var w = s2_eldims(pnode,'width');
   toolw = 180;
   mapw = w - toolw - 30;
   
   tools = s2_getmaptoolstable();
   tools.style.width = toolw+'px';
   div = document.createElement('DIV');
   div.style.width = mapw+'px';
   div.className = 'mapdiv';
   div.id = mapdiv;
   pnode.appendChild(tools);
   pnode.appendChild(div);
   
   s2_initgmap();
   s2_setmaptool('recentre');
   s2_showlocation();

   d.appendChild(document.createElement('HR'));
   button = document.createElement('BUTTON'); 
   button.className = 'dbutton';
   button.onclick = function() {hidedialog();};
   button.appendChild(document.createTextNode('Cancel'));
   d.appendChild(button);             

   button = document.createElement('BUTTON');
   button.className = 'dbutton';
   button.onclick = function() {s2_assignmap();};
   button.appendChild(document.createTextNode('OK'));
   d.appendChild(button);
   d.appendChild(document.createElement('HR'));             
}
function s2_setmaptool(t) {
   var x;
   switch(t) {
   case 'recentre': break;
   default: {
      var b = overlays['boundary'];
      if (b) {
         //map.removeOverlay(b);
         b.setMap(null);
         delete overlays['boundary'];
         points = new Array();
      }
   }break;
   } 
   x = document.getElementById(tool);
   if (x) x.className = 'tool';
   tool = t;
   if (x) {
      x = document.getElementById(tool);
      x.className = 'toolx';
   }
   s2_showtooloptions(t);
   s2_showlocation();
   return true;
}
function s2_showtooloptions(t) {
   var o = document.getElementById('tooloptions');
   var mt = document.getElementById('maptools');
   if (mt) {
      var div,h4,input,table,tbody,tr,th,td;
      var div = document.createElement('DIV');
      div.id = 'tooloptions';
      h4 = document.createElement('H4');
      h4.appendChild(document.createTextNode('Options'));
      div.appendChild(h4);
      table = document.createElement('TABLE');
      tbody = document.createElement('TBODY');
      
      switch(t) {
      case 'circle': {
         tr = document.createElement('TR');
         td = document.createElement('TD');
         td.appendChild(document.createTextNode('Radius (m)'));
         tr.appendChild(td);
         td = document.createElement('TD');
         input = document.createElement('INPUT');
         input.id = 'circle_radius';
         input.type = 'text';
         input.value = 1000;
         td.appendChild(input);
         tr.appendChild(td);
         tbody.appendChild(tr);
      }break;
      case 'square': {
         tr = document.createElement('TR');
         td = document.createElement('TD');
         td.appendChild(document.createTextNode('Grid size (m)'));
         tr.appendChild(td);
         td = document.createElement('TD');
         input = document.createElement('INPUT');
         input.id = 'square_side';
         input.type = 'text';
         input.value = 1000;
         td.appendChild(input);
         tr.appendChild(td);
         tbody.appendChild(tr);
      }break;
      }
   
      tr = document.createElement('TR');
      td = document.createElement('TD');
      td.appendChild(document.createTextNode('NGR'));
      tr.appendChild(td);
      td = document.createElement('TD');
      input = document.createElement('INPUT');
      input.id = 'ngr';
      input.type = 'text';
      input.onchange = function() {s2_movetongr(this.value);};
      td.appendChild(input);
      tr.appendChild(td);
      tbody.appendChild(tr);
   
      tr = document.createElement('TR');
      td = document.createElement('TD');
      td.appendChild(document.createTextNode('LL'));
      tr.appendChild(td);
      td = document.createElement('TD');
      input = document.createElement('INPUT');
      input.id = 'latlng';
      input.type = 'text';
      input.onchange = function() {s2_movetoll(this.value);};
      td.appendChild(input);
      tr.appendChild(td);
      tbody.appendChild(tr);
   
      table.appendChild(tbody);
      div.appendChild(table);
      if (o) mt.replaceChild(div,o);
      else mt.appendChild(div);
   }
}
function s2_getmaptoolstable() {
   var div,h4,table,tbody,div;
   div = document.createElement('DIV');
   div.className = 'maptools';
   div.id = 'maptools';
   h4 = document.createElement('H4');
   h4.appendChild(document.createTextNode('Tools'));
   div.appendChild(h4);
   table = document.createElement('TABLE');
   tbody = document.createElement('TBODY');
   tbody.appendChild(s2_getrecentre_tooltr());
   tbody.appendChild(s2_getcircle_tooltr());
   tbody.appendChild(s2_getgridsquare_tooltr());
   tbody.appendChild(s2_getpolygon_tooltr());
   tbody.appendChild(s2_geteditvertices_tooltr());
   table.appendChild(tbody);
   div.appendChild(table);
   return div; 
}
function s2_getrecentre_tooltr() {
   var tr,th,node,text,input,img,button;
   tr = document.createElement('TR');
   td = document.createElement('TD');
   button = document.createElement('BUTTON');
   button.className = 'toolx';
   img = document.createElement('IMG');
   img.src = '../images/recentre.gif';
   img.alt = 'Re-centre';
   button.appendChild(img);
   button.id = 'recentre'; 
   button.onclick = function() {s2_setmaptool(this.id);};
   td.appendChild(button);
   tr.appendChild(td);
   td = document.createElement('TD');
   td.appendChild(document.createTextNode('Recentre'));
   tr.appendChild(td);
   return tr;
}
function s2_getcircle_tooltr() {
   var tr,th,node,text,input,img,button;
   tr = document.createElement('TR');
   td = document.createElement('TD');
   button = document.createElement('BUTTON');
   button.className = 'tool';
   img = document.createElement('IMG');
   img.src = '../images/circle.gif';
   img.alt = 'Point and radius';
   button.appendChild(img);
   button.id = 'circle'; 
   button.onclick = function() {s2_setmaptool(this.id);};
   td.appendChild(button);
   tr.appendChild(td);
   td = document.createElement('TD');
   td.appendChild(document.createTextNode('Radius'));
   tr.appendChild(td);
   return tr;
}
function s2_getgridsquare_tooltr() {
   var tr,th,node,text,input,img,button;
   tr = document.createElement('TR');
   td = document.createElement('TD');
   button = document.createElement('BUTTON');
   button.className = 'tool';
   img = document.createElement('IMG');
   img.src = '../images/square.gif';
   img.alt = 'Grid Square';
   button.appendChild(img);
   button.id = 'square'; 
   button.onclick = function() {s2_setmaptool(this.id);};
   td.appendChild(button);
   tr.appendChild(td);
   td = document.createElement('TD');
   td.appendChild(document.createTextNode('Gridsquare'));
   tr.appendChild(td);
   return tr;
}
function s2_getpolygon_tooltr() {
   var tr,th,node,text,input,img,button;
   tr = document.createElement('TR');
   td = document.createElement('TD');
   button = document.createElement('BUTTON');
   button.className = 'tool';
   img = document.createElement('IMG');
   img.src = '../images/polygon.gif';
   img.alt = 'Polygon';
   button.appendChild(img);
   button.id = 'polygon'; 
   button.onclick = function() {s2_setmaptool(this.id);};
   td.appendChild(button);
   tr.appendChild(td);
   td = document.createElement('TD');
   td.appendChild(document.createTextNode('Polygon'));
   tr.appendChild(td);
   return tr;
}
function s2_geteditvertices_tooltr() {
   var tr,th,node,text,input;
   tr = document.createElement('TR');
   td = document.createElement('TD');
   td.className = 's2gmaptdbutton';
   input = document.createElement('INPUT');
   input.type = 'checkbox';
   input.id = 's2gmapcheck';
   input.className = 's2gmapcheck';
   input.onclick = function() {s2_editpoly(this.checked);};
   
   td.appendChild(input);
   tr.appendChild(td);
   td = document.createElement('TD');
   td.appendChild(document.createTextNode('Edit Poly'));
   tr.appendChild(td);
   return tr;
}
function s2_editpoly(makeeditable) {
   var p,vc,v1,vn;
   if (overlays['boundary']) {
      p = overlays['boundary'];
      if (makeeditable) {
         //vc = p.getVertexCount();
         //v1 = p.getVertex(0);
         //vn = p.getVertex(vc-1);
         //if (v1 != vn) p.insertVertex(vc,v1); 
         p.setEditable(true);
      }
      else p.setEditable(false);
   }
}

function s2_moveto(point) {
   map.setCenter(point);
   return true;
}
function s2_movetopolycenter() {
   var c = s2_getpolycenter();
   s2_moveto(c);
   return c;
}
function s2_movetongr(gridref) {
   var gr,ll,ctr;
   if (gridref) {
      gr = s2_gr_from_ngrstring(gridref);
      ll = s2_gr_to_ll(gr);
      ctr = s2_ll_to_gmp(ll);
      s2_moveto(ctr);
   }
   return ctr;
}
function s2_gr_from_ngrstring(gridref) {
   var bits,sq,en,e,n,sx,res,ngr,gr,ll,ctr;
   if (gridref) {
      gridref = gridref.toUpperCase();
      gridref = gridref.replace(/(\w{2})(\d+)(\w*)/,'$1-$2-$3');
      bits = gridref.split(/\-/);
      sq = bits[0];
      en = bits[1];
      e = parseInt(en.substr(0,Math.floor(en.length/2)));
      n = parseInt(en.substr(Math.floor(en.length/2),Math.floor(en.length/2)));
      sx = bits[2];
      res = Math.pow(10,(5 - Math.floor(en.length/2)));
      e *= res;
      n *= res;
      switch(sx.length) {
      case 1: {
         // DINTY tetrad notation 2K
         var tn,tx,ty;
         tn = sx.charCodeAt(0) - 65;
         tn = (tn > 14)?(tn-1):tn;
         ty = (tn%5) * 2 * (res/10);
         tx = Math.floor(tn/5) * 2 * (res/10);
         res /= 5;
         e += tx;
         n += ty;
      }break;
      case 2: {
         // NE,SE,SW,NW notation 5K
         var tx = (sx.substring(1,1) == 'E')?5:0;
         tx *= (res/10);
         var ty = (sx.substring(0,1) == 'N')?5:0;
         ty *= (res/10);
         res /= 2;
         e += tx;
         n += ty;
      }break;
      }
      e += (res/2);
      n += (res/2);
      ngr = new NationalGridReference(sq,e,n,res);
      gr = s2_ngr_to_gr(ngr);      
   }
   return gr; 
}
   
function s2_movetoll(llstring) {
   var bits,lat,lng,ctr;
   if (llstring && llstring.match(/\-*\d+\s*\,\s*\-*\d+/)) {
      bits = llstring.split(/\s*\,\s*/);
      lng = parseInt(bits[0]);
      lat = parseInt(bits[1]);
      ctr = new google.maps.LatLng(lng,lat);
      s2_moveto(ctr);                                                  
   }
}


function s2_getpolybounds(poly) {
   var px = (poly)?poly.getPath().getArray():points;
   var p,c,cx,cy,maxx,maxy,minx,miny,sw,ne; 
   for (p in px) {
      c = px[p];
      maxx = (maxx)?Math.max(maxx,c.lng()):c.lng();
      minx = (minx)?Math.min(minx,c.lng()):c.lng();
      maxy = (maxy)?Math.max(maxy,c.lat()):c.lat();
      miny = (miny)?Math.min(miny,c.lat()):c.lat();
   }
   sw = new google.maps.LatLng(miny,minx);
   ne = new google.maps.LatLng(maxy,maxx);
   c = new google.maps.LatLngBounds(sw,ne);
   return c; 
}
function s2_getpolycenter(poly) {
   var px = (poly)?poly.getPath().getArray():points;
   var p,c,cx,cy,maxx,maxy,minx,miny; 
   for (p in px) {
      c = px[p];
      maxx = (maxx)?Math.max(maxx,c.lng()):c.lng();
      minx = (minx)?Math.min(minx,c.lng()):c.lng();
      maxy = (maxy)?Math.max(maxy,c.lat()):c.lat();
      miny = (miny)?Math.min(miny,c.lat()):c.lat();
   }
   cx = minx + ((maxx-minx)/2);
   cy = miny + ((maxy-miny)/2);
   c = new google.maps.LatLng(cy,cx);
   return c; 
}
function s2_initgmap() {
   var md = document.getElementById(mapdiv); 
   try {
      var myLatLng = new google.maps.LatLng(53.38, -1.46);
      var myOptions = {
         zoom: 12,
         center: myLatLng,
         panControl: false,
         zoomControl: true,
         mapTypeControl: true,
         scaleControl: true,
         streetViewControl: true,
         overviewMapControl: true,
         mapTypeId: google.maps.MapTypeId.SATELLITE
      };
      map = new google.maps.Map(md,myOptions);
      google.maps.event.addListener(map, 'click', function(e) {s2_clickmap(e);});
      if (points && points.length > 1) {
         s2_drawpoly(points);
         s2_fittomap();
      } else if (points && points.length == 1) {
         s2_drawpointmarker(points[0]);
      }
      if (paths && paths.length > 0) {
         s2_drawmultipoly(paths);
      }
      //s2_showzoom();
   } catch (e) {
      md.appendChild(document.createTextNode('Google Maps: Initialise failed.'));
   }
}

function s2_zoomto(znum) {
   try {
      map.setOptions({"zoom":znum});                  
   } catch (e) {}
}
function s2_maptype(type) {
   try {
      map.setOptions({"mapTypeId":google.maps.MapTypeId.SATELLITE});
   } catch (e){}
}  
function s2_getres() {
   var z,r;
   z = map.getZoom();
   r = (z>=18)?10:((z>=16)?100:((z>=12)?1000:((z>=8)?10000:100000)));
   return r;
}   
function s2_clickmap(e) {
   var rad;
   var pt = e.latLng;
   if (pt) {
      switch(tool) {
      case 'recentre': {
         s2_moveto(pt);
         s2_showlocation();
      }break;
      case 'circle': {
         rad = parseInt(document.getElementById('circle_radius').value);
         s2_drawcircle(pt,rad);
      }break;
      case 'square': {
         rad = parseInt(document.getElementById('square_side').value);
         s2_drawgridsquare(pt,rad);
      }break;
      case 'polygon': {
         points.push(pt);
         s2_drawpoly(points);
      }break;
      case 'point': {
         var gr,ngr,a;
         a = s2_getres();
         points = new Array(pt);
         gr = s2_pointtogrstring(pt,a);
         s2_drawpointmarker(pt,gr,null)
         ngr = document.getElementById('ngr');
         if (ngr) ngr.value = gr;
      }break;
      }         
   }
   return true;
}  
function s2_showlocation(ctr) {
   var ll,gr,ngr,res,z;
   if (!ctr) ctr = map.getCenter();
   switch(tool) {
   case 'circle': {
      res = parseInt(document.getElementById('circle_radius').value);
      res *= 2;
   }break;
   case 'square': {
      res = parseInt(document.getElementById('square_side').value);
   }break;
   default: {
      res = s2_getres();
   }break;
   }
   ll = s2_gmp_to_ll(ctr);
   gr = s2_ll_to_gr(ll);
   ngr = s2_gr_to_ngr(gr);
   ngr.accuracy = res;
   ll.accuracy = res;
   
   try { document.getElementById('ngr').value = ngr.asString(); } catch(e) {}
   try { document.getElementById('latlng').value = ll.asString(); } catch(e) {}
}
function s2_pointtogrstring(p,a) {
   var ll,gr,ngr,res,z,grs;
   try {
      ll = s2_gmp_to_ll(p);
      gr = s2_ll_to_gr(ll);
      gr.accuracy = a;
      ngr = s2_gr_to_ngr(gr);
      grs = ngr.asString();
   } catch (e) {}
   return grs;
}
function s2_drawpoly(points) {
   var p,c;
   if (overlays['boundary']) {
      p = overlays['boundary'];
      p.setMap(null);
   }
   p = new google.maps.Polygon({
      paths: points,
      strokeColor: "#FF0000",
      strokeOpacity: 0.8,
      strokeWeight: 0.5,
      fillColor: "#FF0000",
      fillOpacity: 0.35,
      clickable: true
   });
   google.maps.event.addListener(p, 'click', function(e) {s2_clickmap(e);});
   p.setMap(map);
   overlays['boundary'] = p;
   c = s2_getpolycenter(p);
   s2_showlocation(c);
}
function s2_drawcircle(ctr,rad) {
   var c,p;
   if (overlays['boundary']) {
      c = overlays['boundary'];
      c.setMap(null);
   }
   c = new google.maps.Circle({
      strokeColor: "#FF0000",
      strokeOpacity: 0.8,
      strokeWeight: 0.5,
      fillColor: "#FF0000",
      fillOpacity: 0.35,
      center: ctr,
      radius: rad,
      clickable: true
   });
   c.setMap(map);
   points = s2_getcirclevertices(c);
   google.maps.event.addListener(c, 'click', function(e) {s2_clickmap(e);});
   c.setMap(map);
   overlays['boundary'] = c;
   s2_showlocation(ctr);
}

function s2_drawgridsquare(clicked,res) {
   var p,ll,gr,ngr;
   if (overlays['boundary']) {
      p = overlays['boundary'];
      p.setMap(null);
   }
   ll = s2_gmp_to_ll(clicked);
   gr = s2_ll_to_gr(ll);
   ngr = s2_gr_to_ngr(gr);
   ngr.easting = ngr.easting - (ngr.easting % res) + (0.5 * res);
   ngr.northing = ngr.northing - (ngr.northing % res) + (0.5 * res);
   ngr.accuracy = res;
   points = s2_getgridsquarefromgr(gr,res);
   
   p = new google.maps.Polygon({
      paths: points,
      strokeColor: "#FF0000",
      strokeOpacity: 0.8,
      strokeWeight: 0.5,
      fillColor: "#FF0000",
      fillOpacity: 0.35,
      clickable: true
   });
   google.maps.event.addListener(p, 'click', function(e) {s2_clickmap(e);});

   p.setMap(map);
   s2_showlocation(clicked);
   overlays['boundary'] = p;
}

var ztime;
function s2_showzoom() {
   google.maps.event.addListener(map, 'zoom_changed', function() {
      // Get the current bounds, which reflect the bounds before the zoom.
      var zrect = new google.maps.Rectangle();
      zrect.setOptions({
         strokeColor: "#0000FF",
         strokeOpacity: 0.8,
         strokeWeight: 0.5,
         fillColor: "#0000FF",
         fillOpacity: 0.35,
         bounds: map.getBounds()
      });
      zrect.setMap(map);
      overlays['zoom'] = zrect;
      ztime = window.setTimeout("s2_hidezoom();",1000);   
   });
}
function s2_hidezoom() {
   var zrect = overlays['zoom'];
   if (zrect) { 
      zrect.setMap(null);
      zrect = null;
      ztime = null;
   }
   delete overlays['zoom'];
}
                                  
function s2_drawpointmarker(point,title,info) {
   var o;
   while(overlays.length > 0) {
      o = overlays.shift();
      o.setMap(null);
   }
   var infowindow = new google.maps.InfoWindow({
      content: info
   });

   var marker = new google.maps.Marker({
      position: point,
      title: title
   });
   
   google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
   });
   marker.setMap(map);
   overlays.push(marker);
}

function s2_showoverlays() {
   if (overlays) {
      for (i in overlays) {
         overlays[i].setMap(map);
      }
   }
}

// Deletes all markers in the array by removing references to them
function s2_deleteoverlays() {
   if (overlays) {
      for (i in overlays) {
         overlays[i].setMap(null);
      }
      overlays.length = 0;
   }
}

function s2_gmp_to_ll(gmp) {
   var ddc = new DegreesDecimalCoordinate(gmp.lat(),gmp.lng());
   return ddc;
}
function s2_ll_to_gr(ll) {
   var gr = wgs84_to_osgb36(ll);
   return gr;
}
function s2_gr_to_ngr(gr) {
   var ngr = find_gridsquare(gr);
   return ngr;
}
function s2_ngr_to_gr(ngr) {
   var gr = conv_ngr_to_ings(ngr);
   return gr; 
}
function s2_gr_to_ll(gr) {
   var ddc = conv_uk_ings_to_ll(gr);
   return ddc;
}
function s2_ll_to_gmp(ll) {
   var gmp = new google.maps.LatLng(ll.lat(),ll.lng());
   return gmp;
}

function s2_getgridsquarefromgr(gr,acc) {
   var x,y,xi,yi,ll;
   x = gr.easting - (gr.easting % acc);
   y = gr.northing - (gr.northing % acc);
   var points = new Array();   
   for (xi=0;xi<=1;xi++) {
      for (yi=0;yi<=1;yi++) {
         gr = new GridReference((x + (xi*acc)),y + (((xi+yi)%2)*acc));
         gr.accuracy = acc;
         ll = s2_gr_to_ll(gr);                  
         points.push(s2_ll_to_gmp(ll));                  
      }
   }
   return points;            
}
function s2_getgridsquarecenter(gr,acc) {
   var x,y,xi,yi,ll;
   x = gr.easting - (gr.easting % acc);
   y = gr.northing - (gr.northing % acc);
   gr = new GridReference((x + (0.5*acc)),(y + (0.5*acc)));
   gr.accuracy = acc;
   ll = s2_gr_to_ll(gr);                  
   return s2_ll_to_gmp(ll);            
}
function s2_getcirclevertices(c) {
   var ctr,rad,seg,ll,gr,ngr,points,vertex;
   ctr = c.getCenter();
   rad = c.getRadius();
   seg = Math.floor(360/vertices) * (Math.PI/180);
   points = new Array();
   ll = s2_gmp_to_ll(ctr);
   gr = s2_ll_to_gr(ll);
   cgr = new GridReference(gr.easting,gr.northing);
   for (vertex=0;vertex<=vertices;vertex++) {
      gr.easting = cgr.easting + (rad * Math.sin(seg*vertex));
      gr.northing = cgr.northing + (rad * Math.cos(seg*vertex));
      ll = s2_gr_to_ll(gr);
      points[vertex] = s2_ll_to_gmp(ll);
   }
   return points;      
} 

function s2_wkt_to_gmappoly(wkt) {
   var pairs,p,pt,xy,ll,polygon;
   polygon = null;
   try {
      ll = new google.maps.LatLng(0,0);  
      if (wkt) {
         points = new Array();
         wkt = wkt.replace(/^[^\(]+\(+/,'');
         wkt = wkt.replace(/\)+$/,'');
         pairs = wkt.split(/\,/);
         for(p in pairs) {
            pt = pairs[p];
            xy = pt.split(/\s+/);
            ll = new google.maps.LatLng(xy[1],xy[0]);
            points.push(ll);
         }
         polygon = new google.maps.Polygon({
            paths: points,
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 0.5,
            fillColor: "#FF0000",
            fillOpacity: 0.35,
            clickable: true
         });
      }
   } catch(err) {
      // add something here.   
   }
   return polygon;
}
function s2_poly_getname(polygon) {
   var wkt,p,ddc,gr,ngr,ptext,pname,parea,path,zoom;
   try {
      if (polygon instanceof google.maps.Circle) {
         path = s2_getcirclevertices(polygon);
         polygon = new google.maps.Polygon({
            paths: path,
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 0.5,
            fillColor: "#FF0000",
            fillOpacity: 0.35,
            clickable: true
         });
         polygon.setMap(map);
      }
      p = s2_getpolycenter(polygon);
      ddc = new DegreesDecimalCoordinate(p.lat(),p.lng());
      gr = s2_ll_to_gr(ddc);
      gr.accuracy = 10;
      gr.easting = Math.floor(gr.easting/gr.accuracy)*gr.accuracy;
      gr.northing = Math.floor(gr.northing/gr.accuracy)*gr.accuracy;
      ngr = s2_gr_to_ngr(gr);
      path = polygon.getPath().getArray();
      parea = s2gmapv3getarea(path);
      if (parea > 900000) ptext = Math.round(parea/100000)/10+' km� @ '+ngr.asString();
      else if (parea > 9000) ptext = Math.round(parea/1000)/10+' ha @ '+ngr.asString();  
      else ptext = Math.round(parea*10)/10+' m� @ '+ngr.asString();
   } catch(err) {
      ptext = 'Google Maps not available in offline mode'; 
   }
   return ptext;
}
function s2_assignmap() {
   var wkt,p,ddc,gr,ngr,ptext,pname,parea,zoom,polygon;
   var mappoly = document.getElementById(mapbutton);        
   var mapname = document.getElementById(mapbutton+'_Name');
   if (!mapname) mapname = document.getElementById('Link_'+mapbutton); 
   zoom = map.getZoom();
   if (points && points.length > 1) {
      wkt = 'POLYGON((';
      for (pcount=0;pcount<points.length;pcount++) {
         p = points[pcount];
         wkt += p.lng() + ' ' + p.lat() + ',';
      }
      p = points[0];
      wkt += p.lng() + ' ' + p.lat() + '))';
      polygon = new google.maps.Polygon({
         paths: points,
         strokeColor: "#FF0000",
         strokeOpacity: 0.8,
         strokeWeight: 0.5,
         fillColor: "#FF0000",
         fillOpacity: 0.35,
         clickable: true
      });
      ptext = s2_poly_getname(polygon);
   } else if (points && points.length > 0) {
      p = points[0];
      ddc = s2_gmp_to_ll(p);
      gr = s2_ll_to_gr(ddc);
      gr.accuracy = (zoom >= 12)?100:(zoom>=8)?1000:(zoom>=5)?10000:100000;
      gr.easting = Math.floor(gr.easting/gr.accuracy)*gr.accuracy;
      gr.northing = Math.floor(gr.northing/gr.accuracy)*gr.accuracy;
      ngr = find_gridsquare(gr);
      wkt = 'POINT('+ p.lng() + ' ' + p.lat() + ')';
      ptext = ngr.asString(); 
   }
   mappoly.value = wkt;
   // execute onchange if the function is defined. 
   // no catch action because it just means the onchange function is not defined
   try {mappoly.onchange();} catch (e) {}
   if (mapname) {
      if (mapname.nodeName == 'INPUT') mapname.value = ptext;
      else mapname.replaceChild(document.createTextNode(ptext),mapname.firstChild);
   }
   //points = new Array();
   hidedialog();
   return true;
}         