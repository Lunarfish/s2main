<?php
function httpgetheader($header,$name) {
   $headers = array();
   foreach (explode("\r\n", $header) as $i => $line)
      if ($i === 0)
         $headers['http_code'] = $line;
      else {
         list ($key, $value) = explode(': ', $line);
         $headers[$key] = $value;
      }
   return (isset($headers[$name]))?$headers[$name]:null;
}
?>