<?php

// decode JSON string to PHP object
$decoded = json_decode($_GET['json']);
/*
print_r($decoded);
exit();
*/
// do something with data here
		
// create response object
/*
$json = array();
$json['errorsNum'] = 2;
$json['error'] = array();
$json['error'][] = 'Wrong email!';
$json['error'][] = 'Wrong hobby!';
*/
switch($decoded->queryType) {
case 'measurement': {
   $sql = "SELECT DISTINCT mq.SHORT_NAME as qualifier,mt.SHORT_NAME as type,mu.SHORT_NAME as unit
   FROM MEASUREMENT_TYPE [mt] 
   INNER JOIN MEASUREMENT_QUALIFIER [mq] ON mt.MEASUREMENT_TYPE_KEY = mq.MEASUREMENT_TYPE_KEY 
   INNER JOIN MEASUREMENT_UNIT [mu] ON mu.MEASUREMENT_TYPE_KEY = mt.MEASUREMENT_TYPE_KEY
   ORDER BY type,qualifier,unit;";
}break;
case 'species': {
   $sql = "SELECT DISTINCT itn2.ACTUAL_NAME as scientific,itn2.COMMON_NAME as english,itn2.AUTHORITY as authority, 
   ns.RECOMMENDED_TAXON_VERSION_KEY as tvkey, ns.RECOMMENDED_TAXON_LIST_ITEM_KEY as tlikey 
   FROM INDEX_TAXON_NAME [itn] 
   INNER JOIN TAXON_LIST_ITEM [tli] ON itn.TAXON_LIST_ITEM_KEY = tli.TAXON_LIST_ITEM_KEY 
   INNER JOIN NAMESERVER [ns] ON tli.TAXON_VERSION_KEY = ns.INPUT_TAXON_VERSION_KEY 
   INNER JOIN INDEX_TAXON_NAME [itn2] ON itn2.TAXON_LIST_ITEM_KEY = ns.RECOMMENDED_TAXON_LIST_ITEM_KEY
   WHERE itn.ACTUAL_NAME LIKE '%$decoded->value%' OR 
   itn.COMMON_NAME LIKE '%$decoded->value%' ORDER BY itn2.ACTUAL_NAME";
}break;
}

$dsn = 'NBNTest';
$username = 'NBNUser';
$password = 'NBNPassword';

$sqlconnect=odbc_pconnect($dsn,$username,$password);
$result=odbc_exec($sqlconnect, $sql);
$json = array();
$json['rows'] = array();
while (odbc_fetch_into($result,$data)) {
   $row = array();
   switch($decoded->queryType) {
   case 'measurement': {
      $row['qualifier'] = $data[0];
      $row['type'] = $data[1];
      $row['unit'] = $data[2];
   }break;
   case 'species': {
      $row['scientific'] = $data[0];
      $row['english'] = $data[1];
      $row['authority'] = $data[2];
      $row['tvkey'] = $data[3];
      $row['tlikey'] = $data[4];
   }break;
   }
   $json['rows'][] = $row;
}

// encode array $json to JSON string
$encoded = json_encode($json);

// send response back to index.html
// and end script execution
die($encoded);

?>
