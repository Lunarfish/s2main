var columns = null;
var wmslayer = 0;
var wfs = 0;
var wfsprogress;
var wfszoom = 0;
var wfspolygons = 0;
var wfsmarkers = 1;
var drawmarkers = 1;
var wfszoomlimit = 12;
var wfssuppress = 0;
var map,wkt,x,y;
var points = new Array();
var polygon;
var polypoints;
var marker,easting,northing,latitude,longitude;
var precision = 10000;
var ddc,gr,ngr,point,action,center;
var vertex,ctr,deg;
var tileCountry;
function s2_eldims(el,dim) {
   var r,x,y;
   x = (parseInt(mapoutline.style.width)>0)?parseInt(mapoutline.style.width):(mapoutline.offsetWidth>0)?mapoutline.offsetWidth:mapoutline.clientWidth;   
   y = (parseInt(mapoutline.style.height)>0)?parseInt(mapoutline.style.height):(mapoutline.offsetHeight>0)?mapoutline.offsetHeight:mapoutline.clientHeight;
   switch(dim) {
   case 'width':  r = x; break;
   case 'height': r = y; break;
   default: r = new Array(x,y); break; 
   }
   return r;   
}
function maplayout() {
   var maptools,mapcontainer,mapoutline,mapsave;
   mapoutline = document.getElementById('mapoutline');
   mapcontainer = document.getElementById('mapcontainer'); 
   maptools = document.getElementById('maptools');
   var x = s2_eldims(mapoutline,'width');
   var t = 150;
   mapcontainer.style.width = Math.floor(x - t - 50) + 'px';
   maptools.style.width = Math.floor(t) + 'px';
   map.checkResize();
   fitToMap();
   return true;    
}
function mapShrinkOrExpand(button) {
   var name = button.firstChild.alt;
   var maptools,mapcontainer,mapoutline,mapdiv,maplabels;
   mapoutline = document.getElementById('mapoutline');
   mapcontainer = document.getElementById('mapcontainer');
   maptools = document.getElementById('maptools'); 
   maplabels = document.getElementById('maplabels');
   mapdiv = document.getElementById('map');
   var x = (mapoutline.style.width)?parseInt(mapoutline.style.width):mapoutline.offsetWidth;
   var t = 250;
   var img;
   switch (name) {
   case 'shrink': {
      img = new Image();
      img.src = '../images/expand2.gif';
      img.alt = 'expand';
      button.replaceChild(img,button.firstChild);
      mapcontainer.style.height = '150px';
      mapcontainer.style.width = t+'px';       
      maptools.style.display = 'none';
      maptools.style.visibility = 'hidden';
      maplabels.style.display = 'none';
      maplabels.style.visibility = 'hidden';
      mapdiv.style.height = '150px';
      map.disableGoogleBar();
   }break;
   case 'expand': {
      img = new Image();
      img.src = '../images/shrink2.gif';
      img.alt = 'shrink';                          
      button.replaceChild(img,button.firstChild);
      maptools.style.display = 'block';
      maptools.style.visibility = 'visible';
      maplabels.style.display = 'block';
      maplabels.style.visibility = 'visible';
      mapdiv.style.height = '350px';
      mapcontainer.style.height = 'auto';
      map.enableGoogleBar();
      maplayout();         
   }break
   }
   fitToMap();      
   
   return true;   
}
function fitToMap() {
   if (map && polygon) {
      map.checkResize();
      var bnd = polygon.getBounds();
      var ctr = bnd.getCenter();
      map.setCenter(ctr);
      map.setZoom(map.getBoundsZoomLevel(bnd));  
   }
}
function drawWKT (wkt) {
   if (polygon) map.removeOverlay(polygon);
   polygon = s2_wkt_to_gmappoly(wkt);
   map.addOverlay(polygon);
   fitToMap();
   return true;
}
function drawPolygon (points,closed) {
   if (polygon) map.removeOverlay(polygon);
   closed = (closed == null)?1:closed;
   if (closed) {
      var polypoints = points.concat(new Array(points[0]));
      polygon = new GPolygon(polypoints,'#330000',1,1,'#ff0000',0.5);
   } else {
      polygon = new GPolyline(points,'#330000',1,1);
   }
   map.addOverlay(polygon);
   return true;
}
function appendPolygon (point,closed) {
   if (polygon) map.removeOverlay(polygon);
   if (point) points = points.concat(new Array(point));
   closed = (closed == null)?1:closed;
   if (closed) {
      polypoints = points.concat(new Array(points[0]));
      polygon = new GPolygon(polypoints,'#330000',1,1,'#ff0000',0.5);
      GEvent.addListener(polygon, "click", function(point) {clickAction(point);});
   } else {
      polygon = new GPolyline(points,'#330000',1,1);
   }
   map.addOverlay(polygon);
   //fitToMap();
   return true;
}
function getSquare(gr,acc) {
   var x,y,xi,yi;
   x = gr.easting - (gr.easting % acc);
   y = gr.northing - (gr.northing % acc);
   var points = new Array();   
   for (xi=0;xi<=1;xi++) {
      for (yi=0;yi<=1;yi++) {
         gr = new GridReference((x + (xi*acc)),y + (((xi+yi)%2)*acc));
         gr.accuracy = acc;
         ddc = conv_uk_ings_to_ll(gr);                  
         points.push(new GPoint(ddc.longitude,ddc.latitude));                  
      }
   }
//alert(JSON.stringify(points));
   return points;            
}
function getSquareCenter(gr,acc) {
   var x,y,xi,yi;
   x = gr.easting - (gr.easting % acc);
   y = gr.northing - (gr.northing % acc);
   gr = new GridReference((x + (0.5*acc)),(y + (0.5*acc)));
   gr.accuracy = acc;
   ddc = conv_uk_ings_to_ll(gr);                  
   return new GPoint(ddc.longitude,ddc.latitude);            
}
function drawPoint(point) {
   point = (point == null)?points[0]:point;
   if (marker) map.removeOverlay(marker);
   var pll = precision * 10;
   latitude = Math.floor(point.y * pll)/pll;
   longitude = Math.floor(point.x * pll)/pll;
   ddc = new DegreesDecimalCoordinate(point.y,point.x);
   gr = wgs84_to_osgb36(ddc);
   ngr = find_gridsquare(gr);
   var acc = parseInt(document.getElementById('acc').value); 
   ngr.easting = ngr.easting - (ngr.easting % acc) + (0.5 * acc);
   ngr.northing = ngr.northing - (ngr.northing % acc) + (0.5 * acc);
   ngr.accuracy = acc;
   easting = formatToXPlaces(ngr.easting,5,0); 
   northing = formatToXPlaces(ngr.northing,5,0);
   points = getSquare(gr,acc);
   var center = getSquareCenter(gr,acc);
   marker = new GMarker(center);
   updatePoint(ngr);
   GEvent.addListener(marker, "click", function() {
      this.openInfoWindowHtml("<h4>Location</h4>" +
                  "<table style='width:250px;'><tbody>" + 
                  "<tr><th class='lined' style='padding: 5px;'>Lat Lon</th><td class='lined'>" + ddc.asString() + "</td></tr>" +
                  "<tr><th class='lined' style='padding: 5px;'>Grid Ref</th><td class='lined' style='text-transform:uppercase;'>" + ngr.asString() + "</td></td>" + 
                  "</tbody></table>");
   });
   drawPolygon(points,true);
   map.addOverlay(marker);
   fitToMap();
   return true;
}
function drawCircle(point,radius,vertices) {
   if (polygon) map.removeOverlay(polygon);
   ddc = new DegreesDecimalCoordinate(point.y,point.x);
   ctr = wgs84_to_osgb36(ddc);
   seg = Math.floor(360/vertices) * (Math.PI/180);
   points = new Array();
   gr = new GridReference(0,0);
   ngr = find_gridsquare(ctr);
   updatePoint(ngr);
   for (vertex=0;vertex<=vertices;vertex++) {
      gr.easting = ctr.easting + (radius * Math.sin(seg*vertex));
      gr.northing = ctr.northing + (radius * Math.cos(seg*vertex));
      ddc = conv_uk_ings_to_ll(gr);
      points[vertex] = new GPoint(ddc.longitude,ddc.latitude);
   }
   polygon = new GPolygon(points,'#330000',1,1,'#ff0000',0.5);
   map.addOverlay(polygon);
   fitToMap();
}
function deletePolygon() {
   points = new Array();
   map.removeOverlay(polygon);
}
function s2_mapload() {
   // Create tile layers
   //maplayout();
   if (GBrowserIsCompatible()) {
      map = new GMap2(document.getElementById("map"));
      GEvent.addListener(map, "moveend", function() {
         center = map.getCenter();
         ddc = new DegreesDecimalCoordinate(center.y,center.x);
         gr = wgs84_to_osgb36(ddc);
         ngr = find_gridsquare(gr);
         if (wfs && wmslayer && wfszoom && drawmarkers) {
            if (!wfssuppress) wfs.refreshInBoundsWithBuffer();
            else wfssuppress = 0;
         }
         return true;
      });
      
      GEvent.addListener(map, "click", function(overlay,point) {
         if (point) {
            ddc = new DegreesDecimalCoordinate(point.y,point.x);
            gr = wgs84_to_osgb36(ddc);
            ngr = find_gridsquare(gr);
            action = document.getElementById('action').value;
            clickAction(point);
         }
         if (wfs && wmslayer && wfszoom && drawmarkers) wfs.refreshInBoundsWithBuffer();
         return true;
      });
      GEvent.addListener(map,"zoomend", function(oldLevel,newLevel) {
         wfszoom = (newLevel > wfszoomlimit);
         return true;
      });
      x = getQueryVariable('x');
      y = getQueryVariable('y');
      if (x && y) {
         gr = new GridReference(x,y);
         ddc = conv_uk_ings_to_ll(gr);
         x = ddc.longitude;
         y = ddc.latitude;
         ctr = new GPoint(x,y);
         points[0] = ctr;
      } else {
         x = -0.44;
         y = 53.69;
         ctr = new GPoint(x,y);
      }
      wkt = getQueryVariable('wkt');
      if (wkt) {
         wkt = unescape(wkt);
         ctr = getWKTPoints(wkt);
      }
      if (points.length > 1) {
         var z1;
         var b1 = new GBounds(points);
         var b2 = new GLatLngBounds(new GLatLng(b1.minY,b1.minX),new GLatLng(b1.maxY,b1.maxX));
         z1 = map.getBoundsZoomLevel(b2);
      } else z1 = 10;
      map.setCenter(new GLatLng(ctr.y,ctr.x),z1);
      map.addControl(new GSmallZoomControl3D());
      map.enableGoogleBar();
      map.enableContinuousZoom();
      if (points.length == 1) drawPoint(points);
      else if (points.length > 1) drawPolygon(points);
      map.getMapTypes().length = 0;
      map.addMapType(G_NORMAL_MAP);
      map.addMapType(G_HYBRID_MAP);
      map.addMapType(G_PHYSICAL_MAP);
      map.setMapType(G_NORMAL_MAP);
      map.addControl(new GMenuMapTypeControl());
   } 
}
function clickAction(point) {
   action = document.getElementById('action').value;
   switch (action) {
      case 'recentre': {
         map.setCenter(point);
      } break;
      case 'polygon': {
         showMarkers(false);
         appendPolygon(point,1);
      }break;
      case 'polyline': {
         showMarkers(false);
         appendPolygon(point,0);
      }break;
      case 'circle': {
         showMarkers(false);
         drawCircle(point,document.getElementById('acc').value,36);
      }break;
      case 'point': {
         showMarkers(false);
         drawPoint(point);
      }break;
   }
}
function addLayer(featureType) {
   if (featureType) {
      /*
         limit at which markers become visible depends on the type of 
         data. gridded thematic data needs to be rendered without 
         markers longer so as not to obscure the thematic.
      */
      
      if (featureType.indexOf('DTI_') > -1) wfszoomlimit = 12;
      else if (featureType.indexOf('YH_') > -1) wfszoomlimit = 10;
      else if (featureType.indexOf('HSR_') > -1) wfszoomlimit = 12;
      else if (featureType.indexOf('NEYEDCCoverage') > -1) wfszoomlimit = 1;
      else wfszoomlimit = 12;
      
      wmslayer = 1;
      wfszoom = (map.getZoom()>wfszoomlimit);
      tileCountry= new GTileLayer(new GCopyrightCollection(""),1,17);
      tileCountry.myLayers = featureType;
      /*
      surprise surprise! internet explorer renders the WMS layers as 
      non-transparent by default so you can't see the map backgrounds.
      you have to test for internet explorer opacity filters by having 
      something with a filter defined on it. so i stuck a 100% opaque 
      filter on the logo
      */
      if (document.getElementById('logo').filters) {
         tileCountry.myOpacity=0.6;
         tileCountry.getOpacity=customOpacity; 
      }
      
      //tileCountry.myBaseURL='http://www.humber-edc.org.uk/wfsrefer.php?vs=' + getQueryVariable('vs');
      tileCountry.myBaseURL = '../wfsrefer.php?vs=' + getQueryVariable('vs');
      //tileCountry.myBaseURL='http://localhost/hedc1/wfsrefer.php?vs=' + getQueryVariable('vs');
      tileCountry.getTileUrl=CustomGetTileUrl;
   
      var layer1=[G_NORMAL_MAP.getTileLayers()[0],tileCountry];
      var custommap1 = new GMapType(layer1, G_NORMAL_MAP.getProjection(), "Map", G_NORMAL_MAP);
      //var layer2=[G_SATELLITE_MAP.getTileLayers()[0],tileCountry];
      //var custommap2 = new GMapType(layer2, G_SATELLITE_MAP.getProjection(), "Satellite", G_SATELLITE_MAP);
      var layer3=[G_SATELLITE_MAP.getTileLayers()[0],G_HYBRID_MAP.getTileLayers()[1],tileCountry];
      var custommap3 = new GMapType(layer3, G_SATELLITE_MAP.getProjection(), "Hybrid", G_SATELLITE_MAP);
      var layer4=[G_PHYSICAL_MAP.getTileLayers()[0],tileCountry];
      var custommap4 = new GMapType(layer4, G_PHYSICAL_MAP.getProjection(), "Terrain", G_PHYSICAL_MAP);
      map.getMapTypes().length = 0;
   
      map.addMapType(custommap1);
      //map.addMapType(custommap2);
      map.addMapType(custommap3);
      map.addMapType(custommap4);
      
      map.setMapType(custommap1);
      addWFS(featureType,tileCountry.myBaseURL);
   } else {
      map.getMapTypes().length = 0;
   
      map.addMapType(G_NORMAL_MAP);
      //map.addMapType(G_SATELLITE_MAP);
      map.addMapType(G_HYBRID_MAP);
      map.addMapType(G_PHYSICAL_MAP);
      
      map.setMapType(G_NORMAL_MAP);
      wfs = null;
      wmslayer = 0;
   }
   updateLegend(featureType);
}
function addWFS(featureType,baseURL) {
   typename = featureType;
   buffer = 0;
   wfs = new GMapWFS(map, baseURL, typename, buffer); 
   if (wfszoom && drawmarkers) wfs.refreshInBoundsWithBuffer();
}
var p,pcount,wkt;
function returnVal() {
   if (points.length > 1) {
      returnPolygon();
   } else if (points.length == 1) {
      returnPoint();            
   } else {
      alert("A usable area has not been defined.");
   }
   window.close();
}
function returnPolygon() {
   wkt = 'POLYGON((';
   for (pcount=0;pcount<points.length;pcount++) {
      p = points[pcount];
      ddc = new DegreesDecimalCoordinate(p.y,p.x);
      gr = wgs84_to_osgb36(ddc);
      wkt += gr.easting + ' ' + gr.northing + ',';
   }
   p = points[0];
   ddc = new DegreesDecimalCoordinate(p.y,p.x);
   gr = wgs84_to_osgb36(ddc);
   wkt += gr.easting + ' ' + gr.northing + '))';
   window.opener.collectWKT(wkt);
   return true;
}
function returnPoint() {
   p = points[0];
   var zoom = map.getZoom();
   
   ddc = new DegreesDecimalCoordinate(p.y,p.x);
   gr = wgs84_to_osgb36(ddc);
   gr.accuracy = (zoom >= 12)?100:(zoom>=8)?1000:(zoom>=5)?10000:0;
   gr.easting = Math.floor(gr.easting/gr.accuracy)*gr.accuracy;
   gr.northing = Math.floor(gr.northing/gr.accuracy)*gr.accuracy;
   if (gr.accuracy == 0) alert('please zoom in to identify your location');
   else {
      window.opener.receiveGridReference(gr);
      window.close();
   }
   return true;
}         
function getWKTPoints(wkt) {
   var srs = getQueryVariable('srs');
   wkt = wkt.replace('POLYGON((','');
   var chopped = 0;
   if (!wkt.indexOf("))") < 0) chopped = 1; 
   else wkt = wkt.replace('))','');
   var coords = wkt.split(/,\s*/);
   coords.shift();
   var vertices = 0;
   x = 0;
   y = 0;
   var xy;
   for (pcount = 0; pcount < (coords.length - chopped); pcount++) {
      coord = coords[pcount];
      xy = coord.split(' ');
      if (xy.length == 2) {
         vertices++;
         if (srs && srs == 4326) {
            ddc = new DegreesDecimalCoordinate(xy[1],xy[0]);
         } else {
            gr = new GridReference(xy[0], xy[1]);
            ddc = conv_uk_ings_to_ll(gr);
         }
         x += ddc.longitude;
         y += ddc.latitude;
         points[pcount] = new GPoint(ddc.longitude,ddc.latitude);
      }
   }
   x /= vertices;
   y /= vertices;
   return new GPoint(x,y);
}
function showMarkers(show) {
   drawmarkers = show;
   if (show) {
      if (wmslayer && wfszoom && wfs) wfs.refreshInBoundsWithBuffer();
   } else {
      if (wmslayer && wfszoom && wfs) wfs.clear();
   }
   return true;
}
function updateLegend(featureType) {
   if (featureType) {
      var sel = document.getElementById("layers");
      var val = sel.options[sel.selectedIndex].firstChild.nodeValue;
      //var url = "http://89.249.160.206/geoserver/wms/GetLegendGraphic?VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=" + featureType;
      //var url = 'http://www.humber-edc.org.uk/wfsrefer.php?vs=' + getQueryVariable('vs');
      var url = '../wfsrefer.php?vs=' + getQueryVariable('vs');
      //var url = 'http://localhost/hedc1/wfsrefer.php?vs=' + getQueryVariable('vs');
      url += "&service=LEGEND&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=" + featureType;
      document.getElementById('legendImage').src = url;
      document.getElementById('legendTitle').innerHTML = val;
      document.getElementById('legendDiv').style.display = 'block';
   } else {
      document.getElementById('legendDiv').style.display = 'none';
   }
   return true;
}
function gridPrecision(num) {
   var len = num.length
   var precision = 5;  
   if (len < precision) {
      num*=10;
      num+=5;
      len++;
      while (len < precision) {
         num*=10;
         len++;
      }
   }
   return num;
}
function updatePoint(ngr) {
   if (ngr) {
      ngr.accuracy = document.getElementById('acc').value;  
      document.getElementById('ngr').value = ngr.asString();
      gr = conv_ngr_to_ings(ngr);
      ddc = conv_uk_ings_to_ll(gr);
      ddc.accuracy = ngr.accuracy;
   } else {
      var ngr = document.getElementById('ngr').value;
      var sq = ngr.substr(0,2).toUpperCase();
      var len = parseInt((ngr.length - 2) / 2);
      var e = gridPrecision(ngr.substr(2,len));
      var n = gridPrecision(ngr.substr(2+len,len));
      var precision = Math.pow(10,5-len);
      if (ngr.length % 2 == 1) {
         precision = 2 * (precision/10);         
         var tetrads = tetradArray();                                       
         var t = ngr.substr(ngr.length-1).toUpperCase();
         for (x in tetrads) {
            var tx = tetrads[x];
            for (y in tx) {
               if (t == tetrads[x][y]) {
                  e += 2 * precision * x;
                  n += 2 * precision * y;
               }
            } 
         }
         document.getElementById('acc').value = precision;
      }                              
      ngr = new NationalGridReference(sq,e,n);
      ngr.accuracy = precision;
      gr = conv_ngr_to_ings(ngr);
      ddc = conv_uk_ings_to_ll(gr);
      point = new GLatLng(ddc.latitude,ddc.longitude);
      map.setCenter(point);
      clickAction(point);
   }
   document.getElementById('ll').value = ddc.asString();
   return true;
}
function updateLLPoint() {
   var ll = document.getElementById('ll').value;
   list(lat,lon) = ll.split('/\s*,*\s*/');            
   ddc = new DegreesDecimalCoordinate(lat,lon);
   gr = wgs84_to_osgb36(ddc);
   ngr = find_gridsquare (gr_obj);
   point = new GLatLng(ddc.latitude,ddc.longitude);
   document.getElementById('ngr').value = ngr.asString();
   map.setCenter(point);
   clickAction(point);
   return true;
}