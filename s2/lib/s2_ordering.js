function s2_enableordering() {
   var orderable = s2_getstorage('s2_orderer');
   var encbutton,ordbutton,img;
   ordbutton = document.getElementById('flip_orderer');
   if (!ordbutton) { 
      encbutton = document.getElementById('flip_encrypt');
      ordbutton = document.createElement('BUTTON');   
      ordbutton.className = 'fliporder';
      ordbutton.id = 'flip_orderer';
      ordbutton.onclick = function() {s2_flip_orderer(this.id);};
      img = document.createElement('IMG');
      if (orderable>0) {
         img.src = '../images/sorticon-on.png'; 
         img.alt = 'ordering: on';
      } else {
         img.src = '../images/sorticon-off.png'; 
         img.alt = 'ordering: off';
      }
      ordbutton.appendChild(img);
      encbutton.parentNode.insertBefore(ordbutton,encbutton);
   }      
}
function s2_orderingenabled() {
   var orderable = s2_getstorage('s2_orderer');
   return orderable;
}
function s2_flip_orderer(buttonid) {            
   var txt,img,onoff,button;
   if (!buttonid) {
      onoff = (!s2_getstorage('s2_orderer'))?'ordering: on':'ordering: off';
      button = document.getElementById('flip_orderer');   
   } else {
      button = document.getElementById(buttonid);
      onoff = button.firstChild.alt;
   }
   switch (onoff) {
   case 'ordering: on': {
      txt = 'ordering: off';
      img = new Image();
      img.src = '../images/sorticon-off.png';
      img.alt = txt;
      button.replaceChild(img,button.firstChild);
      s2_setstorage('s2_orderer',0);
   }break;
   case 'ordering: off': {
      txt = 'ordering: on';
      img = new Image();
      img.src = '../images/sorticon-on.png';
      img.alt = txt;
      button.replaceChild(img,button.firstChild); 
      s2_setstorage('s2_orderer',1);
   }break;                    
   }
   s2_refresh();
   return true;   
}
function s2_getorders(type) {
   var orders,vals,order,text;
   orders = {'None':'\xD7','Up':'\u2192','Down':'\u2190'};
   if (type) {
      vals = [];
      for (order in orders) {
         switch(type) {
         case 'text':   {vals.push(orders[order]);}break;
         case 'sql':    {vals.push(order);}break;
         }
      }
   } else vals = orders;
   return vals;
}
function s2_orderby(id) {
   var args,button,itypeobj,cdom,text,oi,orders,order,oicons,oicon,cn,colm,coli,ordr,oobj;
   args = s2_disectid(id);
   button = document.getElementById(id);
   text = button.firstChild.nodeValue;
   orders = s2_getorders('sql');
   oicons = s2_getorders('text');
   colm = args.Column; 
   cdom = s2_getcurrentdomain();
     
   for (oi in oicons) {
      oicon = oicons[oi];
      if (oicon == text) {
         cn = ((oi + 1) % oicons.length);
         button.firstChild.nodeValue = oicons[cn];
      }     
   }
   var s2settings = s2_getcurrent();
   if (s2settings[args.IType]) {
      itypeobj = s2settings[args.IType];
   } else {
      itypeobj = new Object();
      itypeobj.Domain = cdom;
      itypeobj.IType = args.IType;
   }
   ordr = orders[cn];
   // Orders should be array of objects 
   if (order != 'None' && ((!itypeobj.Orders)||(typeof itypeobj.Orders != 'array'))) itypeobj.Orders = [];
   // Orders should be applied in the order they're selected so if you've 
   // already got a setting for the given column it needs to be removed and 
   // re-added at the end.
   for (oi in itypeobj.Orders) {
      oobj = itypeobj.Orders[oi];
      if (oobj.Column == colm) itype.Orders.splice(oi,1); 
   }
   if (ordr != 'None') itypeobj.Orders.push({Column:colm,Order:ordr});
   if (itypeobj.Orders.length == 0) delete itypeobj.Orders;  
   s2settings[args.IType] = itypeobj;
   s2_setstorage('S2Settings',s2settings);
   s2_refresh();
   return true;  
}
function s2_isorderable(jobj,prop) {
   var isorderable = true;     
   if (jobj.Interface[prop].DataType) {
      switch(jobj.Interface[prop].DataType) {
      case 'LINKEDTO':  isorderable = false;
      case 'LINKEDTOM': isorderable = false;
      case 'Map Data':  isorderable = false;
      }
   } else isorderable = false;
   return isorderable;
}
function s2_addorder(th,jobj,prop) {
   var sobj,orders,oicons,oi,text,order;
   if (s2_isorderable(jobj,prop)) {
      sobj = s2_getcurrent(jobj.IType);
      // get current orders // get order values and set column icons to correct order
      orders = s2_getorders();
      
      // add order by link 
      text = null;
      if (sobj && sobj.Orders) {for (oi in sobj.Orders) {if (sobj.Orders[oi].Column == prop) text = orders[sobj.Orders[oi].Order];}} 
      if (!text) text = orders.None; 
      button = document.createElement('BUTTON');
      button.className = 's2orderer';
      button.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Column-'+prop+'_Action-OrderBy';
      button.onclick = function() {s2_orderby(this.id);};
      button.appendChild(document.createTextNode(text));
      th.insertBefore(button,th.firstChild);
   }
}
