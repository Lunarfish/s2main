<?php
function put($url,$data=null) {
   //error_reporting(E_ALL);      
   $curl = curl_init($url);
   $post = (array)$data;
   foreach ($post as $key => $val)  {
      switch (gettype($val)) {
      case 'object': $post[$key] = json_encode($val);break;
      case 'array': $post[$key] = json_encode($val);break;
      }
   }
   curl_setopt($curl, CURLOPT_HEADER, false);
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
   curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
   if (isset($data)) curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
   // Make the call
   $res = curl_exec($curl);                     
   curl_close($curl);
   //print_r($res);
   return $res;
}
?>