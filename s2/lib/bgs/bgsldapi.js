var bgsbaseurl = 'http://dan.lunarfish.co.uk/bgs/';
var bgsroot = 'http://data.bgs.ac.uk/ref/';
var bgsloading = 0;
var bgsloadlist = [];
var bgsfileloadinprogress = false;

var bgs_cells = [];
var bgs_tcols = [];
var bgs_ctab;
var bgs_show;
var bgscache = [];
 
function s2_bgsify() {
   var tabs,t,tab; 
   tabs = document.getElementsByTagName('TABLE');
   for(t=0;t<tabs.length;t++) {
      tab = tabs[t];
      if (!tab.id) tab.id = 'tab_'+t; 
      s2_bgsifytab(tab);
   }
}
function s2_bgs_geturicols(tab) {
   var tr,th,bgscols;
   bgscols = [];
   tr = tab.tHead.rows[0];
   for(cx=0;cx<tr.cells.length;cx++) {
      th = tr.cells[cx];
      if (th.className && th.className.match(/BGS/)) bgscols.push(cx); 
   }
   return bgscols; 
}
function s2_bgsifytab(tab) {
   var o,bgscols,addcols,tbody,thead,tr,th,td,nde,tvk,tx,rx,cx,row,col,tid,cid,addCN,addTG;
   tbody = tab.tBodies[0];
   o = (tab.tHead)?'h':'v'; // list tables have headings across the top 
   switch(o) {
   case 'h': {
      bgscols = s2_bgs_geturicols(tab);
      for(rx=0;rx<tbody.rows.length;rx++) {
         tr = tbody.rows[rx];
         for(tx=0;tx<bgscols.length;tx++) {
            cx = bgscols[tx];
            td = tr.cells[cx];
            if (!td.id) td.id = tab.id+'r'+rx+'c'+cx;
            s2_bgs_loadcell(td);
         }
      }
   }break;
   case 'v': {
      for(rx=0;rx<tbody.rows.length;rx++) {
         tr = tbody.rows[rx];
         for(cx=0;cx<tr.cells.length;cx++) {
            td = tr.cells[cx];
            if (td.nodeName == 'TD' && td.className.match(/BGS/)) {
               if (!td.id) td.id = tab.id+'r'+rx+'c'+cx;
               s2_bgs_loadcell(td);
            }
         }
      }     
   }break;
   }
}
function s2_bgs_send(url,responder,label) {
   var ac,resp;
   ac = new AjaxClass();
   ac.url = url;
   ac.responder = responder;
   ac.Method = 'GET';
   ac.Async = true;
   ac.label = label;
   ac.init();
   resp = ac.send();            
}
function s2_bgs_getname(uri) {
   var url;
   url = bgsbaseurl + 'api.php?a=find&q='+uri;
   if (!bgscache[uri] || bgscache[uri] == 'null') { 
      s2_bgs_send(url,s2_bgs_loadurijson);
   } else {
      s2_bgs_loadurijson(bgscache[uri]);
   }            
}
function s2_bgs_loadurijson(json) {
   var bgsobj,bgsuri,curi,label,nx,cx,cid,cel;
   bgsobj = JSON.parse(json);
   bgsuri = bgsobj.data[0].concept;
   label = bgsobj.data[0].label;
   if (!bgscache[bgsuri] || bgscache[bgsuri] == 'null') bgscache[bgsuri] = json;
   for(cid in bgs_cells) {
      curi = bgs_cells[cid];
      if (curi == bgsuri) {
         cel = $("#"+cid);
         if (cel && cel.prop("nodeName")) s2_bgs_loaduri(cel,bgsobj);
         else delete bgs_cells[cid];
      }   
   }
}
function s2_bgs_loaduri(cel,bgsobj) {
   var lnk,txt,act,tab,addcols,node;
   if (cel) {
      node = cel.children().last();
      //while(!node.prop("nodeName") && node.prev()) node = node.prev();
      lnk = $('<a/>');
      if (!cel.closest('table').hasClass('listform')) 
         lnk.css("padding-left","5px");
      lnk.append(bgsobj.data[0].label); 
      lnk.attr("id",node.attr("id")+"_BGS-Link");
      lnk.attr("href",bgsobj.data[0].concept);
      lnk.on("click", function(event) {
         s2_showbgsdialog(event.target);
         event.stopPropagation();
         event.preventDefault();
         return false;
      });
      tag = node.prop("nodeName");//(node.nodeName)?node.nodeName: 
      if (tag) {
         switch(tag.toUpperCase()) {
         case 'INPUT': {
            node.attr("type","hidden"); 
            node.attr("value",bgsobj.data[0].concept);
            node.before(lnk);
         }break;
         default: node.replaceWith(lnk); break;
         }
      }
   }
   delete bgs_cells[cel.id];
}
function s2_bgs_loadcell(cel) {
   var uri;
   uri = s2_getndeval(cel);
   if (!bgs_cells[cel.id]) bgs_cells[cel.id] = uri;
   s2_bgs_getname(uri);      
}
function s2_showbgsdialog(lnk) {
   var uri,id,ti,node,text,name,btn,p,prop,val,suppress,lid,nid,namespace,args,tag;
   var tab,tabid,tab,tr,th,td,btn,img,o;
   try {

      lid = lnk.id;
      uri = lnk.href;

      args = s2_disectid(lid);

      if (args.Current) namespace = document.getElementById('Form-'+args.Form+'_IType-'+args.IType+'_Property-'+args.Property+'_Current-'+args.Current+'_Namespace').value;
      else namespace = document.getElementById('Form-'+args.Form+'_IType-'+args.IType+'_Property-'+args.Property+'_Namespace').value;

      cleardialog('dialog-liner');
      dialog = document.getElementById('dialog-liner');

      node = document.createElement('input');
      node.type = 'hidden';
      node.id = 'bgs-property';
      node.value = lid;
      dialog.appendChild(node);
      
      node = document.createElement('input');
      node.type = 'hidden';
      node.id = 'bgs-namespace';
      node.value = namespace;
      dialog.appendChild(node);
      
      node = document.createElement('input');
      node.type = 'text';
      node.id = 'bgs-search';
      dialog.appendChild(node);
      
      node = document.createElement('button');
      node.appendChild(document.createTextNode('Search'));
      node.onclick = s2_bgs_search;
      dialog.appendChild(node);
      
      node = document.createElement('div');
      node.id = 'bgs-results';
      tag = (lnk.nodeName)?lnk.nodeName:lnk.prop("nodeName");
      // if id is set load item

      switch(tag.toUpperCase()) {
      case 'A': s2_bgs_showitem(lnk.href); break;
      }

      dialog.appendChild(node);
      // add event handler to #bgs-results div links
      s2_bgs_pageload();
      showdialog();   
   } catch (e) {
      alert('Unable to draw dialog for item');
   } 
}

function s2_bgs_useval(btn) {
   var lnk,uri,lab,pid,tgt,tag,newlink,parent,input,args,lid,iid;
   lnk = btn.previousSibling;
   uri = lnk.href;
   lab = lnk.firstChild.nodeValue;
   pid = $("#bgs-property").val();
   args = s2_disectid(pid);
   iid = 'Form-'+args.Form+'_IType-'+args.IType+'_Property-'+args.Property;
   lid = iid + '_BGS-Link'; 
   newlink = $("<a/>");
   newlink.attr("href",uri);
   newlink.attr("id",lid);
   newlink.append(lab);
   newlink.on("click",function(event) {
      s2_showbgsdialog(event.target);
      event.stopPropagation();
      event.preventDefault();
      return false;
   });
   $("#"+pid).replaceWith(newlink);
   input = $("<input/>");
   input.attr("type","hidden");
   input.attr("id",iid);
   input.attr("value",uri);
   $("#"+lid).before(input);
   //alert(lab + '\n' + uri + '\n' + tag);
   hidedialog();
}



function s2_bgs_pageload() {
   //var tid = $("#bgs-namespace").val();
   //s2_bgs_preload(tid);
   $("#bgs-results").on("click","a",function(event) {
      s2_bgs_showitem(event.target);
      event.stopPropagation();
      event.preventDefault();
      return false;
   });
}
function s2_bgs_empty() {
   var qs,url,req,res,jsn;
   qs = 'api.php?a=empty';
   url = bgsbaseurl + qs;
   req = {target:url,sync:true,responder:null};
   jsn = snc_send(req);
   res = JSON.parse(jsn);
   return res.status;
}
function s2_bgs_loadallfiles() {
   var qs,url,req,res,jsn;
   s2_bgs_empty();
   qs = 'api.php?a=getfiles';
   url = bgsbaseurl + qs;
   req = {target:url,sync:true,responder:null};
   jsn = snc_send(req);
   res = JSON.parse(jsn);
   if (res.status) {
      bgsloadlist = res.data;
      s2_bgs_processloadlist();
   }
}
function s2_bgs_addtoloadlist(files) {
   var file,i,j,item,isloaded,isadded;
   for (i in files) {
      file = files[i];
      isadded = false;
      for (j in bgsloadlist) {
         item = bgsloadlist[j];
         if (file == item) isadded = true;
      }
      isloaded = s2_bgs_isloaded(file); 
      if (!isloaded && !isadded) bgsloadlist.push(file);
      if (isloaded) bgs_loaded[file] = true;
   }
   s2_bgs_processloadlist();
}
function s2_bgs_processloadlist() {
   var file,filecount;
   filecount = bgsloadlist.length; 
   if (!bgsfileloadinprogress && filecount>0) {
      file = bgsloadlist.shift();
      bgsfileloadinprogress = true;
      s2_bgs_loadfile(file);
   }
}
function s2_bgs_parseid(id) {
   var space,classname,concept,item,escroot,bgs_obj;
   space = id.replace(/\/id\//,'/ref/');
   path = space.replace(bgsroot,'');
   paths = path.split(/\//);
   bgs_obj = {};
   bgs_obj.id = id;
   if (paths[0]) bgs_obj.classname = paths[0];
   if (paths[1]) bgs_obj.concept = paths[1];
   if (paths[2]) bgs_obj.item = paths[2];
   if (bgs_obj.item) space = space.replace(/item/,'');
   if (bgs_obj.concept) spave = space.replace(/bgs_obj.concept\//,'');
   bgs_obj.space = space;
   return bgs_obj;     
}
function s2_bgs_preload(space) {
   var files;
   files = [];
   bgs_obj = s2_bgs_parseid(space);
   files.push(bgs_obj.classname + '.nt');
   if (bgs_obj.concept) files.push(bgs_obj.classname + '_' + bgs_obj.concept + '.nt');
   //s2_bgs_load(files);
   s2_bgs_addtoloadlist(files);
   return files; 
}
function s2_bgs_isloaded(file) {
   var req,jsn,res,url,qs,isloaded;
   isloaded = 0;
   if (bgs_loaded[file]) isloaded = true;
   else {
      try {
         qs = 'api.php?a=isloaded&q='+file;
         url = bgsbaseurl + qs;
         req = {target:url,sync:true,responder:null};
         jsn = snc_send(req);
         res = JSON.parse(jsn);
         if (res.status) isloaded = res.data[file];
      } catch (e) {}
   }
   return isloaded;
}
var bgs_loaded = {};
function s2_bgs_load(files) {
   var i,file,isloaded,allloaded;
   allloaded = true;
   for (i in files) {
      file = files[i];
      isloaded = s2_bgs_isloaded(file);
      allloaded &= isloaded;
      if (!isloaded) {
         s2_bgs_loadfile(file);
         bgsloading++;
      } else bgs_loaded[file] = true;            
   }
} 
function s2_bgs_loadfile(file) {
   var req,jsn,res,url,qs,isloaded;
   isloaded = 0;
   try {
      qs = 'api.php?a=load&q='+file;
      url = bgsbaseurl + qs;
      req = {target:url,responder:s2_bgs_afterload};
      snc_send(req);
   } catch (e) {}
   return isloaded;
}
function s2_bgs_afterload(jsn) {
   var res,i,j,file,ident;
   //alert(jsn);
   res = JSON.parse(jsn);
   if (res.status == 1) {
      loaded = res.data[0];
      bgsfileloadinprogress = false;
      s2_bgs_processoutstanding(loaded);
      s2_bgs_processloadlist();
   } else bgsfileloadinprogress = false;
}
function s2_bgs_processoutstanding(loaded) {
   for (i in bgs_outstanding) {
      ident = bgs_outstanding[i];
      for (j in ident.waitingon) {
         file = ident.waitingon[j];
         if (bgs_loaded[file] == true || loaded.match(/file/)) ident.waitingon.splice(j,1);
      }
      bgs_outstanding[i] = ident;
      if (ident.waitingon.length == 0) {
         switch(ident.action) {
         case 'find': s2_bgs_find(ident.id); break;
         }
         bgs_outstanding.splice(i,1);
      } 
   }
}
var bgs_outstanding = [];
function s2_bgs_loadandfind(id) {
   var ident,files;
   ident = s2_bgs_parseid(id);
   files = s2_bgs_preload(ident.space);
   for (file in bgs_loaded) {
      for (i in files) if (files[i] == file) files.splice(i,1);
   } 
   ident.action = 'find';
   ident.waitingon = files;
   if (files.length > 0) bgs_outstanding.push(ident);
   else s2_bgs_find(id);
}
function s2_bgs_find(id) {
   var url,qs,q,ns;
   qs = 'api.php?a=find&q='+id;
   url = bgsbaseurl + qs;
   d3.json(url, function(error, graph) {
      if (error) alert(error);  
      else s2_bgs_showlink(graph); 
   });
}
function s2_bgs_showlink(res) {
   var id,ident,eidclass,lnk;
   if (res.status == 1 && res.data && res.data.length > 0) {
      id = res.data[0].concept;
      ident = s2_bgs_parseid(id);
      eidclass = '.' + s2_bgs_identtoid(ident);
      $(eidclass).empty();//.children().remove();
      $(eidclass).append(res.data[0].label);
   }   
}
function s2_bgs_search(q) {
   var url,qs,q,ns;
   q =  $("#bgs-search").val();
   ns = $("#bgs-namespace").val();
   qs = 'api.php?q='+escape(q)+'&s='+ns;
   url = bgsbaseurl + qs;
   inprogress();
   d3.json(url, function(error, graph) {
      completed();
      if (error) alert(error);  
      else s2_bgs_showresults(graph); 
   });
}
function s2_bgs_showresults(res) {
   //alert(JSON.stringify(res));
   var cont,list,i,row,lnk,nsref,nsid;
   nsref = $("#bgs-namespace").val();
   nsid = nsref.replace(/\/ref\//,'/id/');
   $("#bgs-results").empty();//.children().remove();
   cont = $("#bgs-results");
   if (res.status == 1) {
      list = $("<ul></ul>");
      list.attr("id","bgs-list");
      for (i in res.data) {
         row = res.data[i];
         path = row.concept.split(/\//);
         path.pop();
         concepttype = path.pop().replace(/([a-z])([A-Z])/g,'$1 $2');
         li = $("<li></li>");
         li.append(concepttype + ': ');
         lnk = $("<a></a>");
         lnk.append(row.label);
         lnk.attr("href",row.concept);
         lnk.attr("target","bgs_data");
         li.append(lnk);
         li.css("clear","both");
         if (row.concept.match(nsid)) {
            btn = $("<button/>");
            btn.append('Use');
            btn.on("click",function() {s2_bgs_useval(this);});
            li.append(btn);
         }
         list.append(li);
      }
      cont.append(list);
      cont.append("<p>"+res.acknowledgements+"</p>");      
   } else {
      cont.append("<p>"+res.message+"</p>");
   }
}
function s2_bgs_showitem(id) {
   var url,qs,q,ns;
   qs = 'api.php?q='+escape(id)+'&a=getitem';
   url = bgsbaseurl + qs;
   inprogress();
   d3.json(url, function(error, graph) {
      completed();
      if (error) alert(error);  
      else s2_bgs_drawitem(graph); 
   });
}
function s2_bgs_drawitem(res) {
   var cont,list,li,p,i,row,lnk,btn,tab,tr,th,td,prop,propname,ident,eid,finds,nsref,nsid;
   nsref = $("#bgs-namespace").val();
   nsid = nsref.replace(/\/ref\//,'/id/');
   
   finds = [];
   $("#bgs-results").empty();//.children().remove();
   cont = $("#bgs-results");
   if (res.status == 1) {
      cont.append("<h4>Properties</h4>");
      tab = $("<table></table>");
      tr = $("<tr></tr>");
      th = $("<th></th>");
      th.append('Property');
      tr.append(th);
      th = $("<th></th>");
      th.append('Value');
      tr.append(th);
      tab.append(tr);
         
      if (res.data.item.length > 0) {
         tr = $("<tr/>");
         tr.append("<th>item</th>");
         td = $("<td/>");
         for(i in res.data.item) {
            p = $("<p/>");
            lnk = $("<a/>");
            lnk.append(res.data.item[i].label);
            lnk.attr("id","bgs-item-"+i);
            lnk.attr("href",res.data.item[i].concept);
            lnk.attr("target","bgs_data");
            //lnk.css("display","block");
            p.append(lnk);
            p.css("clear","both");
            // only allow property set for current namespace
            if (res.data.item[i].concept.match(nsid)) {
               btn = $("<button/>");
               btn.append('Use');
               btn.on("click",function() {s2_bgs_useval(this);});
               p.append(btn);
            }
            td.append(p);
         }
         tr.append(td);
         tab.append(tr);
      }
      if (res.data.parent && res.data.parent.length > 0) {
         tr = $("<tr/>");
         tr.append("<th>parent</th>");
         td = $("<td/>");
         for(i in res.data.parent) {
            p = $("<p/>");        
            lnk = $("<a/>");
            lnk.append(res.data.parent[i].label);
            lnk.attr("id","bgs-parent-"+i);
            lnk.attr("href",res.data.parent[i].concept);
            lnk.attr("target","bgs_data");
            //lnk.css("display","block");
            p.append(lnk);
            p.css("clear","both");
            // only allow property set for current namespace
            if (res.data.parent[i].concept.match(nsid)) {
               btn = $("<button/>");
               btn.append('Use');
               btn.on("click",function() {s2_bgs_useval(this);});
               p.append(btn);
            }
            td.append(p);
         }
         tr.append(td);
         tab.append(tr);
      }
      if (res.data.children.length > 0) {
         tr = $("<tr/>");
         tr.append("<th>children</th>");
         td = $("<td/>");
         for(i in res.data.children) {
            p = $("<p/>");
            p.append((parseInt(i)+1)+': ');        
            lnk = $("<a/>");
            lnk.append(res.data.children[i].label);
            lnk.attr("id","bgs-children-"+i);
            lnk.attr("href",res.data.children[i].concept);
            lnk.attr("target","bgs_data");
            //lnk.css("display","block");
            p.append(lnk);
            p.css("clear","both");
            // only allow property set for current namespace
            if (res.data.children[i].concept.match(nsid)) {
               btn = $("<button/>");
               btn.append('Use');
               btn.on("click",function() {s2_bgs_useval(this);});
               p.append(btn);
            }
            td.append(p);
         }
         tr.append(td);
         tab.append(tr);
      }
      if (typeof res.data.properties == 'object') {
         for(propname in res.data.properties) {
            prop = res.data.properties[propname];
            tr = $("<tr></tr>");
            th = $("<th></th>");
            th.append(s2_bgs_proptotext(propname));
            //th.append(prop.label);
            tr.append(th);
            td = $("<td></td>");
            switch(prop.type) {
            case 'uri': {
               ident = s2_bgs_parseid(prop.value);
               eid = s2_bgs_identtoid(ident);
               lnk = $("<a></a>");
               lnk.attr("href",prop.value);
               lnk.addClass(eid);
               lnk.attr("target","bgsld");
               lnk.append(prop.value);
               td.append(lnk);
               // You can't use a ? as an identifier since it identifies a 
               // query string but it appears in the BGS data 
               // Geochronology / Division concept  
               if (ident.item != '?') finds.push(prop.value);          
            }break; 
            default: td.append(prop.value); break;
            }
            tr.append(td);
            tab.append(tr);
         }
         cont.append(tab);         
      }
      cont.append("<p>"+res.acknowledgements+"</p>");      
   } else {
      cont.append("<p>"+res.message+"</p>");
   } 
   //cont.append(JSON.stringify(res));
   for (i in finds) s2_bgs_find(finds[i]);//s2_bgs_loadandfind(finds[i]);
}
function s2_bgs_identtoid(ident) {
   return 'bgs-' + ident.classname + '-' + ident.concept + '-' + ident.item;
}
function s2_bgs_proptotext(prop) {
   return prop.replace(/([a-z])([A-Z])/g,'$1 $2').toLowerCase(); 
}
