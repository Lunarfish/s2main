var tables;
var lheaderwidth = 200;
var buttonwidth = 20;
var tableids = new Array();
var tabledata = new Array();
var userotate = false;

// split at row = thead / tbody  
// split at col is where row data stops being th elements and starts being td elements
var splitatcol = new Array();
var showrows = new Array();
var showcols = new Array();

var rowcursor = new Array();
var colcursor = new Array();

function table_setupmanager() {
   var table,tnum,par,div,button;
   tables = document.getElementsByTagName('TABLE');
            
   for(tnum=0;tnum<tables.length;tnum++) {
      table = tables[tnum];
      if (!table.id) table.id = 'table_'+tnum;
      tableids.push(table.id);
      if (table.className) table.className = table.className + ',managed_table';
      else table.className = 'managed_table';
      tabledata[table.id] = table.cloneNode(true);
      par = table.parentNode;
      div = document.createElement('DIV');
      div.className = 'managed_table_div';
      div.appendChild(table);
      par.appendChild(div);
      table_split(table.id);      
   }      
}

function table_split(tableid) {
   var div,node,table,thead,tbody,tr,th,td,i,j,trh,trb,img,text,cols,rows,span;
   table = document.getElementById(tableid);
   if (table.tHead && table.tBodies[0].childNodes > 10) {
      var div = table.parentNode;
      var w = (div.style.width)?parseInt(div.style.width):div.offsetWidth;
      var h = window.innerHeight;
      var tdata = tabledata[tableid];
      thead = table.tHead;
      tbody = table.tBodies[0];
      trb = tbody.rows[0];
      var split = 0;
      node = trb.cells[0];
      trh = thead.rows[0]; 
      while(node.nodeName != 'TD') {   
         trh.cells[split].style.width = lheaderwidth+'px';
         node = node.nextSibling;
         split++;
      }
      splitatcol[tableid] = split;
      rows = tbody.rows.length;
      for (i=0;i<thead.rows.length;i++) {
         trh = thead.rows[i];
         cols = trh.cells.length;
         for (j=0+split;j<trh.cells.length;j++) {
            th = trh.cells[j];
            if (userotate) {
               text = th.childNodes[0].nodeValue;
               img = new Image();
               img.src = 'http://www.yhedn.org.uk/textrotate.php?t='+text;
               img.alt = text;
               th.replaceChild(img,th.childNodes[0]);
               th.style.verticalAlign = 'middle';
               th.style.textAlign = 'center';
            }
            /*
            text = th.childNodes[0];
            span = document.createElement('SPAN');
            //span.style.display = 'block';
            span.appendChild(text);
            th.appendChild(span);
            span.className = 'rotate90';
            */
         }   
      } 
      var twidth = (table.style.width)?parseInt(table.style.width):table.offsetWidth;
      var hheight = (thead.style.height)?parseInt(thead.style.height):thead.offsetHeight;
      var bheight = (tbody.style.height)?parseInt(tbody.style.height):tbody.offsetHeight;
      //alert (w + ' vs ' + twidth + '\n' + h + ' vs ' + theight);
      var rheight = 0;
      for (i=0;i<tbody.rows.length;i++) {  
         rheight = Math.max(rheight,((tbody.rows[i].style.height)?parseInt(tbody.rows[i].style.height):tbody.rows[i].offsetHeight));
      }
      showcols[tableid] = Math.floor(((w  - (split * lheaderwidth) - (2 * buttonwidth))/ twidth) * cols);
      showrows[tableid] = Math.floor((h - hheight - (2 * buttonwidth)) / rheight);
      /*
      for (i=0;i<tbody.rows.length;i++) {
         tbody.rows[i].cells[0].style.height = rheight+'px';
      }
      */
      if (showrows[tableid]<1)showrows[tableid] = 1;
      
      //alert(showcols[tableid] + ' ' + showrows[tableid]);
      rowcursor[tableid] = 0;
      colcursor[tableid] = 0;
      for(i=0;i<thead.rows.length;i++) {
         trh = thead.rows[i];
         while(trh.cells.length > (showcols[tableid] + split)) {
            trh.removeChild(trh.lastChild);
         }
      }
      while(tbody.rows.length > showrows[tableid]) {
         tbody.removeChild(tbody.lastChild);
      } 
      for(i=0;i<tbody.rows.length;i++) {
         trb = tbody.rows[i];
         trb.style.height = rheight+'px';
         while(trb.cells.length > (showcols[tableid] + split)) {
            trb.removeChild(trb.lastChild);
         }
      } 
      div = table.parentNode;
      node = div.lastChild;
      while(node.nodeName == 'BUTTON') {
         div.removeChild(node);
         node = div.lastChild;
      }
      button = document.createElement('BUTTON');
      button.onmousedown = function() {table_scroll(this.parentNode.firstChild.id,'up');};
      button.onmouseup = table_stopscrolling;
      img = new Image();
      img.src = 'http://www.yhedn.org.uk/images/up.gif';
      img.alt = 'scroll up'; 
      button.appendChild(img);
      button.className = 'scroller';
      div.appendChild(button);
      button = document.createElement('BUTTON');
      button.onmousedown = function() {table_scroll(this.parentNode.firstChild.id,'down');};
      button.onmouseup = table_stopscrolling;
      img = new Image();
      img.src = 'http://www.yhedn.org.uk/images/down.gif';
      img.alt = 'scroll down'; 
      button.appendChild(img);
      button.className = 'scroller';
      div.appendChild(button);
      button = document.createElement('BUTTON');
      button.onmousedown = function() {table_scroll(this.parentNode.firstChild.id,'left');};
      button.onmouseup = table_stopscrolling;
      img = new Image();
      img.src = 'http://www.yhedn.org.uk/images/left.gif';
      img.alt = 'scroll left'; 
      button.appendChild(img);
      button.className = 'scroller';
      div.appendChild(button);
      button = document.createElement('BUTTON');
      button.onmousedown = function() {table_scroll(this.parentNode.firstChild.id,'right');};
      button.onmouseup = table_stopscrolling;
      img = new Image();
      img.src = 'http://www.yhedn.org.uk/images/right.gif';
      img.alt = 'scroll right'; 
      button.appendChild(img);
      button.className = 'scroller';
      div.appendChild(button);
      
      button = document.createElement('BUTTON');
      button.onclick = function() {table_unsplit(this.parentNode.parentNode.parentNode.parentNode.id);};
      button.appendChild(document.createTextNode('unsplit'));
      thead.rows[0].cells[0].appendChild(document.createElement('BR'));
      thead.rows[0].cells[0].appendChild(button);
   }
   return true;         
}

function table_unsplit(tableid) {
   var table,tdata,div,i,node,button,cell1;
   table = document.getElementById(tableid);
   tdata = tabledata[tableid].cloneNode(true);
   div = table.parentNode;
   div.replaceChild(tdata,table);
   node = div.lastChild;
   while(node.nodeName == 'BUTTON') {
      div.removeChild(node);
      node = div.lastChild;
   }
   
   button = document.createElement('BUTTON');
   button.onclick = function() {table_split(this.parentNode.firstChild.id);};
   button.appendChild(document.createTextNode('split'));
   div.appendChild(button);
   return true;
}


var scrolling;

var interval = 200;
var startinterval = 200;
var step = 0;
var startstepchange = 5
var stepchange = 5;
var scrolltable;
var scrolldirection;
function table_scroll(tableid,direction) {
   scrolltable = tableid;
   scrolldirection = direction;
   table_scroll_now();
}
function table_scroll_now() {
   step++;
   switch (scrolldirection) {
   case 'up': {
      if (rowcursor[scrolltable] > 0) {
         rowcursor[scrolltable]--;
         table_updatedata(scrolltable,false,true);
      }
   }break;
   case 'down': {
      var rows = tabledata[scrolltable].tBodies[0].rows.length;
      var show = showrows[scrolltable];
      if (rowcursor[scrolltable] < (rows-show)) {
         rowcursor[scrolltable]++;
         table_updatedata(scrolltable,false,true);
      }
   }break;
   case 'left': {
      if (colcursor[scrolltable] > 0) {
         colcursor[scrolltable]--;
         table_updatedata(scrolltable,true,false);
      }
   }break;
   case 'right': {
      var cols = tabledata[scrolltable].tHead.rows[0].cells.length;
      var show = showcols[scrolltable];
      if ((colcursor[scrolltable]+splitatcol[scrolltable]) < (cols-show)) {
         colcursor[scrolltable]++;
         table_updatedata(scrolltable,true,false);
      }
   }break;
   }
   scrolling = window.setTimeout('table_scroll_now();',interval);
   if (step >= stepchange) {
      stepchange *= 3;
      interval /= 2;
      step = 0;   
   } 
}
function table_updatedata(tableid,movex,movey) {
   var table = document.getElementById(tableid);
   var i,j,thead,tbody,trh,trb,trd,th,td,img,text,td2;
   var tdata = tabledata[tableid];
   var cursr = rowcursor[tableid];
   var cursc = colcursor[tableid];
   var showr = showrows[tableid];
   var showc = showcols[tableid];
   var split = splitatcol[tableid];
   if (movex) {
      thead = table.tHead;
      for(i=0;i<thead.rows.length;i++) {
         trh = thead.rows[i];
         trd = tdata.tHead.rows[i];
         for(j=0;j<showc;j++) {
            th = trh.cells[j+split];
            text = trd.cells[j+cursc+split].firstChild.nodeValue;
            if (userotate) {
               img = new Image();
               img.src = 'http://www.yhedn.org.uk/textrotate.php?t='+text;
               th.replaceChild(img,th.firstChild);
            } else {
               th.replaceChild(document.createTextNode(text),th.firstChild);
            }             
         }
      }   
   }
   tbody = table.tBodies[0];
   for(i=0;i<tbody.rows.length;i++) {
      trb = tbody.rows[i];
      trd = tdata.tBodies[0].rows[i+cursr];
      if (movey) {
         for(j=0;j<split;j++) {
            th = trb.cells[j];
            text = trd.cells[j].firstChild.cloneNode(true);
            th.replaceChild(text,th.firstChild);             
         }
      }
      for(j=0;j<showc;j++) {
         td = trb.cells[j+split];
         td2 = trd.cells[j+cursc+split].cloneNode(true);
         trb.replaceChild(td2,td);             
      }
   }
   return true;
}

function table_stopscrolling() {
   window.clearTimeout(scrolling);
   step = 0;
   interval = startinterval;
   stepchange = startstepchange;
   return true;
}
window.onmouseup = table_stopscrolling;