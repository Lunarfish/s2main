<?php
class NIA_Nature_Improvement_Area extends DBT  {
   protected $tablename = 'NIA_Nature_Improvement_Area';
   protected $displayname = 'Nature Improvement Area';
   protected $perpage = 10;
   protected $alts = array('Themes' => 'NIA_Theme');
   protected $view = '[
      {
         "Name":"Indicators",
         "Data":[
            {
               "Table":"NIA_Indicator"
            }
         ]
      },
      {
         "Name":"Sources",
         "Data":[
            {
               "Table":"NIA_Dataset"
            }
         ]
      },
      {
         "Name":"Data Collection" 
      },
      {
         "Name":"Monitoring"
      },
      {
         "Name":"Evaluation"
      }
   ]'; 
   protected $columns = array(
      'NIA_Nature_Improvement_Area_ID' => 'Unique Key',
      'Name'                           => '{"DataType":"Short Text","Mandatory":1}',
      'Lead_Contact'                   => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"]}',
      'From_Date'                      => 'Date',
      'Until_Date'                     => 'Date'
   );
   protected $domain = 'nia';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
   protected $defaultpermissions = array(
'USR_Default_Permissions' => 
'"Domain","Label","Description","Value","Anon" 	
"nia","System Administrator","Add and edit projects",0,0
"nia","Partnership Administrator","Add and edit project data",0,0
"nia","Partnership Member","View project data",0,0
"nia","Registered User","View unprotected data",1,0',
'USR_Permissions' => 
'"User_Id","Domain","Label","Description","Value"
1,"nia","Registered User",NULL,1
1,"nia","Partnership Member",NULL,1
1,"nia","Partnership Administrator",NULL,1
1,"nia","System Administrator",NULL,1'   
   );     
}
class NIA_Theme extends DBT {
   protected $tablename = 'NIA_Theme';
   protected $displayname = 'Theme';
   protected $show = array('NIA_Sub_Theme');
   protected $alts = array('NIAs' => 'NIA_Nature_Improvement_Area');
   protected $columns = array(
      'NIA_Theme_ID' => 'Unique Key',
      'Name'         => '{"DataType":"Short Text","Mandatory":1}',
      'Code'         => 'Short Text',
      'Description'  => 'Long Text' 
   );
   protected $domain = 'nia';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );
   protected $primingdata = array(
      'NIA_Theme' => '
"Name","Code"
"Biodiversity","B"
"Ecosystem Servcies","E"
"Socio-economic","S"
"Partnership Working","P"'
   );
   function getshortname() {
      return $this->derivename(array('Code'));   
   }
   
}
class NIA_Sub_Theme extends DBT {
   protected $tablename = 'NIA_Sub_Theme';
   protected $displayname = 'Sub-theme';
   protected $show = array('NIA_Sub_Theme_Category');
   protected $columns = array(
      'NIA_Sub_Theme_ID'   => 'Unique Key',
      'Theme'              => '{"DataType":"LINKEDTO","TargetType":"NIA_Theme","TargetField":"NIA_Theme_ID","Mandatory":1,"Current":1}',
      'Name'               => '{"DataType":"Short Text","Mandatory":1}',
      'Code'               => 'Short Text',
      'Description'        => 'Long Text' 
   );
   protected $domain = 'nia';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );   
   function getname() {
      return $this->derivename(array('Theme','Name'));   
   }
   function getshortname() {
      return $this->derivename(array('Theme','Code'));   
   }
}
class NIA_Sub_Theme_Category extends DBT {
   protected $tablename = 'NIA_Sub_Theme_Category';
   protected $displayname = 'Sub-theme Category';
   //protected $show = array('NIA_Indicator_Category');
   protected $columns = array(
      'NIA_Sub_Theme_Category_ID'   => 'Unique Key',
      'Sub_Theme'                   => '{"DataType":"LINKEDTO","TargetType":"NIA_Sub_Theme","TargetField":"NIA_Sub_Theme_ID","Mandatory":1,"Current":1}',
      'Name'                        => '{"DataType":"Short Text","Mandatory":1}',
      'Code'                        => 'Short Text',
      'Description'                 => 'Long Text' 
   );
   protected $domain = 'nia';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );      
   function getname() {
      return $this->derivename(array('Sub_Theme','Code'));   
   }
}
class NIA_Indicator extends DBT {
   protected $tablename = 'NIA_Indicator';
   protected $displayname = 'Indicator';
   protected $columns = array(
      'NIA_Indicator_ID'   => 'Unique Key',
      'NIA'                => '{"DataType":"LINKEDTO","TargetType":"NIA_Nature_Improvement_Area","TargetField":"NIA_Nature_Improvement_Area_ID","Mandatory":1,"Current":1}',
      'Sub_Theme_Category' => '{"DataType":"LINKEDTO","TargetType":"NIA_Sub_Theme_Category","TargetField":"NIA_Sub_Theme_Category_ID","Mandatory":1,"Current":1}',
      'Name'               => '{"DataType":"Short Text","Mandatory":1}',
      'Code'               => 'Short Text',
      'Indicates'          => 'Long Text',
      'Units'              => 'Short Text',
      'Datasets'           => '{"DataType":"LINKEDTOM","TargetType":"NIA_Dataset","TargetField":"NIA_Dataset_ID","Property":"Indicator_Datasets"}',
       
   );
   protected $domain = 'nia';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );
}
class NIA_Dataset extends DBT {
   protected $tablename = 'NIA_Dataset';
   protected $displayname = 'Dataset';
   protected $columns = array(
      'NIA_Dataset_ID'  => 'Unique Key',
      'NIA'             => '{"DataType":"LINKEDTO","TargetType":"NIA_Nature_Improvement_Area","TargetField":"NIA_Nature_Improvement_Area_ID","Mandatory":1,"Current":1}',
      'Data_Source'     => '{"DataType":"LINKEDTO","TargetType":"NIA_Data_Source","TargetField":"NIA_Data_Source_ID"}',
      'Name'            => '{"DataType":"Short Text","Mandatory":1}',
      'Website'         => '{"DataType":"URL"}',
      'Indicators'      => '{"DataType":"LINKEDTOM","TargetType":"NIA_Indicator","TargetField":"NIA_Indicator_ID","Property":"Indicator_Datasets"}',
      
   );
   protected $domain = 'nia';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );      
   function getname() {
      return $this->derivename(array('Data_Source','Name'));   
   }
}
class NIA_Data_Source extends DBT {
   protected $tablename = 'NIA_Data_Source';
   protected $displayname = 'Data_Source';
   protected $columns = array(
      'NIA_Data_Source_ID' => 'Unique Key',
      'NIA'                => '{"DataType":"LINKEDTO","TargetType":"NIA_Nature_Improvement_Area","TargetField":"NIA_Nature_Improvement_Area_ID","Mandatory":1,"Current":1}',
      'Name'               => '{"DataType":"Short Text","Mandatory":1}',
      'Website'            => '{"DataType":"URL"}'
   );
   protected $domain = 'nia';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );
}
class NIA_Monitoring_Data extends DBT {

}
class NIA_Evaluation_Data extends DBT {
   
}
?>