function getScrollXY() {
  var scrOfX = 0, scrOfY = 0;
  if( typeof( window.pageYOffset ) == 'number' ) {
    //Netscape compliant
    scrOfY = window.pageYOffset;
    scrOfX = window.pageXOffset;
  } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
    //DOM compliant
    scrOfY = document.body.scrollTop;
    scrOfX = document.body.scrollLeft;
  } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
    //IE6 standards compliant mode
    scrOfY = document.documentElement.scrollTop;
    scrOfX = document.documentElement.scrollLeft;
  }
  return [ scrOfX, scrOfY ];
}
function showdialog() {
   var dims,sTop,node;
   hidebackgroundselects();
   dims = getScrollXY();
   sTop = dims[1];
   document.getElementById('main').style.zIndex = 1;
   node = document.getElementById('screen');
   node.style.marginTop = sTop+'px';
   node.style.zIndex = 2;
   node.style.display = 'block';
   node.style.backgroundColor = '#ccf';
   node = document.getElementById('dialog-container');
   node.style.display = 'block';
   node.style.marginTop = sTop+'px';
   node.style.zIndex = 3;
   return true;
}
function hidedialog() {
   showhiddenselects();
   document.getElementById('dialog-container').style.zIndex = 1;
   document.getElementById('dialog-container').style.display = 'none';
   document.getElementById('screen').style.zIndex = 2;
   document.getElementById('screen').style.display = 'none';
   document.getElementById('screen').style.backgroundColor = 'transparent';
   document.getElementById('main').style.zIndex = 3;
   cleardialog('dialog-liner');
   s2_hidetooltip();
   return true;
}
/*
   In IE6 nothing can be on top of a select so when you create a dialog you have
   to first hide all the selects in the underlying content.  
*/
function nodeisindialog(child) {
   var node = child.parentNode;  
   var indialog = false;
   while (node && node.nodeName != 'BODY') {
      if (node.id == 'dialog-liner') indialog = true;
      node = node.parentNode; 
   }
   return indialog;   
} 
function hidebackgroundselects() {
   var selects,select,si,indialog,node;
   selects = document.getElementsByTagName('SELECT');
   for(si in selects) {
      select = selects[si];
      if (select && select.nodeName == 'SELECT') {
         indialog = nodeisindialog(select);
         if (!indialog) select.style.visibility = 'hidden';
      }    
   }
   return true;
}
function showhiddenselects() {
   var selects,select,si,indialog,node;
   selects = document.getElementsByTagName('SELECT');
   for(si in selects) {
      select = selects[si];
      if (select && select.nodeName == 'SELECT') {
         indialog = nodeisindialog(select);
         if (!indialog) select.style.visibility = 'visible';
      }    
   }
   return true;
}

function inprogress() {
   var size = 100;
   var screen = document.getElementById('screen');
   var img = document.createElement('IMG');
   img.src = '../images/progress.gif';
   img.style.position = 'absolute';
   img.style.width = size+'px';
   img.style.height = size+'px';
   img.style.top = Math.floor((parseInt(screen.style.height)/2) - (size/2));
   img.style.left = Math.floor((parseInt(screen.style.width)/2) - (size/2));
   img.alt = 'Communicating with server.';
   screen.appendChild(img);
   screen.textAlign = 'center'; 
   screen.style.zIndex = 4;
   screen.style.display = 'block';
   screen.style.backgroundColor = '#fff';
   //document.getElementById('dialog-container').style.zIndex = 1;
   //document.getElementById('main').style.zIndex = 2;
   return true;   
}
function completed() {
   var screen = document.getElementById('screen');
   screen.style.zIndex = 1;
   screen.style.backgroundColor = 'transparent';
   screen.style.display = 'none';
   while (screen.childNodes.length > 0) screen.removeChild(screen.lastChild);
   //document.getElementById('dialog-container').style.zIndex = 2;
   //document.getElementById('main').style.zIndex = 3;
   return true;   
}
function resolverequirements() {
   
}
function askforrequirement() {
   
}
var mobile = false;
function Coordinate (x, y, o) {
    this.x = x;
    this.y = y;
    this.orientation = o;
}
function getsize(element) {
   var dims = new Coordinate();
   dims.x = element.style.width;
   dims.y = element.style.height;
   if (dims.x && (dims.x.indexOf('%') == -1)) dims.x = stripPX(dims.x);
   else dims.x = element.width;
   if (dims.y && (dims.y.indexOf('%') == -1)) dims.y = stripPX(dims.y);
   else dims.y = element.height;
   return dims;
}

var mobile,startx,starty,correctx,correcty;

function follow(element,e) {

// This doesn't work
   var o,mx,my,dialog;
   if (mobile) mobile = false;
   else {
      mobile = true;
      if (typeof window.event != "undefined") {
         try {
            startx = truebody().scrollLeft + window.event.clientX - element.offsetLeft;
            starty = truebody().scrollTop + window.event.clientY - element.offsetTop;
         } catch (err) {}
      } else if (typeof e != "undefined") {
         try {
            startx = e.pageX - element.offsetLeft;
            starty = e.pageY - element.offsetTop;
         } catch (err) {}
      } 
      var isize = getsize(element);
      correctx = Math.floor(isize.x/2);
      correcty = Math.floor(isize.y/2);
      dialog = element;
   }
   return true;
}
