// communication is handled via the cdm_orequest.js library.

var key;
var iv;
function getencrypted(clear,type) {
   var ko,key,iv;
   if (type && type == 'AB') {
      ko = cdm_getexternaldatakeys();
      key = ko.ab;
      iv =  ko.bc;
   } else {
      //key = readCookie('k1');
      //iv =  readCookie('k2');
      key = s2_getstorage('k1');
      iv =  s2_getstorage('k2');
   }
   var encoded = des(key,clear,1,1,iv,0);
   encoded = stringToHex(encoded);
   return encoded;
}
function decryptit(encoded,type) {
   var ko,key,iv;
   if (type && type == 'AB') {
      ko = cdm_getexternaldatakeys();
      key = ko.ab;
      iv =  ko.bc;
   } else {
      //key = readCookie('k1');
      //iv =  readCookie('k2');
      key = s2_getstorage('k1');
      iv =  s2_getstorage('k2');
   }
   var clear = des(key,hexToString(encoded),0,1,iv,0);
   /* if it is json remove any trailing characters */
   if (clear.match(/\}/)) clear = clear.substring(0,clear.lastIndexOf('}')+1);
   return clear;
}
function getAuthJSON() {
   var JSONObject = new Object();
   JSONObject.session = s2_getcookie('PHPSESSID');
   JSONObject.username = s2_getstorage('username');
   var JSONstring = JSON.stringify(JSONObject);
   return JSONstring;
}
function testJSON() {
   var JSONstring = getJSON();
   document.getElementById('plain').value = JSONstring;
   encryptit(JSONstring);
   return true;
}
var d = s2_getstorage('cdm_client');
//var d = 'www.humber-edc.org.uk/snc';
function auth() {
	inprogress();
   var h = s2_getstorage('handshake');
   if (!h || h != 'completed') authenticate();
   else checkin();
}
function authenticate() {
   var k1 = s2_getstorage('k1');
   var s = s2_getcookie('PHPSESSID');
   var ac = new AjaxClass();
	ac.url = 'cdm_json_proxy.php';
	var params = new Object();
   params.a = 'handshake';
   params.d = s2_getstorage('cdm_client');
   params.s = s;
   params.e = getencrypted(getAuthJSON());
   if (s2_getstorage('cdm_server')!=s2_getstorage('cdm_client')) params.k = k1;
   params.target = "http://"+s2_getstorage('cdm_server')+"/cdm_server_handshake2.php";
   ac.parameters = params;
   ac.responder = authresponse;
   ac.init();
   ac.send(); 
}
function authresponse(response) {
   completed();
   //var clear = decryptit(response);
//alert('response: ' + response + '\ndecoded: ' +clear);
   //var JSONobject = JSON.parse(clear);
   var JSONobject = JSON.parse(response);
//var c1 = ""+document.cookie;   
   s2_delstorage('k1');
   s2_delstorage('k2');
   s2_delstorage('cdm_session_id');
   s2_delstorage('handshake');
//var c2 = ""+document.cookie;   
	s2_setstorage('k1',JSONobject.k1,60,true);
	s2_setstorage('k2',JSONobject.k2,60,true);
	s2_setstorage('cdm_session_id',JSONobject.cdm_session_id,60,true);
	s2_setstorage('handshake','completed',60,true);
//var c3 = ""+document.cookie;   
//alert(c1+'\n'+c2+'\n'+c3);
//alert('Handshake completed.');
//   alert('Key1:return:'+JSONobject.k1+' cookie:'+readCookie('k1')+
//         '\nKey2:return:'+JSONobject.k2+' cookie:'+readCookie('k2'));
   //request('getMethods','methods');
   //enableinterface();
}
function syncauth() {
   var k1 = s2_getstorage('k1');
   var k2 = s2_getstorage('k2');
   var s = s2_getcookie('PHPSESSID');
   var ac = new AjaxClass();
	ac.url = 'cdm_json_proxy.php';
	var params = new Object();
   params.a = 'handshake';
   params.d = s2_getstorage('cdm_client');
   params.s = s;
   params.e = getencrypted(getAuthJSON());
   if (s2_getstorage('cdm_server')!=s2_getstorage('cdm_client')) params.k = k1;
   params.target = "http://"+s2_getstorage('cdm_server')+"/cdm_server_handshake2.php";
   ac.parameters = params;
   ac.Async = false;
   ac.init();
   var response = ac.send();
   response = cdm_decrypt(response);    
   var JSONobject = JSON.parse(response);
   s2_delstorage('k1');
   s2_delstorage('k2');
   s2_delstorage('cdm_session_id');
   s2_delstorage('handshake');
	s2_setstorage('k1',JSONobject.k1,60,true);
	s2_setstorage('k2',JSONobject.k2,60,true);
	s2_setstorage('cdm_session_id',JSONobject.cdm_session_id,60,true);
	s2_setstorage('handshake','completed',60,true);
   completed();
   //enableinterface();
}

function checkin() {
   var s = readCookie('cdm_session_id');
   //var d = 'localhost/dev';
   var ac = new AjaxClass();
	ac.url = 'cdm_json_proxy.php';
	//ac.url = 'jproxy.php';
	ac.responder = checkinresponse;
   var params = new Object();
   params.a = 'checkin';
   params.d = s2_getstorage('cdm_client');
   params.s = s;
   params.e = getencrypted(getAuthJSON());
   params.target = "http://"+s2_getstorage('cdm_server')+"/cdm_server_handshake2.php";
   ac.parameters = params;
   ac.init();
   ac.send();   
}
function checkinresponse(response) {
   completed();
   //var clear = decryptit(response);
   //var JSONobject = JSON.parse(clear);
   var JSONobject = JSON.parse(response);
	if (JSONobject.status) {
	   switch(JSONobject.status) {
	   case 'Session expired': {
	      authenticate();
	   }break;
	   default: {
         //alert(JSONobject.status+'\n'+JSONobject.message );
         s2_setstorage('handshake','completed',60,true);
         enableinterface();
      }break;
      }
	} else {
	   alert('Checkin failed.');
      s2_setstorage('handshake','required',60,true);
   }
}
function cdm_clientinit() {
   var req,par,res,sid,json,jobj;
   req = new Object();
   req.target = s2_getcurrentpath()+'/../cdm_client_handshake2.php';
   par = new Object();
   par.a = 'checkin';
   par.s = s2_getcookie('PHPSESSID');
   par.k = cdm_makekey();
   //s2_setcookie('k1',par.k,60,true);
   s2_setstorage('k1',par.k,60,true);
   req.request = par;
   req.sync = true;
   
   //req.responder = cdm_repairsession;
   //var json = snc_send(req);
   
   sid = s2_getcookie('PHPSESSID');
   req.etype = 'AB';
   req.sid = sid;
   res = snc_send(req);
   json = cdm_decrypt(res,'AB');
   jobj = JSON.parse(json);
   
   //s2_setcookie('k2',jobj.k,60,true);
   //s2_setcookie('encrypt',1,20160,true);
   //s2_setcookie('handshake','required',60,true);
   s2_setstorage('k2',jobj.k,60,true);
   s2_setstorage('encrypt',1,20160,true);
   s2_setstorage('handshake','required',60,true);
   return syncauth();
}
function cdm_repairssession(json) {
   var jobj = JSON.parse(json);
   //s2_setcookie('k2',jobj.k,60,true);
   // set encryption settings to last 2 weeks
   //s2_setcookie('encrypt',1,20160,true);
   //s2_setcookie('handshake','required',60,true);
   s2_setstorage('k2',jobj.k,60,true);
   // set encryption settings to last 2 weeks
   s2_setstorage('encrypt',1,20160,true);
   s2_setstorage('handshake','required',60,true);
   syncauth();
}

function cdm_makekey() {
   var len,k,seed,ran,characters; 
   characters = new Array( 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 
                           'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 
                           'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 
                           'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                           'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                           'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 
                           'w', 'x', 'y', 'z', '0', '1', '2', '3', 
                           '4', '5', '6', '7', '8', '9');
   k = '';
   while (k.length<8) {
      ran = (Math.floor(Math.random()*(characters.length+1)) % characters.length);
      k += characters[ran];
   }
   if (k.length > 8) k = k.substring(0,8);
   return k;  
} 