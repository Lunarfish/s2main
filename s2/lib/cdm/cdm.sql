CREATE TABLE IF NOT EXISTS `cdm_client_domain_referral` (
  `cdm_domain_referral_id` int(10) unsigned NOT NULL,
  `domain_name` varchar(200) NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `database_name` varchar(50) NOT NULL,
  `root_class` varchar(50) NOT NULL,
  PRIMARY KEY (`cdm_domain_referral_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `cdm_client_domain_referral` (`cdm_domain_referral_id`, `domain_name`, `project_id`, `database_name`, `root_class`) VALUES
(0, 'neyedc.org.uk', 1, 'HEDCMETA', 'AccessProject'),
(1, 'neyedc.org.uk', 2, 'HEDCMETA', 'AccessProject'),
(2, 'localhost/dev', 1, 'HEDCMETA', 'AccessProject'),
(3, 'localhost/dev', 2, 'HEDCMETA', 'AccessProject');

CREATE TABLE IF NOT EXISTS `cdm_client_session` (
  `cdm_session_id` int(10) unsigned DEFAULT NULL,
  `session` varchar(36) NOT NULL,
  `server` varchar(100) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `project_id` int(10) unsigned DEFAULT NULL,
  `access_level` int(10) unsigned DEFAULT NULL,
  `key1` varchar(36) DEFAULT NULL,
  `key2` varchar(36) DEFAULT NULL,
  `session_started` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `session_laccess` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`session`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `cdm_server_domain_referral` (
  `cdm_domain_referral_id` int(10) unsigned NOT NULL,
  `domain_name` varchar(200) NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `database_name` varchar(50) NOT NULL,
  `root_class` varchar(50) NOT NULL,
  PRIMARY KEY (`cdm_domain_referral_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `cdm_server_domain_referral` (`cdm_domain_referral_id`, `domain_name`, `project_id`, `database_name`, `root_class`) VALUES
(0, 'neyedc.org.uk', 1, 'HEDCMETA', 'AccessProject'),
(1, 'neyedc.org.uk', 2, 'HEDCMETA', 'AccessProject'),
(2, 'localhost/dev', 1, 'HEDCMETA', 'AccessProject'),
(3, 'localhost/dev', 2, 'HEDCMETA', 'AccessProject');

CREATE TABLE IF NOT EXISTS `cdm_server_session` (
  `cdm_session_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session` varchar(36) NOT NULL,
  `client` varchar(100) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `project_id` int(10) unsigned DEFAULT NULL,
  `access_level` int(10) unsigned DEFAULT NULL,
  `key1` varchar(36) DEFAULT NULL,
  `key2` varchar(36) DEFAULT NULL,
  `session_started` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `session_laccess` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`cdm_session_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=272 ;

