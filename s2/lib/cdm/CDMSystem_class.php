<?php
include_once('cdm/CDM_dbconfig.php');
class CDMSystem {
/*
CREATE TABLE SnC_System (
   Snc_System_Key INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   Snc_System_Name VACHAR(50) NOT NULL,
   Snc_Designation VARCHAR(10) NOT NULL   
)
*/
   public $table = 'SnC_System';
   public $Snc_System_Key;
   public $Snc_System_Name;
   public $Snc_Designation;
   protected $dbhandle;
   function CDMSystem() {
      $this->genDBConnection();   
   }
   function populate($sid) {
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from(  array('s' => $this->table),
			            array('Snc_System_Name','Snc_Designation'));
      $query->where('Snc_System_Key = ?', $sid);
      
      $result = $db->fetchAll($query);
      foreach ($result as $row) {
         $this->Snc_System_Key = $sid;
         $this->Snc_System_Name = $row['Snc_System_Name'];
         $this->Snc_Designation = $row['Snc_Designation'];
      }
      return $this->Snc_System_Name;
   }
   function genDBConnection() {
      //$dbConfig = getDBConfig();
      global $dbConfig,$dbPlatform;
      $this->dbhandle = Zend_Db::factory("Pdo_${dbPlatform}", $dbConfig);
   }
}
?>
