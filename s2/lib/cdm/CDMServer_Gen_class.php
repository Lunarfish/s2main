<?php
class CDMServer_Gen {
   protected $cdm_session_id;
   protected $client;
   protected $domain = 'localhost/dev';// replace with actual script domain
   protected $session_id;
   protected $project_id;
   protected $username;
   protected $access_level;
   protected $session_expiry = "INTERVAL '1 HOUR'";
   protected $dbhandle;
   protected $session_table = 'cdm_server_session';
   protected $referral_table = 'cdm_server_domain_referral';
   protected $keys = array();
   public $blocksize = 8;
   function CDMServer_Gen() {
      $this->genDBConnection();
   }
   function setSessionID($s) {
      $this->session_id = $s;
   }
   function getSessionID() {
      return $this->session_id;
   }
   function setCDMSessionID($cdmid) {
      $this->cdm_session_id = $cdmid;
   }
   function getCDMSessionID() {
      return $this->cdm_session_id;
   }
   function setDomain($domain) {
      $this->domain = $domain;
   }
   function setClient($client) {
      $this->client = $client;
      $this->update('client',$client);
   }
   function genDBConnection() {
      //$dbConfig = getDBConfig();
      global $dbConfig,$dbPlatform;
      $this->dbhandle = Zend_Db::factory("Pdo_${dbPlatform}", $dbConfig);
   }
   function genKey($len) {
      $len = (isset($len))?$len:$this->blocksize;
      $characters = array( 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 
                           'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 
                           'Q'. 'R', 'S', 'T', 'U', 'V', 'W', 'X', 
                           'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                           'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                           'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 
                           'w', 'x', 'y', 'z', '0', '1', '2', '3', 
                           '4', '5', '6', '7', '8', '9');
      $key = '';
      srand(ceil(time()/rand(1,100)));
      while (strlen($key)<8) {
         $rand = (rand() % count($characters));
         $key .= $characters[$rand];
      }
      if (strlen($key) > 8) $key = substr($key,0,8);
      return $key;
   }
   function setKey($key,$replace=null) {
      if (isset($replace)) $this->keys[($replace-1)] = $key;
      else $this->keys[] = $key;
      $this->storeKeys($replace);
   }
   // counts from 0
   function getKey($number) {
      return $this->keys[($number-1)];
   }
   function storeKeys($keynum=null) {
      if (isset($keynum)) {
         $this->update('key'.$keynum,$this->keys[($keynum-1)]);      
      } else {
         foreach($this->keys as $key => $value) {
            $this->update('key'.($key+1),$value);
         }
      }
   }
   function setUsername($user) {
      $this->username = $user;
      $this->update('username',$user);
   }
   function getUsername() {
      return $this->username;
   }
   function populate($sessionid,$cdmid=null) {
      /* The point of the CDM client and CDM server session tables is to 
         maintain the encryption keys on the channels. The session is set by 
         the client and passed to the server and the server sets and controls 
         the cdm_session_id. So long as you the client to server is a many to 
         one relationship then the cdm_session_id should remain unique.
         
         This means that cdm_session_id is the primary key on the server table 
         and is a foreign key on the client table. The primary key on the client 
         table is the session id.
      */
      $newsession = false;
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from(  array('a' => $this->session_table),
		               array('cdm_session_id',
                           'session',
                           'username',
                           'key1',
                           'key2',
                           'session_started',
                           'session_laccess',
                     ));
      if (isset($cdmid)) $query->where('cdm_session_id = ?',($cdmid*1));
      else $query->where('session = ?',$sessionid);      
      $result = $db->fetchAll($query);
      if (count($result)==0) {
         $newsession = true;         
      } else {
         foreach($result as $row) {
            $this->cdm_session_id = $row['cdm_session_id'];
            $this->session_id = $row['session'];
            $this->username = $row['username'];
            $this->keys[] = $row['key1'];
            $this->keys[] = $row['key2'];
         }
      }
      
      if ($newsession == true && $sessionid) {
         $this->setSessionID($sessionid);
         $this->insert();
      }
      return !$newsession;
   }
   function insert() {
      $db = $this->dbhandle;
      $data = array(
         'session'         => $this->session_id,
         'session_started' => new Zend_Db_Expr("NOW()"),
         'session_laccess' => new Zend_Db_Expr("NOW()"),
      );
      $db->insert($this->session_table, $data);
      $this->cdm_session_id = $db->lastInsertId();
   }
   function update($column,$value) {
      if ($this->getSessionID() != null) { 
         $db = $this->dbhandle;
         $data = array($column => $value);
         $n = $db->update($this->session_table, $data, "cdm_session_id = $this->cdm_session_id");
      }
   }
   function removeExpiredSessions() {
      $db = $this->dbhandle;
      $n = $db->delete($this->session_table, "session_laccess + $this->session_expiry < NOW()");
      /*
         check number of sessions in table and if there are none reset the 
         id sequence
      */
      $query = $db->select();
      $query->from(  array('a' => $this->session_table),
			            array('cdm_session_id'));
      $result = $db->fetchAll($query);
      //if (count($result)==0) $db->getConnection()->exec("ALTER TABLE $this->session_table AUTO_INCREMENT = 0");
      $seq = strtolower($this->session_table).'_'.strtolower($this->session_table).'_id_seq';
      if (count($result)==0) $db->getConnection()->exec("SELECT setval('$seq',1);");
   }
   function handshake($client,$e_client) {
      if ($client == $this->domain) {
         include_once('cdm/CDMClient_class2.php');
         $cdm = new CDMClient();
         $cdm->setServer($client);
         $cdm->populate($this->getSessionID());
         $cdm->update('cdm_session_id',$this->cdm_session_id);
         $cdm->update('session_laccess',new Zend_Db_Expr("NOW()"));
         $this->setKey($cdm->getKey(1),1);
         $this->setKey($cdm->getKey(2),2);
//print "<p>".$cdm->getKey(1)."<br/>".$cdm->getKey(2)."</p>";         
         $json_client = json_decode($this->decrypt($e_client));
         $json = array();
         $json['k1'] = $this->genKey($this->blocksize);
         $json['k2'] = $this->genKey($this->blocksize);
         $json['cdm_session_id'] = $this->getCDMSessionID();
         $encoded = json_encode($json);
         $encrypted = $this->encrypt($encoded);
         $k1 = $cdm->setKey($json['k1'],1);
         $k2 = $cdm->setKey($json['k2'],2);
         $this->setKey($json['k1'],1);
         $this->setKey($json['k2'],2);
      } else {
         if ($this->approveDomainReferral($client)) {
            $url = "http://$client/cdm_client_handshake2.php";
            $qs = "a=handshake";
            $qs.= "&s=$this->session_id";
            $qs.= "&d=$this->domain";
            $qs.= "&c=$this->cdm_session_id";
            $ch = curl_init(); //initiate the curl session
            $timeout = 90;
            curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));
            curl_setopt($ch, CURLOPT_URL, $url); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $qs);
            curl_setopt($ch, CURLOPT_TIMEOUT, (int)$timeout);
            $return = curl_exec($ch);
            $json = json_decode($return);
            $s = $json->s;
            if ($s == $this->session_id) {
               $k2 = $json->k;
               $this->setKey($k2,2);
               $e_hshake = $json->e;
               $json_client = json_decode($this->decrypt($e_client));
               $json_hshake = json_decode($this->decrypt($e_hshake));
               if (is_object($json_client)) $c_user = urldecode($json_client->username);
               if (is_object($json_hshake)) $h_user = urldecode($json_hshake->username);
            } 
            $json = array();
            $json['k1'] = $this->genKey($this->blocksize);
            $json['k2'] = $this->genKey($this->blocksize);
            $json['cdm_session_id'] = $this->getCDMSessionID();
            $encoded = json_encode($json);
            $encrypted = $this->encrypt($encoded);
            $this->setKey($json['k1'],1);
            $this->setKey($json['k2'],2);
         } else {
            $json = array();
            $json['status'] = 'Access denied';
            $json['message'] = 'Handshake process failed';
            $encoded = json_encode($json);
            $encrypted = $this->encrypt($encoded);
         }   
      }
      return $encrypted;
   }
   function checkin($client,$e_client) {
      $this->removeExpiredSessions();
      if (!$this->getSessionID()) {
         $json = array();
         $json['status'] = 'Session expired';
         $json['message'] = 'Checkin failed';
         $encoded = json_encode($json);
         $encrypted = $this->encrypt($encoded);            
      } else if ($this->approveDomainReferral($client) || $this->domain == $client) {
      //echo $this->getKey(1) . " " . $this->getKey(2);
         $json_client = json_decode($this->decrypt($e_client));
         $json = array();
         $json['status'] = 'Access granted';
         $json['message'] = 'Checkin succeeded';
         $json['cdm_session_id'] = $this->getCDMSessionID();
         $encoded = json_encode($json);
         $encrypted = $this->encrypt($encoded);
         $this->update('session_laccess',new Zend_Db_Expr("NOW()"));          
      } else {
         $json = array();
         $json['status'] = 'Access denied';
         $json['message'] = 'Checkin failed';
         $encoded = json_encode($json);
         $encrypted = $this->encrypt($encoded);
      }
      return $encrypted;
   }
   function encrypt($clear) {
      // nothing encrypted is nothing so don't bother trying
      if (isset($clear) && $clear != "") {
         $cipher = mcrypt_module_open('des','','cbc','');
         mcrypt_generic_init($cipher, $this->getKey(1), $this->getKey(2));
         $encrypted = stringToHex(mcrypt_generic($cipher,$clear));
         mcrypt_generic_deinit($cipher);
         mcrypt_module_close($cipher);
      } else $encrypted = $clear; 
      return $encrypted;
      
   }
   function decrypt($encrypted) {
      $cipher = mcrypt_module_open('des','','cbc','');
      mcrypt_generic_init($cipher, $this->getKey(1), $this->getKey(2));
      $decrypted = rtrim(mdecrypt_generic($cipher,hexToString($encrypted)),"\0");
      mcrypt_generic_deinit($cipher);
      mcrypt_module_close($cipher); 
      return $decrypted;
   }
   function approveDomainReferral($domain) {
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from(  array('a' => $this->referral_table),
			            array('cdm_domain_referral_id',
                           'database_name',
                           'root_class'
                     ));
	   $query->where('domain_name = ?',$domain);
      $result = $db->fetchAll($query);
      $approved = (count($result)>0);
      
      return $approved;
   }
}
?>