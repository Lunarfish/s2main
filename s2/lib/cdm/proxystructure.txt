Encryption layer



jproxy = json proxy
eproxy = encryption proxy

jenv = envelope for json proxy request to server

eenv = envelope for encryption proxy on server

so at the client the json is the request to be processed by the server 
the json is then added to an object called eenv 
eenv.target = the server target address
this will be sent to the encryption proxy, decoded and the json forwarded to the target
eenv is then encrypted. 

the eenv has to be sent via the json proxy so eenv is then added to an object called jenv
jenv.target = the server encryption proxy.
jenv.isencrypted = true
jenv.session = session id
jenv.request = eenv

the jenv can then be stringified and sent via the json proxy

without encryption 

the json request is added directly to jenv
jenv.target is the server target;
jenv.isencrypted = false
jenv.request = json request

eproxy decrypts json sends request gets response and returns the encrypts the 
response into an eenv to return. so the javascript needs to know how to create 
the jenv and eenv envelopes as necessary.

jproxy just forwards the request on via remoteRequestFunction and passes the 
response back unaltered.   
  
add login button and 