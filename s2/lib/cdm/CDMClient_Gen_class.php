<?php
class CDMClient_Gen {
   protected $cdm_session_id;
   protected $session_id;
   protected $project_id;
   //public $server = 'data.yhedn.org.uk';
   public $server = 'localhost/dev';
   protected $username;
   protected $access_level;
   protected $session_expiry = "INTERVAL '1 HOUR'";
   protected $dbhandle;
   protected $session_table = 'cdm_client_session';
   protected $referral_table = 'cdm_client_domain_referral';
   protected $keys = array();
   public $blocksize = 8;
   public $isnewsession;
   function CDMClient_Gen() {
      $this->genDBConnection();
   }
   function setServer($s) {
      $this->server = $s;
   }
   function getServer() {
      return $this->server;
   }
   function setSessionID($s) {
      $this->session_id = $s;
   }
   function getSessionID($s) {
      return $this->session_id;
   }
   function setCDMSessionID($cdmid) {
      $this->cdm_session_id = $cdmid;
   }
   function getCDMSessionID() {
      return $this->cdm_session_id;
   }
   function genDBConnection() {
      //$dbConfig = getDBConfig();
      global $dbConfig,$dbPlatform;
      $this->dbhandle = Zend_Db::factory("Pdo_${dbPlatform}", $dbConfig);
   }
   function genKey($len=null) {
      $len = (isset($len))?$len:$this->blocksize;
      $characters = array( 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 
                           'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 
                           'Q'. 'R', 'S', 'T', 'U', 'V', 'W', 'X', 
                           'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                           'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                           'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 
                           'w', 'x', 'y', 'z', '0', '1', '2', '3', 
                           '4', '5', '6', '7', '8', '9');
      $key = '';
      srand(ceil(time()/rand(1,100)));
      while (strlen($key)<8) {
         $rand = (rand() % count($characters));
         $key .= $characters[$rand];
      }
      if (strlen($key) > 8) $key = substr($key,0,8);
      return $key;
   }
   function setKey($key,$replace=null) {
      if (isset($replace)) $this->keys[($replace-1)] = $key;
      else $this->keys[] = $key;
      $this->storeKeys($replace);
      return $key;
   }
   // counts from 0
   function getKey($number) {
      return $this->keys[($number-1)];
   }
   function populate($sessionid) {
      /* The point of the CDM client and CDM server session tables is to 
         maintain the encryption keys on the channels. The session is set by 
         the client and passed to the server and the server sets and controls 
         the cdm_session_id. So long as you the client to server is a many to 
         one relationship then the cdm_session_id should remain unique.
         
         This means that cdm_session_id is the primary key on the server table 
         and is a foreign key on the client table. The primary key on the client 
         table is the session id.
      */
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from(  array('a' => $this->session_table),
			            array('cdm_session_id',
                           'session',
                           'server',
                           'username',
                           'key1',
                           'key2',
                           'session_started',
                           'session_laccess',
                     ));
	   $query->where('session = ?',$sessionid);
      $result = $db->fetchAll($query);
      if (count($result)==0) {
         $this->setSessionID($sessionid);
         $this->setKey($this->genKey(),1);
         $this->setKey($this->genKey(),2);
         $this->insert();
         $this->isnewsession = true;
      } else {
         foreach($result as $row) {
            if ($sessionid == $row['session']) {
               $this->cdm_session_id = $row['cdm_session_id'];
               $this->session_id = $sessionid;
               $this->server = $row['server'];
               $this->username = $row['username'];
               $this->setKey($row['key1'],1);
               $this->setKey($row['key2'],2);
            }
         }
         $this->isnewsession = false;
      }
   }
   function setUsername($user) {
      $this->username = $user;
   }
   function getUsername() {
      return $this->username;
   }
   function insert() {
      $db = $this->dbhandle;
      $data = array(
         'session'         => $this->session_id,
         'server'          => $this->server,
         'username'        => $this->username,
         'session_started' => new Zend_Db_Expr("NOW()"),
         'session_laccess' => new Zend_Db_Expr("NOW()"),
      );
      $db->insert($this->session_table, $data);
      $this->storeKeys();
   }
   function storeKeys($keynum=null) {
      if (isset($keynum)) {
         $this->update('key'.$keynum,$this->keys[($keynum-1)]);      
      } else {
         foreach($this->keys as $key => $value) {
            $this->update('key'.($key+1),$value);
         }
      }
   }
   function update($column,$value) {
      $db = $this->dbhandle;
      $data = array($column => $value);
      $n = $db->update($this->session_table, $data, "session = '$this->session_id'");
   }
   function checkin() {
      $this->removeExpiredSessions();
      $db = $this->dbhandle;
      $data = array(
         'session_laccess' => new Zend_Db_Expr("NOW()")
      );
      $n = $db->update($this->session_table, $data, "session = '$this->session_id'");
   }
   function removeExpiredSessions() {
      $db = $this->dbhandle;
      $n = $db->delete($this->session_table, "session_laccess + $this->session_expiry < NOW()");
   }
   function handshake($server) {
      if ($this->approveDomainReferral($server)) {
         $url = "http://$server/cdm_server_handshake2.php";
         $qs;
         $auth=null;
         $authtype=null;
         $ch = curl_init(); //initiate the curl session
         $timeout = 90;
         $auth = "$user:$pass";   
         curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));
         curl_setopt($ch, CURLOPT_URL, $url); 
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
         curl_setopt($ch, CURLOPT_USERPWD, $auth);
         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $qs);
         curl_setopt($ch, CURLOPT_TIMEOUT, (int)$timeout);
         $return = curl_exec($ch);
      }
   }
   function encrypt($clear) {
      $cipher = mcrypt_module_open('des','','cbc','');
      mcrypt_generic_init($cipher, $this->getKey(1), $this->getKey(2));
      $encrypted = stringToHex(mcrypt_generic($cipher,$clear));
      mcrypt_generic_deinit($cipher);
      mcrypt_module_close($cipher); 
      return $encrypted;
   }
   function decrypt($encrypted) {
      $cipher = mcrypt_module_open('des','','cbc','');
      mcrypt_generic_init($cipher, $this->getKey(1), $this->getKey(2));
      $decrypted = rtrim(mdecrypt_generic($cipher,hexToString($encrypted)),"\0");
      mcrypt_generic_deinit($cipher);
      mcrypt_module_close($cipher); 
      return $decrypted;
   }
   function approveDomainReferral($domain) {
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from(  array('a' => $this->referral_table),
			            array('cdm_domain_referral_id',
                           'database_name',
                           'root_class'
                     ));
	   $query->where('domain_name = ?',$domain);
      $result = $db->fetchAll($query);
      return (count($result)>0);
   }
}
?>