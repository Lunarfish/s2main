<?php
$lvl = array(  'None','Public','Project','Admin');
include_once('Zend/Db.php');
class CDMAccessProject {
/*
create table access_projects (
   id int unsigned auto_increment not null,
   panel varchar (200) not null,
   admin varchar (200) not null,
   primary key (id)
);
*/
   public $table = 'access_projects';
   public $id;
   public $project;
   public $admin;
   public $levels = array();
   public $access_descriptions = array();
   protected $dbhandle;
   protected $project_library;
   protected $project_class;
   
   function CDMAccessProject($id=null) {
      $this->id = $id;
      $this->genDBConnection();   
      if ($this->id > 0) $this->getProject();
   }
   function getProject() {
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from(  array('a' => $this->table),
			            array('project', 'admin'));
      $query->where('id = ?', $this->id);
      $result = $db->fetchAll($query);
      foreach ($result as $row) {
         $this->project = $row['project'];
         $this->admin = $row['admin'];
      }
      $apl = new CDMAccessProjectLevels();
      $this->levels = $apl->getAccessLevelArray($this->id);
      $this->access_descriptions = $apl->getAccessLevelDescriptions($this->id);
   }
   /* getProjectMethods and callProjectMethod are twin methods to get data from 
      external data sources. 
   
      I think the way to do this is to associate a class library with each 
      project. Then have a method within the class which describes the rest of 
      the methods and required input arguments. 
      
      Class libraries can call common function libraries to do common tasks.
      
      Once the library has been established it can be included by the 
      callProjectMethod function   
      
      classes communicate in JSON and the JSON is interpreted into DOM nodes to 
      be returned to the screen as html either to a dialog or to the report
   */
   function getProjectLibrary($project) {
      include_once('CDMProjectLibrary_class.php');
      $cpl = new CDMProjectLibrary();
      $cpl->populate($project);
   //print_r($cpl);
      return $cpl;
   }
   function getProjectMethods($project) {
      $cpl = $this->getProjectLibrary($project);
      include_once($cpl->library);
      eval("\$pc = new $cpl->classname;");
      return $pc->getMethods();
   }
   function callProjectMethod($project,$method,$args=null) {
      $cpl = $this->getProjectLibrary($project);
      include_once($cpl->library);
      eval("\$pc = new $cpl->classname;");
      return $pc->request($method,$args);
   }
   function getAccessLevel($email) {
      $lflip = array_flip($this->levels);
      if (strtolower($email) == strtolower($this->admin))
         return $lflip['Admin'];
      else {
         list($user,$domain) = split("@",$email);
         $u = new CDMAccessProjectUser();
         $a1 = $u->getAccessLevel($email,$this->id);
         $d = new CDMAccessProjectDomain();
         $a2 = $d->getAccessLevel($domain,$this->id);
         return max($a1,$a2);
      }
   }
   function genDBConnection() {
      $dbConfig = array(
	     'host'		=> 'localhost',
	     'username'	=> 'hedc',
	     'password'	=> 'hedc',
	     'dbname'	=> 'hedcmeta'
      );
      $this->dbhandle = Zend_Db::factory('Pdo_Mysql', $dbConfig);
   }
}
class CDMAccessProjectUser {
/*
create table access_project_users (
   id int unsigned auto_increment not null,
   project int unsigned not null,
   user varchar (200) not null,
   access int unsigned not null,
   primary key (id)
);
*/
   public $table = 'access_project_users';
   public $id;
   public $project;
   public $user;
   public $access;
   protected $dbhandle;
   function CDMAccessProjectUser() {
      $this->genDBConnection();   
   }
   function getAccessLevel($email,$project) {
      $access = 0;
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from(  array('a' => $this->table),
			            array('access'));
      $query->where('user = ?', $email);
      $query->where('project = ?', $project);
      
      $result = $db->fetchAll($query);
      foreach ($result as $row) {
         $access = ($access>$row['access'])?$access:$row['access'];
      }
      return $access;
   }
   function genDBConnection() {
      $dbConfig = array(
	     'host'		=> 'localhost',
	     'username'	=> 'hedc',
	     'password'	=> 'hedc',
	     'dbname'	=> 'hedcmeta'
      );
      $this->dbhandle = Zend_Db::factory('Pdo_Mysql', $dbConfig);
   }
}
class CDMAccessProjectDomain {
/*
create table access_project_domains (
   id int unsigned auto_increment not null,
   project int unsigned not null,
   domain varchar (200) not null,
   access int unsigned not null,
   primary key (id)
);
*/
   public $table = 'access_project_domains';
   public $id;
   public $project;
   public $domain;
   public $access;
   protected $dbhandle;
   function CDMAccessProjectDomain() {
      $this->genDBConnection();   
   }
   function getAccessLevel($domain,$project) {
      $access = 0;
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from(  array('a' => $this->table),
			            array('access'));
      $query->where('domain = ?', $domain);
      $query->where('project = ?', $project);
      
      $result = $db->fetchAll($query);
      foreach ($result as $row) {
         $access = ($access>$row['access'])?$access:$row['access'];
      }
      return $access;
   }
   function genDBConnection() {
      $dbConfig = array(
	     'host'		=> 'localhost',
	     'username'	=> 'hedc',
	     'password'	=> 'hedc',
	     'dbname'	=> 'hedcmeta'
      );
      $this->dbhandle = Zend_Db::factory('Pdo_Mysql', $dbConfig);
   }
}
class CDMAccessProjectLevels {
/*
create table access_project_levels (
   id int unsigned auto_increment not null,
   project int unsigned not null,
   level varchar(10),
   primary key(id)
);
*/
   public $table = 'access_project_levels';
   public $id;
   public $project;
   public $level;
   public $description;
   protected $dbhandle;
   function CDMAccessProjectLevels($id=null,$project=null,$level=null) {
      $this->id = $id;
      $this->project = $project;
      $this->level = $level;
      $this->genDBConnection();   
   }
   function getAccessLevelArray($project) {
      $lvl = array();
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from(  array('a' => $this->table),
			            array('id', 'level'));
      $query->where('project = ?', $project);
      $result = $db->fetchAll($query);
      foreach ($result as $row) {
         $lvl[] = $row['level'];
      }
      return $lvl;
   }
   function getAccessLevelDescriptions($project) {
      $descs = array();
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from(  array('a' => $this->table),
			            array('level', 'description'));
      $query->where('project = ?', $project);
      $result = $db->fetchAll($query);
      foreach ($result as $row) {
         $descs[$row['level']] = $row['description'];
      }
      return $descs;
   }
   function genDBConnection() {
      $dbConfig = array(
	     'host'		=> 'localhost',
	     'username'	=> 'hedc',
	     'password'	=> 'hedc',
	     'dbname'	=> 'hedcmeta'
      );
      $this->dbhandle = Zend_Db::factory('Pdo_Mysql', $dbConfig);
   }
}
?>
