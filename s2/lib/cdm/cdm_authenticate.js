var key;
var iv;
function getencrypted(clear) {
   var key =   readCookie('k1');
   var iv =    readCookie('k2');
   var encoded = des(key,clear,1,1,iv,0);
   encoded = stringToHex(encoded);
   return encoded;
}
function decryptit(encoded) {
   var key =   readCookie('k1');
   var iv =    readCookie('k2');
   var clear = des(key,hexToString(encoded),0,1,iv,0);
   var clear = clear.substring(0,clear.lastIndexOf('}')+1);
   return clear;
}
function getAuthJSON() {
   var JSONObject = new Object();
   JSONObject.session = readCookie('PHPSESSID');
   JSONObject.username = readCookie('username');
   JSONObject.project = readCookie('project');
   JSONObject.access_level = readCookie('access_level');
   var JSONstring = JSON.stringify(JSONObject);
   return JSONstring;
}
function testJSON() {
   var JSONstring = getJSON();
   document.getElementById('plain').value = JSONstring;
   encryptit(JSONstring);
   return true;
}
function auth() {
   var h = readCookie('handshake');
   if (!h || h != 'completed') authenticate();
   else checkin();
}
function authenticate() {
   var k1 =   readCookie('k1');
   var s = readCookie('PHPSESSID');
   var d = 'localhost/dev';
   var http = getHTTPObject();
   var url = "http://"+readCookie('cdm_server')+"/cdm_server_handshake.php";
   var proxy = 'cdm_json_proxy.php';
   var params = "a=handshake&d="+d+"&s="+s+"&k="+k1+"&e="+getencrypted(getAuthJSON());
   params += '&target='+url;
   http.open("POST", proxy, true);

   //Send the proper header information along with the request
   http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
   http.setRequestHeader("Content-length", params.length);
   http.setRequestHeader("Connection", "close");

   http.onreadystatechange = function() {//Call a function when the state changes.
	  if (http.readyState == 4 && http.status == 200) {
	     authresponse(http.responseText);
	  }
	}
	http.send(params);
}
function authresponse(response) {
   var clear = decryptit(response);
   var JSONobject = JSON.parse(clear);
//var c1 = ""+document.cookie;   
   eraseCookie('k1');
   eraseCookie('k2');
   eraseCookie('cdm_session_id');
   eraseCookie('handshake');
//var c2 = ""+document.cookie;   
	createCookie('k1',JSONobject.k1,60);
	createCookie('k2',JSONobject.k2,60);
	createCookie('cdm_session_id',JSONobject.cdm_session_id,60);
	createCookie('handshake','completed',60);
//var c3 = ""+document.cookie;   
//alert(c1+'\n'+c2+'\n'+c3);
	alert('Handshake completed.\nAccess level '+JSONobject.access_level+' granted.');
//   alert('Key1:return:'+JSONobject.k1+' cookie:'+readCookie('k1')+
//         '\nKey2:return:'+JSONobject.k2+' cookie:'+readCookie('k2'));
   request('getMethods','methods');
}
function checkin() {
   var k1 =   readCookie('k1');
   var s = readCookie('cdm_session_id');
   var d = 'localhost/dev';
   var http = getHTTPObject();
   var url = "http://"+readCookie('cdm_server')+"/cdm_server_handshake.php";
   var proxy = 'cdm_json_proxy.php';
   var params = "a=checkin&d="+d+"&s="+s+"&e="+getencrypted(getAuthJSON());
   params += '&target='+url;
   http.open("POST", proxy, true);

   //Send the proper header information along with the request
   http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
   http.setRequestHeader("Content-length", params.length);
   http.setRequestHeader("Connection", "close");

   http.onreadystatechange = function() {//Call a function when the state changes.
	  if (http.readyState == 4 && http.status == 200) {
	     checkinresponse(http.responseText);
	  }
	}
	http.send(params);
}
function checkinresponse(response) {
   var clear = decryptit(response);
   var JSONobject = JSON.parse(clear);
	//createCookie('handshake','completed',60);
	if (JSONobject.access_level) {
      alert('Checkin completed.\nAccess level '+JSONobject.access_level+' granted.');
      createCookie('handshake','completed',60);
	} else {
	   alert('Checkin failed.');
      createCookie('handshake','required',60);
   }
}

function getHTTPObject() {
	var xhr = false;
	if (window.XMLHttpRequest) {
		xhr = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		try {
			xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch(e) {
			try {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			} catch(e) {
				xhr = false;
			}
		}
	}
	return xhr;
}
function createCookie(name,value,mins) {
	if (mins) {
		var date = new Date();
		date.setTime(date.getTime()+(mins*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

