CREATE TABLE access_project_domains (
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  project INTEGER NOT NULL,
  domain VARCHAR(200) NOT NULL,
  access INTEGER NOT NULL
);

INSERT INTO access_project_domains (project, domain, access) VALUES 
(1, 'neyedc.co.uk', 3);
INSERT INTO access_project_domains (project, domain, access) VALUES 
(2, 'neyedc.co.uk', 3);
INSERT INTO access_project_domains (project, domain, access) VALUES 
(1, 'humber-edc.org.uk', 3);
INSERT INTO access_project_domains (project, domain, access) VALUES 
(2, 'humber-edc.org.uk', 3);
INSERT INTO access_project_domains (project, domain, access) VALUES 
(1, 'northyorks.gov.uk', 2);
INSERT INTO access_project_domains (project, domain, access) VALUES 
(2, 'eastriding.gov.uk', 1);
INSERT INTO access_project_domains (project, domain, access) VALUES 
(2, 'naturalengland.gov.uk', 1);
INSERT INTO access_project_domains (project, domain, access) VALUES 
(1, 'naturalengland.org.uk', 2);

CREATE TABLE access_project_levels (
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  project INTEGER NOT NULL,
  level VARCHAR(10),
  description TEXT
);

INSERT INTO access_project_levels (project, level, description) VALUES 
(1, 'None', '');
INSERT INTO access_project_levels (project, level, description) VALUES 
(1, 'Public', 'This level of access is for public interested parties who are not explicitly involved in the project.');
INSERT INTO access_project_levels (project, level, description) VALUES 
(1, 'Member', 'This is the level of access you would give to a member of a SINC panel for example');
INSERT INTO access_project_levels (project, level, description) VALUES 
(1, 'Admin', 'Users or domains with Admin access are able to control who has access to a project');
INSERT INTO access_project_levels (project, level, description) VALUES 
(2, 'None', '');
INSERT INTO access_project_levels (project, level, description) VALUES 
(2, 'Public', 'This level of access is for public interested parties who are not explicitly involved in the project.');
INSERT INTO access_project_levels (project, level, description) VALUES 
(2, 'Member', 'This is the level of access you would give to a member of a SINC panel for example');
INSERT INTO access_project_levels (project, level, description) VALUES 
(2, 'Admin', 'Users or domains with Admin access are able to control who has access to a project');
INSERT INTO access_project_levels (project, level, description) VALUES 
(3, 'None', '');
INSERT INTO access_project_levels (project, level, description) VALUES 
(3, 'Public', 'This level of access is for public interested parties who are not explicitly involved in the project.');
INSERT INTO access_project_levels (project, level, description) VALUES 
(3, 'Member', 'This is the level of access you would give to a member of a SINC panel for example');
INSERT INTO access_project_levels (project, level, description) VALUES 
(3, 'Admin', 'Users or domains with Admin access are able to control who has access to a project');

CREATE TABLE access_project_users (
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  project INTEGER NOT NULL,
  user VARCHAR(200) NOT NULL,
  access INTEGER NOT NULL
);

INSERT INTO access_project_users (project, user, access) VALUES 
(1, 'louise.moss@northyorks.gov.uk', 2);
INSERT INTO access_project_users (project, user, access) VALUES 
(1, 'matthew.millington@northyorks.gov.uk', 2);
INSERT INTO access_project_users (project, user, access) VALUES 
(2, 'david.renwick@eastriding.gov.uk', 2);
INSERT INTO access_project_users (project, user, access) VALUES 
(1, 'andrew.craven@naturalengland.org.uk', 2);
INSERT INTO access_project_users (project, user, access) VALUES 
(3, 'dan.jones@humber-edc.org.uk', 3);
INSERT INTO access_project_users (project, user, access) VALUES 
(1, 'dan.mcandrew@harrogate.gov.uk', 2);
INSERT INTO access_project_users (project, user, access) VALUES 
(2, 'sue.ogilvy@naturalengland.org.uk', 2);
INSERT INTO access_project_users (project, user, access) VALUES 
(1, 'm.hammond300@googlemail.com', 2);
INSERT INTO access_project_users (project, user, access) VALUES 
(1, 'sarah.kettlewell@harrogate.gov.uk', 2);
INSERT INTO access_project_users (project, user, access) VALUES 
(3, 'carolyn.barber@rotherham.gov.uk', 3);
INSERT INTO access_project_users (project, user, access) VALUES 
(1, 'martin.fuller@environment-agency.gov.uk', 2);
INSERT INTO access_project_users (project, user, access) VALUES 
(1, 'julia.casterton@northyorks.gov.uk', 2);
INSERT INTO access_project_users (project, user, access) VALUES 
(1, 'ann.hanson@fwag.org.uk', 2);
INSERT INTO access_project_users (project, user, access) VALUES 
(2, 'ann.hanson@fwag.org.uk', 2);
INSERT INTO access_project_users (project, user, access) VALUES 
(1, 'Amanda.Coward@naturalengland.org.uk', 2);
INSERT INTO access_project_users (project, user, access) VALUES 
(1, 'jeremy.dick@forestry.gsi.gov.uk', 2);
INSERT INTO access_project_users (project, user, access) VALUES 
(1, 'bernadette.lobo@loboecology.co.uk', 2);
INSERT INTO access_project_users (project, user, access) VALUES 
(2, 'vaughan.grantham@eastriding.gov.uk', 2);
INSERT INTO access_project_users (project, user, access) VALUES 
(2, 'jill.howells@environment-agency.wales.gov.uk', 2);
INSERT INTO access_project_users (project, user, access) VALUES 
(1, 'jill.howells@environment-agency.wales.gov.uk', 2);

CREATE TABLE access_projects (
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  project VARCHAR(200) NOT NULL,
  admin VARCHAR(200) NOT NULL
);

INSERT INTO access_projects (project, admin) VALUES 
('North Yorkshire SINC Panel', 'julia.casterton@northyorks.gov.uk');
INSERT INTO access_projects (project, admin) VALUES 
('East Riding of Yorkshire SINC Panel', 'david.renwick@eastriding.gov.uk');
INSERT INTO access_projects (project, admin) VALUES 
('Rotherham BRC Data Search', 'carolyn.barber@rotherham.gov.uk');

create table access_project_login (
  id int unsigned auto_increment not null,
  project int unsigned not null,
  islocal boolean,
  delegatedto varchar(100),
  primary key (id)
);

insert into access_project_login (project,islocal,delegatedto) values
(1,false,'www.neyedc.org.uk/snc');
insert into access_project_login (project,islocal,delegatedto) values
(2,false,'www.neyedc.org.uk/snc');
insert into access_project_login (project,islocal,delegatedto) values
(3,true,null);
