<?php
include_once('cdm/CDM_dbconfig.php');
class CDMSessions {
/*
create table sessions (
   session_id varchar(32) not null,
   username varchar(100) not null,
   time_in timestamp not null,
   expires timestamp not null,
   last_acess timestamp not null,
   primary key (session_id)
);
*/
   public $table = 'sessions';
   public $username;
   public $time_in;
   public $expires;
   public $last_access;
   protected $dbhandle;
   function CDMSessions() {
      $this->genDBConnection();   
   }
   function populate($sid) {
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from(  array('s' => $this->table),
			            array('username','time_in','expires','last_access'));
      $query->where('session_id = ?', $sid);
      
      $result = $db->fetchAll($query);
      foreach ($result as $row) {
         $this->username = $row['username'];
         $this->time_in = $row['time_in'];
         $this->expires = $row['expires'];
         $this->last_access = $row['last_access'];
      }
      return $this->username;
   }
   function genDBConnection() {
      //$dbConfig = getDBConfig();
      global $dbConfig,$dbPlatform;
      $this->dbhandle = Zend_Db::factory("Pdo_${dbPlatform}", $dbConfig);
   }
}
?>
