<?php
/*
$paths = array(
   '../',
   '../../',
   '../../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
*/
include_once('settings.conf');
include_once('Zend/Db.php');
include_once('cdm/CDMClient_class2.php');
include_once('cdm/CDMSessions_class2.php');
//$s = new CDMSessions();
//$user = $s->populate(session_id());
//if (!$user) header("Location: restricted.php?");
$encrypt = (isset($_COOKIE['encrypt']))?$_COOKIE['encrypt']:$encrypt;
$cdm = new CDMClient();
$cdm->setServer("${server}/${folder}");
//$k1 = $cdm->setKey(substr(session_id(),0,$cdm->blocksize));
//$k2 = $cdm->setKey(cdm_getNewIV($cdm->blocksize));
//$cdm->populate(session_id());
//print "<h1>Populating</h1>";
$cdm->populate(session_id());
//print "<h1>Populated</h1>";
$cdm->checkin();    
//print "<h1>Checked IN</h1>";
             
//$cdm->setUsername($user);
/* 
   populate retrieves the keys from the database the keys need to remain the 
   same for the duration of the session so once they've been retrieved from 
   the database you replace the generated ones.
*/
$k1 = $cdm->getKey(1);
$k2 = $cdm->getKey(2);

//print "$k1 $k2<br/>";
//print session_id()."<br/>";
if (!isset($_COOKIE['S2Path'])||($_COOKIE['S2Path']!=$folder)) {
   setcookie('S2Path',$folder,time()+60*60,'/');
   // if the path changes remove the settings  
   setcookie('S2Settings',null,time(),'/');
}
if (!isset($_COOKIE['cdm_server'])) setcookie('cdm_server',$cdm->server,time()+60*60,'/');
if (!isset($_COOKIE['cdm_client'])) setcookie('cdm_client',"${client}/${folder}",time()+60*60,'/');
if (isset($user) && !isset($_COOKIE['username'])) setcookie('username',$user,time()+60*60,'/');
// since these are the keys the server thinks you're using you need to replace 
// them even if the cookies already exist.
$setkeys = false;
// if any of the following are the case then a new handshake is necessary
if (!isset($_COOKIE['cdm_session_id']) 
      || ($_COOKIE['cdm_session_id'] == 'null') 
      || ($cdm->isnewsession) || !isset($_COOKIE['handshake'])) {
   setcookie('handshake','required',time()+60*60,'/');
   $setkeys = true;   
}
if ($setkeys || !isset($_COOKIE['k1'])) setcookie('k1',$k1,time()+60*60,'/');
if ($setkeys || !isset($_COOKIE['k2'])) setcookie('k2',$k2,time()+60*60,'/');

//if (!isset($_COOKIE['encrypt'])) 
   setcookie('encrypt',$encrypt,time()+60*60,'/');
?>
