<?php
function stringToHex ($s) {
   $r = "";
   $hexes = array ("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");
   for ($i=0; $i<strlen($s); $i++) {$r .= ($hexes [(ord($s{$i}) >> 4)] . $hexes [(ord($s{$i}) & 0xf)]);}
   return $r;
}

function hexToString ($h) {
   $r = "";
   for ($i= (substr($h, 0, 2)=="0x")?2:0; $i<strlen($h); $i+=2) {$r .= chr (base_convert (substr ($h, $i, 2), 16, 10));}
   return $r;
}
/* return the string length used for the key and the input vector */
function cdm_getBlockSize() {
   return 8;
}
function cdm_getNewIV($len) {
   $len = (isset($len))?$len:8;
   $characters = array( 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 
                        'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 
                        'Q'. 'R', 'S', 'T', 'U', 'V', 'W', 'X', 
                        'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                        'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                        'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 
                        'w', 'x', 'y', 'z', '0', '1', '2', '3', 
                        '4', '5', '6', '7', '8', '9');
   $string = '';
   srand(ceil(time()/rand(1,100)));
   while (strlen($string)<8) {
      $rand = (rand() % count($characters));
      $string .= $characters[$rand];
   }
   if (strlen($string) > 8) $string = substr($string,0,8);
   return $string;
}

?>
