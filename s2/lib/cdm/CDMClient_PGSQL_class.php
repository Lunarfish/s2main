<?php 
class CDMClient extends CDMClient_Gen {
   protected $cdm_session_id;
   protected $session_id;
   protected $project_id;
   //public $server = 'data.yhedn.org.uk';
   public $server = 'localhost/s2pg';
   protected $username;
   protected $access_level;
   protected $session_expiry = "INTERVAL '1 HOUR'";
   protected $dbhandle;
   protected $session_table = 'cdm_client_session';
   protected $referral_table = 'cdm_client_domain_referral';
   protected $keys = array();
   public $blocksize = 8;
   public $isnewsession;
   function CDMClient() {
      parent::CDMClient_Gen();
   }
   function populate($sessionid) {
      /* The point of the CDM client and CDM server session tables is to 
         maintain the encryption keys on the channels. The session is set by 
         the client and passed to the server and the server sets and controls 
         the cdm_session_id. So long as you the client to server is a many to 
         one relationship then the cdm_session_id should remain unique.
         
         This means that cdm_session_id is the primary key on the server table 
         and is a foreign key on the client table. The primary key on the client 
         table is the session id.
      */
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from(  array('a' => $this->session_table),
			            array('cdm_session_id',
                           'session',
                           'server',
                           'username',
                           'key1',
                           'key2',
                           'session_started',
                           'session_laccess',
                     ));
	   $query->where('session = ?',$sessionid);
      $result = $db->fetchAll($query);
      if (count($result)==0) {
         $this->setSessionID($sessionid);
         $this->setKey($this->genKey(),1);
         $this->setKey($this->genKey(),2);
         $this->insert();
         $this->isnewsession = true;
      } else {
         foreach($result as $row) {
            if ($sessionid == $row['session']) {
               $this->cdm_session_id = $row['cdm_session_id'];
               $this->session_id = $sessionid;
               $this->server = $row['server'];
               $this->username = $row['username'];
               $this->setKey($row['key1'],1);
               $this->setKey($row['key2'],2);
            }
         }
         $this->isnewsession = false;
      }
   }
   function insert() {
      $db = $this->dbhandle;
      $data = array(
         'session'         => $this->session_id,
         'server'          => $this->server,
         'username'        => $this->username,
         'session_started' => new Zend_Db_Expr("NOW()"),
         'session_laccess' => new Zend_Db_Expr("NOW()"),
      );
      $db->insert($this->session_table, $data);
      $this->storeKeys();
   }
   function storeKeys($keynum=null) {
      if (isset($keynum)) {
         $this->update('key'.$keynum,$this->keys[($keynum-1)]);      
      } else {
         foreach($this->keys as $key => $value) {
            $this->update('key'.($key+1),$value);
         }
      }
   }
   function update($column,$value) {
      $db = $this->dbhandle;
      $data = array($column => $value);
      $n = $db->update($this->session_table, $data, "session = '$this->session_id'");
   }
   function checkin() {
      $this->removeExpiredSessions();
      $db = $this->dbhandle;
      $data = array(
         'session_laccess' => new Zend_Db_Expr("NOW()")
      );
      $n = $db->update($this->session_table, $data, "session = '$this->session_id'");
   }
   function removeExpiredSessions() {
      $db = $this->dbhandle;
      $n = $db->delete($this->session_table, "session_laccess + $this->session_expiry < NOW()");
   }
   function approveDomainReferral($domain) {
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from(  array('a' => $this->referral_table),
			            array('cdm_domain_referral_id',
                           'database_name',
                           'root_class'
                     ));
	   $query->where('domain_name = ?',$domain);
      $result = $db->fetchAll($query);
      return (count($result)>0);
   }
}
?>