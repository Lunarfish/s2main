<?php
class CDM_AP_NYCCLocalSites {
   protected $projecttitle = "North Yorkshire SINC Panel";
   protected $targets = array('Recorder' => 'localhost/dev/cdm_neyedc_recorder.php');
   function request($method,$source=null,$args=null) {
      if (!isset($args)) $args = array();
      $args['projecttitle'] = $this->projecttitle;
      $response = null;
      switch($method) {
      case 'getMethods': {
         $methods = array(
            'getFooterText'      => array('return'    => 'footer',
                                          'title'     => 'Get footer text'),
            'getSiteCodeByName'  => array('return'    => 'sitecode',
                                          'title'     => 'Search for site by name',
                                          'arguments' => array('sitename' => 'Site name')),
            'getSiteCodeByCode'  => array('return'    => 'sitecode',
                                          'title'     => 'Search for site by name',
                                          'arguments' => array('sitename' => 'Site name')),
            'getSiteName'        => array('return'    => 'sitename',
                                          'title'     => 'Get site name',
                                          'arguments' => array('sitecode' => 'Site code')),
            'getSiteArea'        => array('return'    => 'sitearea',
                                          'title'     => 'Get site area',
                                          'arguments' => array('sitearea' => 'Site area')),
            'getCitationTypes'   => array('return'    => 'citationtype',
                                          'title'     => 'Get citation type'),
            'getSurveyDates'     => array('return'    => 'surveydate',
                                          'title'     => 'Get survey date',
                                          'arguments' => array('sitecode' => 'Site code')),
            'getSiteDescription' => array('return'    => 'sitedescription',
                                          'title'     => 'Get site description',
                                          'arguments' => array('sitecode'  => 'Site code',
                                                               'surveydate'=> 'Survey date')),
            'getSiteHabitats'    => array('return'    => 'habitats',
                                          'title'     => 'Get site habitats',
                                          'arguments' => array('sitecode' => 'Site code',
                                                               'surveydate'=> 'Survey date')),
            'getSiteSpeciesList' => array('return'    => 'specieslist',
                                          'title'     => 'Get site species list',
                                          'arguments' => array('sitecode' => 'Site code',
                                                               'surveydate'=> 'Survey date'))
         );
         $response = json_encode($methods);   
      }break;
      case 'getCitationTypes': {
         $types = array('survey'=>'From commissioned survey',
                        'best'=>'From all available data');
         $response = json_encode($types);
      }break;
      default: {
         $source = 'Recorder';
         $target = $this->targets[$source];
         $json = array();
         $json['request'] = $method;
         $json['args'] = $args;
         $json = json_encode($json);
         $response = $this->jsonPost($target,$json);
      }break;
      }
      return $response;
   }
   function jsonPost($url,$json) {
      $qs = "&j=$json";
      $ch = curl_init(); //initiate the curl session
      $timeout = 90;
      curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));
      curl_setopt($ch, CURLOPT_URL, $url); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $qs);
      curl_setopt($ch, CURLOPT_TIMEOUT, (int)$timeout);
      $response = curl_exec($ch);
      curl_close($ch);
      //$json = json_decode($response);      
      return $response;
   }
}
?>
