var encrypt = readCookie('encrypt');
function cdm_send(request) {
//alert('testsend');
   inprogress();
   var ac = new AjaxClass();
   ac.url = request['target'];
   ac.parameters = request['request'];
   ac.responder = cdm_response;
   ac.init();
   ac.send(); 
}
function cdm_response(e_response) {
   var s_response = cdm_decrypt(e_response);
//alert(s_response);
   var response = JSON.parse(s_response);
//alert(response.name + '\n' + response.description);
   completed();       
}
function AjaxClass() {
   this.Method = "POST" //OR "GET"
   this.Async = true; //OR false (asynchronous or synchronous call)
   this.request = null;
   this.url = null;
   this.parameters = null;
   this.init = function() {
      if (window.XMLHttpRequest) // if Mozilla, Safari, IE8 etc
         this.request = new XMLHttpRequest();
      else if (window.ActiveXObject) { // if IE6 or previous 
         try {
            this.request = new    ActiveXObject("Msxml2.XMLHTTP");
         } catch (e) {
            try {
               this.request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e){}
         }
      }
   };
   this.encodeParameters = function () {
      var prop,pstring;
      pstring = '';
      for(prop in this.parameters) {
         // if the parameter is an object or an array then use JSON.stringify 
         // to encode it.
         // otherwise it can simply be appended as is whether it's a number, 
         // string or boolean.
         // when it's decoded you might have to be careful of boolean false 
         // decoded as string "false" which PHP evaluates to true
         if ((typeof this.parameters[prop] == 'object') 
            || (typeof this.parameters[prop] == 'array')) { 
            pstring += prop + '=' + escape(JSON.stringify(this.parameters[prop])) + '&';
         } else pstring += prop + '=' + this.parameters[prop] + '&'; 
      }
      // get rid of the last & symbol
      pstring = pstring.replace(/&$/,"");
      this.parameters = pstring;
   }
   this.send = function () {
      if (this.request && this.url) {
         if (this.Async) this.request.onreadystatechange = this.handleResponse;
         this.request.open(this.Method, this.url , this.Async);
         if (this.parameters && typeof this.parameters != 'string') this.encodeParameters();
         if (this.Method == 'POST') {
            if (this.parameters) {
               this.request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
               this.request.setRequestHeader("Content-length", this.parameters.length);
               this.request.setRequestHeader("Connection", "close");
            }
         } else this.url = this.url + '?' + this.parameters;
         this.request.send(this.parameters);
      }
      if (!this.Async) return this.request.responseText;
   };
   var self = this; // To handle lose of focus
   this.handleResponse = function() {
   // handleResponse role is to check for the completion of the request and 
   // to refer the output from the request to the responder function
   // responder can then be overridden when the class is instantiated to do 
   // different things with responses to different requests.   
      if (self.request.readyState == 4 && self.request.status == 200) {
         var response = (parseInt(readCookie('encrypt')))?cdm_decrypt(self.request.responseText):self.request.responseText;
         self.responder(response);          
      }
   };
   this.responder = append;   
}
function append(text) {
   var p = document.createElement('P');
   p.appendChild(document.createTextNode(text));
   document.body.appendChild(p);
} 
function add_eenv(request,type,svr,sid) {
//alert('add_eenv');
   var eenv = new Object();
   var server = (svr)?svr:unescape(readCookie('cdm_server'));
   var domain = 'localhost/dev';
   domain = window.location.host;
   var paths = window.location.pathname.split('/');
   paths.pop();
   domain = domain + paths.join('/');    
   eenv.target = "http://"+server+"/eproxy.php";
   var params = new Object();
   if (request.etype) params.etype = request.etype; 
   params.session = (sid)?sid:readCookie('cdm_session_id');
   params.domain = domain; 
   params.request = getencrypted(JSON.stringify(request),type);
   eenv.request = params;
   /*
     if you override the default response, GET/POST method or send a synchronous 
     request these parameters have to be included in the envelope.
   */
   if (request.responder) eenv.responder = request.responder;
   if (request.method) eenv.method = request.method;
   if (request.sync) eenv.sync = request.sync;
   return eenv;      
}                                                              
function add_jenv(request) {
//alert('add_jenv');
   var domain = 'localhost/dev';    
   domain = window.location.host;
   var paths = window.location.pathname.split('/');
   paths.pop();paths.pop();
   domain = domain + paths.join('/');    
   var jenv = new Object();
   jenv['target'] = "http://"+domain+"/jproxy.php";   
   jenv['request'] = request;
   /*
     if you override the default response, GET/POST method or send a synchronous 
     request these parameters have to be included in the envelope.
   */
   if (request.responder) jenv.responder = request.responder;
   if (request.method) jenv.method = request.method;
   if (request.sync) jenv.sync = request.sync;
   return jenv;
}
function cdm_getexternaldatakeys() {
   var k = new S2AB();
   k.cd(s2_getcookie('PHPSESSID'));
   return JSON.parse(k.bd());
}
function cdm_wrapper(request,type,svr,sid) {
   var encrypt = parseInt(readCookie('encrypt'));
   var client = unescape(readCookie('cdm_client'));
   var server = (svr)?svr:(request.svr)?request.svr:unescape(readCookie('cdm_server'));
   type = (type)?type:(request.etype)?request.etype:null;
   sid = (sid)?sid:(request.sid)?request.sid:null;
   if (encrypt||(type=='AB')) {
      request.request.SID = (sid)?sid:s2_getcookie('PHPSESSID');
      request = add_eenv(request,type,server,sid);
   }
   var remote = (client != server);
   if (remote) request = add_jenv(request);
   return request;      
}  
function makeqs (params) {
   var strings = new Array();
   for(prop in params) {
      strings.push(prop + '=' + params[prop]);     
   }
   return strings.join('&');  
}  
/*                         
function cdm_send(request) {
//alert('testsend');
   inprogress();
   var http = getHTTPObject();
   var url = request['target'];
   http.open("POST", url, true);
   var params = 'request='+request['request'];//makeqs(request['request']);
   //Send the proper header information along with the request
   http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
   http.setRequestHeader("Content-length", params.length);
   http.setRequestHeader("Connection", "close");

   http.onreadystatechange = function() {//Call a function when the state changes.
	  if (http.readyState == 4 && http.status == 200) {
	     var response = (readCookie('encrypt'))?cdm_decrypt(http.responseText):http.responseText;
	     cdm_response(response);
	  }
	}
	http.send(params);
}
*/
function cdm_decrypt(e_response,type) {
   var clear = (parseInt(readCookie('encrypt'))||(type=='AB'))?unescape(decryptit(e_response,type)):e_response;
   clear = clear.replace(/[\u0000]+$/,'');
   return clear;    
}
function cdm_encrypt(clear,type) {
   var encrypted = getencrypted(clear,type);
   return encrypted;    
}
