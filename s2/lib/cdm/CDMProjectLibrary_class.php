<?php
class CDMProjectLibrary {
/*
   CREATE TABLE cdm_project_library (
      project INT UNSIGNED NOT NULL,
      library VARCHAR(100) NOT NULL,
      classname VARCHAR(50) NOT NULL,
      PRIMARY KEY (project)
   );
   INSERT INTO cdm_project_library (project,library,classname) VALUES 
   (1,'CDM_AP_NYCCLocalSites_class.php','CDM_AP_NYCCLocalSites'),
   (2,'CDM_AP_ERLocalSites_class.php','CDM_AP_ERLocalSites');
*/
   public $tablename = 'cdm_project_library';
   protected $dbhandle;
   public $project;
   public $library;
   public $classname;
   function CDMProjectLibrary() {
      $this->genDBConnection();
   }
   function populate($project) {
      if ($project) {
         $this->project = $project;
         $db = $this->dbhandle;
         $query = $db->select();
         $query->from(  array('a' => $this->tablename),
			               array(
                           'library',
                           'classname'
                        ));
	      $query->where('project = ?',($project));
         $result = $db->fetchAll($query);
         if (count($result)==0) {
         } else {
            foreach($result as $row) {
               $this->library = $row['library'];
               $this->classname = $row['classname'];
            }
         }
      }
   }
   function genDBConnection() {
      $dbConfig = array(
	     'host'		=> 'localhost',
	     'username'	=> 'hedc',
	     'password'	=> 'hedc',
	     'dbname'	=> 'hedcmeta'
      );
      $this->dbhandle = Zend_Db::factory('Pdo_Mysql', $dbConfig);
   }
}
?>
