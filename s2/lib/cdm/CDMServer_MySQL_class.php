<?php
class CDMServer extends CDMServer_Gen {
   protected $session_expiry = 'INTERVAL 1 HOUR';
   protected $session_table = 'cdm_server_session';
   protected $referral_table = 'cdm_server_domain_referral';
   function CDMServer() {
      parent::CDMServer_Gen();
   }
   function populate($sessionid,$cdmid=null) {
      /* The point of the CDM client and CDM server session tables is to 
         maintain the encryption keys on the channels. The session is set by 
         the client and passed to the server and the server sets and controls 
         the cdm_session_id. So long as you the client to server is a many to 
         one relationship then the cdm_session_id should remain unique.
         
         This means that cdm_session_id is the primary key on the server table 
         and is a foreign key on the client table. The primary key on the client 
         table is the session id.
      */
      $newsession = false;
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from(  array('a' => $this->session_table),
		               array('cdm_session_id',
                           'session',
                           'username',
                           'key1',
                           'key2',
                           'session_started',
                           'session_laccess',
                     ));
      if (isset($cdmid)) $query->where('cdm_session_id = ?',($cdmid*1));
      else $query->where('session = ?',$sessionid);      
      $result = $db->fetchAll($query);
      if (count($result)==0) {
         $newsession = true;         
      } else {
         foreach($result as $row) {
            $this->cdm_session_id = $row['cdm_session_id'];
            $this->session_id = $row['session'];
            $this->username = $row['username'];
            $this->keys[] = $row['key1'];
            $this->keys[] = $row['key2'];
         }
      }
      
      if ($newsession == true && $sessionid) {
         $this->setSessionID($sessionid);
         $this->insert();
      }
      return !$newsession;
   }
   function insert() {
      $db = $this->dbhandle;
      $data = array(
         'session'         => $this->session_id,
         'session_started' => new Zend_Db_Expr("NOW()"),
         'session_laccess' => new Zend_Db_Expr("NOW()"),
      );
      $db->insert($this->session_table, $data);
      $this->cdm_session_id = $db->lastInsertId();
   }
   function update($column,$value) {
      if ($this->getSessionID() != null) { 
         $db = $this->dbhandle;
         $data = array($column => $value);
         $n = $db->update($this->session_table, $data, "cdm_session_id = $this->cdm_session_id");
      }
   }
   function removeExpiredSessions() {
      $db = $this->dbhandle;
      $n = $db->delete($this->session_table, "session_laccess + $this->session_expiry < NOW()");
      /*
         check number of sessions in table and if there are none reset the 
         id sequence
      */
      $query = $db->select();
      $query->from(  array('a' => $this->session_table),
			            array('cdm_session_id'));
      $result = $db->fetchAll($query);
      if (count($result)==0) $db->getConnection()->exec("ALTER TABLE $this->session_table AUTO_INCREMENT = 0");
   }
   function approveDomainReferral($domain) {
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from(  array('a' => $this->referral_table),
			            array('cdm_domain_referral_id',
                           'database_name',
                           'root_class'
                     ));
	   $query->where('domain_name = ?',$domain);
      $result = $db->fetchAll($query);
      $approved = (count($result)>0);
      
      return $approved;
   }
}
?>