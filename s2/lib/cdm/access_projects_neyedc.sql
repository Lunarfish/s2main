-- phpMyAdmin SQL Dump
-- version 2.9.1.1
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Mar 22, 2010 at 08:58 AM
-- Server version: 4.1.21
-- PHP Version: 5.2.0
-- 
-- Database: `neyedc_org_uk_-_site`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `access_project_domains`
-- 

CREATE TABLE `access_project_domains` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `project` int(10) unsigned NOT NULL default '0',
  `domain` varchar(200) NOT NULL default '',
  `access` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- 
-- Dumping data for table `access_project_domains`
-- 

INSERT INTO `access_project_domains` (`id`, `project`, `domain`, `access`) VALUES 
(1, 1, 'neyedc.co.uk', 3),
(2, 2, 'neyedc.co.uk', 3),
(3, 1, 'humber-edc.org.uk', 3),
(4, 2, 'humber-edc.org.uk', 3),
(5, 1, 'northyorks.gov.uk', 2),
(6, 2, 'eastriding.gov.uk', 1),
(8, 2, 'naturalengland.gov.uk', 1),
(10, 1, 'naturalengland.org.uk', 2);

-- --------------------------------------------------------

-- 
-- Table structure for table `access_project_levels`
-- 

CREATE TABLE `access_project_levels` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `project` int(10) unsigned NOT NULL default '0',
  `level` varchar(10) default NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- 
-- Dumping data for table `access_project_levels`
-- 

INSERT INTO `access_project_levels` (`id`, `project`, `level`, `description`) VALUES 
(1, 1, 'None', ''),
(2, 1, 'Public', 'This level of access is for public interested parties who are not explicitly involved in the project.'),
(3, 1, 'Member', 'This is the level of access you would give to a member of a SINC panel for example'),
(4, 1, 'Admin', 'Users or domains with Admin access are able to control who has access to a project'),
(5, 2, 'None', ''),
(6, 2, 'Public', 'This level of access is for public interested parties who are not explicitly involved in the project.'),
(7, 2, 'Member', 'This is the level of access you would give to a member of a SINC panel for example'),
(8, 2, 'Admin', 'Users or domains with Admin access are able to control who has access to a project'),
(9, 3, 'None', ''),
(10, 3, 'Public', 'This level of access is for public interested parties who are not explicitly involved in the project.'),
(11, 3, 'Member', 'This is the level of access you would give to a member of a SINC panel for example'),
(12, 3, 'Admin', 'Users or domains with Admin access are able to control who has access to a project');

-- --------------------------------------------------------

-- 
-- Table structure for table `access_project_users`
-- 

CREATE TABLE `access_project_users` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `project` int(10) unsigned NOT NULL default '0',
  `user` varchar(200) NOT NULL default '',
  `access` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

-- 
-- Dumping data for table `access_project_users`
-- 

INSERT INTO `access_project_users` (`id`, `project`, `user`, `access`) VALUES 
(2, 1, 'louise.moss@northyorks.gov.uk', 2),
(3, 1, 'matthew.millington@northyorks.gov.uk', 2),
(6, 2, 'david.renwick@eastriding.gov.uk', 2),
(7, 1, 'andrew.craven@naturalengland.org.uk', 2),
(8, 3, 'dan.jones@humber-edc.org.uk', 3),
(9, 1, 'dan.mcandrew@harrogate.gov.uk', 2),
(10, 2, 'sue.ogilvy@naturalengland.org.uk', 2),
(11, 1, 'm.hammond300@googlemail.com', 2),
(12, 1, 'sarah.kettlewell@harrogate.gov.uk', 2),
(13, 3, 'carolyn.barber@rotherham.gov.uk', 3),
(14, 1, 'martin.fuller@environment-agency.gov.uk', 2),
(15, 1, 'julia.casterton@northyorks.gov.uk', 2),
(16, 1, 'ann.hanson@fwag.org.uk', 2),
(17, 2, 'ann.hanson@fwag.org.uk', 2),
(18, 1, 'Amanda.Coward@naturalengland.org.uk', 2),
(19, 1, 'jeremy.dick@forestry.gsi.gov.uk', 2),
(20, 1, 'bernadette.lobo@loboecology.co.uk', 2),
(21, 2, 'vaughan.grantham@eastriding.gov.uk', 2),
(22, 2, 'jill.howells@environment-agency.wales.gov.uk', 2),
(23, 1, 'jill.howells@environment-agency.wales.gov.uk', 2);

-- --------------------------------------------------------

-- 
-- Table structure for table `access_projects`
-- 

CREATE TABLE `access_projects` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `project` varchar(200) NOT NULL default '',
  `admin` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `access_projects`
-- 

INSERT INTO `access_projects` (`id`, `project`, `admin`) VALUES 
(1, 'North Yorkshire SINC Panel', 'julia.casterton@northyorks.gov.uk'),
(2, 'East Riding of Yorkshire SINC Panel', 'david.renwick@eastriding.gov.uk'),
(3, 'Rotherham BRC Data Search', 'bill.ely@rotherham.gov.uk');

create table access_project_login (
  id int unsigned auto_increment not null,
  project int unsigned not null,
  islocal boolean,
  delegatedto varchar(100),
  primary key (id)
);

insert into access_project_login (project,islocal,delegatedto) values
(1,false,'www.neyedc.org.uk/snc'),
(2,false,'www.neyedc.org.uk/snc'),
(3,true,null);
