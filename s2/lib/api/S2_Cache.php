<?php
// function points to base of the NBN API
function s2api_getapibaseurl() {
   $url = "http://${server}/${folder}/api.php";
   return $url;
}
// function to turn camel case property: taxonVersionKey
// into capitalised column name style: Taxon_Version_Key
// will need equivalent in JS.  
function s2api_colnamefromjsonprop($prop) {
   $prop = preg_replace('/([A-Z])/','_$1',$prop);
   $prop = preg_replace_callback('/^([a-z])/',function($matches){return strtoupper($matches[1]);},$prop);
   return $prop;
}

class S2_API_Cache extends DBT {
   protected $tablename = 'S2_API_Cache';
   protected $displayname = 'API Cache';
   protected $expiry = '6M';
   protected $perpage = 25;
   protected $user;
   protected $alts = array();
   protected $columns = array(
      'S2_API_Cache_ID' => 'Unique Key',
      'Endpoint'        => 'Medium Text',
      'JSON'            => 'Long Text', 
      'Expiry'          => 'Date'
   );
   protected $domain = 'api';
   protected $permissions = array();
   protected $primingdata = array();
   function get($endpoint,$exp=null) { 
      $exp = ($exp == null)?$this->expiry:$exp;
      if (!$this->exists()) $this->create();
      $data = $this->select('Endpoint',$endpoint);
      if (isset($data) && count($data) > 0) {
         $id = $data[0]['S2_API_Cache_ID'];
         $expiry = $data[0]['Expiry'];
         $valid = $this->valid($expiry);
         if ($valid) $json = $data[0]['JSON'];
         else if ($id) $this->delete($this->getpk(),$id);                               
      } 
      if (!isset($json)) {
         $ch = curl_init();
         
         curl_setopt($ch, CURLOPT_URL, $endpoint);
         
         $sid = $_COOKIE['PHPSESSID'];
         $cookie_jar = tempnam('/tmp','cookie');
         curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_jar);
         curl_setopt($ch, CURLOPT_COOKIE, "PHPSESSID=$sid;");
         curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_jar);
         
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_HEADER, false);
         $json = curl_exec($ch);
         // replace html element tags.
         $json = preg_replace('/\<\/*[a-z,A-Z,0-9]+\/*\>/','',$json); 
         
         $ival = $this->getinterval($this->expiry);
         $data = array(
            'Endpoint'  => $endpoint,
            'JSON'      => addslashes($json),
            'Expiry'    => new Zend_Db_Expr("NOW() + $ival")  
         );
         $this->setdata($data);
         $this->insert();
      }
      return $json;      
   }
   function valid($date) {
      // 0 = expired , 1 = valid
      $expiry = preg_replace('/(\d{2})\/(\d{2})\/(\d{4})/','$3-$2-$1',$date);
      $today = date("Y-m-d",gmmktime());
      $valid = ($expiry >= $today);
      return $valid; 
   }   
}
?>