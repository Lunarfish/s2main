<?php
class RCD_Survey extends DBT {
   protected $tablename = 'RCD_Survey';
   protected $displayname = 'Survey';
   protected $perpage = 10;
   protected $user;
   protected $show = array('RCD_Adopted_Square');
   protected $columns = array(
      'RCD_Survey_ID'   => 'Unique Key',
      'RCD_Project_ID'   => '{"DataType":"LINKEDTO","TargetType":"RCD_Project","TargetField":"RCD_Project_ID","Mandatory":1,"Current":1}',
      'Name'            => 'Short Text',
      'Description'     => 'Long Text',
      'From_Date'       => 'Date',
      'To_Date'         => 'Date'
   );
   protected $domain = 'record';
   protected $permissions = array();
   protected $primingdata = array();
   protected $defaultpermissions = array(
      'USR_Permissions' => '
"User_Id","Domain","Label","Value"
1,"record","Registered User",1
1,"record","Survey Contributor",1
1,"record","Survey Administrator",1
1,"record","System Administrator",1',
      'USR_Default_Permissions' => '
"Domain","Label","Description","Value","Anon" 
"record","System Administrator","Add and edit surveys",0,0
"record","Survey Administrator","Add and edit survey data and setup",0,0
"record","Survey Contributor","Add survey data",0,0
"record","Registered User","View unprotected data",1,0'   
   );   
} 
class RCD_Project extends DBT {
   protected $tablename = 'RCD_Project';
   protected $displayname = 'Project';
   protected $perpage = 10;
   protected $user;
   protected $show = array('RCD_Survey');
   protected $columns = array(
      'RCD_Project_ID'   => 'Unique Key',
      'Name'            => 'Short Text',
      'Description'     => 'Long Text',
      'Project_Lead'    => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"]}'
   );
   protected $domain = 'record';
   protected $permissions = array();
   protected $primingdata = array();   
}
class RCD_Adopted_Square extends DBT {
   protected $tablename = 'RCD_Adopted_Square';
   protected $displayname = 'Adopted Square';
   protected $perpage = 10;
   protected $user;
   protected $show = array('RCD_Record');
   protected $columns = array(
      'RCD_Adopted_Square_ID'   => 'Unique Key',
      'RCD_Survey_ID'   => '{"DataType":"LINKEDTO","TargetType":"RCD_Survey","TargetField":"RCD_Survey_ID","Mandatory":1,"Current":1}',
      'Grid_Reference'  => '{"DataType":"Short Text","Derives":[{"Column":"On_Map","Method":"s2_getgrsquare","Params":[{"Type":"Value"}]}]}',
      'On_Map'          => '{"DataType":"Map Data","Derives":[{"Column":"Grid_Reference","Method":"s2_getgr","Params":[{"Type":"Value"}]}]}',
      'On_Date'         => 'Date',
      'Recorder'        => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person"]}',
      'RCD_Square_Status_Type_ID'   => '{"DataType":"LINKEDTO","TargetType":"RCD_Square_Status_Type","TargetField":"RCD_Square_Status_Type_ID","Mandatory":1,"Current":1}'
   );
   protected $domain = 'record';
   protected $permissions = array();
   protected $primingdata = array(); 
   function getname() {
      return $this->data['Grid_Reference'];
   }  
}
class RCD_Square_Status_Type extends DBT {
   protected $tablename = 'RCD_Square_Status_Type';
   protected $displayname = 'Square Status Type';
   protected $perpage = 10;
   protected $user;
   protected $columns = array(
      'RCD_Square_Status_Type_ID'   => 'Unique Key',
      'Name'            => 'Short Text',
      'Descriptipn'     => 'Long Text'
   );
   protected $domain = 'record';
   protected $permissions = array();
   protected $primingdata = array();   
}
class RCD_Record extends DBT {
   protected $tablename = 'RCD_Record';
   protected $displayname = 'Record';
   protected $perpage = 10;
   protected $user;
   protected $revision_tables = array('Taxa'=>'RCD_Record_Taxon_Revision');
   protected $show = array('RCD_Record_Taxon_Revision','RCD_Record_Attribute');
   protected $columns = array(
      'RCD_Record_ID'   => 'Unique Key',
      'RCD_Adopted_Square_ID'   => '{"DataType":"LINKEDTO","TargetType":"RCD_Adopted_Square","TargetField":"RCD_Adopted_Square_ID","Mandatory":1,"Current":1}',
      'On_Date'         => 'Date',
      'Location'        => '{"DataType":"Map Data","Derives":[{"Column":"Grid_Reference","Method":"s2_getgr","Params":[{"Type":"Value"}]}]}',
      'Grid_Reference'  => '{"DataType":"Short Text","Derives":[{"Column":"Location","Method":"s2_getgrsquare","Params":[{"Type":"Value"}]}]}',
      'Recorder'        => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person"],"Mandatory":1}',   
      'Taxa'            => '{"DataType":"Taxon Version Key","Interface":"TVK","Use":"Name","Revise":1}'
   );
   protected $domain = 'record';
   protected $permissions = array();
   protected $primingdata = array();   
      function addrevision($col,$newval,$reason) {
      $id = $this->getid();
      if(isset($this->revision_tables) && isset($this->revision_tables[$col])) {
         $type = $this->revision_tables[$col]; 
         $oldval = $this->data[$col];
         $dbti = new $type();
         $dbti->autorevision($id,$oldval,$newval,$reason);                                 
      }
   }
   function getname() {
      return $this->derivename(array('On_Date','Grid_Reference'));
   } 
}
class RCD_Record_Taxon_Revision extends DBT {
   protected $tablename = 'RCD_Record_Taxon_Revision';
   protected $displayname = 'Record Taxon Revision';
   protected $perpage = 25;
   protected $user;
   protected $parent = 'RCD_Record';
   protected $columns = array(
      'RCD_Record_Taxon_Revision_ID' => 'Unique Key',
      'RCD_Record_ID'    => '{"DataType":"LINKEDTO","TargetType":"RCD_Record","TargetField":"RCD_Record_ID","Mandatory":1,"Current":1}',
      'Date'            => 'Date',
      'Reason'          => '{"DataType":"Short Text"}',
      'New_Taxa'        => '{"DataType":"Taxon Version Key","Interface":"TVK","Use":"Name","Revise":0}',
      'Old_Taxa'        => '{"DataType":"Taxon Version Key","Interface":"TVK","Use":"Name","Revise":0}',
      'Recorder'        => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person"]}'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $primingdata = array(
   );
   function autorevision($rid,$oldval,$newval,$reason) {
      $data = array(
         'RCD_Record_ID' => $rid,
         'Old_Taxa'     => $oldval,
         'New_Taxa'     => $newval,
         'Date'         => new Zend_Db_Expr('NOW()'),
         'Reason'       => $reason
      );
      $this->setdata($data);
      return $this->insert();
   } 
   function insert() {
      $inserted = parent::insert();
      if ($inserted) {
         $rec = new RCD_Record();
         //$rec->loaddata($this->data['RCD_Record_ID']);
         $rec->setdata(array('Taxa'=>$this->data['New_Taxa']));
         $rec->update('RCD_Record_ID',$this->data['RCD_Record_ID']);
      }
   }  
   function getname() {
      return $this->derivename(array('Date','Reason'));
   }
}
class RCD_Record_Attribute extends DBT {
   protected $tablename = 'RCD_Record_Attribute';
   protected $displayname = 'Record Attribute';
   protected $perpage = 10;
   protected $user;
   protected $columns = array(
      'RCD_Record_Attribute_ID'  => 'Unique Key',
      'RCD_Record_ID'            => '{"DataType":"LINKEDTO","TargetType":"RCD_Record","TargetField":"RCD_Record_ID","Mandatory":1,"Current":1}',
      'RCD_Attribute_Type_ID'    => '{"DataType":"LINKEDTO","TargetType":"RCD_Attribute_Type","TargetField":"RCD_Attribute_Type_ID","Mandatory":1,"Current":1}'
   );
   protected $domain = 'record';
   protected $permissions = array();
   protected $primingdata = array();   
}
class RCD_Attribute_Type extends DBT {
   protected $tablename = 'RCD_Attribute_Type';
   protected $displayname = 'Attribute Type';
   protected $perpage = 10;
   protected $user;
   protected $columns = array(
      'RCD_Attribute_Type_ID'       => 'Unique Key',
      'Name'                        => 'Short Text',
      'Description'                 => 'Long Text',
      'RCD_Attribute_Value_Type_ID' => '{"DataType":"LINKEDTO","TargetType":"RCD_Attribute_Value_Type","TargetField":"RCD_Attribute_Value_Type_ID","Mandatory":1,"Current":1}'
   );
   protected $domain = 'record';
   protected $permissions = array();
   protected $primingdata = array();   
}
class RCD_Attribute_Value_Type extends DBT {
   protected $tablename = 'RCD_Attribute_Value_Type';
   protected $displayname = 'Attribute Value Type';
   protected $perpage = 10;
   protected $user;
   protected $columns = array(
      'RCD_Attribute_Value_Type_ID' => 'Unique Key',
      'Name'                        => 'Short Text',
      'Description'                 => 'Long Text'
   );
   protected $domain = 'record';
   protected $permissions = array();
   protected $primingdata = array();   
}
class RCD_Taxon_Revision extends DBT {
   protected $tablename = 'RCD_Taxon_Revision';
   protected $displayname = 'Taxon Revision';
   protected $perpage = 25;
   protected $user;
   protected $revises = array('RCD_Record'=>'Taxa'); // array of key(table) = value(column)
   protected $columns = array(
      'RCD_Taxon_Revision_ID'  => 'Unique Key',
      'Old_Taxa'              => '{"DataType":"Taxon Version Key","Interface":"TVK","Use":"Name","Revise":0}',
      'New_Taxa'              => '{"DataType":"Taxon Version Key","Interface":"TVK","Use":"Name","Revise":0,"Inherit":1,"Derives":[{"Column":"Data_From","Method":"s2_getdatecolumnvalue","Params":[{"Type":"Static","Value":"S2_Site_Visit"},{"Type":"Value"},{"Type":"Static","Value":"Survey_Date"}]},{"Column":"Old_Taxa","Method":"s2_getcolumnvalue","Params":[{"Type":"Static","Value":"RCD_Record"},{"Type":"Column","Value":"RCD_Record_ID"},{"Type":"Static","Value":"Taxa"}]}]}',
      'Date'                  => 'Date'
   );
   protected $domain = 'species';
   protected $permissions = array(
      'Def'   => 'Registered User'
   );
   protected $primingdata = array(
   );
   function insert() {
      if (!$this->isduplicate()) parent::insert();
      $otvk = $this->data['Old_Taxa'];
      $ntvk = $this->data['New_Taxa'];
      foreach($this->revises as $type => $col) {
         $dbti = new $type();
         $recs = $dbti->select($col,$otvk);
         foreach($recs as $num => $data) {
            $data[$col] = $otvk;
            $dbti->setdata($data);
            $dbti->addrevision('Taxa',$ntvk,'Taxonomic Revision');
         }  
      } 
   }   
}

?>