<!--
function refocus() {
   var requiresreload = readCookie('requiresreload');
   if (requiresreload) {
      window.location.reload();
      eraseCookie('requiresreload');
   }
   return true;
}
function removeElements(nodeids) {
   var parent;
   var child;
   for (var i=0;i<nodeids.length;i++) {
      while(child = document.getElementById(nodeids[i])) {
         parent = child.parentNode;
         parent.removeChild(child);
      }
   }
   return true;
}
function setreload() {
   createCookie('requiresreload',1,5);
}

/*
   For IE6 the cookie limitations are quite restrictive so you have to use the 
   userData method to persist larger volumes of data. 
   
   On non-IE browsers the try fails at the addBehavior step so there's not a 
   huge penalty to running this code.
   
   noud (no user data) can be set to true to force IE6 to use the 
   document.cookie rather than userData behavior storage. 
   
   noud is specified on all the authentication settings.    
*/
function createCookie(name,value,mins,noud) {
//alert('Saving cookie: '+name+'\n'+value);
   var dt,err,node;
   var expires;
   if (noud) {
      if (mins) {
   		dt = new Date();
   		dt.setTime(dt.getTime()+(mins*60000));
   		expires = "; expires="+dt.toGMTString();
   	}
   	else expires = "";
   	document.cookie = name+"="+value+expires+"; path=/";
   } else {
   	try {
         node = document.createElement('TEXTAREA');
         node.style.display = 'none';
         node.id = 's2ud_'+name;
         document.lastChild.appendChild(node);
         node.addBehavior("#default#userData");
         if (mins != -1) node.setAttribute(node.id,value);
         else node.removeAttribute(node.id);
         node.save(node.id);
         document.lastChild.removeChild(node);
      } catch (err) {
         if (node) document.lastChild.removeChild(node);
         if (mins) {
      		dt = new Date();
      		dt.setTime(dt.getTime()+(mins*60000));
      		expires = "; expires="+dt.toGMTString();
      	}
      	else expires = "";
      	document.cookie = name+"="+value+expires+"; path=/";
      }
   }
	return true;
}

function readCookie(name) {
	var cval,err,node;
	try {
      node = document.createElement('TEXTAREA');
      node.id = 's2ud_'+name;
      node.style.display = 'none';
      document.lastChild.appendChild(node);
      node.addBehavior("#default#userData");
      node.load(node.id);
      cval = node.getAttribute(node.id);
      document.lastChild.removeChild(node);
   } catch (err) {
      if (node) document.lastChild.removeChild(node);
   }
   // since some cookies are set by PHP they may not all use the userdata workaround
   if (!cval) {
   	var nameEQ = name + "=";
   	var ca = document.cookie.split(';');
   	for(var i=0;i < ca.length;i++) {
   		var c = ca[i];
   		while (c.charAt(0)==' ') c = c.substring(1,c.length);
   		if (c.indexOf(nameEQ) == 0) cval = c.substring(nameEQ.length,c.length);
   	}
	}
	return cval;
}

function eraseCookie(name) {
   try {
      node = document.createElement('TEXTAREA');
      node.id = 's2ud_'+name;
      node.style.display = 'none';
      document.lastChild.appendChild(node);
      node.addBehavior("#default#userData");
      node.load(node.id);
      node.removeAttribute(node.id);
      node.save(node.id);
      document.lastChild.removeChild(node);
   } catch (err) {
      if (node) document.lastChild.removeChild(node);
   	createCookie(name,0,-1);
   }
}
function getQueryVariable(variable,url) {
//alert("FUNCTION: getQueryVariable");
   var query,vars,pair;
   if (url) query = url.replace(/.+\?/,'');
   else query = window.location.search.substring(1);
   vars = query.split("&");
   for (var i=0;i<vars.length;i++) {
      pair = vars[i].split("=");
      if (pair[0] && pair[0] == variable) {
         return pair[1];
      }
   } 
   //alert('Query Variable ' + variable + ' not found');
   return false;
}
function getSessionId() {
	var nameEQ = "PHPSESSID=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) {
	      return c.substring(nameEQ.length,c.length);
      }
	}
	return null;
}

function carryQueryVariables() {
//alert("FUNCTION: carryQueryVariables");
   var query = window.location.search.substring(1);
   var vars = query.split("&");
   var pairs = new Array();
   for (var i=0;i< vars.length ; i++) {
      pairs = vars[i].split('=');
      if (pairs[0].indexOf('mapImage')== -1)
         document.getElementById(pairs[0]).value = unescape(pairs[1]);
   }
   return true;
}
function setBanner() {
    var banner_dir = './images/banners/';
    var banner_images = new Array('banner_khp_chunks.jpg',
                                  'banner_nettle_chunks.jpg',
                                  'banner_orchid_chunks.jpg',
                                  'banner_cloud_chunks.jpg',
                                  'banner_wave_chunks.jpg'/*,
                                  'banner_reeds_chunks.jpg'*/);
    var now = new Date();
    var rand = now % banner_images.length;
    var el_banner = document.getElementById('banner');
    var src_image = 'url(' + banner_dir + banner_images[rand] + ')';
    el_banner.style.backgroundImage = src_image;
    //alert (rand + " : " + src_image);
    return true;
}
function setCurrent() {
    node = document.getElementById('operation');
    value = node.value;
    document.getElementById(value).className = 'current';
}
function launchDialog(url, x, y) {
//alert ('FUNCTION: launchDialog');
    var popWin = null;
    var bits = url.split('?');
    var wname = bits[0];
    /*
      apparently internet explorer doesn't like . or / in the 
      window name so the next two lines take the page url and strip
      the ./ from the front and .php from the end.
      
      not sure whether it will work with directory/page urls.   
    */
    wname = wname.replace(/\//g, '_');
    wname = wname.replace(/\./, '');
    wname = wname.replace(/\.php/,'');
    if (!popWin || !popWin.open) {
        if (!x || x == '') x = 740;
        if (!y || y == '') y = 400;
        var attributes = "width=" + x + "px,height=" + y + "px,toolbar=0,status=0,scrollbars=1,resizable=1,title=0";
//alert(url);
        popWin = window.open(url, wname, attributes);
    }
    popWin.focus();
    return false;
}

function launchImageViewer (image,title) {
   var popWin = null;
   var url = './imageServer.php?image=' + image;
   attributes = "toolbar=0,status=0,scrollbars=1,resizable=1,title=1"
   popWin = window.open(url,title,attributes);
   popWin.focus();
   return false;
}
function launchPointDialog (url, id) {
    node = document.getElementById('select:coordinateSystem');
    tokens = id.split(':');
    pointNumber = tokens[tokens.length-1];
    index = node.selectedIndex;
    options = node.options;
    coords = (options[index]).value;
    url += '?coords=' + coords;
    url += '&point=' + pointNumber;
    launchDialog(url, id);
}
/* 
    this function should have a url and can then be used by anything to
    create child locations, organisations, individuals, classifications etc
    
    name change to launchDefinitionDialog(url, parentNamespace, id)
*/
function launchDefinitionDialog(page, namespace) {
    var url = page;
    var org_id = '';
    var checked = getCheckedList(namespace);
    for (i=0;i<checked.length;i++) {
        org_id += checked[i].id + ':';
    }
    // remove last : from string to avoid null last element
    org_id = org_id.substr(0,(org_id.length-1));
    url += '?org_id=' + org_id;
    launchDialog(url);
}
function showDiv(id) {
    var node = document.getElementById(id);
    node.style.visibility = 'visible';
    node.style.height = 'auto';
    node.style.width = 'auto';
    
}
function hideDiv(id) {
    var node = document.getElementById(id);
    node.style.visibility = 'hidden';
    node.style.height = '0px';
    node.style.width = '0px';
}
function addPoint(id) {
    parent = document.getElementById('boundaries');
    node = parent.lastChild;
    newNode = node.cloneNode(true);
    points = parent.childNodes.length;
    newNode.id = "point:" + points;
    newNode.lastChild.id = 'delete:' + points;
    parent.appendChild(newNode);
    document.getElementById(id).blur();
    return false;
}
function deletePoint(id) {
    tokens = id.split(':');
    pointNumber = tokens[1];
    pointId = 'point:' + pointNumber;
    parent = document.getElementById('boundaries');
    if (parent.childNodes.length > 1) {
        child = document.getElementById(pointId);
        parent.removeChild(child);
    } else {
        alert('Locations must have at least one defined boundary point. This can be as simple as a name with no coordinate reference.');
    }
    return false;
}
function definePoint() {
    validate_coords();
}
function updateElement(elementId,value) {
   document.getElementById(elementId).value = value;
   return true;
}
function shiftElement(div,margin) {
   document.getElementById(div).style.marginLeft = margin;
   return true;
}
//-->
