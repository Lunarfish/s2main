function showdialog() {
   var dims = getScrollXY();
   var sTop = dims[1];
   var node = document.getElementById('screen');
   node.style.marginTop = sTop+'px';
   var node = document.getElementById('dialog-container');
   node.style.marginTop = sTop+'px';
   document.getElementById('main').style.zIndex = 1;
   document.getElementById('screen').style.zIndex = 2;
   document.getElementById('screen').style.display = 'block';
   document.getElementById('dialog-container').style.zIndex = 3;
   return true;
}
function hidedialog() {
   document.getElementById('dialog-container').style.zIndex = 1;
   document.getElementById('screen').style.zIndex = 2;
   document.getElementById('screen').style.display = 'none';
   document.getElementById('main').style.zIndex = 3;
//document.getElementById('filter_session').blur();
}
var mobile,startx,starty,pagex,pagey,correctx,correcty;
var dialog;
function Coordinate (x, y, o) {
    this.x = x;
    this.y = y;
    this.orientation = o;
}
function getsize(element) {
   var dims = new Coordinate();
   dims.x = element.style.width;
   dims.y = element.style.height;
   if (dims.x && (dims.x.indexOf('%') == -1)) dims.x = stripPX(dims.x);
   else dims.x = element.width;
   if (dims.y && (dims.y.indexOf('%') == -1)) dims.y = stripPX(dims.y);
   else dims.y = element.height;
   return dims;
}
function getScrollXY() {
  var scrOfX = 0, scrOfY = 0;
  if( typeof( window.pageYOffset ) == 'number' ) {
    //Netscape compliant
    scrOfY = window.pageYOffset;
    scrOfX = window.pageXOffset;
  } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
    //DOM compliant
    scrOfY = document.body.scrollTop;
    scrOfX = document.body.scrollLeft;
  } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
    //IE6 standards compliant mode
    scrOfY = document.documentElement.scrollTop;
    scrOfX = document.documentElement.scrollLeft;
  }
  return [ scrOfX, scrOfY ];
}
function follow(element,e) {
   var o,mx,my;
   if (mobile) mobile = false;
   else {
      mobile = true;
      if (typeof window.event !="undefined") {
         startx = truebody().scrollLeft + event.clientX - element.offsetLeft;
         starty = truebody().scrollTop + event.clientY - element.offsetTop;
      } else if (typeof e != "undefined") {
         startx = e.pageX - element.offsetLeft;
         starty = e.pageY - element.offsetTop;
      } 
      var isize = getsize(element);
      correctx = Math.floor(isize.x/2);
      correcty = Math.floor(isize.y/2);
      dialog = element;
   }
   return true;
}
function followmouse(e) {
   if (mobile) {
      if (typeof e != "undefined") {
        pagey = e.pageY - starty;
        pagex = e.pageX - startx;
      } else if (typeof window.event !="undefined") {
        pagey = truebody().scrollTop + event.clientY - starty;
        pagex = truebody().scrollLeft + event.clientX - startx;
      }
      setDialogMargins(pagex,pagey);
   } 
   return true;
}
function setDialogMargins(dx,dy) {
   if (mobile && dialog) {
      dialog.style.marginTop = dy + 'px';  
      dialog.style.marginLeft = dx + 'px';
   }
   return true;
}
var docheight;
document.onmousemove=followmouse; 
function setdocheight() {
   if( typeof( window.innerWidth ) == 'number' ) {
   //Non-IE
      myWidth = window.innerWidth;
      myHeight = window.innerHeight;
      // on mozilla the space used by the vertical scroll bar is included so 
      // you have to test for it and remove it.
      if (document.documentElement.clientHeight > myHeight) myWidth -= 17;
   } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
   //IE 6+ in 'standards compliant mode'
      myWidth = document.documentElement.clientWidth;
      myHeight = document.documentElement.clientHeight;
   } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
   //IE 4 compatible
      myWidth = document.body.clientWidth;
      myHeight = document.body.clientHeight;
   }
   document.getElementById('screen').style.width = myWidth;
   document.getElementById('screen').style.height = myHeight;
   docheight = document.all
      ? Math.max(truebody().scrollHeight, truebody().clientHeight) 
      : Math.max(document.body.offsetHeight, window.innerHeight);
   return true;
}
function stripPX(pixlen) {
/*
   takes a css length declaration of the form 100px and removes the px
   then multiplies the result by 1 to ensure it is treated as a numeric 
   value not a text string.
*/
   return (pixlen.substr(0,(pixlen.length-2))*1);
}
function truebody(){
    return (!window.opera 
            && document.compatMode 
            && document.compatMode!="BackCompat")
                ? document.documentElement 
                : document.body;
}

function keepField(element) {
   var field = element.id.substr(element.id.search(/\_/)+1);
   var value = document.getElementById(field).value;
   var keep = element.checked;
   keep &= value != '';
   var tr = element.parentNode.parentNode;
   tr.style.backgroundColor = (keep)?'#999':'#fcf';
   tr.style.color = (keep)?'#ccc':'#000';
   if (keep) {
      createCookie(field,value,60);
   } else eraseCookie(field);
   if (element.checked && value == '') {
      element.checked = false;
      alert('You can only saved completed fields');
   }
}
function loadCookies() {
   var fields = new Array('Species','Locations','Grids','Recorders','Dates','Times');
   for (fieldnum in fields) {
      var field = fields[fieldnum];
      value = readCookie(field);
      if (value) {
         var el = document.getElementById(field);
         el.value = value;
         el = document.getElementById('keep_'+field);
         el.checked = true;
         keepField(el);
      }
   }
}
function addAttribute(element) {
   if (element.value != 'None') {
      var tr = element.parentNode.parentNode;
      var row = parseInt(tr.id);
      var tbody = tr.parentNode;
      if (row == tbody.childNodes.length) {
         var newtr = tr.cloneNode(true);
         newtr.id = row+1;
         for (nodenum in newtr.childNodes) {
            var node = newtr.childNodes[nodenum];
            if (node.childNodes && node.firstChild && node.nodeName == 'TD') {
               var regex = new RegExp(row+'');
               var newid = node.firstChild.id.substring(0,node.firstChild.id.length-1)+newtr.id; 
               node.firstChild.id = newid;
               node.firstChild.name = newid;
               if (node.firstChild.type == 'text') node.firstChild.value = '';
            }
         }
         tbody.appendChild(newtr);
      } 
   }
   return true;
}
function setAttributeTypes(attribute) {
   var atttypeprefix = 'attribute_type_';
   var atttype = atttypeprefix + attribute;
   var sel = document.getElementById(atttype);
   var opts = new Array();
   var opt,optval;
   opt = document.createElement('OPTION');
   opt.value = "";
   opt.appendChild(document.createTextNode("none"));
   sel.appendChild(opt);
   for (row in lookups['AttributeTypes']['Type']) {
      optval = lookups['AttributeTypes']['Type'][row];
      if (!opts[optval]) {
         opts[optval] = 1;
         opt = document.createElement('OPTION');
         opt.value = optval;
         opt.appendChild(document.createTextNode(optval));
         sel.appendChild(opt);
      }
   }
   return true;
}
function setAttributeNames(atttype) {
// get number from element
   var attribute = atttype.id.substr(atttype.id.lastIndexOf('_')+1);
   var atttypeprefix = 'attribute_type_';
   var attnameprefix = 'attribute_name_';
   //var atttype = atttypeprefix + attribute;
   var attname = attnameprefix + Number(attribute).toString();
   var type = atttype.childNodes[atttype.selectedIndex].value;
   
   var nametd = document.getElementById(attname).parentNode;
   var sel = document.createElement('select');
   sel.id = attname;
   if (type) {
      var opts = new Array();
      var opt,optval;
      opt = document.createElement('OPTION');
      opt.value = "";
      opt.appendChild(document.createTextNode("none"));
      sel.appendChild(opt);
      for (row in lookups['AttributeTypes']['Name']) {
         typeval = lookups['AttributeTypes']['Type'][row];
         nameval = lookups['AttributeTypes']['Name'][row];
         if (typeval == type) {
            opt = document.createElement('OPTION');
            opt.value = nameval;
            opt.appendChild(document.createTextNode(nameval));
            sel.appendChild(opt);
         }
      }
      sel.selectedIndex = 0;
   }
   nametd.replaceChild(sel,nametd.firstChild);
   return true;
}
function checkEnter(e){ //e is event object passed from function invocation   
   var characterCode;// literal character code will be stored in this variable
   if(e && e.which){ //if which property of event object is supported (NN4)
      e = e;
      characterCode = e.which; //character code is contained in NN4's which property
   } else {
      e = event;
      characterCode = e.keyCode; //character code is contained in IE's keyCode property
   }
   if(characterCode == 13){ //if generated character code is equal to ascii 13 (if enter key)
      document.forms[0].submit(); //submit the form
      return false;
   } else{
      return true;
   }
}

function getHTTPObject() {
	var xhr = false;
	if (window.XMLHttpRequest) {
		xhr = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		try {
			xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch(e) {
			try {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			} catch(e) {
				xhr = false;
			}
		}
	}
	var isset = false;
	for (var i=0;i<requests.length;i++) {
      if (requests[i] == null) {
         requests[i] = xhr;
         isset = true;
      }
   }
   if (!isset) requests.push(xhr);
	return xhr;
}
var requests = new Array();
function getLookups() {
   var request = getHTTPObject();
   request.onreadystatechange = function() {//Call a function when the state changes.
	  if (request.readyState == 4 && request.status == 200) {
	     setLookups(request.responseText);
	  }
	}
   request.open("GET", "./json_getlookups.php");
   request.send(null);
}
var lookups;
function setLookups(json) {
   lookups = JSON.parse(json);
   setAttributeTypes(1);
}
function getSessionRecords(session) {
   var request = getHTTPObject();
   request.onreadystatechange = function() {//Call a function when the state changes.
	  if (request.readyState == 4 && request.status == 200) {
	     setSessionRecords(request.responseText);
	     request = null;
	  }
	}
   request.open("GET", "./sqlitetasks.php?task=getSessionRecords&session="+session);
   request.send(null);
}
function setSessionRecords(json) {
   var response = JSON.parse(json);
   var div = document.getElementById('recordtable-div'); 
   if (div.childNodes.length > 1) {
      while(div.childNodes.length > 1 && div.lastChild.id != 'recordtable-title') {
         div.removeChild(div.lastChild);
      }
   }
   var tab,thead,tbody,tr,th,td;
   for (record in response[SpeciesRecords]) {
      if (tab == null) {
         tab = document.createElement('TABLE');
         thead = document.createElement('THEAD');
         tbody = document.createElement('TBODY');
         tr = document.createElement('TR');
         for (col in record) {
            th = document.createElement('TH');
            th.appendChild(document.createTextNode(col));
            tr.appendChild(th);
         }
         thead.appen
         
      }   
   }
   if (tab) div.appendChild(tab);
}
var daterange = false;
var datetarget;
function clearDate() {
   var target = document.getElementById(datetarget);
   target.value = '';
   target.blur();
   hidedialog();
   unsetDateComment();
}
function useDate() {
   var dayval = currentday;
   if(dayval.length == 1) dayval = "0" + dayval;
   var monthval = currentmonth + 1;
   if(monthval.length == 1) monthval = "0" + monthval;
   //var date_Text = currentyear + '-' + monthval + '-' + dayval;
   //var date_Text = dayval + '/' + monthval + '/' + currentyear;
   var d = new Date();
   var t = new Date();
   d.setFullYear(currentyear);
   d.setMonth(monthval-1);
   d.setDate(dayval);
   if (t < d) {
      alert('You cannot record a future date.');
      return false;
   }
   var date_Text = d.toLocaleDateString();
   var dates = document.getElementById(datetarget);
   if (daterange) {
      var sdate = dates.value;
      var sd = Date.parse(sdate);
      var d = Date.parse(date_Text);
      if (sd >= d) {
         alert('End date cannot be the same as or before the start date');
         return false;
      } 
      var edate = date_Text;
      dates.value = sdate + ' - ' + edate;
      setDateComment();
   } else {
      dates.value = date_Text;
      unsetDateComment();
   }
   dates.focus();
   dates.blur();
   hidedialog();
}
function getDateOptions() {
   var text = 'Convert to date range?\n' +
         'Hit cancel to change current date.'
   daterange = confirm(text);
   if (daterange) {
      if (!readCookie('throughout')) {
         text = 'Was species present throughout this period?\n'+
               'Cancel means species observed during period'
         var throughout = confirm(text);
         text = (throughout)?'throughout':'during';
         createCookie('throughout',text,60);
      } else text = readCookie('throughout');
   }         
}
function setDateComment() {
   var text = readCookie('throughout');
   var comment = document.getElementById('comment');
   text = 'Species present '+text+' specified date range.\n';
   if (!comment.value.match(/Species present/)) {
      comment.value = text + comment.value;
   }
}
function unsetDateComment() {
   var comment = document.getElementById('comment');
   var matchstring = 'specified date range.\n';
   var text;
   if (comment.value.match(new RegExp(matchstring))) {
      text = comment.value.substr(comment.value.indexOf(matchstring) + matchstring.length);
      text = text.replace(/^\s+/,'');
      comment.value = text;
   }
}
function showLookup(field,target) {
   if (document.getElementById('screen').style.display != 'block') {
      if (!target) target = field;
      var dliner = document.getElementById('dialog-liner');
      var dtitle = document.getElementById('dialog-title');
      // set title
      if (dtitle.childNodes.length > 0) {
         dtitle.replaceChild(document.createTextNode('Lookup ' + field),dtitle.firstChild);
         while(dliner.lastChild.id != 'dialog-title') {
            dliner.removeChild(dliner.lastChild);
         }
      } else {
         dtitle.appendChild(document.createTextNode('Lookup ' + field));
      }
      switch (field) {
      case 'Dates': {
         if (!target) target = 'Dates';
         var dates = document.getElementById(target);
         var text;
         daterange = false;
         if (dates.value && !dates.value.match(/-/)) {getDateOptions();} 
         var cal;
         cal = document.createElement('DIV');
         cal.id = 'calendar';
         dliner.appendChild(cal);
         datetarget = target;
         loadCalendar();
      }break;
      default: {
         //draw lookups
         var use,sel,opt,tab,trh,trb,th,td,thead,tbody,button;
         use = lookups['UseValues'][field][0];
         tab = document.createElement('TABLE');
         thead = document.createElement('THEAD');
         tbody = document.createElement('TBODY');
         trh = document.createElement('TR');
         trb = document.createElement('TR');
         for(col in lookups[field]) {
            th = document.createElement('TH');
            th.appendChild(document.createTextNode(col));
            trh.appendChild(th);
            td = document.createElement('TD');
            sel = document.createElement('SELECT');
            for (row in lookups[field][col]) {
               opt = document.createElement('OPTION');
               opt.value = lookups[field][use][row];
               opt.appendChild(document.createTextNode(lookups[field][col][row]));
               sel.onchange = function () {
                  updateField(target,this.value);
                  this.blur();
               }
               sel.appendChild(opt);
            }
            td.appendChild(sel);
            trb.appendChild(td);
         }  
         thead.appendChild(trh);
         tbody.appendChild(trb);
         tab.appendChild(thead);
         tab.appendChild(tbody);
         dliner.appendChild(tab);
         button = document.createElement('BUTTON');
         button.appendChild(document.createTextNode('Clear'));
         button.onclick = function() {
            updateField(target,null);
         };
         dliner.appendChild(button);
      }break;
      }
      showdialog();
   }
}
function updateField(field,value) {
   //alert(field + ' ' + value);
   var keepprefix = 'keep_';
   var input = document.getElementById(field);
   input.value = value;
   input.focus();
   hidedialog();
   if (input.id.match(/filter/)) getRecords();
   if (document.getElementById(keepprefix+field) && document.getElementById(keepprefix+field).checked) 
      keepField(document.getElementById(keepprefix+field));
   return true;
}
function defaultGrid() {
   var field = 'Locations';
   var target = 'Grids';
   var column = 'GridReference';
   var used = lookups['UseValues'][field][0];
   var location = document.getElementById(field).value;
   for (row in lookups[field][used]) {
      if (lookups[field][used][row] == location) {
         document.getElementById(target).value = lookups[field][column][row];
      }
   }
}
function clearGrid() {
   var target = 'Grids';
   document.getElementById(target).value = '';
}
function setDateToday() {
   var d = new Date();
   document.getElementById('Dates').value = d.toLocaleDateString();
}
function setTimeNow() {
   var d = new Date();
   document.getElementById('Times').value = d.toLocaleTimeString();
}
function checkMandatory() {
   var fields = new Array();
   fields['Species']='Species';
   fields['Locations']='Location';
   fields['Grids']='Grid Reference';
   fields['Recorders']='Recorder';
   fields['Dates']='Dates';
   var element,fieldname;
   var missing = new Array();
   for (element in fields) {
      fieldname = fields[element];
      if (!document.getElementById(element).value) missing.push(fieldname);
   }
   if (missing.length > 0) {
      var text = 'Missing mandatory values in the following fields:\n';
      text += missing.join(',');
      alert(text);
      return false;
   } else return true;
}
function date_to_ymd(date) {
   var day,mon,dstring
   day = (date.getDate() < 10)?"0"+date.getDate():date.getDate();
   mon = ((date.getMonth()+1) < 10)?"0"+(date.getMonth()+1):(date.getMonth()+1);
   dstring = date.getFullYear() + '-' + mon + '-' + day;
   return dstring;
} 
function makeRecordObject() {
   var fields = new Array('Species','Locations','Grids','Recorders','Dates','Times','comment');
   var i,field,value;
   var record = new Object();
   for (i in fields) {
      field = fields[i];
      switch (field) {
      case 'Dates': {
         var dates,sdate,edate,sd,ed;//,sdmon,sdday,edday,edmon;
         value = document.getElementById(field).value;
         if (value.match('-')) {
            dates = value.split(/\s*-\s*/);
            sdate = dates[0];
            edate = dates[1];
            sd = new Date();sd.setTime(Date.parse(sdate));
            ed = new Date();ed.setTime(Date.parse(edate));
            sdate = date_to_ymd(sd);
            edate = date_to_ymd(ed);
            record['StartDate'] = sdate;
            record['EndDate'] = edate;
         } else {
            sd = new Date();sd.setTime(Date.parse(value));
            sdate = date_to_ymd(sd);
            record['StartDate'] = sdate;
         }
      }break;
      default: {
         record[field] = escape(document.getElementById(field).value);
      }break;
      }
   }
   record['Session'] = readCookie('rs');
   record['Attributes'] = makeAttributesArray();
//alert(JSON.stringify(record));
   return record;
}
function makeAttributesArray() {
   var i = 1;
   var atttypeprefix = 'attribute_type_';
   var attnameprefix = 'attribute_name_';
   var attvalueprefix = 'attribute_value_';
   var att,type,name,value;
   var attributes = new Array();
   while (document.getElementById(atttypeprefix+Number(i).toString()).value != '') {
      type = document.getElementById(atttypeprefix+Number(i).toString()).value;
      name = document.getElementById(attnameprefix+Number(i).toString()).value;
      value = document.getElementById(attvalueprefix+Number(i).toString()).value;
      att = new Object();
      att['type'] = type;
      att['name'] = name;
      att['value'] = value;
      attributes.push(att);
      i++;
   }
   return attributes;
}
function setRecordFilter(type,value) {
   var prefix = 'filter_';
   var filter = document.getElementById(prefix+type);
   filter.focus();
   filter.value = value;
   filter.blur();
}
function getRecordFilters() {
   var filters = new Array();
   var prefix = 'filter_';
   var type,filterval;
   var types = new Array('session','species','recorder','location','sdate');
   for (i in types) {
      type = types[i];
      filterval = document.getElementById(prefix+type).value;
      if (filterval) {
         filter = new Object();
         filter['Column'] = type;
         switch (type) {
         case 'sdate': {
            filter['Type'] = '>=';
            var d = new Date();
            d.setTime(Date.parse(filterval));
            filter['Value'] = date_to_ymd(d);
         //alert(filter['Column'] + ' ' + filter['Type'] + ' ' + filter['Value']);
         }break;
         case 'edate': {
            filter['Type'] = '<=';
            var d = new Date();
            d.setTime(Date.parse(filterval));
            filter['Value'] = date_to_ymd(d);
         //alert(filter['Column'] + ' ' + filter['Type'] + ' ' + filter['Value']);
         }break;
         default: {
            filter['Type'] = '=';
            filter['Value'] = filterval;
         }break;
         }
         filters.push(filter);
      }
   }
   return filters;
}
function getRecordOrders() {
   var orders = new Array();
   var prefix = 'order_';
   var type,filterval;
   var types = new Array('session','record','species','recorder','location','gridref','sdate');
   for (i in types) {
      type = types[i];
      orderval = document.getElementById(prefix+type).value;
      if (orderval) {
         order = new Object();
         order['Column'] = type;
         order['Direction'] = (type=='sdate')?'DESC':'ASC';
         orders[orderval-1] = order;
      }
   }
//alert(JSON.stringify(orders));
   return orders;
}
function getRecords() {
//alert('Updating record table');
   var jobj = new Object();
   jobj['filters'] = getRecordFilters();
   jobj['orders'] = getRecordOrders();
   var json = JSON.stringify(jobj);
   executeTask('getRecords',json);
}
function updateRecordList(records) {
   var i,record,key,tr,td,input,add,button;
   var tbody = document.getElementById('recordtable-tbody');
   while (tbody.childNodes.length > 0) {
      tbody.removeChild(tbody.lastChild);
   }
   for (i=0;i<records.length;i++) {
      tr = document.createElement('TR');
      record = records[i];
      td = document.createElement('TD');
      button = document.createElement('BUTTON');
      button.id = i;
      button.onclick= function () {
         deleteRecord(this.id);
      };
      button.appendChild(document.createTextNode('Del'));
      td.appendChild(button);
      tr.appendChild(td);
      for (key in record) {
         td = document.createElement('TD');
         suppress = false;
         switch (key) {
         case 'attid': {suppress=true;}break;
         case 'created': {suppress=true;}break;
         case 'comment': {
            input = document.createElement('TEXTAREA');
            input.className = 'rtable';
            input.id = 'edit_'+key+'_'+i;
            input.appendChild(document.createTextNode(record[key]));
         }break; 
         case 'sdate': {
            input = document.createElement('INPUT');
            input.type = 'text';
            input.className = 'rtable';
            input.id = 'edit_'+key+'_'+i;
            if (record[key]!= '') {
               var d,bits;
               d = new Date()
               bits = record[key].split('-'); 
               d.setFullYear(bits[0]);
               d.setMonth(bits[1]-1);
               d.setDate(bits[2]);
               input.value = d.toLocaleDateString();
            } else input.value = '';
         }break;
         case 'edate': {
            input = document.createElement('INPUT');
            input.type = 'text';
            input.className = 'rtable';
            input.id = 'edit_'+key+'_'+i;
            if (record[key]!= '') {
               var d,bits;
               d = new Date()
               bits = record[key].split('-'); 
               d.setFullYear(bits[0]);
               d.setMonth(bits[1]-1);
               d.setDate(bits[2]);
               input.value = d.toLocaleDateString();
            } else input.value = '';
         }break;
         default: {
            input = document.createElement('INPUT');
            input.type = 'text';
            input.className = 'rtable';
            input.id = 'edit_'+key+'_'+i;
            input.value = (record[key] == null)?'':record[key];
         }break;
         }
         if (!suppress) {
            td.appendChild(input);
            tr.appendChild(td);
         }
      }
      tbody.appendChild(tr);
   }
}
function saveRecord() {
   if (checkMandatory()) {
      var json = JSON.stringify(makeRecordObject());
      //alert(json);
      executeTask('insertRecord',json);
   } else return false;
}
function deleteRecord(recnumber) {
   var record_id;
   var prefix = 'edit_record_';
   var goahead = confirm('You are about to delete a record.\nThis cannot be undone.');
   if (goahead) {
      record_id = document.getElementById(prefix+Number(recnumber).toString()).value;
      var jobj = new Object();
      jobj['Record'] = record_id;
      json = JSON.stringify(jobj);
      executeTask('deleteRecord',json);
   }
}
function executeTask(task,json) {
   var request = getHTTPObject();
   request.onreadystatechange = function() {//Call a function when the state changes.
      if (request.readyState == 4 && request.status == 200) {
         actionStatus(request.responseText);
      }
	}
   request.open("GET", './sqlitetasks.php?task='+task+'&json='+json);
   request.send(null);
}
function actionStatus(json) {
   var statusmessage = JSON.parse(json);
   switch (statusmessage['task']) {
   case 'insertRecord': {
      //alert(json);
      alert(statusmessage['message']);
      getRecords();
      clearForm();
   }break;
   case 'deleteRecord': {
      //alert(json);
      alert(statusmessage['message']);
      getRecords();
   }break;
   case 'getRecords': {
      //alert(json);
      updateRecordList(statusmessage['records']);
   }break;
   }
}
function clearForm() {
   var fields = new Array('Species','Locations','Grids','Recorders','Dates','Times');
   document.getElementById('comment').value = '';
   var keepprefix = 'keep_';
   for (fieldnum in fields) {
      var field = fields[fieldnum];
      if (document.getElementById(keepprefix+field)&&!document.getElementById(keepprefix+field).checked) {
         document.getElementById(field).value = '';
      } 
      if (field == 'Dates' && document.getElementById(field).value.match(/-/)) setDateComment();
   }
   clearAttributes();
}
function clearAttributes() {
   var atttypeprefix = 'attribute_type_';
   var attnameprefix = 'attribute_name_';
   var attnameprefix = 'attribute_value_';
   var tbody = document.getElementById('attributes-tbody');
   while (tbody.firstChild.nodeName != 'TR') {
      tbody.removeChild(tbody.firstChild);
   }
   while (tbody.childNodes.length > 1) {
      tbody.removeChild(tbody.lastChild);
   }
   document.getElementById(atttypeprefix+'1').selectedIndex=0;
}
function showhidesection(button) {
   var div = button.parentNode;
   if (!div.style.height || div.style.height == 'auto') {
      div.style.height = '40px';
      div.style.overflow = 'none';
      button.replaceChild(document.createTextNode('+'),button.firstChild);
   } else {
      div.style.height = 'auto';
      div.style.overflow = 'auto';
      button.replaceChild(document.createTextNode('-'),button.firstChild);
   }
}
function stop() {
   //if (window.event.clientX < 0 && window.event.clientY < 0) {
      if (confirm('Do you want to stop recording?')) {
         executeTask('stop');
      }
   //}
}
