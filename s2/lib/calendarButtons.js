var currentday;
var currentmonth;
var currentyear;
var xmlhttp;
var events = new Array();
function loadCalendar(smonth,syear,type) {
   var d = new Date();
   if (smonth!=null || syear!=null) {
      if (smonth!=null) {
         if (syear) syear = (smonth<0)?syear-1:(smonth>11)?syear+1:syear;
         smonth = smonth % 12;
         if (smonth<0) smonth = 12 + smonth;
         d.setUTCMonth(smonth);   
         d.setUTCFullYear(syear);
      }
   }
   if(smonth==null) smonth = d.getUTCMonth();
   syear = d.getUTCFullYear();
   var table,node,tn;
   var cal = document.getElementById('calendar');
   while(cal.childNodes.length>0) cal.removeChild(cal.childNodes[0]);
   type = (type==null)?"":type;
   switch (type.toLowerCase()) {
   default: {
      table = getCalendarTable(smonth,syear,type);
      cal.appendChild(table);
      node = document.createElement('BUTTON');
      node.id = 'cancel';
      node.className = 'dbutton';
      node.onclick = function() {hidedialog();};
      tn = document.createTextNode('Cancel');
      node.appendChild(tn);
      cal.appendChild(node);
      node = document.createElement('BUTTON');
      node.id = 'usedate';
      node.className = 'dbutton';
      node.onclick = function() {useDate();};
      tn = document.createTextNode('OK');
      node.appendChild(tn);
      cal.appendChild(node);
   }break;
   }
   return true;
}
function getCalendarTable(smonth,syear,type) {
   var d = new Date();
   var today = d.getUTCDate();
   d.setUTCDate(1);
   var thismonth = d.getUTCMonth();
   var thisyear = d.getUTCFullYear();
   if (smonth!=null || syear!=null) {
      if (smonth!=null) {
         d.setUTCMonth(smonth);
         if (syear) syear = (smonth<0)?syear-1:(smonth>11)?syear+1:syear;
      }
      if (syear!=null) d.setUTCFullYear(syear);
   }
   currentday = d.getUTCDate();
   currentmonth = d.getUTCMonth();
   currentyear = d.getUTCFullYear();
   var monnum = currentmonth;
   var mfirst = new Date();
   mfirst.setTime(d.getTime());
   d.setUTCMonth(currentmonth+1);
   var time = d.getTime();
   var time = time - timeMillis(1,'day');
   d.setTime(time);
   var mlast = new Date();
   mlast.setTime(d.getTime());
   var days = new Array('M','T','W','T','F','S','S');
   var months = new Array( 'Jan','Feb','Mar',
                           'Apr','May','Jun',
                           'Jul','Aug','Sep',
                           'Oct','Nov','Dec');
   var node,table,thead,tr,th,tbody,td,tn,button;
   //var monnum = d.getUTCMonth();
   var month = months[monnum];
   var year = d.getUTCFullYear();
   var firstday = mfirst.getUTCDay();
   firstday = (firstday==0)?6:firstday-1;
   var daysinmonth = mlast.getUTCDate();
   table = document.createElement('TABLE');
   table.id = 'calendartab';
   table.className = 'options';
   thead = document.createElement('THEAD');
   tr = document.createElement('TR');
   // back a year
   th = document.createElement('TH');
   th.className = 'day';
   node = document.createElement('BUTTON');
   node.onclick = function () {
      //loadCalendar(monnum,year-1,type);
      var cal = document.getElementById('calendartab');
      var par = cal.parentNode;
      par.replaceChild(getCalendarTable(monnum,year-1,type),cal);
      return true;
   };
   tn = document.createTextNode("<Y");
   node.appendChild(tn);
   th.appendChild(node);
   tr.appendChild(th);
   // back a month
   th = document.createElement('TH');
   th.className = 'day';
   node = document.createElement('BUTTON');
   node.onclick = function () {
      //loadCalendar(monnum-1,year,type);
      var cal = document.getElementById('calendartab');
      var par = cal.parentNode;
      par.replaceChild(getCalendarTable(monnum-1,year,type),cal);
      return true;
   };
   tn = document.createTextNode("<M");
   node.appendChild(tn);
   th.appendChild(node);
   tr.appendChild(th);
   th = document.createElement('TH');
   th.className = 'day';
   node = document.createElement('H3');
   tn = document.createTextNode(month + ' ' + year);
   node.appendChild(tn);
   th.appendChild(node);
   th.colSpan = 3;
   tr.appendChild(th);
   // forward a month
   th = document.createElement('TH');
   th.className = 'day';
   node = document.createElement('BUTTON');
   node.onclick = function () {
      //loadCalendar(monnum+1,year,type);
      var cal = document.getElementById('calendartab');
      var par = cal.parentNode;
      par.replaceChild(getCalendarTable(monnum+1,year,type),cal);
      return true;
   };
   tn = document.createTextNode("M>");
   node.appendChild(tn);
   th.appendChild(node);
   tr.appendChild(th);
   // forward a year
   th = document.createElement('TH');
   th.className = 'day';
   node = document.createElement('BUTTON');
   node.onclick = function () {
      //loadCalendar(monnum,year+1,type);
      var cal = document.getElementById('calendartab');
      var par = cal.parentNode;
      par.replaceChild(getCalendarTable(monnum,year+1,type),cal);
      return true;
   };
   tn = document.createTextNode("Y>");
   node.appendChild(tn);
   th.appendChild(node);
   tr.appendChild(th);
   thead.appendChild(tr);
   tr = document.createElement('TR');
   var x,y;
   for(x=0;x<=6;x++) {
      th = document.createElement('TH');
      th.className = 'day';
      tn = document.createTextNode(days[x]);
      th.appendChild(tn);
      tr.appendChild(th);
   }
   thead.appendChild(tr);
   table.appendChild(thead);
   tbody = document.createElement('TBODY');
   var daynum = 1;
   for (y=0;y<=5;y++) {
      if (daynum<=daysinmonth) {
         tr = document.createElement('TR');
         for (x=0;x<=6;x++) {
            td = document.createElement('TD');
            if (monnum == thismonth && year == thisyear && daynum == today) td.className = 'today';
            else td.className = 'day';
            if (!((y==0 && x<firstday)||(daynum>daysinmonth))) {
               tn = document.createTextNode(daynum);
               td.appendChild(tn);
               td.id = 'day_'+daynum;
               td.onclick = function() {changeDay(this.id);};
               daynum++;
            } else {
               td.className = 'autodata';
            }
            tr.appendChild(td);
         }
         tbody.appendChild(tr);
      }
   }
   table.appendChild(tbody);
   return table;
}
function timeMillis(x,t) {
   var millis;
   t = t.replace(/s$/,"");
   switch(t.toLowerCase()) {
   case 'second': millis = x*1000;break;
   case 'minute': millis = x*60*1000;break;
   case 'hour':   millis = x*60*60*1000;break;
   case 'day':    millis = x*24*60*60*1000;break;
   case 'week':   millis = x*7*24*60*60*1000;break;
   case 'year':   millis = x*365*24*60*60*1000;break;
   }
   return millis;
}
function changeDay(id) {
    var oldDayNode = document.getElementById('day_'+currentday);
    if (oldDayNode.className != 'today') {
        oldDayNode.className = 'day';
    }
    var newDayNode = document.getElementById(id);
    if (newDayNode.className != 'today') {
        newDayNode.className = 'select';
    }
    id = id.substring(4);
    currentday = parseInt(id);
    return false;
}
function loadXMLEvents(url) {
   xmlhttp=null;
   //url.replace("/csv/","xml");
   if (window.XMLHttpRequest) {// code for all new browsers
      xmlhttp=new XMLHttpRequest();
   } else if (window.ActiveXObject) {// code for IE5 and IE6
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
   }
   if (xmlhttp!=null) {
      xmlhttp.onreadystatechange=readEventsFile;
      xmlhttp.open("GET",url,true);
      xmlhttp.send(null);
   } 
}
function readEventsFile()  {
   var eventdate,y,m,d,v;
   var xml,i,j,parent,element,credit,lines,line,vals,filename,headings,heading;
   if (xmlhttp.readyState == 4) {
      var response = xmlhttp.responseText;
      lines = response.split(/\n/);
      headings = getCSVValues(lines[0]);
      for (i=1;i<lines.length;i++) {
         vals = getCSVValues(lines[i]);
         d = vals[0];
         eventdate = d.split(/\//);
         y = eventdate[2];
         m = eventdate[1];
         d = eventdate[0];
         eventname = vals[1];
         events[filename] = new Array();
         for(j=2;j<vals.length;j++) {
            v = vals[j];
            v = v.toString();
            heading = headings[j];
            if (v.length>0) v.replace(/^\s+$/,'');
            if (!events[y]) events[y] = new Array();
            if (!events[y][m]) events[y][m] = new Array();
            if (!events[y][m][d]) events[y][m][d] = new Array();
            if (!events[y][m][d][eventname]) events[y][m][d][eventname] = new Array();
            if (!events[y][m][d][eventname][heading]) events[y][m][d][eventname][heading] = new Array();
            if (vals[j].length > 0) events[y][m][d][eventname][heading] = v;
         }
      }
      loadCalendar(null,null,'events');
   }
   return true;
}
function eventList(month,year) {
   var table,tbody,thead,tr,th,td,node,tn,a,d,m,y,e,h,v,dayevents,monthevents,thisevent;
   d = new Date();
   if (month==null) month = d.getUTCMonth();
   if (year==null) year = d.getUTCFullYear();
   month = (month*1) + 1;
   if (month < 10) month = "0"+month;
   if (events[year] && events[year][month]) {
      monthevents = events[year][month];
      table = document.createElement('TABLE');
      tbody = document.createElement('TBODY');
      for (d in monthevents) {
         dayevents = monthevents[d];
         document.getElementById(d*1).style.borderColor = '#f00';
         for (e in dayevents) {
            thisevent = dayevents[e];
            if (!thead) {
               thead = document.createElement('THEAD');
               tr = document.createElement('TR');
               th = document.createElement('TH');
               th.className = 'top';
               tn = document.createTextNode('Date');
               th.appendChild(tn);
               tr.appendChild(th);
               th = document.createElement('TH');
               th.className = 'top';
               tn = document.createTextNode('Event');
               th.appendChild(tn);
               tr.appendChild(th);
               for(h in thisevent) {
                  th = document.createElement('TH');
                  th.className = 'top';
                  tn = document.createTextNode(h);
                  th.appendChild(tn);
                  tr.appendChild(th);
               }
               thead.appendChild(tr);
            }
            tr = document.createElement('TR');
            td = document.createElement('TD');
            td.className = 'lined';
            tn = document.createTextNode(d+'/'+month+'/'+year);
            td.appendChild(tn);
            tr.appendChild(td);
            td = document.createElement('TD');
            td.className = 'lined';
            tn = document.createTextNode(e);
            td.appendChild(tn);
            tr.appendChild(td);
            for(h in thisevent) {
               v = thisevent[h];
               td = document.createElement('TD');
               td.className = 'lined';
               if (v.match('http')||v.match('mailto')) {
                  a = document.createElement('A');
                  a.href = v;
                  a.target = 'new';
                  tn = document.createTextNode(v);
                  a.appendChild(tn);
                  td.appendChild(a);
               } else {
                  v = replaceHTMLEntities(v);
                  v = v.replace(/EMPTY/,"");
                  tn = document.createTextNode(v);
                  td.appendChild(tn);
               }
               tr.appendChild(td);
            }
            tbody.appendChild(tr);
         }  
      }
      if (thead) table.appendChild(thead);
      if (tbody) table.appendChild(tbody);
      return table;
   } else {
      return null;
   }
}
function getCSVValues(string) {
   /*
      JavaScript push will overwrite elements assigned as null. 
      In order to deal with empty ( ..",,".. ) values in the CSV file you need 
      to assign them with a specific value and replace it later hence EMPTY
   */
   var elements = new Array();
   var bits,s;
   if (string.match(/\"/)) {
      bits = string.split('"');
      for (var i=0;i<bits.length;i++) {
         bit = bits[i];
         bit = bit.replace(/^[\s+]$/,"");
         if (i%2==1) {
            elements.push(bit);
         } else if (bit != ',' && bit.length>0) {
            s = bit;
            s = s.replace(/^\,/,"");
            s = s.replace(/\,$/,"");
            if (s.match(/\,/)) elements.concat(s.split(/\,/));
            else if (!s.length) elements.push("EMPTY");
            else elements.push(s);
         }
      }
   } else {
      elements = string.split(/\,/);
   }
   return elements;
}
function replaceHTMLEntities(val) {
   val=val.replace(/&/g,'\&');
   val=val.replace(/"/g,'\"');
   val=val.replace(/'/g,'\'');
   val=val.replace(/£/g,'\£');
   return val;
}
function addslashes(str) {
   str=str.replace(/\'/g,'\\\'');
   str=str.replace(/\"/g,'\\"');
   str=str.replace(/\\/g,'\\\\');
   str=str.replace(/\0/g,'\\0');
   return str;
}
function pausecomp(millis) {
   var date = new Date();
   var curDate = null;
   do { curDate = new Date(); }
   while(curDate-date < millis);
} 
function getObjectClass(obj) {  
   if (obj && obj.constructor && obj.constructor.toString) {  
      var arr = obj.constructor.toString().match(  
         /function\s*(\w+)/);  
      if (arr && arr.length == 2) {  
         return arr[1];  
      }  
   }  
   return undefined;  
}
  
function s2_padnum(number, length) {   
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }   
    return str;
}

function s2_readdate(dstring) {
   var mats,dobj,d,m,y;
   mats = dstring.match(/(\d{2})\/(\d{2})\/(\d{4})/);
   if (mats && mats.length >= 4) {
      d = mats[1]*1;
      m = mats[2]*1;
      m -= 1;
      y = mats[3]*1;
      dobj = new Date(y,m,d);
   }
   return dobj;
}

Date.prototype.addBusDays = function(dd) {
   var wks = Math.floor(dd/5);
   //var dys = dd.mod(5);
   var dys = dd % 5;
   var dy = this.getDay();
   if (dy === 6 && dys > -1) {
      if (dys === 0) {dys-=2; dy+=2;}
      dys++; dy -= 6;
   }
   if (dy === 0 && dys < 1) {
      if (dys === 0) {dys+=2; dy-=2;}
      dys--; dy += 6;
   }
   if (dy + dys > 5) dys += 2;
   if (dy + dys < 1) dys -= 2;
   this.setDate(this.getDate()+wks*7+dys);
}