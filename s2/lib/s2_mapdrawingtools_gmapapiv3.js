var columns = null;
var map,wkt,x,y;
var points = new Array();
var polygon;
var polypoints;
var marker,easting,northing,latitude,longitude;
var precision = 10000;
var ddc,gr,ngr,point,action,center;
var vertex,ctr,deg;
var tileCountry;
function s2_eldims(el,dim) {
   var r,x,y;
   x = (parseInt(el.style.width)>0)?parseInt(el.style.width):(el.offsetWidth>0)?el.offsetWidth:el.clientWidth;   
   y = (parseInt(el.style.height)>0)?parseInt(el.style.height):(el.offsetHeight>0)?el.offsetHeight:el.clientHeight;
   switch(dim) {
   case 'width':  r = x; break;
   case 'height': r = y; break;
   default: r = new Array(x,y); break; 
   }
   return r;   
}
function maplayout() {
   var maptools,mapcontainer,mapoutline,mapsave;
   mapoutline = document.getElementById('mapoutline');
   mapcontainer = document.getElementById('mapcontainer'); 
   maptools = document.getElementById('maptools');
   var x = s2_eldims(mapoutline,'width');
   var t = 150;
   mapcontainer.style.width = Math.floor(x - t - 50) + 'px';
   maptools.style.width = Math.floor(t) + 'px';
   map.checkResize();
   fitToMap();
   return true;    
}
function mapShrinkOrExpand(button) {
   var name = button.firstChild.alt;
   var maptools,mapcontainer,mapoutline,mapdiv,maplabels;
   mapoutline = document.getElementById('mapoutline');
   mapcontainer = document.getElementById('mapcontainer');
   maptools = document.getElementById('maptools'); 
   maplabels = document.getElementById('maplabels');
   mapdiv = document.getElementById('map');
   var x = (mapoutline.style.width)?parseInt(mapoutline.style.width):mapoutline.offsetWidth;
   var t = 250;
   var img;
   switch (name) {
   case 'shrink': {
      img = new Image();
      img.src = '../images/expand2.gif';
      img.alt = 'expand';
      button.replaceChild(img,button.firstChild);
      mapcontainer.style.height = '150px';
      mapcontainer.style.width = t+'px';       
      maptools.style.display = 'none';
      maptools.style.visibility = 'hidden';
      maplabels.style.display = 'none';
      maplabels.style.visibility = 'hidden';
      mapdiv.style.height = '150px';
      map.disableGoogleBar();
   }break;
   case 'expand': {
      img = new Image();
      img.src = '../images/shrink2.gif';
      img.alt = 'shrink';                          
      button.replaceChild(img,button.firstChild);
      maptools.style.display = 'block';
      maptools.style.visibility = 'visible';
      maplabels.style.display = 'block';
      maplabels.style.visibility = 'visible';
      mapdiv.style.height = '350px';
      mapcontainer.style.height = 'auto';
      map.enableGoogleBar();
      maplayout();         
   }break
   }
   fitToMap();      
   
   return true;   
}
function fitToMap() {
   if (map && polygon) {
      map.checkResize();
      var bnd = polygon.getBounds();
      var ctr = bnd.getCenter();
      map.setCenter(ctr);
      map.setZoom(map.getBoundsZoomLevel(bnd));  
   }
}
function drawWKT (wkt) {
   if (polygon) map.removeOverlay(polygon);
   polygon = s2_wkt_to_gmappoly(wkt);
   map.addOverlay(polygon);
   fitToMap();
   return true;
}
function drawPolygon (points,closed) {
   if (polygon) map.removeOverlay(polygon);
   closed = (closed == null)?1:closed;
   if (closed) {
      var polypoints = points.concat(new Array(points[0]));
      polygon = new google.maps.Polygon(polypoints,'#330000',1,1,'#ff0000',0.5);
   } else {
      polygon = new google.maps.Polyline(points,'#330000',1,1);
   }
   map.addOverlay(polygon);
   return true;
}
function appendPolygon (point,closed) {
   if (polygon) map.removeOverlay(polygon);
   if (point) points = points.concat(new Array(point));
   closed = (closed == null)?1:closed;
   if (closed) {
      polypoints = points.concat(new Array(points[0]));
      polygon = new google.maps.Polygon(polypoints,'#330000',1,1,'#ff0000',0.5);
      GEvent.addListener(polygon, "click", function(point) {clickAction(point);});
   } else {
      polygon = new google.maps.Polyline(points,'#330000',1,1);
   }
   map.addOverlay(polygon);
   //fitToMap();
   return true;
}
function getSquare(gr,acc) {
   var x,y,xi,yi;
   x = gr.easting - (gr.easting % acc);
   y = gr.northing - (gr.northing % acc);
   var points = new Array();   
   for (xi=0;xi<=1;xi++) {
      for (yi=0;yi<=1;yi++) {
         gr = new GridReference((x + (xi*acc)),y + (((xi+yi)%2)*acc));
         gr.accuracy = acc;
         ddc = conv_uk_ings_to_ll(gr);                  
         points.push(new google.maps.Point(ddc.longitude,ddc.latitude));                  
      }
   }
//alert(JSON.stringify(points));
   return points;            
}
function getSquareCenter(gr,acc) {
   var x,y,xi,yi;
   x = gr.easting - (gr.easting % acc);
   y = gr.northing - (gr.northing % acc);
   gr = new GridReference((x + (0.5*acc)),(y + (0.5*acc)));
   gr.accuracy = acc;
   ddc = conv_uk_ings_to_ll(gr);                  
   return new google.maps.Point(ddc.longitude,ddc.latitude);            
}
function drawPoint(point) {
   point = (point == null)?points[0]:point;
   if (marker) map.removeOverlay(marker);
   var pll = precision * 10;
   latitude = Math.floor(point.y * pll)/pll;
   longitude = Math.floor(point.x * pll)/pll;
   ddc = new DegreesDecimalCoordinate(point.y,point.x);
   gr = wgs84_to_osgb36(ddc);
   ngr = find_gridsquare(gr);
   var acc = parseInt(document.getElementById('acc').value); 
   ngr.easting = ngr.easting - (ngr.easting % acc) + (0.5 * acc);
   ngr.northing = ngr.northing - (ngr.northing % acc) + (0.5 * acc);
   ngr.accuracy = acc;
   easting = formatToXPlaces(ngr.easting,5,0); 
   northing = formatToXPlaces(ngr.northing,5,0);
   points = getSquare(gr,acc);
   var center = getSquareCenter(gr,acc);
   marker = new google.maps.Marker(center);
   updatePoint(ngr);
   google.maps.Event.addListener(marker, "click", function() {
      this.openInfoWindowHtml("<h4>Location</h4>" +
                  "<table style='width:250px;'><tbody>" + 
                  "<tr><th class='lined' style='padding: 5px;'>Lat Lon</th><td class='lined'>" + ddc.asString() + "</td></tr>" +
                  "<tr><th class='lined' style='padding: 5px;'>Grid Ref</th><td class='lined' style='text-transform:uppercase;'>" + ngr.asString() + "</td></td>" + 
                  "</tbody></table>");
   });
   drawPolygon(points,true);
   map.addOverlay(marker);
   fitToMap();
   return true;
}
function drawCircle(point,radius,vertices) {
   if (polygon) map.removeOverlay(polygon);
   ddc = new DegreesDecimalCoordinate(point.y,point.x);
   ctr = wgs84_to_osgb36(ddc);
   seg = Math.floor(360/vertices) * (Math.PI/180);
   points = new Array();
   gr = new GridReference(0,0);
   ngr = find_gridsquare(ctr);
   updatePoint(ngr);
   for (vertex=0;vertex<=vertices;vertex++) {
      gr.easting = ctr.easting + (radius * Math.sin(seg*vertex));
      gr.northing = ctr.northing + (radius * Math.cos(seg*vertex));
      ddc = conv_uk_ings_to_ll(gr);
      points[vertex] = new google.maps.Point(ddc.longitude,ddc.latitude);
   }
   polygon = new google.maps.Polygon(points,'#330000',1,1,'#ff0000',0.5);
   map.addOverlay(polygon);
   fitToMap();
}
function deletePolygon() {
   points = new Array();
   map.removeOverlay(polygon);
}
function s2_mapload() {
   // Create tile layers
   //maplayout();
   x = getQueryVariable('x');
   y = getQueryVariable('y');
   if (x && y) {
      gr = new GridReference(x,y);
      ddc = conv_uk_ings_to_ll(gr);
      x = ddc.longitude;
      y = ddc.latitude;
      ctr = new GPoint(x,y);
      points[0] = ctr;
   } else {
      x = -0.44;
      y = 53.69;
      ctr = new GPoint(x,y);
   }
   wkt = getQueryVariable('wkt');
   if (wkt) {
      wkt = unescape(wkt);
      ctr = getWKTPoints(wkt);
   }
   var z1;
   if (points.length > 1) {
      var b1 = new google.maps.Bounds(points);
      var b2 = new google.maps.LatLngBounds(new google.maps.LatLng(b1.minY,b1.minX),new google.maps.LatLng(b1.maxY,b1.maxX));
      z1 = map.getBoundsZoomLevel(b2);
   } else z1 = 10;
      
   var myLatlng = new google.maps.LatLng(y,x);
   var myOptions = {
      zoom: z1,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.TERRAIN
   };
   map = new google.maps.Map(document.getElementById("map_canvas"),myOptions); 
}
function clickAction(point) {
   action = document.getElementById('action').value;
   switch (action) {
      case 'recentre': {
         map.setCenter(point);
      } break;
      case 'polygon': {
         showMarkers(false);
         appendPolygon(point,1);
      }break;
      case 'polyline': {
         showMarkers(false);
         appendPolygon(point,0);
      }break;
      case 'circle': {
         showMarkers(false);
         drawCircle(point,document.getElementById('acc').value,36);
      }break;
      case 'point': {
         showMarkers(false);
         drawPoint(point);
      }break;
   }
}
var p,pcount,wkt;
function returnVal() {
   if (points.length > 1) {
      returnPolygon();
   } else if (points.length == 1) {
      returnPoint();            
   } else {
      alert("A usable area has not been defined.");
   }
   window.close();
}
function returnPolygon() {
   wkt = 'POLYGON((';
   for (pcount=0;pcount<points.length;pcount++) {
      p = points[pcount];
      ddc = new DegreesDecimalCoordinate(p.y,p.x);
      gr = wgs84_to_osgb36(ddc);
      wkt += gr.easting + ' ' + gr.northing + ',';
   }
   p = points[0];
   ddc = new DegreesDecimalCoordinate(p.y,p.x);
   gr = wgs84_to_osgb36(ddc);
   wkt += gr.easting + ' ' + gr.northing + '))';
   window.opener.collectWKT(wkt);
   return true;
}
function returnPoint() {
   p = points[0];
   var zoom = map.getZoom();
   
   ddc = new DegreesDecimalCoordinate(p.y,p.x);
   gr = wgs84_to_osgb36(ddc);
   gr.accuracy = (zoom >= 12)?100:(zoom>=8)?1000:(zoom>=5)?10000:0;
   gr.easting = Math.floor(gr.easting/gr.accuracy)*gr.accuracy;
   gr.northing = Math.floor(gr.northing/gr.accuracy)*gr.accuracy;
   if (gr.accuracy == 0) alert('please zoom in to identify your location');
   else {
      window.opener.receiveGridReference(gr);
      window.close();
   }
   return true;
}         
function getWKTPoints(wkt) {
   var srs = getQueryVariable('srs');
   wkt = wkt.replace('POLYGON((','');
   var chopped = 0;
   if (!wkt.indexOf("))") < 0) chopped = 1; 
   else wkt = wkt.replace('))','');
   var coords = wkt.split(/,\s*/);
   coords.shift();
   var vertices = 0;
   x = 0;
   y = 0;
   var xy;
   for (pcount = 0; pcount < (coords.length - chopped); pcount++) {
      coord = coords[pcount];
      xy = coord.split(' ');
      if (xy.length == 2) {
         vertices++;
         if (srs && srs == 4326) {
            ddc = new DegreesDecimalCoordinate(xy[1],xy[0]);
         } else {
            gr = new GridReference(xy[0], xy[1]);
            ddc = conv_uk_ings_to_ll(gr);
         }
         x += ddc.longitude;
         y += ddc.latitude;
         points[pcount] = new GPoint(ddc.longitude,ddc.latitude);
      }
   }
   x /= vertices;
   y /= vertices;
   return new GPoint(x,y);
}
function gridPrecision(num) {
   var len = num.length
   var precision = 5;  
   if (len < precision) {
      num*=10;
      num+=5;
      len++;
      while (len < precision) {
         num*=10;
         len++;
      }
   }
   return num;
}
function updatePoint(ngr) {
   if (ngr) {
      ngr.accuracy = document.getElementById('acc').value;  
      document.getElementById('ngr').value = ngr.asString();
      gr = conv_ngr_to_ings(ngr);
      ddc = conv_uk_ings_to_ll(gr);
      ddc.accuracy = ngr.accuracy;
   } else {
      var ngr = document.getElementById('ngr').value;
      var sq = ngr.substr(0,2).toUpperCase();
      var len = parseInt((ngr.length - 2) / 2);
      var e = gridPrecision(ngr.substr(2,len));
      var n = gridPrecision(ngr.substr(2+len,len));
      var precision = Math.pow(10,5-len);
      if (ngr.length % 2 == 1) {
         precision = 2 * (precision/10);         
         var tetrads = tetradArray();                                       
         var t = ngr.substr(ngr.length-1).toUpperCase();
         for (x in tetrads) {
            var tx = tetrads[x];
            for (y in tx) {
               if (t == tetrads[x][y]) {
                  e += 2 * precision * x;
                  n += 2 * precision * y;
               }
            } 
         }
         document.getElementById('acc').value = precision;
      }                              
      ngr = new NationalGridReference(sq,e,n);
      ngr.accuracy = precision;
      gr = conv_ngr_to_ings(ngr);
      ddc = conv_uk_ings_to_ll(gr);
      point = new GLatLng(ddc.latitude,ddc.longitude);
      map.setCenter(point);
      clickAction(point);
   }
   document.getElementById('ll').value = ddc.asString();
   return true;
}
function updateLLPoint() {
   var ll = document.getElementById('ll').value;
   list(lat,lon) = ll.split('/\s*,*\s*/');            
   ddc = new DegreesDecimalCoordinate(lat,lon);
   gr = wgs84_to_osgb36(ddc);
   ngr = find_gridsquare (gr_obj);
   point = new GLatLng(ddc.latitude,ddc.longitude);
   document.getElementById('ngr').value = ngr.asString();
   map.setCenter(point);
   clickAction(point);
   return true;
}