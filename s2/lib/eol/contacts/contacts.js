function s2_contacts_pageload() {
   placescreen();
   var encrypt = readCookie('encrypt');
   if (encrypt) auth();
   else enableinterface();
}
function s2_contacts_setview(viewname) {
   var req = new Object();
   req.target = './profilereader.php';
   var json = new Object();
   var action = new Object();
   action.Task = 'setview';
   json.Action = action;
   json.Parameters = new Object();
   json.Parameters.ViewName = viewname;  
   var parameters = new Object();
   parameters['json'] = json;
   req.request = parameters; 
   req.responder = s2_contacts_drawview;
   //req.method = 'POST';
   //req.sync = false;   
   snc_send(req);   
}
function s2_contacts_getdata() {
   var req = new Object();
   req.target = './s2_contacts_getdata.php';
   var json = new Object();
   var action = new Object();
   action.Task = 'getdata';
   json.Action = action;
   var parameters = new Object();
   parameters['json'] = json;
   req.request = parameters; 
   req.responder = s2_contacts_drawdata;
   //req.method = 'POST';
   //req.sync = false;   
   snc_send(req);         
}
function s2_contacts_drawdata(json) {
   alert(json);
}
function cleardialog(dlinerid) {
   var dliner = document.getElementById(dlinerid);
   while(dliner.lastChild.id != 'dialog-title') {
      dliner.removeChild(dliner.lastChild);
   }
   return true;
}
function enableinterface() {
   var settings = unescape(readCookie('Settings'));
   //if (!settings) {
      var json = new Object();
      if (settings) json.Settings = JSON.parse(settings);
      var action = new Object();
      action.Task = 'startsession';
      json.Action = action;
      snc_executeTask(json,true);
   //}             
}

function snc_executeTask(json) {
   var server = unescape(readCookie('cdm_server'));
   var r = new Object();
   r.target = 'http://'+server+'/snc_sqlitetasks2.php';
   var parameters = new Object();
   parameters['json'] = json;
   r.request = parameters;
   r.responder = snc_actionStatus;
   snc_send(r);    
}
function snc_send(r) {   
   inprogress();   
   r = cdm_wrapper(r);
   var ac = new AjaxClass();
   ac.url = r.target;
   ac.parameters = r.request;
   if (r.responder) ac.responder = r.responder;
   if (r.method) ac.Method = r.method;
   if (r.sync) ac.Async = !r.sync;
   ac.init();
   var resp = ac.send();
   if (r.sync) return resp;
}
function snc_actionStatus(json) {
   var json_obj = JSON.parse(json);
   if (json_obj.Settings) {
      var settings = json_obj.Settings;
      createCookie('Settings',JSON.stringify(settings));
   }
   if (json_obj.Status) {
      alert(json_obj.Status.Message);
   }
   if (json_obj.Choices) {
      var newchoices = json_obj.Choices;
      var choices = JSON.parse(readCookie('Choices'));
      if (!choices) choices = new Object();
      for (prop in newchoices) choices[prop] = newchoices[prop];
      createCookie('Choices',JSON.stringify(choices));
   }
   if (json_obj.Action) snc_actionDialog(json_obj.Action);
   if (json_obj.Options) snc_report_json_obj(json);
   s2_contacts_getdata();
   completed();
}
function snc_clearMenus(menuid) {
   var menudiv = document.getElementById(menuid);
   while(menudiv.childNodes.length > 0) {
      menudiv.removeChild(menudiv.lastChild);
   }
   return true;
}

function snc_report_json_obj(json) {
/*
   debugging code to print the returned json readably formatted to the screen  
   
   for some reason the last close bracket indent doesn't get removed.
*/
   var dialog,i,j,indent,tabs,extratab;
   snc_clearMenus('incontentdialog');   
   dialog = document.getElementById('incontentdialog');
   var json_obj = JSON.parse(json);
   var pre = document.createElement('PRE');
   var options = JSON.stringify(json_obj.Options);
   indent = 0;
   tabs = '';
   for (i=0;i<options.length;i++) {
      switch(options.substr(i,1)) {
      case ',': {
         tabs = '\n';
         for(j=0;j<indent;j++) tabs = tabs + '\t';          
         options = options.substr(0,i+1) + tabs + options.substr(i+1);
         i += tabs.length;      
      }break;
      case '{': {
         indent++;
         tabs = '\n';
         for(j=0;j<indent;j++) tabs = tabs + '\t';          
         options = options.substr(0,i+1) + tabs + options.substr(i+1);
         i += tabs.length;         
      }break;
      case '[': {
         indent++;
         tabs = '\n';
         for(j=0;j<indent;j++) tabs = tabs + '\t';          
         options = options.substr(0,i+1) + tabs + options.substr(i+1);
         i += tabs.length;
      }break;
      case '}': {
         if (options.substr(i+1,1) == ',') {
            indent--;
            tabs = '\n';
            for(j=0;j<indent;j++) tabs = tabs + '\t';          
            options = options.substr(0,i) + tabs + options.substr(i,2) + tabs + options.substr(i+2);
            i += tabs.length * 2;
         } else {
            indent-=2;
            tabs = '\n';
            extratab = (indent >= 0)?'\t':''; 
            for(j=0;j<indent;j++) tabs = tabs + '\t';
            options = options.substr(0,i) + tabs + extratab + options.substr(i,1) + tabs + options.substr(i+1);
            i += (tabs.length + 1) * 2; // extra 2 for the extra \t tab
         }      
      }break;
      case ']': {
         if (options.substr(i+1,1) == ',') {
            indent--;
            tabs = '\n';
            for(j=0;j<indent;j++) tabs = tabs + '\t';          
            options = options.substr(0,i) + tabs + options.substr(i,2) + tabs + options.substr(i+2);
            i += tabs.length * 2;
         } else {
            indent-=2;
            tabs = '\n';
            extratab = (indent >= 0)?'\t':''; 
            for(j=0;j<indent;j++) tabs = tabs + '\t';
            options = options.substr(0,i) + tabs + extratab + options.substr(i,1) + tabs + options.substr(i+1);
            i += (tabs.length + 1) * 2; // extra 2 for the extra \t tab
         }
      }break;
      }      
   }   
   pre.appendChild(document.createTextNode(options));
   dialog.appendChild(pre);      
   return true;
}

