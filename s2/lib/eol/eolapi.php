<?php
function eolgetresponse($url) {

   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
   $response = curl_exec($ch);
   curl_close($ch);
   return $response;
}
function eolremoveauthorities($names) {
   foreach ($names as $nk => $name) {
      $name = eolremoveauthority($name);
      $names[$nk] = $name;
   }
   return $names;
}
function eolremoveauthority($name) {
   // remove well formed name (Linnaeus, 1758)
   //$name = preg_replace('/\s+\([^\)]+\).*$/','',$name);
   $name = preg_replace('/\s+[\[,\(][^\),\]]+[\),\]].*$/','',$name);
   // remove badly formed name eg L. Koch, 1858...
   // but need to keep L. var. or L. ssp. or L. hybrid. x. or whatever.
   if (!preg_match('/[var|ssp]\./',$name)) $name = preg_replace('/\s+[A-Z].*$/','',$name);
   return $name;
}
function eolgetpagesforspecies($species) {
   $sp1 = urlencode($species);
   $url = "http://eol.org/api/search/1.0/${sp1}.json";//?exact=1";
   $res = eolgetresponse($url);
   $job = json_decode($res);
   $matched = false;
   $pageids = array();
   if (isset($job->results)) {
      foreach($job->results as $k => $t) {
         if (!$matched) {
            $names = eolremoveauthorities(preg_split('/\s*\;\s*/',$t->content));
            if (in_array($species,$names)) $pageids[] = $t->id;
         }
      }      
   }
   return $pageids;
}
$eolh = null;
//*
$eolh = array(
   529 => 'Species 2000 & ITIS Catalogue of Life: Annual Checklist 2010',
   759 => 'NCBI Taxonomy',      
   903 => 'Integrated Taxonomic Information System (ITIS)'
);
//*/      
function eolloadhierarchies() {
   global $eolh;
   if (!isset($eolh)) {
      $url = "http://eol.org/api/provider_hierarchies/1.0.json";
      $res = eolgetresponse($url);
      $job = json_decode($res);   
      $h = array();
      foreach ($job as $list) {
         $h[$list->id] = $list->label;
      }
      $eolh = $h;
   }
}
//eolloadhierarchies();
function eolgethierarchies() {  
   global $eolh;
   $h = $eolh;
   return $h;
}
function eolgettaxonidforpages($pageids) {
   $taxonid = null;
   $pageid;
   $namelists = eolgethierarchies();   
   foreach($pageids as $x => $pageid) {
      $url = "http://eol.org/api/pages/1.0/${pageid}.json?common_names=1&details=1&images=2&subjects=all&text=2";
      $res = eolgetresponse($url);
      $job = json_decode($res);   
      if (isset($job->taxonConcepts)) {
         foreach($job->taxonConcepts as $k => $t) {
            if (in_array($t->nameAccordingTo,$namelists)) $taxonid = $t->identifier;
         }      
      }
      if (isset($taxonid)) break;
   }
   return array($pageid,$taxonid);   
}
function eolgetranklist($taxonid,$species) {
   $url = "http://eol.org/api/hierarchy_entries/1.0/${taxonid}.json";
   $res = eolgetresponse($url);
   $job = json_decode($res);
   $pranks = array('kingdom','division','phylum','class','order','family','genus','species','infraspecies');   
   $ranks = array();
   if (isset($job->ancestors)) {
      foreach($job->ancestors as $k => $t) {
         if (isset($t->taxonRank) && in_array(strtolower($t->taxonRank),$pranks))  
            $ranks[strtolower($t->taxonRank)] = eolremoveauthority($t->scientificName);
      }         
   }
   $ranks[strtolower($job->taxonRank)] = $species; 
   return $ranks;
}
function eolgethierarchyfortaxa($taxonid,$species) {
   $url = "http://eol.org/api/hierarchy_entries/1.0/${taxonid}.json";
   $res = eolgetresponse($url);
   $job = json_decode($res);
//print "<pre>"; print_r($job); print "</pre>"; exit;
   $pranks = array('kingdom','division','phylum','class','order','family','genus','species','infraspecies');   

   $taxap = null;
   $taxa = null;
   $p = null;
   if (isset($job->ancestors)) {
      foreach($job->ancestors as $k => $t) {
         if (isset($t->taxonRank) && in_array(strtolower($t->taxonRank),$pranks)) {
            if (isset($taxa)) {
               $taxa->children = array();
               $taxa->children[$t->scientificName] = $t;
               $taxa = $taxa->children[$t->scientificName]; 
            } else {
               $taxap = $t;
               $taxa = $t;
            }
         }
      }         
   }
   unset($job->ancestors);
   //unset($job->children);
   $taxa->children = array();
   // get original species name to stop things like 
   // Arvicola amphibius reverting to terrestris
   $job->scientificName = $species;
   $taxa->children[$species] = $job;
   $h = array($taxap->scientificName => $taxap);
//print "<pre>"; print_r($h); print "</pre>"; exit;
   return $h;
}
function eolmergehierarchies($h1,$h2) {
   foreach($h2 as $n2 => $t2) {
      if (!isset($h1[$n2])) $h1[$n2] = $t2;
      else {
         $c1 = $h1[$n2]->children;
         $c2 = $t2->children;
         $h1[$n2]->children = eolmergehierarchies($c1,$c2);
      }
   }
   return $h1;
}
function eolgetsinglespecieshierarchy($sp1,$tx1=null) {
   $hierarchy = array();
   $parentage = array();   
   $pg1 = null;
   //$tx1 = null;
   $hr1 = null;
   $pgs = eolgetpagesforspecies($sp1);
   if (isset($pgs)) {
      list($pg1,$tx1) = eolgettaxonidforpages($pgs);
      if (isset($tx1)) {
         $hierarchy = eolgethierarchyfortaxa($tx1,$sp1);
      }
   }   
   return $hierarchy;
}
function eolgetspecieshierarchy($species) {
   $hierarchy = array();
   $parentage = array();   
   foreach($species as $sp1) {
      $pg1 = null;
      $tx1 = null;
      $hr1 = null;
      $pgs = eolgetpagesforspecies($sp1);
      if (isset($pgs)) {
         list($pg1,$tx1) = eolgettaxonidforpages($pgs);
         if (isset($tx1)) {
            $hr1 = eolgethierarchyfortaxa($tx1,$sp1);
            $hierarchy = eolmergehierarchies($hierarchy,$hr1);
         }
      }   
   }
   return $hierarchy;
}

function eolgethierarchylist($h) {
   $rooturl = 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'].'?species=';
   $html = "<ul class='tlist'>";
   foreach($h as $n1 => $t1) {
      // checking for taxonRank eliminates non taxonomic children
      if (isset($t1->taxonRank)) {
         $r1 = $t1->taxonRank;
         // subspecies are an indexed array rather than a hash
         $n1 = (is_numeric($n1))?$t1->scientificName:$n1;
         $n1 = eolremoveauthority($n1);
         $linkurl = $rooturl . $n1;  
         if (isset($t1->vernacularNames)) {
            $named = false;
            foreach ($t1->vernacularNames as $i => $name) {
               if (!$named && $name->language = 'en') { 
                  $n1 = $name->vernacularName . "<br/>($n1)"; 
                  $named = true;
               } 
            }
         }
         $html.= "<li class='tlist'><a id='$n1' href='$linkurl'>$n1</a>&nbsp;<span class='trank'>[$r1]</span>";
         if (isset($t1->children)) {
            $html.= "<br/>";
            $html.= eolgethierarchylist($t1->children);
         }
         $html.= "</li>";
      }
   }
   $html.= "</ul>";
   return $html;   
}

function eolgetvernacularname($s,$h) {
   $vn = null;
   foreach($h as $n1 => $t1) {
      if ($s == $n1) {
         if (isset($t1->vernacularNames)) {
            $named = false;
            foreach ($t1->vernacularNames as $i => $name) {
               if (!$named && $name->language == 'en') { 
                  $vn = $name->vernacularName; 
                  $named = true;
               } 
            }
         } 
      } else {
         if (isset($t1->children)) {
            $vn = eolgetvernacularname($s,$t1->children);
         }
      }
   }
   return $vn;
}
function eolgetcommonnamebylatin($s) {
   $h = eolgetsinglespecieshierarchy($s);
   $c = eolgetvernacularname($s,$h);
   return $c;
}

function eolgetspeciesmedia($species) {
   $pgs = eolgetpagesforspecies($species);
   $media = array();
   foreach($pgs as $pg1) {         
      if (isset($pg1)) {
         $url = "http://eol.org/api/pages/1.0/${pg1}.json?common_names=1&details=1&images=2&subjects=all&text=2";
         $res = eolgetresponse($url);
         $job = json_decode($res);  
//print "<pre>"; print_r($job); print "</pre>";
         if (isset($job->dataObjects)) {
            foreach($job->dataObjects as $i => $do) {
               // http://creativecommons.org/licenses/by-nc-sa/3.0/
               if (preg_match('/nc/',$do->license)) {
                  $dtype = preg_replace("/http\:\/{2}purl\.org\/dc\/dcmitype\//",'',$do->dataType);
                  if (!isset($media[$dtype])) $media[$dtype] = array();
                  $media[$dtype][] = $do;
               }
            }
         }      
      }
   }
   return $media;
}
function eolgetimages($media) {
   $html = "<div class='photocontainer'>"; 
   if (isset($media['StillImage'])) {
      foreach($media['StillImage'] as $i => $do) {
         $url = $do->mediaURL;
         //if (preg_match('/staticflickr/',$url)) {$url = preg_replace('/\_o\./','_m.',$url);}
         //else 
            $url = './imageresizer.php?h=240&url='.$url;
         $title = (isset($do->title))? $do->title:null;
         //$desc = (isset($do->description))? "<p>$do->description</p>":null;
         $credit = (isset($do->agents))?eolgetcredit($do->agents,'photographer'):null;
         $source = (isset($do->source))?$do->source:null;
         $imwraptop = null; $imwrapbtm = null;
         if (isset($source)) {
            $imwraptop = "<a target='source' href='${source}'>";
            $imwrapbtm = "</a>";
         }
         $html.= "<div class='photo'>${imwraptop}<img class='thumb' src='$url' alt='$title'/>${imwrapbtm}${credit}</div>";
      }
   }
   $html.= "</div>";
   return $html;
}
function eolgettext($media) {
   $html = ""; 
   if (isset($media['Text'])) {
      $texted = 0;
      foreach($media['Text'] as $i => $do) {
         if (($texted<1) && isset($do->description) && ! preg_match('/^\s*$/',$do->description)) {
            $do->description = preg_replace('/^\s+/','',$do->description);
            $do->description = preg_replace('/\s+$/','',$do->description);
            $html.= "<p class='inlinetext'>$do->description</p>";
            if (isset($do->references)) $html.= eolgetreferences($do->references);
            if (isset($do->agents)) $html.= eolgetcredit($do->agents,'author');
            $texted++;
         }
      }   
   }
   return $html;   
}
function eolgetreferences($refs) {
   $ref = null;
   if (count($refs)>0) $ref = "<div class='ref'><ul class='ref'><li class='ref'>".join("</li><li class='ref'>",$refs)."</li></ul></div>";
   return $ref;
}
function eolgetcredit($agents,$type) {
   $credit = null; 
   foreach($agents as $agent) {
      if ($agent->role == $type) {
         $link = (isset($agent->homepage))?$agent->homepage:null;
         $name = (isset($agent->full_name))?$agent->full_name:null;
         $cwraptop = null; $cwrapbtm = null;
         if (isset($link)) {
            $cwraptop = "<a href='${link}'>";
            $cwrapbtm = "</a>";
         }
         $credit = "<h4 class='credit'>${cwraptop}${name}${cwrapbtm}</h4>";
      }
   }
   return $credit;
}
?>