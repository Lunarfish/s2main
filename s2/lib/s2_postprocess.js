
var pp_type;
var pp_id;
var pp_stattimeout;
var pp_steptimeout;
var pp_update = true;
var pp_statgap = 5;
var pp_nextgap = 5;

function s2_showprocess(jobj) {
   var thisstep,nextstep,step,stepnum,btn,p,ipt,msg,a,hr;
   var statusnodeid,dialog,table,thead,tbody,trh,trb,th,td,tn,cname,div,h2;
   cleardialog('dialog-liner');
   dialog = document.getElementById('dialog-liner');
   statusnodeid = 's2pp_statusbar';
   
   div = document.createElement('DIV');
   div.id = statusnodeid;
   div.appendChild(document.createElement('HR'));
   h2 = document.createElement('H2');
   h2.appendChild(document.createTextNode('Process Status'));
   div.appendChild(h2);
   table = document.createElement('TABLE');
   trh = document.createElement('TR');
   trb = document.createElement('TR');
   thisstep = (jobj.Data && jobj.Data.PP_Step)?jobj.Data.PP_Step:0;
//alert(JSON.stringify(jobj.PostProcess));
   for(stepnum in jobj.PostProcess) {
      step = jobj.PostProcess[stepnum];
      cname = (stepnum <= thisstep)?'s2pp_completed':'s2pp_notrun'; 
      th = document.createElement('TH');
      th.appendChild(document.createTextNode('Step:'+(parseInt(stepnum)+1)));
      trh.appendChild(th);
      td = document.createElement('TD');
      td.className = cname;
      //td.appendChild(document.createTextNode(step.Name));
      a = document.createElement('A');
      a.href = '#'
      a.id = 's2ppstep-'+stepnum;
      a.appendChild(document.createTextNode(step.Name));
      a.onclick = function() {
         var json,jobj,step;
         step = parseInt(this.id.replace(/[^\-]+\-/,''));
         json = document.getElementById('s2_postprocess').value;
         jobj = JSON.parse(json);
         jobj.Data.PP_Step = (step-1);
         s2_postprocess_nextstep(jobj);
         return false;
      }
      td.appendChild(a);
      trb.appendChild(td);
   }
   thead = document.createElement('THEAD');
   tbody = document.createElement('TBODY');
   thead.appendChild(trh);
   tbody.appendChild(trb);
   table.appendChild(thead);
   table.appendChild(tbody);
   div.appendChild(table);
   dialog.appendChild(div);
   
   s2_postprocess_addprogressbars(div);
   
   thisstep = (jobj.Data && jobj.Data.PP_Step>0)?jobj.Data.PP_Step:0;
   nextstep = thisstep + 1;
   
   /*
   if (thisstep >= 0) {
      step = jobj.PostProcess[thisstep];
      h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode('Step ' + thisstep + ': ' + step.Name));
      dialog.appendChild(h2);
      p = document.createElement('P');
      msg = (jobj.Message)?jobj.Message:'Last step completed successfully.';
      p.appendChild(document.createTextNode(msg));
      dialog.appendChild(p);      
   }
   
   if (nextstep < jobj.PostProcess.length) {
      step = jobj.PostProcess[nextstep];
      h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode('Step ' + nextstep + ': ' + step.Name));
      dialog.appendChild(h2);
      p = document.createElement('P');
      p.appendChild(document.createTextNode('Continue to next step?'));      
      dialog.appendChild(p);
   }
   */
   btn = document.createElement('BUTTON');
   btn.appendChild(document.createTextNode('Cancel'));
   btn.onclick = function() {
      cleardialog('dialog-liner');
      hidedialog();
      if (pp_steptimeout) window.clearTimeout(pp_steptimeout);
   };
   dialog.appendChild(btn);
   if (nextstep < jobj.PostProcess.length) {
      btn = document.createElement('BUTTON');
      btn.appendChild(document.createTextNode('Continue'));
      btn.onclick = function() {
         var json,jobj;
         json = document.getElementById('s2_postprocess').value;
         jobj = JSON.parse(json);
         s2_postprocess_nextstep(jobj);
         return false;
      };
      dialog.appendChild(btn);
   }
   dialog.appendChild(document.createElement('HR'));   
   ipt = document.createElement('INPUT');
   ipt.type = 'hidden';
   ipt.id = 's2_postprocess';
   ipt.value = JSON.stringify(jobj);
   dialog.appendChild(ipt);
   pp_update = false;
   s2_postprocess_getstatus();
   completed();
   showdialog();
}

function s2_postprocess(jobj) {
   //completed();
   s2_showprocess(jobj);
   pp_update = false;
   if (pp_steptimeout) window.clearTimeout(pp_steptimeout);
   pp_steptimeout = window.setTimeout('s2_postprocess_autonext();',pp_nextgap*1000);
}   
function s2_postprocess_autonext() {
   var json,jobj;
   json = document.getElementById('s2_postprocess').value;
   jobj = JSON.parse(json);
   s2_postprocess_nextstep(jobj);         
}
function s2_postprocess_nextstep(jobj) {
   var req,params,step,thisstep,nextstep,user; 
   if (pp_steptimeout) window.clearTimeout(pp_steptimeout);
   thisstep = (jobj.Data && jobj.Data.PP_Step>=0)?jobj.Data.PP_Step:-1;
   nextstep = thisstep + 1;
   params = new Object();
   params.IType = jobj.IType;
   params.Current = jobj.Current;
   params.Action = 'PostProcess';
   params.Step = nextstep;
//   s2_query(params);
   pp_update = true; 
   //inprogress(); 
   user = s2_getcookie('u_info');
   if (user) params.UInfo = user; 
   req = new Object();
   req.target = s2_getcurrentpath()+'/S2_query.php';
   req.request = params;
   req.responder = s2_draw;
   snc_send(req);
   s2_postprocess_getstatus();
   completed();   
   showdialog();
}
function s2_postprocess_addprogressbars(parent) {
   var cont,pbar,h2,p,text,bars,bar,b,hr;
   bars = ['Step','Process'];
   for (b in bars) { 
      bar = bars[b];
      hr = document.createElement('HR');
      parent.appendChild(hr);
      cont = document.createElement('DIV');
      cont.id = 'pp-'+bar+'-container';
      h2 = document.createElement('H2');
      h2.style.display = 'inline';
      h2.style.marginRight = '10px';
      h2.id = 'pp-'+bar+'-name';
      h2.appendChild(document.createTextNode(bar + ' Status..'));
      cont.appendChild(h2);
      p = document.createElement('P');
      p.style.display = 'inline';
      p.id = 'pp-'+bar+'-status';
      cont.appendChild(p);
      pbar = document.createElement('DIV');
      pbar.id = 'pp-'+bar+'-progresscontainer';
      cont.appendChild(pbar);
      parent.appendChild(cont);
      s2_makeprogressbar('pp-'+bar+'-progresscontainer','pp-'+bar+'-progressbar',0);
   }
}
function s2_postprocess_getstatus() {
   var req,params,step,thisstep,nextstep,json,jobj,pnode;
   pnode = document.getElementById('s2_postprocess');
   if (pnode) { 
      json = pnode.value;
      jobj = JSON.parse(json);
      params = new Object();
      params.IType = jobj.IType;
      params.Current = jobj.Current;
      params.Action = 'ProcessStatus';
      user = s2_getcookie('u_info');
      if (user) params.UInfo = user; 
      req = new Object();
      req.target = s2_getcurrentpath()+'/S2_query.php';
      req.request = params;
      req.responder = s2_postprocess_updatestatus;
      snc_send(req);
   }   
}
function s2_postprocess_updatestatus(json) {
   var div,statusnodeid,jobj,bars,b,bar,node,text,tnode;
   try {
      jobj = JSON.parse(json);
      bars = ['Step','Process'];
      for (b in bars) { 
         bar = bars[b];
         if (jobj.Process) {
            text = jobj.Process.Status;
            node = document.getElementById('pp-Process-status');
            tnode = document.createTextNode(text);
            if (node.childNodes.length>0) node.replaceChild(tnode,node.firstChild);
            else node.appendChild(tnode);
            s2_updateprogressbar('pp-Process-progressbar',jobj.Process.Percentage_Complete);
         }
         if (jobj.Step) {
            text = 'Step ' + jobj.Step.Number + ': ' + jobj.Step.Name + '..'; 
            node = document.getElementById('pp-Step-name');
            tnode = document.createTextNode(text);
            if (node.childNodes.length>0) node.replaceChild(tnode,node.firstChild);
            else node.appendChild(tnode);
            text = jobj.Step.Status;
            node = document.getElementById('pp-Step-status');
            tnode = document.createTextNode(text);
            if (node.childNodes.length>0) node.replaceChild(tnode,node.firstChild);
            else node.appendChild(tnode);
            s2_updateprogressbar('pp-Step-progressbar',jobj.Step.Percentage_Complete)
         }
      }
      if (pp_update) {
         if (pp_stattimeout) window.clearTimeout(pp_stattimeout);
         pp_stattimeout = window.setTimeout('s2_postprocess_getstatus();',pp_statgap*1000);
      }
   } catch(e) {
      pp_update = false;
   }
   completed();
   showdialog();
}
