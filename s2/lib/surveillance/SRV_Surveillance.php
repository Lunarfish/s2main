<?php
class SRV_Site extends DBT {
   protected $tablename = 'SRV_Site';
   protected $displayname = 'Site';
   protected $alts = array('Programmes'=>'SRV_Programme');
   protected $show = array('SRV_Site_Boundary','SRV_Site_Description');
   protected $view = '[
      {
         "Name":"Overview",
         "Data":[
            {
               "Name":"Current Description",
               "Table":"SRV_Site_Description",
               "Show":"Current"
            },
            {
               "Name":"Current Boundary",
               "Table":"SRV_Site_Boundary",
               "Show":"Current"
            },
            {
               "Name":"Photos",
               "Table":"SRV_Site_Photo"
            }            
         ]        
      },
      {
         "Name":"Descriptions",
         "Data":[
            {
               "Table":"SRV_Site_Description"
            }
         ]
      },
      {
         "Name":"Boundaries",
         "Data":[
            {
               "Name":"Boundary History",
               "Table":"SRV_Site_Boundary",
               "Show":"All"               
            },
            {
               "Name":"Compartments",
               "Table":"SRV_Site_Compartment",
               "Show":"All"
            }
         ]
      },
      {
         "Name":"Access",
         "Data":[
            {
               "Name":"Access",
               "Table":"SRV_Site_Access"
            },
            {
               "Name":"Considerations",
               "Table":"SRV_Site_Consideration"
            },
            {
               "Name":"Contacts",
               "Table":"SRV_Site_Contact"
            },
            {
               "Name":"Notification",
               "Table":"SRV_Site_Notification_Rule"
            }
         ]
      },
      {
         "Name":"Surveys",
         "Data": [
            {
               "Name":"Surveys",
               "Table":"SRV_Site_Survey"
            },
            {
               "Name":"Notification Rules",
               "Table":"SRV_Site_Notification_Rule"
            },
            {
               "Name":"Considerations",
               "Table":"SRV_Site_Consideration"
            }
         ]
      }
   ]'; 
   protected $columns = array(
      'SRV_Site_ID'     => 'Unique Key',
      'Name'            => 'Short Text',
      'Zone'            => '{"DataType":"Short Text","Derives":[{"Column":"Site_Code","Method":"s2_nextsitecode","Params":[{"Type":"Static","Value":"SRV_Site"},{"Type":"Static","Value":"Site_Code"},{"Type":"Value"}]}]}',
      'Site_Code'       => 'Short Text'
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $defaultpermissions = array(
      'USR_Permissions' => '
"User_Id","Domain","Label","Value"
1,"surveillance","Registered User",1
1,"surveillance","Programme Member",1
1,"surveillance","Programme Administrator",1
1,"surveillance","System Administrator",1',
      'USR_Default_Permissions' => '
"Domain","Label","Description","Value","Anon" 
"surveillance","System Administrator","Add and edit programmes",0,0
"surveillance","Programme Administrator","Add and edit programme data",0,0
"surveillance","Programme Member","View programme data",0,0
"surveillance","Registered User","View unprotected data",1,0'   
   );      
}
class SRV_Programme extends DBT {
   protected $tablename = 'SRV_Programme';
   protected $displayname = 'Programme';
   protected $alts = array('Sites'=>'SRV_Site');
   protected $show = array('SRV_Programme_Surveyor','SRV_Site_Survey');
   protected $columns = array(
      'SRV_Programme_ID'   => 'Unique Key',
      'Name'               => 'Short Text',
      'Established'        => 'Date'
   );
   protected $domain = 'surveillance';
   protected $instancedomain = true;
   protected $instanceof = 'SRV_Programme';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );     
}
class SRV_Programme_Surveyor extends DBT {
   protected $tablename = 'SRV_Programme_Surveyor';
   protected $displayname = 'Surveyor';
   //protected $alts = array('Programme'=>'SRV_Programme','Sites'=>'SRV_Site');
   protected $columns = array(
      'SRV_Programme_Surveyor_ID'   => 'Unique Key',
      'SRV_Programme_ID'            => '{"DataType":"LINKEDTO","TargetType":"SRV_Programme","TargetField":"SRV_Programme_ID","Mandatory":1,"Current":1}',
      'Surveyor'                    => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"],"Mandatory":1}',
      'Contracted'                  => 'Date'
   );
   protected $domain = 'surveillance';
   protected $instancedomain = true;
   protected $instanceof = 'SRV_Programme';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      //return $this->data['Surveyor']->Name;
      return $this->derivename(array('SRV_Programme_ID','Surveyor'));
   }      
}

class SRV_Site_Description extends DBT {
   protected $tablename = 'SRV_Site_Description';
   protected $displayname = 'Description';
   protected $show = array();
   protected $columns = array(
      'SRV_Site_Description_ID'  => 'Unique Key',
      'SRV_Site_ID'              => '{"DataType":"LINKEDTO","TargetType":"SRV_Site","TargetField":"SRV_Site_ID","Mandatory":1,"Current":1}',
      'Survey_Date'              => 'Date',
      'Site_Description'         => 'Long Text',
      'Version'                  => '{"DataType":"Version","VersionOfType":"SRV_Site","VersionOfField":"SRV_Site_ID"}' 
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'Programme Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      return "V" . $this->data['Version'] . " (" . $this->data['Survey_Date'] . ")";
   }   
}

class SRV_Site_Boundary extends DBT {
   protected $tablename = 'SRV_Site_Boundary';
   protected $displayname = 'Boundary';
   protected $show = array();
   protected $columns = array(
      'SRV_Site_Boundary_ID'     => 'Unique Key',
      'SRV_Site_ID'              => '{"DataType":"LINKEDTO","TargetType":"SRV_Site","TargetField":"SRV_Site_ID","Mandatory":1,"Current":1,"Inherit":1}',
      'Review_Date'              => 'Date',
      'Boundary'                 => 'Map Data',
      'Notes'                    => 'Long Text',
      'Version'                  => '{"DataType":"Version","VersionOfType":"SRV_Site","VersionOfField":"SRV_Site_ID"}' 
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'Programme Administrator'
   );
   function getname() {
      return "V" . $this->data['Version'] . " (" . preg_replace('/(\d{4})\-(\d{2})\-(\d{2})/',"$3/$2/$1",$this->data['Review_Date']) . ")";
   }   
}
class SRV_Site_Compartment extends DBT {
   protected $tablename = 'SRV_Site_Compartment';
   protected $displayname = 'Compartment';
   protected $perpage = 10;
   protected $show = array('SRV_Site_Compartment_Boundary');
   protected $columns = array(
      'SRV_Site_Compartment_ID'  => 'Unique Key',
      'SRV_Site_ID'              => '{"DataType":"LINKEDTO","TargetType":"SRV_Site","TargetField":"SRV_Site_ID","Current":1,"Inherit":1}',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Compartment_Code'         => '{"DataType":"Short Text","Mandatory":1}',
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'Programme Administrator',
      'List'   => 'Programme Member',
      'View'   => 'Programme Member'
   );
   function getname() {
      return  $this->data['Compartment_Code'] . " : " . $this->data['Name'];
   }
   function getshortname() {
      return  $this->data['Compartment_Code'];
   }
}
class SRV_Site_Compartment_Boundary extends DBT {
   protected $tablename = 'SRV_Site_Compartment_Boundary';
   protected $displayname = 'Compartment Boundary';
   protected $show = array();
   protected $columns = array(
      'SRV_Site_Compartment_Boundary_ID'  => 'Unique Key',
      'SRV_Site_Compartment_ID'           => '{"DataType":"LINKEDTO","TargetType":"SRV_Site_Compartment","TargetField":"SRV_Site_Compartment_ID","Mandatory":1,"Current":1,"Inherit":1}',
      'Review_Date'                       => 'Date',
      'Boundary'                          => 'Map Data',
      'Notes'                             => 'Long Text',
      'Version'                           => '{"DataType":"Version","VersionOfType":"SRV_Site_Compartment","VersionOfField":"SRV_Site_Compartment_ID"}' 
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'Programme Administrator'
   );
   function getname() {
      return "V" . $this->data['Version'] . " (" . $this->data['Review_Date'] . ")";
   }   
}

class SRV_Site_Survey extends DBT {
   protected $tablename = 'SRV_Site_Survey';
   protected $displayname = 'Site Survey';
   //protected $alts = array('Programme'=>'SRV_Programme','Sites'=>'SRV_Site');
   protected $show = array('SRV_Survey_Notification','SRV_Site_Surveyor');
   protected $columns = array(
      'SRV_Site_Survey_ID'    => 'Unique Key',
      'SRV_Programme_ID'      => '{"DataType":"LINKEDTO","TargetType":"SRV_Programme","TargetField":"SRV_Programme_ID","Current":1}',
      'SRV_Site_ID'           => '{"DataType":"LINKEDTO","TargetType":"SRV_Site","TargetField":"SRV_Site_ID","Current":1}',
      'Date_From'             => 'Date',
      'Date_To'               => 'Date',      
      'Compartments'          => '{"DataType":"LINKEDTOM","TargetType":"SRV_Site_Compartment","TargetField":"SRV_Site_Compartment_ID","Property":"Compartments","Inherit":1}'
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'Programme Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}

class SRV_Site_Surveyor extends DBT {
   protected $tablename = 'SRV_Site_Surveyor';
   protected $displayname = 'Site Surveyor';
   //protected $alts = array('Programme'=>'SRV_Programme','Sites'=>'SRV_Site');
   //protected $show = array('SRV_Survey_Notification');
   protected $columns = array(
      'SRV_Site_Surveyor_ID'        => 'Unique Key',
      'SRV_Programme_Surveyor_ID'   => '{"DataType":"LINKEDTO","TargetType":"SRV_Programme_Surveyor","TargetField":"SRV_Programme_Surveyor_ID","Inherit":1}',
      'SRV_Site_Survey_ID'          => '{"DataType":"LINKEDTO","TargetType":"SRV_Site_Survey","TargetField":"SRV_Site_Survey_ID","Current":1}',
      'Contracted'                  => 'Date',
      'Pack_Sent'                   => 'Date',
      'Data_Received'               => 'Date'
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      return $this->derivename(array('SRV_Programme_Surveyor_ID','Contracted'));
   }   
}

class SRV_Survey_Notification extends DBT {
   protected $tablename = 'SRV_Survey_Notification';
   protected $displayname = 'Survey Notification';
   //protected $alts = array('Programme'=>'SRV_Programme','Sites'=>'SRV_Site');
   protected $columns = array(
      'SRV_Survey_Notification_ID'  => 'Unique Key',
      'SRV_Site_Survey_ID'          => '{"DataType":"LINKEDTO","TargetType":"SRV_Site_Survey","TargetField":"SRV_Site_Survey_ID","Current":1,"Inherit":1}',
      'Sent'                        => 'Date',
      'Acknowledged'                => 'Date',
      'Access_Granted'              => 'True or False'
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'Programme Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );      
}

class SRV_Site_Access extends DBT {
   protected $tablename = 'SRV_Site_Access';
   protected $displayname = 'Site Access';
   //protected $alts = array('Programme'=>'SRV_Programme','Sites'=>'SRV_Site');
   protected $columns = array(
      'SRV_Site_Access_ID'    => 'Unique Key',
      'SRV_Site_ID'           => '{"DataType":"LINKEDTO","TargetType":"SRV_Site","TargetField":"SRV_Site_ID","Current":1,"Inherit":1}',
      'SRV_Access_Type_ID'    => '{"DataType":"LINKEDTO","TargetType":"SRV_Access_Type","TargetField":"SRV_Access_Type_ID"}'
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );   
}

class SRV_Access_Type extends DBT {
   protected $tablename = 'SRV_Access_Type';
   protected $displayname = 'Access Type';
   protected $alts = array('Programme'=>'SRV_Programme','Sites'=>'SRV_Site');
   protected $columns = array(
      'SRV_Access_Type_ID' => 'Unique Key',
      'Name'               => 'Short Text',
      'Description'        => 'Long Text' 
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'SRV_Access_Type' => '
"Name","Description"
"Public Access","Road verges and such."
"Open Access","Private land but with public right of way."
"Private (Unknown)","Private land where contact has not been made with land owner."
"Private (Agreed)","Private land where survey rights have been granted."
"Private (Agreed - With Notification)","Private land where survey rights have been granted but only with prior notification."
"Private (Refused)","Private land where permission to survey has been refused."'
   );         
}

class SRV_Site_Consideration extends DBT {
   protected $tablename = 'SRV_Site_Consideration';
   protected $displayname = 'Access Consideration';
   //protected $alts = array('Programme'=>'SRV_Programme','Sites'=>'SRV_Site');
   protected $columns = array(
      'SRV_Site_Consideration_Type_ID' => 'Unique Key',
      'SRV_Site_ID'                    => '{"DataType":"LINKEDTO","TargetType":"SRV_Site","TargetField":"SRV_Site_ID","Current":1,"Inherit":1}',
      'SRV_Consideration_Type_ID'      => '{"DataType":"LINKEDTO","TargetType":"SRV_Consideration_Type","TargetField":"SRV_Consideration_Type_ID"}',
      'Name'                           => 'Short Text',
      'Description'                    => 'Long Text' 
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      return $this->derivename(array('Name','SRV_Consideration_Type_ID'));
   }      
}

class SRV_Consideration_Type extends DBT {
   protected $tablename = 'SRV_Consideration_Type';
   protected $displayname = 'Consideration Type';
   protected $alts = array('Programme'=>'SRV_Programme','Sites'=>'SRV_Site');
   protected $columns = array(
      'SRV_Consideration_Type_ID'   => 'Unique Key',
      'Name'                        => 'Short Text',
      'Description'                 => 'Long Text' 
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'SRV_Consideration_Type' => '
"Name","Description"
"Restriction","Site unavailable for survey between given dates."
"Hazard","Electric fences, Steep banks, Barbed wire ..."
"Copy Data","Send copy of survey date to land owner."
"Condition","Other condition."'
   );            
}

class SRV_Site_Contact extends DBT {
   protected $tablename = 'SRV_Site_Contact';
   protected $displayname = 'Site Contact';
   protected $show = array();
   protected $columns = array(
      'SRV_Site_Contact_ID'   => 'Unique Key',
      'SRV_Site_ID'           => '{"DataType":"LINKEDTO","TargetType":"SRV_Site","TargetField":"SRV_Site_ID","Mandatory":1,"Current":1}',
      'Contact'               => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"],"Mandatory":1}',
      'Type'                  => '{"DataType":"LINKEDTO","TargetType":"SRV_Contact_Type","TargetField":"SRV_Contact_Type_ID"}',
      'From_Date'             => 'Date',
      'Until_Date'            => 'Date',               
      'Compartments'          => '{"DataType":"LINKEDTOM","TargetType":"SRV_Site_Compartment","TargetField":"SRV_Site_Compartment_ID","Property":"Compartments","Inherit":1}'
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'Programme Administrator'
   );
   function getname() {
      return $this->derivename(array('Contact','Type'));
   }      
}

class SRV_Contact_Type extends DBT {
   protected $tablename = 'SRV_Contact_Type';
   protected $displayname = 'Contact Type';
   protected $alts = array('Programme'=>'SRV_Programme','Sites'=>'SRV_Site');
   protected $columns = array(
      'SRV_Contact_Type_ID'   => 'Unique Key',
      'Name'                  => 'Short Text',
      'Description'           => 'Long Text' 
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'SRV_Contact_Type' => '
"Name","Description"
"Owner",""
"Tenant",""
"Agent",""
"Manager",""
"Game Keeper",""
"Warden",""'
   );         
}

class SRV_Site_Notification_Rule extends DBT {
   protected $tablename = 'SRV_Site_Notification_Rule';
   protected $displayname = 'Site Notification Rule';
   //protected $alts = array('Programme'=>'SRV_Programme','Sites'=>'SRV_Site');
   protected $columns = array(
      'SRV_Site_Notification_Rule_ID'  => 'Unique Key',
      'SRV_Site_ID'                    => '{"DataType":"LINKEDTO","TargetType":"SRV_Site","TargetField":"SRV_Site_ID","Mandatory":1,"Current":1}',
      'SRV_Site_Contact_ID'            => '{"DataType":"LINKEDTO","TargetType":"SRV_Site_Contact","TargetField":"SRV_Site_Contact_ID","Inherit":1}',
      'Type'                           => '{"DataType":"LINKEDTO","TargetType":"SRV_Notification_Type","TargetField":"SRV_Notification_Type_ID"}',
      'Media'                          => '{"DataType":"LINKEDTO","TargetType":"SRV_Media_Type","TargetField":"SRV_Media_Type_ID"}',
      'Notice_Period_Days'             => 'Number',        
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'Programme Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      return $this->derivename(array('Media','Type'));
   }
}

class SRV_Notification_Type extends DBT {
   protected $tablename = 'SRV_Notification_Type';
   protected $displayname = 'Notification Type';
   protected $alts = array('Programme'=>'SRV_Programme','Sites'=>'SRV_Site');
   protected $columns = array(
      'SRV_Notification_Type_ID' => 'Unique Key',
      'Name'               => 'Short Text',
      'Description'        => 'Long Text' 
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'SRV_Notification_Type' => '
"Name","Description"
"Required","Surveyors cannot access land without prior notification."
"Courtesy","Land owner would like to know that people are on their land."
"Preferred","Land owner would prefer notification in advance but it is not a requirement."'
   );         
}
class SRV_Media_Type extends DBT {
   protected $tablename = 'SRV_Media_Type';
   protected $displayname = 'Media';
   protected $alts = array('Programme'=>'SRV_Programme','Sites'=>'SRV_Site');
   protected $columns = array(
      'SRV_Media_Type_ID' => 'Unique Key',
      'Name'               => 'Short Text',
      'Description'        => 'Long Text' 
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'SRV_Media_Type' => '
"Name","Description"
"Written (Post)",""
"Written (Email)",""
"Phone",""
"Person","Knock at address before carrying out survey."'
   );         
}

class SRV_Site_Photo extends DBT {
   protected $tablename = 'SRV_Site_Photo';
   protected $displayname = 'Photo';
   protected $columns = array(
      'SRV_Site_Photo_ID'  => 'Unique Key',
      'SRV_Site_ID'        => '{"DataType":"LINKEDTO","TargetType":"SRV_Site","TargetField":"SRV_Site_ID","Current":1}',
      'Title'              => '{"DataType":"Short Text"}',
      'Description'        => '{"DataType":"Long Text","NoList":1}',
      'Photographer'       => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"]}',
      'Received'           => '{"DataType":"Date","NoList":1}',
      'File'               => '{"DataType":"File Path","Mandatory":1}'
   );
   protected $domain = 'surveillance';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'Programme Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      return $this->derivename(array('Title','Photographer'));
   }
}

?>