var baseurl   = 'http://dan.lunarfish.co.uk/s2/';
var nbnapiurl = baseurl+'taxa/nbnapi.php';
var taxacache = [];
var nbn_cells = [];
var nbn_tcols = [];
var nbn_ctab;
var nbn_show;
 
function s2_nbnify() {
   var tabs,t,tab; 
   tabs = document.getElementsByTagName('TABLE');
   for(t=0;t<tabs.length;t++) {
      tab = tabs[t];
      if (!tab.id) tab.id = 'tab_'+t; 
      s2_nbnifytab(tab);
   }
}
function s2_nbntaxa_gettvkcols(tab) {
   var tr,th,tvkcols;
   tvkcols = [];
   tr = tab.tHead.rows[0];
   for(cx=0;cx<tr.cells.length;cx++) {
      th = tr.cells[cx];
      if (th.className && th.className.match(/TVK/)) tvkcols.push(cx); 
   }
   return tvkcols; 
}
function s2_nbnifytab(tab) {
   var o,tvkcols,addcols,tbody,thead,tr,th,td,nde,tvk,tx,rx,cx,row,col,tid,cid,addCN,addTG;
   tbody = tab.tBodies[0];
   o = (tab.tHead)?'h':'v'; // list tables have headings across the top 
   switch(o) {
   case 'h': {
      tvkcols = s2_nbntaxa_gettvkcols(tab);
      for(rx=0;rx<tbody.rows.length;rx++) {
         tr = tbody.rows[rx];
         for(tx=0;tx<tvkcols.length;tx++) {
            cx = tvkcols[tx];
            td = tr.cells[cx];
            if (!td.id) td.id = tab.id+'r'+rx+'c'+cx;
            s2_nbntaxa_loadcell(td);
         }
      }
   }break;
   case 'v': {
      for(rx=0;rx<tbody.rows.length;rx++) {
         tr = tbody.rows[rx];
         for(cx=0;cx<tr.cells.length;cx++) {
            td = tr.cells[cx];
            if (td.nodeName == 'TD' && td.className.match(/TVK/)) {
               if (!td.id) td.id = tab.id+'r'+rx+'c'+cx;
               s2_nbntaxa_loadcell(td);
            }
         }
      }     
   }break;
   }
}
function s2_nbntaxa_loadcell(cel) {
   var tvk;
   tvk = s2_getndeval(cel);
   if (!nbn_cells[cel.id]) nbn_cells[cel.id] = tvk;
   s2_nbntaxa_gettvkjson(tvk);      
}
function s2_nbntaxa_send(url,responder,label) {
   var ac,resp;
   ac = new AjaxClass();
   ac.url = url;
   ac.responder = responder;
   ac.Method = 'GET';
   ac.Async = true;
   ac.label = label;
   ac.init();
   resp = ac.send();            
}
function s2_nbntaxa_gettvkjson(tvk) {
   var url;
   if (tvk.match(/[A-Z]{6}\d{10}/)) {
      if (!taxacache[tvk] || taxacache[tvk] == 'null') { 
         url = nbnapiurl+'/taxa/'+tvk;
         s2_nbntaxa_send(url,s2_nbntaxa_loadtvkjson);
      } else {
         s2_nbntaxa_loadtvkjson(taxacache[tvk]);
      }            
   }
}
function s2_nbntaxa_loadtvkjson(json) {
   var taxa,tvk,ctvk,nx,cx,cid,cel;
   taxa = JSON.parse(json);
   tvk = taxa.taxonVersionKey;
   if (!taxacache[tvk] || taxacache[tvk] == 'null') taxacache[tvk] = json;
   // if the commmon name is not populated get the synonyms straight away
   if (!taxa.commonName) s2_nbntaxa_getsynonyms(tvk);
   if (!taxa.designations) s2_nbntaxa_getdesignations(tvk);
   
   for(cid in nbn_cells) {
      ctvk = nbn_cells[cid];
      if (tvk == ctvk) {
         cel = document.getElementById(cid);
         s2_nbntaxa_loadtaxa(cel,taxa);
      }   
   }
}
function s2_nbntaxa_settab(el) {
   var tab,tabid;
   // use elemment to get 
   tab = el;
   while(tab && tab.nodeName != 'TABLE') tab = tab.parentNode;
   tabid = tab.id;
   nbn_ctab = tabid;
}
function s2_nbntaxa_gettab() {
   return nbn_ctab;
}
function s2_nbntaxa_loadtaxa(cel,taxa) {
   var lnk,txt,act,tab,addcols;
   lnk = document.createElement('A');
   lnk.style.paddingLeft = '5px';
   lnk.appendChild(document.createTextNode(taxa.name)); 
   if (!cel.className.match(/NoRevise/) && s2_nbntaxa_toberevised(taxa)) {
      lnk.appendChild(document.createTextNode(' [updating to:'+taxa.ptaxonVersionKey + ']'));
      s2_nbntaxa_revisetaxa(cel,taxa.ptaxonVersionKey);
      s2_nbntaxa_postrevision(taxa.taxonVersionKey,taxa.ptaxonVersionKey);
   } else {
      delete nbn_cells[cel.id];
   }  
   lnk.alt = taxa.taxonVersionKey;
   lnk.href = '#';
   lnk.onclick = function() {s2_nbntaxa_settab(this);s2_nbntaxa_showtaxa(this);};
   switch(cel.firstChild.nodeName) {
   case 'INPUT': {
      cel.firstChild.type = 'hidden'; 
      cel.firstChild.value = taxa.taxonVersionKey;
      cel.insertBefore(lnk,cel.firstChild);
   }break;
   default: cel.replaceChild(lnk,cel.firstChild); break;
   }
}
function s2_nbntaxa_toberevised(taxa) {
   return (taxa.taxonVersionKey != taxa.ptaxonVersionKey); 
}
function s2_nbntaxa_revisetaxa(cel,tvk) {
   nbn_cells[cel.id] = tvk;
   s2_nbntaxa_gettvkjson(tvk);
}
function s2_nbntaxa_resusenode(id) {
   var node;
   node = document.getElementById(id);
   if (node) {
      while(node.childNodes.length > 0) node.removeChild(node.lastChild);
   } else {
      node = document.createElement('DIV');
      node.id = id;
   }                                                                      
   return node;
}
function s2_nbntaxa_gettabparent(el) {
   while (el && el.nodeName != 'TABLE') el = el.parentNode;
   return el; 
}
function s2_nbntaxa_showtaxa(lnk) {
   var id,taxa,ti,node,text,name,tvk,btn,p,prop,val,suppress;
   var tab,tabid,tab,tr,th,td,btn,img,o;
   tvk = lnk.alt;
   tab = s2_nbntaxa_gettabparent(lnk);
   o = (tab&&tab.tHead)?'h':'v'; // list tables have headings across the top 
     
   taxa = JSON.parse(taxacache[tvk]);
   nbn_show = tvk;
   if (!taxa.synonyms) s2_nbntaxa_getsynonyms(tvk);
   if (!taxa.designations) s2_nbntaxa_getdesignations(tvk);
   cleardialog('dialog-liner');
   dialog = document.getElementById('dialog-liner');
   id = 's2taxa'; 
   node = s2_nbntaxa_resusenode(id);
   tabid = s2_nbntaxa_gettab();
   tab = document.createElement('TABLE');
   suppress = [
      'commonNameTaxonVersionKey',
      'gatewayRecordCount',
      'href',
      'languageKey',
      'nameStatus',
      'ptaxonVersionKey',
      'taxonVersionKey',
      'taxonOutputGroupKey',
      'organismKey',
      'versionForm'
   ];
   for(p in taxa) {
      val = taxa[p];
      if (suppress.indexOf(p)<0
         && val != '' 
         && val != []
         && val != {}) {
         prop = s2_nbntaxa_colnamefromjsonprop(p);
         tr = document.createElement('TR');
         th = document.createElement('TH');
         th.appendChild(document.createTextNode(prop));
         tr.appendChild(th);
         td = document.createElement('TD');
         val = s2_objecttonode(val); 
         td.appendChild(val);
         if (o=='h'&&val.nodeName == '#text') {
            btn = document.createElement('BUTTON');
            btn.id = 't-'+tabid+'_p-'+p; 
            btn.className = 'inline';
            img = new Image();
            if (nbn_tcols[tabid] && nbn_tcols[tabid][p]) {
               img.src = baseurl+'images/colicon-rem.png';
               img.alt = 'Remove column';
               btn.onclick = function() {s2_nbntaxa_remascolumn(this.id);};
            } else {
               img.src = baseurl+'images/colicon-add.png';
               img.alt = 'Add column';
               btn.onclick = function() {s2_nbntaxa_addascolumn(this.id);};
            } 
            btn.appendChild(img);
            td.appendChild(btn);
         }
         tr.appendChild(td); 
         tab.appendChild(tr);
      }
   }
   node.appendChild(tab);
   dialog.appendChild(node);
   showdialog();    
}
function s2_objecttonode(obj) {
   var node,ul,li,x,span,vals,text;
   switch (typeof obj) {
   case 'number': node = document.createTextNode(obj); break;
   case 'string': node = document.createTextNode(obj); break;
   case 'array': {
      node = document.createElement('UL');
      node.style.display = 'inline-block';
      for(x in obj) {
         li = document.createElement('LI');
         li.appendChild(s2_objecttonode(obj[x]));
         node.appendChild(li);
      }
   }break;
   case 'object': {
      node = document.createElement('UL');
      node.style.display = 'inline-block';
      for(x in obj) {
         li = document.createElement('LI');
         if (!x.match(/^\d+$/)) {
            span = document.createElement('SPAN');
            span.style.fontWeight = 'bold';
            span.appendChild(document.createTextNode(x + ': '));
            li.appendChild(span);
         }
         li.appendChild(s2_objecttonode(obj[x]));
         node.appendChild(li);
      }
   }break;
   default: node = document.createTextNode(typeof obj); break;
   }
   return node;
}
function s2_nbntaxa_postrevision(oldtvk,newtvk) {
   var args = new Object();
   args.IType     = 'RCD_Taxon_Revision';
   args.Action    = 'Add';
   args.Old_Taxa  = oldtvk;
   args.New_Taxa  = newtvk;
   args.Reason    = 'Taxonomic Revision';
   args.Date      = s2_getdatetoday();
   args.NoDraw    = 1;
   args.Refresh   = 1;
   s2_query(args);                                                                            
}
function s2_nbntaxa_getsynonyms(tvk) {
   var url;
   url = nbnapiurl+'/taxa/'+tvk+'/synonyms';
   s2_nbntaxa_send(url,s2_nbntaxa_loadsynjson);            
}
function s2_nbntaxa_loadsynjson(json) {
   var synlist,syn,taxa,ptvk,err;
   synlist = JSON.parse(json);
   taxa = null
   for(sid in synlist) {
      syn = synlist[sid];
      ptvk = syn.ptaxonVersionKey;
      try {
         if (!taxa && taxacache[ptvk]) taxa = JSON.parse(taxacache[ptvk]);
         // even if there are no synonyms create an empty array so that request 
         // is not repeatedly re-sent
         if (taxa) {
            if (!taxa.synonyms) taxa.synonyms = [];            
            if (ptvk != syn.taxonVersionKey) {
               switch(syn.languageKey) {
               case 'en': {// common name
                  if (!taxa.commonName && syn.nameStatus == 'Recommended') taxa.commonName = syn.name;
                  else if (!taxa.alternateNames) taxa.alternateNames = []; 
                  if (syn.name != taxa.commonName 
                     && (taxa.alternateNames.indexOf(syn.name) < 0)) taxa.alternateNames.push(syn.name); 
               }break;
               case 'la': {// latin name
                  if ((taxa.name != syn.name) 
                     && (syn.nameStatus == 'Synonym') 
                     && (taxa.synonyms.indexOf(syn.name) < 0)) taxa.synonyms.push(syn.name);
               }break;
               }
            }
         }
      } catch(err) {
         //alert(taxacache[ptvk]);
      }
   }
   taxacache[ptvk] = JSON.stringify(taxa);
   s2_nbntaxa_edittaxa(ptvk);
}
function s2_nbntaxa_edittaxa(tvk) {
   if (s2_livedialog()) s2_nbntaxa_showtaxa(tvk);
}
function s2_nbntaxa_getdesignations(tvk) {
   var url,lab;
   url = nbnapiurl+'/taxa/'+tvk+'/designations';
   s2_nbntaxa_send(url,s2_nbntaxa_loaddesjson,tvk);            
}
function s2_nbntaxa_loaddesjson(json) {
   var deslist,syn,taxa,ptvk,d,des,obj;
   ptvk = json.replace(/\${2}([^\$]+)\${2}.*/,"$1");
   json = json.replace(/\${2}[^\$]+\${2}(.*)/,"$1");
   deslist = JSON.parse(json);
   if (taxacache[ptvk]) {
      taxa = JSON.parse(taxacache[ptvk]);
      if (taxa) {
         taxa.designations = [];
         for(d in deslist) {
            des = deslist[d];
            taxa.designations.push(des.designation.label);
         }
         taxacache[ptvk] = JSON.stringify(taxa);
         s2_nbntaxa_edittaxa(ptvk);
      }
   }
}

var element;
function s2_nbntaxa_eloffsets(el) {
	var x = 0;
   var y = 0;
	if (el.offsetParent) {
	  do {
	  	  x += el.offsetLeft;
		  y += el.offsetTop;
	  } while (el = el.offsetParent);
	}
	return {"x":x,"y":y};
}

function s2_nbntaxa_colnamefromjsonprop(prop) {
   prop = prop.replace(/([A-Z])/g," $1");
   prop = prop.replace(/^([a-z])/,function(v) {return v.toUpperCase();});
   return prop;
}


function s2_nbntaxa_remascolumn(id) {
   var args,tvk,taxa,tab,tabid,tbody,tr,th,td,p,prop,x,c,node;
   args = s2_disectid(id);
   tabid = args.t;
   p = args.p;
   prop = s2_nbntaxa_colnamefromjsonprop(p);
   
   tab = document.getElementById(tabid);
   s2_tabremcols(tab,[prop]);
   delete nbn_tcols[tabid][p];
   hidedialog();
}
function s2_nbntaxa_addascolumn(id) {
   var args,tvk,taxa,tab,tabid,tbody,tr,th,td,p,prop,x,c,node;
   args = s2_disectid(id);
   tabid = args.t;
   p = args.p;
   prop = s2_nbntaxa_colnamefromjsonprop(p);
      
   tab = document.getElementById(tabid);
   tvkcols = s2_nbntaxa_gettvkcols(tab);
   hidedialog();
   for(x in tvkcols) {
      c = tvkcols[x];
      s2_tabaddcols(tab,(c+1),[prop]);
      // populate column
      tbody = tab.tBodies[0];
      for(rx=0;rx<tbody.rows.length;rx++) {
         tr = tbody.rows[rx];
         // get link alt from tvk col
         tvk = tr.cells[c].firstChild.alt;
         taxa = JSON.parse(taxacache[tvk]);
         node = null;
         if (taxa[p]) node = s2_objecttonode(taxa[p]);
         if (node) tr.cells[c+1].appendChild(node);
         
      }
   } 
   if (!nbn_tcols[tabid]) nbn_tcols[tabid] = [];
   nbn_tcols[tabid][p] = prop;     
}
function s2_renamecol(th,name) {
   th.replaceChild(document.createTextNode(name),th.firstChild);
}
function s2_tabremcols(tab,cols) {
   var tbody,tr,th,td,cx,rx,col,clx,cel,remcols;
   tr = tab.tHead.rows[0];
   remcols = [];
   for(cx in cols) {
      col = cols[cx];
      for(clx=tr.cells.length-1;clx>=0;clx--) {
         cel = tr.cells[clx];
         if (cel.firstChild.nodeValue == col) {
            remcols.push(clx);
            tr.removeChild(cel);
         }
      } 
   } 
   tbody = tab.tBodies[0];
   for(rx=0;rx<tbody.rows.length;rx++) {
      tr = tbody.rows[rx];
      for(clx=tr.cells.length-1;clx>=0;clx--) {
         cel = tr.cells[clx];
         if (remcols.indexOf(clx)>=0) {
            tr.removeChild(cel);
         }
      }
   }  
}
function s2_tabaddcols(tab,beforeindex,cols) {
   var tbody,tr,th,td,cx,rx,col;
   tr = tab.tHead.rows[0];
   for(cx in cols) {
      col = cols[cx];
      th = document.createElement('TH');
      th.appendChild(document.createTextNode(col));
      if ((beforeindex) >= tr.childNodes.length) tr.appendChild(th); 
      else tr.insertBefore(th,tr.childNodes[beforeindex+1]); 
   }
   tbody = tab.tBodies[0];
   for(rx=0;rx<tbody.rows.length;rx++) {
      tr = tbody.rows[rx];
      for(cx in cols) {
         td = document.createElement('TD');
         if ((beforeindex) >= tr.childNodes.length) tr.appendChild(td); 
         else tr.insertBefore(td,tr.childNodes[beforeindex+1]);
      }
   }   
}

// move to other file
function s2_livedialog() {
   return (document.getElementById('dialog-container').style.display == 'block');
}  



/* 
   NBN REST API search dialog
*/ 
var taxasearchcache = [];
function s2_nbntaxa_search(term) {
   var url;
   if (!taxasearchcache[term] || taxasearchcache[term] == 'null') { 
      url = nbnapiurl+'/search/taxa?q='+term;
      s2_nbntaxa_send(url,s2_nbntaxa_loadsearchjson,term);
   } else {
      s2_nbntaxa_loadsearchjson(taxasearchcache[tvk]);
   }            
}
function s2_nbntaxa_loadsearchjson(json) {
   var taxa,tvk,ctvk,nx,cx,cid,cel;
   results = JSON.parse(json);
   if (!taxasearchcache[term] || taxasearchcache[term] == 'null') taxasearchcache[term] = json;
   // if the commmon name is not populated get the synonyms straight away
   
   //if (!taxa.commonName) s2_nbntaxa_getsynonyms(tvk);
   //if (!taxa.designations) s2_nbntaxa_getdesignations(tvk);
   
   
}
function s2_nbntaxa_loadsearchtaxa(cel,taxa) {

}
