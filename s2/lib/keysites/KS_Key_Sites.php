<?php
/*
$paths = array(
   '../',
   '../../',
   '../../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
*/
include_once('settings.conf');
include_once('snc/S2_DBT.php');

class KS_Project extends DBT {
   protected $tablename = 'KS_Project';
   protected $displayname = 'Key Site Project';
   protected $show = array('KS_Key_Site','KS_Meeting');
   protected $view = '[
      {
         "Name":"Sites",
         "Data": [
            {
               "Table":"KS_Key_Site"
            }
         ]
      },
      {
         "Name":"Meetings",
         "Data":[
            {
               "Name":"Next Meeting",
               "Table":"KS_Meeting",
               "Show":"Next"
            },
            {
               "Name":"Previous Meeting",
               "Table":"KS_Meeting",
               "Show":"Previous"
            },
            {
               "Name":"Meeting History",
               "Table":"KS_Meeting",
               "Show":"All"
            }
         ]
      }
   ]'; 
   protected $columns = array(
      'KS_Project_ID'   => 'Unique Key',
      'Name'            => 'Short Text',
      'Established'     => 'Date'
   );
   protected $domain = 'keysites';
   protected $instancedomain = true;
   protected $instanceof = 'KS_Project';
   protected $permissions = array(
      'Def'    => 'System Administrator'
   );
   protected $defaultpermissions = array(
'USR_Default_Permissions' => 
'"Domain","Label","Description","Value","Anon" 	
"keysites","System Administrator","Add and edit projects",0,0
"keysites","Key Sites Administrator","Add and edit key site data",0,0
"keysites","Key Sites Member","View key site data",0,0
"keysites","Registered User","View unprotected data",1,0',
'USR_Permissions' => 
'"User_Id","Domain","Label","Description","Value"
1,"keysites","Registered User",NULL,1
1,"keysites","Key Sites Member",NULL,1
1,"keysites","Key Sites Administrator",NULL,1
1,"keysites","System Administrator",NULL,1'   
   );  
}

class KS_Key_Site extends DBT {
   protected $tablename = 'KS_Key_Site';
   protected $displayname = 'Key Site';
   protected $perpage = 3;
   protected $show = array('KS_Site_Boundary','KS_Site_Visit','KS_Site_Description','KS_Site_Broad_Habitat','KS_Site_Land_Owner');
   protected $view = '[
      {
         "Name":"Overview",
         "Data":[
            {
               "Name":"Current Description",
               "Table":"KS_Site_Description",
               "Show":"Current"
            },
            {
               "Name":"Current Boundary",
               "Table":"KS_Site_Boundary",
               "Show":"Current"
            }            
         ]        
      },
      {
         "Name":"Descriptions",
         "Data":[
            {
               "Table":"KS_Site_Description"
            }
         ]
      },
      {
         "Name":"Boundaries",
         "Data":[
            {
               "Name":"Boundary History",
               "Table":"KS_Site_Boundary",
               "Show":"All"               
            }
         ]
      },
      {
         "Name":"Ownership",
         "Data":[
            {
               "Table":"KS_Site_Land_Owner"
            }
         ]
      },
      {
         "Name":"Key Species",
         "Data":[
            {
               "Table":"KS_Site_Species"
            }
         ]
      },
      {
         "Name":"Habitats",
         "Data":[
            {
               "Table":"KS_Site_Broad_Habitat"
            }
         ]
      }
   ]'; 
   protected $columns = array(
      'KS_Key_Site_ID'  => 'Unique Key',
      'KS_Project_ID'   => '{"DataType":"LINKEDTO","TargetType":"KS_Project","TargetField":"KS_Project_ID","Current":1}',
      'Name'            => '{"DataType":"Short Text","Mandatory":1}',
      'Site_Code'       => '{"DataType":"Short Text","Mandatory":1}'
   );
   protected $domain = 'keysites';
   protected $instancedomain = true;
   protected $instanceof = 'KS_Authority';
   protected $permissions = array(
      'Def'    => 'Key Sites Administrator',
      'List'   => 'Key Sites Member',
      'View'   => 'Key Sites Member'
   );
   function getname() {
      return  $this->data['Site_Code'] . " : " . $this->data['Name'];
   }
   function getshortname() {
      return  $this->data['Site_Code'];
   }
}

class KS_Meeting extends DBT {
   protected $tablename = 'KS_Meeting';
   protected $displayname = 'Meeting';
   protected $columns = array(
      'KS_Meeting_ID'      => 'Unique Key',
      'KS_Project_ID'      => '{"DataType":"LINKEDTO","TargetType":"KS_Project","TargetField":"KS_Project_ID","Current":1}',
      'Name'               => '{"DataType":"Short Text","Mandatory":1}',
      'Number'             => 'Count',
      'On_Date'            => '{"DataType":"Date","Mandatory":1}',
      'Agenda'             => '{"DataType":"LINKEDTO","TargetType":"KS_Document","TargetField":"KS_Document_ID"}',    
      'Minutes'            => '{"DataType":"LINKEDTO","TargetType":"KS_Document","TargetField":"KS_Document_ID"}',    
   );
   protected $domain = 'keysites';
   protected $instancedomain = true;
   protected $instanceof = 'KS_Project';
   protected $permissions = array(
      'Def'    => 'Key Sites Administrator',
      'List'   => 'Key Sites Member',
      'View'   => 'Key Sites Member'
   );
   function getname() {
      $name = $this->data['Name'];
      if (isset($this->data['Number'])) $name .= " " . $this->data['Number'];
      return $name;
   }
}
class KS_Document extends DBT {
   protected $tablename = 'KS_Document';
   protected $displayname = 'Document';
   protected $show = array();
   protected $columns = array(
      'KS_Document_ID'      => 'Unique Key',
      'Name'                => '{"DataType":"Short Text","Mandatory":1}',
      'File'                => '{"DataType":"File Path","Mandatory":1}' 
   );
   protected $domain = 'keysites';
   protected $permissions = array(
      'Def'    => 'Key Sites Member'
   );
}


class KS_Site_Boundary extends DBT {
   protected $tablename = 'KS_Site_Boundary';
   protected $displayname = 'Boundary';
   protected $show = array();
   protected $columns = array(
      'KS_Site_Boundary_ID'      => 'Unique Key',
      'KS_Key_Site_ID'           => '{"DataType":"LINKEDTO","TargetType":"KS_Key_Site","TargetField":"KS_Key_Site_ID","Mandatory":1,"Current":1}',
      'Review_Date'              => 'Date',
      'Boundary'                 => 'Map Data',
      'Notes'                    => 'Long Text',
      'Version'                  => '{"DataType":"Version","VersionOfType":"KS_Key_Site","VersionOfField":"KS_Key_Site_ID"}' 
   );
   protected $domain = 'keysites';
   protected $instancedomain = true;
   protected $instanceof = 'KS_Project';
   protected $permissions = array(
      'Def'    => 'Key Sites Administrator'
   );
   function getname() {
      return "V" . $this->data['Version'] . " (" . $this->data['Review_Date'] . ")";
   }   
}
class KS_Site_Visit extends DBT {
   protected $tablename = 'KS_Site_Visit';
   protected $displayname = 'Site Visit';
   protected $show = array();
   protected $columns = array(
      'KS_Site_Visit_ID'      => 'Unique Key',
      'KS_Key_Site_ID'        => '{"DataType":"LINKEDTO","TargetType":"KS_Key_Site","TargetField":"KS_Key_Site_ID","Mandatory":1,"Current":1}',
      'KS_Visit_Type_ID'      => '{"DataType":"LINKEDTO","TargetType":"KS_Visit_Type","TargetField":"KS_Visit_Type_ID","Mandatory":1}',
      'Survey_Date'           => 'Date' 
   );
   protected $domain = 'keysites';
   protected $instancedomain = true;
   protected $instanceof = 'KS_Project';
   protected $permissions = array(
      'Def'    => 'Key Sites Administrator'
   );
   function getname() {
      return $this->data['Survey_Date'];
   }   
}
class KS_Visit_Type extends DBT {
   protected $tablename = 'KS_Visit_Type';
   protected $displayname = 'Visit Type';
   protected $show = array();
   protected $columns = array(
      'KS_Visit_Type_ID'      => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text","Mandatory":1}',
      'Description'           => 'Long Text' 
   );
   protected $domain = 'keysites';
   protected $permissions = array(
      'Def'    => 'Key Sites Administrator'
   );
   protected $primingdata = array(
      'KS_Visit_Type' => '
"Name","Description"
"Full Survey","Full ecological survey"
"Monitoring Survey","Monitoring change of habitat, condition and use."
"Informal","Notes from passing observation."'
   );   
}

class KS_Site_Description extends DBT {
   protected $tablename = 'KS_Site_Description';
   protected $displayname = 'Description';
   protected $show = array();
   protected $columns = array(
      'KS_SiteDescription_ID'    => 'Unique Key',
      'KS_Key_Site_ID'           => '{"DataType":"LINKEDTO","TargetType":"KS_Key_Site","TargetField":"KS_Key_Site_ID","Mandatory":1,"Current":1}',
      'Survey_Date'              => 'Date',
      'Site_Description'         => 'Long Text',
      'Version'                  => '{"DataType":"Version","VersionOfType":"KS_Key_Site","VersionOfField":"KS_Key_Site_ID"}' 
   );
   protected $domain = 'keysites';
   protected $instancedomain = true;
   protected $instanceof = 'KS_Project';
   protected $permissions = array(
      'Def'    => 'Key Sites Administrator'
   );
   function getname() {
      return "V" . $this->data['Version'] . " (" . $this->data['Survey_Date'] . ")";
   }   
}

class KS_Broad_Habitat extends DBT {
   protected $tablename = 'KS_Broad_Habitat';
   protected $displayname = 'Broad Habitat';
   protected $show = array();
   protected $columns = array(
      'KS_Broad_Habitat_ID'      => 'Unique Key',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Code'                     => 'Short Text',
      'Description'              => 'Long Text' 
   );
   protected $domain = 'keysites';
   protected $permissions = array(
      'Def'    => 'Key Sites Administrator'
   );
   protected $primingdata = array(
      'KS_Broad_Habitat' => '
"Name","Code"
"Broadleaved, mixed and yew woodland",1
"Coniferous woodland",2
"Boundary and linear features",3
"Arable and horticultural",4
"Improved grassland",5
"Neutral grassland",6
"Calcareous grassland",7
"Acid grassland",8
"Bracken",9
"Dwarf shrub heath",10
"Fen, marsh and swamp",11
"Bogs",12
"Standing open water and canals",13
"Rivers and streams",14
"Montane habitats",15
"Inland rock",16
"Built up areas and gardens",17
"Supralittoral rock",18
"Supralittoral sediment",19
"Littoral rock",20
"Littoral sediment",21
"Inshore sublittoral rock",22
"Inshore sublittoral sediment",23
"Offshore shelf rock",24
"Offshore shelf sediment",25
"Continental shelf slope",26
"Oceanic seas",27'
   );   
}
class KS_Site_Broad_Habitat extends DBT {
   protected $tablename = 'KS_Site_Broad_Habitat';
   protected $displayname = 'Broad Habitat';
   protected $show = array();
   protected $columns = array(
      'KS_Site_Broad_Habitat_ID'    => 'Unique Key',
      'KS_Key_Site_ID'              => '{"DataType":"LINKEDTO","TargetType":"KS_Key_Site","TargetField":"KS_Key_Site_ID","Mandatory":1,"Current":1}',
      'KS_Broad_Habitat_ID'         => '{"DataType":"LINKEDTO","TargetType":"KS_Broad_Habitat","TargetField":"KS_Broad_Habitat_ID","Mandatory":1}'
   );
   protected $domain = 'keysites';
   protected $instancedomain = true;
   protected $instanceof = 'KS_Project';
   protected $permissions = array(
      'Def'    => 'Key Sites Administrator'
   );
   function getname() {
      $names = array();
      foreach($this->columns as $col => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $table = $params->TargetType;
            $tcol = $params->TargetField;
         } else if (preg_match('/([^\:]+)\:([^\.]+)\.(.+)/',$type,$matches)) {
            $type = $matches[1];
            $table = $matches[2];
            $tcol = $matches[3];
         }
         if (isset($table) && isset($tcol)) {
            $link = null;
            eval("\$link = new $table();");
            $id = (is_object($this->data[$col]))
                  ?$this->data[$col]->Id
                  :$this->data[$col];
            $link->loaddata($id);
            $names[] = $link->getname();
         }  
      }
      return join(':',$names);   
   }   
}
class KS_Site_Land_Owner extends DBT {
   protected $tablename = 'KS_Site_Land_Owner';
   protected $displayname = 'Land Owner';
   protected $show = array();
   protected $columns = array(
      'KS_Site_Land_Owner_ID' => 'Unique Key',
      'KS_Key_Site_ID'        => '{"DataType":"LINKEDTO","TargetType":"KS_Key_Site","TargetField":"KS_Key_Site_ID","Mandatory":1,"Current":1}',
      'Contact'               => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"],"Mandatory":1}',
      'From_Date'             => 'Date',
      'Until_Date'            => 'Date'               
   );
   protected $domain = 'keysites';
   protected $instancedomain = true;
   protected $instanceof = 'KS_Project';
   protected $permissions = array(
      'Def'    => 'Key Sites Administrator'
   );
   function getname() {
      return $this->data['Contact']->Name;
   }   
}
class KS_Site_Species extends DBT {
   protected $tablename = 'KS_Site_Species';
   protected $displayname = 'Key Species';
   protected $show = array();
   protected $columns = array(
      'KS_Site_Species_ID'    => 'Unique Key',
      'KS_Key_Site_ID'        => '{"DataType":"LINKEDTO","TargetType":"KS_Key_Site","TargetField":"KS_Key_Site_ID","Mandatory":1,"Current":1}',
      'Species'               => 'Taxa' 
   );
   protected $domain = 'projects';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );
   function getname() {
      return $this->data['Species']->Name;
   }   
}

?>