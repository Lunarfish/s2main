<?php
include_once('settings.conf');
include_once('snc/S2_DBT.php');

class YH_Application extends DBT {
   protected $tablename = 'YH_Application';
   protected $displayname = 'Application';
   protected $columns = array(
      'YH_Application_ID'  => 'Unique Key',
      'Name'               => 'Short Text',
      'Established'        => 'Date'
   );
   protected $domain = 'applications';
   protected $instancedomain = true;
   protected $instanceof = 'YH_Application';
   protected $permissions = array(
      'Def'    => 'System Administrator'
   );
   protected $defaultpermissions = array(
'USR_Default_Permissions' => 
'"Domain","Label","Description","Value","Anon" 	
"applications","System Administrator","Add and edit applications",0,0
"applications","Application Administrator","Add and edit application data",0,0
"applications","Application Member","View application data",0,0
"applications","Registered User","View unprotected data",1,0',
'USR_Permissions' => 
'"User_Id","Domain","Label","Description","Value"
1,"applications","Registered User",NULL,1
1,"applications","Application Member",NULL,1
1,"applications","Application Administrator",NULL,1
1,"applications","System Administrator",NULL,1'   
   );  
}

?>