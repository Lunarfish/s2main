<?php

include_once('settings.conf');
include_once('snc/S2_DBT.php');
include_once('gen/G_Species.php');

class ODS_Search_Profile extends DBT {
   protected $tablename = 'ODS_Search_Profile';
   protected $displayname = 'Search Profile';
   protected $perpage = 10;
   protected $show = array('ODS_Search_Profile_Buffer','ODS_Species_Profile_Section','ODS_Search');
   protected $view = '[
      {
         "Name":"Definition",
         "Data": [
            {
               "Name":"Buffers",
               "Table":"ODS_Search_Profile_Buffer"
            },
            {
               "Name":"Species Sections",
               "Table":"ODS_Species_Profile_Section"
            }
         ]
      },
      {
         "Name":"Searches Using This Profile",
         "Data": [
            {
               "Name":"Searches",
               "Table":"ODS_Search"
            }
         ] 
      }
   ]';
   protected $columns = array(
      'ODS_Search_Profile_ID' => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text","Mandatory":1}',
      'Abbreviation'          => '{"DataType":"Short Text"}',
      'Description'           => '{"DataType":"Long Text"}',
      
   );
   protected $domain = 'ods';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   protected $defaultpermissions = array(
      'USR_Permissions' => '
"User_Id","Domain","Label","Value"
1,"ods","Search Full Resolution",1
1,"ods","Search Sensitive Access",1
1,"ods","Search Run",1
1,"ods","Search Administrator",1',
      'USR_Default_Permissions' => '
"Domain","Label","Description","Value","Anon" 
"ods","Search Administrator","Administer search data",0,0
"ods","Search Full Resolution","View original search data",0,0
"ods","Search Sensitive Access","View original search data",0,0
"ods","Search Run","Run searches based on existing profiles",0,0'   
   );
   function getshortname() {
      return ($this->data['Abbreviation'] != null)
         ?$this->data['Abbreviation']
         :$this->data['Name'];
   }
}
class ODS_Search_Definition extends DBT {
   protected $tablename = 'ODS_Search_Definition';
   protected $displayname = 'Search Definition';
   protected $perpage = 10;
   protected $show = array('ODS_Search_Profile_Buffer','ODS_Species_Profile_Section','ODS_Search');
   protected $view = '[
      {
         "Name":"Parameters",
         "Data": [
            {
               "Name":"Buffers",
               "Table":"ODS_Search_Profile_Buffer"
            },
            {
               "Name":"Species Sections",
               "Table":"ODS_Species_Profile_Section"
            }
         ]
      },
      {
         "Name":"Searches Using This Profile",
         "Data": [
            {
               "Name":"Searches",
               "Table":"ODS_Search"
            }
         ] 
      }
   ]';
   protected $columns = array(
      'ODS_Search_Definition_ID' => 'Unique Key',
      'ODS_Search_Profile_ID'    => 'Unique Key',
      'Applies_From'             => 'Date',
      'Applies_Until'            => 'Date',
      'Version'                  => '{"DataType":"Version","VersionOfType":"ODS_Search_Profile","VersionOfField":"ODS_Search_Profile_ID"}'
   );
   protected $domain = 'ods';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Search_Profile_ID','Version'));   
   }
}

class ODS_Search_Profile_Buffer extends DBT {
   protected $tablename = 'ODS_Search_Profile_Buffer';
   protected $displayname = 'Profile Buffer';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Search_Profile_Buffer_ID'   => 'Unique Key',
      'ODS_Search_Profile_ID'          => '{"DataType":"LINKEDTO","TargetType":"ODS_Search_Profile","TargetField":"ODS_Search_Profile_ID","Current":1,"Inherit":1}',
      'Buffer_Size'                    => '{"DataType":"Number"}',
      'Number'                         => '{"DataType":"Count","CountOfType":"ODS_Search_Profile","CountOfField":"ODS_Search_Profile_ID"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('Buffer_Size'));   
   }
}
class ODS_Species_Profile_Section extends DBT {
   protected $tablename = 'ODS_Species_Profile_Section';
   protected $displayname = 'Profile Species Section';
   protected $domain = 'ods';
   protected $show = array('ODS_Species_Filter','ODS_Species_Output');
   protected $columns = array(
      'ODS_Species_Profile_Section_ID' => 'Unique Key',
      'ODS_Search_Profile_ID'          => '{"DataType":"LINKEDTO","TargetType":"ODS_Search_Profile","TargetField":"ODS_Search_Profile_ID","Current":1,"Inherit":1}',
      'Name'                           => '{"DataType":"Short Text"}',
      'Buffer'                         => '{"DataType":"LINKEDTO","TargetType":"ODS_Search_Profile_Buffer","TargetField":"ODS_Search_Profile_Buffer_ID","Inherit":1}',
      'Return'                         => '{"DataType":"Short Text","Options":["All Species","Only Filter Species"]}',
      'Number'                         => '{"DataType":"Count","CountOfType":"ODS_Search_Profile","CountOfField":"ODS_Search_Profile_ID"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('Name','Buffer'));   
   }
}

class ODS_Species_Filter extends DBT {
   protected $tablename = 'ODS_Species_Filter';
   protected $displayname = 'Species Data Filter';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Species_Filter_ID'          => 'Unique Key',
      'ODS_Species_Profile_Section_ID' => '{"DataType":"LINKEDTO","TargetType":"ODS_Species_Profile_Section","TargetField":"ODS_Species_Profile_Section_ID","Current":1,"Inherit":1}',
      'List'                           => '{"DataType":"INTERFACE","Domain":"species","ValidTypes":["G_Species_List"],"Mandatory":1}',
      'Number'                         => '{"DataType":"Count","CountOfType":"ODS_Species_Profile_Section","CountOfField":"ODS_Species_Profile_Section_ID"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('List','Number'));   
   }
   function getlist($list) {
      global $client,$folder;
      $url = "http://${client}/${folder}/species/S2_query.php";
      $uinfo = $_REQUEST['UInfo'];
      $params     = array(
         "Action"       => "View",
         "IType"        => 'G_Species_List',
         "Current"      => $list['Value'],
         "ViewName"     => "List Members",
         "NoPaging"     => true,
         "UInfo"        => $uinfo
      );
      $res = json_decode(post($url,$params));
      unset($res->Interface);
      unset($res->IsAllowedTo);
      unset($res->Alternatives);
      unset($res->Views);
      unset($res->ViewName);
      unset($res->UserPermissions);
      return $res;      
   }
}
class ODS_Species_Output extends DBT {
   protected $tablename = 'ODS_Species_Output';
   protected $displayname = 'Species Data Output';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Species_Output_ID'          => 'Unique Key',
      'ODS_Species_Profile_Section_ID' => '{"DataType":"LINKEDTO","TargetType":"ODS_Species_Profile_Section","TargetField":"ODS_Species_Profile_Section_ID","Current":1,"Inherit":1}',
      'ODS_Species_Output_Type_ID'     => '{"DataType":"LINKEDTO","TargetType":"ODS_Species_Output_Type","TargetField":"ODS_Species_Output_Type_ID"}',
      'Highlight_List_Members'         => '{"DataType":"True or False"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Species_Profile_Section_ID','ODS_Species_Output_Type_ID'));   
   }
}
class ODS_Species_Output_Type extends DBT {
   protected $tablename = 'ODS_Species_Output_Type';
   protected $displayname = 'Species Data Output Type';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Species_Output_Type_ID'     => 'Unique Key',
      'Name'                           => '{"DataType":"Short Text","Mandatory":1}',
      'Description'                    => '{"DataType":"Long Text"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   protected $primingdata = array(
      'ODS_Species_Output_Type' => '
"Name","Description"
"Summary","Brief analysis of returned data."
"Species List","Unique list of species showing first and last year recorded and number of records."
"Record List","Full list of returned records showing the grid references and recorders."'
   );
}

class ODS_Search extends DBT {
   protected $tablename = 'ODS_Search';
   protected $displayname = 'Data Search';
   protected $show = array('ODS_Search_Buffer','ODS_Search_Output');
   protected $view = '[
      {
         "Name":"Buffers",
         "Data": [
            {
               "Table":"ODS_Search_Buffer"
            }
         ]
      },
      {
         "Name":"Results",
         "Data": [
            {
               "Name":"Reports",
               "Table":"ODS_Search_Output",
               "Show":"Links"
            }
         ]
      }
   ]';
   protected $postprocess = '[
      {
         "Name":"Create Search"         
      },
      {
         "Name":"Create Buffers",
         "Method":"createbuffers"   
      },
      {
         "Name":"Retrieve Species Data",
         "Method":"retrievespeciesdata"         
      },
      {
         "Name":"Process Search Response",
         "Method":"processspeciesdata"   
      },
      {
         "Name":"Filter Species Data",
         "Method":"filterspeciesdata"
      },
      {
         "Name":"Produce Species Report",
         "Method":"producespeciesreport"
      }      
   ]';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Search_ID'         => 'Unique Key',
      'ODS_Search_Profile_ID' => '{"DataType":"LINKEDTO","TargetType":"ODS_Search_Profile","TargetField":"ODS_Search_Profile_ID","Current":1,"Inherit":1}',
      'Submitted'             => '{"DataType":"Date","NoList":1}',
      'Data_From'             => '{"DataType":"Date","NoList":1}',
      'Data_To'               => '{"DataType":"Date","NoList":1}',
      'Boundary'              => '{"DataType":"Map Data"}',
      'Number'                => '{"DataType":"Count","CountOfType":"ODS_Search_Profile","CountOfField":"ODS_Search_Profile_ID"}',
      'Source'                => '{"DataType":"Short Text","NoList":1,"Options":["NBN Gateway","WFS Server"]}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Search_Profile_ID','Submitted','Number'));   
   }
   function createbuffers() {
      global $epsg_native;
      $ospc  = "ODS_Search_Profile";
      $ospbc = "ODS_Search_Profile_Buffer";
      $osbc  = "ODS_Search_Buffer"; 
      $poly = $this->data['Boundary'];
      $success = true;
      if (isset($poly)) {
         $osp = new $ospc();
         $ospb = new $ospbc();
         $spc_col = "${ospc}_ID";
         $spid = $this->data[$spc_col]; 
         $osp->loaddata($spid);
         $ospb = new $ospbc();
         $pbuffers = $ospb->select($spc_col,$spid); 
         foreach($pbuffers as $bufnum => $buf) {
            $bsize = $buf['Buffer_Size'];
            $snum = $buf['Number'];
            if ($bsize > 0) {
               $expr = new Zend_Db_Expr("st_astext(st_transform(st_buffer(st_transform(st_geomfromtext('${poly}',4326),${epsg_native}),${bsize}),4326))");
               $sel = "SELECT $expr AS buffer";
               //$buffer = $this->dbhandle->fetchOne($sel);
               $res = pg_query($this->dbpghandle,$sel);
               $res = pg_fetch_array($res);
               $buffer = $res['buffer'];
            } else $buffer = $poly;
            $osb = new $osbc();
            $data = array(
               'ODS_Search_ID'                  => $this->getid(),
               'Buffer_Size'                    => $bsize,
               'Buffer'                         => $buffer,
               'Number'                         => $snum   
            );
            $osb->setdata($data);
            $osb->insert();             
         }
      }
      // FIGURE OUT ERROR HANDLING
      return 1;
   }
   function retrievespeciesdata() {
      global $configsettings,$client,$folder;
      // before retrieving the raw data 
      $rdo = new ODS_Raw_Data();
      $rdo->delete($this->getpk(),$this->getid());
      $server = "localhost/r6";
      $client = "${client}/${folder}";
      $target = "/species2json.php";
      $session = $this->getsid();
      $baseurl = "http://${server}${target}";
      
      $source     = $this->data['Source'];
      $data_from  = $this->data['Data_From'];
      $data_to    = $this->data['Data_To'];
      
      $k = new KLib($session);
      $keys = json_decode($k->getkeys());
      $creds = $configsettings[$source]; 
      
      $osbc = "ODS_Search_Buffer";
      $osb = new $osbc();
      
      $buffers   = $osb->select($this->getpk(),$this->getid());
      
      // Start with un-buffered search area
      $boundary = $this->data['Boundary'];
      $number = 0;
      $buffer = 0;
      // Set boundary to largest buffer
      foreach($buffers as $bufnum => $buf) {
         $bufid      = $buf[$osb->getpk()];
         $bsize      = $buf['Buffer_Size'];
         if ($bsize > $buffer) {
            $buffer = $bsize;
            $boundary   = $buf['Buffer'];
            $number     = $buf['Number'];
         }
      }
      
      $params     = array(
         "Action"       => "GetSpeciesList",
         "Source"       => $source,
         "Boundary"     => $boundary,
         "Start_Date"   => $data_from,
         "End_Date"     => $data_to,
         "Sections"     => '[{"Name":"Records","Output_Type":"Records"}]'
      );
      foreach($creds as $i => $p) $params["CR".($i+1)] = $p;
      
      $req = new stdClass();
      $req->target = $baseurl;
      $req->request = $params;
      $req->svr = $server;
      $req->sid = $session;                                             
      $er = new stdClass();
      $er->domain = $client;
      $er->session = $session;
      $er->etype = 'AB';            
      $er->request = encrypt(json_encode($req),$keys->k1,$keys->k2);
      $jurl = "http://${client}/jproxy.php";
      $jr = new stdClass();
      $jr->target = "http://${server}/eproxy.php";
      $jr->request = json_encode($er);
      $res = post($jurl,$jr);
      $cres = decrypt($res,$keys->k1,$keys->k2);
      if (isset($cres)) {
         chdir('..');
         $sid = $this->getid();
         $f = new S2_File();
         if (!$f->exists()) $f->create();
         $d = new stdClass();
         $d->Name = "ODS_Search_${sid}_SRD.json";
         $d->Type = "text/json"; 
         $d->Data = $cres;
         $d = s2_savefile($d);
         $f->setdata((array)$d);
         $fid = $f->insert();
         $url = 'http://'.$_SERVER['SERVER_NAME']."/${folder}/echofile.php?a=echo&i=$fid";
//print $url;exit;
         $d->Link = $url;
         $f->setdata(array('Link'=>$url));
         $f->update($f->getpk(),$fid);
         
         $rdo = new ODS_Raw_Data();
         if (!$rdo->exists()) $rdo->create();
         $d = array(
            $this->getpk() => $this->getid(),
            $f->getpk()    => $fid
         );
         $rdo->setdata($d);
         $rdid = $rdo->insert();
//print_r($rdo->getdata());exit;
         return 1;                              
      } else return 0;              
   }
   function processspeciesdata() {
      global $epsg_native;
      chdir('..');
      $rdo = new ODS_Raw_Data();
      $rdo->loaddata($this->getid(),$this->getpk());
//print_r($rdo->getdata());exit;      
      $rdata = $rdo->getdata();
      $fid = $rdata['S2_File_ID'];
      $fid = (is_object($fid))?$fid->Id:$fid;
//print "FILE: $fid"; 
      $f = new S2_File();
      $f->loaddata($fid);
      $fdata = $f->getdata();
//print_r($fdata);exit;
      $path = $fdata['Data'];
      $cres = s2_getfile($path);
      $jres = json_decode($cres);
      $srco = new ODS_Search_Record();
      if (!$srco->exists()) $srco->create();
      if (isset($jres->Tables) && isset($jres->Tables->Records)) {
         $records = count($jres->Tables->Records);
         $ro = null;
         foreach ($jres->Tables->Records as $recnum => $record) {
            //print "<pre>"; print_r($record); print "</pre>";
            $name = $record->scientificname;
            $tvk = $record->gatewaykey;
            $jo = new stdClass();
            $jo->Name = $name;
            $jo->Value = $tvk;
            $tid = json_encode($jo);
            $wkt = null;
            $ngr = new NationalGridReference();
            $ngr->fromString($record->spatialref);
            $gr = conv_ngr_to_ings($ngr);
            $bb = new BoundingBox(
               ($gr->northing + $gr->accuracy),
               ($gr->easting + $gr->accuracy),
               $gr->northing,
               $gr->easting
            );
            $oswkt = $bb->asWKT();
            $expr = new Zend_Db_Expr("st_astext(st_transform(st_geomfromtext('${oswkt}',${epsg_native}),4326))");
            $sel = "SELECT $expr AS buffer";
            $wkt = $this->dbhandle->fetchOne($sel);
                                                          
            $ro = array(
               'ODS_Search_ID'         => $this->getid(),
               'G_Species_ID'          => $tid,
               'Place_Name'            => $record->location,
               'Grid_Reference'        => $record->spatialref,
               'Boundary'              => $wkt,
               'Recorder'              => $record->recordedby,
               'Start_Date'            => $record->sdate,
               'End_Date'              => $record->edate,
               'Comments'              => ((isset($record->comments))?$record->comments:null),
               'Details'               => ((isset($record->measurement))?"$record->measurement $record->measure":null),
               'Source'                => $record->custodian,
               'Survey'                => urlencode($record->survey)
            );
            $srco->setdata($ro);
            $srco->insert();         
         }
      }
      $f->delete($f->getpk(),$fid);
      $rdo->delete($this->getpk(),$this->getid());
      return 1;
   }
   function filterspeciesdata() {
//error_reporting(E_ALL);
      $bc  = "ODS_Search_Buffer";
      $pc  = "ODS_Search_Profile";
      $psc = "ODS_Species_Profile_Section";
      $pbc = "ODS_Search_Profile_Buffer";
      $fc  = "ODS_Species_Filter";
      $oc  = "ODS_Species_Output";
      $otc = "ODS_Species_Output_Type";
      $rc  = "ODS_Search_Record";
      $b   = new $bc();
      $ps  = new $psc();
      $pb  = new $pbc();
      $f   = new $fc();
      $o   = new $oc();
      $ot  = new $otc();
      $r   = new $rc();
      $plnk = $this->getlinkedcolumn($pc);
      $sections = $ps->select($plnk[1],$this->data[$plnk[0]]);
      $blnk = $b->getlinkedcolumn($this->tablename);
      $buffers = $b->select($blnk[0],$this->data[$blnk[1]]);
      foreach($sections as $secnum => $psd) {
         $psid   = $psd[$ps->getpk()];
         $return = $psd['Return'];
         $blnk = $ps->getlinkedcolumn($pbc);
         $pbid = $psd[$blnk[0]]->Id; 
         $pb->loaddata($pbid);
         $pbd = $pb->getdata();
         $bsize = $pbd['Buffer_Size'];
         $boundary = $this->data['Boundary'];
         $islargest = true;
         foreach($buffers as $bx) {
            if ($bx['Buffer_Size'] == $bsize) {
               $bd = $bx;
               $boundary = $bd['Buffer'];
            } else if ($bx['Buffer_Size'] > $bsize) $islargest = false;              
         }
         $srecords = $r->select($this->getpk(),$this->getid());
         $orecords = array();
//print "<pre>"; print_r($srecords); print "</pre>";exit;
         if (count($srecords)>0) {
            if (!$islargest) {
               foreach($srecords as $rid => $rec) {
                  $poly = $rec['Boundary'];
                  $expr = new Zend_Db_Expr("st_intersects(st_geomfromtext('$poly',4326),st_geomfromtext('$boundary',4326))");
                  //$expr = $expr->__toString();
                  $sel = "SELECT $expr AS isin";
                  //$intersects = $this->dbhandle->fetchOne($sel);
                  $res = pg_query($this->dbpghandle,$sel);
                  $res = pg_fetch_array($res);
                  // pg returns t or f as strings rather than 1 or 0;
                  $intersects = ($res['isin'] == 't');
                  if ($intersects) $orecords[] = $rec;            
               }
            } else $orecords = $srecords;
         }
         if (count($orecords) > 0) {
            $sectionlists = array();
            
            $flnk = $f->getlinkedcolumn($psc);
            $filters = $f->select($flnk[0],$psid);
            foreach($filters as $fi => $filter) {
               $list = $filter['List'];
               $listname = $list['Name'];
               $lst = $f->getlist($list);
               $lmem = $lst->LinkedData[0];
               $lmembers = $lmem->ListData;
               $ldata = $lst->Data;
               $lst = new stdClass();
               $lst->Cache = $list;
               $lst->Data = $ldata;
               $lst->Members = $lmembers;
               $gcids = array();
               foreach($lmembers as $lx => $lmember) {
//print_r($lmember);
                  $gcid = $lmember->G_Species_ID->Id;
                  if (!in_array($gcid,$gcids)) $gcids[] = $gcid; 
               }
               $lst->IDs = $gcids;
//print_r($lst->IDs);
               $sectionlists[$listname] = $lst;
            } 
//print_r($sectionlists[$listname]);           
            $species = array();
            $speciesrcount = array();
            $ismemberof = array();
            $firstyears = array();
            $lastyears = array();
            $lists = array();
            $listspecies = array();
            $listscount = array();
            $listrcount = array();
            $listscores = array();
            $rle = new ODS_Record_List_Entry();
            if (!$rle->exists())$rle->create();
//print_r($orecords);exit;
            foreach($orecords as $rid => $rec) {
               $rid = $rec[$r->getpk()];
               $gcid = intval($rec['G_Species_ID']->Id);
               
               // update record count for species
               if (!isset($species[$gcid])) {
                  $species[$gcid] = $rec['G_Species_ID'];
                  $speciesrcount[$gcid] = 1;
               } else $speciesrcount[$gcid]++;     
//print_r($species);exit;                         
               // update recorded date range for species
               $year = intval(substr($rec['Start_Date'],-4));
               if (!isset($firstyears[$gcid])) $firstyears[$gcid] = $year;
               else $firstyears[$gcid] = min($year,$firstyears[$gcid]); 
               if (!isset($lastyears[$gcid])) $lastyears[$gcid] = $year;
               else $lastyears[$gcid] = max($year,$lastyears[$gcid]); 
               
               // update list membership data
               foreach($sectionlists as $listname => $lst) {
                  $gcids  = $lst->IDs;
                  if (in_array($gcid,$gcids)) {
                     $ldata  = $lst->Data;
                     $labbr  = $ldata->Abbreviation;
                     $lcache = $lst->Cache;
                     $lists[$listname] = $lst->Cache; 
                     // initialise ismemberof array for species if it doesn't exist
                     if (!isset($ismemberof[$gcid])) $ismemberof[$gcid] = array();
                     if (!in_array($labbr,$ismemberof[$gcid])) $ismemberof[$gcid][] = $labbr;
                     // initialise list counting if first list member
                     if (!isset($listspecies[$listname])) {
                        $listspecies[$listname] = array();
                        $listscount[$listname] = 0;
                        $listrcount[$listname] = 0;
                        $listscores[$listname] = 0;
                     }
                     // check whether species has been recorded for list already
                     if (!in_array($gcid,$listspecies)) {
                        $listspecies[$listname][] = $gcid;
                        $listscount[$listname]++;
                        $score = 1;
                        foreach($lst->Members as $mid => $lmem) {
                           if($lmem->G_Species_ID->Id == $gcid) $score = (isset($lmem->Score))?$lmem->Score:1;
                        }
                        $listscores[$listname] += $score;
                     }
                     $listrcount[$listname]++;
                  }                                                           
               }
               // add to records list depending on filter settings
               if ($return == 'All Species' || (isset($ismemberof[$gcid]) && count($ismemberof[$gcid]) > 0)) {
                  $memberof = ($return == 'All Species')?null:join(', ',$ismemberof[$gcid]);
                  $data = array(
                     'ODS_Search_ID'                  => $this->getid(),
                     'ODS_Species_Profile_Section_ID' => $psid,
                     'ODS_Search_Record_ID'           => $rid, 
                     'Member_Of'                      => $memberof 
                  );
                  $rle->setdata($data);
                  $rle->insert(); 
               }
            }
            // add summary data
            if (count($lists)>0) {
               $sse = new ODS_Species_Summary_Entry();
               if (!$sse->exists())$sse->create();
               foreach ($lists as $listname => $lc) {
                  $scount = $listscount[$listname];
                  $rcount = $listrcount[$listname];
                  $score  = $listscores[$listname];
                  $data = array(
                     'ODS_Search_ID'                  => $this->getid(),
                     'ODS_Species_Profile_Section_ID' => $psid,
                     'List'                           => $lc['G_Cache_ID'],
                     'Species_Count'                  => $scount,
                     'Record_Count'                   => $rcount,
                     'Score'                          => $score 
                  );
                  $sse->setdata($data);
                  $sse->insert();
               }
            }
            // add species list data
            $sle = new ODS_Species_List_Entry();
            if (!$sle->exists())$sle->create();
            foreach ($species as $sdata) {
               $gcid = $sdata->Id;
               if ($return == 'All Species' || (isset($ismemberof[$gcid]) && count($ismemberof[$gcid]) > 0)) {
                  $fyear  = $firstyears[$gcid];
                  $lyear  = $lastyears[$gcid];
                  $rcount = $speciesrcount[$gcid];
                  $memberof = (!isset($ismemberof[$gcid]))?null:join(', ',$ismemberof[$gcid]);
                  $data = array(
                     'ODS_Search_ID'                  => $this->getid(),
                     'ODS_Species_Profile_Section_ID' => $psid,
                     'G_Species_ID'                   => $gcid,
                     'First_Year'                     => $fyear,
                     'Last_Year'                      => $lyear,
                     'Record_Count'                   => $rcount,
                     'Member_Of'                      => $memberof    
                  );
                  $sle->setdata($data);
                  $sle->insert();
               } 
            }                          
         }                      
      }
      return 1;
   }
   function producespeciesreport() {
      global $folder;
      $pc   = "ODS_Search_Profile";
      $psc  = "ODS_Species_Profile_Section";
      $oc   = "ODS_Species_Output";
      $otc  = "ODS_Species_Output_Type";
      $rc   = "ODS_Search_Record";
      $ssec = "ODS_Species_Summary_Entry";
      $slec = "ODS_Species_List_Entry";
      $rlec = "ODS_Record_List_Entry";
      
      $sc = $this->tablename;
      $set = new stdClass();
      $sid = new stdClass();
      $sid->Current = $this->getid();
      $set->$sc = $sid; 
      
      $ps   = new $psc();
      $o    = new $oc();
      $ot   = new $otc();
      $r    = new $rc();
      $sse  = new $ssec();
      $sle  = new $slec();
      $rle  = new $rlec();
      $plnk = $this->getlinkedcolumn($pc);
      $sections = $ps->select($plnk[1],$this->data[$plnk[0]]);
      $sname = $this->getname();
      $date = date("d/m/Y",time());      
      $html = "<div id='s2rep_data-container'><h1 class='s2rep_search-name'>Search: ${sname} ~ Run:${date}</h1><hr/><div id='s2rep_data'>";
      foreach($sections as $secnum => $psd) {
         $psid    = $psd[$ps->getpk()];
         $secname = $psd['Name'];
         $psecnum = $secnum + 1;
         $html   .= "<h2>Section ${psecnum}: ${secname}</h2><div id='s2rep_sec-${secnum}' class='s2rep_section-container'>";  
         $return  = $psd['Return'];
         $olnk    = $o->getlinkedcolumn($psc);
         $otlnk   = $o->getlinkedcolumn($otc);
         $outputs = $o->select($olnk[0],$psid);
         
         foreach($outputs as $oind => $output) {
            $otype = $output[$otlnk[0]]->Name; 
            switch($otype) {
            case 'Summary': {
               $lnk  = $sse->getlinkedcolumn($psc);
               $rows = $sse->select($lnk[0],$psid,$set);
               if (count($rows)>0) $ohtm = $sse->tohtml($rows);                                       
            }break;
            case 'Species List': {
               $lnk  = $sle->getlinkedcolumn($psc);
               $rows = $sle->select($lnk[0],$psid,$set);
               if (count($rows)>0) $ohtm = $sle->tohtml($rows);
            }break;
            case 'Record List': {
               $lnk  = $rle->getlinkedcolumn($psc);
               $rows = $rle->select($lnk[0],$psid,$set);
               foreach($rows as $rleid => $rlentry) {
                  $rlnk = $rle->getlinkedcolumn($rc);
                  $rid = $rlentry[$rlnk[0]]->Id; 
                  $r->loaddata($rid,$rlnk[1]);
                  $rdata = $r->getdata();
                  $rdata['Member_Of'] = $rlentry['Member_Of'];
                  $rows[$rleid] = $rdata;
               }
               if (count($rows)>0) $ohtm = $r->tohtml($rows);
            }break;
            }
            $html .= "<h3>$otype</h3><div id='s2rep_sec-${secnum}_out-${oind}' class='s2_collapsible'>";
            if (!isset($ohtm)) $html .= "<p>No data was found to meet section requirements</p>";  
            else $html .= $ohtm;
            $html .= "</div>";                                                            
         }
         $html.= "</div>";
      }
      $html.= "</div></div>";
      
      chdir('..');
      $sid = $this->getid();
      $f = new S2_File();
      if (!$f->exists()) $f->create();
      $d = new stdClass();
      $d->Name = "ODS_Search_${sid}.html";
      $d->Type = "text/html"; 
      $d->Data = $html;
      $d = s2_savefile($d);
      $f->setdata((array)$d);
      $fid = $f->insert();
      $url = 'http://'.$_SERVER['SERVER_NAME']."/${folder}/echofile.php?a=echo&i=$fid";
      $d->Link = $url;
      $f->setdata((array)$d);
      $f->update($f->getpk(),$fid);
      
      $so = new ODS_Search_Output();
      $count = $so->getcount($this->getid());
      if (!$so->exists()) $so->create();
      $d = array(
         $this->getpk() => $this->getid(),
         $f->getpk()    => $fid,
         'Number'       => ($count+1)
      );
      $so->setdata($d);
      
      $soid = $so->insert();
      $url = 'http://'.$_SERVER['SERVER_NAME']."/${folder}/report?o=$soid";
      $d['Link'] = $url;
      $so->setdata((array)$d);
      $so->update($so->getpk(),$soid);
      $status = ($soid)?1:0;
      
      $r->delete($this->getpk(),$this->getid());
      $sse->delete($this->getpk(),$this->getid()); 
      $sle->delete($this->getpk(),$this->getid());
      $rle->delete($this->getpk(),$this->getid());
      
      return $status;
   } 
}

class ODS_Raw_Data extends DBT {
   protected $tablename = 'ODS_Raw_Data';
   protected $displayname = 'Search Raw Species Data';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Raw_Data_ID'       => 'Unique Key',
      'ODS_Search_ID'         => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1}',
      'S2_File_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_File","TargetField":"S2_File_ID","Current":0,"Inherit":0}' 
   ); 
   protected $permissions = array(
      'Def'    => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Search_ID','Run_On'));
   }
}

class ODS_Search_Record extends DBT {
   protected $tablename = 'ODS_Search_Record';
   protected $displayname = 'Search Species Record';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Search_Record_ID'  => 'Unique Key',
      'ODS_Search_ID'         => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1,"NoList":1}',
      'G_Species_ID'          => '{"DataType":"LINKEDTO","TargetType":"G_Species","TargetField":"G_Species_ID"}',
      'Place_Name'            => 'Short Text',
      'Grid_Reference'        => '{"DataType":"Short Text","Derives":[{"Column":"Boundary","Method":"s2_getgrsquare","Params":[{"Type":"Value"}]}]}',
      'Boundary'              => '{"DataType":"Map Data","NoList":1}',
      'Recorder'              => 'Short Text',
      'Start_Date'            => 'Date',
      'End_Date'              => 'Date',
      'Comments'              => 'Medium Text',
      'Details'               => 'Medium Text',
      'Source'                => 'Short Text',
      'Survey'                => 'Long Text' 
   ); 
   protected $permissions = array(
      'Def'    => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('G_Species_ID','Grid_Reference'));   
   }
   function insert() {
      $sids = array();
      if (preg_match('/^\[/',$this->data['G_Species_ID'])) {
         $svals = json_decode($this->data['G_Species_ID']);
         foreach ($svals as $x => $sx) {
            $s = new G_Species();
            $s->setdata(array('Latin_Name' => json_encode($sx)));
            $s->insert();
            $this->data['G_Species_ID'] = $s->getid();
            parent::insert();
            $sids[] = $this->getid();
         }
      } else if (preg_match('/\{/',$this->data['G_Species_ID'])) {
         $s = new G_Species();
         $s->setdata(array('Latin_Name' => $this->data['G_Species_ID']));
         $s->insert();
         $this->data['G_Species_ID'] = $s->getid();
         parent::insert();
         $sids[] = $this->getid();
      } else {
         parent::insert();
         $sids = $this->getid();
      }
      return $sids;
   }
}

class ODS_Search_Buffer extends DBT {
   protected $tablename = 'ODS_Search_Buffer';
   protected $displayname = 'Data Search Buffer';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Search_Buffer_ID'           => 'Unique Key',
      'ODS_Search_ID'                  => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1,"NoList":1}',
      'Buffer_Size'                    => '{"DataType":"Number","NoList":1,"Derives":[{"Column":"Buffer","Method":"s2_buffergeom","Params":[{"Type":"Column","IType":"ODS_Search","Value":"ODS_Search_ID","Property":"Boundary"},{"Type":"Value"},{"Type":"Static","Value":27700}]}]}',
      'Buffer'                         => '{"DataType":"Map Data"}',
      'Number'                         => '{"DataType":"Count","CountOfType":"ODS_Search","CountOfField":"ODS_Search_ID"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Search_ID','Buffer_Size'));   
   }
}

class ODS_Species_Summary_Entry extends DBT {
   protected $tablename = 'ODS_Species_Summary_Entry';
   protected $displayname = 'Species Summary Entry';
   protected $domain = 'ods';
   protected $species = array();
   protected $scoringgroups = array();
   protected $columns = array(
      'ODS_Species_Summary_Entry_ID'   => 'Unique Key',
      'ODS_Search_ID'                  => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1,"NoList":1}',
      'ODS_Species_Profile_Section_ID' => '{"DataType":"LINKEDTO","TargetType":"ODS_Species_Profile_Section","TargetField":"ODS_Species_Profile_Section_ID","Current":1,"Inherit":1,"NoList":1}',
      'List'                           => '{"DataType":"INTERFACE","Domain":"species","ValidTypes":["G_Species_List"]}',
      'Species_Count'                  => '{"DataType":"Number"}',
      'Record_Count'                   => '{"DataType":"Number"}',
      'Score'                          => '{"DataType":"Decimal"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Search_ID','ODS_Species_Profile_Section_ID','List'));   
   }
}

class ODS_Species_List_Entry extends DBT {
   protected $tablename = 'ODS_Species_List_Entry';
   protected $displayname = 'Species List Entry';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Species_List_Entry_ID'      => 'Unique Key',
      'ODS_Search_ID'                  => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1,"NoList":1}',
      'ODS_Species_Profile_Section_ID' => '{"DataType":"LINKEDTO","TargetType":"ODS_Species_Profile_Section","TargetField":"ODS_Species_Profile_Section_ID","Current":1,"Inherit":1,"NoList":1}',
      'G_Species_ID'                   => '{"DataType":"LINKEDTO","TargetType":"G_Species","TargetField":"G_Species_ID"}',
      'First_Year'                     => '{"DataType":"Number"}',
      'Last_Year'                      => '{"DataType":"Number"}',
      'Record_Count'                   => '{"DataType":"Number"}',
      'Member_Of'                      => '{"DataType":"Long Text"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Search_ID','ODS_Species_Profile_Section_ID','List'));   
   }
   function insert() {
      $sids = array();
      if (preg_match('/^\[/',$this->data['G_Species_ID'])) {
         $svals = json_decode($this->data['G_Species_ID']);
         foreach ($svals as $x => $sx) {
            $s = new G_Species();
            $s->setdata(array('Latin_Name' => json_encode($sx)));
            $s->insert();
            $this->data['G_Species_ID'] = $s->getid();
            parent::insert();
            $sids[] = $this->getid();
         }
      } else if (preg_match('/\{/',$this->data['G_Species_ID'])) {
         $s = new G_Species();
         $s->setdata(array('Latin_Name' => $this->data['G_Species_ID']));
         $s->insert();
         $this->data['G_Species_ID'] = $s->getid();
         parent::insert();
         $sids[] = $this->getid();
      } else {
         parent::insert();
         $sids = $this->getid();
      }
      return $sids;
   }
}
class ODS_Record_List_Entry extends DBT {
   protected $tablename = 'ODS_Record_List_Entry';
   protected $displayname = 'Record List Entry';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Record_List_Entry_ID'       => 'Unique Key',
      'ODS_Search_ID'                  => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1,"NoList":1}',
      'ODS_Species_Profile_Section_ID' => '{"DataType":"LINKEDTO","TargetType":"ODS_Species_Profile_Section","TargetField":"ODS_Species_Profile_Section_ID","Current":1,"Inherit":1,"NoList":1}',
      'ODS_Search_Record_ID'           => '{"DataType":"LINKEDTO","TargetType":"ODS_Search_Record","TargetField":"ODS_Search_Record_ID","Current":1,"Inherit":1}',
      'Member_Of'                      => '{"DataType":"Long Text"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Search_ID','ODS_Species_Profile_Section_ID','List'));   
   }
}

class ODS_Search_Output extends DBT {
   protected $tablename = 'ODS_Search_Output';
   protected $displayname = 'Data Search Output';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Search_Output_ID'  => 'Unique Key',
      'ODS_Search_ID'         => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1}',
      'S2_File_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_File","TargetField":"S2_File_ID","Current":0,"Inherit":0,"Show":0}',
      'Link'                  => '{"DataType":"URL"}',
      'Number'                => '{"DataType":"Count","CountOfType":"ODS_Search","CountOfField":"ODS_Search_ID"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Search_ID','Number'));   
   }
}
?>