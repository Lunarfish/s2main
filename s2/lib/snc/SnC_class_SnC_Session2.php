<?php
class SnC_Session {
   private $table = 'ADM_Session';
   private $ADM_Session_Id;
   private $ADM_User_Id;
   private $ADM_Application_Id;
   private $Started;
   private $LastAccessed;
   function SnC_Session($id=null) {
      if ($id!=null) {
         $this->ADM_Session_Id = $id;
         $this->retrieve();
      }
   }
   function retrieve() {
      $dbh = $GLOBALS['dbh'];
      $query = $dbh->select();
      $query->from($this->table,
            array("ADM_User_Id"        => "ADM_User_Id",
                  "ADM_Application_Id" => "ADM_Application_Id",
                  "Started"            => "Started",
                  "LastAccessed"       => "LastAccessed"));
      $query->where('ADM_Session_Id = ?',$this->ADM_Session_Id);
      $rows = $dbh->fetchAll($query);
      for ($i=0;$i<count($rows);$i++) {
         $record = $rows[$i];
         $this->ADM_User_Id         = $record["ADM_User_Id"];
         $this->ADM_Application_Id  = $record["ADM_Application_Id"];
         $this->Started             = $record["Started"];
         $this->LastAccessed        = $record["LastAccessed"];
      }      
   }
   function getSessionID() {
      return $this->ADM_Session_Id;      
   }
   function insert() {
      $dbh = $GLOBALS['dbh'];  
      $data = array(
         'Started'         => new Zend_Db_Expr('CURRENT_TIMESTAMP'),
         'LastAccessed'      => new Zend_Db_Expr('CURRENT_TIMESTAMP')
      );
      $status = $dbh->insert($this->table, $data);
      if ($status) $this->ADM_Session_Id = $dbh->lastInsertId();
      return $status;
   }
   function setUser($userid) {
      return $this->update($this->ADM_Session_Id,'ADM_User_Id',$userid);
   }
   function update($session,$field,$value) {
      $dbh = $GLOBALS['dbh'];
      $data = array($field => $value);
      $where = $dbh->quoteInto('ADM_Session_Id = ?',$this->ADM_Session_Id);
      $status = $dbh->update($this->table, $data, $where);
      if ($status == 0) $status = $this->insert(); // if no rows get updated you need a new session;
      else $expired = $this->expire();
      return $status;               
   }
   function expire() {
      $dbh = $GLOBALS['dbh'];
      $where = $dbh->quoteInto('LastAccessed < ?',new Zend_Db_Expr("subtime(now(),'0 1:0:0:0')"));
      return $dbh->delete($this->table, $where);
   }     
}
?>