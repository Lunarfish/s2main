<?php
ini_set('include_path',ini_get('include_path').';./lib/;');
include_once 'Zend/Db.php';
include_once('snc/SnCDatabaseConnect2.php');

class Genera {
   private $dbh;
   private $settings;
   function Genera($settings) {
      $this->dbh = SnC_getDatabaseConnection();
      $this->settings = $settings;   
   }
   function getSettingId($intable,$fromtable=null) {
      $query = $this->dbh->select();
      $query->from('ADM_Setting');
      $query->where('InTable = ?',$intable);
      if (isset($fromtable)) $query->where('FromTable = ?',$fromtable);
      else $query->where('FromTable IS NULL');
      $settings = $this->dbh->fetchAll($query);
      $id = null;
      foreach($settings as $row => $setting) $id = $setting['ADM_Setting_Id'];
      return $id; 
   }
   function getSetting($intable,$fromtable=null) {
      $query = $this->dbh->select();
      $query->from('ADM_Setting');
      $query->where('InTable = ?',$intable);
      if (isset($fromtable)) $query->where('FromTable = ?',$fromtable);
      else $query->where('FromTable IS NULL');
      $settings = $this->dbh->fetchAll($query);
      $s = null;
      foreach($settings as $row => $setting) $s = (object)$setting;
      return $s; 
   }
   function getSettingTables($settingid) {
      $query = $this->dbh->select();
      $query->from('ADM_Setting');
      $query->where('ADM_Setting_Id = ?',$settingid);
      $settings = $this->dbh->fetchAll($query);
      $s = null;
      foreach($settings as $row => $setting) {
         $s = new stdClass();
         $s->InTable = $setting['InTable'];
         $s->FromTable = $setting['FromTable'];
      }
      return $s; 
   }
   function getOptions($createdby = false) {
      $query = $this->dbh->select();
      $query->from('ADM_Table');
      //$query->where('Name = ?', $tablename);
      $tables = $this->dbh->fetchAll($query);
      $intables = array();
      $setvalues = array();
      foreach ($this->settings as $setting => $sobj) {
         if ($setting != 'Current') {
            if (is_object($sobj)) {
               $value = $sobj->Value;
            //   $prefix = (isset($sobj->Prefix))?$sobj->Prefix:'ADM_';         
            } else if (is_array($sobj)) {
               $value = $sobj['Value'];
            //   $prefix = (isset($sobj['Prefix']))?$sobj['Prefix']:'ADM_';         
            } else {
               $value = $sobj;
            //   $prefix = 'ADM_';
            }
            $query = $this->dbh->select();
            $query->from('ADM_Setting');
            $query->where('Name = ?',$setting);
            $ss = $this->dbh->fetchAll($query);
            foreach($ss as $s) $tablename = $s['InTable'];
            //$tablename = "${prefix}${setting}";
            $intables[] = $tablename;
            if (isset($value)) $setvalues[] = $tablename;            
         }
      }
      $query = $this->dbh->select();
      $query->from('ADM_TableForeignKey');
      //if (!$createdby) $query->where('LinkFromColumn <> ?','CreatedBy');
      $query->where('LinkFromTable IN (?)',$intables);
      $query->orwhere('LinkToTable IN (?)',$intables);
      // exclude CreatedBy data unless explicitly asked for.
      $query->order('LinkFromTable', 'ASC');
      $links = $this->dbh->fetchAll($query);
      $options = array();
      foreach($links as $linktable) {
         $include = ($createdby || !($linktable['LinkFromColumn'] == 'CreatedBy'));
         $columnisset = (in_array($linktable['LinkFromTable'],$setvalues) 
                        && in_array($linktable['LinkToTable'],$setvalues));  
         if ($include && !$columnisset) {
            $options[] = $linktable;
                     
         }
      }  
          
      return $options;      
   }
   function getRestrictionsForTable($table) {
      $query = $this->dbh->select();
      $query->from('DEV_DataType');
      $datatypes = $this->dbh->fetchAll($query);
      
      $query = $this->dbh->select();
      $query->from('ADM_Table');
      $query->where('Name = ?',$table);
      $adm_tables = $this->dbh->fetchAll($query);
      foreach ($adm_tables as $admt) {                              
         $query = $this->dbh->select();
         $query->from('ADM_Column');
         $query->where('ADM_Table_Id = ?',$admt['ADM_Table_Id']);
         $adm_columns = $this->dbh->fetchAll($query);
      }
   
      $query = $this->dbh->select();
      $query->from('PER_Restriction');
      $query->where('OnTable = ?',$table);
      $restrictions = $this->dbh->fetchAll($query);
      if (count($restrictions)>0) {
         foreach($restrictions as $row => $r) {
            $r = (object) $r;
            $query = $this->dbh->select();
            $qcols = array('Action');
            $query->from('PER_Restricted_Action',$qcols);
            $query->where('PER_Restriction_Id = ?',$r->PER_Restriction_Id);
            $a = $this->dbh->fetchAll($query);
            if (count($a)>0) $r->Actions = $a;
            
            $query = $this->dbh->select();
            $qcols = array('Permission');
            $query->from('PER_Requirement_Permission',$qcols);
            $query->where('PER_Restriction_Id = ?',$r->PER_Restriction_Id);
            $p = $this->dbh->fetchAll($query);
            if (count($p)>0) $r->Permissions = $p;
            
            $query = $this->dbh->select();
            $qcols = array('Setting');
            $query->from('PER_Requirement_Setting',$qcols);
            $query->where('PER_Restriction_Id = ?',$r->PER_Restriction_Id);
            $s = $this->dbh->fetchAll($query);
            if (count($s)>0) $r->Settings = $s;
            
            $query = $this->dbh->select();
            $qcols = array('SetColumn');
            $query->from('PER_Requirement_UserSupplied',$qcols);
            $query->where('PER_Restriction_Id = ?',$r->PER_Restriction_Id);
            $u = $this->dbh->fetchAll($query);
            if (count($u)>0) {
               foreach ($u as $row => $usersupplied) {
                  $u2 = new stdClass();
                  $u2->Name = $usersupplied['SetColumn'];
                  
                  foreach ($adm_columns as $column) {
                     if ($column['Name'] == $usersupplied['SetColumn']) {
                        $u2->DisplayName = $column['DisplayName'];
                        foreach($datatypes as $dt) {
                           if ($column['DEV_DataType_Id'] == $dt['DEV_DataType_Id']) {
                              $u2->DataType = $dt['Name'];
                           }
                        }
                        
                     }
                  }
                  $u[$row] = $usersupplied;
               }
               $r->UserSupplied = $u;
            }
            $restrictions[$row] = $r;
         }
      }
      return $restrictions;                     
   }
   function getAvailableMenuChoices($inccreatedby=false) {
      /*
         start with the current setting and get permissions 
         then get linked data tables and get permissions .. 
         
      */ 
      $query = $this->dbh->select();
      $query->from('DEV_ActionType');
      $functions = $this->dbh->fetchAll($query);
      
      $query = $this->dbh->select();
      $query->from('DEV_DataType');
      $datatypes = $this->dbh->fetchAll($query);
      
      $options = null;
      foreach($this->settings as $setting => $s_obj) {
         if (is_object($s_obj)&&isset($s_obj->IsCurrent)) {
            if (!is_bool($s_obj->IsCurrent)) $s_obj->IsCurrent = ($s_obj->IsCurrent == 'true')?true:false;
            if ($s_obj->IsCurrent) {
               $options = new stdClass();
               $menus = array();
               $stables = $this->getSettingTables($s_obj->SettingId);
               $table = $stables->InTable;
               $idcol = $table."_Id";
               $value = $s_obj->Value;
               $query = $this->dbh->select();
               $query->from($table);
               $query->where($idcol.' = ?',$s_obj->Value);
               $current = $this->dbh->fetchAll($query);
               if (count($current)>0) $current = $current[0];
               else $current = null;
      //$options->Current = $current;               
               $query = $this->dbh->select();
               $query->from('ADM_Table');
               $query->where('Name = ?',$table);
               $adm_tables = $this->dbh->fetchAll($query);
               foreach ($adm_tables as $admt) {                              
                  $query = $this->dbh->select();
                  $query->from('ADM_Column');
                  $query->where('ADM_Table_Id = ?',$admt['ADM_Table_Id']);
                  $adm_columns = $this->dbh->fetchAll($query);
               }
               
               $rs = $this->getRestrictionsForTable($stables->InTable);
               
               $menu = new stdClass();
               $menu->Name = $setting;
               $menuitems = array();
               
               foreach($functions as $f) {
                  if($f['InMenu']) {
                     $allowed = ($admt['IsEditable']);
                     if ($allowed) {
                        $menuitem = new stdClass();
                        $menuitem->Name = $f['DisplayName'] . " " . $setting;
                        foreach ($rs as $r) {
                           foreach ($r->Actions as $a) {
                              if ($a['Action'] == $f['DisplayName']) {
                                 // add check for permissions
                                 if (isset($r->UserSupplied)) $menuitem->UserSupplied = $r->UserSupplied;
                                 if (isset($r->Settings)) $menuitem->Settings = $r->Settings;    
                              }
                           }                           
                        }   
                     }
                     if ($allowed) $menuitems[] = $menuitem;
                  }                  
               }
               $query = $this->dbh->select();
               $query->from('ADM_TableForeignKey');
               $query->where('LinkFromTable = ?',$stables->InTable);
               if(!$inccreatedby)$query->where('LinkFromColumn <> ?','CreatedBy');
               $query->order('LinkFromTable', 'ASC');
               $links = $this->dbh->fetchAll($query);
               foreach($links as $row => $l) {
                  $s = $this->getSetting($l['LinkToTable'],$l['LinkFromTable']);
                  $query = $this->dbh->select();
                  $query->from('ADM_Table');
                  $query->where('Name = ?',$l['LinkToTable']);
                  $adm_tables = $this->dbh->fetchAll($query);
                  foreach ($adm_tables as $admt) {                              
                     $query = $this->dbh->select();
                     $query->from('ADM_Column');
                     $query->where('ADM_Table_Id = ?',$admt['ADM_Table_Id']);
                     $adm_columns = $this->dbh->fetchAll($query);
                  }
               
                  $rs = $this->getRestrictionsForTable($l['LinkToTable']);
                  $allowed = true;//($admt['IsEditable']);
                  // edit permission is on parent and add permission is on child?
                  if (isset($current['LinkFromColumn'])&&$current['LinkFromColumn']>0) {
                     $funcs = array('UnselectActionName'=>'Edit');
                  } else {
                     $funcs = array('SelectActionName'=>'Select','InsertActionName'=>'Add');                  
                  }
                  foreach($funcs as $fname => $f) {
                     $menuitem = new stdClass();
                     $menuitem->Name = $l[$fname];
                     foreach ($rs as $r) {
                        foreach ($r->Actions as $a) {
                           if ($a['Action'] == $f) {
                              // add check for permissions
                              if (isset($r->UserSupplied)) $menuitem->UserSupplied = $r->UserSupplied;
                              if (isset($r->Settings)) $menuitem->Settings = $r->Settings;
                           }
                        }                           
                     }
                     if ($allowed) $menuitems[] = $menuitem;    
                  }
                  $links[$row] = (object)$l;
                  $links[$row]->Restrictions = $rs;
               }  
               $options->Single = $links;               
               $query = $this->dbh->select();
               $query->from('ADM_TableForeignKey');
               $query->where('LinkToTable = ?',$stables->InTable);
               if(!$inccreatedby)$query->where('LinkFromColumn <> ?','CreatedBy');
               $query->order('LinkFromTable', 'ASC');
               $links = $this->dbh->fetchAll($query);
               foreach($links as $row => $l) {
                  $s = $this->getSetting($l['LinkToTable'],$l['LinkFromTable']);
                  $menuitem = new stdClass();
                  $menuitem->Name = $l['SelectActionName'];
                  $menuitems[] = $menuitem;
                  $rs = $this->getRestrictionsForTable($l['LinkFromTable']);
                  $links[$row] = (object)$l;
                  $links[$row]->Restrictions = $rs;
               }  
               $menu->MenuItems = $menuitems;               
               $menus[] = $menu;
               $options->Multiple = $links;
               $options->Menus = $menus; 
            }                        
         }  
      }
      return $options;      
   }
   /*
   function getOptions($permissions=null) {
      //*
         From the current settings and the current users
         permissions you can get a list of restrictions 
         and build a set of menu options from that.
         
         Start with settings and then build in permissions  
      //*
      $rtable  = "PER_Restriction";               
      $ratable = "PER_Restricted_Action";
      $rptable = "PER_Requirement_Permission"; 
      $rstable = "PER_Requirement_Setting";
      $rutable = "PER_Requirement_UserSupplied";                   
            
      $query = $this->dbh->select();
      $query->from('ADM_Table');
      //$query->where('Name = ?', $tablename);
      $tables = $this->dbh->fetchAll($query);
      $intables = array();
      $setvalues = array();
      foreach ($this->settings as $setting => $sobj) {
         if ($setting != 'Current') {
            if (is_object($sobj)) $value = $sobj->Value;
            else if (is_array($sobj)) $value = $sobj['Value'];
            else $value = $sobj;
            $query = $this->dbh->select();
            $query->from('ADM_Setting');
            $query->where('Name = ?',$setting);
            $ss = $this->dbh->fetchAll($query);
            foreach($ss as $s) $tablename = $s['InTable'];
            //$tablename = "${prefix}${setting}";
            $intables[] = $tablename;
            if (isset($value)) $setvalues[] = $tablename;
            
            
            
            $query = $this->dbh->select();
            $query->from($rtable);
            $query->where('InTable = ?', $tablename);
            $restricts = $this->dbh->fetchAll($query);
            $restrictions = array();
            
            foreach($restrictions as $r) {
               $rid = $r->PER_Restriction_Id;
               $robj = new stcClass();
               $robj->Restriction = $r;
               $query = $this->dbh->select();         
               $query->from($ratable);
               $query->where('PER_Restriction_Id = ?', $rid);
               $robj->Actions = $this->dbh->fetchAll($query);
               
               $query = $this->dbh->select();
               $query->from($rptable);
               $query->where('PER_Restriction_Id = ?', $rid);
               $robj->Permissions = $this->dbh->fetchAll($query);
               
               $query = $this->dbh->select();
               $query->from($rstable);
               $query->where('PER_Restriction_Id = ?', $rid);
               $robj->Settings = $this->dbh->fetchAll($query);
               
               $query = $this->dbh->select();
               $query->from($rutable);
               $query->where('PER_Restriction_Id = ?', $rid);
               $robj->UserSupplied = $this->dbh->fetchAll($query);
               
               $restrictions[] = $robj;
            }
                  
         }
      }
            
      return $restrictions; 
   }
   */
}
?>