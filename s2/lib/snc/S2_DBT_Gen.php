<?php
session_start();
include_once('remoteRequestFunction.php');
include_once('post.php');
include_once('putverb.php');
include_once('deleteverb.php');

class DBT_Gen {
   protected $sid;
   protected $dbhandle;
   protected $dbp;
   protected $id;
   protected $tablename;
   protected $displayname;
   protected $validated = false;
   // alts contains a list of tables which can be listed instead of the 
   // default table in list views
   protected $alts = array();
   // show contains a list of tables to be shown on screen along with an entry 
   // for this table. 
   protected $show = array();
   // view supercedes show allowing for named views to limit the amount of data 
   // on any one page. view is defined as json text
   protected $orders = null; // example protected $orders = array("TextCol" => "ASC", "DateCol" => "DESC");
   protected $view;
   protected $perpage = 10;
   protected $user;
   // data types can be one of $this->datatypes or 
   // LINK:<tablename>.<columnname> and will pick up their 
   // datatype from that table. 
   // note that table will need to be included using 
   // include_once to ensure the definition can be read
   
   // column types can be appended with :Mandatory for required fields
   // linked data column types can be appended with :Current to use the 
   // current selected entry.  
   protected $columns = array();
   // permissions array can contain a permission for each action 
   // Add, Edit, Delete, List, Select, Create, Drop
   // if no permission is supplied, no permission is required 
   protected $domain = 'genera';
   protected $permissions = array();
   protected $instancedomain = false;
   protected $instanceof = null;
   
   protected $data = array();
   protected $primingdata = array();
   protected $selectedby;
   protected $childtables = array();
   protected $deriveddata = array();
   protected $importreq = array();
   // FilePath and URL are essentially just text but allow you to attach 
   // a different interface to populate them. 
   protected $showopts = array('Current','All','Next','Previous','Every');
   protected $actions = array('Def','List','View','Add','Edit','Delete','Import');               
   function DBT_Gen () {
      global $_CURRENT_USER;
      $this->dbhandle = SnC_getDatabaseConnection($this->domain);
      $this->dbp = SnC_getPlatform($this->domain);
      if (isset($_CURRENT_USER)) {
         if (!$_CURRENT_USER->is_anonymous && !isset($_CURRENT_USER->email)) $_CURRENT_USER->init($_REQUEST['UInfo']);
      }
      $this->user = $_CURRENT_USER;
      $this->sid = $_COOKIE['PHPSESSID'];
      // Post processed tables have an additional hidden column to record the 
      // current step in the process and to allow you to reset / reprocess the 
      // data.
      if (isset($this->postprocess)) $this->columns['PP_Step'] = '{"DataType":"Number","NoView":1,"NoList":1}'; 
   }
   function drawform() {
      $form = "<form method='post' action='' enctype='multipart/form-data'>
      <h3>$this->displayname</h3>
      <table><tbody>";
      foreach ($this->columns as $name => $type) {
         $displayname = ucwords(preg_replace('/\_/',' ',$name));
         $form.= "<tr><th>$displayname</th><td><input type='text' id='$name' name='$name'/></td></tr>";      
      }
      $form.= "</tbody></table></form>";
      return $form;
   }
   function settablename($tablename) {
      $this->tablename = $tablename;
   }
   function gettablename() {
      return $this->tablename;   
   }
   function getperpage() {
      return $this->perpage;
   }
   function getdatatypes() {
      $dt = array();
      foreach($this->datatypes as $type => $def) $dt[] = $type;
      
      return $dt;
   }
   function getactions() {
      return $this->actions;
   }
   function getsid() {
      return $this->sid;
   }
   function setsid($sid) {
      $this->sid = $sid;
   }
   function setdata($data,$revalidate=false) {
      //$this->data = $data;
      foreach($data as $col => $val) $this->data[$col] = $val;
      if (!$this->validated || $revalidate) $this->validate();
   }
   function isduplicate() {
      $dbh = $this->dbhandle;                       
      $query = $dbh->select();
      $query->from(strtolower($this->tablename));
      // CreatedBy and CreatedOn have to be excluded from duplicate testing.
      // Automatically populated data has to be excluded as well. 
      $excludedcolumns = array('CreatedBy','CreatedOn', 'Count', 'Version');
      foreach($this->columns as $col => $def) {
         $val = (isset($this->data[$col]))?$this->data[$col]:null;
         if (preg_match('/^\{/',$def)) {
            $params = json_decode($def);
            $dtype = $params->DataType;
         } else $dtype = preg_replace('/\:.*/','',$def);
         if (!in_array($col,$excludedcolumns) && isset($val)) {
            switch ($dtype) {
            case 'Map Data': {
               //$val = new Zend_Db_Expr("st_geomfromtext('$val',4326)");
               $val = $this->insertterm($col,$dtype,$val);
               $query->where("$col = ?",$val);   
            }break;
            case 'LINKEDTO': {
               if (is_object($val)) $val = $val->Id;
               $query->where("$col = ?",$val);
            }break;
            default: {
               $query->where("$col = ?",$val);
            }break;
            } 
         }
      }
//echo $query->__toString();exit;
      $result = $dbh->fetchRow($query);
      return (is_array($result))?true:false;      
   }
   function cleardata() {
      $this->data = array();
   }
   function getparent() {
      $parent = null;
      $pval;
      foreach($this->columns as $col => $def) {
         if(!isset($parent)) {
            if (preg_match('/^\{/',$def)) {
               $params = json_decode($def);
               $dtype = $params->DataType;
            } 
            if ($dtype == 'LINKEDTO' && isset($this->data[$col])) {
               $parent = $params->TargetType;
               $pval = $this->data[$col];
            }
         }  
      }
      $p = new stdClass();
      $p->IType = $parent;
      $p->Data = $pval;
      return $p;
   }
   function setuser($user) {
      $this->user = $user;
   }
   function getuser() {
      return $this->user;
   }
   function getdisplayname() {
      return $this->displayname;
   }
   function getversions($val) {
      $versions = array();
      foreach ($this->columns as $col => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $classname = (isset($params->VersionOfType))?$params->VersionOfType:null;
            $thiscol = (isset($params->VersionOfField))?$params->VersionOfField:null;
         } else list($type,$classname,$thiscol) = preg_split('/\:/',$type);
         if ($type == 'Version') {
            $link = null;
            $link = new $classname();
            if ($link->exists() && $val) {
               $versions['Column'] = $col;
               $link->loaddata($val,null,false);
               $column = $this->displayname;
               $column = preg_replace('/\s+/','_',$column);
               $cversion = $link->selectcolumnval($column);
               $cversion = (!$cversion>0)?1:$cversion;
               $versions['Current'] = floor($cversion*10)/10;
               
               if ($this->exists()) {
                  $query = $this->dbhandle->select();
                  $ltable = strtolower($this->tablename);
                  $lcol = strtolower($col);
                  $query->from($ltable, array('MaxVer' => new Zend_Db_Expr("Max($lcol)")));
                  $lthiscol = strtolower($thiscol);
                  $query->where("$lthiscol = ?",$val);
                  $result = $this->dbhandle->fetchRow($query);
                  if (isset($result['MaxVer'])) //$versions['Maximum'] = floor($result['MaxVer']*10)/10;
                     $versions['Maximum'] = floor(round($result['MaxVer'],1)*10)/10;
               }
            }               
         } 
      }
      return $versions;
   }
   function reserve($col,$maxinuse) {
      $dom = $this->getdomain();
      $tab = $this->tablename;
      $link = new G_Reserve_Number(); 
      $res = $link->reserve($dom,$tab,$col,$maxinuse);
      return $res;
   }    
   function unreserve() {
      $dom = $this->getdomain();
      $tab = $this->tablename;
      $link = new G_Reserve_Number(); 
      $link->unreserve($dom,$tab);
   }
   function getcount($val) {
      $count = 0;
      foreach ($this->columns as $col => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $classname = (isset($params->CountOfType))?$params->CountOfType:null;
            $thiscol = (isset($params->CountOfField))?$params->CountOfField:null;
         } else list($type,$classname,$thiscol) = preg_split('/\:/',$type);
         if ($type == 'Count') {
            if ($val && $thiscol) {
               $query = $this->dbhandle->select();
               $lcol = strtolower($col);
               $lthiscol = strtolower($thiscol);
               $query->from(strtolower($this->tablename), array('MaxCount' => new Zend_Db_Expr("Max($lcol)")));
               $query->where("$lthiscol = ?",$val);
               $result = $this->dbhandle->fetchRow($query);
               if (isset($result['MaxCount'])) $count = $result['MaxCount'];
            }               
         } 
      }
      return $count;        
   } 
   function getnameid() {
      $obj = new stdClass();
      $obj->Id = $this->getid();
      $obj->Name = $this->getname();
      return $obj;
   }
   function derivename($columns,$sep=': ') {
      $names = array();
      foreach($columns as $col) {
         $type = $this->columns[$col];
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            switch($type) {
            case 'LINKEDTO': {
               $table = (isset($params->TargetType))?$params->TargetType:null;
               $tcol = (isset($params->TargetField))?$params->TargetField:null;
            }break;
            }
         } else if (preg_match('/([^\:]+)\:([^\.]+)\.(.+)/',$type,$matches)) {
            $type = $matches[1];
            $table = $matches[2];
            $tcol = $matches[3];
         }
         switch($type) {
         case 'LINKEDTO': {
            if (isset($table) && isset($tcol)) {
               $ilink = new $table();
               $ilid = (is_object($this->data[$col]))?$this->data[$col]->Id:$this->data[$col];
               if ($ilid > 0) {
                  $isloaded = $ilink->loaddata($ilid,$tcol);
                  if ($isloaded) $names[] = $ilink->getshortname();
               }
            }
         }break;
         case 'INTERFACE': {
            $iobj = $this->data[$col];
            if (is_object($iobj) && isset($iobj->Name)) $names[] = $iobj->Name;
            else if (is_array($iobj) && isset($iobj['Name'])) $names[] = $iobj['Name'];
         }break;
         default: {
            $names[] = $this->data[$col];
         }break;
         }
      }
      return join($sep,$names);   
   }
   function getdata() {
      foreach($this->data as $col => $val) {
         $type = $this->columns[$col];
         $params = null;
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
         } else {
            $bits = preg_split('/\:/',$type);
            $type = $bits[0];
         }
         switch ($type) {
         case 'Date': {
            if (preg_match('/(\d+)\-(\d+)\-(\d+)/',$val,$matches)) {
               $y = $matches[1];
               $m = $matches[2];
               $d = $matches[3];
               if (intval($y)+intval($m)+intval($d)>0) {
                  $val = "$d/$m/$y";
                  $this->data[$col] = $val;
               } else $this->data[$col] = '';
            }
         }break;
         case 'Date From': {
            if (preg_match('/(\d+)\-(\d+)\-(\d+)/',$val,$matches)) {
               $y = $matches[1];
               $m = $matches[2];
               $d = $matches[3];
               if (intval($y)+intval($m)+intval($d)>0) {
                  $val = "$d/$m/$y";
                  $this->data[$col] = $val;
               } else $this->data[$col] = '';
            }
         }break;
         case 'Date Until': {
            if (preg_match('/(\d+)\-(\d+)\-(\d+)/',$val,$matches)) {
               $y = $matches[1];
               $m = $matches[2];
               $d = $matches[3];
               if (intval($y)+intval($m)+intval($d)>0) {
                  $val = "$d/$m/$y";
                  $this->data[$col] = $val;
               } else $this->data[$col] = '';
            }
         }break;
         case 'LINKEDTO': {
            if (isset($params)) {
               if (!is_object($val) && $val > 0) {
                  $type = $params->DataType;
                  $table = $params->TargetType;
                  $column = $params->TargetField;
                  $link = null;
                  $link = new $table();
                  $link->loaddata($val,$column);
                  $obj = new stdClass();
                  $obj->Name = $link->getname();
                  $obj->Id = $link->getid();
                  $this->data[$col] = $obj;
               }
            } else if(preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches)) {
               if (!is_object($val)) {
                  $type = $matches[1];
                  $table = $matches[2];
                  $column = $matches[3];
                  $link = null;
                  $link = new $table();
                  $link->loaddata($val,$column);
                  $obj = new stdClass();
                  $obj->Name = $link->getname();
                  $obj->Id = $link->getid();
                  $this->data[$col] = $obj;
               }
            }   
         }break;
         case 'LINKEDTOM': {
            if (isset($params)) {
               if (!is_object($val)) {
                  $type = $params->DataType;
                  $table = $params->TargetType;
                  $column = $params->TargetField;
                  $pk = $this->getpk();
                  $id = $this->getid();
                  $p = (isset($params->Property))?$params->Property:$col;
                  $ltm = new DBT_Lookup($p,$this->tablename,$pk,$table,$column);
                  $obj = new stdClass();
                  $obj->IType = $table;
                  $obj->ListData = $ltm->selecttargets($pk,$id);
                  $this->data[$col] = $obj;
               }
            } else if(preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches)) {
               if (!is_object($val)) {
                  $type = $matches[1];
                  $table = $matches[2];
                  $column = $matches[3];
                  $pk = $this->getpk();
                  $id = $this->getid();
                  $ltm = new DBT_Lookup($col,$this->tablename,$pk,$table,$column);
                  $obj = new stdClass();
                  $obj->IType = $table;
                  $obj->ListData = $ltm->selecttargets($pk,$id);
                  $this->data[$col] = $obj;
               }
            }   
         }break;
         case 'INTERFACE': {
            if(isset($params)) {
               if (is_object($val) && isset($val->JSON)) $this->data[$col] = json_decode($val->JSON); 
               else if (is_array($val) && isset($val['JSON'])) $this->data[$col] = json_decode($val['JSON']);  
            }
         }break;
         case 'Taxa': {
            if(isset($params)) { 
               if (is_object($val) && isset($val->JSON)) {
                  $this->data[$col] = json_decode($val->JSON); 
               }
            }
         }break;
         case 'Password': {
            $k = new KLib($this->getsid());
            $keys = json_decode($k->getkeys());
            $value = encrypt($value,$keys->k1,$keys->k2);
         }break;
         case 'Encrypted Text': {
            $k = new KLib($this->getsid());
            $keys = json_decode($k->getkeys());
            $value = encrypt($value,$keys->k1,$keys->k2);
         }break;
         }
      }
      if (!isset($this->data['Name'])) {
         $this->data['Name'] = $this->getname();
      }
      return $this->data;
   }
   function setcolumns($cols) {
      $this->columns = $cols;
   }                                           
   function getcolumns() {
      return $this->columns;
   }
   function getcolumnsbytype($type) {
      $tcols = array();
      foreach($this->columns as $col => $ctype) {
         $params = $this->getcoldef($col);
         if ($params->DataType == $type) {   $tcols[] = $col;  }   
      }
      return $tcols;
   }
   function getcoldef($col) {
      $type = $this->columns[$col];
      $params = null;
      if (preg_match('/^\{/',$type)) $params = json_decode($type);
      else if (preg_match('/([^\:]+)\:([^\.]+)\.(.+)/',$type,$matches)) {
         $params = new stdClass();
         $params->DataType = $matches[1];
         $params->TargetType = (isset($matches[2]))?$matches[2]:null;
         $params->TargetField = (isset($matches[3]))?$matches[3]:null;
      }
      return $params;         
   }
   function getpermissions() {
      return $this->permissions;
   }
   function getdomain() {
      if ($this->instancedomain) {
         $settings = (isset($_REQUEST['CurrentSettings']))?(array)json_decode(stripslashes($_REQUEST['CurrentSettings'])):array();
//if($this->tablename == 'S2_Site') print $this->instancedomain; print "SETTINGS"; print_r($settings);exit;
         $domain = $this->domain;
         if (isset($settings[$this->instanceof])) $domain .= "_".$settings[$this->instanceof]->Current;  
         return $domain;
      } else return $this->domain;
   }
   function getorigdomain() {
      return $this->domain;
   }
   function getdomaininstancename() {
      if ($this->instancedomain) {
         $settings = (array)json_decode(stripslashes($_REQUEST['CurrentSettings']));
         $domain = $this->domain;
         if ($settings[$this->instanceof]) $domain = $settings[$this->instanceof]->Name;  
         return $domain;
      } else return $this->domain;
   }
   function getinstanceof() {
      if ($this->instancedomain) return $this->instanceof;
      else return null;
   }
   function getuserpermissions() {
      //$perms = array('List','View','Add','Edit','Delete','Import');
      $perms = $this->actions;
      array_shift($perms);
      $granted = array();
      foreach($perms as $perm) {
         if ($this->isallowed($this->user,$perm)) $granted[$perm] = true;      
      }
      return $granted;      
   }
   function isallowed($user,$action) {
      //global $_CURRENT_USER;
      // permissions can be set on a specific action
      if (!in_array($action,$this->actions)) {
      // limited actions are set in $this->actions 
      // other actions such as CheckIn or Calculate do not require permission
         return 1;
      } else {
         $required = (isset($this->permissions[$action]))?$this->permissions[$action]:null;
         // or you can set a single requirement for all actions
         if (!isset($required)) $required = (isset($this->permissions['Def']))?$this->permissions['Def']:null;
         if (isset($required)) {
            if (isset($user)) {
               if (isset($this->data['Created_By']) && ($user->getid() == $this->data['Created_By'])) return 1;
               if ($this->instancedomain) {
                  $effective = $this->getdomain();
                  return max(
                        $user->get_permission($this->domain,$required),
                        $user->get_permission($effective,$required));                               
               } else return $user->get_permission($this->domain,$required); 
            }
         } else return 1;
      }
      // If no permission is required return 1
   }
   function geticon() {
      return null;
   }
   function getname() {
      $name = null;
      if(isset($this->columns['Name'])) $name = $this->data['Name'];
      else {
         foreach($this->columns as $column => $type) {
            preg_match('/([^\:]+)/',$type,$matches);
            $type = $matches[1];                                                                                  
            if($type == 'Short Text') {
               $name = $this->data[$column];
               break;
            }
         }
      }
      return $name; 
   }
   // the short name method is a place holder to be overridden for subclasses 
   function getshortname() {
      return $this->getname();
   }
   function noshow() {
      $this->show = array();
   }
   function getviewdefs($vname = null) {
      $v = (isset($this->view))?json_decode($this->view):null;
      if (isset($vname)) {
         foreach ($v as $i => $vi) if ($vi->Name == $vname) $vdef = $vi;
         if (isset($vdef)) $v = $vdef;
      }
      return $v;   
   }
   function getshowlist() {
      $s = (isset($this->show))?$this->show:null;
      return $s;      
   }
   function getshowopts() {
      return $this->showopts;      
   }
   function getextdataparams($extdata) {
      global $configsettings;
      foreach($extdata as $ex => $edef) {
         $ps = $edef->Parameters;
         foreach($ps as $px => $pdef) {
            switch($pdef->Type) {
            case 'Column': $pdef->Value = $this->data[$pdef->Value];break;
            case 'Config': $pdef->Value = $configsettings[$pdef->Value];break;
            }
            if ($pdef->Encrypt) {
               $k = new KLib($this->getsid());
               $keys = json_decode($k->getkeys());
               $pdef->Value = encrypt($pdef->Value,$keys->k1,$keys->k2);
            }
            $ps[$px] = $pdef;
         }
         if (isset($edef->Credentials)) {
            $cred = $edef->Credentials; 
            foreach($ps as $px => $pdef) {
               if ($cred->Parameter == $pdef->Name) {
                  $cs = $configsettings[$pdef->Value];
                  foreach ($cs as $cx => $c) {
                     $np = new stdClass();
                     $np->Name = "CR".($cx+1);
                     $np->Value = $c;
                     $ps[] = $np;
                  }
               }                
            }
         }
         $edef->Parameters = $ps;
         $extdata[$ex] = $edef;
      }
      return $extdata;
   }
   function getprocessor() {
      $ptype = 'G_Post_Processor';
      $proc = new $ptype();
      $proc->loadmyprocessor($this->getid(),$this->tablename);
      $pid = $proc->getid();
      if (!isset($pid)) {
         $json->PostProcess = json_decode($this->postprocess);
         $steps = count($json->PostProcess);
         $pdata = array(
            'Type'                  => $this->tablename,
            'Type_ID'               => $this->getid(),
            'Last_Step'             => 0,
            'Out_Of'                => $steps,
            'Status'                => 'Processing started',
            'Percentage_Complete'   => 0
         );
         $proc->setdata($pdata);
         $proc->insert();
      }
      return $proc;              
   }
   function getview($viewsettings=null,$viewname=null) {
      $json = new stdClass();
      $json->IType = $this->tablename;
      $json->DisplayName = $this->getdisplayname();
      $json->Interface = $this->getinterface();
      //$json->Versions = $this->getversions();
      $json->Current = (isset($this->data[$this->getpk()]))?$this->data[$this->getpk()]:null;
      $json->Domain =  $this->getorigdomain();
      $json->IsAllowedTo = $this->getuserpermissions();
               
      if ($json->Current) {
         // reload data because some of the data might have been manipulated 
         // for the database insert.
         $id = $this->getid();
         $this->loaddata($this->getid());
         $json->Data = $this->getdata();
         $json->ItemName = $this->getname();
         if (isset($this->postprocess)) {
            $json->PostProcess = json_decode($this->postprocess);
            $proc = $this->getprocessor();
            $json->ProcessStatus = $proc->getstatus();
         }
      }
         
      $linkdata = array();
      $show = null;
      $views = array();
      $iview = null;
      if (isset($this->view)) {
         $viewdata = json_decode($this->view);
         if (count($viewdata)>0) {
            $isgroup = false;
            $sviews = array();
            foreach($viewdata as $view) {
               $vtype = isset($view->Type)?$view->Type:null;
               if ($vtype != 'Submenu') $views[] = $view->Name;
               //if ((isset($viewname)&&$viewname == $view->Name)||(!isset($viewname)&&!isset($show))) {
               if (isset($viewname)&&($viewname == $view->Name)) {
                  $show = $view->Data;
                  $json->ViewName = $viewname;
                  if (isset($view->External)) {$json->External = $this->getextdataparams($view->External);}
                  switch($vtype) {
                  case 'Group': {
                     $isgroup = true;
                     $sviews = array_merge(array($view->Name),$view->Submenu);                     
                  }break;
                  case 'Submenu': {
                     $isgroup = true;
                     foreach($viewdata as $sview) if ($sview->Name == $view->Under) $sviews = array_merge(array($sview->Name),$sview->Submenu);
                  }break;
                  } 
               }
            }
            if ($isgroup) $views = $sviews;
         }
      } 
      if (!isset($show)&&!isset($this->view)) $show = $this->show;
      if (isset($this->alts)) $json->Alternatives = $this->alts;
      
      if(count($views)>0) $json->Views = $views;
      if (is_array($show)) {      
         foreach($show as $data) {
            if (is_object($data)) {
               $classname = $data->Table;
               $showopts = (isset($data->Show))?$data->Show:'All';
               $sectionname = (isset($data->Name))?$data->Name:null;            
            } else {
               // This is for if the show array replaces the view definition
               $classname = $data;
               if (preg_match('/([^:]+):(.+)/',$classname,$m)) $classname = $m[1];
               $showopts = 'All';
            }         
            $link = new $classname();
            if (isset($link)) $link->setuser($this->user);
            if (isset($link) && $link->isallowed($this->user,'List')) {
               if (!$link->exists()) $link->create();
               eval("\$osettings = (isset(\$viewsettings->$classname))?\$viewsettings->$classname:null;");
               // see if link is in this table first
               list($scol,$tcol) = $this->getlinkedcolumn($classname);
               // then if it isn't see if it is in the linked table
               if (!isset($scol)) list($tcol,$scol) = $link->getlinkedcolumn($this->tablename);
               $val = $this->data[$scol];
               if (is_object($val)) $val = ($val->Current)?$val->Current:$val->Id;
            //print "$classname $scol $tcol $val\n";
               if (isset($val)||$showopts == 'Every') {
                  $ld = new stdClass();
                  $ld->IType = $classname;
                  $ld->DisplayName = (isset($sectionname))?$sectionname:$link->getdisplayname();
                  if (isset($data->Columns)) $ld->Columns = $data->Columns;
                  $ld->Interface = $link->getinterface();
                  $ld->Domain =  $link->getorigdomain();
                  $ld->Versions = $link->getversions($val);
                  $ld->Count = $link->getcount($val);
                  $countcols = $this->getcolumnsbytype('Count');
                  if (count($countcols)>0) {
                     $col = $countcols[0];
                     $ld->Reserved = $link->reserve($col,$ld->Count);
                  } 
                  $ld->IsAllowedTo = $link->getuserpermissions();
                  switch($showopts) {
                  case 'Current': {
                     $usecol = $link->getdisplayname();
                     $usecol = preg_replace('/\s+/','_',$usecol);
                     $byval = $this->selectcolumnval($usecol); 
                     $link->loadone($val,$tcol,'Current',$byval);
                     
                     $lid = $link->data[$link->getpk()];
                     if (isset($lid)) {  
                        $ld->Data = $link->getdata();
                        $ld->Current = $lid; 
                        $linkdata[] = $ld;
                     }
                  }break;
                  case 'This': {
                     $tcol = (isset($data->Link))?$data->Link:((isset($this->indexon))?$this->indexon:$this->getpk());
                     $type = (isset($this->selectto))?$this->selectto:$this->tablename;
                     if (isset($data->Link)) {
                        $val = $this->data[$data->Link];
                        if (is_object($val)) $val = $val->Id;
                     } else if (isset($type) && is_object($viewsettings->$type)) {
                        $vset = $viewsettings->$type;
                        $val = $vset->Current;
                     }
                     if (isset($val)) {
                        //$val = (is_object($val))?$val->Id:$val;
                        $link->loaddata($val,$tcol,false);
                        $lid = $link->getid();
                        if (isset($lid)) {
                           $ld->Data = $link->getdata();
                           $ld->Current = $lid; 
                           if (isset($data->View)) {
                              $iviewdata = $link->getview($viewsettings,$data->View);
                              if(isset($iviewdata->LinkedData)) $linkdata = $iviewdata->LinkedData;
                              if(isset($iviewdata->External)) $json->External = $iviewdata->External;
                              $ld->View = $data->View;
                           } 
                           else $linkdata[] = $ld;
                        }
                     }
                  }break;
                  case 'Next': {
                     $link->loadone($val,$tcol,'Next');
                     $lid = $link->data[$link->getpk()];
                     if (isset($lid)) {  
                        $ld->Data = $link->getdata();
                        $ld->Current = $lid; 
                        $linkdata[] = $ld;
                     }
                  }break;
                  case 'Previous': {
                     $link->loadone($val,$tcol,'Previous');
                     $lid = $link->data[$link->getpk()];
                     if (isset($lid)) {  
                        $ld->Data = $link->getdata();
                        $ld->Current = $lid; 
                        $linkdata[] = $ld;
                     }                  
                  }break;
                  case 'All': {
                     /*
                     $data = $link->select($tcol,$val,$viewsettings);
                     if (count($data) > $link->perpage && !isset($_REQUEST['NoPaging'])) {
                        $pages = array_chunk($data,$link->perpage);
                        $page = (isset($osettings) && isset($osettings->Page))
                                    ?$osettings->Page
                                    :1;
                        $ld->ListData = $pages[$page-1];
                        $ld->Page = $page;
                        $ld->Pages = count($pages);
                     } else $ld->ListData = $data;
                     //$ld->Count = count($data);
                     //*/
                     if (!isset($_REQUEST['NoPaging'])) {
                        $rows = $link->selectRowCount($tcol,$val,$viewsettings);
                        $page = (isset($osettings) && isset($osettings->Page))
                                 ?$osettings->Page
                                 :1;
                        $data = $link->select($tcol,$val,$viewsettings,null,$page);
                        $ld->ListData = $data;
                        $ld->Page = $page;
                        $ld->Pages = $pages = ($rows>0)?ceil($rows/$link->perpage):1;
                     } else {
                        $data = $link->select($tcol,$val,$viewsettings);
                        $ld->ListData = $data;   
                     }
                     $linkdata[] = $ld;
                     
                  }break;
                  case 'Every': {
                     /*
                     $data = $link->selectAll($viewsettings);
                     if (count($data) > $link->perpage) {
                        $pages = array_chunk($data,$link->perpage);
                        $page = (isset($osettings) && isset($osettings->Page))
                                    ?$osettings->Page
                                    :1;
                        $ld->ListData = $pages[$page-1];
                        $ld->Page = $page;
                        $ld->Pages = count($pages);
                     } else $ld->ListData = $data;
                     //$ld->Count = count($data);
                     $linkdata[] = $ld;
                     //*/
                     if (!isset($_REQUEST['NoPaging'])) {
                        $rows = $link->selectAllRowCount($viewsettings);
                        $page = (isset($osettings) && isset($osettings->Page))
                                 ?$osettings->Page
                                 :1;
                        $data = $link->selectAll($viewsettings,$page);
                        $ld->ListData = $data;
                        $ld->Page = $page;
                        $ld->Pages = $pages = ($rows>0)?ceil($rows/$link->perpage):0;
                     } else {
                        $data = $link->selectAll($viewsettings);
                        $ld->ListData = $data;
                     }
                     $linkdata[] = $ld;
                  }break;
                  case 'Links': {
                     $ld->Type = 'Links';
                     $data = $link->select($tcol,$val,$viewsettings);
                     $ucol;
                     foreach($ld->Interface as $col => $def) {
                        $type = (is_object($def))?$def->DataType:$def;
                        if ($type == 'URL') $ucol = $col;     
                     }
                     foreach($data as $ex => $entry) {                
                        $name =(isset($entry['Name']))?$entry['Name']:((is_object($entry['S2_File_ID']))?$entry['S2_File_ID']->Name:null);
                        $lnk = array(
                           'URL'       => $entry[$ucol],
                           'Name'      => $name,
                           'Current'   => $entry[$link->getpk()]
                        );
                        $data[$ex] = $lnk;       
                     }
                     $ld->ListData = $data;
                     $linkdata[] = $ld;
                  }break;
                  case 'Within': {   
                     $icol = $this->getmapcol();
                     $lcol = $link->getmapcol();
                     $data = (isset($icol) && isset($this->data[$lcol]))
                        ? $link->select($lcol,$this->data[$lcol],$viewsettings)
                        : array();               
                  }break;
                  }
               }
            } 
//else {$eff = $link->getdomain();$linkdata[] = "Permission refused for $classname list. ($eff).";}
         }
      }
      if (count($linkdata)>0) $json->LinkedData = $linkdata;
//print "<pre>"; print_r($json); print "</pre>"; exit; 
      return $json;
   }
   function getmapcol() {
      $dtype = null;
      foreach($this->columns as $col => $type) {
         $int = $this->getcolumndef($col);
         $dtype = $int->DataType;
         if ($dtype == "Map Data") break;  
      }
      return $dtype;
   }
   function getcolumndef($col) {
      $type = $this->columns[$col];
      $int = null;
      if ($type) {
         if (is_string($type) && preg_match('/^\{/',$type)) {
            $int = json_decode($type);           
         } else {
            $int = new stdClass();
            $int->DataType = preg_replace('/\:*/','',$type);
         }
      }
      return $int;
   }
   function getderiverequirements() {
      $int = $this->getinterface();
      $req = array();
      foreach ($int as $column => $type) {
         if (is_object($type) && $type->DataType == 'LINKEDTO') {
            $c = $type->TargetType;
            $l = new $c();
            $o = new stdClass();
            $o->DataType = $c;
            $o->Column = $column;
            //$o->Definition = $type;
            $lint = $l->getinterface();
            foreach($lint as $lcol => $ltype) {
               if (is_object($ltype) 
                     && ($ltype->DataType == 'LINKEDTO' 
                        || $ltype->DataType == 'LINKEDTOM')) 
                  unset($lint[$lcol]);
            } 
            $o->Interface = $lint;
            $req[$c] = $o; 
            $d = $l->getderiverequirements();
            $req = array_merge($req,$d);                           
         }         
      }
      return $req;
   }
   function loadderiveddata($crumbs,$icol=null,$ival=null) {
//print "loading data for ".$this->gettablename()."<br/>";
      $int = $this->getinterface();
      $query = $this->dbhandle->select();
      $query->from($this->tablename);
      $pk = $this->getpk();
      $crumbs = (array)$crumbs;
      foreach ($int as $col => $type) {
         if (is_object($type) && $type->DataType == 'LINKEDTO') {
            foreach($crumbs as $ind => $crumb) {
               if ($crumb->IType == $type->TargetType) {
                  //print "<pre>";print_r($type);print "</pre>";
                  $it = $crumb->IType;
                  switch ($crumb->Type) {
                  case 'SameForAll': {
                     $query->where("$col = ?", $crumb->Value);
                  }break;
                  case 'Link': {
                     $link = new $it();
                     $lid = $link->loadderiveddata($crumbs,$crumb->TColumn,$crumb->Value); 
                     $query->where("$col = ?",$lid);
                  }break;
                  } 
               }
            }
         }   
      }
      if (isset($icol) && isset($ival)) $query->where("$icol = ?",$ival);
//echo $query->__toString();
      $this->data = $this->dbhandle->fetchRow($query);
      return $this->getid();          
   }
   function getinterface() {
      $interface = array();
      foreach ($this->columns as $column => $type) {
         switch($type) {
         //case 'Unique Key': break;
         default: {
            if (is_string($type) && preg_match('/^\{/',$type)) {
               $interface[$column] = json_decode($type);           
            } else $interface[$column] = $type;
         }break;
         }
      }
      return $interface;
   }  
   function getimportrequirements() {
      $ireq = array();
      foreach($this->importrequirements as $itype) {
         $link = new $itype();
         $iface = $link->getinterface();
         $ireq[$itype] = $iface;
      }
      return $ireq;
   }                
   function getshow() {
      return $this->show;
   }
   function getpk() {
      foreach($this->columns as $name => $type) if ($type == 'Unique Key') $pk = $name;
      return $pk;
   }
   function getlinkedcolumn($matchtable) {
      $incolumn = null;
      $tocolumn = null;
      foreach($this->columns as $column => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
         }
         $matches = array();
         if (isset($params)) {
            $table = $params->TargetType;
            if ($matchtable == $table) {
               $incolumn = $column;
               $tocolumn = $params->TargetField;
               break;
            }
         } else if (preg_match('/LINKEDTO\:([^\.]+)\.([^\:]+)/',$type,$matches)) {
            $table = $matches[1];
            if ($matchtable == $table) {
               $incolumn = $column;
               $tocolumn = $matches[2];
               break;
            }
         }
      }
      return array($incolumn,$tocolumn);
   }
   function prime() {
      if (isset($this->primingdata)) {
         $this->validated = true;
         foreach ($this->primingdata as $classname => $data) {
            $data = trim($data);
            $data = preg_split('/\n/',$data);
            foreach($data as $line => $text) {
               if ($line == 0) $cols = $this->parselinecsv($text);
               else {
                  $idata = array();
                  $vals = $this->parselinecsv($text); 
                  foreach($cols as $colnum => $col) {
                     $idata[$col] = $vals[$colnum]; 
                  }
                  if ($classname == $this->tablename) {
                     $this->setdata($idata);
                     $this->insert();                 
                  } else {
                     $link = null;
                     $link = new $classname();
                     if (isset($link)) {
                        if (!$link->exists()) $link->create();
                        $link->validated = true;
                        $link->setdata($idata);
                        $link->insert();
                     }
                  }
               }               
            }                              
         }
      }
      $this->data = array();
   }
   function initialise() {
      if ($this->defaultpermissions) {
         $this->validated = true;
         foreach ($this->defaultpermissions as $classname => $data) {
            $data = trim($data);
            $data = preg_split('/\n/',$data);
            foreach($data as $line => $text) {
               if ($line == 0) $cols = $this->parselinecsv($text);
               else {
                  $idata = array();
                  $vals = $this->parselinecsv($text); 
                  foreach($cols as $colnum => $col) {
                     $idata[$col] = $vals[$colnum]; 
                  }
                  if ($classname == $this->tablename) {
                     $this->setdata($idata);
                     if (!$this->isduplicate()) $this->insert();                 
                  } else {
                     $link = null;
                     $link = new $classname();
                     if (isset($link)) {
                        if (!$link->exists()) $link->create();
                        $link->validated = true;
                        $link->setdata($idata);
                        if (!$link->isduplicate()) $link->insert();
                     }
                  }
               }               
            }                              
         }
      }
      $this->data = array();
   }
   function parselinecsv($string) {
      $elements = array();
      if (preg_match("/\"/",$string)) {
         $bits = split('"',$string);
         for ($i=0;$i<count($bits);$i++) {
            $bit = $bits[$i];
            $bit = preg_replace("/\s+$/","",$bit);
            if ($i%2==1) {
               $elements[] = $bit;
            } else if ($bit != ',' && strlen($bit)>0) {
               $s = $bit;
               $s = preg_replace("/^,/","",$s);
               $s = preg_replace("/,$/","",$s);
               $elements = array_merge($elements,split(",",$s));
            }
         }
      } else {
         $elements = split(",",$string);
      }
      return $elements;
   }
   function preformatterm($col,$type,$val) {
      switch($type) {
      case 'Taxa': {
         if(isset($val) && !is_object($val)) { 
            //$val = getnbntaxa($col,$val);
            $ccol = (preg_match('/^[A-Z,0-9]{16}$/',$val))?'Value':'Name';
            $gc = new G_Cache();
            $ismatched = $gc->loaddata($val,$ccol);
            if (!$ismatched) {
               $val = (array)getnbntaxa($col,$val);
               $gc->setdata($val);
               $id = $gc->insert();
            }
            $val = (object)$gc->getdata();
         }
      }break;
      case 'UUID': {
         if (!isst($val)) $val = gen_uuid();
      }break;
      }
      return $val;
   }
   function dbencrypt($val) {
      //*
      $k = new KLib($this->getsid());
      $keys = json_decode($k->getkeys());
      $val = '$'.$keys->k1.$keys->k2.encrypt($val,$keys->k1,$keys->k2);
      //*/
      return $val;
   }
   function dbdecrypt($col,$val) {
      //*
      if (preg_match('/^\$(.{8})(.{8})(.+)/',$val,$mat)) $val = decrypt($mat[3],$mat[1],$mat[2]);
      else $this->dbupcode($col,$val);
      //*/ 
      return $val;
   }
   function postprocessterm($col,$type,$val,$params=null) {
      switch ($type) {
      case 'True or False': {
         $val = ($val == true)?1:0;
      }break;
      case 'Password': {
         $val = $this->dbdecrypt($col,$val);
         $k = new KLib($this->getsid());
         $keys = json_decode($k->getkeys());
         $val = encrypt($val,$keys->k1,$keys->k2);
      }break;
      case 'Encrypted Text': {
         $val = $this->dbdecrypt($col,$val);
         $k = new KLib($this->getsid());
         $keys = json_decode($k->getkeys());
         $val = encrypt($val,$keys->k1,$keys->k2);
      }break;
      case 'Date': {
         if (preg_match('/(\d+)\-(\d+)\-(\d+)/',$val,$matches)) {
            $y = $matches[1];
            $m = $matches[2];
            $d = $matches[3];
            if (intval($y)+intval($m)+intval($d)>0) {
               $val = "$d/$m/$y";
            } else $val = ''; 
         }
      }break;
      case 'Date From': {
         if (preg_match('/(\d+)\-(\d+)\-(\d+)/',$val,$matches)) {
            $y = $matches[1];
            $m = $matches[2];
            $d = $matches[3];
            if (intval($y)+intval($m)+intval($d)>0) {
               $val = "$d/$m/$y";
            } else $val = ''; 
         }
      }break;
      case 'Date Until': {
         if (preg_match('/(\d+)\-(\d+)\-(\d+)/',$val,$matches)) {
            $y = $matches[1];
            $m = $matches[2];
            $d = $matches[3];
            if (intval($y)+intval($m)+intval($d)>0) {
               $val = "$d/$m/$y";
            } else $val = ''; 
         }
      }break;
      case 'LINKEDTO': {
         if (isset($params)) {
            if (!is_object($val)) {
               $type = $params->DataType;
               $table = $params->TargetType;
               $column = $params->TargetField;
               $link = null;
               $link = new $table();
               $link->loaddata($val,$column,false);
               $obj = new stdClass();
               $obj->Name = $link->getname();
               $obj->Id = $link->getid();
               $val = $obj;
            }
         }   
      }break;
      case 'LINKEDTOM': {
         if (isset($params)) {
            if (!is_object($val)) {
               $type = $params->DataType;
               $ttab = $params->TargetType;
               $tcol = $params->TargetField;
               $p = (isset($params->Property))?$params->Property:$col;
               $pk = $this->getpk();
               $id = $this->getid();
               $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
               $obj = new stdClass();
               $obj->IType = $ttab;
               $obj->ListData = $ltm->selecttargets($pk,$row[$pk]);
               $val = $obj;
            }
         }   
      }break;
      case 'INTERFACE': {
         if ($val > 0) { 
            $cache = new G_Cache();
            $cache->loaddata($val);
            $val = $cache->getdata();
         }
      }break;
      case 'Taxa': {
         if ($val > 0) { 
            $cache = new G_Cache();
            $cache->loaddata($val);
            $val = $cache->getdata();
         }
      }break;
      case 'Short Text':   {$val = stripslashes($val);}break;
      case 'Medium Text':  {$val = stripslashes($val);}break;
      case 'Long Text':    {$val = stripslashes($val);}break;
      }
      return $val;
   }
   function validate() {
      foreach($this->columns as $cname => $ctype) {
         if (isset($this->data[$cname]) && $this->data[$cname] == '') $this->data[$cname] = null; 
         if (preg_match('/^\{/',$ctype)) {
            $params = json_decode($ctype);
            $ctype = $params->DataType;
         } else {
            $bits = preg_split('/\:/',$ctype);
            $ctype = $bits[0];
         }
         switch($ctype) {
         case 'Number': {
            if (isset($this->data[$cname])) $this->data[$cname] = intval($this->data[$cname]);
         }break;
         case 'Decimal': {
            if (isset($this->data[$cname])) $this->data[$cname] = floatval($this->data[$cname]);
         }break;
         case 'Date': {
            if (isset($this->data[$cname]) && preg_match('/(\d{2})\/(\d{2})\/(\d{4})/',$this->data[$cname],$matches)) {
               $this->data[$cname] = $matches[3]."-".$matches[2]."-".$matches[1];
            } 
         }break;
         case 'Date From': {
            if (isset($this->data[$cname]) && preg_match('/(\d{2})\/(\d{2})\/(\d{4})/',$this->data[$cname],$matches)) {
               $this->data[$cname] = $matches[3]."-".$matches[2]."-".$matches[1];
            } 
         }break;
         case 'Date Until': {
            if (isset($this->data[$cname]) && preg_match('/(\d{2})\/(\d{2})\/(\d{4})/',$this->data[$cname],$matches)) {
               $this->data[$cname] = $matches[3]."-".$matches[2]."-".$matches[1];
            } 
         }break;
         case 'Time': {
            //?
         }break;
         case 'Date and Time': {
            // Timestamps should always default to now. 
            if (!isset($this->data[$cname])) $this->data[$cname] = new Zend_Db_Expr('NOW()');
         }break;
         case 'Password': {
            $k = new KLib($this->getsid());
            $keys = json_decode($k->getkeys());
            if(isset($this->data[$cname])) $this->data[$cname] = decrypt($this->data[$cname],$keys->k1,$keys->k2);
         }break;
         case 'Encrypted Text': {
            $k = new KLib($this->getsid());
            $keys = json_decode($k->getkeys());
            if(isset($this->data[$cname])) $this->data[$cname] = decrypt($this->data[$cname],$keys->k1,$keys->k2);
         }break;
         case 'INTERFACE': {
            if(isset($this->data[$cname])) $this->data[$cname] = json_decode(stripslashes($this->data[$cname]));//(is_object($this->data[$cname]))?$this->data[$cname]:
         }break;
         case 'LINKEDTO': {
            if (is_object($this->data[$cname])) $this->data[$cname] = $this->data[$cname]->Id;
         }break;
         case 'Taxa': {
            if (!is_object($this->data[$cname])) $this->data[$cname] = json_decode($this->data[$cname]);
         }break;
         default: {
            if (isset($this->data[$cname]) && is_object($this->data[$cname])) $this->data[$cname] = json_encode($this->data[$cname]);
         }break;
         }
         $this->validated = true;
      }
      $this->data['CreatedBy'] = $this->user->getid();
      //$this->data['CreatedOn'] = new Zend_Db_Expr('NOW()');
   }
   function setid($id) {
      $pk = $this->getpk();
      $this->data[$pk] = $id;
   } 
   function getid() {
      $pk = $this->getpk();
      $id = $this->data[$pk]; 
      $id = (is_object($id))?$id->Id:$id; 
      return $id;
   }
   function cascadedelete() {
      $pk = $this->getpk();
      $id = $this->getid();
      $ctab = $this->tablename;
      // 1st delete from any LTM column data  
      foreach($this->columns as $col => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
         } else $type = preg_replace('/\:.+/','',$type);
         if (isset($this->data[$col]) && $this->data[$col] == 'null') unset($this->data[$col]);
         switch ($type) {
         case 'LINKEDTOM': {
            if (isset($params)) {
               $ttab = $params->TargetType;
               $tcol = $params->TargetField;
               $p = (isset($params->Property))?$params->Property:$col;
            } else {
               preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
               $ttab = $matches[2];
               $tcol = $matches[3];
               $p = $col;
            }
            $ltmd[$col] = $this->data[$col];
            $ltm = new DBT_Lookup($p,$ctab,$pk,$ttab,$tcol);
            if (isset($id) && $id != '') $ltm->delete($pk,$id);
         }break;
         }
      }
      // 2nd delete from tables named in show list
      if (is_array($this->show)) {
         foreach($this->show as $tab) {
            $link = new $tab();
            list($icol,$tcol) = $link->getlinkedcolumn($tab);
            $val = ($tcol != $pk)?$this->data[$tcol]:$id;
            $link->delete($icol,$val);                                                             
         }                                                                                                          
      }
      
      if (isset($this->view)) {
         $v = (isset($this->view))?json_decode($this->view):null;
         foreach ($v as $i => $vi) {
            $show = $view->Data;
            foreach ($show as $data) {
               $tab = $data->Table;
               $link = new $tab();
               list($icol,$tcol) = $link->getlinkcolumn($tab);
               $val = ($tcol != $pk)?$this->data[$tcol]:$id;
               $link->delete($icol,$val);   
            }
         }
      }
   }
   function getpostprocess() {
      return (isset($this->postprocess))?json_decode($this->postprocess):null; 
   }
   function tohtml($data,$excludecols=array()) {
      $html = null;
      if (isset($data) && count($data)> 0) { 
         $trh = "<tr>";
         $trb = "";
         $defs = array();
         foreach($this->columns as $col => $def) {
            $defs[$col] = $this->getcolumndef($col);
         } 
         foreach($data as $rowid => $row) {
            $trb.= "<tr>";
            foreach($this->columns as $col => $def) {
               if (!in_array($col,$excludecols)) {
                  $def = $defs[$col];
                  $typ = $def->DataType;
                  $list = ($typ != 'Unique Key' && !isset($def->NoList));
                  if ($list) {
                     if ($rowid == 0) {
                        $colname = preg_replace('/^[A-Z,0-9]+_/','',$col);
                        $colname = preg_replace('/_ID$/','',$colname);
                        $colname = preg_replace('/\_/',' ',$colname);
                        $trh.= "<th class='$typ'>${colname}</th>";                  
                     }
                     $coldata = isset($row[$col])?$row[$col]:'';
                     if (isset($coldata)) {
                     
                        if (is_object($coldata)) $cdata = $coldata->Name;
                        else if (is_array($coldata)) $cdata = $coldata['Name']; 
                        else $cdata = urldecode($coldata);
                        $trb .= "<td class='$typ'>${cdata}</td>";
                     } else $trb .= "<td/>";
                  }
               }   
            }
            $trb.= "</tr>";         
         }
         $trh.= "</tr>";
         $html = "<table><thead>${trh}</thead><tbody>${trb}</tbody></table>";
      } 
      return $html;
   }  
   
   protected $gs_publishme = false;
   protected $gs_workspace = 'yisf';
   protected $gs_datastore = 'yisf';
   protected $gs_style = 'polygon';
   protected $gs_server = 'http://localhost:8080/geoserver';
   protected $gs_auth = 'admin:rw010647';
   protected $gs_authtype = 'basic';
   
   protected $gs_styles = array();
   function gs_publish() {
      $isws = false;
      $isds = false;
      $isft = false;
      $isst = false;
      if ($this->gs_publishme) {
         $isws = $this->gs_createworkspace();
         if ($isws) $isds = $this->gs_createdatastore();
         if ($isds) $isft = $this->gs_createfeaturetype(); 
         if ($isft) $isst = $this->gs_setstyle($this->tablename,$this->gs_style);
         //if ($isst) print "Workspace: $isws Datastore: $isds Feature Type: $isft";           
      }            
      return $isft;
   }
   function gs_getworkspaceurl($ws=null) {
      $url = '/workspaces';
      if (isset($ws)) $url .= '/' . $ws;
      return $url;
   }
   function gs_createworkspace() {
      $url = $this->gs_getworkspaceurl($this->gs_workspace);
      $res = $this->gs_json_get($url);
      $created = (is_object($res) && isset($res->workspace));
      if (!$created) {
         $pst = new stdClass();
         $pst->workspace = new stdClass();
         $pst->workspace->name   = $this->gs_workspace;
         $this->gs_json_post($this->gs_getworkspaceurl(),$pst);
         $res = $this->gs_json_get($url);
         $created = (is_object($res) && isset($res->workspace)); 
      }
      return $created;
   }
   function gs_getdatastoreurl($ds=null) {
      $url = $this->gs_getworkspaceurl($this->gs_workspace) . '/datastores';
      if (isset($ds)) $url .= '/' . $ds; 
      return $url;
   }
   function gs_createdatastore() {
      global $databases;
      $db = $databases['default'];
      $dbPlatform = $db->getplatform();
      $dbConfig   = $db->getconn();
      
      $url = $this->gs_getdatastoreurl($this->gs_datastore);
      $res = $this->gs_json_get($url);
      $created = (is_object($res) && isset($res->dataStore));
      if (!$created) {
         $pst = new stdClass();
         $pst->dataStore = new stdClass();
         $pst->dataStore->name   = $this->gs_datastore;
         $pst->dataStore->description = 'MySQL database';
         $pst->dataStore->type = 'MySQL';
         $pst->dataStore->enabled = true;
			$pst->dataStore->connectionParameters = new stdClass();
         $pst->dataStore->connectionParameters->entry = array();
         	$pst->dataStore->connectionParameters->entry[] = array('@key' => "port",'$' => "".$dbConfig['port']);
            $pst->dataStore->connectionParameters->entry[] = array('@key' => "Connection timeout", '$' => "20");
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "passwd", '$' => $dbConfig['password']);
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "dbtype", '$' => 'mysql');
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "validate connections", '$' => "true");
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "host", '$' => $dbConfig['host']);
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "max connections", '$' => "10");
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "database", '$' => $dbConfig['dbname']);
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "Expose primary keys", '$' => "false");
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "user", '$' => $dbConfig['username']);
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "min connections", '$' => "1");
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "storage engine", '$' => "MyISAM");
            $pst->dataStore->connectionParameters->entry[] = array('@key' => "fetch size", '$' => "1000");
         $res = $this->gs_json_post($this->gs_getdatastoreurl(),$pst);
         $res = $this->gs_json_get($url);
         $created = (is_object($res) && isset($res->dataStore)); 
      }
      return $created;
   }
   function gs_getfeaturetypeurl($ft=null) {
      $url = $this->gs_getdatastoreurl($this->gs_datastore) . '/featuretypes';
      if (isset($ft)) $url .= '/' . $ft;
      return $url;
   }
   function gs_createfeaturetype() {
      $proj = "GEOGCS[\"WGS 84\", \r\n  DATUM[\"World Geodetic System 1984\", \r\n    SPHEROID[\"WGS 84\", 6378137.0, 298.257223563, AUTHORITY[\"EPSG\",\"7030\"]], \r\n    AUTHORITY[\"EPSG\",\"6326\"]], \r\n  PRIMEM[\"Greenwich\", 0.0, AUTHORITY[\"EPSG\",\"8901\"]], \r\n  UNIT[\"degree\", 0.017453292519943295], \r\n  AXIS[\"Geodetic longitude\", EAST], \r\n  AXIS[\"Geodetic latitude\", NORTH], \r\n  AUTHORITY[\"EPSG\",\"4326\"]]";
      $ftype = strtolower($this->tablename);
      $url = $this->gs_getfeaturetypeurl($ftype);
      $res = $this->gs_json_get($url);
      $created = (is_object($res) && isset($res->featureType));
      if (!$created) {
         $pst = new stdClass();
         $pst->featureType = new stdClass();
         $pst->featureType->name   = $ftype;
         $pst->featureType->description = $this->displayname;
         
         // add default bounding box for empty tables; 
         $pst->featureType->nativeCRS = $proj;
         $pst->featureType->srs = "EPSG:4326";
         $pst->featureType->nativeBoundingBox = (object)array("minx"=>-2.56,"maxx"=>0.15,"miny"=>53.30,"maxy"=>54.57,"crs"=>"EPSG:4326");
         $pst->featureType->latLonBoundingBox = (object)array("minx"=>-2.56,"maxx"=>0.15,"miny"=>53.30,"maxy"=>54.57,"crs"=>$proj);
         $pst->featureType->projectionPolicy = "FORCE_DECLARED";
         
         $res = $this->gs_json_post($this->gs_getfeaturetypeurl(),$pst);
         $res = $this->gs_json_get($url);
         $created = (is_object($res) && isset($res->featureType));
      }
      return $created;
   }
   function gs_deletefeaturetype($ft) {
      $url = '/layers/'.$ft;
      $dellyr = $this->gs_json_del($url);
      $deltyp = false;
      if ($dellyr) {
         $url = '/workspaces/'.$this->gs_workspace.'/datastores/'.$this->gs_datastore.'/featuretypes/'.$ft;
         $deltyp = $this->gs_json_del($url);
      }
      return ($deltyp);
   }
   function gs_deleteallfeaturetypesinstore() {
      $url = '/workspaces/'.$this->gs_workspace.'/datastores/'.$this->gs_datastore.'/featuretypes';
      $res = $this->gs_json_get($url);
      if (isset($res->featureTypes) && isset($res->featureTypes->featureType)) {
         foreach($res->featureTypes->featureType as $num => $typ) {
            $ft = $typ->name;
            $this->gs_deletefeaturetype($ft);
         } 
         $url = '/reload';
      }
      $res = $this->gs_json_put($url);                      
   }
   function gs_createstyle() {
      
   }
   function gs_setstyle($ft,$st) {
      //*
      $ft = strtolower($ft);
      $url = '/layers/'.$ft.'/styles';
      $pst = new stdClass();
      $pst->style = new stdClass();
      $pst->style->name = $st;
      $pst->style->workspace = $this->gs_workspace;
      $res = $this->gs_json_post($url,$pst);
      //*/
      //*
      $url = '/layers/'.$this->gs_workspace.':'.$ft;
      $pst = new stdClass();
      $pst->layer = new stdClass();
      $pst->layer->defaultStyle = array('name'=>$st);
      $res = $this->gs_json_put($url,$pst);
      //*/
      $res = $this->gs_json_get($url);          
      return $res;
   }
   function gs_json_get($url) {
      $url = $this->gs_server . '/rest' . $url . '.json';
      $res = remoteRequest($url,true,$this->gs_auth,$this->gs_authtype);
      $res = $this->gs_json_read($res);
      return $res;
   }
   function gs_json_post($url,$pst=null) {
      $url = $this->gs_server . '/rest' . $url . '.json';
      $data = json_encode($pst);
      $res = post($url,$data,'application/json',$this->gs_auth,$this->gs_authtype);
      return $res;   
   }
   function gs_json_put($url,$pst=null) {
      $url = $this->gs_server . '/rest' . $url;
      $data = json_encode($pst);
      $res = putverb($url,$data,'application/json',$this->gs_auth,$this->gs_authtype);
      return $res;   
   }
   function gs_json_del($url) {
      $url = $this->gs_server . '/rest' . $url . '.json';
      $res = deleteverb($url,$this->gs_auth,$this->gs_authtype);
      $res = $this->gs_json_read($res);
      return $res;
   }
   function gs_json_read($res) {
      $jres = json_decode(stripslashes($res));
      if (json_last_error() == JSON_ERROR_NONE) $res = $jres;
      else if (preg_match('/^\{/',$res)) $res = json_decode($res);
      
      //else $res = json_last_error_msg();
      return $res;
   }
   function asjson() {
      $json = new stdClass();
      $json->IType = $this->tablename;
      $json->DisplayName = $this->getdisplayname();
      $json->Current = (isset($this->data[$this->getpk()]))?$this->getid():null;
      if ($json->Current) {
         $id = $this->getid();
         $json->Data = $this->getdata();
         $json->ItemName = $this->getname();
      }
      $linkdata = array();
      $show = array();
      $views = array();
      $iview = null;
      if (isset($this->view)) {
         $viewdata = json_decode($this->view);
         if (count($viewdata)>0) {
            foreach($viewdata as $view) {
               $views[] = $view->Name;
               $show = array_merge($show,$view->Data);
            }
         }
      } else if (!isset($show)&&!isset($this->view)) {
         $show = array_merge($show,$this->show);
      }
      foreach($show as $data) {
         if (is_object($data)) {
            $classname = $data->Table;
            $showopts = (isset($data->Show))?$data->Show:'All';
            $sectionname = (isset($data->Name))?$data->Name:null;            
         } else {
            // This is for if the show array replaces the view definition
            $classname = $data;
            if (preg_match('/([^:]+):(.+)/',$classname,$m)) $classname = $m[1];
            $showopts = 'All';
         }         
         $link = new $classname();
         if (isset($link)) $link->setuser($this->user);
         if (isset($link) && $link->isallowed($this->user,'List')) {
            if (!$link->exists()) $link->create();
            // see if link is in this table first
            list($scol,$tcol) = $this->getlinkedcolumn($classname);
            // then if it isn't see if it is in the linked table
            if (!isset($scol)) list($tcol,$scol) = $link->getlinkedcolumn($this->tablename);
            $val = $this->data[$scol];
            if (is_object($val)) $val = ($val->Current)?$val->Current:$val->Id;
            if (isset($val)) {
               $ld = new stdClass();
               $ld->IType = $classname;
               $ld->Interface = $link->getinterface();
               $ld->DisplayName = (isset($sectionname))?$sectionname:$link->getdisplayname();
               switch($showopts) {
               case 'All': {
                  $data = $link->select($tcol,$val);
                  $ld->ListData = $data;
                  if (count($data)>0) $linkdata[] = $ld;
               }break;
               }
            }
         } 
      }
      $json->LinkedData = $linkdata;
      return json_encode($json);
   }   
}
/* $select->reset( Zend_Db_Select::ORDER ); */
?>
