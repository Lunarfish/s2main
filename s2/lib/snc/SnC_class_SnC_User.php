<?php
include_once('SnC_class_SnC_Session.php');
/*
CREATE TABLE SnC_User (
   Snc_User_Key INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   UserName VARCHAR(100),
   SnCPassword VARCHAR(20),
   CreatedOn DATE NOT NULL 
);
*/
class SnC_User {
   private $table = 'SnC_User';
   private $Snc_User_Key;
   private $UserName;
   private $SnCPassword;
   private $CreatedOn;
   function setUserName($user) {
      $this->UserName = $user;
   }
   function getUserName() {
      return $this->UserName;
   }
   function setSnCPassword($pass) {
      $this->SnCPassword = $pass;
   }
   function getSnCPassword() {
      return $this->SnCPassword;
   }
   function login($session,$username,$password) {
      $this->UserName = $username;
      $this->retrieve();
      $status = ($password == $this->SnCPassword); 
      if ($status) {
         $session = new SnC_Session($session);
         $status = $session->setUser($this->Snc_User_Key);         
      }
      return $this->Snc_User_Key;   
   }
   function logout($session) {
      $session = new SnC_Session($session);
      $status = $session->setUser('');         
      return $status;   
   }
   function retrieve() {
      $dbh = $GLOBALS['dbh'];
      $query = $dbh->select();
      $query->from($this->table,
            array("Snc_User_Key" => "Snc_User_Key",
                  "SnCPassword"  => "SnCPassword",
                  "CreatedOn"    => "CreatedOn"));
      $query->where('UserName = ?',$this->UserName);
      $rows = $dbh->fetchAll($query);
      for ($i=0;$i<count($rows);$i++) {
         $record = $rows[$i];
         $this->Snc_User_Key  = $record["Snc_User_Key"];
         $this->SnCPassword   = $record["SnCPassword"];
         $this->CreatedOn     = $record["CreatedOn"];
      }      
   }
   function register($session,$username,$password) {
      $dbh = $GLOBALS['dbh'];
      $data = array(
         'UserName'     => $username,
         'SnCPassword'  => $password,
         'CreatedOn'    => new Zend_Db_Expr('CURRENT_DATE')
      );
      $status = $dbh->insert($this->table, $data);
      if ($status) {
         $this->Snc_User_Key = $dbh->lastInsertId();
         $session = new SnC_Session($session);
         $status = $session->setUser($this->Snc_User_Key);         
      }
      return $this->Snc_User_Key;      
   }
   function update($session,$field,$value) {
      $dbh = $GLOBALS['dbh'];
      $data = array($field => $value);
      return $dbh->update($this->table, $data, 'Snc_User_Key = '.$this->Snc_User_Key);               
   }
}
?>