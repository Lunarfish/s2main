<?php
// ===============================================================
// S&C Sites & Citations - Recorder connection library
// ---------------------------------------------------------------
// Written by YHEDN (Dan Jones)
// ===============================================================
// Retrieves data from Recorder6 database via a PDO MSSQL connection 
// and returns the data as JSON to be interpreted by the 
ini_set('include_path',ini_get('include_path').';./lib/;');
include_once 'Zend/Db.php';
include_once 'SnCDatabaseConnect.php';

function SnC_getR6DatabaseHandle() {
   if ($GLOBALS['dbh'] == null) { 
      $r6 = new SnCRecorder6_Database();
   }
}

class SnCRecorder6_Database {
   public $cprofile;
   public $sites;
   private $config;
   function SnCRecorder6_Database() {
      $this->getConnectionProfile();
      $this->connect();
   }
   function getConnectionProfile() {
      $db = SnC_getDatabaseConnection();
      $query = $db->select();
      $query->from(  'SnCRecorder6_ConnectionProfile',
			            array('ODBC_DSN',
                           'ServerName',
                           'ServerPort',
                           'DatabaseName',
                           'DBUser',
                           'DBPassword'));
	   $records = $db->fetchAll($query);
      for($i=0;$i<count($records);$i++) {
         $record = $records[$i];
         foreach($record as $key => $value) {
            unset($record[$key]);
            $key = preg_replace("/\"SnCRecorder6_ConnectionProfile\"\.\"(\S+)\"/", "$1", $key);
            $record[$key] = $value;
         }
      }
      $records[$i] = $record;
      $this->config = array(  'host'      => $record['ServerName'],
                              'port'      => $record['ServerPort'],
                              'username'  => $record['DBUser'],
                              'password'  => $record['DBPassword'], 
                              'dbname'	   => $record['DatabaseName']);
      //print_r($this->config);
   }
   function connect() {
      $GLOBALS['dbh'] = Zend_Db::factory('Pdo_Mssql', $this->config);
      /*
      //
      // demo code to update the last key for a table and return the last key 
      // value into a variable which you can then use for an insert statement 
      // to that table. 
      // ideally this will be used to mirror any changes from the 
      // Sites and Citations database into linked Recorder Locations
      // 
      $dbh = $GLOBALS['dbh'];
      $select = $dbh->select();
      $select->from(array('S' => 'SETTING'),array( 'NAME' => 'NAME', 'DATA' => 'DATA'));
      $settingstable = $dbh->fetchAll($select);
      $settings = array();
      foreach ($settingstable as $i => $row) { $settings[$row['NAME']] = $row['DATA']; }
      $table = 'ADMIN_AREA';
      $siteid = $settings['SiteID'];
      $newkey = '';
      print "Table $table Site $siteid<br/>";
      $response = $dbh->query("DECLARE @newkey CHAR(16)
      EXECUTE spNextKey '$table',@newkey OUTPUT,'$siteid' 
      SELECT @newkey")->fetchAll();
      $newkey = $response[0]['computed'];
      print "Key: $newkey";
      */
   }
   // Within Recorder sites are often designated as Candidate LWS before becoming 
   // Designated LWS and then hopefully not Deleted LWS so the string is a pattern 
   // to indicate that the search is done for *pattern* rather than explicitly 
   // that string. 
   function retrieveSitesForPattern($dpattern, $adminarea) {
      $sites = array();
      $dbh = $GLOBALS['dbh'];
      $select = $dbh->select();
      $select->from(array('D' => 'LOCATION_DESIGNATION'),
                    array( 'LocationKey' => 'LOCATION_KEY', 
                           'SiteCode' => 'REF_CODE',
                           'DateFrom' => 'DATE_FROM',
                           'DateTo' => 'DATE_TO'));
      $select->join(array('N' => 'LOCATION_NAME'),
                    'N.LOCATION_KEY = D.LOCATION_KEY',
                    array( 'SiteName' => 'N.ITEM_NAME') );
	   $select->join(array('S' => 'SITE_STATUS'),
                    'S.SITE_STATUS_KEY = D.SITE_STATUS_KEY',
                    array( 'Designation' => 'S.LONG_NAME') );
      $select->join(array('LA' => 'LOCATION_ADMIN_AREAS'),
                    'LA.LOCATION_KEY = D.LOCATION_KEY',
                    array() );
      $select->join(array('A' => 'ADMIN_AREA'),
                    'LA.ADMIN_AREA_KEY = A.ADMIN_AREA_KEY',
                    array() );
      $select->where("N.PREFERRED <> ?", 0);
      $select->where("S.LONG_NAME LIKE ?", "%$dpattern%");
      $select->where("A.ITEM_NAME LIKE ?", "$adminarea%");
      $select->order('SiteName ASC');
      $sites = $dbh->fetchAll($select);
	   foreach ($sites as $i => $site) {
         if (preg_match('/Candidate/',$site['Designation'])) {
            $site['Designation'] = $dpattern;
            $site['Status'] = 'Candidate';
         } else if (preg_match('/Deleted/',$site['Designation'])) {
            $site['Designation'] = $dpattern;
            $site['Status'] = 'Deleted';            
         } else {
            $site['Designation'] = $dpattern;
            $site['Status'] = 'Designated';
         }
         if ($site['DateFrom']) {
            $t = strtotime($site['DateFrom']);
            $d = gmdate('Y-m-d', $t);
            $site['DateFrom'] = $d;
         }
         if ($site['DateTo']) {
            $t = strtotime($site['DateTo']);
            $d = gmdate('Y-m-d', $t);
            $site['DateTo'] = $d;
         }
         $sites[$i] = $site; 
      }
      $this->sites = $sites;
      return $sites;                                             
   }
}
class SnCRecorder6_Location {
   public $location_key;
   public $sitename;
   public $sitecode;
   public $events;
   public $designations;
   public $ownership;
   public $descriptions;
   public $features;
   public $habitats;
   function retrieveEvents() {
      $e = new SnCRecorder6_SurveyEvent($this->location_key);      
      $this->events = $e->retrieveEvents();
   }
   function retrieveDesignations() {
   
   }
   function retrieveOwnership() {
   
   }
   function retrieveDescriptions() {
   
   }
   function retrieveFeatures() {
   
   }
   function retrieveHabitats() {
   
   }
}
class SnCRecorder6_SurveyEvent {
   public $location_key;
   function SnCRecorder6_SurveyEvent($lkey) {
      $this->location_key = $lkey;   
   }
   function retrieveEvents() {
      $events = array();
         
      return $events;
   }
}
class SnCRecorder6_LocationDesignation {
   public $location_key;

}
class SnCRecorder6_LocationOwnership {
   public $location_key;

}
class SnCRecorder6_LocationDescription {
   public $location_key;

}
class SnCRecorder6_LocationFeature {
   public $location_key;
   
}
class SnCRecorder6_LocationHabitat {
   public $location_key;
   
}
?>