<?php
   function createbuffers() {
      global $epsg_native;
      $step = 2;
      $proc = $this->getprocessor();
      $proc->completestep($step-1);
      $ospc  = "ODS_Search_Profile";
      $osdc  = "ODS_SDef";
      $ospbc = "ODS_SP_Buffer";
      $osbc  = "ODS_Search_Buffer"; 
      $poly = $this->data['Boundary'];
      $success = true;
      if (isset($poly)) {
         $osd = new $osdc();
         $ospb = new $ospbc();
         list($scol,$tcol) = $this->getlinkedcolumn($osdc);
         $sdid = $this->data[$scol]; 
         $osd->loaddata($sdid);
         $ospb = new $ospbc();
         list($scol,$tcol) = $ospb->getlinkedcolumn($osdc);
         $pbuffers = $ospb->select($scol,$sdid);
         $ssteps = count($pbuffers);
         $scompleted = 0;
         $pstep = $proc->createstep('Create Buffers',$step,$ssteps);
         $psid = $pstep->getid();
         $proc->setstep($psid);
         foreach($pbuffers as $bufnum => $buf) {
            $bsize = $buf['Buffer_Size'];
            $snum = $buf['Number'];
            $this->createbuffer($poly,$bsize,$snum)
            $scompleted++;
            $pstep->completestep($completed);
         }
         $pstep->complete();
      }
      $proc->completestep($step);
      return 1;
   }
   function createbuffer($poly,$bsize,$snum) {
      global $epsg_native;
      $osbc  = "ODS_Search_Buffer"; 
      if ($bsize > 0) {
         $expr = new Zend_Db_Expr("st_astext(st_transform(st_buffer(st_transform(st_geomfromtext('${poly}',4326),${epsg_native}),${bsize}),4326))");
         $sel = "SELECT $expr AS buffer";
         //$buffer = $this->dbhandle->fetchOne($sel);
         $res = pg_query($this->dbpghandle,$sel);
         $res = pg_fetch_array($res);
         $buffer = $res['buffer'];
      } else $buffer = $poly;
      $osb = new $osbc();
      $data = array(
         'ODS_Search_ID'                  => $this->getid(),
         'Buffer_Size'                    => $bsize,
         'Buffer'                         => $buffer,
         'Number'                         => $snum   
      );
      $osb->setdata($data);
      $osb->insert();             
   }
   function getlargestbuffer() {
      $osbc = "ODS_Search_Buffer";
      $osb = new $osbc();
      $buffers   = $osb->select($this->getpk(),$this->getid());
      // Start with un-buffered search area
      $boundary = $this->data['Boundary'];
      $number = 0;
      $buffer = 0;
      // Set boundary to largest buffer
      foreach($buffers as $bufnum => $buf) {
         $bufid      = $buf[$osb->getpk()];
         $bsize      = $buf['Buffer_Size'];
         if ($bsize > $buffer) {
            $buffer = $bsize;
            $boundary   = $buf['Buffer'];
            $number     = $buf['Number'];
         }
      }
      return array($boundary,$number);
   }
   function getdaterange() {
      $osplc = "ODS_SP_Limit";
      $ospl = new $osplc();
      list($scol,$tcol) = $ospl->getlinkedcolumn($osdc);
      $sdid = $this->data[$scol]; 
      $ospl->loaddata($sdid,$scol);
      $ldata = $ospl->getdata();
      $limit = $ldata['Last_X_Years'];
      $now = gmmktime();
      $ny = intval(date("Y",$now));
      $nm = intval(date("m",$now));
      $nd = intval(date("d",$now));
      $data_to = date("d/m/Y",$now);
      $data_from = date("d/m/Y",mktime(0,0,0,1,1,($ny-$limit)));   
      return array($data_from,$data_to);
   }
   function retrievespeciesdata() {
      global $configsettings,$server,$client,$folder;
      $step = 3;
      $proc = $this->getprocessor();
      $proc->completestep($step-1);
      $sid = $this->getid();
      $ordc  = "ODS_Raw_Data";
      $ospc  = "ODS_Search_Profile";
      $osdc  = "ODS_SDef";
      $ospsc = "ODS_SP_Source";
      // before retrieving the raw data 
      $rdo = new $ordc();
      $rdo->delete($this->getpk(),$this->getid());
   
      $osd  = new $osdc();
      $osps = new $ospsc();
      list($scol,$tcol) = $this->getlinkedcolumn($osdc);
      $sdid = $this->data[$scol]; 
      $osd->loaddata($sdid);
      $ddata = $osd->getdata();
      $sources = $ddata['Sources']->ListData;
      $ssteps = count($sources);
      $pstep = $proc->createstep('Retrieve Species Data',$step,$ssteps+2);
      list($data_from,$data_to) = $this->getdaterange();
      $pstep->completestep(1,'Set Date Limits');      
      list($boundary,$number) = $this->getlargestbuffer();
      $pstep->completestep(2,'Set Search Boundary');      
      $scompleted = 2;
   
      foreach($sources as $snum => $src) {
         $this->getspeciessourcedata($src,$boundary,$data_from,$data_to);
         $scompleted++;
         $pstep->completestep($scompleted);                              
      }
      
      $pstep->complete();      
      $proc->completestep($step);
      return 1;         
   }
   function getspeciessourcedata($src,$boundary,$data_from,$data_to) {
      $ordc  = "ODS_Raw_Data";
      $ospc  = "ODS_Search_Profile";
      $osdc  = "ODS_SDef";
      $ospsc = "ODS_SP_Source";
      // before retrieving the raw data 
      $rdo = new $ordc();
      $osd  = new $osdc();
      $osps = new $ospsc();
      
      $osps->loaddata($src->Id);
      $srcdata = $osps->getdata();
      $source = $srcdata['Type'];
      $creds = $configsettings[$source]; 
               
      $params     = array(
         "Action"       => "GetSpeciesList",
         "Source"       => $source,
         "Boundary"     => $boundary,
         "Start_Date"   => $data_from,
         "End_Date"     => $data_to,
         "Sections"     => '[{"Name":"Records","Output_Type":"Records"}]'
      );
      foreach($creds as $i => $p) $params["CR".($i+1)] = $p;
      $svr = "${server}/r6";
      $tgt = "/species2json.php";
      $cres = $this->sendrequest($svr,$tgt,$params);
      if (isset($cres)) {
         $fname = "ODS_Search_${sid}_${source}_SpeciesData.json";
         $type = 'text/json';
         $rdid = $this->saveresponse($fname,$type,$cres);
      }
      return 1;
   }
   function processspeciesdata() {
      global $epsg_native;
      $step = 4;
      $proc = $this->getprocessor();
      $proc->completestep($step-1);
      chdir('..');
      $rdo = new ODS_Raw_Data();
      
      $rawdatas = $rdo->select($this->getpk(),$this->getid());
      $ssteps = count($rawdatas);
      $scompleted = 0;
      $pstep = $proc->createstep('Process Species Data',$step,$ssteps);
      $psid = $pstep->getid();
      foreach($rawdatas as $rnum => $rdata) {
         $pstepcur = $scompleted + 1;
         $this->processspeciessource($rdata,$pstepcur,$psid);
         $scompleted++;
         $pstep->completestep($scompleted,'Processed source: '.($rnum+1));
      }
      $rdo->delete($this->getpk(),$this->getid());
      
      $pstep->complete();
      $proc->completestep($step);
      return 1;
   }
   function processspeciessource($rdata,$pstepcur,$psid) {
      $src = "ODS_Search_Record";
      $srco = new $src();
      
      $fid = $rdata['S2_File_ID'];
      $fid = (is_object($fid))?$fid->Id:$fid;
      $f = new S2_File();
      $f->loaddata($fid);
      $fdata = $f->getdata();
      $path = $fdata['Data'];
      $cres = s2_getfile($path);
      $jres = json_decode($cres);
      if (!$srco->exists()) $srco->create();
      if (isset($jres->Tables) && isset($jres->Tables->Records)) {
         $records = count($jres->Tables->Records);
         $ro = null;
         $tenpcent = (intval($records/10)>0)?intval($records/10):1;
         $istepcur = 0;
         $istep = $proc->createstep("Source $pstepcur Records",$pstepcur,10,$psid);
         $recnum = 0;
         foreach ($jres->Tables->Records as $record) {
            $rid = $this->addrecord($record);
            // update at 10% marker
            $recnum++;
            $istepcur = intval($recnum/$tenpcent);
            if (($istepcur > 0) && (($recnum%$tenpcent)==0)) $istep->completestep($istepcur,"Processed $recnum out of $records records.");
         }
         $istep->complete();
      }
      $f->delete($f->getpk(),$fid);
      return 1;   
   }
   function addrecord($record) {
      global $epsg_native;
      $name = $record->scientificname;
      $tvk = $record->gatewaykey;
      $jo = new stdClass();
      $jo->Name = $name;
      $jo->Value = $tvk;
      $tid = json_encode($jo);
      $wkt = null;
      $ngr = new NationalGridReference();
      $ngr->fromString($record->spatialref);
      $gr = conv_ngr_to_ings($ngr);
      $bb = new BoundingBox(
         ($gr->northing + $gr->accuracy),
         ($gr->easting + $gr->accuracy),
         $gr->northing,
         $gr->easting
      );
      $oswkt = $bb->asWKT();
      $expr = new Zend_Db_Expr("st_astext(st_transform(st_geomfromtext('${oswkt}',${epsg_native}),4326))");
      $sel = "SELECT $expr AS buffer";
      $wkt = $this->dbhandle->fetchOne($sel);
      $ro = array(
         'ODS_Search_ID'         => $this->getid(),
         'G_Species_ID'          => $tid,
         'Place_Name'            => $record->location,
         'Grid_Reference'        => $record->spatialref,
         'Boundary'              => $wkt,
         'Recorder'              => $record->recordedby,
         'Start_Date'            => $record->sdate,
         'End_Date'              => $record->edate,
         'Comments'              => ((isset($record->comments))?$record->comments:null),
         'Details'               => ((isset($record->measurement))?"$record->measurement $record->measure":null),
         'Source'                => $record->custodian,
         'Survey'                => urlencode($record->survey)
      );
      $srco = new ODS_Search_Record();
      $srco->setdata($ro);
      $srco->insert();         
      return $srco->getid();
   }
   function filterspeciesdata() {
//error_reporting(E_ALL);
      $step = 5;
      $proc = $this->getprocessor();
      $proc->completestep($step-1);
      $bc  = "ODS_Search_Buffer";
      $pc  = "ODS_Search_Profile";
      $dc  = "ODS_SDef";
      $psc = "ODS_SpP_Section";
      $pbc = "ODS_SP_Buffer";
      $fc  = "ODS_Species_Filter";
      $oc  = "ODS_Species_Output";
      $otc = "ODS_SpO_Type";
      $rc  = "ODS_Search_Record";
      $b   = new $bc();
      $d   = new $dc();
      $ps  = new $psc();
      $pb  = new $pbc();
      $f   = new $fc();
      $o   = new $oc();
      $ot  = new $otc();
      $r   = new $rc();
      $dlnk = $this->getlinkedcolumn($dc);
      $sections = $ps->select($dlnk[1],$this->data[$dlnk[0]]);
//print_r($sections);
      $blnk = $b->getlinkedcolumn($this->tablename);
      $buffers = $b->select($blnk[0],$this->data[$blnk[1]]);
//print_r($buffers);
      $ssteps = count($sections);
      $scompleted = 0;
      $pstep = $proc->createstep('Filter Species Data',$step,$ssteps);
   
      foreach($sections as $secnum => $psd) {
         $pstepcur = $scompleted + 1;
         $psid   = $psd[$ps->getpk()];
         $return = $psd['Return'];
         $blnk = $ps->getlinkedcolumn($pbc);
         $boundary = $this->data['Boundary'];
         $pbid = (isset($psd[$blnk[0]])
                  &&isset($psd[$blnk[0]]->Id))
                  ?$psd[$blnk[0]]->Id:0;
         if ($pbid > 0) {
            $pb->loaddata($pbid);
            $pbd = $pb->getdata();
            $bsize = $pbd['Buffer_Size'];
            $islargest = true;
            foreach($buffers as $bx) {
               if ($bx['Buffer_Size'] == $bsize) {
                  $bd = $bx;
                  $boundary = $bd['Buffer'];
               } else if ($bx['Buffer_Size'] > $bsize) $islargest = false;              
            }
         }
         
         $srecords = $r->select($this->getpk(),$this->getid());
         $istep = $proc->createstep("Section $pstepcur Filtering Records",1,4,$pstep->getid());
         $reccount = count($srecords);   
         if ($reccount>0) {
            if (!$islargest) $orecords = $this->spatialfilter($boundary,$srecords,$istep->getid());
            else $orecords = $srecords;
         }
         $istep->completestep(1,'Completed Spatial Filter');
         if (count($orecords) > 0) {
            $sectionlists = array();
            $flnk = $f->getlinkedcolumn($psc);
            $filters = $f->select($flnk[0],$psid);
            foreach($filters as $fi => $filter) {
               $list = $filter['List'];
               $listname = $list['Name'];
               $lst = $f->getlist($list);
               $lmem = $lst->LinkedData[0];
               $lmembers = $lmem->ListData;
               $ldata = $lst->Data;
               $lst = new stdClass();
               $lst->Cache = $list;
               $lst->Data = $ldata;
               $lst->Members = $lmembers;
               $gcids = array();
               foreach($lmembers as $lx => $lmember) {
                  $gcid = $lmember->G_Species_ID->Id;
                  if (!in_array($gcid,$gcids)) $gcids[] = $gcid; 
               }
               $lst->IDs = $gcids;
               $sectionlists[$listname] = $lst;
            }
            $species = array();
            $speciesrcount = array();
            $ismemberof = array();
            $firstyears = array();
            $lastyears = array();
            $lists = array();
            $listspecies = array();
            $listscount = array();
            $listrcount = array();
            $listscores = array();
            $rle = new ODS_Record_List_Entry();
            if (!$rle->exists())$rle->create();
            $reccount = count($orecords);
            $tenpcent = (intval($reccount/10)>0)?intval($reccount/10):1;
            $iistepcur = 0;
            $iistep = $proc->createstep("Section $pstepcur List Filter",2,10,$istep->getid());
            $rnum = 0;
            foreach($orecords as $rid => $rec) {
               $rid = $rec[$r->getpk()];
               $gcid = intval($rec['G_Species_ID']->Id);
               // update record count for species
               if (!isset($species[$gcid])) {
                  $species[$gcid] = $rec['G_Species_ID'];
                  $speciesrcount[$gcid] = 1;
               } else $speciesrcount[$gcid]++;     
               // update recorded date range for species
               $year = intval(substr($rec['Start_Date'],-4));
               if (!isset($firstyears[$gcid])) $firstyears[$gcid] = $year;
               else $firstyears[$gcid] = min($year,$firstyears[$gcid]); 
               if (!isset($lastyears[$gcid])) $lastyears[$gcid] = $year;
               else $lastyears[$gcid] = max($year,$lastyears[$gcid]); 
               // update list membership data
               foreach($sectionlists as $listname => $lst) {
                  $gcids  = $lst->IDs;
                  if (in_array($gcid,$gcids)) {
                     $ldata  = $lst->Data;
                     $labbr  = $ldata->Abbreviation;
                     $lcache = $lst->Cache;
                     $lists[$listname] = $lst->Cache; 
                     // initialise ismemberof array for species if it doesn't exist
                     if (!isset($ismemberof[$gcid])) $ismemberof[$gcid] = array();
                     if (!in_array($labbr,$ismemberof[$gcid])) $ismemberof[$gcid][] = $labbr;
                     // initialise list counting if first list member
                     if (!isset($listspecies[$listname])) {
                        $listspecies[$listname] = array();
                        $listscount[$listname] = 0;
                        $listrcount[$listname] = 0;
                        $listscores[$listname] = 0;
                     }
                     // check whether species has been recorded for list already
                     if (!in_array($gcid,$listspecies[$listname])) {
                        $listspecies[$listname][] = $gcid;
                        $listscount[$listname]++;
                        $score = 1;
                        foreach($lst->Members as $mid => $lmem) {
                           if($lmem->G_Species_ID->Id == $gcid) $score = (isset($lmem->Score))?$lmem->Score:1;
                        }
                        $listscores[$listname] += $score;
                     }
                     $listrcount[$listname]++;
                  }                                                           
               }
               // add to records list depending on filter settings
               if ($return == 'All Species' || (isset($ismemberof[$gcid]) && count($ismemberof[$gcid]) > 0)) {
                  $memberof = ($return == 'All Species')?null:join(', ',$ismemberof[$gcid]);
                  $data = array(
                     'ODS_Search_ID'                  => $this->getid(),
                     'ODS_SpP_Section_ID' => $psid,
                     'ODS_Search_Record_ID'           => $rid, 
                     'Member_Of'                      => $memberof 
                  );
                  $rle->setdata($data);
                  $rle->insert(); 
               }
               $rnum++;
               $iistepcur = intval($rnum/$tenpcent);
               if (($iistepcur > 0) && (($rnum%$tenpcent)==0)) $iistep->completestep($iistepcur,"Processed $rnum of $reccount records.");                          
            }
            $iistep->complete();
            $istep->completestep(2,'Created Record List');
            $numlists = count($lists);
            $iistep = $proc->createstep('Create Summary Data',3,$numlists,$istep->getid());
            // add summary data
            if ($numlists>0) {
               $sse = new ODS_Species_Summary_Entry();
               if (!$sse->exists())$sse->create();
               $listnum = 0;
               foreach ($lists as $listname => $lc) {
                  $scount = $listscount[$listname];
                  $rcount = $listrcount[$listname];
                  $score  = $listscores[$listname];
                  $data = array(
                     'ODS_Search_ID'      => $this->getid(),
                     'ODS_SpP_Section_ID' => $psid,
                     'List'               => $lc['G_Cache_ID'],
                     'Species_Count'      => $scount,
                     'Record_Count'       => $rcount,
                     'Score'              => $score 
                  );
                  $sse->setdata($data);
                  $sse->insert();
                  $listnum++;
                  $iistep->completestep($listnum,"Summarized $listnum of $numlists lists.");
               }
            }
            $iistep->complete();
            $istep->completestep(3,'Created Summary Data');
            // add species list data
            $sle = new ODS_Species_List_Entry();
            if (!$sle->exists())$sle->create();
            $numspecies = count($species);
            $tenpcent = (intval($numspecies/10)>0)?intval($numspecies/10):1;
            $iistep = $proc->createstep('Create Species List',4,10,$istep->getid());
            $snum = 0;
            foreach ($species as $sdata) {
               $gcid = $sdata->Id;
               if ($return == 'All Species' || (isset($ismemberof[$gcid]) && count($ismemberof[$gcid]) > 0)) {
                  $fyear  = $firstyears[$gcid];
                  $lyear  = $lastyears[$gcid];
                  $rcount = $speciesrcount[$gcid];
                  $memberof = (!isset($ismemberof[$gcid]))?null:join(', ',$ismemberof[$gcid]);
                  $data = array(
                     'ODS_Search_ID'      => $this->getid(),
                     'ODS_SpP_Section_ID' => $psid,
                     'G_Species_ID'       => $gcid,
                     'First_Year'         => $fyear,
                     'Last_Year'          => $lyear,
                     'Record_Count'       => $rcount,
                     'Member_Of'          => $memberof    
                  );
                  $sle->setdata($data);
                  $sle->insert();
               } 
               $snum++;
               $iistepcur = intval($snum/$tenpcent);
               if (($iistepcur > 0) && (($snum%$tenpcent)==0)) $iistep->completestep($iistepcur,"Processed $snum of $numspecies species.");
            }
            $iistep->complete();
         }
         $scompleted++;
         $istep->complete();
         $pstep->completestep($scompleted);                      
      }
      
      $pstep->complete();
      $proc->completestep($step);
      return 1;
   }
   function spatialfilter($boundary,$srecords,$parentstepid) {
      $proc = $this->getprocessor();
      $reccount = count($srecords);
      $tenpcent = (intval(count($srecords)/10)>0)?intval(count($srecords)/10):1;
      $iistep = $proc->createstep("Section $pstepcur Spatial Filter",1,10,$parentstepid);
      $orecords = array();
      foreach($srecords as $rid => $rec) {
         $poly = $rec['Boundary'];
         $expr = new Zend_Db_Expr("st_intersects(st_geomfromtext('$poly',4326),st_geomfromtext('$boundary',4326))");
         //$expr = $expr->__toString();
         $sel = "SELECT $expr AS isin";
         //$intersects = $this->dbhandle->fetchOne($sel);
         $res = pg_query($this->dbpghandle,$sel);
         $res = pg_fetch_array($res);
         // pg returns t or f as strings rather than 1 or 0;
         $intersects = ($res['isin'] == 't');
         if ($intersects) $orecords[] = $rec;   
         $iistepcur = intval($rid/$tenpcent);
         if (($iistepcur > 0) && (($rid%$tenpcent)==0)) $iistep->completestep($iistepcur,"Processed $rid out of $reccount records.");         
      }
      $iistep->complete();
      return $orecords;
   }
   function producespeciesreport() {
      global $folder;
      $step = 6;
      $proc = $this->getprocessor();
      $proc->completestep($step-1);
      $pc   = "ODS_Search_Profile";
      $psc  = "ODS_SpP_Section";
      $dc   = "ODS_SDef";
      $oc   = "ODS_Species_Output";
      $otc  = "ODS_SpO_Type";
      $rc   = "ODS_Search_Record";
      $ssec = "ODS_Species_Summary_Entry";
      $slec = "ODS_Species_List_Entry";
      $rlec = "ODS_Record_List_Entry";
      $soc  = "ODS_Search_Output";
      
      $sc = $this->tablename;
      $set = new stdClass();
      $sid = new stdClass();
      $sid->Current = $this->getid();
      $set->$sc = $sid; 
      
      $ps   = new $psc();
      $d    = new $dc();
      $o    = new $oc();
      $ot   = new $otc();
      $r    = new $rc();
      $sse  = new $ssec();
      $sle  = new $slec();
      $rle  = new $rlec();
      $dlnk = $this->getlinkedcolumn($dc);
      $sections = $ps->select($dlnk[1],$this->data[$dlnk[0]]);
//print_r($sections);
      $sname = $this->getname();
      $date = date("d/m/Y",time());
      $html = "<div id='s2rep_data-container'><h1 class='s2rep_search-name'>Search: ${sname} ~ Run:${date}</h1><hr/><div id='s2rep_data'>";
      
      $ssteps = count($sections);
      $scompleted = 0;
      $pstep = $proc->createstep('Produce Species Report',$step,$ssteps);
      
      foreach($sections as $secnum => $psd) {
//print_r($psd);
         $psid    = $psd[$ps->getpk()];
         $secname = $psd['Name'];
         $psecnum = $secnum + 1;
         $html   .= "<h2>Section ${psecnum}: ${secname}</h2><div id='s2rep_sec-${secnum}' class='s2rep_section-container'>";  
         $return  = $psd['Return'];
         
         $fc  = "ODS_Species_Filter";
         $f = new $fc();
         $flnk = $f->getlinkedcolumn($psc);
         $filters = $f->select($flnk[0],$psid);
         $ex = (!count($filters)>0)?array('Member_Of'):array(); 
               
         $olnk    = $o->getlinkedcolumn($psc);
         $otlnk   = $o->getlinkedcolumn($otc);
         $outputs = $o->select($olnk[0],$psid);
//print_r($outputs);         
         foreach($outputs as $oind => $output) {
            $otype = $output[$otlnk[0]]->Name;
            $expand = ($output['Is_Expanded'])?'expand':'collapse';
            switch($otype) {
            case 'Summary': {
               $lnk  = $sse->getlinkedcolumn($psc);
               $rows = $sse->select($lnk[0],$psid,$set);
               if (count($rows)>0) $ohtm = $sse->tohtml($rows);                                       
            }break;
            case 'Species List': {
               $lnk  = $sle->getlinkedcolumn($psc);
               $rows = $sle->select($lnk[0],$psid,$set);
               if (count($rows)>0) $ohtm = $sle->tohtml($rows,$ex);
            }break;
            case 'Record List': {
               $lnk  = $rle->getlinkedcolumn($psc);
               $rows = $rle->select($lnk[0],$psid,$set);
               foreach($rows as $rleid => $rlentry) {
                  $rlnk = $rle->getlinkedcolumn($rc);
                  $rid = $rlentry[$rlnk[0]]->Id; 
                  $r->loaddata($rid,$rlnk[1]);
                  $rdata = $r->getdata();
                  $rdata['Member_Of'] = $rlentry['Member_Of'];
                  $rows[$rleid] = $rdata;
               }
               if (count($rows)>0) $ohtm = $r->tohtml($rows,$ex);
            }break;
            }
            $html .= "<h3>$otype</h3><div id='s2rep_sec-${secnum}_out-${oind}' class='s2_collapsible,${expand}'>";
            if (!isset($ohtm)) $html .= "<p>No data was found to meet section requirements</p>";  
            else $html .= $ohtm;
            $html .= "</div>";                                                            
         }
         $html.= "</div>";
         $scompleted++;
         $pstep->completestep($scompleted);                                  
      }
      $html.= "</div></div>";
      
      chdir('..');
      $sid = $this->getid();
      $f = new S2_File();
      if (!$f->exists()) $f->create();
      $d = new stdClass();
      $d->Name = "ODS_Search_${sid}.html";
      $d->Type = "text/html"; 
      $d->Data = $html;
      $d = s2_savefile($d);
      $f->setdata((array)$d);
      $fid = $f->insert();
      $url = 'http://'.$_SERVER['SERVER_NAME']."/${folder}/echofile.php?a=echo&i=$fid";
      $d->Link = $url;
      $f->setdata((array)$d);
      $f->update($f->getpk(),$fid);
      
      $so = new $soc();
      $count = $so->getcount($this->getid());
      if (!$so->exists()) $so->create();
      $d = array(
         $this->getpk() => $this->getid(),
         $f->getpk()    => $fid,
         'Number'       => ($count+1)
      );
      $so->setdata($d);
      $name = $so->getname();
      $f->setdata(array('Name'=>$name));
      $f->update($f->getpk(),$fid);
      $soid = $so->insert();
      $url = 'http://'.$_SERVER['SERVER_NAME']."/${folder}/report?t=$soc&o=$soid";
      $d['Link'] = $url;
      $so->setdata((array)$d);
      $so->update($so->getpk(),$soid);
      $status = ($soid)?1:0;
      
      $r->delete($this->getpk(),$this->getid());
      $sse->delete($this->getpk(),$this->getid()); 
      $sle->delete($this->getpk(),$this->getid());
      $rle->delete($this->getpk(),$this->getid());
      
      $pstep->complete();
      $proc->completestep($step);
      return $status;
   } 
   function retrievesitedata() {
      global $configsettings,$server,$client,$folder;
      $step = 7;
      $proc = $this->getprocessor();
      $proc->completestep($step-1);
      
      $sid = $this->getid();
      
      $k = new KLib($this->getsid());
      $keys = json_decode($k->getkeys());
            
      
      $bc  = "ODS_Search_Buffer";
      $pc  = "ODS_Search_Profile";
      $dc  = "ODS_SDef";
      $psc = "ODS_SiP_Section";
      $pbc = "ODS_SP_Buffer";
      $ltc = "ODS_Layer_Type";
      $lcc = "ODS_Layer_Catalogue";
      $lac = "ODS_Layer_Attributes";
      $slc = "ODS_Server_Layer";
      $dsc = "ODS_Data_Server";
      $sfc = "ODS_Search_Site_File";
      $b   = new $bc();
      $d   = new $dc();
      $ps  = new $psc();
      $pb  = new $pbc();
      $lt  = new $ltc();
      $la  = new $lac();
      $cat = new $lcc();
      $sl  = new $slc();
      $ds  = new $dsc();
      $sf  = new $sfc();
      if (!$sf->exists())$sf->create();
      $dlnk = $this->getlinkedcolumn($dc);
      $sections = $ps->select($dlnk[1],$this->data[$dlnk[0]]);

      $blnk = $b->getlinkedcolumn($this->tablename);
      $buffers = $b->select($blnk[0],$this->data[$blnk[1]]);

      $ssteps = count($sections);
      $scompleted = 0;
      $pstep = $proc->createstep('Retrieve Site Data',$step,$ssteps);
      $psid = $pstep->getid();
      foreach($sections as $secnum => $section) {
         $blnk = $ps->getlinkedcolumn($pbc);
         $wkt = $this->data['Boundary'];
         $pbid = (isset($section[$blnk[0]])
                  &&isset($section[$blnk[0]]->Id))
                  ?$section[$blnk[0]]->Id:0;
         if ($pbid > 0) {
            $pb->loaddata($pbid);
            $bd = $pb->getdata();
            $bsize = $bd['Buffer_Size'];
            foreach($buffers as $bdata) { if ($bdata['Buffer_Size'] == $bd['Buffer_Size']) $wkt = $bdata['Buffer']; }
         }
         $slnk = $ps->getlinkedcolumn($ltc);
         $ltype = $section[$slnk[0]]->Id;
         $clnk = $cat->getlinkedcolumn($ltc);
         $layers = $cat->select($clnk[0],$ltype);
         $lcompleted = 0;
         $lsteps = count($layers);
         $lstep = $proc->createstep('Retrieve Layer Data',1,$lsteps,$psid);

         foreach($layers as $layer) {
            $this->retrievesitesourcedata($layer);
            $lcompleted++;
            $lstep->completestep($lcompleted);
         }

         $scompleted++;
         $pstep->completestep($scompleted);                              
      }
      $pstep->complete();      
      $proc->completestep($step);
      return 1;
   }
   function retrievesitesourcedata($layer) {
      $bc  = "ODS_Search_Buffer";
      $pc  = "ODS_Search_Profile";
      $dc  = "ODS_SDef";
      $psc = "ODS_SiP_Section";
      $pbc = "ODS_SP_Buffer";
      $ltc = "ODS_Layer_Type";
      $lcc = "ODS_Layer_Catalogue";
      $lac = "ODS_Layer_Attributes";
      $slc = "ODS_Server_Layer";
      $dsc = "ODS_Data_Server";
      $sfc = "ODS_Search_Site_File";
      $b   = new $bc();
      $d   = new $dc();
      $ps  = new $psc();
      $pb  = new $pbc();
      $lt  = new $ltc();
      $la  = new $lac();
      $cat = new $lcc();
      $sl  = new $slc();
      $ds  = new $dsc();
      $sf  = new $sfc();
      
      $labbrev = $layer['Abbreviation'];
      $llnk = $cat->getlinkedcolumn($slc);
      $slid = $layer[$llnk[1]]->Id; 
      $sl->loaddata($slid);
      $slayer = $sl->getdata();
      $feature = $slayer['Feature_Name'];
      $slnk = $sl->getlinkedcolumn($dsc);
      $ds->loaddata($slayer[$slnk[0]]->Id);
      $sdata = $ds->getdata(); 
      $ogc = new OGCFilter();
      $alnk = $la->getlinkedcolumn($lcc); 
      $la->loaddata($layer[$alnk[1]]);
      $atts = $la->getdata();
      $geom = $atts['Geometry_Attribute'];
      $ftr_s = $ogc->define('Overlaps',array($geom,$wkt));
      if ($layer['Return'] == 'Only Live Sites') {
         $ftr_d = $ogc->define('PropertyIsEqualTo',array($atts['Status_Attribute'],$atts['Live_Status']));
         $ftr_c = $ogc->define('And',array($ftr_d,$ftr_s));
         $opts =  $ogc->define('Filter',array($ftr_c));
      } else $opts = $ogc->define('Filter',array($ftr_s));
      $params     = array(
         'Action'    => 'GetFeature',
         'Feature'   => $feature,
         'URL'       => $sdata['URL'],
         'CR1'       => decrypt($sdata['User_Name'],$keys->k1,$keys->k2),
         'CR2'       => decrypt($sdata['Password'],$keys->k1,$keys->k2), 
         'Options'   => addslashes(json_encode($opts))
      );
      
      $svr = "${server}/r6";
      $tgt = "/wfs2json.php"; 
      $cres = $this->sendrequest($svr,$tgt,$params);
      if (isset($cres)) {
         $fname = "ODS_Search_${sid}_${labbrev}_SiteData.json";
         $type = 'text/json';
         $rdid = $this->saveresponse($fname,$type,$cres);
      }
            
      return 1;
   }
   function processsitedata() {
      global $folder;
      $step = 8;
      $proc = $this->getprocessor();
      $proc->completestep($step-1);
      chdir('..');
      
      $fc  = "S2_File";
      $pc  = "ODS_Search_Profile";
      $dc  = "ODS_SDef";
      $rdc = "ODS_Raw_Data";
      $sfc = "ODS_Search_Site_File";
      $sec = "ODS_Site_Summary_Entry";
      $lec = "ODS_Site_List_Entry";
      $psc = "ODS_SiP_Section";
      $ltc = "ODS_Layer_Type";
      $lcc = "ODS_Layer_Catalogue";
      $lac = "ODS_Layer_Attributes";
      $slc = "ODS_Server_Layer";
      $f   = new $fc();
      $p   = new $pc();
      $d   = new $dc();
      $rd  = new $rdc();
      $sf  = new $sfc();
      $se  = new $sec();
      $le  = new $lec();
      $ps  = new $psc();
      $lt  = new $ltc();
      $cat = new $lcc();
      $la  = new $lac();
      $sl  = new $slc();
      
      if (!$se->exists())$se->create();
      if (!$le->exists())$le->create();
      
      $layerdata = array();
      $rawdatas = $rd->select($this->getpk(),$this->getid());
      foreach($rawdatas as $rnum => $rdata) {
         $pstepcur = $scompleted + 1;
         try {
            $fid = $rdata['S2_File_ID'];
            $fid = (is_object($fid))?$fid->Id:$fid;
            $f = new S2_File();
            $f->loaddata($fid);
            $fdata = $f->getdata();
            $path = $fdata['Data'];
            $cres = s2_getfile($path);
            $jres = json_decode($cres);
            if (is_object($jres) && $jres->status == 1) {
               foreach($jres->data as $feature => $data) {
                  $lfeature = strtolower($feature);
                  $layerdata[$lfeature] = $data;
               }
            }
         } catch(Exception $e) {}
      }
//print_r($layerdata);exit;
      $dlnk = $this->getlinkedcolumn($dc);
      $sections = $ps->select($dlnk[1],$this->data[$dlnk[0]]);
      $ssteps = count($sections);
      $scompleted = 0;
      $pstep = $proc->createstep('Read Site Data',$step,$ssteps);
      $psid = $pstep->getid();
      foreach($sections as $secnum => $section) {
         $secid = (is_object($section[$ps->getpk()]))?$section[$ps->getpk()]->Id:$section[$ps->getpk()];
         $slnk = $ps->getlinkedcolumn($ltc);
         $ltype = $section[$slnk[0]]->Id;
         $clnk = $cat->getlinkedcolumn($ltc);
         $layers = $cat->select($clnk[0],$ltype);
         $lcompleted = 0;
         $lsteps = count($layers);
         $lstep = $proc->createstep('Read Layer Data',1,$lsteps,$psid);
         $lsumarea = array();
         $lsumperm = array();
         $lsumnumb = array();
         foreach($layers as $layer) {
            $layid = $layer[$cat->getpk()];
            $labbrev = $layer['Abbreviation'];
            $llnk = $cat->getlinkedcolumn($slc);
            $slid = $layer[$llnk[1]]->Id; 
            $sl->loaddata($slid);
            $slayer = $sl->getdata();
            $feature = $slayer['Feature_Name'];
            $lfeature = strtolower($feature);
            $alnk = $la->getlinkedcolumn($lcc); 
            $la->loaddata($layer[$alnk[1]]);
            $atts = $la->getdata();
            
            if (isset($layerdata[$lfeature])) {
               foreach($layerdata[$lfeature] as $fnum => $fdata) {
                  $geomcol = $fdata->geom;
                  $poly = $fdata->data->$geomcol->wkt;
                  
                  $measures = $this->getpolygonmeasurements($poly);
                  // convert area from m2 to hectares to 2 decimal places
                  $area = floor($measures->Area / 100) / 100;
                  // limit perimeter to no decimal places 
                  $peri = round($measures->Perimeter);
                  if (!isset($lsumarea[$feature])) $lsumarea[$feature] = $area;
                  else $lsumarea[$feature] += $measures->Area;
                  if (!isset($lsumperm[$feature])) $lsumperm[$feature] = $peri;
                  else $lsumperm[$feature] += $measures->Perimeter;
                  if (!isset($lsumnumb[$feature])) $lsumnumb[$feature] = 1;
                  else $lsumnumb[$feature] += 1;
                   
                  foreach($atts as $atttype => $attname) {
                     if (isset($attname)) {
                        $attname = strtolower($attname); 
                        switch($atttype) {
                        case 'Name_Attribute':  {$name   = (isset($fdata->data->$attname))?$fdata->data->$attname:null;}break;
                        case 'Code_Attribute':  {$code   = (isset($fdata->data->$attname))?$fdata->data->$attname:null;}break;
                        case 'Status_Attribute':{$status = (isset($fdata->data->$attname))?$fdata->data->$attname:null;}break;
                        case 'Update_Attribute':{$update = (isset($fdata->data->$attname))?$fdata->data->$attname:null;}break;
                        }
                     }
                  } 
                  
                  $sitelistdata = array(
                     'ODS_Search_ID'      => $this->getid(),
                     'ODS_SiP_Section_ID' => $secid,
                     'Type'               => $ltype,
                     'Layer'              => $layid,
                     'Boundary'           => $poly,
                     'Site_Name'          => $name,                   
                     'Site_Code'          => $code,
                     'Status'             => $status,
                     'Updated'            => $update,
                     'Hectares'           => $area,
                     'Perimeter'          => $peri
                  );
                  $le->setdata($sitelistdata);
                  $le->insert();
               }
               
               $sitesumdata = array(
                  'ODS_Search_ID'      => $this->getid(),
                  'ODS_SiP_Section_ID' => $secid,
                  'Type'               => $ltype,
                  'Layer'              => $layid,
                  'Sites'              => $lsumnumb[$feature],
                  'Total_Hectares'     => $lsumarea[$feature],
                  'Total_Perimeter'    => $lsumperm[$feature]
               );
               $se->setdata($sitesumdata);
               $se->insert();
            }
            $lcompleted++;
            $lstep->completestep($lcompleted,"Read layer: $lcompleted");
         }
      }
      $pstep->complete();
      $proc->completestep($step);
      return 1;
   }
   function producesitereport() {
      global $folder;
      $step = 9;
      $proc = $this->getprocessor();
      $proc->completestep($step-1);

      $pc   = "ODS_Search_Profile";
      $psc  = "ODS_SiP_Section";
      $dc   = "ODS_SDef";
      $oc   = "ODS_Site_Output";
      $otc  = "ODS_SiO_Type"; 
      
      $ssec = "ODS_Site_Summary_Entry";
      $slec = "ODS_Site_List_Entry";
      $soc  = "ODS_Search_Output";
      
      $sc = $this->tablename;
      $set = new stdClass();
      $sid = new stdClass();
      $sid->Current = $this->getid();
      $set->$sc = $sid; 
      
      $ps   = new $psc();
      $d    = new $dc();
      $o    = new $oc();
      $ot   = new $otc();
      $sse  = new $ssec();
      $sle  = new $slec();
      $dlnk = $this->getlinkedcolumn($dc);
      $sections = $ps->select($dlnk[1],$this->data[$dlnk[0]]);
//print_r($sections);
      $sname = $this->getname();
      $date = date("d/m/Y",time());      
      $html = "<div id='s2rep_data-container'><h1 class='s2rep_search-name'>Search: ${sname} ~ Run:${date}</h1><hr/><div id='s2rep_data'>";
      
      $ssteps = count($sections);
      $scompleted = 0;
      $pstep = $proc->createstep('Produce Site Report',$step,$ssteps);
      
      foreach($sections as $secnum => $psd) {
//print_r($psd);
         $psid    = $psd[$ps->getpk()];
         $secname = $psd['Type']->Name;
         $psecnum = $secnum + 1;
         $html   .= "<h2>Section ${psecnum}: ${secname}</h2><div id='s2rep_sec-${secnum}' class='s2rep_section-container'>";  
         
         $olnk    = $o->getlinkedcolumn($psc);
         $otlnk   = $o->getlinkedcolumn($otc);
         $outputs = $o->select($olnk[0],$psid);
//print_r($outputs);         
         foreach($outputs as $oind => $output) {
            $otype = $output[$otlnk[0]]->Name;
            $expand = ($output['Is_Expanded'])?'expand':'collapse';
            $ohtm = null;
            $rows = array();
            switch($otype) {
            case 'Summary': {
               $lnk  = $sse->getlinkedcolumn($psc);
               $rows = $sse->select($lnk[0],$psid,$set);
               if (count($rows)>0) $ohtm = $sse->tohtml($rows);                                       
            }break;
            case 'Site List': {
               $lnk  = $sle->getlinkedcolumn($psc);
               $rows = $sle->select($lnk[0],$psid,$set);
               if (count($rows)>0) $ohtm = $sle->tohtml($rows,$ex);
            }break;
            }
            $html .= "<h3>$otype</h3><div id='s2rep_sec-${secnum}_out-${oind}' class='s2_collapsible,${expand}'>";
            if (!isset($ohtm)) $html .= "<p>No data was found to meet section requirements</p>";  
            else $html .= $ohtm;
            $html .= "</div>";                                                            
         }
         $html.= "</div>";
         $scompleted++;
         $pstep->completestep($scompleted);                                  
      }
      $html.= "</div></div>";
      
      chdir('..');
      $sid = $this->getid();
      $f = new S2_File();
      if (!$f->exists()) $f->create();
      $d = new stdClass();
      $d->Name = "ODS_Search_${sid}.html";
      $d->Type = "text/html"; 
      $d->Data = $html;
      $d = s2_savefile($d);
      $f->setdata((array)$d);
      $fid = $f->insert();
      $url = 'http://'.$_SERVER['SERVER_NAME']."/${folder}/echofile.php?a=echo&i=$fid";
      $d->Link = $url;
      $f->setdata((array)$d);
      $f->update($f->getpk(),$fid);
      
      $so = new $soc();
      $count = $so->getcount($this->getid());
      if (!$so->exists()) $so->create();
      $d = array(
         $this->getpk() => $this->getid(),
         $f->getpk()    => $fid,
         'Number'       => ($count+1)
      );
      $so->setdata($d);
      $name = $so->getname();
      $f->setdata(array('Name'=>$name));
      $f->update($f->getpk(),$fid);
      $soid = $so->insert();
      $url = 'http://'.$_SERVER['SERVER_NAME']."/${folder}/report?t=$soc&o=$soid";
      $d['Link'] = $url;
      $so->setdata((array)$d);
      $so->update($so->getpk(),$soid);
      $status = ($soid)?1:0;
      
      $sse->delete($this->getpk(),$this->getid()); 
      $sle->delete($this->getpk(),$this->getid());
      
      $pstep->complete();
      $proc->completestep($step);
      return $status;
   }
?>