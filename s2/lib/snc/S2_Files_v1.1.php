<?php
/*
$paths = array(
   '../',
   '../../',
   '../../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
*/
include_once('settings.conf');
class S2_File extends DBT {
   protected $tablename = 'S2_File';
   protected $displayname = 'File';
   protected $show = array();
   protected $columns = array(
      'S2_File_ID'     	   => 'Unique Key',
      'Name'               => 'Short Text:Mandatory',
      'Type'               => 'Medium Text',
      'Data'               => 'File',
      'Link'               => 'URL' 
   );
   protected $domain = 'sites';
   protected $permissions = array(
      'Def' => 'Registered User'
   );
   function delete($col=null,$val=null) {
      /* before deleting from the database delete the file from the filesystem */
      if (isset($val)&&isset($col)) $this->loaddata($val,$col);
      if (isset($this->data['Data'])) s2_deletefile($this->data['Data']);
      $status = parent::delete($col,$val);
      return $status;
   }
}
function s2_is_image($ext) {
   $isimg = null;
   $ext = strtolower($ext);
   switch($ext) {
   case 'jpg': $isimg = true; break;
   case 'jpeg': $isimg = true; break;
   case 'gif': $isimg = true; break;
   case 'png': $isimg = true; break;
   default: $isimg = false; break;
   }
   return $isimg;   
}
function s2_mime_type($ext) {
   $type = null;
   $ext = strtolower($ext);
   switch($ext) {
   case 'jpg': $type = 'image/jpg'; break;
   case 'gif': $type = 'image/gif'; break;
   case 'png': $type = 'image/png'; break;
   case 'doc': $type = 'application/msword'; break;
   case 'docx': $type = 'application/msword'; break;
   case 'xls': $type = 'application/vnd.ms-excel'; break;
   case 'xlsx': $type = 'application/vnd.ms-excel'; break;
   case 'pdf': $type = 'application/pdf'; break;
   case 'eml': $type = 'message/rfc822'; break;
   case 'txt': $type = 'text/plain'; break;
   case 'mid': $type = 'application/mid'; break;
   case 'mif': $type = 'application/mif'; break;
   case 'csv': $type = 'text/csv'; break;
   case 'json': $type = 'text/json'; break;
   }
   return $type;   
}
function s2_inputfile_byget($file) {
   $obj = new stdClass();
   $filename = $_GET['qqfile'];
   $obj->Name = $filename;
   $size = $_SERVER['CONTENT_LENGTH'];
   preg_match('/\.(.+$)/',$obj->Name,$matches);
   $ext = $matches[1]; 
   $obj->Type = s2_mime_type($ext);
   $isimg = s2_is_image($ext);
   if ($isimg) {
      $dims = getimagesize("php://input");
      $url .= '&w='.$dims[0].'&h='.$dims[1];
   }
   $file->setdata((array)$obj);
   $id = $file->insert();
   if ($id) {
      $file->setid($id);
      $url = 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']."?a=echo&i=$id";
      $obj->Link = $url;
      $file->setdata((array)$obj);
      $file->update($file->getpk(),$id);
   }
   $fh = fopen("php://input", "r");
   $obj->Data = stream_get_contents($fh);
   fclose($fh);
   return $obj;
}
function s2_inputfile_byfiles($file) {
   $obj = new stdClass();
   $obj->Name = $_FILES['qqfile']['name']; 
   preg_match('/\.(.+$)/',$obj->Name,$matches);
   $ext = $matches[1]; 
   $size = $_FILES['qqfile']['size']; 
   $obj->Type = $_FILES['qqfile']['type'];
   $isimg = s2_is_image($ext);
   if ($isimg) {
      $dims = getimagesize($_FILES['qqfile']['tmp_name']);
      $url .= '&w='.$dims[0].'&h='.$dims[1];
   }
   $file->setdata((array)$obj);
   $id = $file->insert();
   if ($id) {
      $file->setid($id);
      $url = 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']."?a=echo&i=$id";
      $obj->Link = $url;
      $file->setdata((array)$obj);
      $file->update($file->getpk(),$id);
   }
   $fh = fopen($_FILES['qqfile']['tmp_name'], "r");
   //$obj->Data = fread($fh, $_FILES['qqfile']['size']);
   $obj->Data = stream_get_contents($fh);
   fclose($fh);
   return $obj;   
}
function s2_getfile($path) {
   $data = file_get_contents($path);
   return $data;
}
// function moves back to default s2 folder and returns the relative path to 
// the current directory. 
function s2_gohome() {
   global $folder;
   $path = getcwd();

   $sep = DIRECTORY_SEPARATOR;
   $rxp = '\\'.$sep;              

   $folders = preg_split("/$rxp/",$path);
   if (is_array($folders)) {

	   $relpath = null;
	   $cdir = array_pop($folders);
	   while(count($folders)>0 && $cdir != $folder) {
		  $relpath = (isset($relpath))?$cdir.$sep.$relpath:$cdir;
		  $cdir = array_pop($folders);
		  chdir('..');
	   }   
   } else $relpath = $path;
   return $relpath;
}

function s2_savefile($obj) {
   $goback = s2_gohome();
   chdir('files');
   $cwd = getcwd();
   $datestr = date("Ymd");

   $is_date_dir = file_exists($datestr);
   if (!$is_date_dir) $made = mkdir($datestr);
   $navd = chdir($datestr);
   $data = $obj->Data;
   $files = glob("*");
   sort($files);
   $dec1 = hexdec(array_pop($files)); 
   $written = false;
   $tries = 0;
   // to protect against 2 clients writing the same file at the same time loop 
   // through adding powers of 2 from the last entry until you find a filename 
   // which doesn't exist.
   $failed = false;  
   while(!$written && !$failed) {        
      $fdec = $dec1 + (pow(2,$tries));
      $fhex = strtoupper(str_pad(dechex($fdec),6,"0",STR_PAD_LEFT));
      if (!file_exists($fhex)) {
		  $written = file_put_contents($fhex,$data);
		  if (!$written) $failed = true;
	  }
      $tries++;
   }
   
   unset($obj->Data);
   $obj->Data = ($failed)?"upload failed":"files/$datestr/$fhex";
   chdir("../..");
   chdir($goback);
   return $obj;   
}
function s2_deletefile($path) {
   return unlink($path);
}                 
?>
