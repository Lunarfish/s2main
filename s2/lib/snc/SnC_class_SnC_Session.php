<?php
/*                                                                                                  
CREATE TABLE SnC_Session (
   Snc_Session_Key INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   Snc_User_Key INTEGER,
   Started TIMESTAMP,
   LastAccess TIMESTAMP,
   Expires TIMESTAMP
);
*/
class SnC_Session {
   private $table = 'SnC_Session';
   private $Snc_Session_Key;
   private $Snc_User_Key;
   private $Started;
   private $LastAccess;
   private $Expires;
   function SnC_Session($id=null) {
      if ($id!=null) {
         $this->Snc_Session_Key = $id;
         $this->retrieve();
      }
   }
   function retrieve() {
      $dbh = $GLOBALS['dbh'];
      $query = $dbh->select();
      $query->from($this->table,
            array("Snc_User_Key"    => "Snc_User_Key",
                  "Started"         => "Started",
                  "LastAccess"      => "LastAccess",
                  "Expires"         => "Expires"));
      $query->where('Snc_Session_Key = ?',$this->Snc_Session_Key);
      $rows = $dbh->fetchAll($query);
      for ($i=0;$i<count($rows);$i++) {
         $record = $rows[$i];
         $this->SnC_User_Key  = $record["Snc_User_Key"];
         $this->Started       = $record["Started"];
         $this->LastAccess    = $record["LastAccess"];
         $this->Expires       = $record["Expires"]; 
      }      
   }
   function getSessionID() {
      return $this->Snc_Session_Key;      
   }
   function insert() {
      $dbh = $GLOBALS['dbh'];  
      $data = array(
         'Started'         => new Zend_Db_Expr('CURRENT_TIMESTAMP'),
         'LastAccess'      => new Zend_Db_Expr('CURRENT_TIMESTAMP')
      );
      $status = $dbh->insert($this->table, $data);
      if ($status) $this->Snc_Session_Key = $dbh->lastInsertId();
      return $status;
   }
   function setUser($userid) {
      return $this->update($this->Snc_Session_Key,'Snc_User_Key',$userid);
   }
   function update($session,$field,$value) {
      $dbh = $GLOBALS['dbh'];
      $data = array($field => $value);
      return $dbh->update($this->table, $data, 'Snc_Session_Key = '.$this->Snc_Session_Key);               
   }     
}
?>