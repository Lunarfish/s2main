<?php
class DBT_Lookup extends DBT {
   protected $domain = 'genera';
   protected $isself = false;         
   function DBT_Lookup($property,$tab1,$col1,$tab2,$col2) {
      parent::DBT();
      if (isset($property)
            && isset($tab1) && isset($col1)
            && isset($tab1) && isset($col2)) {
            
         if ($tab1 < $tab2) $this->getproperties($property,$tab1,$col1,$tab2,$col2);            
         else $this->getproperties($property,$tab2,$col2,$tab1,$col1);            
         
         if (!$this->exists()) $this->create();
         $this->isself = ($col1 == $col2);
      }
   }
   
   function getproperties($property,$tab1,$col1,$tab2,$col2) {
      $lp = new DBTL_Lookup_Properties();
      $props = $lp->getproperties($property,$tab1,$col1,$tab2,$col2);
      $this->settablename($props->TableName);
      $this->setcolumns($props->Columns);
   }
   
   function selecttargets($col,$val) {
      if (!$this->isself) { $ltms = $this->select($col,$val); } 
      else {
         $ltms1 = $this->select($col . 1,$val);
         $ltms2 = $this->select($col . 2,$val);
         $ltms = array_merge($ltms1,$ltms2); 
      }
      
      foreach ($ltms as $k => $ltm) {
         foreach($ltm as $dcol => $dval) {
            if (!($dcol==$col) && $this->columns[$dcol] != 'Unique Key') {
               if ($this->isself && 
                  ((is_object($dval) && $dval->Id != $val)|| (!is_object($dval) && $dval != $val))
               ) { $ltms[$k] = $dval; } 
               else {
                  $ltype = $this->columns[$col];
                  if (preg_match('/^\{/',$ltype)) {
                     $params = json_decode($ltype);
                     $tcol = $params->TargetField;
                     $ltms[$k] = $dval;
                  } else if (preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches)) {
                     $tcol = $matches[3];
                     $ltms[$k] = $dval;
                  }   
               }            
            }
         }
         if (is_object($ltms[$k]) && $ltms[$k]->Id == null) unset($ltms[$k]);
         else if (!($ltms[$k]>0)) unset($ltms[$k]);
      }
//print_r($ltms);
      return $ltms;
   }
}

class DBTL_Lookup_Properties extends DBT {
   protected $domain = 'genera';
   protected $tablename = 'DBTL_Lookup_Properties';
   protected $columns = array(
      "DBTL_Lookup_Properties_ID"   => 'Unique Key',
      "Property"                    => 'Short Text',
      "Table1"                      => 'Short Text',
      "Column1"                     => 'Short Text',
      "Table2"                      => 'Short Text',
      "Column2"                     => 'Short Text'
   );
   
   function DBT_Lookup_Properties() {
      parent::DBT();
   }
   
   function getproperties($property,$tab1,$col1,$tab2,$col2) {
      if (!$this->exists()) $this->create();
      $this->setdata(array(
         'Property' => $property,
         'Table1'   => $tab1,
         'Column1'  => $col1,
         'Table2'   => $tab2,
         'Column2'  => $col2
      ));
      $id = $this->isduplicate();
      if ($id > 0) {
         $this->loaddata($id);
      } else {
         $this->insert();
         $id = $this->getid();
      }
      $props = new stdClass();
      $props->TableName = "DBTL_$id";
      $props->Columns = $this->getlookupcolumns($props->TableName); 
      return $props;
   }
    
   function getlookupcolumns($tablename) {
      $tab1 = $this->data['Table1'];
      $col1 = $this->data['Column1'];
      $tab2 = $this->data['Table2'];
      $col2 = $this->data['Column2'];
      if ($col1 == $col2) { $als1 = $col1 . 1; $als2 = $col2 . 2; } 
      else { $als1 = $col1; $als2 = $col2; }
      $cols = array(
         "${tablename}_ID" => "Unique Key",
         $als1             => '{"DataType":"LINKEDTO","TargetType":"'.$tab1.'","TargetField":"'.$col1.'"}',
         $als2             => '{"DataType":"LINKEDTO","TargetType":"'.$tab2.'","TargetField":"'.$col2.'"}'
      );
      return $cols;
   }
   protected $permissions = array(
      'Def'    => 'Registered User'
   );
}

/*
class DBT_Lookup extends DBT {
   protected $domain = 'genera';
   function DBT_Lookup($property,$tab1,$col1,$tab2,$col2) {
      parent::DBT();
      if (isset($property)
            && isset($tab1) && isset($col1)
            && isset($tab1) && isset($col2)) {
         $name = ($tab1 < $tab2)?"DBTL_${tab1}_${tab2}_${property}":"DBTL_${tab2}_${tab1}_${property}";
         $name = preg_replace('/_S2_/','_',$name);
         //$name = "${tab1}_${property}"; 
         $this->settablename($name);
         $columns = array(
            "${name}_ID"   => 'Unique Key',
            $col1          => '{"DataType":"LINKEDTO","TargetType":"'.$tab1.'","TargetField":"'.$col1.'"}',
            $col2          => '{"DataType":"LINKEDTO","TargetType":"'.$tab2.'","TargetField":"'.$col2.'"}'
         );
         $this->setcolumns($columns);
         if (!$this->exists()) $this->create();
      }
   }
   function selecttargets($col,$val) {
      $ltms = $this->select($col,$val);
      foreach ($ltms as $k => $ltm) {
         foreach($ltm as $dcol => $dval) {
            if (!($dcol==$col) && $this->columns[$col] != 'Unique Key') {
               $ltype = $this->columns[$col];
               if (preg_match('/^\{/',$ltype)) {
                  $params = json_decode($ltype);
                  $tcol = $params->TargetField;
                  $ltms[$k] = $dval;
               } else if (preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).* /',$this->columns[$col],$matches)) {
                  $tcol = $matches[3];
                  $ltms[$k] = $dval;
               }               
            }
         }
         if (is_object($ltms[$k]) && $ltms[$k]->Id == null) unset($ltms[$k]);
         else if (!($ltms[$k]>0)) unset($ltms[$k]);
      }
//print_r($ltms);
      return $ltms;
   }   
}
*/

?>
