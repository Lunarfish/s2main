<?php    
//error_reporting(E_ALL);                           
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('cdm/cdm_access_functions.php');
include_once('users/users.inc');
include_once('gen/G_Cache.php');
include_once('klib.php');

/*
   echo $query->__toString();
   
   Derived Data - uses a javascript function to update a column 
   
   "Derives":[{"Column":"colname","Method":jsmethod,"Params":[{"Type":"Value"},{"Type":"Static","Value":"val"}]]

*/                           

class DBT {
   protected $sid;
   protected $dbhandle;
   protected $id;
   protected $tablename;
   protected $displayname;
   protected $validated = false;
   // alts contains a list of tables which can be listed instead of the 
   // default table in list views
   protected $alts = array();
   // show contains a list of tables to be shown on screen along with an entry 
   // for this table. 
   protected $show = array();
   // view supercedes show allowing for named views to limit the amount of data 
   // on any one page. view is defined as json text
   protected $order = null; 
   protected $view;
   protected $perpage = 10;
   protected $user;
   // data types can be one of $this->datatypes or 
   // LINK:<tablename>.<columnname> and will pick up their 
   // datatype from that table. 
   // note that table will need to be included using 
   // include_once to ensure the definition can be read
   
   // column types can be appended with :Mandatory for required fields
   // linked data column types can be appended with :Current to use the 
   // current selected entry.  
   protected $columns = array();
   // permissions array can contain a permission for each action 
   // Add, Edit, Delete, List, Select, Create, Drop
   // if no permission is supplied, no permission is required 
   protected $domain = 'genera';
   protected $permissions = array();
   protected $instancedomain = false;
   protected $instanceof = null;
   
   protected $data = array();
   protected $primingdata = array();
   protected $selectedby;
   protected $childtables = array();
   protected $deriveddata = array();
   protected $importreq = array();
   // FilePath and URL are essentially just text but allow you to attach 
   // a different interface to populate them. 
   protected $datatypes = array(
      'Unique Key'      => 'SERIAL PRIMARY KEY',                                    
      'Linked Key'      => 'INT',
      'Interface'       => 'INT',
      'Short Text'      => 'VARCHAR(100)',
      'Password'        => 'VARCHAR(100)',
      'Medium Text'     => 'VARCHAR(200)',
      'Long Text'       => 'TEXT',
      'Encrypted Text'  => 'TEXT',
      'Number'          => 'INTEGER',
      'Percentage'      => 'FLOAT',
      'Count'           => 'INTEGER',
      'Decimal'         => 'FLOAT',
      'Version'         => 'FLOAT',
      'True or False'   => 'BOOLEAN',
      'Date'            => 'DATE',
      'Time'            => 'TIME',
      'Date and Time'   => 'TIMESTAMP',
      'Map Data'        => 'GEOMETRY',
      'Taxa'            => 'INT',
      'URL'             => 'VARCHAR(500)',
      'Email'           => 'VARCHAR(100)',
      'File Path'       => 'VARCHAR(500)',
      'File'            => 'VARCHAR(500)' 
   ); 
   protected $showopts = array('Current','All','Next','Previous','Every');
   protected $actions = array('Def','List','View','Add','Edit','Delete','Import');               
   function DBT () {
      global $_CURRENT_USER;
      $this->dbhandle = SnC_getDatabaseConnection();
      if (isset($_CURRENT_USER)) {
         if (!$_CURRENT_USER->is_anonymous && !isset($_CURRENT_USER->email)) $_CURRENT_USER->init($_REQUEST['UInfo']);
      }
      $this->user = $_CURRENT_USER;
      $this->sid = $_COOKIE['PHPSESSID']; 
   }
   function drawform() {
      $form = "<form method='post' action='' enctype='multipart/form-data'>
      <h3>$this->displayname</h3>
      <table><tbody>";
      foreach ($this->columns as $name => $type) {
         $displayname = ucwords(preg_replace('/\_/',' ',$name));
         $form.= "<tr><th>$displayname</th><td><input type='text' id='$name' name='$name'/></td></tr>";      
      }
      $form.= "</tbody></table></form>";
      return $form;
   }
   function settablename($tablename) {
      $this->tablename = $tablename;
   }
   function gettablename() {
      return $this->tablename;   
   }
   function getdatatypes() {
      $dt = array();
      foreach($this->datatypes as $type => $def) $dt[] = $type;
      
      return $dt;
   }
   function getactions() {
      return $this->actions;
   }
   function getsid() {
      return $this->sid;
   }
   function setsid($sid) {
      $this->sid = $sid;
   }
   function setdata($data,$revalidate=false) {
      $this->data = $data;
      if (!$this->validated || $revalidate) $this->validate();
   }
   function isduplicate() {
      $dbh = $this->dbhandle;                       
      $query = $dbh->select();
      $query->from(strtolower($this->tablename));
      // CreatedBy and CreatedOn have to be excluded from duplicate testing.
      // Automatically populated data has to be excluded as well. 
      $excludedcolumns = array('CreatedBy','CreatedOn', 'Count', 'Version');
      foreach($this->columns as $col => $def) {
         $val = (isset($this->data[$col]))?$this->data[$col]:null;
         if (preg_match('/^\{/',$def)) {
            $params = json_decode($def);
            $dtype = $params->DataType;
         } else $dtype = preg_replace('/\:.*/','',$def);
         if (!in_array($col,$excludedcolumns) && isset($val)) {
            switch ($dtype) {
            case 'Map Data': {
               //$val = new Zend_Db_Expr("st_geomfromtext('$val',4326)");
               $val = $this->insertterm($col,$dtype,$val);
               $query->where("$col = ?",$val);   
            }break;
            case 'LINKEDTO': {
               if (is_object($val)) $val = $val->Id;
               $query->where("$col = ?",$val);
            }break;
            default: {
               $query->where("$col = ?",$val);
            }break;
            } 
         }
      }
//echo $query->__toString();exit;
      $result = $dbh->fetchRow($query);
      return (is_array($result))?true:false;      
   }
   function cleardata() {
      $this->data = array();
   }
   function getparent() {
      $parent = null;
      $pval;
      foreach($this->columns as $col => $def) {
         if(!isset($parent)) {
            if (preg_match('/^\{/',$def)) {
               $params = json_decode($def);
               $dtype = $params->DataType;
            } 
            if ($dtype == 'LINKEDTO') {
               $parent = $params->TargetType;
               $pval = $this->data[$col];
            }
         }  
      }
      $p = new stdClass();
      $p->IType = $parent;
      $p->Data = $pval;
      return $p;
   }
   function setuser($user) {
      $this->user = $user;
   }
   function getuser() {
      return $this->user;
   }
   function getdisplayname() {
      return $this->displayname;
   }
   function getversions($val) {
      $versions = array();
      foreach ($this->columns as $col => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $classname = (isset($params->VersionOfType))?$params->VersionOfType:null;
            $thiscol = (isset($params->VersionOfField))?$params->VersionOfField:null;
         } else list($type,$classname,$thiscol) = preg_split('/\:/',$type);
         if ($type == 'Version') {
            $link = null;
            $link = new $classname();
            if ($link->exists() && $val) {
               $versions['Column'] = $col;
               $link->loaddata($val,null,false);
               $column = $this->displayname;
               $column = preg_replace('/\s+/','_',$column);
               $cversion = $link->selectcolumnval($column);
               $cversion = (!$cversion>0)?1:$cversion;
               $versions['Current'] = floor($cversion*10)/10;
               
               if ($this->exists()) {
                  $query = $this->dbhandle->select();
                  $ltable = strtolower($this->tablename);
                  $lcol = strtolower($col);
                  $query->from($ltable, array('MaxVer' => new Zend_Db_Expr("Max($lcol)")));
                  $lthiscol = strtolower($thiscol);
                  $query->where("$lthiscol = ?",$val);
                  $result = $this->dbhandle->fetchRow($query);
                  if (isset($result['MaxVer'])) //$versions['Maximum'] = floor($result['MaxVer']*10)/10;
                     $versions['Maximum'] = floor(round($result['MaxVer'],1)*10)/10;
               }
            }               
         } 
      }
      return $versions;
   }
   function getcount($val) {
      $count = 0;
      foreach ($this->columns as $col => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $classname = (isset($params->CountOfType))?$params->CountOfType:null;
            $thiscol = (isset($params->CountOfField))?$params->CountOfField:null;
         } else list($type,$classname,$thiscol) = preg_split('/\:/',$type);
         if ($type == 'Count') {
            if ($val && $thiscol) {
               $query = $this->dbhandle->select();
               $lcol = strtolower($col);
               $lthiscol = strtolower($thiscol);
               $query->from(strtolower($this->tablename), array('MaxCount' => new Zend_Db_Expr("Max($lcol)")));
               $query->where("$lthiscol = ?",$val);
               $result = $this->dbhandle->fetchRow($query);
               if (isset($result['MaxCount'])) $count = $result['MaxCount'];
            }               
         } 
      }
      return $count;        
   } 
   function updateversion($id,$versionof,$version) {
      $pk = $this->getpk();
      $link = null;
      $link = new $versionof();
      $ucol = $link->getdisplayname();
      $ucol = preg_replace('/\s+/','_',$ucol);
      $lucol = strtolower($ucol);
      $data = array($lucol => $version);
      $lpk = strtolower($pk); 
      $where = $this->dbhandle->quoteInto("$lpk = ?",$id);
      $status = $this->dbhandle->update(strtolower($this->tablename),$data,$where);
      return $status;
   }
   function selectcolumnval($col) {
      $value = null;
      if (isset($this->data[$col])) {
         $value = $this->data[$col];
      } else
      if($this->exists()) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
         } else {
            $type = $this->columns[$col];
            $type = preg_replace('/\:.+/','',$type);
         }
         $query = $this->dbhandle->select();
         $lcol = strtolower($col);
         $query->from(strtolower($this->tablename), array($col=>$lcol));
         $pk = $this->getpk();
         $lpk = strtolower($pk);
         $id = $this->getid();
         $query->where("$lpk = ?",$id);
         $result = $this->dbhandle->fetchRow($query);
         $value = (isset($result[$col]))?$result[$col]:null;
         switch ($type) {
         case 'LINKEDTO': {
            if (isset($params)) {
               if (!is_object($value)) {
                  $type = $params->DataType;
                  $table = $params->TargetType;
                  $column = $params->TargetField;
                  $link = null;
                  $link = new $table();
                  $link->loaddata($value,$column);
                  $obj = new stdClass();
                  $obj->Name = $link->getname();
                  $obj->Id = $link->getid();
                  $value = $obj;
               }
            }   
         }break;
         case 'LINKEDTOM': {
            if (isset($params)) {
               if (!is_object($value)) {
                  $type = $params->DataType;
                  $table = $params->TargetType;
                  $column = $params->TargetField;
                  $pk = $this->getpk();
                  $id = $this->getid();
                  $p = (isset($params->Property))?$params->Property:$col;
                  $ltm = new DBT_Lookup($p,$this->tablename,$pk,$table,$column);
                  $obj = $ltm->selecttargets($value,$pk);
                  $value = $obj;
               }
            }   
         }break;
         /*
         case 'Date': {
            if (preg_match('/(\d+)\-(\d+)\-(\d+)/',$value,$matches)) {
               $y = $matches[1];
               $m = $matches[2];
               $d = $matches[3];
               if (intval($y)+intval($m)+intval($d)>0) {
                  $value = "$d/$m/$y";
               } $value = null;
            }
         }break;
         case 'INTERFACE': {
            if (isset($params)) {
               if (is_string($value) && $value != "") {
                  $value = json_decode($value);
               } 
            } 
         }break;
         case 'Taxa': {
            if (isset($params)) {
               if (is_string($value) && $value != "") {
                  $value = json_decode($value);
               } 
            } 
         }break;
         case 'Password': {
            $k = new KLib($this->getsid());
            $keys = json_decode($k->getkeys());
            $value = encrypt($value,$keys->k1,$keys->k2);
         }break;
         case 'Encrypted Text': {
            $k = new KLib($this->getsid());
            $keys = json_decode($k->getkeys());
            $value = encrypt($value,$keys->k1,$keys->k2);
         }break;
         */
         default: {
            $value = $this->postprocessterm($col,$type,$value);
         }break;
         } 
      }
      return $value; 
   }
   function selectvalwhere($col,$where) {
      $query = $this->dbhandle->select();
      $lcol = strtolower($col);
      $query->from(strtolower($this->tablename), array($col=>$lcol));
      foreach($where as $wcol => $val) {
         $lwcol = strtolower($wcol);
         $query->where("$lwcol = ?",$val);
      }
//print $query->__toString();exit;
      $result = $this->dbhandle->fetchRow($query);
      $value = (isset($result[$col]))?$result[$col]:null;
      return $value;      
   }
   function getnameid() {
      $obj = new stdClass();
      $obj->Id = $this->getid();
      $obj->Name = $this->getname();
      return $obj;
   }
   function derivename($columns) {
      $names = array();
      foreach($columns as $col) {
         $type = $this->columns[$col];
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            switch($type) {
            case 'LINKEDTO': {
               $table = (isset($params->TargetType))?$params->TargetType:null;
               $tcol = (isset($params->TargetField))?$params->TargetField:null;
            }break;
            }
         } else if (preg_match('/([^\:]+)\:([^\.]+)\.(.+)/',$type,$matches)) {
            $type = $matches[1];
            $table = $matches[2];
            $tcol = $matches[3];
         }
         switch($type) {
         case 'LINKEDTO': {
            if (isset($table) && isset($tcol)) {
               $link = new $table();
               $id = (is_object($this->data[$col]))
                     ?$this->data[$col]->Id
                     :$this->data[$col];
               if ($id > 0) {
                  $link->loaddata($id);
                  $names[] = $link->getshortname();
               }
            }
         }break;
         case 'INTERFACE': {
            $iobj = $this->data[$col];
            if (is_object($iobj) && isset($iobj->Name)) $names[] = $iobj->Name;
            else if (is_array($iobj) && isset($iobj['Name'])) $names[] = $iobj['Name'];
            //else {print_r($iobj);exit;}
         }break;
         default: {
            $names[] = $this->data[$col];
         }break;
         }  
      }
      return join(':',$names);   
   }
   function getdata() {
      if (!isset($this->data['Name'])) {
         $this->data['Name'] = $this->getname();
      }
      foreach($this->data as $col => $val) {
         $type = $this->columns[$col];
         $params = null;
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
         } else {
            $bits = preg_split('/\:/',$type);
            $type = $bits[0];
         }
         switch ($type) {
         case 'Date': {
            if (preg_match('/(\d+)\-(\d+)\-(\d+)/',$val,$matches)) {
               $y = $matches[1];
               $m = $matches[2];
               $d = $matches[3];
               if (intval($y)+intval($m)+intval($d)>0) {
                  $val = "$d/$m/$y";
                  $this->data[$col] = $val;
               } else $this->data[$col] = '';
            }
         }break;
         case 'LINKEDTO': {
            if (isset($params)) {
               if (!is_object($val) && $val > 0) {
                  $type = $params->DataType;
                  $table = $params->TargetType;
                  $column = $params->TargetField;
                  $link = null;
                  $link = new $table();
                  $link->loaddata($val,$column);
                  $obj = new stdClass();
                  $obj->Name = $link->getname();
                  $obj->Id = $link->getid();
                  $this->data[$col] = $obj;
               }
            } else if(preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches)) {
               if (!is_object($val)) {
                  $type = $matches[1];
                  $table = $matches[2];
                  $column = $matches[3];
                  $link = null;
                  $link = new $table();
                  $link->loaddata($val,$column);
                  $obj = new stdClass();
                  $obj->Name = $link->getname();
                  $obj->Id = $link->getid();
                  $this->data[$col] = $obj;
               }
            }   
         }break;
         case 'LINKEDTOM': {
            if (isset($params)) {
               if (!is_object($val)) {
                  $type = $params->DataType;
                  $table = $params->TargetType;
                  $column = $params->TargetField;
                  $pk = $this->getpk();
                  $id = $this->getid();
                  $p = (isset($params->Property))?$params->Property:$col;
                  $ltm = new DBT_Lookup($p,$this->tablename,$pk,$table,$column);
                  $obj = new stdClass();
                  $obj->IType = $table;
                  $obj->ListData = $ltm->selecttargets($pk,$id);
                  $this->data[$col] = $obj;
               }
            } else if(preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches)) {
               if (!is_object($val)) {
                  $type = $matches[1];
                  $table = $matches[2];
                  $column = $matches[3];
                  $pk = $this->getpk();
                  $id = $this->getid();
                  $ltm = new DBT_Lookup($col,$this->tablename,$pk,$table,$column);
                  $obj = new stdClass();
                  $obj->IType = $table;
                  $obj->ListData = $ltm->selecttargets($pk,$id);
                  $this->data[$col] = $obj;
               }
            }   
         }break;
         case 'INTERFACE': {
            if(isset($params)) {
               if (is_object($val) && isset($val->JSON)) $this->data[$col] = json_decode($val->JSON); 
               else if (is_array($val) && isset($val['JSON'])) $this->data[$col] = json_decode($val['JSON']);  
            }
         }break;
         case 'Taxa': {
            if(isset($params)) { 
               if (is_object($val) && isset($val->JSON)) {
                  $this->data[$col] = json_decode($val->JSON); 
               }
            }
         }break;
         case 'Password': {
            $k = new KLib($this->getsid());
            $keys = json_decode($k->getkeys());
            $value = encrypt($value,$keys->k1,$keys->k2);
         }break;
         case 'Encrypted Text': {
            $k = new KLib($this->getsid());
            $keys = json_decode($k->getkeys());
            $value = encrypt($value,$keys->k1,$keys->k2);
         }break;
         }
      }
      return $this->data;
   }
   function setcolumns($cols) {
      $this->columns = $cols;
   }                                           
   function getcolumns() {
      return $this->columns;
   }
   function getpermissions() {
      return $this->permissions;
   }
   function getdomain() {
      if ($this->instancedomain) {
         $settings = (isset($_REQUEST['CurrentSettings']))?(array)json_decode($_REQUEST['CurrentSettings']):array();
//if($this->tablename == 'S2_Site') print $this->instancedomain; print "SETTINGS"; print_r($settings);exit;
         $domain = $this->domain;
         if (isset($settings[$this->instanceof])) $domain .= "_".$settings[$this->instanceof]->Current;  
         return $domain;
      } else return $this->domain;
   }
   function getorigdomain() {
      return $this->domain;
   }
   function getdomaininstancename() {
      if ($this->instancedomain) {
         $settings = (array)json_decode($_REQUEST['CurrentSettings']);
         $domain = $this->domain;
         if ($settings[$this->instanceof]) $domain = $settings[$this->instanceof]->Name;  
         return $domain;
      } else return $this->domain;
   }
   function getinstanceof() {
      if ($this->instancedomain) return $this->instanceof;
      else return null;
   }
   function getuserpermissions() {
      //$perms = array('List','View','Add','Edit','Delete','Import');
      $perms = $this->actions;
      array_shift($perms);
      $granted = array();
      foreach($perms as $perm) {
         if ($this->isallowed($this->user,$perm)) $granted[$perm] = true;      
      }
      return $granted;      
   }
   function isallowed($user,$action) {
      //global $_CURRENT_USER;
      // permissions can be set on a specific action
      if (!in_array($action,$this->actions)) {
      // limited actions are set in $this->actions 
      // other actions such as CheckIn or Calculate do not require permission
         return 1;
      } else {
         $required = (isset($this->permissions[$action]))?$this->permissions[$action]:null;
         // or you can set a single requirement for all actions
         if (!isset($required)) $required = (isset($this->permissions['Def']))?$this->permissions['Def']:null;
         if (isset($required)) {
            if (isset($user)) {
               if (isset($this->data['Created_By']) && ($user->getid() == $this->data['Created_By'])) return 1;
               if ($this->instancedomain) {
                  $effective = $this->getdomain();
                  return max(
                        $user->get_permission($this->domain,$required),
                        $user->get_permission($effective,$required));                               
               } else return $user->get_permission($this->domain,$required); 
            }
         } else return 1;
      }
      // If no permission is required return 1
   }
   function getname() {
      $name = null;
      if(isset($this->columns['Name'])) $name = $this->data['Name'];
      else {
         foreach($this->columns as $column => $type) {
            preg_match('/([^\:]+)/',$type,$matches);
            $type = $matches[1];                                                                                  
            if($type == 'Short Text') {
               $name = $this->data[$column];
               break;
            }
         }
      }
      return $name; 
   }
   // the short name method is a place holder to be overridden for subclasses 
   function getshortname() {
      return $this->getname();
   }
   function noshow() {
      $this->show = array();
   }
   function getviewdefs($vname = null) {
      $v = (isset($this->view))?json_decode($this->view):null;
      if (isset($vname)) {
         foreach ($v as $i => $vi) if ($vi->Name == $vname) $vdef = $vi;
         if (isset($vdef)) $v = $vdef;
      }
      return $v;   
   }
   function getshowlist() {
      $s = (isset($this->show))?$this->show:null;
      return $s;      
   }
   function getshowopts() {
      return $this->showopts;      
   }
   function getextdataparams($extdata) {
      global $configsettings;
      foreach($extdata as $ex => $edef) {
         $ps = $edef->Parameters;
         foreach($ps as $px => $pdef) {
            switch($pdef->Type) {
            case 'Column': $pdef->Value = $this->data[$pdef->Value];break;
            case 'Config': $pdef->Value = $configsettings[$pdef->Value];break;
            }
            if ($pdef->Encrypt) {
               $k = new KLib($this->getsid());
               $keys = json_decode($k->getkeys());
               $pdef->Value = encrypt($pdef->Value,$keys->k1,$keys->k2);
            }
            $ps[$px] = $pdef;
         }
         if (isset($edef->Credentials)) {
            $cred = $edef->Credentials; 
            foreach($ps as $px => $pdef) {
               if ($cred->Parameter == $pdef->Name) {
                  $cs = $configsettings[$pdef->Value];
                  foreach ($cs as $cx => $c) {
                     $np = new stdClass();
                     $np->Name = "CR".($cx+1);
                     $np->Value = $c;
                     $ps[] = $np;
                  }
               }                
            }
         }
         $edef->Parameters = $ps;
         $extdata[$ex] = $edef;
      }
      return $extdata;
   }
   function getview($viewsettings=null,$viewname=null) {
      $json = new stdClass();
      $json->IType = $this->tablename;
      $json->DisplayName = $this->getdisplayname();
      $json->Interface = $this->getinterface();
      //$json->Versions = $this->getversions();
      $json->Current = (isset($this->data[$this->getpk()]))?$this->data[$this->getpk()]:null;
      $json->Domain =  $this->getorigdomain();
      $json->IsAllowedTo = $this->getuserpermissions();
               
      if ($json->Current) {
         // reload data because some of the data might have been manipulated 
         // for the database insert.
         $id = $this->getid();
         $this->loaddata($this->getid());
         $json->Data = $this->getdata();
         $json->ItemName = $this->getname();
      }
         
      $linkdata = array();
      $show = null;
      $views = array();
      $iview = null;
      if (isset($this->view)) {
         $viewdata = json_decode($this->view);
         if (count($viewdata)>0) {
            foreach($viewdata as $view) {
               $views[] = $view->Name;
               //if ((isset($viewname)&&$viewname == $view->Name)||(!isset($viewname)&&!isset($show))) {
               if (isset($viewname)&&($viewname == $view->Name)) {
                  $show = $view->Data;
                  $json->ViewName = $viewname;
                  if (isset($view->External)) {$json->External = $this->getextdataparams($view->External);} 
               }
            }
         }
      } 
      if (!isset($show)&&!isset($this->view)) $show = $this->show;
      if (isset($this->alts)) $json->Alternatives = $this->alts;
      
      if(count($views)>0) $json->Views = $views;
      if (is_array($show)) {      
         foreach($show as $data) {
            if (is_object($data)) {
               $classname = $data->Table;
               $showopts = (isset($data->Show))?$data->Show:'All';
               $sectionname = (isset($data->Name))?$data->Name:null;            
            } else {
               // This is for if the show array replaces the view definition
               $classname = $data;
               if (preg_match('/([^:]+):(.+)/',$classname,$m)) $classname = $m[1];
               $showopts = 'All';
            }         
            $link = new $classname();
            if (isset($link)) $link->setuser($this->user);
            if (isset($link) && $link->isallowed($this->user,'List')) {
               if (!$link->exists()) $link->create();
               eval("\$osettings = (isset(\$viewsettings->$classname))?\$viewsettings->$classname:null;");
               // see if link is in this table first
               list($scol,$tcol) = $this->getlinkedcolumn($classname);
               // then if it isn't see if it is in the linked table
               if (!isset($scol)) list($tcol,$scol) = $link->getlinkedcolumn($this->tablename);
               $val = $this->data[$scol];
               if (is_object($val)) $val = ($val->Current)?$val->Current:$val->Id;
            //print "$classname $scol $tcol $val\n";
               if (isset($val)||$showopts == 'Every') {
                  $ld = new stdClass();
                  $ld->IType = $classname;
                  $ld->DisplayName = (isset($sectionname))?$sectionname:$link->getdisplayname();
                  if (isset($data->Columns)) $ld->Columns = $data->Columns;
                  $ld->Interface = $link->getinterface();
                  $ld->Domain =  $link->getorigdomain();
                  $ld->Versions = $link->getversions($val);
                  $ld->Count = $link->getcount($val);
                  $ld->IsAllowedTo = $link->getuserpermissions();
                  switch($showopts) {
                  case 'Current': {
                     $usecol = $link->getdisplayname();
                     $usecol = preg_replace('/\s+/','_',$usecol);
                     $byval = $this->selectcolumnval($usecol); 
                     $link->loadone($val,$tcol,'Current',$byval);
                     
                     $lid = $link->data[$link->getpk()];
                     if (isset($lid)) {  
                        $ld->Data = $link->getdata();
                        $ld->Current = $lid; 
                        $linkdata[] = $ld;
                     }
                  }break;
                  case 'This': {
                     $tcol = (isset($data->Link))?$data->Link:((isset($this->indexon))?$this->indexon:$this->getpk());
                     $type = (isset($this->selectto))?$this->selectto:$this->tablename;
                     if (isset($data->Link)) {
                        $val = $this->data[$data->Link];
                        if (is_object($val)) $val = $val->Id;
                     } else if (isset($type) && is_object($viewsettings->$type)) {
                        $vset = $viewsettings->$type;
                        $val = $vset->Current;
                     }
                     if (isset($val)) {
                        //$val = (is_object($val))?$val->Id:$val;
                        $link->loaddata($val,$tcol,false);
                        $lid = $link->getid();
                        if (isset($lid)) {
                           $ld->Data = $link->getdata();
                           $ld->Current = $lid; 
                           if (isset($data->View)) {
                              $iviewdata = $link->getview($viewsettings,$data->View);
                              if(isset($iviewdata->LinkedData)) $linkdata = $iviewdata->LinkedData;
                              if(isset($iviewdata->External)) $json->External = $iviewdata->External;
                              $ld->View = $data->View;
                           } 
                           else $linkdata[] = $ld;
                        }
                     }
                  }break;
                  case 'Next': {
                     $link->loadone($val,$tcol,'Next');
                     $lid = $link->data[$link->getpk()];
                     if (isset($lid)) {  
                        $ld->Data = $link->getdata();
                        $ld->Current = $lid; 
                        $linkdata[] = $ld;
                     }
                  }break;
                  case 'Previous': {
                     $link->loadone($val,$tcol,'Previous');
                     $lid = $link->data[$link->getpk()];
                     if (isset($lid)) {  
                        $ld->Data = $link->getdata();
                        $ld->Current = $lid; 
                        $linkdata[] = $ld;
                     }                  
                  }break;
                  case 'All': {
                     $data = $link->select($tcol,$val,$viewsettings);
                     if (count($data) > $link->perpage) {
                        $pages = array_chunk($data,$link->perpage);
                        $page = (isset($osettings) && isset($osettings->Page))
                                    ?$osettings->Page
                                    :1;
                        $ld->ListData = $pages[$page-1];
                        $ld->Page = $page;
                        $ld->Pages = count($pages);
                     } else $ld->ListData = $data;
                     //$ld->Count = count($data);
                     $linkdata[] = $ld;
                  }break;
                  case 'Every': {
                     $data = $link->selectAll($viewsettings);
                     if (count($data) > $link->perpage) {
                        $pages = array_chunk($data,$link->perpage);
                        $page = (isset($osettings) && isset($osettings->Page))
                                    ?$osettings->Page
                                    :1;
                        $ld->ListData = $pages[$page-1];
                        $ld->Page = $page;
                        $ld->Pages = count($pages);
                     } else $ld->ListData = $data;
                     //$ld->Count = count($data);
                     $linkdata[] = $ld;
                  }break;
                  }
               }
            } 
//else {$eff = $link->getdomain();$linkdata[] = "Permission refused for $classname list. ($eff).";}
         }
      }
      if (count($linkdata)>0) $json->LinkedData = $linkdata;
//print "<pre>"; print_r($json); print "</pre>"; exit; 
      return $json;
   }
   function getcolumndef($col) {
      $type = $this->columns[$col];
      $int = null;
      if ($type) {
         if (is_string($type) && preg_match('/^\{/',$type)) {
            $int = json_decode($type);           
         } else {
            $int = new Object();
            $int->DataType = preg_replace('/\:*/','',$type);
         }
      }
      return $int;
   }
   function getderiverequirements() {
      $int = $this->getinterface();
      $req = array();
      foreach ($int as $column => $type) {
         if (is_object($type) && $type->DataType == 'LINKEDTO') {
            $c = $type->TargetType;
            $l = new $c();
            $o = new stdClass();
            $o->DataType = $c;
            $o->Column = $column;
            //$o->Definition = $type;
            $lint = $l->getinterface();
            foreach($lint as $lcol => $ltype) {
               if (is_object($ltype) 
                     && ($ltype->DataType == 'LINKEDTO' 
                        || $ltype->DataType == 'LINKEDTOM')) 
                  unset($lint[$lcol]);
            } 
            $o->Interface = $lint;
            $req[$c] = $o; 
            $d = $l->getderiverequirements();
            $req = array_merge($req,$d);                           
         }         
      }
      return $req;
   }
   function loadderiveddata($crumbs,$icol=null,$ival=null) {
//print "loading data for ".$this->gettablename()."<br/>";
      $int = $this->getinterface();
      $query = $this->dbhandle->select();
      $query->from($this->tablename);
      $pk = $this->getpk();
      $crumbs = (array)$crumbs;
      foreach ($int as $col => $type) {
         if (is_object($type) && $type->DataType == 'LINKEDTO') {
            foreach($crumbs as $ind => $crumb) {
               if ($crumb->IType == $type->TargetType) {
                  //print "<pre>";print_r($type);print "</pre>";
                  $it = $crumb->IType;
                  switch ($crumb->Type) {
                  case 'SameForAll': {
                     $query->where("$col = ?", $crumb->Value);
                  }break;
                  case 'Link': {
                     $link = new $it();
                     $lid = $link->loadderiveddata($crumbs,$crumb->TColumn,$crumb->Value); 
                     $query->where("$col = ?",$lid);
                  }break;
                  } 
               }
            }
         }   
      }
      if (isset($icol) && isset($ival)) $query->where("$icol = ?",$ival);
//echo $query->__toString();
      $this->data = $this->dbhandle->fetchRow($query);
      return $this->getid();          
   }
   function getinterface() {
      $interface = array();
      foreach ($this->columns as $column => $type) {
         switch($type) {
         //case 'Unique Key': break;
         default: {
            if (is_string($type) && preg_match('/^\{/',$type)) {
               $interface[$column] = json_decode($type);           
            } else $interface[$column] = $type;
         }break;
         }
      }
      return $interface;
   }  
   function getimportrequirements() {
      $ireq = array();
      foreach($this->importrequirements as $itype) {
         $link = new $itype();
         $iface = $link->getinterface();
         $ireq[$itype] = $iface;
      }
      return $ireq;
   }                
   function getshow() {
      return $this->show;
   }
   function getpk() {
      foreach($this->columns as $name => $type) if ($type == 'Unique Key') $pk = $name;
      return $pk;
   }
   function getlinkedcolumn($matchtable) {
      $incolumn = null;
      $tocolumn = null;
      foreach($this->columns as $column => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
         }
         $matches = array();
         if (isset($params)) {
            $table = $params->TargetType;
            if ($matchtable == $table) {
               $incolumn = $column;
               $tocolumn = $params->TargetField;
               break;
            }
         } else if (preg_match('/LINKEDTO\:([^\.]+)\.([^\:]+)/',$type,$matches)) {
            $table = $matches[1];
            if ($matchtable == $table) {
               $incolumn = $column;
               $tocolumn = $matches[2];
               break;
            }
         }
      }
      return array($incolumn,$tocolumn);
   }
   function exists() {
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from('pg_tables',array('tablename'=>'tablename'));
      $query->where('tablename = ?',strtolower($this->tablename));
      $resp = $db->fetchAll($query);
      return (count($resp)>0);
   }
   function drop() {
      $statement = "drop table $this->tablename";
      $status = $this->dbhandle->exec($statement);
      return $status;
   }
   function create() {
// CREATE TABLE $tablename(${tablename}_id serial primary key, gridref varchar(20), geom geometry, CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 27700));";      
      global $pending;
      
      if (!isset($pending[$this->tablename]) && isset($this->tablename) && (count($this->columns)>0)) {
         $pending[$this->tablename] = true;
      
         $statement = "CREATE TABLE $this->tablename (";
         $constraints = array();
         $pk = null;
         foreach($this->columns as $name => $type) {
            /* force these back to blank so previous values are not used. */
            $params = null;
            $bits = null;
            if (preg_match('/^\{/',$type)) {
               $params = json_decode($type);
               $type = $params->DataType;
            } else {
               $bits = preg_split('/\:/',$type);
               $type = $bits[0];         
            }
            $suppress = false;
            if ($type == 'Unique Key') $pk = $name;
            if ($type == 'Map Data') $constraints[] = "CONSTRAINT enforce_srid_geom CHECK (st_srid($name) = 4326)";
            $mand = false;
            if ($type == 'LINKEDTO') {
               if (isset($params)) {
                  $table = $params->TargetType;
                  $column = $params->TargetField;
               } else {
                  preg_match('/([^\.]+)\.(.+)/',$bits[1],$matches);
                  $table = $matches[1];
                  $column = $matches[2];
               }
               if ($table) {
                  $dbt = new $table();
                  $tcols = $dbt->getcolumns();
                  $type = $tcols[$column];
                  if ($type == 'Unique Key') $type = 'Linked Key';
               }
            }
            if ($type == 'LINKEDTOM') {
               if (isset($params)) {
                  $table = $params->TargetType;
                  $column = $params->TargetField;
                  $property = (isset($params->Property))?$params->Property:$name;
               } else {
                  preg_match('/([^\.]+)\.(.+)/',$bits[1],$matches);
                  $table = $matches[1];
                  $column = $matches[2];
                  $property = $name;
               }
               $suppress = true;
               $dbt = new $table();
               if (!$dbt->exists()) $dbt->create();
               $tcols = $dbt->getcolumns();
               $ttype = $tcols[$column];
               if (preg_match('/^\{/',$ttype)) {
                  $params = json_decode($ttype);
                  $ttype = $params->DataType;
               } else {
                  $bits = preg_split('/\:/',$ttype);
                  $ttype = $bits[0];         
               }
               $lt = new DBT_Lookup($property,$this->tablename,$pk,$table,$column);
               if (!$lt->exists()) $lt->create();
            }
            if ($type == 'INTERFACE') {
               $type = 'Interface';            
            } 
            $def = $this->datatypes[$type];
            if ((isset($params) && isset($params->Mandatory))||($bits[count($bits)-1] == 'Mandatory')) $def .= " NOT NULL";
            if (!$suppress) $statement .= "$name $def,";  
         }
         
         /* 
            have to add Version fields before issuing create table statement 
            but have to create link tables after creating the source table. 
         */
         foreach($this->show as $classname) {
            if (preg_match('/([^:]+)/',$classname,$m)) {
               $classname = $m[1];
               $link = new $classname();
               if (!$link->exists()) $link->create();            
               $cols = $link->getinterface();
               foreach ($cols as $col => $type) {
                  if (is_object($type)) { 
                     $params = $type;
                     $type = $params->DataType;
                  } else if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $type = $params->DataType;
                  } else $type = preg_replace('/\:.+$/','',$type);
                  switch($type) {
                  case 'LINKEDTO': {
                     $lc = $params->TargetType;
                     $l2 = new $lc;
                     if (!$l2->exists()) $l2->create(); 
                  }break;      
                  case 'Version': {
                     $column = $link->getdisplayname();
                     $column = preg_replace('/\s+/','_',$column);
                     $dtype = $this->datatypes['Version'];
                     $statement .= "$column $dtype,";
                  }break;
                  }
               }
            }            
         }
         
         $statement .= "CreatedBy INT,";
         $statement .= "CreatedOn TIMESTAMP";
         if (count($constraints)>0) {
            $cs = join(', ',$constraints);
            $statement .= ", $cs";
         }
         $statement .= ");";
         
         $status = $this->dbhandle->exec($statement);
         
         /*foreach($this->show as $classname) {
            if (preg_match('/([^:]+)/',$classname,$m)) {
               $classname = $m[1];
               $link = new $classname();
               if (!$link->exists()) $link->create();            
               $cols = $link->getinterface();
               foreach ($cols as $col => $type) {
                  if (is_object($type)) { 
                     $params = $type;
                     $type = $params->DataType;
                  } else if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $type = $params->DataType;
                  } else $type = preg_replace('/\:.+$/','',$type);
                  switch($type) {
                  case 'LINKEDTO': {
                     $lc = $params->TargetType;
                     $l2 = new $lc;
                     if (!$l2->exists()) $l2->create(); 
                  }break;      
                  }
               }
            }            
         } */
         
         if ($this->primingdata) $this->prime();
         if ($this->deriveddata) {
            foreach($this->deriveddata as $view) {
               $link = new $view();
               if (!$link->exists()) $link->create();
            }
         }
         unset($pending[$this->tablename]);
      }                                    
      return $status; 
   }
   function prime() {
      if (isset($this->primingdata)) {
         $this->validated = true;
         foreach ($this->primingdata as $classname => $data) {
            $data = trim($data);
            $data = preg_split('/\n/',$data);
            foreach($data as $line => $text) {
               if ($line == 0) $cols = $this->parselinecsv($text);
               else {
                  $idata = array();
                  $vals = $this->parselinecsv($text); 
                  foreach($cols as $colnum => $col) {
                     $idata[$col] = $vals[$colnum]; 
                  }
                  if ($classname == $this->tablename) {
                     $this->setdata($idata);
                     $this->insert();                 
                  } else {
                     $link = null;
                     $link = new $classname();
                     if (isset($link)) {
                        if (!$link->exists()) $link->create();
                        $link->validated = true;
                        $link->setdata($idata);
                        $link->insert();
                     }
                  }
               }               
            }                              
         }
      }
      $this->data = array();
   }
   function initialise() {
      if ($this->defaultpermissions) {
         $this->validated = true;
         foreach ($this->defaultpermissions as $classname => $data) {
            $data = trim($data);
            $data = preg_split('/\n/',$data);
            foreach($data as $line => $text) {
               if ($line == 0) $cols = $this->parselinecsv($text);
               else {
                  $idata = array();
                  $vals = $this->parselinecsv($text); 
                  foreach($cols as $colnum => $col) {
                     $idata[$col] = $vals[$colnum]; 
                  }
                  if ($classname == $this->tablename) {
                     $this->setdata($idata);
                     if (!$this->isduplicate()) $this->insert();                 
                  } else {
                     $link = null;
                     $link = new $classname();
                     if (isset($link)) {
                        if (!$link->exists()) $link->create();
                        $link->validated = true;
                        $link->setdata($idata);
                        if (!$link->isduplicate()) $link->insert();
                     }
                  }
               }               
            }                              
         }
      }
      $this->data = array();
   }
   function parselinecsv($string) {
      $elements = array();
      if (preg_match("/\"/",$string)) {
         $bits = split('"',$string);
         for ($i=0;$i<count($bits);$i++) {
            $bit = $bits[$i];
            $bit = preg_replace("/\s+$/","",$bit);
            if ($i%2==1) {
               $elements[] = $bit;
            } else if ($bit != ',' && strlen($bit)>0) {
               $s = $bit;
               $s = preg_replace("/^,/","",$s);
               $s = preg_replace("/,$/","",$s);
               $elements = array_merge($elements,split(",",$s));
            }
         }
      } else {
         $elements = split(",",$string);
      }
      return $elements;
   }
   function preformatterm($col,$type,$val) {}
   function insertterm($col,$type,$val) {
      switch($type) {
      case 'Map Data': {
         $val = new Zend_Db_Expr("st_geomfromtext('$val',4326)");   
      }break;
      default: {
         if (isset($val) && is_object($val)) $val = json_encode($val);
      }break;
      }
      return $val;
   }
   function selectterm($col,$type) {
      $val = null;
      $lcol = strtolower($col);
      switch ($type) {
      case 'Map Data': {
         $val = new Zend_Db_Expr("ST_AsText($lcol)");
      }break;
      default: {
         $val = $lcol;   
      }break;
      }
      return $val;
   }
   function postprocessterm($col,$type,$val,$params=null) {
      switch ($type) {
      case 'True or False': {
         $val = ($val == true)?1:0;
      }break;
      case 'Password': {
         $k = new KLib($this->getsid());
         $keys = json_decode($k->getkeys());
         $val = encrypt($val,$keys->k1,$keys->k2);
      }break;
      case 'Encrypted Text': {
         $k = new KLib($this->getsid());
         $keys = json_decode($k->getkeys());
         $val = encrypt($val,$keys->k1,$keys->k2);
      }break;
      case 'Date': {
         if (preg_match('/(\d+)\-(\d+)\-(\d+)/',$val,$matches)) {
            $y = $matches[1];
            $m = $matches[2];
            $d = $matches[3];
            if (intval($y)+intval($m)+intval($d)>0) {
               $val = "$d/$m/$y";
            } else $val = ''; 
         }
      }break;
      case 'LINKEDTO': {
         if (isset($params)) {
            if (!is_object($val)) {
               $type = $params->DataType;
               $table = $params->TargetType;
               $column = $params->TargetField;
               $link = null;
               $link = new $table();
               $link->loaddata($val,$column,false);
               $obj = new stdClass();
               $obj->Name = $link->getname();
               $obj->Id = $link->getid();
               $val = $obj;
            }
         }   
      }break;
      case 'LINKEDTOM': {
         if (isset($params)) {
            if (!is_object($val)) {
               $type = $params->DataType;
               $ttab = $params->TargetType;
               $tcol = $params->TargetField;
               $p = (isset($params->Property))?$params->Property:$col;
               $pk = $this->getpk();
               $id = $this->getid();
               $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
               $obj = new stdClass();
               $obj->IType = $ttab;
               $obj->ListData = $ltm->selecttargets($pk,$row[$pk]);
               $val = $obj;
            }
         }   
      }break;
      case 'INTERFACE': {
         if ($val > 0) { 
            $cache = new G_Cache();
            $cache->loadone($val);
            $val = $cache->getdata();
         }
      }break;
      case 'Taxa': {
         if ($val > 0) { 
            $cache = new G_Cache();
            $cache->loadone($val);
            $val = $cache->getdata();
         }
      }break;
      }
      return $val;
   }
   function validate() {
      foreach($this->columns as $cname => $ctype) {
         if (isset($this->data[$cname]) && $this->data[$cname] == '') $this->data[$cname] = null; 
         if (preg_match('/^\{/',$ctype)) {
            $params = json_decode($ctype);
            $ctype = $params->DataType;
         } else {
            $bits = preg_split('/\:/',$ctype);
            $ctype = $bits[0];
         }
         switch($ctype) {
         case 'Number': {
            if (isset($this->data[$cname])) $this->data[$cname] = intval($this->data[$cname]);
         }break;
         case 'Decimal': {
            if (isset($this->data[$cname])) $this->data[$cname] = floatval($this->data[$cname]);
         }break;
         case 'Date': {
            if (preg_match('/(\d{2})\/(\d{2})\/(\d{4})/',$this->data[$cname],$matches)) {
               $this->data[$cname] = $matches[3]."-".$matches[2]."-".$matches[1];
            } 
         }break;
         case 'Time': {
            //?
         }break;
         case 'Date and Time': {
            // Timestamps should always default to now. 
            if (!isset($this->data[$cname])) $this->data[$cname] = new Zend_Db_Expr('NOW()');
         }break;
         case 'Password': {
            $k = new KLib($this->getsid());
            $keys = json_decode($k->getkeys());
            $this->data[$cname] = decrypt($this->data[$cname],$keys->k1,$keys->k2);
         }break;
         case 'Encrypted Text': {
            $k = new KLib($this->getsid());
            $keys = json_decode($k->getkeys());
            $this->data[$cname] = decrypt($this->data[$cname],$keys->k1,$keys->k2);
         }break;
         case 'INTERFACE': {
            $this->data[$cname] = json_decode($this->data[$cname]);
         }break;
         case 'LINKEDTO': {
            if (is_object($this->data[$cname])) $this->data[$cname] = $this->data[$cname]->Id;
         }break;
         case 'Taxa': {
            $this->data[$cname] = json_decode($this->data[$cname]);
         }break;
         default: {
            if (isset($this->data[$cname]) && is_object($this->data[$cname])) $this->data[$cname] = json_encode($this->data[$cname]);
         }break;
         }
         $this->validated = true;
      }
      $this->data['CreatedBy'] = $this->user->getid();
      //$this->data['CreatedOn'] = new Zend_Db_Expr('NOW()');
   }
   function setid($id) {
      $pk = $this->getpk();
      $this->data[$pk] = $id;
   } 
   function getid() {
      $pk = $this->getpk();
      $id = $this->data[$pk]; 
      $id = (is_object($id))?$id->Id:$id; 
      return $id;
   }
   function pgnativeinsert($id,$data) {
   /*
      This function has been included because the PDO abstracted library fails 
      to insert geometry data. This could be removed if a later version of 
      PHP fixes this problem. 
   */
      global $dbConfig, $dbPlatform;
      
      // pg native uses user in the connection string rather than username;
      $pgConfig = array();
      foreach($dbConfig as $key => $val) {
         switch($key) {
         case 'username': $pgConfig['user'] = $val; break;
         default: $pgConfig[$key] = $val;
         }
      }      
      
      $data[strtolower($this->getpk())] = $id;
      $tname = strtolower($this->tablename);
      $pgcopts = array();
      foreach ($pgConfig as $key => $val) $pgcopts[] = "${key}=${val}";
      $pgcstring = join(' ',$pgcopts);
      $dbpg = pg_connect($pgcstring);
      $res = null;
      if (isset($dbpg)) {
         $cols = array();
         $vals = array();
         foreach($data as $col => $val) {
            // get text statement from Zend_Db_Expr() instance //
            if (is_object($val)) $v = $val->__toString();
            // leave numeric data unquoted // 
            else if (is_numeric($val)) $v = $val; 
            else if (is_bool($val)) $v = ($val)?'true':'false';
            // and then quote text data for the insert statement //
            else $v = (isset($val) && strlen($val)>0)?"'$val'":'null';
            
            $cols[] = $col;
            $vals[] = $v;
            // update data array for pg_insert call if using // 
            if (isset($v)) $data[$col] = $v; 
         }
         $colstring = join(',',$cols);           
         $valstring = join(',',$vals);           
         $sql = "INSERT INTO $tname ($colstring) VALUES ($valstring);";
//print "$sql\n";exit;
         $res = pg_query($dbpg,$sql);
         //$res = pg_insert($dbpg,$tname,$data);
         pg_close($dbpg);
      }
      return $res;
   }                                    
   function insert() {
      $updates = array();
      $ltmd = array();
      if (!$this->validated) $this->validate();
      $data = array();
      $multiples = array();
      $total = 1;
      $counts = array();
      foreach($this->columns as $col => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
         } else $type = preg_replace('/\:.+/','',$type);
         if (isset($this->data[$col]) && $this->data[$col] == 'null') unset($this->data[$col]);
         switch ($type) {
         case 'Map Data': {
            if (isset($this->data[$col]) && is_string($this->data[$col])) {
               $wkt = $this->data[$col];
               //$this->data[$col] = new Zend_Db_Expr("ST_GeomFromText(\"$wkt\",4326)");
               $this->data[$col] = $this->insertterm($col,$type,$wkt);
            }
         }break;
         case 'True or False': {
            if (is_numeric($this->data[$col])) $this->data[$col] = ($this->data[$col] == 1);
         }break;
         case 'INTERFACE': {
            if (isset($this->data[$col]) && is_object($this->data[$col])) {
               $obj = $this->data[$col];
               $cache = array();
               $cache['Data_Type'] = $obj->Data_Type;
               $cache['Name'] = $obj->Name;
               $cache['Value_Type'] = (is_integer($obj->Value))?'Number':'Short Text';
               $cache['Value'] = $obj->Value;
               $cache['Domain'] = $obj->In_Domain; 
               $cache['JSON'] = json_encode($obj);
               $c = new G_Cache();
               $c->setdata($cache);
               $interfaceid = $c->insert();
               $this->data[$col] = $interfaceid;
            }
         }break;
         case 'Taxa': {
            if (isset($this->data[$col]) && is_object($this->data[$col])) {
               $obj = $this->data[$col];
               $cache = array();
               $cache['Data_Type'] = $obj->Data_Type;
               $cache['Name'] = $obj->Name;
               $cache['Value_Type'] = (is_integer($obj->Value))?'Number':'Short Text';
               $cache['Value'] = $obj->Value;
               $cache['Domain'] = $obj->In_Domain; 
               $cache['JSON'] = json_encode($obj);
               $c = new G_Cache();
               $c->setdata($cache);
               $interfaceid = $c->insert();
               $this->data[$col] = $interfaceid;
            }
         }break;
         case 'LINKEDTO': {
            if (isset($this->data[$col])) {
               if (is_object($this->data[$col])) {
                  $this->data[$col] = $this->data[$col]->Id;
               } else if (!is_array($this->data[$col]) && preg_match('/^\[.+\]$/',$this->data[$col])) {
                  $this->data[$col] = json_decode($this->data[$col]);
                  if(count($this->data[$col])>0) {
                     $multiples[] = $col;
                     $total *= count($this->data[$col]);
                  }
               } else if (is_array($this->data[$col]) && count($this->data[$col])>0) {
                  $multiples[] = $col;
                  $total *= count($this->data[$col]);
               } 
            }
         }break;
         case 'LINKEDTOM': {
         // Have to insert data in order to get the primary key before 
         // inserting the linked to data
            $ltmd[$col] = $this->data[$col];
            unset($this->data[$col]); 
         }break;
         case 'Password': {
            if (!$this->validated) {
               $k = new KLib($this->getsid());
               $keys = json_decode($k->getkeys());
               $this->data[$col] = decrypt($this->data[$col],$keys->k1,$keys->k2);
            } 
         }break;
         case 'Encrypted Text': {
            if (!$this->validated) {
               $k = new KLib($this->getsid());
               $keys = json_decode($k->getkeys());
               $this->data[$col] = decrypt($this->data[$col],$keys->k1,$keys->k2);
            }
         }break;
         case 'Version': {
            /* 
               If there is a version column you need to check whether there is
               a value already and if there isn't then you need to update the 
               parent VersionOf table with the newly inserted version.
            */
            if (isset($params->VersionOfType) && ($this->data[$col] == 1)) {
               $type = $params->VersionOfType;
               $field = $params->VersionOfField;
               $link = null; 
               $link = new $type();
               $link->updateversion($this->data[$field],$this->tablename,1);
            }
         }break;
         }         
      }
      if ($total>1) {
         $inserts = array();
         $data = $this->data;
         for($i=0;$i<$total;$i++) {
            $tdata = $data;
            $divor = 1;
            foreach($multiples as $prop) {
               $moder = count($data[$prop]); 
               $ind = intval($i/$divor)%$moder;
               $tdata[$prop] = $data[$prop][$ind];
               $divor *= $moder;
            }
            $inserts[] = $tdata;
         }
         foreach($inserts as $data) {
            $this->setdata($data);
            //$this->dbhandle->insert(strtolower($this->tablename),array_change_key_case($this->data,CASE_LOWER));
            $pk = $this->getpk();
            $sq = strtolower($this->tablename).'_'.strtolower($pk).'_seq';
            $id = $this->dbhandle->nextSequenceId($sq);
            $r = $this->pgnativeinsert($id,$data);
            
            //$id = $this->dbhandle->lastInsertId(); // May not work in PostGIS
            //$sq = strtolower($this->tablename).'_'.strtolower($pk).'_seq';
            //$id = $this->dbhandle->lastSequenceId($sq);
            
            if (isset($id) && $r) {
               $this->data[$pk] = $id;
               $this->id = $id;
               $status = $id;
               foreach($ltmd as $col => $val) {
                  $type = $this->columns[$col];
                  if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $type = $params->DataType;
                  } else $type = preg_replace('/\:.+/','',$type);
                  $pk = $this->getpk();
                  if (isset($params)) {
                     $ttab = $params->TargetType;
                     $tcol = $params->TargetField;
                     $p = (isset($params->Property))?$params->Property:$col;
                  } else {
                     preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                     $ttab = $matches[2];
                     $tcol = $matches[3];
                     $p = $col;
                  }
                  $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                  $ltm->delete($pk,$id);
                  $vals = array();
                  if (preg_match('/^\[.+\]$/',$val)) {$vals = json_decode($val);} 
                  else if (is_array($val)) {$vals = $val;} 
                  else {$vals[] = $val;}
                  foreach($vals as $v) { 
                     $data = array(
                        $pk   => $id,
                        $tcol => $v
                     );
                     $ltm->setdata($data);
                     $ltm->insert();
                  }
               }
            }
            $message = ($status==0)?'Insert data failed':'Data inserted successfully';
            $obj = $this->getview($settings);
            $obj->Status = $status;
            $obj->Message = $message;
         }
      } else {
      //error_reporting(E_ALL);       
         $t = strtolower($this->tablename);
         $d = array_change_key_case($this->data,CASE_LOWER);
      //print $t;
      //print_r($d);exit;
         //$this->dbhandle->insert($t,$d);
         $pk = $this->getpk();
         $sq = strtolower($this->tablename).'_'.strtolower($pk).'_seq';
         $id = $this->dbhandle->nextSequenceId($sq);
         $r = $this->pgnativeinsert($id,$d);
                  
         $pk = $this->getpk();
         //$id = $this->dbhandle->lastInsertId(); // May not work in PostGIS
         //$sq = strtolower($this->tablename).'_'.strtolower($pk).'_seq';
         //$id = $this->dbhandle->lastSequenceId($sq);
          
         if (isset($id) && $r) {
            $this->data[$pk] = $id;
            $this->id = $id;
            $status = $id;
            foreach($ltmd as $col => $val) {
               $type = $this->columns[$col];
               if (preg_match('/^\{/',$type)) {
                  $params = json_decode($type);
                  $type = $params->DataType;
               } else $type = preg_replace('/\:.+/','',$type);
               $pk = $this->getpk();
               if (isset($params)) {
                  $ttab = $params->TargetType;
                  $tcol = $params->TargetField;
                  $p = (isset($params->Property))?$params->Property:$col;
               } else {
                  preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                  $ttab = $matches[2];
                  $tcol = $matches[3];
                  $p = $col;
               }
               $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
               $ltm->delete($pk,$id);
               $vals = array();
               if (preg_match('/^\[.+\]$/',$val)) {$vals = json_decode($val);} 
               else if (is_array($val)) {$vals = $val;} 
               else {$vals[] = $val;}
               foreach($vals as $v) {
                  if ($id != null && $v != null) { 
                     $data = array(
                        $pk   => $id,
                        $tcol => $v
                     );
                     $ltm->setdata($data);
                     $ltm->insert();
                  }
               }
            }
         }
      }
      return $status;   
   }
   function delete($col=null,$val=null) {
      $lcol = strtolower($col);
      if (isset($val)&&isset($col)) {
         $clause = $this->dbhandle->quoteInto("$lcol = ?",$val);
      } else {
         $pk = $this->getpk();
         $lpk = strtolower($pk);
         $clause = $this->dbhandle->quoteInto("$lpk = ?",$this->data[$pk]);
      }
      // if $col is set it has to exist as a column of the table.
      if (!isset($col)|| isset($this->columns[$col])) {
         $this->cascadedelete();
         $status = $this->dbhandle->delete(strtolower($this->tablename),$clause);
      }
      return $status;
   }
   function cascadedelete() {
      $pk = $this->getpk();
      $id = $this->getid();
      $ctab = $this->tablename;
      // 1st delete from any LTM column data  
      foreach($this->columns as $col => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
         } else $type = preg_replace('/\:.+/','',$type);
         if (isset($this->data[$col]) && $this->data[$col] == 'null') unset($this->data[$col]);
         switch ($type) {
         case 'LINKEDTOM': {
            if (isset($params)) {
               $ttab = $params->TargetType;
               $tcol = $params->TargetField;
               $p = (isset($params->Property))?$params->Property:$col;
            } else {
               preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
               $ttab = $matches[2];
               $tcol = $matches[3];
               $p = $col;
            }
            $ltmd[$col] = $this->data[$col];
            $ltm = new DBT_Lookup($p,$ctab,$pk,$ttab,$tcol);
            $ltm->delete($pk,$id);
         }break;
         }
      }
      // 2nd delete from tables named in show list
      if (is_array($this->show)) {
         foreach($this->show as $tab) {
            $link = new $tab();
            list($icol,$tcol) = $link->getlinkedcolumn($tab);
            $val = ($tcol != $pk)?$this->data[$tcol]:$id;
            $link->delete($icol,$val);                                                             
         } 
      }
      
      if (isset($this->view)) {
         $v = (isset($this->view))?json_decode($this->view):null;
         foreach ($v as $i => $vi) {
            $show = $view->Data;
            foreach ($show as $data) {
               $tab = $data->Table;
               $link = new $tab();
               list($icol,$tcol) = $link->getlinkcolumn($tab);
               $val = ($tcol != $pk)?$this->data[$tcol]:$id;
               $link->delete($icol,$val);   
            }
         }
      }
   }
   function deleteAll() {
      $status = $this->dbhandle->delete($this->tablename);
      return $status;
   }
   function pgnativeupdate($icol,$id,$data) {
   /*
      This function has been included because the PDO abstracted library fails 
      to insert geometry data. This could be removed if a later version of 
      PHP fixes this problem. 
   */
      global $dbConfig, $dbPlatform;
      
      // pg native uses user in the connection string rather than username;
      $pgConfig = array();
      foreach($dbConfig as $key => $val) {
         switch($key) {
         case 'username': $pgConfig['user'] = $val; break;
         default: $pgConfig[$key] = $val;
         }
      }      
      
      $data[strtolower($this->getpk())] = $id;
      $tname = strtolower($this->tablename);
      $pgcopts = array();
      foreach ($pgConfig as $key => $val) $pgcopts[] = "${key}=${val}";
      $pgcstring = join(' ',$pgcopts);
      $dbpg = pg_connect($pgcstring);
      $res = null;
      if (isset($dbpg)) {
         $cols = array();
         $vals = array();
         $udts = array();
         foreach($data as $col => $val) {
            $cols[] = $col;
            
            // get text statement from Zend_Db_Expr() instance //
            if (is_object($val)) $v = $val->__toString();
            // leave numeric data unquoted // 
            else if (is_numeric($val)) $v = $val; 
            else if (is_bool($val)) $v = ($val)?'true':'false';
            // and then quote text data for the insert statement //
            else $v = (isset($val) && strlen($val)>0)?"'$val'":'null';
            $vals[] = $v;
            // update data array for pg_insert call if using // 
            $data[$col] = $v;
            $lcol = strtolower($col);
            $udts[] = "$lcol = $v"; 
         }
         $udtstring = join(',',$udts);     
         $licol = strtolower($icol);      
         $sql = "UPDATE $tname SET $udtstring WHERE($licol = $id);";
         $res = pg_query($dbpg,$sql);
         //$res = pg_update($dbpg,$tname,$data);
         pg_close($dbpg);
      }
      return $res;
   }                                    
   function update($col,$val) {
      if (!$this->validated) $this->validate();
      if (isset($this->data['CreatedBy'])) unset($this->data['CreatedBy']);
      if (isset($this->data['CreatedOn'])) unset($this->data['CreatedOn']);
      foreach($this->columns as $dcol => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;         
         } else $type = preg_replace('/\:.+/','',$type);
         if ($params->Hidden) {
            unset($this->data[$dcol]);
         } else {
            switch ($type) {
            case 'Unique Key': {
               unset($this->data[$dcol]);
            }break;
            case 'Map Data': {
               if (isset($this->data[$dcol]) && is_string($this->data[$dcol])) {
                  $wkt = $this->data[$dcol];
                  //$this->data[$dcol] = new Zend_Db_Expr("st_geomfromtext('$wkt',4326)");
                  $this->data[$dcol] = $this->insertterm($dcol,$type,$wkt);
               }
            }break;
            case 'True or False': {
               if (is_numeric($this->data[$col])) $this->data[$col] = ($this->data[$col] == 1);
            }break;
            case 'INTERFACE': {
               if (isset($this->data[$dcol]) && is_object($this->data[$dcol])) {
                  $obj = $this->data[$dcol];
                  $cache = array();
                  $cache['Data_Type'] = $obj->Data_Type;
                  $cache['Name'] = $obj->Name;
                  $cache['Value_Type'] = (is_integer($obj->Value))?'Number':'Short Text';
                  $cache['Value'] = $obj->Value;
                  $cache['Domain'] = $obj->In_Domain; 
                  $cache['JSON'] = json_encode($obj);
                  $c = new G_Cache();
                  $c->setdata($cache);
                  $interfaceid = $c->insert();
                  $this->data[$dcol] = $interfaceid;
               }
            }break;
            case 'Taxa': {
               if (isset($this->data[$dcol]) && is_object($this->data[$dcol])) {
                  $obj = $this->data[$dcol];
                  $cache = array();
                  $cache['Data_Type'] = $obj->Data_Type;
                  $cache['Name'] = $obj->Name;
                  $cache['Value_Type'] = (is_integer($obj->Value))?'Number':'Short Text';
                  $cache['Value'] = $obj->Value;
                  $cache['Domain'] = $obj->In_Domain; 
                  $cache['JSON'] = json_encode($obj);
                  $c = new G_Cache();
                  $c->setdata($cache);
                  $interfaceid = $c->insert();
                  $this->data[$dcol] = $interfaceid;
               }
            }break;
            case 'LINKEDTOM': {
               $lookuptable = $this->tablename."_${dcol}";
               $pk = $this->getpk();
               if (isset($params)) {
                  $ttab = $params->TargetType;
                  $tcol = $params->TargetField;
                  $p = (isset($params->Property))?$params->Property:$dcol;
               } else {
                  preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$dcol],$matches);
                  $ttab = $matches[2];
                  $tcol = $matches[3];
                  $p = $dcol;
               }
               $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
               $ltm->delete($pk,$val);
               $data = array(
                  $pk   => $val,
                  $tcol => $this->data[$dcol]
               );
               if (is_array($data[$tcol])) {
                  $ivals = $data[$tcol];
                  foreach ($ivals as $ival) {
                     $idata = $data;
                     $idata[$tcol] = $ival;
                     if ($val != null && $ival != null) {
                        $ltm->setdata($idata);
                        $ltm->insert();
                     }
                  }
               } else if (preg_match('/\[/',$data[$tcol])) {
                  $ivals = json_decode($data[$tcol]);
                  foreach ($ivals as $ival) {
                     $idata = $data;
                     $ival = (is_object($ival))?$ival->Id:$ival;
                     $idata[$tcol] = $ival;
                     if ($val != null && $ival != null) {
                        $ltm->setdata($idata);
                        $ltm->insert();
                     }
                  }
               } else {
                  $ival = $this->data[$dcol]; 
                  if ($val != null && $ival != null) {
                     $ltm->setdata($data);
                     $ltm->insert();
                  }
               }
               unset($this->data[$dcol]); 
            }break;
            case 'Password': {
               if (!$this->validated) {
                  $k = new KLib($this->getsid());
                  $keys = json_decode($k->getkeys());
                  $this->data[$dcol] = decrypt($this->data[$dcol],$keys->k1,$keys->k2);
               }
            }break;
            case 'Encrypted Text': {
               if (!$this->validated) {
                  $k = new KLib($this->getsid());
                  $keys = json_decode($k->getkeys());
                  $this->data[$dcol] = decrypt($this->data[$dcol],$keys->k1,$keys->k2);
               }
            }break;
            }
         }
      }
      //$where = $this->dbhandle->quoteInto("$col = ?",$val);
      //$status = $this->dbhandle->update($this->tablename,$this->data,$where);
      $status = $this->pgnativeupdate($col,$val,$this->data);
      $this->data[$col] = $val;
      return $status;
   }
   function loaddata($id,$col=null,$innerdata=true) {
      $ismatched = false;
      if (!isset($col)) $col = $this->getpk();
      // NEED TO MAKE THIS WORK FOR {NAME,ID} AND FOR GENUINE OBJ SEARCH
      if (is_object($id)) $id = json_encode($id);
      if (isset($id) && $this->exists()) {
         $pk = $this->getpk();
         if (!$col) $col = $pk; 
         if (is_object($id)) $id = $id->Id;
         $query = $this->dbhandle->select();
         $cols = array();
         $ltmd = array();
         foreach ($this->columns as $dcol => $type) {
            if (preg_match('/^\{/',$type)) {
               $params = json_decode($type);
               $type = $params->DataType;         
            } else $type = preg_replace('/\:.+/','',$type);
            if (!isset($params->Hidden) || !$params->Hidden) {
               switch ($type) {
               case 'Map Data': {
                  //$cols[$dcol] = new Zend_Db_Expr("ST_AsText($dcol)");
                  $cols[$dcol] = $this->selectterm($dcol,$type);               
               } break;
               case 'LINKEDTOM': {
                  if ($innerdata) {
                     if (isset($params)) {
                        $ttab = $params->TargetType;
                        $tcol = $params->TargetField;
                        $p = (isset($params->Property))?$params->Property:$dcol;
                     } else {
                        preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$dcol],$matches);
                        $ttab = $matches[2];
                        $tcol = $matches[3];
                        $p = $dcol;
                     }
                     $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                     $obj = new stdClass();
                     $obj->IType = $ttab;
                     $obj->Current = 'Multiple';
                     $obj->Key = $tcol;
                     $obj->ListData = $ltm->selecttargets($pk,$id);
                     $ltmd[$dcol] = $obj;
                  }
               }break;
               default: {
                  //$cols[$dcol] = $dcol;
                  $cols[$dcol] = $this->selectterm($dcol,$type); 
               } break;
               }
            }   
         }
            
         $query->from(strtolower($this->tablename), $cols);
         $lcol = strtolower($col);
         $query->where("$lcol = ?",$id);
//echo "<pre>" . $query->__toString() . "</pre>";exit;         
         $this->data = $this->dbhandle->fetchRow($query);
         if (count($ltmd)) {
            foreach($ltmd as $dcol => $dval) {
               $this->data[$dcol] = $dval;
            }
         }
         $ismatched = (is_array($this->data) && $this->getid()>0);
         if ($ismatched) {
            foreach($this->data as $col => $val) {
               $type = $this->columns[$col];
               if (preg_match('/^\{/',$type)) {
                  $params = json_decode($type);
                  $type = $params->DataType;         
               } else $type = preg_replace('/\:.+/','',$type);
               $this->data[$col] = $this->postprocessterm($col,$type,$val);   
               /*
               switch($type) {
               case 'True or False': {
                  $this->data[$col] = ($val == true)?1:0;
               }break;
               case 'Password': {
                  $k = new KLib($this->getsid());
                  $keys = json_decode($k->getkeys());
                  $this->data[$col] = encrypt($val,$keys->k1,$keys->k2);
               }break;
               case 'Encrypted Text': {
                  $k = new KLib($this->getsid());
                  $keys = json_decode($k->getkeys());
                  $this->data[$col] = encrypt($val,$keys->k1,$keys->k2);
               }break;
               case 'INTERFACE': {
                  if ($val > 0) {
                     $cache = new G_Cache();
                     $cache->loaddata($val);
                     $this->data[$col] = (object)$cache->getdata();
                  }
               }break;
               case 'Taxa': {
                  if ($val > 0) {
                     $cache = new G_Cache();
                     $cache->loaddata($val);
                     $this->data[$col] = (object)$cache->getdata();
                  }
               }break;
               default: {
                  $this->data[$col] = $this->postprocessterm($col,$type,$val);   
               }break;
               }
               */
            } 
         }
         $this->id = $this->getid();
      }
      return $ismatched;
   }
   function loadone($id,$col=null,$vtype=null,$byval=null) {
      if (is_object($id)) $id = json_encode($id);
      if ($id > 0 && $this->exists()) {
         $pk = $this->getpk();
         if (!$col) $col = $pk;
         $lcol = strtolower($col); 
         //if (is_object($id)) $id = $id->Id;
         $query = $this->dbhandle->select();
         $cols = array();
         $ltmd = array();
         foreach ($this->columns as $dcol => $type) {
            $ldcol = strtolower($dcol);
            if (preg_match('/^\{/',$type)) {
               $params = json_decode($type);
               $type = $params->DataType;         
            } else $type = preg_replace('/\:.+/','',$type);
            if (!$params->Hidden) {
               switch ($type) {
               case 'Map Data': {
                  $cols[$dcol] = new Zend_Db_Expr("ST_AsText($ldcol)");               
               } break;
               case 'LINKEDTOM': {
                  // insert linkedtom management
                  if (isset($params)) {
                     $ttab = $params->TargetType;
                     $tcol = $params->TargetField;
                     $p = (isset($params->Property))?$params->Property:$dcol;
                  } else {
                     preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                     $ttab = $matches[2];
                     $tcol = $matches[3];
                     $p = $dcol;
                  }
                  $pk = $this->getpk();
                  $lid = $this->getid();
                  $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                  $obj = new stdClass();
                  $obj->IType = $ttab;
                  $obj->ListData = $ltm->selecttargets($pk,$lid);
                  $ltmd[$dcol] = $obj;
               } break;
               default: {
                  $cols[$dcol] = $ldcol; 
               } break;
               }
            }   
         }
            
         $query->from(strtolower($this->tablename), $cols);
         $query->where("$lcol = ?",$id);
         switch($vtype) {
         case 'Current': {
            $vercol = null;
            foreach ($this->columns as $dcol => $ctype) {
               if (preg_match('/^\{/',$ctype)) {
                  $cparams = json_decode($ctype);
                  $ctype = $cparams->DataType;         
               } else $ctype = preg_replace('/\:.+/','',$ctype);
               if ($ctype == 'Version') $vercol = $dcol;
            }
            if (isset($vercol)&&isset($byval)) {
            /* For some reason = will not always match float values 
               so it is necessary to add 2 clauses to sandwich the required 
               version number */ 
               $byval = floatval($byval);
               $lvercol = strtolower($vercol);
               $query->where("$lvercol > ?", $byval-0.05);
               $query->where("$lvercol < ?", $byval+0.05);
            }
         }break;
         case 'Last': {
            $countcol = null;
            foreach ($this->columns as $dcol => $ctype) {
               if (preg_match('/^\{/',$ctype)) {
                  $cparams = json_decode($ctype);
                  $ctype = $cparams->DataType;         
               } else $ctype = preg_replace('/\:.+/','',$ctype);
               if ($ctype == 'Count') $countcol = $dcol;
            }
            if (isset($countcol)) {
               $lcountcol = strtolower($countcol);
               $query->order("$lcountcol DESC");
            }
         }break;
         case 'Next': {
            $datecol = null;
            foreach ($this->columns as $dcol => $ctype) {
               if (preg_match('/^\{/',$ctype)) {
                  $cparams = json_decode($ctype);
                  $ctype = $cparams->DataType;         
               } else $ctype = preg_replace('/\:.+/','',$ctype);
               if ($ctype == 'Date') $datecol = $dcol;
            }
            if (isset($datecol)) {
               $ldatecol = strtolower($datecol);   
               $query->where("$ldatecol >= CURDATE()");//?", new Zend_Db_Expr("CURDATE()"));
               $query->order("$ldatecol ASC");
            }
         }break;
         case 'Previous': {
            $datecol = null;
            foreach ($this->columns as $dcol => $ctype) {
               if (preg_match('/^\{/',$ctype)) {
                  $cparams = json_decode($ctype);
                  $ctype = $cparams->DataType;         
               } else $ctype = preg_replace('/\:.+/','',$ctype);
               if ($ctype == 'Date') $datecol = $dcol; 
            }
            if (isset($datecol)) {
               $ldatecol = strtolower($datecol);   
               $query->where("$ldatecol < CURDATE()");//?", new Zend_Db_Expr("CURDATE()"));
               $query->order("$ldatecol DESC");
            }   
         }break;
         }
//echo $query->__toString();  
         $this->data = $this->dbhandle->fetchRow($query);
         foreach ($this->columns as $dcol => $ctype) {
            if (preg_match('/^\{/',$ctype)) {
               $cparams = json_decode($ctype);
               $ctype = $cparams->DataType;         
            } else $ctype = preg_replace('/\:.+/','',$ctype);
            if ($ctype == 'Date') $datecol = $dcol;
            switch($ctype) {
            /*
            case 'True or False': {
               $result[$rownum][$col] = ($val == true)?1:0;
            }break;
            case 'INTERFACE': {
               if ($val > 0) {
                  $cache = new G_Cache();
                  $cache->loadone($val);
                  $this->data[$col] = $cache;
               }
            }break;
            case 'Taxa': {
               if ($val > 0) {
                  $cache = new G_Cache();
                  $cache->loadone($val);
                  $this->data[$col] = $cache;
               }
            }break;
            */
            case 'LINKEDTOM': {
               // insert linkedtom management
               if (isset($cparams)) {
                  $ttab = $cparams->TargetType;
                  $tcol = $cparams->TargetField;
                  $p = (isset($cparams->Property))?$cparams->Property:$dcol;
               } else {
                  preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                  $ttab = $matches[2];
                  $tcol = $matches[3];
                  $p = $dcol;
               }
               $pk = $this->getpk();
               $lid = $this->getid();
               $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
               $obj = new stdClass();
               $obj->IType = $ttab;
               $obj->ListData = $ltm->selecttargets($pk,$lid);
               $ltmd[$dcol] = $obj;
            } break;
            default: {
               $this->data[$col] = $this->postprocessterm($col,$type,$val);
            } break; 
            } 
         }
            
         if (count($ltmd)) {
            foreach($ltmd as $dcol => $dval) {
               $this->data[$dcol] = $dval;
            }
         }
         $this->id = $this->getid();
      }
   }
   function select($col,$val,$settings=null) {
      $result = array();
      if (isset($settings)) {
         $itype = $this->gettablename();
         $isettings = $settings->$itype;
         $sby = (isset($isettings->SearchBy))?$isettings->SearchBy:null;
         $sfor = (isset($isettings->SearchFor))?$isettings->SearchFor:null;
      }
      if (isset($col)&&isset($val)) {
         if ($this->exists()) {
            $query = $this->dbhandle->select();
            $cols = array();
            $ltmd = array();
            $order = null;
            foreach ($this->columns as $dcol => $type) {
               // don't select the column you already know. 
               // if this is a link it can lead to recursion
               if ($dcol != $col) {
                  if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $type = $params->DataType;         
                  } else $type = preg_replace('/\:.+/','',$type);
                  if (!isset($params->Hidden) || !$params->Hidden) {
                     $term = $this->selectterm($dcol,$type);
                     switch ($type) {
                     case 'Version': {
                        $order = "$term ASC";
                        $cols[$dcol] = $term; 
                     } break;
                     case 'Date': {
                        $order = "$term DESC";
                        $cols[$dcol] = $term; 
                     } break;
                     case 'Map Data': {
                        $cols[$dcol] = $term;               
                     } break;
                     case 'LINKEDTOM': {
                        // insert linkedtom management
                        if (isset($params)) {
                           $ttab = $params->TargetType;
                           $tcol = $params->TargetField;
                           $p = (isset($params->Property))?$params->Property:$dcol;
                        } else {
                           preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                           $ttab = $matches[2];
                           $tcol = $matches[3];
                           $p = $dcol;
                        }
                        $pk = $this->getpk();
                        $id = $this->getid();
                        $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                        $obj = new stdClass();
                        $obj->IType = $ttab;
                        $obj->ListData = $ltm->selecttargets($pk,$id);
                        $ltmd[$dcol] = $obj;
                     } break;
                     default: {
                        $cols[$dcol] = $term; 
                     } break;
                     }
                  }
               }
            }
               
            $query->from(strtolower($this->tablename), $cols);
            if (preg_match('/\%/',$val)) $query->where("$col LIKE ?",$val);
            else $query->where("$col = ?",$val);
            if (isset($sby)) {
               $stype = "".$this->columns[$sby];
               if (preg_match('/^\{/',$stype)) {
                  $params = json_decode($stype);
                  $stype = $params->DataType;
               } else {
                  $bits = preg_split('/\:/',$stype);
                  $stype = $bits[0];
               }
               switch($stype) {
               case 'INTERFACE': {
                  $cache = new G_Cache();
                  $ints = $cache->select('Name',"%$sfor%");
                  $sfors = array();
                  foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
                  $query->where("$sby IN(?)",$sfors);
               }break;
               case 'Taxa': {
                  $cache = new G_Cache();
                  $ints = $cache->select('Name',"%$sfor%");
                  $sfors = array();
                  foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
                  $query->where("$sby IN(?)",$sfors);
               }break;
               case 'Date': {
                  if (preg_match('/([>,<]\=*)/',$sfor,$m)) $sop = $m[1];
                  else $sop = '=';
                  if ($sop == '') $sop = '=';
                  $sfor = preg_replace('/[>,<]\=*\s*/','',$sfor);
                  if (preg_match('/(\d{2})\/(\d{2})\/(\d{4})/',$sfor,$m)) $sfor = $m[3]."-".$m[2]."-".$m[1];
                  if (preg_match('/^\d{4}$/',$sfor)) $sby = new Zend_Db_Expr("YEAR($sby)");
                  $scol = (preg_match('/^\d{4}$/',$sfor))?new Zend_Db_Expr("YEAR($sby)"):$sby;
                  $query->where("$scol $sop ?",$sfor);
               }break;
               /* linked data is handled separately */
               case 'LINKEDTO':break;
               case 'LINKEDTOM':break;
               default: {
                  if (preg_match('/\,/',$sfor)) {
                     $sfors = preg_split('/\s*\,\s*/',$sfor); 
                     $query->where("$sby IN(?)",$sfors);
                  } else {
                     $query->where("$sby LIKE ?","%$sfor%");
                  }
               }break;
               }  
            }
            if ($settings) {
               $settings = (array) $settings;
               foreach($this->columns as $col => $type) {
                  $inherit = false;
                  if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $inherit = $params->Inherit;
                     $dtype = $params->DataType;
                     $itype = $params->TargetType;
                  } else {
                     $types = preg_split('/\:/',$type);
                     $dtype = $types[0];
                     $inherit = in_array('Inherit',$types);
                     if ($inherit) {
                        preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                        $itype = $matches[2];
                     } 
                  }
                  if ($inherit && isset($settings[$itype]) 
                           && isset($settings[$itype]->Current) 
                           && (isset($dtype) && ($dtype != 'LINKEDTOM'))) {
                     $query->where("$col = ?",$settings[$itype]->Current);
                  }
               }
            }
            if (isset($this->order) && count($this->order)>0) {
               foreach($this->order as $col) $query->order($col);
            } else if (isset($order)) $query->order($order); 
            $result = $this->dbhandle->fetchAll($query);
            foreach($result as $rownum => $row) {
               $match = true;
               foreach ($row as $col => $val) {
                  $type = "".$this->columns[$col];
                  if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $type = $params->DataType;
                  } else {
                     $bits = preg_split('/\:/',$type);
                     $type = $bits[0];
                  }
                  switch ($type) {
                  case 'LINKEDTO': {
                     if (isset($params)) {
                        if (!is_object($val)) {
                           $type = $params->DataType;
                           $table = $params->TargetType;
                           $column = $params->TargetField;
                           $link = null;
                           $link = new $table();
                           $link->loaddata($val,$column,false);
                           $obj = new stdClass();
                           $obj->Name = $link->getname();
                           $obj->Id = $link->getid();
                           $result[$rownum][$col] = $obj;
                           if (isset($sby) && $col == $sby) {
                              $match = preg_match("/$sfor/",$obj->Name); 
                           }
                        }
                     }   
                  }break;
                  case 'LINKEDTOM': {
                     if (isset($params)) {
                        if (!is_object($val)) {
                           $type = $params->DataType;
                           $ttab = $params->TargetType;
                           $tcol = $params->TargetField;
                           $p = (isset($params->Property))?$params->Property:$col;
                           $pk = $this->getpk();
                           $id = $this->getid();
                           $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                           $obj = new stdClass();
                           $obj->IType = $ttab;
                           $obj->ListData = $ltm->selecttargets($pk,$row[$pk]);
                           $result[$rownum][$col] = $obj;
                           $result[$rownum][$col] = $this->postprocessterm($col,$type,$val,$params);
                           if (isset($sby) && $col == $sby) {
                              $match = false;
                              foreach($obj->ListData as $item => $liobj) {
                                 $match = $match || preg_match("/$sfor/",$liobj->Name);
                              } 
                           }
                        }
                     }   
                  }break;
                  /*
                  case 'True or False': {
                     $result[$rownum][$col] = ($val == true)?1:0;
                  }break;
                  case 'Date': {
                     if (preg_match('/(\d+)\-(\d+)\-(\d+)/',$val,$matches)) {
                        $y = $matches[1];
                        $m = $matches[2];
                        $d = $matches[3];
                        if (intval($y)+intval($m)+intval($d)>0) {
                           $val = "$d/$m/$y";
                           $result[$rownum][$col] = $val;
                        } else $result[$rownum][$col] = ''; 
                     }
                  }break;
                  case 'INTERFACE': {
                     if ($val > 0) { 
                        $cache = new G_Cache();
                        $cache->loadone($val);
                        $result[$rownum][$col] = $cache->getdata();
                     }
                  }break;
                  case 'Taxa': {
                     if ($val > 0) { 
                        $cache = new G_Cache();
                        $cache->loadone($val);
                        $result[$rownum][$col] = $cache->getdata();
                     }
                  }break;
                  */
                  default: {
                     $result[$rownum][$col] = $this->postprocessterm($col,$type,$val);
                  }break;
                  }
               }
               if (!$match) unset($result[$rownum]);
            }      
         }
      }
      $result = array_values($result);
      return $result;   
   }
   function selectAll($settings = null) {
      $result = array();
      $t = $this->tablename;
      $sby = (isset($settings->$t->SearchBy))?$settings->$t->SearchBy:null;
      $sfor = (isset($settings->$t->SearchFor))?$settings->$t->SearchFor:null;
      if ($this->exists()) {
         $query = $this->dbhandle->select();
         $cols = array();
         foreach ($this->columns as $dcol => $type) {
            if (preg_match('/^\{/',$type)) {
               $params = json_decode($type);
               $type = $params->DataType;
            } else $type = preg_replace('/\:.+/','',$type);
            if (!isset($params) || !isset($params->Hidden) || !$params->Hidden) {
               switch ($type) {
               case 'Map Data': {
                  //$cols[$dcol] = new Zend_Db_Expr("ST_AsText($dcol)");
                  $cols[$dcol] = $this->selectterm($dcol,$type);               
               } break;
               case 'LINKEDTOM': {
                  $ltmd[] = $dcol;
               } break;
               default: {
                  //$cols[$dcol] = $dcol; 
                  $cols[$dcol] = $this->selectterm($dcol,$type);               
               } break;
               }
            }   
         }
            
         $query->from(strtolower($this->tablename), $cols);
         if (isset($sby)) {
            $stype = "".$this->columns[$sby];
            if (preg_match('/^\{/',$stype)) {
               $params = json_decode($stype);
               $stype = $params->DataType;
            } else {
               $bits = preg_split('/\:/',$stype);
               $stype = $bits[0];
            }
            switch($stype) {
            case 'INTERFACE': {
               $cache = new G_Cache();
               $ints = $cache->select('Name',"%$sfor%");
               $sfors = array();
               foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
               $query->where("$sby IN(?)",$sfors);
            }break;
            case 'Taxa': {
               $cache = new G_Cache();
               $ints = $cache->select('Name',"%$sfor%");
               $sfors = array();
               foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
               $query->where("$sby IN(?)",$sfors);
            }break;
            case 'Date': {
               if (preg_match('/([>,<]\=*)/',$sfor,$m)) $sop = $m[1];
               else $sop = '=';
               if ($sop == '') $sop = '=';
               $sfor = preg_replace('/[>,<]\=*\s*/','',$sfor);
               if (preg_match('/(\d{2})\/(\d{2})\/(\d{4})/',$sfor,$m)) $sfor = $m[3]."-".$m[2]."-".$m[1];
               $scol = (preg_match('/^\d{4}$/',$sfor))?new Zend_Db_Expr("YEAR($sby)"):$sby;
               $query->where("$scol $sop ?",$sfor);
            }break;
            /* linked data is handled separately */
            case 'LINKEDTO':break;
            case 'LINKEDTOM':break;
            default: {
               if (preg_match('/\,/',$sfor)) {
                  $sfors = preg_split('/\s*\,\s*/',$sfor); 
                  $query->where("$sby IN(?)",$sfors);
               } else {
                  $query->where("$sby LIKE ?","%$sfor%");
               }
            }break;
            }  
         }
         if ($settings) {
            $settings = (array) $settings;
            foreach($this->columns as $col => $type) {
               $inherit = false;
               if (preg_match('/^\{/',$type)) {
                  $params = json_decode($type);
                  $inherit = (isset($params->Inherit))?$params->Inherit:0;
                  $itype = (isset($params->TargetType))?$params->TargetType:null;
               } else {
                  $types = preg_split('/\:/',$type);
                  $inherit = in_array('Inherit',$types);
                  if ($inherit) {
                     preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                     $itype = $matches[2];
                  } 
               }
               if (($inherit) && isset($settings[$itype]) && isset($settings[$itype]->Current)) {
                  if (!isset($params) || ($params->DataType != 'LINKEDTOM'))
                     $query->where("$col = ?",$settings[$itype]->Current);
               }
            }
         }
         if (isset($this->order) && count($this->order)>0) {
            foreach($this->order as $col) $query->order($col);
         }
//print $query->__toString();exit;
         $result = $this->dbhandle->fetchAll($query);
         foreach($result as $rownum => $row) {
            $match = true;
            foreach ($row as $col => $val) {
               $type = "".$this->columns[$col];
               if (preg_match('/^\{/',$type)) {
                  $params = json_decode($type);
                  $type = $params->DataType;
               } else {
                  $bits = preg_split('/\:/',$type);
                  $type = $bits[0];
               }
               switch ($type) {
               case 'LINKEDTO': {
                  if (isset($params)) {
                     if (!is_object($val)) {
                        $type = $params->DataType;
                        $table = $params->TargetType;
                        $column = $params->TargetField;
                        $link = null;
                        $link = new $table();
                        $link->loaddata($val,$column,false);
                        $obj = new stdClass();
                        $obj->Name = $link->getname();
                        $obj->Id = $link->getid();
                        $result[$rownum][$col] = $obj;
                        if ($settings->SearchBy && $col == $settings->SearchBy) {
                           $sfor = $settings->SearchFor;   
                           $match = preg_match("/$sfor/",$obj->Name); 
                        }
                     }
                  }   
               }break;
               case 'INTERFACE': {
                  if ($val > 0) {
                     $cache = new G_Cache();
                     $cache->loadone($val);
                     $result[$rownum][$col] = $cache->getdata();
                  }
               }break;
               /*
               case 'True or False': {
                  $result[$rownum][$col] = ($val == true)?1:0;
               }break;
               case 'Date': {
                  if (preg_match('/(\d+)\-(\d+)\-(\d+)/',$val,$matches)) {
                     $y = $matches[1];
                     $m = $matches[2];
                     $d = $matches[3];
                     if (intval($y)+intval($m)+intval($d)>0) {
                        $val = "$d/$m/$y";
                        $result[$rownum][$col] = $val;
                     } else $result[$rownum][$col] = '';
                  }
               }break;
               case 'Taxa': {
                  if ($val > 0) {
                     $cache = new G_Cache();
                     $cache->loadone($val);
                     $result[$rownum][$col] = $cache->getdata();
                  }
               }break;
               */
               default: {
                  $result[$rownum][$col] = $this->postprocessterm($col,$type,$val);
               }break;
               }
            }
            if (isset($ltmd) && is_array($ltmd) && count($ltmd)>0) {
               foreach($ltmd as $col) {
                  $mtype = $this->columns[$col];
                  if (preg_match('/^\{/',$mtype)) {
                     $mparams = json_decode($mtype);
                     $type = $mparams->DataType;
                     $ttab = $mparams->TargetType;
                     $tcol = $mparams->TargetField;
                     $p = ($mparams->Property)?$mparams->Property:$col;
                  } else if(preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches)) {
                     $type = $matches[1];
                     $ttab = $matches[2];
                     $tcol = $matches[3];
                     $p = $col;
                  }
                  if (isset($ttab) && isset($tcol)) {
                     if (!is_object($val)) {
                        $pk = $this->getpk();
                        $id = $row[$pk];
                        $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                        $obj = new stdClass();
                        $obj->IType = $ttab;
                        $obj->Current = 'Multiple';
                        $obj->Key = $tcol; 
                        $obj->ListData = $ltm->selecttargets($pk,$id);
                        $result[$rownum][$col] = $obj;
                        if ($sby && $col == $sby) {
                           $match = false;
                           foreach($obj->ListData as $item => $liobj) {
                              $match = $match || preg_match("/$sfor/",$liobj->Name);
                           } 
                        }
                     }
                  }      
               }
            }
            if (!$match) unset($result[$rownum]);
            if (!count($result)) $result = $query->__toString();            
         }
      }
      $result = array_values($result);
      return $result;   
   }     
}
class DBT_Lookup extends DBT {
   protected $domain = 'genera';
   function DBT_Lookup($property,$tab1,$col1,$tab2,$col2) {
      parent::DBT();
      if (isset($property)
            && isset($tab1) && isset($col1)
            && isset($tab1) && isset($col2)) {
         $name = ($tab1 < $tab2)?"DBTL_${tab1}_${tab2}_${property}":"DBTL_${tab2}_${tab1}_${property}";
         $name = preg_replace('/_S2_/','_',$name);
         //$name = "${tab1}_${property}"; 
         $this->settablename($name);
         $columns = array(
            "${name}_ID"   => 'Unique Key',
            $col1          => '{"DataType":"LINKEDTO","TargetType":"'.$tab1.'","TargetField":"'.$col1.'"}',
            $col2          => '{"DataType":"LINKEDTO","TargetType":"'.$tab2.'","TargetField":"'.$col2.'"}'
         );
         $this->setcolumns($columns);
         if (!$this->exists()) $this->create();
      }
   }
   function selecttargets($col,$val) {
      $ltms = $this->select($col,$val);
      foreach ($ltms as $k => $ltm) {
         foreach($ltm as $dcol => $dval) {
            if (!($dcol==$col) && $this->columns[$col] != 'Unique Key') {
               $ltype = $this->columns[$col];
               if (preg_match('/^\{/',$ltype)) {
                  $params = json_decode($ltype);
                  $tcol = $params->TargetField;
                  $ltms[$k] = $dval;
               } else if (preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches)) {
                  $tcol = $matches[3];
                  $ltms[$k] = $dval;
               }               
            }
         }
         if (is_object($ltms[$k]) && $ltms[$k]->Id == null) unset($ltms[$k]);
         else if (!($ltms[$k]>0)) unset($ltms[$k]);
      }
//print_r($ltms);
      return $ltms;
   }   
}
class DBT_FilterView extends DBT {
   protected $sourcetable;
   protected $filters = array();
   // eg $filters = array("Col1" => "Val1", "Col2" => "Val2");
   protected $orders = array();
   // eg $orders = array("Col1" => "ASC", "Col2" => "DESC");
   protected $indexon;
   protected $selectto;
   protected $domain = 'genera';
   function DBT_FilterView() {
      parent::DBT();
      $st = $this->sourcetable;
      if (isset($st)) {  
         $dbt = new $st();;
         if (!$dbt->exists()) $dbt->create();
         $this->columns = $dbt->getcolumns();
      }
   }
   function getuserpermissions() {
      $perms = array('View','Add','Edit','Delete');
      $granted = array();
      /*
      foreach($perms as $perm) {
         if ($this->isallowed($this->user,$perm)) $granted[] = $perm;      
      }
      */
      return $granted;      
   }
   function getpk() {
      return $this->indexon;
   }
   function create() {
      global $pending;
      if (!isset($pending[$this->tablename])) {
         $pending[$this->tablename] = true;
         $dbh = $this->dbhandle;
         $query = $dbh->select();
         $query->from($this->sourcetable);
         foreach($this->filters as $col => $val) {
            $query->where("$col = ?", $val);
         }               
         foreach($this->orders as $col => $dir) {
            $query->order("$col $dir");
         }               
         $select = $query->__toString();
         $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select"; 
         $dbh->exec($statement);
         unset($pending[$this->tablename]);
      }                  
   }
   function exists() {
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from('pg_tables',array('tablename'=>'tablename'));
      $query->where('tablename = ?',strtolower($this->tablename));
      $resp = $db->fetchAll($query);
      return (count($resp)>0);
   }       
}
class DBT_ReportView extends DBT {
   protected $sourcetables;
   // eg $sourcetables = array("Table1" => "Alias1", "Table2" => "Alias2");
   protected $joins = array();
   // eg $joins = array ("TargetTable1" => "SourceTable1");
   protected $sourcecolumns;
   // eg $sourcetables = array(  "Table1" => array("AsAlias1" => "Col1","AsAlias2" => "Col2"), 
   //                            "Table2" => array("AsAlias1" => "Col1","AsAlias2" => "Col2"));
   protected $filters = array();
   // eg $filters = array("Col1" => "Val1", "Col2" => "Val2");
   protected $colmatch = array();
   protected $orders = array();
   // eg $orders = array("Col1" => "ASC", "Col2" => "DESC");
   protected $group = array();
   // eg $group = array("Col1");
   protected $indexon;
   protected $selectto;
   protected $nolist = array();
   protected $domain = 'genera';
   function DBT_ReportView() {
      parent::DBT();
      foreach($this->sourcetables as $table => $alias) {
         $link = new $table();
         if (!$link->exists()) $link->create();
         $scols = $link->getcolumns();
         if ($this->sourcecolumns[$table]) {
            foreach($this->sourcecolumns[$table] as $alias => $column) {
               $alias = (is_int($alias))?$column:$alias;
               if ($scols[$column] == 'Unique Key') {
                  $obj = new stdClass();
                  $obj->DataType = 'LINKEDTO';
                  $obj->TargetType = $table;
                  $obj->TargetField = $column;
                  if (in_array($alias,$this->nolist)) $obj->NoList = 1;
                  $this->columns[$alias] = json_encode($obj);
               } else if (preg_match('/^\{/',$scols[$column])) {
                  $obj = json_decode($scols[$column]);
                  if (in_array($alias,$this->nolist)) $obj->NoList = 1;
                  $this->columns[$alias] = json_encode($obj);
               } else {
                  $obj = new stdClass();
                  $obj->DataType = preg_replace('/\:.+/','',$scols[$column]);
                  if (in_array($alias,$this->nolist)) $obj->NoList = 1;
                  $this->columns[$alias] = json_encode($obj);
               }                            
            }
         }
      }
//print "<pre>"; print_r($this->columns); print "</pre>"; exit;   
   }
   function getversions($val) {return null;}
   function getuserpermissions() {
      $perms = array('View','Add','Edit','Delete');
      $granted = array();
      foreach($perms as $perm) {
         if ($this->isallowed($this->user,$perm)) $granted[] = $perm;      
      }
      return $granted;      
   }
   function getpk() {
      return $this->indexon;
   }
   function create() {
      global $pending;
      if (!isset($pending[$this->tablename])) {
         $pending[$this->tablename] = true;
         $dbh = $this->dbhandle;
         $query = $dbh->select();
         $count = 0;
         foreach($this->sourcetables as $table => $alias) {
            $ltable = strtolower($table);
            $lalias = strtolower($alias);
            $cols = array();
            if (isset($this->sourcecolumns[$table])) {
               foreach ($this->sourcecolumns[$table] as $k => $col) {
                  $lcol = strtolower($col);
                  $lk = strtolower($k);
                  if (is_numeric($k)) {
                     $cols[$lcol] = $lcol;
                  } else if (preg_match('/^\{/',$col)) {
                     $expr = json_decode($col);
                     $expr->Expression = strtolower($expr->Expression);
                     $cols[$lk] = json_encode($expr); 
                  } else {
                     $cols[$lk] = $lcol;
                  }
               }
            }
            if ($count == 0) $query->from(array($lalias => $ltable),$cols);         
            else {
               $link = new $table();
               if (!$link->exists()) $link->create();
               $totable = $this->joins[$table];
               $toalias = $this->sourcetables[$totable];
               $ltotable = strtolower($totable);  
               list($incol,$tocol) = $link->getlinkedcolumn($totable);
               if (!isset($incol)) {
                  $link = new $totable();
                  if (!$link->exists()) $link->create();
                  list($tocol,$incol) = $link->getlinkedcolumn($table);
               }
               $join = strtolower("${alias}.${incol} = ${toalias}.${tocol}");
               foreach($cols as $col => $val) {
                  if (preg_match('/^\{/',$val)) {
                     $val = json_decode($val);
                     $val = new Zend_Db_Expr($val->Expression);
                     $cols[$col] = $val;
                  }
               }
               $query->joinLeft(array($lalias => $ltable),$join,$cols);
            }
            $count++;
         }
         
         foreach($this->filters as $clause => $val) {
            if (preg_match('/^\{/',$val)) {
               $val = json_decode($val);
               $val = new Zend_Db_Expr($val->Expression);
            }
            $query->where($clause, $val);
         }               
         foreach($this->colmatch as $col1 => $col2) {
            $query->where(strtolower("$col1 = $col2"));
         }               
         foreach($this->orders as $col => $dir) {
            $query->order(strtolower("$col $dir"));
         }
         foreach($this->group as $col) {
            $query->group(strtolower($col));
         }               
         $select = $query->__toString();
//print "$select";exit;         
         $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
         $this->dbhandle->exec($statement);
         unset($pending[$this->tablename]);
      }
   }
   function exists() {
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from('pg_views',array('viewname'=>'viewname'));
      $query->where('viewname = ?',strtolower($this->tablename));
      $resp = $db->fetchAll($query);
      return (count($resp)>0);
   }   
}
class DBT_Analysis extends DBT {
   protected $domain = 'genera';
   function DBT_Analysis() {
      parent::DBT();
   }
}

$pending = array();


/*
class DBTest1 extends DBT {
   protected $tablename = 'DBTest1';
   protected $displayname = 'Database Test 1';
   // data types can be one of $this->datatypes or 
   // LINK:<tablename>.<columnname> and will pick up their 
   // datatype from that table. 
   // note that table will need to be included using 
   // include_once to ensure the definition can be read
   protected $columns = array(
      'DBTest1_ID'      => 'Unique Key',
      'name_column_1'   => 'Short Text',
      'value_column_1'  => 'Number'
   );
   protected $childtables = array('DBTest2');
}
class DBTest2 extends DBT {
   protected $tablename = 'DBTest2';
   protected $displayname = 'Database Test 2';
   protected $columns = array(
      'DBTest2_ID'      => 'Unique Key',
      'name_column_2'   => 'Short Text',
      'value_column_2'  => 'Number',
      'dbt1'            => 'LINKEDTO:DBTest1.DBTest1_ID:Optional'                  
   );
}
class DBTest3 extends DBT {
   protected $tablename = 'DBTest3';
   protected $displayname = 'Database Test 3';
   protected $columns = array(
      'DBTest3_ID'      => 'Unique Key',
      'name_column_3'   => 'Short Text',
      'value_column_3'  => 'Number',
      'dbt1'            => 'LINKEDTO:DBTest1.DBTest1_ID:Optional'                  
   );
}

$DBTest1 = new DBTest1();
$DBTest1->create();

$cols = array(
   'name_column_1' => 'Name Test 1',
   'value_column_1' => 'Value 1'   
);
$DBTest1->setdata($cols);
$status = $DBTest1->insert();
print "<h3>DBTest1 id = ".$DBTest1->getid()."</h3>";

$dbt1s = $DBTest1->selectAll();
//print_r($dbt1s);


$cols = array(
   'name_column_1' => 'Name Test 2',
   'value_column_1' => 'Value 1'   
);
$DBTest1->setdata($cols);
$status = $DBTest1->update('name_column_1', 'Name Test 1');
print "<h3>Update Status : $status</h3>";

$dbt1s = $DBTest1->selectAll();
//print_r($dbt1s);
$DBTest2 = new DBTest2();
$DBTest2->create();
$DBTest2->drop();
print $DBTest2->drawform();


print $DBTest1->drawtable();

print_r($DBTest1->exists());
$DBTest3 = new DBTest3();
print_r($DBTest3->exists());
*/
?>