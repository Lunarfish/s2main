<?php 
// ===============================================================
// S&C Sites & Citations - Recorder connection library
// ---------------------------------------------------------------
// Written by YHEDN (Dan Jones)
// ===============================================================
// Retrieves data from Recorder6 database via an ODBC connection 
// and returns the data as JSON to be interpreted by the 
header("Cache-Control: no-cache, must-revalidate");
$paths = array(
   '../',
   '../../',
   '../../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once 'Zend/Db.php';
function SnC_getDatabaseConnection() {
   $dbConfig = array('dbname'	=> 'SnC_Db_V1.sqlite');
   $db = Zend_Db::factory('Pdo_Sqlite', $dbConfig);
   return $db;
}
?>