<?php 
// ===============================================================
// S&C Sites & Citations - Recorder connection library
// ---------------------------------------------------------------
// Written by YHEDN (Dan Jones)
// ===============================================================
// Retrieves data from Recorder6 database via an ODBC connection 
// and returns the data as JSON to be interpreted by the
header("Cache-Control: no-cache, must-revalidate");
/*
$paths = array(
   '../',
   '../../',
   '../../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
*/
include_once('settings.conf');
include_once('Zend/Db.php');
include_once('gen/G_DBConfig.php');

function SnC_openZendDb($domain='default') {
   global $databases;
   $db = (isset($databases[$domain]))?$databases[$domain]:$databases['default']; 
   $dbConfig = $db->getconn();
   $dbPlatform = $db->getplatform();
   //if ($dbPlatform == 'PGSQL') $dbConfig['options'] = array(Zend_Db::CASE_FOLDING => Zend_Db::CASE_LOWER);
   $dbh = Zend_Db::factory("Pdo_${dbPlatform}", $dbConfig);
   return $dbh;
}

$zdb = array();
$zdb['default'] = SnC_openZendDb('default');

function SnC_getDatabaseConnection($domain='default') {
   global $zdb,$databases;
   $dbh = null;
   if (!isset($zdb[$domain])) {
      if (isset($databases[$domain])) {
         $dbh = SnC_openZendDb($domain);
         $zdb[$domain] = $dbh;
      } 
      else $dbh = $zdb['default'];
   } 
   else $dbh = $zdb[$domain];
   return $dbh;
}
function SnC_getPlatform($domain='default') {
   global $databases;
   $db = (isset($databases[$domain]))?$databases[$domain]:$databases['default']; 
   $dbPlatform = $db->getplatform();
   return $dbPlatform;
}

$incpgnative = false;
foreach ($databases as $dbi => $db) {
   if ($db->getplatform() == 'PGSQL') {
      $incpgnative = true;
   }
}
if ($incpgnative) include_once('snc/SnCDatabaseConnectPG.php');
   
?>
