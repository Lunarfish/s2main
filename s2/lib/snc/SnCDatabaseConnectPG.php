<?php
include_once('gen/G_DBConfig.php');
function SnC_openPGNative($domain='default') {    
   global $databases;
   $db = (isset($databases[$domain]))?$databases[$domain]:$databases['default'];
   $dbConfig = $db->getconn();
   $dbPlatform = $db->getplatform();
   
   // pg native uses user in the connection string rather than username;
   $pgConfig = array();
   foreach($dbConfig as $key => $val) {
      switch($key) {
      case 'username': $pgConfig['user'] = $val; break;
      default: $pgConfig[$key] = $val;
      }
   }      
   
   $pgcopts = array();
   foreach ($pgConfig as $key => $val) $pgcopts[] = "${key}=${val}";
   $pgcstring = join(' ',$pgcopts);
   $dbh = pg_connect($pgcstring);
   return $dbh;
}
$pdb = array();
$pdb['default'] = SnC_openPGNative('default');  
function SnC_getPGNativeConnection($domain='default') {
   global $pdb,$databases;
   $dbh = null;
   if (!isset($pdb[$domain])) {
      if (isset($databases[$domain])) {
         $dbh = SnC_openPGNative($domain);
         $pdb[$domain] = $dbh;
      } else $dbh = $pdb['default'];
   } else $dbh = $pdb[$domain];
   return $dbh;
}
?>