/* 

   GENERA Table Definitions
*/

/* 
   PER_ tables
   
   Users can have more than one Role.
   Roles can have more than one Permission. 
   Functions are allowed by Permission 
   Roles are hierarchic so Users get all the Permissions associated with their 
   Roles and the children of their Roles  
*/
CREATE TABLE PER_Role (
   PER_Role_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   PER_ParentRole_Id INT UNSIGNED,
   ADM_Application_Id INT UNSIGNED,
   Name VARCHAR(100) NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL,
   PRIMARY KEY(PER_Role_Id)   
);
CREATE TABLE PER_UserRole (
   PER_UserRole_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   PER_Role_Id INT UNSIGNED NOT NULL,
   ADM_User_Id INT UNSIGNED NOT NULL,
   ADM_Application_Id INT UNSIGNED,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL,
   PRIMARY KEY(PER_UserRole_Id)   
);
CREATE TABLE PER_Permission (
   PER_Permission_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Application_Id INT UNSIGNED NOT NULL,
   Name VARCHAR(100) NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL,
   PRIMARY KEY(PER_Permission_Id)   
);
CREATE TABLE PER_RolePermission (
   PER_RolePermission_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   PER_Role_Id INT UNSIGNED NOT NULL,
   PER_Permission_Id INT UNSIGNED NOT NULL,      
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL,
   PRIMARY KEY(PER_RolePermission_Id)   
);

/*   
   DEV_ tables    
*/

/*
   DEV DataType is a short list of column definitions which can be chosen by 
   the user with descriptive advice about what to use each column type for. 
*/
CREATE TABLE DEV_DataType(
   DEV_DataType_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   Name VARCHAR(100) NOT NULL,
   Description TEXT NOT NULL,
   Definition VARCHAR(100) NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL,
   PRIMARY KEY(DEV_DataType_Id)    
);
/* 
   DEV Method describes how you can query records from a table. If a table has 
   a date column then that can implement the future,past,next and previous 
   methods.  
*/
CREATE TABLE DEV_Method (
   DEV_Method_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   Name VARCHAR(100) NOT NULL,
   Description TEXT NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL,
   PRIMARY KEY(DEV_Method_Id)    
);
/*
   DEV DataType method defines which methods can be used by each data type. 
   Some methods are used by more than one data type. 
*/
CREATE TABLE DEV_DataTypeMethod (
   DEV_DataTypeMethod_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   DEV_Method_Id INT UNSIGNED NOT NULL,
   DEV_DataType_Id INT UNSIGNED NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(DEV_DataTypeMethod_Id)    
);

/* 
   Likewise each data type has an associated interface for rendering it on the 
   screen. 
   
   So dates have calendars and geometries have maps.
   
   Short lists can be drop downs and longer lists might be paged with details.
   With drop downs tool tips can render the description.
   
   JavaScript libraries need to describe how to implement search and add 
   interfaces    
*/
CREATE TABLE DEV_Interface (
   DEV_Interface_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   Name VARCHAR(100) NOT NULL,
   Description TEXT NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(DEV_Interface_Id)       
);

CREATE TABLE DEV_DataTypeInterface (
   DEV_DataTypeInterface_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   DEV_Interface_Id INT UNSIGNED NOT NULL,
   DEV_DataType_Id INT UNSIGNED NOT NULL,
   PRIMARY KEY(DEV_DataTypeInterface_Id)    
);

CREATE TABLE DEV_ActionType (
   DEV_ActionType_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   DisplayName VARCHAR(100),
   DatabaseAction VARCHAR(100),
   PRIMARY KEY(DEV_ActionType_Id)                                                                                                         
);

/*
   ADM_ tables 
*/
CREATE TABLE ADM_Application ( 
   ADM_Application_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   Name VARCHAR(100) NOT NULL,
   Prefix VARCHAR(100) NOT NULL, 
   Template VARCHAR(100),
   Version VARCHAR(100),
   Customised BOOLEAN,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_Application_Id)   
);
/*
   Default permissions can be applied to a whole application and then 
   overridden for a table (?or column?)
   
   Table permisssions override default application permissions regardless of 
   whether they are more or less restrictive. 
*/
CREATE TABLE ADM_Requirement_ApplictionPermission (
   ADM_Requirement_ApplicationPermission_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Application_Id INT UNSIGNED NOT NULL,
   DEV_ActionType_Id INT UNSIGNED NOT NULL,
   PER_Permission_Id INT UNSIGNED NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_Requirement_ApplicationPermission_Id)   
);


/*
   When you start a session you don't have a user or an application. 
   Creating the session is step 1. 
   Logging in is the 2nd step and then choosing an application is 3rd.  
*/
CREATE TABLE ADM_Session (
   ADM_Session_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_User_Id INT UNSIGNED,
   ADM_Application_Id INT UNSIGNED,
   Started TIMESTAMP NOT NULL,
   LastAccessed TIMESTAMP NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_Session_Id)     
);

CREATE TABLE ADM_Setting (
   ADM_Setting_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Table_Id INT UNSIGNED NOT NULL,
   Name VARCHAR(100) NOT NULL,
   DisplayName VARCHAR(100),
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_Setting_Id)
);

CREATE TABLE ADM_SessionSetting (
   ADM_SessionSetting_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Setting_Id INT UNSIGNED NOT NULL,
   SetTo VARCHAR(100) NOT NULL,
   DisplayName VARCHAR(100),
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_SessionSetting_Id)
);

CREATE TABLE ADM_User (
   ADM_User_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   Username VARCHAR(100) NOT NULL,
   Email VARCHAR(100) NOT NULL,
   Password VARCHAR(100) NOT NULL,
   Locked BOOLEAN,
   Verified BOOLEAN,
   VerifyCode VARCHAR(100),
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_User_Id)        
);

CREATE TABLE ADM_Table (
   ADM_Table_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Application_Id INT UNSIGNED,
   Name VARCHAR(100) NOT NULL,
   IsSearchable BOOLEAN,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_Table_Id)
); 

/*
   If IsSearchable is set to true, the option will only be given if at least 1 
   search interface method has been implemented for that column 
*/
CREATE TABLE ADM_Column (
   ADM_Column_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Table_Id INT UNSIGNED NOT NULL,
   DEV_DataType_Id INT UNSIGNED NOT NULL,
   Name VARCHAR(100) NOT NULL,
   DisplayName VARCHAR(100),
   IsSearchable BOOLEAN,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL,
   PRIMARY KEY(ADM_Column_Id)       
);

/*
   Mandatory will be covered by the reqiurements table but should be checked 
   before trying to insert data. 
   
   SelectActionName allows you to override the default name so you can call 
   a dialog Login rather than Select User.
   
   Because Foreign keys are added separately there is no need to add them as a 
   Column in the ADM_Column table but it may be useful to have them there. 
*/
CREATE TABLE ADM_TableForeignKey (
   ADM_TableForeignKey_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_LinkFromTable_Id INT UNSIGNED NOT NULL,
   ADM_LinkFromColumn_Id INT UNSIGNED NOT NULL,
   ADM_LinkToTable_Id INT UNSIGNED NOT NULL,
   Name VARCHAR(100) NOT NULL,
   Mandatory BOOLEAN,
   SelectActionName VARCHAR(100),
   IsSearchable BOOLEAN,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_TableForeignKey_Id)
);

CREATE TABLE ADM_Requirement_UserSupplied (
   ADM_Requirement_UserSupplied_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Table_Id INT UNSIGNED NOT NULL,
   ADM_Column_Id INT UNSIGNED NOT NULL,
   DEV_ActionType_Id INT UNSIGNED NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_Requirement_UserSupplied_Id)   
); 
CREATE TABLE ADM_Requirement_CurrentSetting (
   ADM_Requirement_UserSupplied_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Table_Id INT UNSIGNED NOT NULL,
   ADM_Column_Id INT UNSIGNED NOT NULL,
   DEV_ActionType_Id INT UNSIGNED NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_Requirement_UserSupplied_Id)   
);

CREATE TABLE ADM_Requirement_TablePermission (
   ADM_Requirement_TablePermission_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Table_Id INT UNSIGNED NOT NULL,
   ADM_Setting_Id INT UNSIGNED NOT NULL,
   DEV_ActionType_Id INT UNSIGNED NOT NULL,
   PER_Permission_Id INT UNSIGNED NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_Requirement_TablePermission_Id)   
);

INSERT INTO ADM_User (ADM_User_Id, Username, Email, Password, Locked, Verified, VerifyCode, CreatedBy, CreatedDate) VALUES 
(1, 'Dan Jones', 'dan.jones@lunarfish.co.uk', MD5('ur5ula'), 1, 1, NULL, 1, CURDATE()); 
INSERT INTO PER_Role (PER_Role_Id,PER_ParentRole_Id,ADM_Application_Id,Name) VALUES 
(1,NULL,NULL,'Developer');

INSERT INTO ADM_Table (ADM_Table_Id, ADM_Application_Id, Name, IsSearcable, CreatedBy, CreatedDate) VALUES 
(1,1,'DEV_ActionType',1,1,CURDATE()),
(2,1,'DEV_DataType',1,1,CURDATE()),
(3,1,'DEV_DataTypeInterface',1,1,CURDATE()),
(4,1,'DEV_DataTypeMethod',1,1,CURDATE()),
(5,1,'DEV_Interface',1,1,CURDATE()),
(6,1,'DEV_Method',1,1,CURDATE()),
(7,1,'DEV_DataType',1,1,CURDATE()),
(8,2,'PER_Role',1,1,CURDATE()),
(9,2,'PER_UserRole',0,1,CURDATE()),
(10,2,'PER_Permission',1,1,CURDATE()),
(11,2,'PER_RolePermission',0,1,CURDATE()),
(12,3,'ADM_Application',1,1,CURDATE()),
(13,3,'ADM_User',0,1,CURDATE()),
(14,3,'ADM_Table',0,1,CURDATE()),
(15,3,'ADM_Column',0,1,CURDATE()),
(16,3,'ADM_Requirement_ApplicationPermission',0,1,CURDATE()),
(17,3,'ADM_Requirement_CurrentSetting',0,1,CURDATE()),
(18,3,'ADM_Requirement_TablePermission',0,1,CURDATE()),
(19,3,'ADM_Requirement_UserSupplied',0,1,CURDATE()),
(20,3,'ADM_Session',0,1,CURDATE()),
(21,3,'ADM_SessionSetting',0,1,CURDATE()),
(22,3,'ADM_Setting',0,1,CURDATE()),
(23,3,'ADM_TableForeignKey',0,1,CURDATE());

INSERT INTO Per_Permission (PER_Permission_Id, ADM_Application_Id, Name, CreatedBy, CreatedDate) VALUES
(1, 0, 'Developer', 1, CURDATE()),
(2, 0, 'System Adminstrator', 1, CURDATE()),
(3, 4, 'Full Permissions', 1, CURDATE()),
(6, 4, 'Data Protection Act Data', 1, CURDATE()),
(5, 4, 'Land Owner Data', 1, CURDATE()),
(4, 4, 'Editor', 1, CURDATE()),
(7, 4, 'Search', 1, CURDATE()),
(8, 4, 'View', 1, CURDATE()),
(9, 4, 'Meeting Data', 1, CURDATE()),
(10, 4, 'Correspondence Records', 1, CURDATE());

INSERT INTO Per_Role (PER_Role_Id, PER_ParentRole_Id, ADM_Application_Id, Name, CreatedBy, CreatedDate) VALUES
(1, 0, 0, 'Developer', 1, CURDATE()),
(2, 1, 0, 'System Administrator', 1, CURDATE()),
(3, 2, 4, 'Administrator', 1, CURDATE()),
(4, 3, 4, 'Officer', 1, CURDATE()),
(5, 4, 4, 'Partnership Member', 1, CURDATE()),
(6, 3, 4, 'Planning Officer', 1, CURDATE());

per_userrole
per_permission
per_rolepermission
adm_application
dev_actiontype
dev_datatype
dev_datatypeinterface
dev_datatypemethod
dev_interface
dev_method
adm_column
adm_requirement_applictionpermission
adm_requirement_currentsetting
adm_requirement_tablepermission
adm_requirement_usersupplied
adm_session
adm_sessionsetting
adm_setting
adm_table
adm_tableforeignkey

1 	ShortText
2 	MediumText
3 	LongText
4 	EncryptedText
5 	Date
6 	Date and Time
7 	Map Data
8 	Unique Key
9 	Linked Data

INSERT INTO ADM_TableForeignKey(ADM_TableForeignKey_Id,ADM_LinkFromTable_Id,ADM_LinkFromColumn_Id,ADM_LinkToTable_Id,Name,Mandatory,SelectActionName,IsSearchable,CreatedBy,CreatedDate) VALUES
(NULL,8,)
INSERT INTO ADM_Column (ADM_Column_Id,ADM_Table_Id,DEV_DataType_Id,Name,DisplayName,IsSearchable,CreatedBy,CreatedDate) VALUES
(NULL,8,)
8 CREATE TABLE PER_Role (
   PER_ParentRole_Id INT UNSIGNED,
   ADM_Application_Id INT UNSIGNED,
   Name VARCHAR(100) NOT NULL,
   PRIMARY KEY(PER_Role_Id)   
);
9 CREATE TABLE PER_UserRole (
   PER_UserRole_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   PER_Role_Id INT UNSIGNED NOT NULL,
   ADM_User_Id INT UNSIGNED NOT NULL,
   ADM_Application_Id INT UNSIGNED,
   PRIMARY KEY(PER_UserRole_Id)   
);
10 CREATE TABLE PER_Permission (
   PER_Permission_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Application_Id INT UNSIGNED NOT NULL,
   Name VARCHAR(100) NOT NULL,
   PRIMARY KEY(PER_Permission_Id)   
);
11 CREATE TABLE PER_RolePermission (
   PER_RolePermission_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   PER_Role_Id INT UNSIGNED NOT NULL,
   PER_Permission_Id INT UNSIGNED NOT NULL,      
   PRIMARY KEY(PER_RolePermission_Id)   
);

/*   
   DEV_ tables    
*/

/*
   DEV DataType is a short list of column definitions which can be chosen by 
   the user with descriptive advice about what to use each column type for. 
*/
2 CREATE TABLE DEV_DataType(
   DEV_DataType_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   Name VARCHAR(100) NOT NULL,
   Description TEXT NOT NULL,
   Definition VARCHAR(100) NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL,
   PRIMARY KEY(DEV_DataType_Id)    
);
/* 
   DEV Method describes how you can query records from a table. If a table has 
   a date column then that can implement the future,past,next and previous 
   methods.  
*/
6 CREATE TABLE DEV_Method (
   DEV_Method_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   Name VARCHAR(100) NOT NULL,
   Description TEXT NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL,
   PRIMARY KEY(DEV_Method_Id)    
);
/*
   DEV DataType method defines which methods can be used by each data type. 
   Some methods are used by more than one data type. 
*/
4 CREATE TABLE DEV_DataTypeMethod (
   DEV_DataTypeMethod_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   DEV_Method_Id INT UNSIGNED NOT NULL,
   DEV_DataType_Id INT UNSIGNED NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(DEV_DataTypeMethod_Id)    
);

/* 
   Likewise each data type has an associated interface for rendering it on the 
   screen. 
   
   So dates have calendars and geometries have maps.
   
   Short lists can be drop downs and longer lists might be paged with details.
   With drop downs tool tips can render the description.
   
   JavaScript libraries need to describe how to implement search and add 
   interfaces    
*/
5 CREATE TABLE DEV_Interface (
   DEV_Interface_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   Name VARCHAR(100) NOT NULL,
   Description TEXT NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(DEV_Interface_Id)       
);

3 CREATE TABLE DEV_DataTypeInterface (
   DEV_DataTypeInterface_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   DEV_Interface_Id INT UNSIGNED NOT NULL,
   DEV_DataType_Id INT UNSIGNED NOT NULL,
   PRIMARY KEY(DEV_DataTypeInterface_Id)    
);

1 CREATE TABLE DEV_ActionType (
   DEV_ActionType_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   DisplayName VARCHAR(100),
   DatabaseAction VARCHAR(100),
   PRIMARY KEY(DEV_ActionType_Id)                                                                                                         
);

/*
   ADM_ tables 
*/
12 CREATE TABLE ADM_Application ( 
   ADM_Application_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   Name VARCHAR(100) NOT NULL,
   Prefix VARCHAR(100) NOT NULL, 
   Template VARCHAR(100),
   Version VARCHAR(100),
   Customised BOOLEAN,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_Application_Id)   
);
/*
   Default permissions can be applied to a whole application and then 
   overridden for a table (?or column?)
   
   Table permisssions override default application permissions regardless of 
   whether they are more or less restrictive. 
*/
16 CREATE TABLE ADM_Requirement_ApplictionPermission (
   ADM_Requirement_ApplicationPermission_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Application_Id INT UNSIGNED NOT NULL,
   DEV_ActionType_Id INT UNSIGNED NOT NULL,
   PER_Permission_Id INT UNSIGNED NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_Requirement_ApplicationPermission_Id)   
);


/*
   When you start a session you don't have a user or an application. 
   Creating the session is step 1. 
   Logging in is the 2nd step and then choosing an application is 3rd.  
*/
20 CREATE TABLE ADM_Session (
   ADM_Session_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_User_Id INT UNSIGNED,
   ADM_Application_Id INT UNSIGNED,
   Started TIMESTAMP NOT NULL,
   LastAccessed TIMESTAMP NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_Session_Id)     
);

22 CREATE TABLE ADM_Setting (
   ADM_Setting_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Table_Id INT UNSIGNED NOT NULL,
   Name VARCHAR(100) NOT NULL,
   DisplayName VARCHAR(100),
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_Setting_Id)
);

21 CREATE TABLE ADM_SessionSetting (
   ADM_SessionSetting_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Setting_Id INT UNSIGNED NOT NULL,
   SetTo VARCHAR(100) NOT NULL,
   DisplayName VARCHAR(100),
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_SessionSetting_Id)
);

13 CREATE TABLE ADM_User (
   ADM_User_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   Username VARCHAR(100) NOT NULL,
   Email VARCHAR(100) NOT NULL,
   Password VARCHAR(100) NOT NULL,
   Locked BOOLEAN,
   Verified BOOLEAN,
   VerifyCode VARCHAR(100),
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_User_Id)        
);

14 CREATE TABLE ADM_Table (
   ADM_Table_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Application_Id INT UNSIGNED,
   Name VARCHAR(100) NOT NULL,
   IsSearchable BOOLEAN,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_Table_Id)
); 

/*
   If IsSearchable is set to true, the option will only be given if at least 1 
   search interface method has been implemented for that column 
*/
15 CREATE TABLE ADM_Column (
   ADM_Column_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Table_Id INT UNSIGNED NOT NULL,
   DEV_DataType_Id INT UNSIGNED NOT NULL,
   Name VARCHAR(100) NOT NULL,
   DisplayName VARCHAR(100),
   IsSearchable BOOLEAN,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL,
   PRIMARY KEY(ADM_Column_Id)       
);

/*
   Mandatory will be covered by the reqiurements table but should be checked 
   before trying to insert data. 
   
   SelectActionName allows you to override the default name so you can call 
   a dialog Login rather than Select User.
   
   Because Foreign keys are added separately there is no need to add them as a 
   Column in the ADM_Column table but it may be useful to have them there. 
*/
23 CREATE TABLE ADM_TableForeignKey (
   ADM_TableForeignKey_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_LinkFromTable_Id INT UNSIGNED NOT NULL,
   ADM_LinkFromColumn_Id INT UNSIGNED NOT NULL,
   ADM_LinkToTable_Id INT UNSIGNED NOT NULL,
   Name VARCHAR(100) NOT NULL,
   Mandatory BOOLEAN,
   SelectActionName VARCHAR(100),
   IsSearchable BOOLEAN,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_TableForeignKey_Id)
);


19 CREATE TABLE ADM_Requirement_UserSupplied (
   ADM_Requirement_UserSupplied_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Table_Id INT UNSIGNED NOT NULL,
   ADM_Column_Id INT UNSIGNED NOT NULL,
   DEV_ActionType_Id INT UNSIGNED NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_Requirement_UserSupplied_Id)   
); 
17 CREATE TABLE ADM_Requirement_CurrentSetting (
   ADM_Requirement_UserSupplied_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Table_Id INT UNSIGNED NOT NULL,
   ADM_Column_Id INT UNSIGNED NOT NULL,
   DEV_ActionType_Id INT UNSIGNED NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_Requirement_UserSupplied_Id)   
);

18 CREATE TABLE ADM_Requirement_TablePermission (
   ADM_Requirement_TablePermission_Id INT UNSIGNED AUTO_INCREMENT NOT NULL,
   ADM_Table_Id INT UNSIGNED NOT NULL,
   ADM_Setting_Id INT UNSIGNED NOT NULL,
   DEV_ActionType_Id INT UNSIGNED NOT NULL,
   PER_Permission_Id INT UNSIGNED NOT NULL,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedDate DATE NOT NULL ,
   PRIMARY KEY(ADM_Requirement_TablePermission_Id)   
);
