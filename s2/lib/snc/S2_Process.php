<?php
/* 
   Generally data is retrieved from the database or from an external source
   and then rendered straight to the screen. 
   
   In some circunstances it is preferable to get data from an external source, 
   store it and then post process it before rendering to the screen. 
   
   In some cases the post processing involves the user making decisions which 
   are presented in a wizard style.
   
   External definition sections then need a target table to load the data into.
   The data retreived should have the correct names and data types to be 
   directly inserted into the target table.
   
         "External": [
            {
               "Name":"Species Data",
               "Server":"data.yhedn.org.uk/r6",
               "Target":"/species2json.php",
               "Credentials":{
                  "Parameter":"Source",
                  "Encrypt":true
               },
               "Encrypt":true,
               "Parameters": [
                  {"Name":"Action","Type":"Static","Value":"GetSpeciesList"},
                  {"Name":"Source","Type":"Column","Value":"Source"},
                  {
                     "Name":"Sections",
                     "Type":"Static",
                     "Value":[
                        {
                           "Name":"Assessment",
                           "Output_Type":"Summary",
                           "Filter":"Indicator"
                        }
                     ]
                  }
               ]
            }
   
*/ 
?>