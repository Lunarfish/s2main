<?php 
// ===============================================================
// S&C Sites & Citations - Recorder connection library
// ---------------------------------------------------------------
// Written by YHEDN (Dan Jones)
// ===============================================================
// Retrieves data from Recorder6 database via an ODBC connection 
// and returns the data as JSON to be interpreted by the 
header("Cache-Control: no-cache, must-revalidate");
$paths = array(
   '../',
   '../../',
   '../../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
ini_set('include_path',ini_get('include_path')."${includepathseparator}./lib/${includepathseparator}");
include_once 'Zend/Db.php';
function SnC_getDatabaseConnection() {
   global $dbConfig;
   $db = Zend_Db::factory('Pdo_MySQL', $dbConfig);
   return $db;
}
?>