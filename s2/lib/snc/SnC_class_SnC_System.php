<?php
/*
CREATE TABLE SnC_System (
   Snc_System_Key INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   Snc_System_Name VACHAR(50) NOT NULL,
   Snc_Designation VARCHAR(10) NOT NULL   
)
*/
class SnC_System {
   private $table = 'SnC_System';
   private $domain_table = 'access_project_domains';
   private $user_table = 'access_project_users';
   private $level_table = 'access_project_levels';
   // name and value are public properties used for options in an HTML select 
   // element
   // the designation is also made public so that it can be passed back encoded 
   // in JSON
   public $Name;
   public $Value;
   public $Access;
   private $Snc_System_Key;
   private $Snc_System_Name;
   public $Snc_Designation;
   function SnC_System($k=null,$n=null,$d=null,$a=null) {
      $this->Snc_System_Key = $k;
      $this->Value = $this->getValue();
      $this->Snc_System_Name = $n;
      $this->Name = $this->getName();
      $this->Snc_Designation = $d;
      $this->Access = $a;
   }
   function getValue() {
      return $this->Snc_System_Key;
   }
   function getName() {
      return $this->Snc_System_Name;
   }
   function getContent() {
      
   
   }
   function insert() {
   
   }
   function update() {
   
   }
   function getSiteList() {
   
   }
   function addSite() {
   
   }
   function getMeetingList() {
      
   }
   function addMeeting() {
   
   }
   function getUserSystems($user) {
      $systems = array();
      $dbh = $GLOBALS['dbh'];
      
      $query = $dbh->select();
      $userdomain = substr($user,stripos($user,'@')+1); 
      $query->from($this->domain_table,
         array("project" => "project",
               "access"=> "access"));
      $query->where('domain = ?',$userdomain);
      $domains = $dbh->fetchAll($query);
            
      $query = $dbh->select();
      $query->from($this->user_table,
         array("project" => "project",
               "access"=> "access"));
      $query->where('user = ?',$user);
      $users = $dbh->fetchAll($query);
   //print_r($users);
      $query = $dbh->select();
      
      $query = $dbh->select();
      $query->from($this->table,
            array("Snc_System_Key" => "Snc_System_Key",
                  "Snc_System_Name"=> "Snc_System_Name",
                  "Snc_Designation"=> "Snc_Designation"));
      $rows = $dbh->fetchAll($query);
      for ($i=0;$i<count($rows);$i++) {
         $record = $rows[$i];
         $s = new SnC_System( $record["Snc_System_Key"],
                              $record["Snc_System_Name"],
                              $record["Snc_Designation"] );
         $da = 0;
         for ($j=0;$j<count($domains);$j++) {
            if ($domains[$j]['project'] == $record['Snc_System_Key']) 
               $da = max($da,$domains[$j]['access']);            
         }
         $ua = 0;
         for ($j=0;$j<count($users);$j++) {
            if ($users[$j]['project'] == $record['Snc_System_Key']) 
               $ua = max($ua,$users[$j]['access']);            
         }
         $access = max($da,$ua);
         if ($access > 0) {
            $query = $dbh->select();
            $query->from($this->level_table,
            array("level"        => "level",
                  "description"  => "description"));
            $query->where('project = ?', $s->Snc_System_Key);
            $query->where('access = ?', $access);
            $levels = $dbh->fetchAll($query);
            //$al = new Object();
            for ($j=0;$j<count($levels);$j++) {
               $al = new SnC_Setting($levels[$j]['level'], $access, $levels[$j]['description']);
            }
            $s->Access = $al;
            $systems[] = $s;
         }
      }
      return $systems;
   }
   function getUserAccess($user,$system) {
      $available = $this->getUserSystems($user);
      $response = array();
      for ($i=0;$i<count($available);$i++) {
         $sys = $available[$i];
         if ($sys->Value == $system) {
            return $sys;
         } 
      }
      return false;
   }
}
class SnC_Setting {
   public $Name;
   public $Value;
   public $Description;
   function SnC_Setting($n=null,$v=null,$d=null) {
      $this->Name = $n;
      $this->Value = $v;
      $this->Description = $d;
   }
}
?>