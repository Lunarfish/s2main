<?php
ini_set('include_path',ini_get('include_path').';./lib/;');
include_once 'Zend/Db.php';
include_once('snc/SnCDatabaseConnect2.php');

/*
CREATE TABLE ADM_Setting (
   ADM_Setting_Id INT UNSIGNED NOT NULL AUTO_INCREMENT,
   InTable VARCHAR(100),
   FromTable VARCHAR(100),
   Name VARCHAR(100),
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedOn DATE,
   PRIMARY KEY(ADM_Setting_Id)
);

CREATE TABLE ADM_SessionSetting (
   ADM_SessionSetting_Id INT UNSIGNED NOT NULL AUTO_INCREMENT,
   ADM_Setting_Id INT UNSIGNED NOT NULL,
   ADM_Session_Id INT UNSIGNED NOT NULL,
   SetTo VARCHAR(100),
   DisplayName VARCHAR(100),
   IsCurrent BOOLEAN,
   CreatedBy INT UNSIGNED NOT NULL,
   CreatedOn DATE,
   PRIMARY KEY(ADM_SessionSetting_Id)
)
*/

class Genera_Setting {
   private $dbh;
   private $settings;
   private $lookup_table = 'ADM_Setting';
   private $store_table = 'ADM_SessionSetting';
   private $ADM_SessionSetting_Id;
   private $ADM_Session_Id;
   private $SetTo;
   private $DisplayName;
   private $IsCurrent;
   private $CreatedBy;
   private $CreatedOn;
   /*
      Need a function to populate the variables for a new setting before call to 
      insert method
      
      Must need some way of showing CreatedBy system for things like sessions 
      before the user is logged in.
   */
   function Genera_Setting($session) {
      $this->dbh = SnC_getDatabaseConnection();
   }
   function create($intable,$fromtable,$name,$value,$iscurrent) {
      $this->ADM_Setting_Id = getSettingId($intable,$fromtable);
      $this->SetTo = $value;
      $this->DisplayName = $name;
      $this->IsCurrent = $iscurrent;      
   }
   /*
      Need  
   */
   function insert() {
   
   }
   function delete($session=null,$setting=null) {
   
   
   }
   function update($col,$val) {
   
   }
   function select($col,$val) {
   
   }
   function getSettingId($intable,$fromtable=null) {
            
   }
   function getSessionSettings($session) {
   
   }
   function setCurrent() {
      
   }      
}
?>