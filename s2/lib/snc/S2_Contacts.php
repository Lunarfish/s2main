<?php
/*
$paths = array(
   '../',
   '../../',
   '../../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
*/
include_once('settings.conf');
include_once('snc/S2_DBT.php');

class S2_Organisation extends DBT {
   protected $tablename = 'S2_Organisation';
   protected $displayname = 'Organisation';
   protected $alts = array('People'=>'S2_Person','Correspondence'=>'S2_Conversation');
   protected $columns = array(
      'S2_Organisation_ID' => 'Unique Key',
      'Name'               => 'Short Text',
      'Acronym'            => 'Short Text',
      'Established'        => 'Date',
      'Emails'             => '{"DataType":"LINKEDTOM","TargetType":"S2_Email","TargetField":"S2_Email_ID"}',
      'Address'            => '{"DataType":"LINKEDTOM","TargetType":"S2_Address","TargetField":"S2_Address_ID"}',
      'Phone'              => '{"DataType":"LINKEDTOM","TargetType":"S2_Phone_Number","TargetField":"S2_Phone_Number_ID"}',
      'Contacts'           => '{"DataType":"LINKEDTOM","TargetType":"S2_Person","TargetField":"S2_Person_ID","Property":"Contact"}',
      'Members'            => '{"DataType":"LINKEDTOM","TargetType":"S2_Person","TargetField":"S2_Person_ID","Property":"Membership"}'
   );
   protected $domain = 'contacts';
   protected $permissions = array(
      'Def'    => 'Contact Editor',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $defaultpermissions = array(
      'USR_Permissions' => '
"User_Id","Domain","Label","Value"
1,"contacts","System Administrator",1
1,"contacts","DPA Access",1
1,"contacts","Contact Editor",1
1,"contacts","Registered User",1',
      'USR_Default_Permissions' => '
"Domain","Label","Value"
"contacts","System Administrator",1
"contacts","DPA Access",1
"contacts","Contact Editor",1
"contacts","Registered User",1'
   );
}

class S2_Person extends DBT {
   protected $tablename = 'S2_Person';
   protected $displayname = 'People';
   protected $alts = array('Organisations'=>'S2_Organisation','Correspondence'=>'S2_Conversation');
   protected $columns = array(
      'S2_Person_ID'       => 'Unique Key',
      'First_Name'         => 'Short Text',
      'Last_Name'          => '{"DataType":"Short Text","Mandatory":1}',
      'Title'              => 'Short Text',
      'Emails'             => '{"DataType":"LINKEDTOM","TargetType":"S2_Email","TargetField":"S2_Email_ID"}',
      'Address'            => '{"DataType":"LINKEDTOM","TargetType":"S2_Address","TargetField":"S2_Address_ID"}',
      'Phone'              => '{"DataType":"LINKEDTOM","TargetType":"S2_Phone_Number","TargetField":"S2_Phone_Number_ID"}',
      'Contact_For'        => '{"DataType":"LINKEDTOM","TargetType":"S2_Organisation","TargetField":"S2_Organisation_ID","Property":"Contact"}',
      'Member_Of'          => '{"DataType":"LINKEDTOM","TargetType":"S2_Organisation","TargetField":"S2_Organisation_ID","Property":"Membership"}',
      'Conversations'      => '{"DataType":"LINKEDTOM","TargetType":"S2_Conversation","TargetField":"S2_Conversation_ID","Property":"Participant"}'
   );
   protected $domain = 'contacts';
   protected $permissions = array(
      'Def'    => 'Contact Editor',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      $name = $this->data['Last_Name'];
      if (isset($this->data['First_Name'])&&$this->data['First_Name']!='') $name .= ", ".$this->data['First_Name'];
      if (isset($this->data['Title'])&&$this->data['Title']!='') $name .= " (".$this->data['Title'].")";
      return $name;              
   }   
}
class S2_Organisation_Member extends DBT {
   protected $tablename = 'S2_Organisation_Member';
   protected $displayname = 'Member of...';
   protected $columns = array(
      'S2_Organisation_Member_ID'   => 'Unique Key',
      'S2_Organisation_ID'          => '{"DataType":"LINKEDTO","TargetType":"S2_Organisation","TargetField":"S2_Organisation_ID","Mandatory":1,"Current":1}',
      'S2_Person_ID'                => '{"DataType":"LINKEDTO","TargetType":"S2_Person","TargetField":"S2_Person_ID","Mandatory":1,"Current":1}'
   ); 
   protected $domain = 'contacts';
   protected $permissions = array(
      'Def'    => 'Contact Editor',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      $names = array();
      foreach($this->columns as $col => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $table = $params->TargetType;
            $tcol = $params->TargetField;
         } else if (preg_match('/([^\:]+)\:([^\.]+)\.(.+)/',$type,$matches)) {
            $type = $matches[1];
            $table = $matches[2];
            $tcol = $matches[3];
         }
         if (isset($table) && isset($tcol)) {
            $link = null;
            eval("\$link = new $table();");
            $id = (is_object($this->data[$col]))
                  ?$this->data[$col]->Id
                  :$this->data[$col];
            $link->loaddata($id);
            $names[] = $link->getname();
         }  
      }
      return join(':',$names);   
   }   
}
class S2_Organisation_Contact extends DBT {
   protected $tablename = 'S2_Organisation_Contact';
   protected $displayname = 'Contact for...';
   protected $columns = array(
      'S2_Organisation_Member_ID'   => 'Unique Key',                        
      'S2_Organisation_ID'          => '{"DataType":"LINKEDTO","TargetType":"S2_Organisation","TargetField":"S2_Organisation_ID","Mandatory":1,"Current":1}',
      'S2_Person_ID'                => '{"DataType":"LINKEDTO","TargetType":"S2_Person","TargetField":"S2_Person_ID","Mandatory":1,"Current":1}'
   ); 
   protected $domain = 'contacts';
   protected $permissions = array(
      'Def'    => 'Contact Editor',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      $names = array();
      foreach($this->columns as $col => $type) {
         if (!isset($this->selectedby)||!($col == $this->selectedby)) {
            if (preg_match('/^\{/',$type)) {
               $params = json_decode($type);
               $type = $params->DataType;
               $table = $params->TargetType;
               $tcol = $params->TargetField;
            } else if (preg_match('/([^\:]+)\:([^\.]+)\.(.+)/',$type,$matches)) {
               $type = $matches[1];
               $table = $matches[2];
               $tcol = $matches[3];
            }
            if (isset($table) && isset($tcol)) {
               $link = null;
               eval("\$link = new $table();");
               $id = (is_object($this->data[$col]))
                     ?$this->data[$col]->Id
                     :$this->data[$col];
               $link->loaddata($id);
               $names[] = $link->getname();
            }
         }  
      }
      return join(':',$names);
   }   
}

class S2_Address extends DBT {
   protected $tablename = 'S2_Address';
   protected $displayname = 'Address';
   protected $columns = array(
      'S2_Address_ID'   => 'Unique Key',
      'Line_1'          => '{"DataType":"Short Text","Mandatory":1}',
      'Line_2'          => 'Short Text',
      'City'            => 'Short Text',
      'Post_Code'       => '{"DataType":"Short Text","Mandatory":1}'
   ); 
   protected $domain = 'contacts';
   protected $permissions = array(
      'Def'    => 'Contact Editor',
      'List'   => 'DPA Access',
      'View'   => 'DPA Access'
   );
   function getname() {
      $name = ""; 
      $name .= (isset($this->data['Line_1']))?$this->data['Line_1']."\n":"";
      $name .= (isset($this->data['Line_2']))?$this->data['Line_2']."\n":"";
      $name .= (isset($this->data['City']))?$this->data['City']."\n":"";
      $name .= (isset($this->data['Post_Code']))?$this->data['Post_Code']:"";
      return $name;
   }   
}

class S2_Phone_Number extends DBT {
   protected $tablename = 'S2_Phone_Number';
   protected $displayname = 'Phone Number';
   protected $columns = array(
      'S2_Phone_Number_ID'       => 'Unique Key',
      'S2_Phone_Number_Type_ID'  => '{"DataType":"LINKEDTO","TargetType":"S2_Phone_Number_Type","TargetField":"S2_Phone_Number_Type_ID","Mandatory":1}',
      'Number'                   => '{"DataType":"Short Text","Mandatory":1}',
      'Extension'                => 'Short Text'
   ); 
   protected $domain = 'contacts';
   protected $permissions = array(
      'Def'    => 'Contact Editor',
      'List'   => 'DPA Access',
      'View'   => 'DPA Access'
   );
   function getname() {
      return $this->data['Number'];
   }      
}
class S2_Phone_Number_Type extends DBT {
   protected $tablename = 'S2_Phone_Number_Type';
   protected $displayname = 'Type';
   protected $columns = array(
      'S2_Phone_Number_Type_ID'  => 'Unique Key',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}'
   );
   protected $domain = 'contacts';
   protected $permissions = array(  
      'Def'    => 'Contact Editor',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'S2_Phone_Number_Type' => '
"Name"
"Landline"
"Mobile"
"Office"'
   );   
}

class S2_Email extends DBT {
   protected $tablename = 'S2_Email';
   protected $displayname = 'Email';
   protected $columns = array(
      'S2_Email_ID'        => 'Unique Key',
      'S2_Email_Type_ID'   => '{"DataType":"LINKEDTO","TargetType":"S2_Email_Type","TargetField":"S2_Email_Type_ID"}',
      'Email'              => '{"DataType":"Email","Mandatory":1}'
   ); 
   protected $domain = 'contacts';
   protected $permissions = array(
      'Def'    => 'Contact Editor',
      'List'   => 'DPA Access',
      'View'   => 'DPA Access'
   );
   function getname() {
      return $this->data['Email'];
   }      
}
class S2_Email_Type extends DBT {
   protected $tablename = 'S2_Email_Type';
   protected $displayname = 'Type';
   protected $columns = array(
      'S2_Email_Type_ID'  => 'Unique Key',
      'Name'              => '{"DataType":"Short Text","Mandatory":1}'
   );
   protected $domain = 'contacts';
   protected $permissions = array(  
      'Def'    => 'Contact Editor',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'S2_Email_Type' => '
"Name"
"Personal"
"Work"'
   );   
}

// Correspondence
class S2_Correspondence_Type extends DBT {
   protected $tablename = 'S2_Correspondence_Type';
   protected $displayname = 'Correspondence Type';
   protected $show = array();
   protected $columns = array(
      'S2_Correspondence_Type_ID'      => 'Unique Key',
      'Name'                           => '{"DataType":"Short Text","Mandatory":1}',
      'Code'                           => 'Short Text',
      'Description'                    => 'Long Text' 
   );
   protected $domain = 'contacts';
   protected $permissions = array(
      'Def'    => 'Contact Editor',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'S2_Correspondence_Type' => '
"Name","Code"
"Post",1
"Phone",2
"Email",3
"Meeting",4'
   );   
}
class S2_Correspondence_Theme_Type extends DBT {
   protected $tablename = 'S2_Correspondence_Theme_Type';
   protected $displayname = 'Correspondence Theme';
   protected $show = array();
   protected $columns = array(
      'S2_Correspondence_Theme_Type_ID'      => 'Unique Key',
      'Name'                                 => '{"DataType":"Short Text","Mandatory":1}',
      'Code'                                 => 'Short Text',
      'Description'                          => 'Long Text' 
   );
   protected $domain = 'contacts';
   protected $permissions = array(
      'Def'    => 'Contact Editor',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'S2_Correspondence_Theme_Type' => '
"Name","Code"
"Land Ownership",1
"Permission To Survey",2
"Management Advice",3
"Management Agreement",4
"Site Condition",5
"Species Data",6
"Habitat Data",7
"Key Sites",8'
   );   
}
class S2_Correspondence_Theme extends DBT {
   protected $tablename = 'S2_Correspondence_Theme';
   protected $displayname = 'Correspondence Theme';
   protected $show = array();
   protected $columns = array(
      'S2_Correspondence_Theme_ID'        => 'Unique Key',
      'S2_Conversation_ID'                => '{"DataType":"LINKEDTO","TargetType":"S2_Conversation","TargetField":"S2_Conversation_ID","Mandatory":1,"Current":1}',
      'S2_Correspondence_Theme_Type_ID'   => '{"DataType":"LINKEDTO","TargetType":"S2_Correspondence_Theme_Type","TargetField":"S2_Correspondence_Theme_Type_ID","Mandatory":1}'
   );
   protected $domain = 'contacts';
   protected $permissions = array(
      'Def'    => 'Contact Editor',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}

class S2_Conversation extends DBT  {
   protected $tablename = 'S2_Conversation';
   protected $displayname = 'Conversation';
   protected $alts = array('Organisations'=>'S2_Organisation','Person'=>'S2_Person','Type'=>'S2_Correspondence_Type','Theme'=>'S2_Correspondence_Theme');
   protected $show = array('S2_Conversation_Participant','S2_Correspondence_Theme','S2_Correspondence_Relates_To','S2_Correspondence_Record');
   protected $columns = array(
      'S2_Conversation_ID'       => 'Unique Key',
      'Started'                  => '{"DataType":"Date","Mandatory":1}',
      'Name'                     => 'Short Text',
      'Description'              => 'Long Text',
      'Participants'             => '{"DataType":"LINKEDTOM","TargetType":"S2_Person","TargetField":"S2_Person_ID","Property":"Participant"}'      
   );
   protected $domain = 'contacts';
   protected $permissions = array(
      'Def'    => 'Contact Editor',
      'List'   => 'DPA Access',
      'View'   => 'DPA Access'
   );
   function getname() {
      $name = $this->data['Name'] . " (" . $this->data['Started'] . ")";
      return $name;              
   }   
}
class S2_Conversation_Participant extends DBT {
   protected $tablename = 'S2_Conversation_Participant';
   protected $displayname = 'Participant';
   protected $alts = array('Organisations'=>'S2_Organisation','Person'=>'S2_Person','Type'=>'S2_Correspondence_Type','Theme'=>'S2_Correspondence_Theme');
   protected $columns = array(
      'S2_Conversation_Participant_ID'    => 'Unique Key',
      'S2_Conversation_ID'                => '{"DataType":"LINKEDTO","TargetType":"S2_Conversation","TargetField":"S2_Conversation_ID","Mandatory":1,"Current":1,"Inherit":1}',
      'Participant'                       => '{"DataType":"LINKEDTO","TargetType":"S2_Person","TargetField":"S2_Person_ID","Mandatory":1}'      
   );
   protected $domain = 'contacts';
   protected $permissions = array(
      'Def'    => 'Contact Editor',
      'List'   => 'DPA Access',
      'View'   => 'DPA Access'
   );
   function getname() {
      $dbt2 = new S2_Person();
      $dbt2->loaddata(is_object($this->data['Participant'])?$this->data['Participant']->Id:$this->data['Participant']);
      $name = $dbt2->getname();
      return $name;              
   }   

}
class S2_Correspondence_Record extends DBT  {
   protected $tablename = 'S2_Correspondence_Record';
   protected $displayname = 'Correspondence Record';
   protected $alts = array('Organisations'=>'S2_Organisation','Person'=>'S2_Person','Type'=>'S2_Correspondence_Type','Theme'=>'S2_Correspondence_Theme');
   protected $show = array('S2_Correspondence_Theme','S2_Correspondence_Relates_To');
   protected $columns = array(
      'S2_Correspondence_Record_ID'       => 'Unique Key',
      'S2_Conversation_ID'                => '{"DataType":"LINKEDTO","TargetType":"S2_Conversation","TargetField":"S2_Conversation_ID","Mandatory":1,"Current":1}',
      'Sent_By'                           => '{"DataType":"LINKEDTO","TargetType":"S2_Conversation_Participant","TargetField":"S2_Conversation_Participant_ID","Mandatory":1,"Inherit":1}',
      'Sent_To'                           => '{"DataType":"LINKEDTO","TargetType":"S2_Conversation_Participant","TargetField":"S2_Conversation_Participant_ID","Mandatory":1,"Inherit":1}',
      'Type'                              => '{"DataType":"LINKEDTO","TargetType":"S2_Correspondence_Type","TargetField":"S2_Correspondence_Type_ID","Mandatory":1}',
      'Dated'                             => '{"DataType":"Date","Mandatory":1,"Derives":[{"Column":"Due_Date","Method":"s2_targetdate","Params":[{"Type":"Value"},{"Type":"Static","Value":"+2W"}]}]}',
      'Due_Date'                          => '{"DataType":"Date"}',
      'Description'                       => 'Long Text',
      'Copy'                              => 'File Path'
   );
   protected $domain = 'contacts';
   protected $permissions = array(
      'Def'    => 'Contact Editor',
      'List'   => 'DPA Access',
      'View'   => 'DPA Access'
   );
   function getname() {
      $name = $this->data['Dated'];
      if (isset($this->data['Type'])) {
         $name = (is_object($this->data['Type']))
            ? $this->data['Type']->Name." ($name): "
            : $this->data['Type']." ($name): ";
      } else $name .= ": ";  
      if (isset($this->data['Sent_By'])) {
         $name = (is_object($this->data['Sent_By']))
            ? $name.$this->data['Sent_By']->Name
            : $name.$this->data['Sent_By'];
      }  
      if (isset($this->data['Sent_To'])) {
         $name = (is_object($this->data['Sent_To']))
            ? "$name to ".$this->data['Sent_To']->Name
            : "$name to ".$this->data['Sent_To'];
      }  
      return $name;              
   }   
}
class S2_Correspondence_Relates_To extends DBT {
   protected $tablename = 'S2_Correspondence_Relates_To';
   protected $displayname = 'Correspondence Relates To';
   protected $show = array();
   protected $columns = array(
      'S2_Correspondence_Theme_ID'        => 'Unique Key',
      'S2_Conversation_ID'                => '{"DataType":"LINKEDTO","TargetType":"S2_Conversation","TargetField":"S2_Conversation_ID","Current":1,"Mandatory":1}',
      'Relates_To'                        => '{"DataType":"INTERFACE","Domain":"sites","ValidTypes":["S2_Site","S2_Meeting"],"Mandatory":1}'
   );
   protected $domain = 'contacts';
   protected $permissions = array(
      'Def'    => 'Contact Editor',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}
?>