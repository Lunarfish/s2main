<?php
/*
$paths = array(
   '../',
   '../../',
   '../../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
*/
include_once('settings.conf');
include_once('snc/S2_DBT.php');

class S2_Habitat_Source extends DBT {
   protected $tablename = 'S2_Habitat_Source';
   protected $displayname = 'Habitat Source';
   protected $columns = array(
      'S2_Authority_ID' => 'Unique Key',
      'Name'            => 'Short Text',
      'Established'     => 'Date'
   );
   protected $domain = 'habitats';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $defaultpermissions = array(
'USR_Default_Permissions' => 
'"Domain","Label","Description","Value","Anon" 	
"habitats","Habitat Administrator","Add and edit projects",0,0
"habitats","Registered User","View unprotected data",1,0',
'USR_Permissions' => 
'"User_Id","Domain","Label","Description","Value"
1,"habitats","Registered User",NULL,1
1,"habitats","Habitat Administrator",NULL,1'   
   );  
}



/*
class Example extends DBT {
   //
   // CreatedBy and CreatedOn are added and populated automatically
   //
   protected $tablename = 'ExampleTable1';
   protected $displayname = 'Example Table'; // Describes how the table will appear on the screen. 
   protected $columns = array(
      'Example_ID'      => 'Unique Key',
      'Name'            => 'Short Text',
      'Value'           => 'Number',
      'Link'            => 'LINKEDTO:DBTableName1.DBColumnName:Optional'                  
   );
}
*/
?>