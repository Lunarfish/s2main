<?php
/*
$paths = array(
   '../',
   '../../',
   '../../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
*/
include_once('settings.conf');
include_once('snc/S2_DBT.php');

class S2_Project extends DBT {
   protected $tablename = 'S2_Project';
   protected $displayname = 'Project';
   protected $perpage = 3;
   protected $show = array('S2_Project_Boundary','S2_Project_Description','S2_Project_Funding');
   protected $view = '[
      {
         "Name":"Overview",
         "Data":[
            {
               "Name":"Current Description",
               "Table":"S2_Project_Description",
               "Show":"Current"
            },
            {
               "Name":"Current Boundary",
               "Table":"S2_Project_Boundary",
               "Show":"Current"
            }            
         ]        
      },
      {
         "Name":"Incubator"
      },
      {
         "Name":"Descriptions",
         "Data":[
            {
               "Table":"S2_Project_Description"
            },
            {
               "Table":"S2_Project_Scope_Creep"
            }
         ]
      },
      {
         "Name":"Staffing",
         "Data":[
            {
               "Table":"S2_Project_Staff"
            }
         ]
      },
      {
         "Name":"Key Species",
         "Data":[
            {
               "Table":"S2_Project_Species"
            }
         ]
      },
      {
         "Name":"Boundaries",
         "Data":[
            {
               "Name":"Boundary History",
               "Table":"S2_Project_Boundary",
               "Show":"All"               
            }
         ]
      },
      {
         "Name":"Funding",
         "Data":[
            {
               "Table":"S2_Project_Funding"
            }
         ]
      }
   ]'; 
   protected $columns = array(
      'S2_Project_ID'         => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text","Mandatory":1}',
      'Contact'               => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"]}',
      'BAP_Habitats'          => '{"DataType":"LINKEDTOM","TargetType":"S2_BAP_Habitat_P","TargetField":"S2_BAP_Habitat_P_ID","Current":1}',
      'From_Date'             => 'Date',
      'Until_Date'            => 'Date'
   );
   protected $domain = 'projects';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
   protected $defaultpermissions = array(
'USR_Default_Permissions' => 
'"Domain","Label","Description","Value","Anon" 	
"projects","System Administrator","Add and edit projects",0,0
"projects","Partnership Administrator","Add and edit project data",0,0
"projects","Partnership Member","View project data",0,0
"projects","Registered User","View unprotected data",1,0',
'USR_Permissions' => 
'"User_Id","Domain","Label","Description","Value"
1,"projects","Registered User",NULL,1
1,"projects","Partnership Member",NULL,1
1,"projects","Partnership Administrator",NULL,1
1,"projects","System Administrator",NULL,1'   
   );  
}


class S2_Project_Boundary extends DBT {
   protected $tablename = 'S2_Project_Boundary';
   protected $displayname = 'Boundary';
   protected $show = array();
   protected $columns = array(
      'S2_Project_Boundary_ID'   => 'Unique Key',
      'S2_Project_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_Project","TargetField":"S2_Project_ID","Mandatory":1,"Current":1}',
      'Review_Date'              => 'Date',
      'Boundary'                 => 'Map Data',
      'Notes'                    => 'Long Text',
      'Version'                  => '{"DataType":"Version","VersionOfType":"S2_Project","VersionOfField":"S2_Project_ID"}' 
   );
   protected $domain = 'projects';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );
   function getname() {
      return "V" . $this->data['Version'] . " (" . $this->data['Review_Date'] . ")";
   }   
}

class S2_Project_Description extends DBT {
   protected $tablename = 'S2_Project_Description';
   protected $displayname = 'Description';
   protected $show = array();
   protected $columns = array(
      'S2_Project_Description_ID'   => 'Unique Key',
      'S2_Project_ID'               => '{"DataType":"LINKEDTO","TargetType":"S2_Project","TargetField":"S2_Project_ID","Mandatory":1,"Current":1}',
      'Dated'                       => 'Date',
      'Project_Description'         => 'Long Text',
      'Version'                     => '{"DataType":"Version","VersionOfType":"S2_Project","VersionOfField":"S2_Project_ID"}' 
   );
   protected $domain = 'projects';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );
   function getname() {
      return "V" . $this->data['Version'] . " (" . $this->data['Dated'] . ")";
   }   
}
class S2_Project_Staff extends DBT {
   protected $tablename = 'S2_Project_Staff';
   protected $displayname = 'Staff';
   protected $show = array();
   protected $columns = array(
      'S2_Project_Staff_ID'         => 'Unique Key',
      'S2_Project_ID'               => '{"DataType":"LINKEDTO","TargetType":"S2_Project","TargetField":"S2_Project_ID","Mandatory":1,"Current":1}',
      'Paid_Staff'                  => 'Number',
      'Volunteers'                  => 'Number' 
   );
   protected $domain = 'projects';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );
   function getname() {
      return "Total: ".($this->data['Paid_Staff'] + $this->data['Volunteers']);
   }   
}
class S2_Project_Species extends DBT {
   protected $tablename = 'S2_Project_Species';
   protected $displayname = 'Key Species';
   protected $show = array();
   protected $columns = array(
      'S2_Project_Species_ID'    => 'Unique Key',
      'S2_Project_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_Project","TargetField":"S2_Project_ID","Mandatory":1,"Current":1}',
      'Species'                  => 'Taxa' 
   );
   protected $domain = 'projects';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );
   function getname() {
      return $this->data['Species']->Name;
   }   
}
class S2_Project_Funding extends DBT {
   protected $tablename = 'S2_Project_Funding';
   protected $displayname = 'Funding';
   protected $show = array();
   protected $columns = array(
      'S2_Project_Funding_ID'    => 'Unique Key',
      'S2_Project_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_Project","TargetField":"S2_Project_ID","Mandatory":1,"Current":1}',
      'Total_Funding'            => '{"DataType":"Currency"}',
      'Secured_Funding'          => '{"DataType":"Currency"}',
      'Funding_Required'         => '{"DataType":"Currency"}' 
   );
   protected $domain = 'projects';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );
   function getname() {
      return $this->data["Total_Funding"];
   }   
}
class S2_Project_Scope_Creep extends DBT {
   protected $tablename = 'S2_Project_Scope_Creep';
   protected $displayname = 'Scope Creep';
   protected $show = array();
   protected $columns = array(
      'S2_Project_Description_ID'   => 'Unique Key',
      'S2_Project_ID'               => '{"DataType":"LINKEDTO","TargetType":"S2_Project","TargetField":"S2_Project_ID","Mandatory":1,"Current":1}',
      'Dated'                       => 'Date',
      'Element'                     => 'Medium Text',
      'Description'                 => 'Long Text' 
   );
   protected $domain = 'projects';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );
   function getname() {
      return $this->data['Dated'];
   }   
}


class S2_BAP_Habitat_P extends DBT {
   protected $tablename = 'S2_BAP_Habitat_P';
   protected $displayname = 'BAP Habitat';
   protected $show = array();
   protected $columns = array(
      'S2_BAP_Habitat_P_ID'      => 'Unique Key',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Code'                     => 'Short Text',
      'Description'              => 'Long Text' 
   );
   protected $domain = 'projects';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );
   protected $primingdata = array(
      'S2_BAP_Habitat_P' => '
"Name","Code"
"Aquifer Fed Naturally Fluctuating Water Bodies",1
"Arable Field Margins",2
"Blanket Bog",3
"Blue mussel beds",4
"Calaminarian Grasslands",5
"Carbonate mounds",6
"Coastal and Floodplain Grazing Marsh",7
"Coastal saltmarsh",8
"Coastal Sand Dunes",9
"Coastal Vegetated Shingle",10
"Cold-water coral reefs",11
"Deep-sea sponge communities",12
"Estuarine rocky habitats",13
"Eutrophic Standing Waters",14
"File shell beds",15
"Fragile sponge and anthozoan communities on subtidal rocky habitats",16
"Hedgerows",17
"Horse mussel beds",18
"Inland Rock Outcrop and Scree Habitats",19
"Intertidal chalk",20
"Intertidal mudflats",21
"Intertidal underboulder communities",22
"Limestone Pavements",23
"Lowland Beech and Yew Woodland",24
"Lowland Calcareous Grassland",25
"Lowland Dry Acid Grassland",26
"Lowland Fens",27
"Lowland Heathland",28
"Lowland Meadows",29
"Lowland Mixed Deciduous Woodland",30
"Lowland Raised Bog",31
"Machair",32
"Maerl beds",33
"Maritime Cliff and Slopes",34
"Mesotrophic Lakes",35
"Mountain Heaths and Willow Scrub",36
"Mud habitats in deep water",37
"Native Pine Woodlands",38
"Oligotrophic and Dystrophic Lakes",39
"Open Mosaic Habitats on Previously Developed Land",40
"Peat and clay exposures",41
"Ponds",42
"Purple Moor Grass and Rush Pastures",43
"Reedbeds",44
"Rivers",45
"Sabellaria alveolata reefs",46
"Sabellaria spinulosa reefs",47
"Saline lagoons",48
"Seagrass beds",49
"Seamount communities",50
"Serpulid reefs",51
"Sheltered muddy gravels",52
"Subtidal chalk",53
"Subtidal sands and gravels",54
"Tide-swept channels",55
"Traditional Orchards",56
"Upland Birchwoods",57
"Upland Calcareous Grassland",58
"Upland Flushes, Fens and Swamps",59
"Upland Hay Meadows",60
"Upland Heathland",61
"Upland Mixed Ashwoods",62
"Upland Oakwood",63
"Wet Woodland",64
"Wood-Pasture and Parkland",65'
   );   
}

/*
CREATE OR REPLACE VIEW S2_SiteMap AS 
SELECT A.Name AS Authority, S.S2_Site_ID, S.Name AS Site_Name, S.Site_Code, B.Version, B.Boundary, D.Name AS Designation, DS.Name AS Status 
FROM S2_Site_Boundary AS B  
INNER JOIN S2_Site AS S ON (S.S2_Site_ID = B.S2_Site_ID)
INNER JOIN S2_Site_Designation AS SD ON SD.S2_Site_ID = B.S2_Site_ID
INNER JOIN S2_Designation AS D ON SD.S2_Designation_ID = D.S2_Designation_ID 
INNER JOIN S2_Site_Designation_Status AS SDS ON (SDS.S2_Site_Designation_ID = SD.S2_Site_Designation_ID) 
INNER JOIN S2_Designation_Status AS DS ON DS.S2_Designation_Status_ID = SDS.S2_Designation_Status_ID 
INNER JOIN S2_Authority AS A ON A.S2_Authority_ID = S.S2_Authority_ID
WHERE (S.Boundary = B.Version)
AND (SD.Site_Designation_Status = SDS.Version);
*/
/*
class Example extends DBT {
   //
   // CreatedBy and CreatedOn are added and populated automatically
   //
   protected $tablename = 'ExampleTable1';
   protected $displayname = 'Example Table'; // Describes how the table will appear on the screen. 
   protected $columns = array(
      'Example_ID'      => 'Unique Key',
      'Name'            => 'Short Text',
      'Value'           => 'Number',
      'Link'            => 'LINKEDTO:DBTableName1.DBColumnName:Optional'                  
   );
}
*/
?>