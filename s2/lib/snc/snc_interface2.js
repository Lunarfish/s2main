// communication is handled via the cdm_orequest.js library.

var settings;
var menus;
function cleardialog(dlinerid) {
   var dliner = document.getElementById(dlinerid);
   while(dliner.lastChild.id != 'dialog-title') {
      dliner.removeChild(dliner.lastChild);
   }
   return true;
}

function snc_pageload() {
   placescreen();
   var encrypt = readCookie('encrypt');
   if (encrypt) auth();
   else enableinterface();
}
function enableinterface() {
   var settings = readCookie('Settings');   
   //if (!settings) {
      var json = new Object();
      json.Settings = JSON.parse(settings);
      action = new Object();
      action.Task = 'startsession';
      json.Action = action;
      snc_executeTask(json,true);
   //}
   var menus = readCookie('Menus');
   if (!menus) snc_getMenuJSON();
   else snc_createLeftMenus();          
}

function snc_getMenuJSON() {
   inprogress();   
   var server = unescape(readCookie('cdm_server'));
   var r = new Object();
   r.target = 'http://'+server+'/snc_getMenus.php';
   r = cdm_wrapper(r);
   var ac = new AjaxClass();
   ac.url = r.target;
   if (r.request) ac.parameters = r.request;
   ac.responder = receiveMenus;
   ac.init();
   ac.send();        
}
function receiveMenus(json) {
   var received = JSON.parse(json);
   var menus;
   menus = new Object();
   menus.Actions = received.Actions;
   createCookie('Actions',JSON.stringify(menus));
   menus = new Object();
   menus.Settings = received.Settings;
   createCookie('Headers',JSON.stringify(menus));
   menus = new Object();
   menus.Menus = received.Menus;
   createCookie('Menus',JSON.stringify(menus));
   snc_createLeftMenus(menus);
   completed();
   return true;
}
/*
   snc_executeTask sends a JSON encoded request to the same place
   the response is always returned to snc_actionStatus
   
   snc_send allows more flexibility as you can send a request to anyway,
   encode the parameters any way you like and assign whatever function you 
   want to process the response. 
   
   the response will be passed as received so the receiver function has to check 
   for encryption settings and decrypt the message if necessary   
*/
function snc_executeTask(json) {
   var server = unescape(readCookie('cdm_server'));
   var r = new Object();
   r.target = 'http://'+server+'/snc_sqlitetasks2.php';
   var parameters = new Object();
   parameters['json'] = json;
   r.request = parameters;
   r.responder = snc_actionStatus;
   snc_send(r);    
}
function snc_send(r) {   
   inprogress();   
   r = cdm_wrapper(r);
   var ac = new AjaxClass();
   ac.url = r.target;
   ac.parameters = r.request;
   if (r.responder) ac.responder = r.responder;
   if (r.method) ac.Method = r.method;
   if (r.sync) ac.Async = !r.sync;
   ac.init();
   var resp = ac.send();
   if (r.sync) return resp;
}
function snc_actionStatus(json) {
   var json_obj = JSON.parse(json);
   if (json_obj.Settings) {
      var settings = json_obj.Settings;
      createCookie('Settings',JSON.stringify(settings));
      snc_createLeftMenus(menus);
   }
   if (json_obj.Status) {
      alert(json_obj.Status.Message);
   }
   if (json_obj.Choices) {
      var newchoices = json_obj.Choices;
      var choices = JSON.parse(readCookie('Choices'));
      if (!choices) choices = new Object();
      for (prop in newchoices) choices[prop] = newchoices[prop];
      createCookie('Choices',JSON.stringify(choices));
   }
   if (json_obj.Action) snc_actionDialog(json_obj.Action);
   if (json_obj.Options) snc_report_json_obj(json);
   completed();
}
function snc_clearMenus(menuid) {
   var menudiv = document.getElementById(menuid);
   while(menudiv.childNodes.length > 0) {
      menudiv.removeChild(menudiv.lastChild);
   }
   return true;
}
function snc_actionDialog(id) {
   var bits = id.split(/\-/);
   var menulink = bits[0];
   var actionlink = bits[1];
   var dialog,div,node,button,input,select,option,element,type,table,thead,tbody,tr,th,td;
   snc_clearMenus('incontentdialog');   
   dialog = document.getElementById('incontentdialog');
   dialog.className = 'bordered';
   div = document.createElement('DIV');
   var menus = JSON.parse(readCookie('Actions'));   
   for (i=0;i<menus.Actions.length;i++) {
       action = menus.Actions[i];
      menuname = action.MenuName;
      actionname = action.ActionName;
      if (menuname == menulink && actionname == actionlink) {
         node = document.createElement('H2');
         node.appendChild(document.createTextNode(menulink));
         div.appendChild(node);
         node = document.createElement('H3');
         node.appendChild(document.createTextNode(action.ActionTitle));
         div.appendChild(node);
         
         input = document.createElement('INPUT');
         input.id = 'DMenu';
         input.type = 'hidden';
         input.value = menulink;
         div.appendChild(input);
         
         input = document.createElement('INPUT');
         input.id = 'DAction';
         input.type = 'hidden';
         input.value = actionlink;
         div.appendChild(input);
         if (action.Message) { 
            node = document.createElement('P');
            node.appendChild(document.createTextNode(action.Message));
            div.appendChild(node);
         }

         table = document.createElement('TABLE');
         tbody = document.createElement('TBODY');
         for (element in action.Parameters) {
            type = action.Parameters[element];
            switch (type) {
            case 'Text': {
               tr = document.createElement('TR');
               th = document.createElement('TH');
               th.appendChild(document.createTextNode(element));
               tr.appendChild(th);
               td = document.createElement('TD');
               input = document.createElement('INPUT');
               input.id = element;
               input.type = 'text';
               td.appendChild(input);
               tr.appendChild(td);
               tbody.appendChild(tr);
            }break;
            case 'Password': {
               tr = document.createElement('TR');
               th = document.createElement('TH');
               th.appendChild(document.createTextNode(element));
               tr.appendChild(th);
               td = document.createElement('TD');
               input = document.createElement('INPUT');
               input.id = element;
               input.type = 'password';
               td.appendChild(input);
               tr.appendChild(td);
               tbody.appendChild(tr);
            }break;
            case 'Confirmed': {
               tr = document.createElement('TR');
               th = document.createElement('TH');
               th.appendChild(document.createTextNode(element));
               tr.appendChild(th);
               td = document.createElement('TD');
               input = document.createElement('INPUT');
               input.id = element;
               input.type = 'password';
               td.appendChild(input);
               tr.appendChild(td);
               tbody.appendChild(tr);
               tr = document.createElement('TR');
               th = document.createElement('TH');
               th.appendChild(document.createTextNode('Confirm'));
               tr.appendChild(th);
               td = document.createElement('TD');
               input = document.createElement('INPUT');
               input.id = 'Confirm_'+element;
               input.type = 'password';
               input.onchange = function() {snc_checkMatch(this.id,this.value);}
               td.appendChild(input);
               tr.appendChild(td);
               tbody.appendChild(tr);
            }break;
            case 'Choice': {
               var choice,c;
               tr = document.createElement('TR');
               th = document.createElement('TH');
               th.appendChild(document.createTextNode(element));
               tr.appendChild(th);
               td = document.createElement('TD');
               select = document.createElement('SELECT');
               select.id = element;
               select.className = 'text';
               // choices can either be hardcoded into the menu JSON or 
               // they can be retrieved from the server via the menu item 
               // GetAction task
               if (element.Choices) {
                  choice = element.Choices;
                  for (c in choice) {
                     option = document.createElement('OPTION');
                     option.value = choice[c]['Value'];
                     option.appendChild(document.createTextNode(choice[c]['Name']));
                     select.appendChild(option);                                                
                  }
               } else {
                  var choices = JSON.parse(readCookie('Choices'));
                  if (!choices || !choices[element]) {
                     var server = unescape(readCookie('cdm_server'));
                     var r = new Object();
                     r.target = 'http://'+server+'/snc_sqlitetasks.php';
                     var json = new Object();
                     var a = new Object();
                     // figure out how to set tasks
                     a.Task = action.MenuName + '-' + action.GetAction;
                     json.Action = a;
                     json.Settings = JSON.parse(unescape(readCookie('Settings')));
                     var parameters = new Object();
                     parameters['json'] = JSON.stringify(json);
                     r.request = parameters;
                     r.sync = true;
                     response = (readCookie('encrypt'))?cdm_decrypt(snc_send(r)):snc_send(r);
                     response = JSON.parse(response);
                     if (!choices) choices = new Object(); 
                     choices[element] = response['Choices']['Partnerships'];
                     createCookie('Choices',JSON.stringify(choices));
                     completed();                            
                  } 
                  choice = choices[element];
                  for (c in choice) {
                     option = document.createElement('OPTION');
                     option.value = choice[c]['Value'];
                     option.appendChild(document.createTextNode(choice[c]['Name']));
                     select.appendChild(option);                                                
                  }
               }
               td.appendChild(select);
               tr.appendChild(td);
               tbody.appendChild(tr);
            }break;
            }
         }
         table.appendChild(tbody);
         div.appendChild(table);
         button = document.createElement('BUTTON');
         button.appendChild(document.createTextNode('Send'));
         button.onclick = function() {snc_performDialogAction();};
         div.appendChild(button);
         button = document.createElement('BUTTON');
         button.appendChild(document.createTextNode('Cancel'));
         button.onclick = function() {
            snc_clearMenus('incontentdialog');
            var dialog = document.getElementById('incontentdialog');
            dialog.className = 'notbordered';
         };
         div.appendChild(button);
         dialog.appendChild(div);                  
      }
   } 
   //showdialog(); 
}
function snc_performDialogAction() {
   var menus = JSON.parse(readCookie('Actions')); 
   var json = new Object();
   json.Settings = JSON.parse(readCookie('Settings'));
   
   var daction = document.getElementById('DAction').value;
   var dmenu = document.getElementById('DMenu').value;    
   var action = new Object();
   action.Task = dmenu + '-' + daction;
   for (i=0;i<menus.Actions.length;i++) {
      var maction = menus.Actions[i];
      menuname = maction.MenuName;
      actionname = maction.ActionName
      if (menuname == dmenu && actionname == daction) {
         for (element in maction.Parameters) {
            action[element] = document.getElementById(element).value;               
         }
      }
   }                                                                        
   
   json.Action = action;
//alert(JSON.stringify(action));
   snc_clearMenus('incontentdialog');
   dialog = document.getElementById('incontentdialog');
   dialog.className = 'notbordered';
   //hidedialog();
   snc_executeTask(JSON.stringify(json));            
}
function snc_checkMatch(confirmid,confirmvalue) {
   var sourceid = confirmid.replace(/Confirm_/,'');
   var isdiff = document.getElementById(sourceid).value != confirmvalue;
   if (isdiff) {
      alert(sourceid+'s entered do not match');
      document.getElementById(sourceid).value = '';
      document.getElementById(confirmid).value = '';
   }    
}
    
function snc_createLeftMenus() {
   settings = JSON.parse(unescape(readCookie('Settings')));
   var menus = JSON.parse(readCookie('Menus'));      
   var lmenu = document.getElementById('left-menus');
   var i,j,menuname,itemname,actionname,action,menu,div,node,button,ul,li;
   snc_clearMenus('left-menus');
   // left menus
   var lmenus = menus.Menus;
   for (i=0;i<lmenus.length;i++) {
      menu = lmenus[i];
      menuname = menu.MenuName;
      var metallreqs = true;
      var req,set;            
      if (menu.Requirements) {
         if (!settings) metallreqs = false; 
         else {
            for (req in menu.Requirements) {
               set = settings[req];
               metallreqs &= (menu.Requirements[req])?(set!=null):(set==null);               
            }
         }
      } 
      if (metallreqs) {        
         div = document.createElement('DIV');
         div.className = 'left-menu';
         div.id = 'Menu-'+menuname;
         button = document.createElement('BUTTON');
         button.id = 'ShowHide-'+menuname;
         button.className = 'showhide';
         node = document.createElement('IMG');
         node.src = '../images/hide.gif';
         node.alt = 'Hide';
         button.appendChild(node);
         button.onclick = function() {snc_showHideMenu(this.id);};
         div.appendChild(button);
         node = document.createElement('H3');
         node.appendChild(document.createTextNode(menuname));
         div.appendChild(node);
         ul = document.createElement('UL');
         for (j=0;j<menu.MenuItems.length;j++) {
            action = menu.MenuItems[j];
            metallreqs = true;
            if (action.Requirements) {
               if (!settings) metallreqs = false;
               else {
                  var prop,req,set;
                  for (prop in action.Requirements) {
                     set = (settings[prop])?(typeof settings[prop] == 'object')?settings[prop]['Value']:settings[prop]:null;
                     req = action.Requirements[prop];
                     switch (typeof req) {                    
                     case 'boolean': {
                        metallreqs &= (req)?(set!=null):(set==null);
                     }break;
                     case 'number': {
                        metallreqs &= (set >= req);
                     }break;                        
                     }               
                  }
               }
            } 
            if (metallreqs) {        
               li = document.createElement('LI');
               itemname = action.MenuItemName;
               actionname = action.Action
               button = document.createElement('BUTTON');
               button.id = menuname+"-"+actionname;
               button.className = 'snc_menu_button';
               button.onclick = function(){snc_actionDialog(this.id);};                                
               button.appendChild(document.createTextNode(itemname));
               li.appendChild(button);
               ul.appendChild(li);
            }
         }
         div.appendChild(ul);
         lmenu.appendChild(div);
         if (!menu.Open) snc_showHideMenu('ShowHide-'+menuname);
      }            
   }
   snc_createSettingHeaders();
}
function snc_createSettingHeaders() {
   var tmenu = document.getElementById('top-menus');
   var i,j,menuname,itemname,actionname,action,menu,div,node,button,ul,li;
   snc_clearMenus('top-menus');
   var menus = JSON.parse(readCookie('Headers')); 
   settings = JSON.parse(unescape(readCookie('Settings')));

   var msettings = menus.Settings;
   var msetting,removable,message;
   if (settings) {
      for (msetting in msettings) {
         value = settings[msetting];
         if (value) {
            if (msettings[msetting]['Message']) {
               message = msettings[msetting]['Message'];
               removable = msettings[msetting]['Removable'];
               if (typeof value == 'object') message = message + value.Name; 
               else message = message + value;
               div = document.createElement('DIV');
               div.className = 'top-menu';
               div.id = 'Setting-'+msetting;
               if (removable) {
                  button = document.createElement('BUTTON');
                  button.id = 'Remove-'+msetting;
                  button.onclick = function () {snc_removeSetting(this.id);};
                  node = document.createElement('IMG');
                  node.src = '../images/remove.gif';
                  node.alt = 'Remove';
                  node.className = 'snc_menu_remove_setting'; 
                  button.appendChild(node);
                  div.appendChild(button);         
               }
               button = document.createElement('BUTTON');
               button.id = 'top-menu-'+msetting;
               button.className = 'snc_menu_setting';
               button.appendChild(document.createTextNode(message));
               div.appendChild(button);
               tmenu.appendChild(div);
            }
         }             
      }
   }
   completed();
   return true;      
}

function snc_removeSetting(id) {
   var bits = id.split(/\-/);
   var set = bits[1];
   settings = JSON.parse(unescape(readCookie('Settings')));
   settings[set] = null;
   createCookie('Settings',JSON.stringify(settings));       
   snc_createLeftMenus();
}

function snc_showHideMenu(id) {
   var button,menus,menu,img;
   var menus = JSON.parse(readCookie('Menus')); 
   var bits = id.split(/\-/);
   var menuname = bits[1];
   button = document.getElementById(id);
   menu = button.parentNode.lastChild;
   img = button.firstChild;
   var open;
   if (img.alt == 'Hide') {
      img.alt = 'Show';
      img.src = '../images/show.gif';
      menu.style.display = 'none';
      open = false;
   } else {
      img.alt = 'Hide';
      img.src = '../images/hide.gif';
      menu.style.display = 'block';
      open = true;
   }
   var lmenus = menus.Menus;
   for (i=0;i<lmenus.length;i++) {
      menu = lmenus[i];
      if (menuname == menu.MenuName) {
         menu.Open = open; 
      }
   }
   createCookie('Menus',JSON.stringify(menus));
   button.blur();
   return true;   
}
//  onmouseout='UnTip();' onmouseover='Tip("Today");'
function snc_table_addTableDialog() {
   var dialog,div,node,button,input,select,option,element,type,table,thead,tbody,tr,th,td;
   snc_clearMenus('incontentdialog');   
   dialog = document.getElementById('incontentdialog');
   dialog.className = 'bordered';
   div = document.createElement('DIV');
   
   node = document.createElement('H2');
   node.appendChild(document.createTextNode("Add data type"));
   div.appendChild(node);
   
   input = document.createElement('INPUT');
   input.id = 'DMenu';
   input.type = 'hidden';
   input.value = 'Admin';
   div.appendChild(input);
   
   input = document.createElement('INPUT');
   input.id = 'DAction';
   input.type = 'hidden';
   input.value = 'AddTable';
   div.appendChild(input);
   if (action.Message) { 
      node = document.createElement('P');
      node.appendChild(document.createTextNode(action.Message));
      div.appendChild(node);
   }

   table = document.createElement('TABLE');
   tbody = document.createElement('TBODY');
   
   tr = document.createElement('TR');
   th = document.createElement('TH');
   th.appendChild(document.createTextNode('Name'));
   tr.appendChild(th);
   td = document.createElement('TD');
   input = document.createElement('INPUT');
   input.id = element;
   input.type = 'text';
   td.appendChild(input);
   tr.appendChild(td);
   tbody.appendChild(tr);
   
   // Requirements and Linked Data
   /*
   var choice,c;
   tr = document.createElement('TR');
   th = document.createElement('TH');
   th.appendChild(document.createTextNode(element));
   tr.appendChild(th);
   td = document.createElement('TD');
   select = document.createElement('SELECT');
   select.id = element;
   select.className = 'text';
   // choices can either be hardcoded into the menu JSON or 
   // they can be retrieved from the server via the menu item 
   // GetAction task
   if (element.Choices) {
      choice = element.Choices;
      for (c in choice) {
         option = document.createElement('OPTION');
         option.value = choice[c]['Value'];
         option.appendChild(document.createTextNode(choice[c]['Name']));
         select.appendChild(option);                                                
      }
   } else {
      var choices = JSON.parse(readCookie('Choices'));
      if (!choices || !choices[element]) {
         var server = unescape(readCookie('cdm_server'));
         var r = new Object();
         r.target = 'http://'+server+'/snc_sqlitetasks.php';
         var json = new Object();
         var a = new Object();
         // figure out how to set tasks
         a.Task = action.MenuName + '-' + action.GetAction;
         json.Action = a;
         json.Settings = JSON.parse(unescape(readCookie('Settings')));
         var parameters = new Object();
         parameters['json'] = JSON.stringify(json);
         r.request = parameters;
         r.sync = true;
         response = (readCookie('encrypt'))?cdm_decrypt(snc_send(r)):snc_send(r);
         response = JSON.parse(response);
         if (!choices) choices = new Object(); 
         choices[element] = response['Choices']['Partnerships'];
         createCookie('Choices',JSON.stringify(choices));
         completed();                            
      } 
      choice = choices[element];
      for (c in choice) {
         option = document.createElement('OPTION');
         option.value = choice[c]['Value'];
         option.appendChild(document.createTextNode(choice[c]['Name']));
         select.appendChild(option);                                                
      }
   }
   td.appendChild(select);
   tr.appendChild(td);
   tbody.appendChild(tr);
   */
   // 
   
   
   table.appendChild(tbody);
   div.appendChild(table);
   button = document.createElement('BUTTON');
   button.appendChild(document.createTextNode('Send'));
   button.onclick = function() {snc_performDialogAction();};
   div.appendChild(button);
   button = document.createElement('BUTTON');
   button.appendChild(document.createTextNode('Cancel'));
   button.onclick = function() {
      snc_clearMenus('incontentdialog');
      var dialog = document.getElementById('incontentdialog');
      dialog.className = 'notbordered';
   };
   div.appendChild(button);
   dialog.appendChild(div);                  
}
function snc_table_addColumn(element) {
   if (element.value != 'None') {
      var tr = element.parentNode.parentNode;
      var row = parseInt(tr.id);
      var tbody = tr.parentNode;
      if (row == tbody.childNodes.length) {
         var newtr = tr.cloneNode(true);
         newtr.id = row+1;
         for (nodenum in newtr.childNodes) {
            var node = newtr.childNodes[nodenum];
            if (node.childNodes && node.firstChild && node.nodeName == 'TD') {
               var regex = new RegExp(row+'');
               var newid = node.firstChild.id.substring(0,node.firstChild.id.length-1)+newtr.id; 
               node.firstChild.id = newid;
               node.firstChild.name = newid;
               if (node.firstChild.type == 'text') node.firstChild.value = '';
            }
         }
         tbody.appendChild(newtr);
      } 
   }
   return true;
}
function snc_report_json_obj(json) {
/*
   debugging code to print the returned json readably formatted to the screen  
   
   for some reason the last close bracket indent doesn't get removed.
*/
   var dialog,i,j,indent,tabs,extratab;
   snc_clearMenus('incontentdialog');   
   dialog = document.getElementById('incontentdialog');
   var json_obj = JSON.parse(json);
   var pre = document.createElement('PRE');
   var options = JSON.stringify(json_obj.Options);
   indent = 0;
   tabs = '';
   for (i=0;i<options.length;i++) {
      switch(options.substr(i,1)) {
      case ',': {
         tabs = '\n';
         for(j=0;j<indent;j++) tabs = tabs + '\t';          
         options = options.substr(0,i+1) + tabs + options.substr(i+1);
         i += tabs.length;      
      }break;
      case '{': {
         indent++;
         tabs = '\n';
         for(j=0;j<indent;j++) tabs = tabs + '\t';          
         options = options.substr(0,i+1) + tabs + options.substr(i+1);
         i += tabs.length;         
      }break;
      case '[': {
         indent++;
         tabs = '\n';
         for(j=0;j<indent;j++) tabs = tabs + '\t';          
         options = options.substr(0,i+1) + tabs + options.substr(i+1);
         i += tabs.length;
      }break;
      case '}': {
         if (options.substr(i+1,1) == ',') {
            indent--;
            tabs = '\n';
            for(j=0;j<indent;j++) tabs = tabs + '\t';          
            options = options.substr(0,i) + tabs + options.substr(i,2) + tabs + options.substr(i+2);
            i += tabs.length * 2;
         } else {
            indent-=2;
            tabs = '\n';
            extratab = (indent >= 0)?'\t':''; 
            for(j=0;j<indent;j++) tabs = tabs + '\t';
            options = options.substr(0,i) + tabs + extratab + options.substr(i,1) + tabs + options.substr(i+1);
            i += (tabs.length + 1) * 2; // extra 2 for the extra \t tab
         }      
      }break;
      case ']': {
         if (options.substr(i+1,1) == ',') {
            indent--;
            tabs = '\n';
            for(j=0;j<indent;j++) tabs = tabs + '\t';          
            options = options.substr(0,i) + tabs + options.substr(i,2) + tabs + options.substr(i+2);
            i += tabs.length * 2;
         } else {
            indent-=2;
            tabs = '\n';
            extratab = (indent >= 0)?'\t':''; 
            for(j=0;j<indent;j++) tabs = tabs + '\t';
            options = options.substr(0,i) + tabs + extratab + options.substr(i,1) + tabs + options.substr(i+1);
            i += (tabs.length + 1) * 2; // extra 2 for the extra \t tab
         }
      }break;
      }      
   }   
   pre.appendChild(document.createTextNode(options));
   dialog.appendChild(pre);      
   return true;
}                                                                                                        