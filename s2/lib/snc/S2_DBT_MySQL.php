<?php                               
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('cdm/cdm_access_functions.php');
include_once('users/users.inc');
include_once('gen/G_Cache.php');
include_once('gen/G_Reserve_Number.php');
include_once('klib.php');

/*
   echo $query->__toString();
   
   Derived Data - uses a javascript function to update a column 
   
   "Derives":[{"Column":"colname","Method":jsmethod,"Params":[{"Type":"Value"},{"Type":"Static","Value":"val"}]]

*/                           

class DBT extends DBT_Gen {
   function DBT() {
      parent::DBT_Gen();
   }
   protected $datatypes = array(
      'Unique Key'         => 'INT UNSIGNED NOT NULL AUTO_INCREMENT',                                    
      'UUID'               => 'VARCHAR(36)',                                    
      'Linked Key'         => 'INT UNSIGNED',
      'Interface'          => 'INT UNSIGNED',
      'Short Text'         => 'VARCHAR(100)',
      'Password'           => 'VARCHAR(100)',
      'Medium Text'        => 'VARCHAR(200)',
      'Long Text'          => 'TEXT',
      'Encrypted Text'     => 'TEXT',
      'Number'             => 'INTEGER',
      'Percentage'         => 'FLOAT',
      'Count'              => 'INTEGER',
      'Decimal'            => 'FLOAT',
      'Currency'           => 'FLOAT',
      'Version'            => 'FLOAT',
      'True or False'      => 'BOOLEAN',
      'Date'               => 'DATE',
      'Date From'          => 'DATE',
      'Date Until'         => 'DATE',
      'Time'               => 'TIME',
      'Date and Time'      => 'TIMESTAMP',
      'Map Data'           => 'GEOMETRY',
      'Taxa'               => 'INT UNSIGNED',
      'URL'                => 'VARCHAR(500)',
      'Email'              => 'VARCHAR(100)',
      'File Path'          => 'VARCHAR(500)',
      'File'               => 'LONGBLOB',
      'Taxon Version Key'  => 'VARCHAR(16)',
      'TVK'                => 'VARCHAR(16)' 
   ); 
   function isduplicate() {
      $dbh = $this->dbhandle;                       
      $query = $dbh->select();
      $query->from($this->tablename);
      // CreatedBy and CreatedOn have to be excluded from duplicate testing.
      // Automatically populated data has to be excluded as well. 
      $excludedcolumns = array('CreatedBy','CreatedOn', 'Count', 'Version', $this->getpk());
      foreach($this->columns as $col => $def) {
         $val = (isset($this->data[$col]))?$this->data[$col]:null;
         if (preg_match('/^\{/',$def)) {
            $params = json_decode($def);
            $dtype = $params->DataType;
         } else $dtype = preg_replace('/\:.*/','',$def);
         if (!in_array($col,$excludedcolumns) && isset($val)) {
            switch ($dtype) {
            case 'Map Data': {
               $val = new Zend_Db_Expr("GeomFromText(\"$val\")");
               $query->where("$col = ?",$val);   
            }break;
            case 'LINKEDTO': {
               if (is_object($val)) $val = $val->Id;
               $query->where("$col = ?",$val);
            }break;
            case 'INTERFACE': {
               if (isset($this->data[$col]) && is_object($this->data[$col])) {
                  $val = $this->data[$col]->G_Cache_ID;
                  $query->where("$col = ?",$val);
               }
            }break;
            case 'Taxa': {
               if (isset($this->data[$col]) && is_object($this->data[$col])) {
                  $val = $this->data[$col]->G_Cache_ID;
                  $query->where("$col = ?",$val);
               } 
            }break;
            default: {
               $query->where("$col = ?",$val);
            }break;
            } 
         }
      }
//echo $query->__toString();exit;
      $result = $dbh->fetchRow($query);
      return (is_array($result))?$result[$this->getpk()]:false;      
   }
   function getversions($val=null) {
      $versions = array();
      foreach ($this->columns as $col => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $classname = (isset($params->VersionOfType))?$params->VersionOfType:null;
            $thiscol = (isset($params->VersionOfField))?$params->VersionOfField:null;
         } else list($type,$classname,$thiscol) = preg_split('/\:/',$type);
         if ($type == 'Version') {
            $link = null;
            $link = new $classname();
            if ($link->exists() && $val) {
               $versions['Column'] = $col;
               $link->loaddata($val,null,false);
               $column = $this->displayname;
               $column = preg_replace('/\s+/','_',$column);
               $cversion = $link->selectcolumnval($column);
               $cversion = (!$cversion>0)?1:$cversion;
               $versions['Current'] = floor($cversion*10)/10;
               
               if ($this->exists()) {
                  $query = $this->dbhandle->select();
                  $query->from($this->tablename, array('MaxVer' => new Zend_Db_Expr("Max($col)")));
                  $query->where("$thiscol = ?",$val);
                  $result = $this->dbhandle->fetchRow($query);
                  if (isset($result['MaxVer'])) //$versions['Maximum'] = floor($result['MaxVer']*10)/10;
                     $versions['Maximum'] = floor(round($result['MaxVer'],1)*10)/10;
               }
            }               
         } 
      }
      return $versions;
   }
   function getcount($val) {
      $count = 0;
      foreach ($this->columns as $col => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $classname = (isset($params->CountOfType))?$params->CountOfType:null;
            $thiscol = (isset($params->CountOfField))?$params->CountOfField:null;
         } else list($type,$classname,$thiscol) = preg_split('/\:/',$type);
         if ($type == 'Count') {
            if ($val && $thiscol) {
               $query = $this->dbhandle->select();
               $query->from($this->tablename, array('MaxCount' => new Zend_Db_Expr("Max($col)")));
               $query->where("$thiscol = ?",$val);
               $result = $this->dbhandle->fetchRow($query);
               if (isset($result['MaxCount'])) $count = $result['MaxCount'];
            }               
         } 
      }
      return $count;        
   } 
   function updateversion($id,$versionof,$version) {
      $pk = $this->getpk();
      $link = null;
      $link = new $versionof();
      $ucol = $link->getdisplayname();
      $ucol = preg_replace('/\s+/','_',$ucol);
      $data = array($ucol => $version);
      $where = $this->dbhandle->quoteInto("$pk = ?",$id);
      $status = $this->dbhandle->update($this->tablename,$data,$where);
      return $status;
   }
   function selectcolumnval($col) {
      $value = null;
      if (isset($this->data[$col])) {
         $value = $this->data[$col];
      } else
      if($this->exists()) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
         } else {
            $type = $this->columns[$col];
            $type = preg_replace('/\:.+/','',$type);
         }
         $query = $this->dbhandle->select();
         $query->from($this->tablename, array($col));
         $pk = $this->getpk();
         $id = $this->getid();
         $query->where("$pk = ?",$id);
         $result = $this->dbhandle->fetchRow($query);
         $value = (isset($result[$col]))?$result[$col]:null;
         switch ($type) {
         case 'LINKEDTO': {
            if (isset($params)) {
               if (!is_object($value)) {
                  $type = $params->DataType;
                  $table = $params->TargetType;
                  $column = $params->TargetField;
                  $link = null;
                  $link = new $table();
                  $link->loaddata($value,$column);
                  $obj = new stdClass();
                  $obj->Name = $link->getname();
                  $obj->Id = $link->getid();
                  $value = $obj;
               }
            }   
         }break;
         case 'LINKEDTOM': {
            if (isset($params)) {
               if (!is_object($value)) {
                  $type = $params->DataType;
                  $table = $params->TargetType;
                  $column = $params->TargetField;
                  $pk = $this->getpk();
                  $id = $this->getid();
                  $p = (isset($params->Property))?$params->Property:$col;
                  $ltm = new DBT_Lookup($p,$this->tablename,$pk,$table,$column);
                  $obj = $ltm->selecttargets($value,$pk);
                  $value = $obj;
               }
            }   
         }break;
         default: {
            $value = $this->postprocessterm($col,$type,$value);
         }break;
         } 
      }
      return $value; 
   }
   function selectvalwhere($col,$where) {
      $query = $this->dbhandle->select();
      $query->from($this->tablename, array($col));
      foreach($where as $col => $val) $query->where("$col = ?",$val);
      $result = $this->dbhandle->fetchRow($query);
      $value = (isset($result[$col]))?$result[$col]:null;
      return $value;      
   }
   function loadderiveddata($crumbs,$icol=null,$ival=null) {
//print "loading data for ".$this->gettablename()."<br/>";
      $int = $this->getinterface();
      $query = $this->dbhandle->select();
      $query->from($this->tablename);
      $pk = $this->getpk();
      $crumbs = (array)$crumbs;
      foreach ($int as $col => $type) {
         if (is_object($type) && $type->DataType == 'LINKEDTO') {
            foreach($crumbs as $ind => $crumb) {
               if ($crumb->IType == $type->TargetType) {
                  //print "<pre>";print_r($type);print "</pre>";
                  $it = $crumb->IType;
                  switch ($crumb->Type) {
                  case 'SameForAll': {
                     $query->where("$col = ?", $crumb->Value);
                  }break;
                  case 'Link': {
                     $link = new $it();
                     $lid = $link->loadderiveddata($crumbs,$crumb->TColumn,$crumb->Value); 
                     $query->where("$col = ?",$lid);
                  }break;
                  } 
               }
            }
         }   
      }
      if (isset($icol) && isset($ival)) $query->where("$icol = ?",$ival);
//echo $query->__toString();
      $this->data = $this->dbhandle->fetchRow($query);
      return $this->getid();          
   }
   function exists() {
      $statement = "show tables like '$this->tablename'";
      $status = $this->dbhandle->fetchAll($statement);
      return count($status);
   }
   function drop() {
      $statement = "drop table $this->tablename";
      $status = $this->dbhandle->exec($statement);
      return $status;
   }
   function create() {
      global $pending;
      
      if (!isset($pending[$this->tablename]) && isset($this->tablename) && (count($this->columns)>0)) {
         $pending[$this->tablename] = true;
      
         $statement = "CREATE TABLE IF NOT EXISTS $this->tablename (";
         $pk = null;
         foreach($this->columns as $name => $type) {
            /* force these back to blank so previous values are not used. */
            $params = null;
            $bits = null;
            if (preg_match('/^\{/',$type)) {
               $params = json_decode($type);
               $type = $params->DataType;
            } else {
               $bits = preg_split('/\:/',$type);
               $type = $bits[0];         
            }
            $suppress = false;
            if ($type == 'Unique Key') $pk = $name;
            $mand = false;
            if ($type == 'LINKEDTO') {
               if (isset($params)) {
                  $table = $params->TargetType;
                  $column = $params->TargetField;
               } else {
                  preg_match('/([^\.]+)\.(.+)/',$bits[1],$matches);
                  $table = $matches[1];
                  $column = $matches[2];
               }
               if ($table) {
                  $dbt = new $table();
                  $tcols = $dbt->getcolumns();
                  $type = $tcols[$column];
                  if ($type == 'Unique Key') $type = 'Linked Key';
               }
            }
            if ($type == 'LINKEDTOM') {
               if (isset($params)) {
                  $table = $params->TargetType;
                  $column = $params->TargetField;
                  $property = (isset($params->Property))?$params->Property:$name;
               } else {
                  preg_match('/([^\.]+)\.(.+)/',$bits[1],$matches);
                  $table = $matches[1];
                  $column = $matches[2];
                  $property = $name;
               }
               $suppress = true;
               $dbt = new $table();
               if (!$dbt->exists()) $dbt->create();
               $tcols = $dbt->getcolumns();
               $ttype = $tcols[$column];
               if (preg_match('/^\{/',$ttype)) {
                  $params = json_decode($ttype);
                  $ttype = $params->DataType;
               } else {
                  $bits = preg_split('/\:/',$ttype);
                  $ttype = $bits[0];         
               }
               $lt = new DBT_Lookup($property,$this->tablename,$pk,$table,$column);
               $lt->create();
            }
            if ($type == 'INTERFACE') {
               $type = 'Interface';            
            } 
            $def = $this->datatypes[$type];
            if ((isset($params) && (isset($params->Mandatory)&&$params->Mandatory==1))||($bits[count($bits)-1] == 'Mandatory')) $def .= " NOT NULL";
            if (!$suppress) $statement .= "$name $def,";  
         }
         
         /* 
            have to add Version fields before issuing create table statement 
            but have to create link tables after creating the source table. 
         */
         foreach($this->show as $classname) {
            if (preg_match('/([^:]+)/',$classname,$m)) {
               $classname = $m[1];
               $link = new $classname();
               if (!$link->exists()) $link->create();            
               $cols = $link->getinterface();
               foreach ($cols as $col => $type) {
                  if (is_object($type)) { 
                     $params = $type;
                     $type = $params->DataType;
                  } else if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $type = $params->DataType;
                  } else $type = preg_replace('/\:.+$/','',$type);
                  switch($type) {
                  case 'LINKEDTO': {
                     $lc = $params->TargetType;
                     $l2 = new $lc;
                     if (!$l2->exists()) $l2->create(); 
                  }break;      
                  case 'Version': {
                     $column = $link->getdisplayname();
                     $column = preg_replace('/\s+/','_',$column);
                     $dtype = $this->datatypes['Version'];
                     $statement .= "$column $dtype,";
                  }break;
                  }
               }
            }            
         }
         
         $statement .= "CreatedBy INT UNSIGNED,";
         $statement .= "CreatedOn TIMESTAMP";
         if (isset($pk)) $statement .= ", PRIMARY KEY($pk));";
         else $statement = ");";
         $status = $this->dbhandle->exec($statement);
                  
         if ($this->gs_publishme) $this->gs_publish();
         if ($this->primingdata) $this->prime();
         if ($this->deriveddata) {
            foreach($this->deriveddata as $view) {
               $link = new $view();
               if (!$link->exists()) $link->create();
            }
         }
         unset($pending[$this->tablename]);
      }                                    
      return $status; 
   }
   function prime() {
      if ($this->primingdata) {
         $this->validated = true;
         foreach ($this->primingdata as $classname => $data) {
            $data = trim($data);
            $data = preg_split('/\n/',$data);
            foreach($data as $line => $text) {
               if ($line == 0) $cols = $this->parselinecsv($text);
               else {
                  $idata = array();
                  $vals = $this->parselinecsv($text); 
                  foreach($cols as $colnum => $col) {
                     $idata[$col] = $vals[$colnum]; 
                  }
                  if ($classname == $this->tablename) {
                     $this->setdata($idata);
                     $this->insert();                 
                  } else {
                     $link = null;
                     $link = new $classname();
                     if (isset($link)) {
                        if (!$link->exists()) $link->create();
                        $link->validated = true;
                        $link->setdata($idata);
                        $link->insert();
                     }
                  }
               }               
            }                              
         }
      }
      $this->data = array();
   }
   function initialise() {
      if ($this->defaultpermissions) {
         $this->validated = true;
         foreach ($this->defaultpermissions as $classname => $data) {
            $data = trim($data);
            $data = preg_split('/\n/',$data);
            foreach($data as $line => $text) {
               if ($line == 0) $cols = $this->parselinecsv($text);
               else {
                  $idata = array();
                  $vals = $this->parselinecsv($text); 
                  foreach($cols as $colnum => $col) {
                     $idata[$col] = $vals[$colnum]; 
                  }
                  if ($classname == $this->tablename) {
                     $this->setdata($idata);
                     if (!$this->isduplicate()) $this->insert();                 
                  } else {
                     $link = null;
                     $link = new $classname();
                     if (isset($link)) {
                        if (!$link->exists()) $link->create();
                        $link->validated = true;
                        $link->setdata($idata);
                        if (!$link->isduplicate()) $link->insert();
                     }
                  }
               }               
            }                              
         }
      }
      $this->data = array();
   }
   function insertterm($col,$type,$val) {
      switch($type) {
      case 'Map Data': {
         $val = new Zend_Db_Expr("ST_GeomFromText('$val')");   
      }break;
      case 'UUID': {
         if (!isst($val)) $val = gen_uuid();
      }break;
      default: {
         if (isset($val) && is_object($val)) $val = json_encode($val);
      }break;
      }
      return $val;
   }
   function selectterm($col,$type) {
      $val = null;
      $lcol = strtolower($col);
      switch ($type) {
      case 'Map Data': {
         $val = new Zend_Db_Expr("ST_AsText($lcol)");
      }break;
      default: {
         $val = $lcol;   
      }break;
      }
      return $val;
   }
   function insert() {
//print "<pre>"; print_r($this->data); print "</pre>"; exit;
      $updates = array();
      $ltmd = array();
      if (!$this->validated) $this->validate();
      $data = array();
      $multiples = array();
      $total = 1;
      $counts = array();
      foreach($this->columns as $col => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
         } else $type = preg_replace('/\:.+/','',$type);
         if (isset($this->data[$col]) && $this->data[$col] == 'null') unset($this->data[$col]);
         switch ($type) {
         case 'Unique Key': {
            unset($this->data[$col]);
         }break;
         case 'Map Data': {
            if (isset($this->data[$col]) && is_string($this->data[$col])) {
               $wkt = $this->data[$col];
               $this->data[$col] = new Zend_Db_Expr("GeomFromText(\"$wkt\")");
            }
         }break;
         case 'INTERFACE': {
            if (isset($this->data[$col]) && is_object($this->data[$col])) {
               $obj = $this->data[$col];
               $cache = array();
               $cache['Data_Type'] = $obj->Data_Type;
               $cache['Name'] = $obj->Name;
               $cache['Value_Type'] = (is_integer($obj->Value))?'Number':'Short Text';
               $cache['Value'] = $obj->Value;
               $cache['Domain'] = $obj->In_Domain; 
               if (isset($obj->JSON)) {
                  if (is_object($obj->JSON)) $json = json_encode($obj->JSON);
                  else $json = $obj->JSON;
               } else $json = json_encode($obj); 
               $cache['JSON'] = $json; 
               $c = new G_Cache();
               $c->setdata($cache);
               $interfaceid = $c->insert();
               $this->data[$col] = $interfaceid;
            }
         }break;
         case 'Taxa': {
            if (isset($this->data[$col]) && is_object($this->data[$col])) {
               $obj = $this->data[$col];
               $cache = array();
               $cache['Data_Type'] = $obj->Data_Type;
               $cache['Name'] = $obj->Name;
               $cache['Value_Type'] = (is_integer($obj->Value))?'Number':'Short Text';
               $cache['Value'] = $obj->Value;
               $cache['Domain'] = $obj->In_Domain; 
               if (isset($obj->JSON)) {
                  if (is_object($obj->JSON)) $json = json_encode($obj->JSON);
                  else $json = $obj->JSON;
               } else $json = json_encode($obj); 
               $cache['JSON'] = $json; 
               $c = new G_Cache();
               $c->setdata($cache);
               $interfaceid = $c->insert();
               $this->data[$col] = $interfaceid;
            }
         }break;
         case 'LINKEDTO': {
            if (isset($this->data[$col])) {
               if (is_object($this->data[$col])) {
                  $this->data[$col] = $this->data[$col]->Id;
               } else if (!is_array($this->data[$col]) && preg_match('/^\[.+\]$/',$this->data[$col])) {
                  $this->data[$col] = json_decode($this->data[$col]);
                  if(count($this->data[$col])>0) {
                     $multiples[] = $col;
                     $total *= count($this->data[$col]);
                  }
               } else if (is_array($this->data[$col]) && count($this->data[$col])>0) {
                  $multiples[] = $col;
                  $total *= count($this->data[$col]);
               } 
            }
         }break;
         case 'LINKEDTOM': {
         // Have to insert data in order to get the primary key before 
         // inserting the linked to data
            $ltmd[$col] = $this->data[$col];
            unset($this->data[$col]); 
         }break;
         case 'Password': {
            if (!$this->validated) {
               $k = new KLib($this->getsid());
               $keys = json_decode($k->getkeys());
               $this->data[$col] = decrypt($this->data[$col],$keys->k1,$keys->k2);
            } 
            $this->data[$col] = $this->dbencrypt($this->data[$col]);
         }break;
         case 'Encrypted Text': {
            if (!$this->validated) {
               $k = new KLib($this->getsid());
               $keys = json_decode($k->getkeys());
               $this->data[$col] = decrypt($this->data[$col],$keys->k1,$keys->k2);
            }
            $this->data[$col] = $this->dbencrypt($this->data[$col]);
         }break;
         case 'Version': {
            /* 
               If there is a version column you need to check whether there is
               a value already and if there isn't then you need to update the 
               parent VersionOf table with the newly inserted version.
            */
            if (isset($params->VersionOfType) && ($this->data[$col] == 1)) {
               $type = $params->VersionOfType;
               $field = $params->VersionOfField;
               $link = null; 
               $link = new $type();
               $link->updateversion($this->data[$field],$this->tablename,1);
            }
         }break;
         case 'UUID': {
            if(!isset($this->data[$col])) $this->data[$col] = gen_uuid();
         }break;
         }         
      }
//print "<pre>"; print_r($ltmd); print "</pre>"; exit;
      if ($total>1) {
         $inserts = array();
         $data = $this->data;
         for($i=0;$i<$total;$i++) {
            $tdata = $data;
            $divor = 1;
            foreach($multiples as $prop) {
               $moder = count($data[$prop]); 
               $ind = intval($i/$divor)%$moder;
               $tdata[$prop] = $data[$prop][$ind];
               $divor *= $moder;
            }
            $inserts[] = $tdata;
         }
         foreach($inserts as $data) {
            $this->setdata($data);
            $this->dbhandle->insert($this->tablename,$this->data);
            $pk = $this->getpk();
            $id = $this->dbhandle->lastInsertId();
//print "<pre>"; print_r($ltmd); print "</pre>"; exit;
            if ($id) {
               $this->data[$pk] = $id;
               $this->id = $id;
               $status = $id;
               foreach($ltmd as $col => $val) {
                  $type = $this->columns[$col];
                  if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $type = $params->DataType;
                  } else $type = preg_replace('/\:.+/','',$type);
                  $pk = $this->getpk();
                  if (isset($params)) {
                     $ttab = $params->TargetType;
                     $tcol = $params->TargetField;
                     $p = (isset($params->Property))?$params->Property:$col;
                  } else {
                     preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                     $ttab = $matches[2];
                     $tcol = $matches[3];
                     $p = $col;
                  }
                  $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                  if ($pk == $tcol) {
                     $ltm->delete($pk . 1,$id);
                     $ltm->delete($pk . 2,$id);
                  } else $ltm->delete($pk,$id);
                  $vals = array();
                  if (preg_match('/^\[[^\]]*\]$/',$val)) {$vals = json_decode(stripslashes($val));} 
                  else if (is_array($val)) {$vals = $val;} 
                  else {$vals[] = $val;}
//print $val; print_r($vals); exit;               
                  
                  foreach($vals as $v) {
                     if (preg_match('/^\d+$/',$v)) $v = intval($v);
                     if ($pk == $tcol) $data = array($pk . 1 => $id, $tcol . 2 => $v);
                     else $data = array($pk   => $id, $tcol => $v);
                     $ltm->setdata($data);
                     $ltm->insert();
                  }
               }
            }
            $message = ($status==0)?'Insert data failed':'Data inserted successfully';
            $obj = $this->getview($settings);
            $obj->Status = $status;
            $obj->Message = $message;
         }
      } else {
//print "<pre>"; print_r($this->data); print "</pre>"; exit;
         $this->dbhandle->insert($this->tablename,$this->data);
         $pk = $this->getpk();
         $id = $this->dbhandle->lastInsertId();
         if ($id) {
            $this->data[$pk] = $id;
            $this->id = $id;
            $status = $id;
//print "<pre>"; print_r($ltmd); print "</pre>"; exit;
            foreach($ltmd as $col => $val) {
               $type = $this->columns[$col];
               if (preg_match('/^\{/',$type)) {
                  $params = json_decode($type);
                  $type = $params->DataType;
               } else $type = preg_replace('/\:.+/','',$type);
               $pk = $this->getpk();
               if (isset($params)) {
                  $ttab = $params->TargetType;
                  $tcol = $params->TargetField;
                  $p = (isset($params->Property))?$params->Property:$col;
               } else {
                  preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                  $ttab = $matches[2];
                  $tcol = $matches[3];
                  $p = $col;
               }
               $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
               if ($pk == $tcol) {
                  $ltm->delete($pk . 1,$id);
                  $ltm->delete($pk . 2,$id);
               } else $ltm->delete($pk,$id);
               $vals = array();
               if (preg_match('/^\[[^\]]*\]$/',$val)) {$vals = json_decode(stripslashes($val));} 
               else if (is_array($val)) {$vals = $val;} 
               else {$vals[] = $val;}
//print $val; print_r($vals); exit;               
               foreach($vals as $v) {
                  if (preg_match('/^\d+$/',$v)) $v = intval($v);
                  if ($id != null && $v != null) { 
                     if ($pk == $tcol) $data = array($pk . 1 => $id, $tcol . 2 => $v);
                     else $data = array($pk   => $id, $tcol => $v);
                     $ltm->setdata($data);
                     $ltm->insert();
                  }
               }
            }
         }
      }
      return $status;   
   }
   function delete($col=null,$val=null) {    
      if (!$this->exists()) $this->create();
      if (isset($val)&&isset($col)) {
         $clause = $this->dbhandle->quoteInto("$col = ?",$val);
      } else {
         $pk = $this->getpk();
         $clause = $this->dbhandle->quoteInto("$pk = ?",$this->data[$pk]);
      }
      // if $col is set it has to exist as a column of the table.
      if (!isset($col)|| isset($this->columns[$col])) {
         $this->cascadedelete();
         $status = $this->dbhandle->delete($this->tablename,$clause);
      }
      return $status;
   }
   function deleteAll() {
      $status = $this->dbhandle->delete($this->tablename);
      return $status;
   }
   function update($col,$val) {
      try {
         if (!$this->validated) $this->validate();
         if (isset($this->data['CreatedBy'])) unset($this->data['CreatedBy']);
         if (isset($this->data['CreatedOn'])) unset($this->data['CreatedOn']);
         foreach($this->columns as $dcol => $type) {
            if (preg_match('/^\{/',$type)) {
               $params = json_decode($type);
               $type = $params->DataType;         
            } else $type = preg_replace('/\:.+/','',$type);
            if ($params->Hidden) {
               unset($this->data[$dcol]);
            } else {
               switch ($type) {
               case 'Unique Key': {
                  unset($this->data[$dcol]);
               }break;
               case 'Map Data': {
                  if (isset($this->data[$dcol]) && is_string($this->data[$dcol])) {
                     $wkt = $this->data[$dcol];
                     $this->data[$dcol] = new Zend_Db_Expr("GeomFromText(\"$wkt\")");
                  }
               }break;
               case 'INTERFACE': {
                  if (isset($this->data[$dcol]) && is_object($this->data[$dcol])) {
                     $obj = $this->data[$dcol];
                     $cache = array();
                     $cache['Data_Type'] = $obj->Data_Type;
                     $cache['Name'] = $obj->Name;
                     $cache['Value_Type'] = (is_integer($obj->Value))?'Number':'Short Text';
                     $cache['Value'] = $obj->Value;
                     $cache['Domain'] = $obj->In_Domain; 
                     if (isset($obj->JSON)) {
                        if (is_object($obj->JSON)) $json = json_encode($obj->JSON);
                        else $json = $obj->JSON;
                     } else $json = json_encode($obj); 
                     $cache['JSON'] = $json; 
                     $c = new G_Cache();
                     $c->setdata($cache);
                     $interfaceid = $c->insert();
                     $this->data[$dcol] = $interfaceid;
                  } 
               }break;
               case 'Taxa': {
                  if (isset($this->data[$dcol]) && is_object($this->data[$dcol])) {
                     $obj = $this->data[$dcol];
                     $cache = array();
                     $cache['Data_Type'] = $obj->Data_Type;
                     $cache['Name'] = $obj->Name;
                     $cache['Value_Type'] = (is_integer($obj->Value))?'Number':'Short Text';
                     $cache['Value'] = $obj->Value;
                     $cache['Domain'] = $obj->In_Domain; 
                     if (isset($obj->JSON)) {
                        if (is_object($obj->JSON)) $json = json_encode($obj->JSON);
                        else $json = $obj->JSON;
                     } else $json = json_encode($obj); 
                     $cache['JSON'] = $json; 
                     $c = new G_Cache();
                     $c->setdata($cache);
                     $interfaceid = $c->insert();
                     $this->data[$dcol] = $interfaceid;
                  }
               }break;
               case 'LINKEDTOM': {
                  $lookuptable = $this->tablename."_${dcol}";
                  $pk = $this->getpk();
                  if (isset($params)) {
                     $ttab = $params->TargetType;
                     $tcol = $params->TargetField;
                     $p = (isset($params->Property))?$params->Property:$dcol;
                  } else {
                     preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$dcol],$matches);
                     $ttab = $matches[2];
                     $tcol = $matches[3];
                     $p = $dcol;
                  }
                  $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                  if ($pk == $tcol) {
                     $ltm->delete($pk . 1,$val);
                     $ltm->delete($pk . 2,$val);
                  } else $ltm->delete($pk,$val);
                  $data = array(
                     $pk   => $val,
                     $tcol => $this->data[$dcol]
                  );
                  if (is_array($data[$tcol])) {
                     $ivals = $data[$tcol];
                     foreach ($ivals as $ival) {
                        $idata = $data;
                        $idata[$tcol] = $ival;
                        if ($val != null && $ival != null) {
                           $ltm->setdata($idata);
                           $ltm->insert();
                        }
                     }
                  } else if (preg_match('/\[/',$data[$tcol])) {
                     $ivals = json_decode(stripslashes($data[$tcol]));
                     foreach ($ivals as $ival) {
                        $idata = $data;
                        $ival = (is_object($ival))?$ival->Id:$ival;
                        if (preg_match('/^\d+$/',$ival)) $ival = intval($ival);
                        if ($pk == $tcol) $idata = array($pk . 1 => $val, $tcol . 2 => $ival);
                        else $idata = array($pk   => $val, $tcol => $ival);
                        //$idata[$tcol] = $ival;
                        if ($val != null && $ival != null) {
                           $ltm->setdata($idata);
                           $ltm->insert();
                        }
                     }
                  } else {
                     $ival = $this->data[$dcol]; 
                     if ($val != null && $ival != null) {
                        if ($pk == $tcol) $data = array($pk . 1 => $val, $tcol . 2 => $ival);
                        else $data = array($pk   => $val, $tcol => $ival);
                        $ltm->setdata($data);
                        $ltm->insert();
                     }
                  }
                  unset($this->data[$dcol]); 
               }break;
               case 'Password': {
                  if (!$this->validated) {
                     $k = new KLib($this->getsid());
                     $keys = json_decode($k->getkeys());
                     $this->data[$dcol] = decrypt($this->data[$dcol],$keys->k1,$keys->k2);
                  }
                  $this->data[$dcol] = $this->dbencrypt($this->data[$dcol]);
               }break;
               case 'Encrypted Text': {
                  if (!$this->validated) {
                     $k = new KLib($this->getsid());
                     $keys = json_decode($k->getkeys());
                     $this->data[$dcol] = decrypt($this->data[$dcol],$keys->k1,$keys->k2);
                  }
                  $this->data[$dcol] = $this->dbencrypt($this->data[$dcol]);
               }break;
               case 'UUID': {
                  if(!isset($this->data[$dcol])) $this->data[$dcol] = gen_uuid();
               }break;
               }
            }
         }
         $where = $this->dbhandle->quoteInto("$col = ?",$val);
         $status = $this->dbhandle->update($this->tablename,$this->data,$where);      
         $this->data[$col] = $val;
         $return = 1;
      } catch (Exception $e) {
         $return = 0;
      }
      return $return;
   }
   function loaddata($id,$col=null,$innerdata=true) {
      $ismatched = false;
      if (!isset($col)) $col = $this->getpk();
      // NEED TO MAKE THIS WORK FOR {NAME,ID} AND FOR GENUINE OBJ SEARCH
      if (is_object($id)) $id = json_encode($id);
      if (isset($id) && $this->exists()) {
         $pk = $this->getpk();
         if (!$col) $col = $pk; 
         if (is_object($id)) $id = $id->Id;
         $query = $this->dbhandle->select();
         $cols = array();
         $ltmd = array();
         foreach ($this->columns as $dcol => $type) {
            if (preg_match('/^\{/',$type)) {
               $params = json_decode($type);
               $type = $params->DataType;         
            } else $type = preg_replace('/\:.+/','',$type);
            if (!isset($params->Hidden) || !$params->Hidden) {
               switch ($type) {
               case 'Map Data': {
                  $cols[$dcol] = new Zend_Db_Expr("AsText($dcol)");               
               } break;
               case 'LINKEDTOM': {
                  if ($innerdata) {
                     if (isset($params)) {
                        $ttab = $params->TargetType;
                        $tcol = $params->TargetField;
                        $p = (isset($params->Property))?$params->Property:$dcol;
                     } else {
                        preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$dcol],$matches);
                        $ttab = $matches[2];
                        $tcol = $matches[3];
                        $p = $dcol;
                     }
                     $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                     $obj = new stdClass();
                     $obj->IType = $ttab;
                     $obj->Current = 'Multiple';
                     $obj->Key = $tcol;
                     $obj->ListData = $ltm->selecttargets($pk,$id);
                     $ltmd[$dcol] = $obj;
                  }
               }break;
               default: {
                  $cols[$dcol] = $dcol; 
               } break;
               }
            }   
         }
         $query->from($this->tablename, $cols);
         $query->where("$col = ?",$id);
//echo "<pre>" . $query->__toString() . "</pre>";         
         $this->data = $this->dbhandle->fetchRow($query);

         if (count($ltmd)) {
            foreach($ltmd as $dcol => $dval) {
               $this->data[$dcol] = $dval;
            }
         }
         $ismatched = (is_array($this->data) && $this->getid()>0);
         if ($ismatched) {
            foreach($this->data as $col => $val) {
               $type = $this->columns[$col];
               if (preg_match('/^\{/',$type)) {
                  $params = json_decode($type);
                  $type = $params->DataType;         
               } else $type = preg_replace('/\:.+/','',$type);
               $this->data[$col] = $this->postprocessterm($col,$type,$val);   
            } 
         }
         $this->id = $this->getid();
      }
      return $ismatched;
   }
   function loadone($id,$col=null,$vtype=null,$byval=null) {
      if (is_object($id)) $id = json_encode($id);
      if ($id > 0 && $this->exists()) {
         $pk = $this->getpk();
         if (!$col) $col = $pk; 
         //if (is_object($id)) $id = $id->Id;
         $query = $this->dbhandle->select();
         $cols = array();
         $ltmd = array();
         foreach ($this->columns as $dcol => $type) {
            if (preg_match('/^\{/',$type)) {
               $params = json_decode($type);
               $type = $params->DataType;         
            } else $type = preg_replace('/\:.+/','',$type);
            if (!$params->Hidden) {
               switch ($type) {
               case 'Map Data': {
                  $cols[$dcol] = new Zend_Db_Expr("AsText($dcol)");               
               } break;
               case 'LINKEDTOM': {
                  // insert linkedtom management
                  if (isset($params)) {
                     $ttab = $params->TargetType;
                     $tcol = $params->TargetField;
                     $p = (isset($params->Property))?$params->Property:$dcol;
                  } else {
                     preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                     $ttab = $matches[2];
                     $tcol = $matches[3];
                     $p = $dcol;
                  }
                  $pk = $this->getpk();
                  $lid = $this->getid();
                  $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                  $obj = new stdClass();
                  $obj->IType = $ttab;
                  $obj->ListData = $ltm->selecttargets($pk,$lid);
                  $ltmd[$dcol] = $obj;
               } break;
               default: {
                  $cols[$dcol] = $dcol; 
               } break;
               }
            }   
         }
         $query->from($this->tablename, $cols);
         $query->where("$col = ?",$id);
         switch($vtype) {
         case 'Current': {
            $vercol = null;
            foreach ($this->columns as $dcol => $ctype) {
               if (preg_match('/^\{/',$ctype)) {
                  $cparams = json_decode($ctype);
                  $ctype = $cparams->DataType;         
               } else $ctype = preg_replace('/\:.+/','',$ctype);
               if ($ctype == 'Version') $vercol = $dcol;
            }
            if (isset($vercol)&&isset($byval)) {
            /* For some reason = will not always match float values 
               so it is necessary to add 2 clauses to sandwich the required 
               version number */ 
               $byval = floatval($byval);
               $query->where("$vercol > ?", $byval-0.05);
               $query->where("$vercol < ?", $byval+0.05);
            }
         }break;
         case 'Last': {
            $countcol = null;
            foreach ($this->columns as $dcol => $ctype) {
               if (preg_match('/^\{/',$ctype)) {
                  $cparams = json_decode($ctype);
                  $ctype = $cparams->DataType;         
               } else $ctype = preg_replace('/\:.+/','',$ctype);
               if ($ctype == 'Count') $countcol = $dcol;
            }
            if (isset($vercol)) {
               $query->order("$countcol DESC");
            }
         }break;
         case 'Present': {
            $dfcol = null;
            $dtcol = null;
            foreach ($this->columns as $dcol => $ctype) {
               if (preg_match('/^\{/',$ctype)) {
                  $cparams = json_decode($ctype);
                  $ctype = $cparams->DataType;         
               } else $ctype = preg_replace('/\:.+/','',$ctype);
               if ($ctype == 'Date From')    $dfcol = $dcol;
               if ($ctype == 'Date Until')   $dtcol = $dcol;
            }
            if (isset($dfcol)) {   
               $query->where("$dfcol <= CURDATE()");
               $query->where("$dtcol >= CURDATE() OR $dtcol IS NULL");
            }
         }break;
         case 'Next': {
            $datecol = null;
            foreach ($this->columns as $dcol => $ctype) {
               if (preg_match('/^\{/',$ctype)) {
                  $cparams = json_decode($ctype);
                  $ctype = $cparams->DataType;         
               } else $ctype = preg_replace('/\:.+/','',$ctype);
               if ($ctype == 'Date') $datecol = $dcol;
            }
            if (isset($datecol)) {   
               $query->where("$datecol >= CURDATE()");//?", new Zend_Db_Expr("CURDATE()"));
               $query->order("$datecol ASC");
            }
         }break;
         case 'Previous': {
            $datecol = null;
            foreach ($this->columns as $dcol => $ctype) {
               if (preg_match('/^\{/',$ctype)) {
                  $cparams = json_decode($ctype);
                  $ctype = $cparams->DataType;         
               } else $ctype = preg_replace('/\:.+/','',$ctype);
               if ($ctype == 'Date') $datecol = $dcol; 
            }
            if (isset($datecol)) {   
               $query->where("$datecol < CURDATE()");//?", new Zend_Db_Expr("CURDATE()"));
               $query->order("$datecol DESC");
            }   
         }break;
         }
//echo $query->__toString();  
         $this->data = $this->dbhandle->fetchRow($query);
         foreach ($this->columns as $dcol => $ctype) {
            if (preg_match('/^\{/',$ctype)) {
               $cparams = json_decode($ctype);
               $ctype = $cparams->DataType;         
            } else $ctype = preg_replace('/\:.+/','',$ctype);
            if ($ctype == 'Date') $datecol = $dcol;
               
            switch($ctype) {
            case 'LINKEDTOM': {
               // insert linkedtom management
               if (isset($cparams)) {
                  $ttab = $cparams->TargetType;
                  $tcol = $cparams->TargetField;
                  $p = (isset($cparams->Property))?$cparams->Property:$dcol;
               } else {
                  preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                  $ttab = $matches[2];
                  $tcol = $matches[3];
                  $p = $dcol;
               }
               $pk = $this->getpk();
               $lid = $this->getid();
               $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
               $obj = new stdClass();
               $obj->IType = $ttab;
               $obj->ListData = $ltm->selecttargets($pk,$lid);
               $ltmd[$dcol] = $obj;
            } break;
            default: {
               $this->data[$col] = $this->postprocessterm($col,$type,$val);
            } break;   
            } 
         }
            
         if (count($ltmd)) {
            foreach($ltmd as $dcol => $dval) {
               $this->data[$dcol] = $dval;
            }
         }
         $this->id = $this->getid();
      }
   }
   function search($val) {
      $result = array();
      if ($this->exists()) {
         $query = $this->dbhandle->select();
         $cols = array($this->getpk());
         $query->from($this->tablename, $cols);
         $matches = 0;
         foreach ($this->columns as $dcol => $type) {
            if (preg_match('/^\{/',$type)) {
               $params = json_decode($type);
               $type = $params->DataType;         
            } else $type = preg_replace('/\:.+/','',$type);
            switch($type) {
            case 'Short Text': {
               if ($matches == 0) $query->where("$dcol LIKE ?",$val);
               else $query->orwhere("$dcol LIKE ?",$val);
               $matches++;
            }break; 
            }
         }
         $result = $this->dbhandle->fetchAll($query);                           
      }
      return $result;      
   }
   function selectRowCount($col,$val,$settings=null,$where=null) {
      $rowcount = array(0);
      if (isset($settings)) {
         $itype = $this->gettablename();
         $isettings = $settings->$itype;
         $sby = (isset($isettings->SearchBy))?$isettings->SearchBy:null;
         if (isset($sby)) $qsby = $this->dbhandle->quoteIdentifier($sby);  
         $sfor = (isset($isettings->SearchFor))?$isettings->SearchFor:null;
         $stype = (isset($isettings->SearchType))?$isettings->SearchType:null;
      }
      if (isset($col)&&isset($val)) {
         if ($this->exists()) {
            $query = $this->dbhandle->select();
            $cols = array();
            $ltmd = array();
            $cols = array('rowcount' => 'COUNT(*)');
            $query->from($this->tablename, $cols);
            if (preg_match('/\%/',$val)) $query->where("$col LIKE ?",$val);
            else $query->where("$col = ?",$val);
            if (isset($where) && is_array($where)) {
               foreach($where as $col => $val){
                  $def = $this->getcoldef($col);
                  $type = $def->DataType;
                  switch($type) {
                  default: $query->where("$col = ?",$val);
                  }  
               } 
            }
            if (isset($sby)) {
               $stype = "".$this->columns[$sby];
               if (preg_match('/^\{/',$stype)) {
                  $params = json_decode($stype);
                  $stype = $params->DataType;
               } else {
                  $bits = preg_split('/\:/',$stype);
                  $stype = $bits[0];
               }
               switch($stype) {
               case 'INTERFACE': {
                  $cache = new G_Cache();
                  $ints = $cache->select('Name',"%$sfor%");
                  $sfors = array();
                  foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
                  $query->where("$qsby IN(?)",$sfors);
               }break;
               case 'Taxa': {
                  $cache = new G_Cache();
                  $ints = $cache->select('Name',"%$sfor%");
                  $sfors = array();
                  foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
                  $query->where("$qsby IN(?)",$sfors);
               }break;
               case 'Date': {
                  if (preg_match('/([>,<]\=*)/',$sfor,$m)) $sop = $m[1];
                  else $sop = '=';
                  if ($sop == '') $sop = '=';
                  $sfor = preg_replace('/[>,<]\=*\s*/','',$sfor);
                  if (preg_match('/(\d{2})\/(\d{2})\/(\d{4})/',$sfor,$m)) $sfor = $m[3]."-".$m[2]."-".$m[1];
                  if (preg_match('/^\d{4}$/',$sfor)) $sby = new Zend_Db_Expr("YEAR($qsby)");
                  $scol = (preg_match('/^\d{4}$/',$sfor))?new Zend_Db_Expr("YEAR($qsby)"):$qsby;
                  $query->where("$scol $sop ?",$sfor);
               }break;
               /* linked data is handled separately */
               /* case 'LINKEDTO':break; */
               case 'LINKEDTO': {
                  if (isset($params)) {
                     $type = $params->DataType;
                     $table = $params->TargetType;
                     $column = $params->TargetField;
                     $link = null;
                     $link = new $table();
                     $lpk = $link->getpk();
                     $ints = $link->search("%$sfor%");
                     $sfors = array();
                     foreach($ints as $i => $int) $sfors[] = $int[$lpk]; 
                     $query->where("$qsby IN(?)",$sfors);
                  }   
               }break;
               case 'LINKEDTOM':break;
               case 'Map Data': {
                  switch($stype) {
                  case 'Contains': $stype = 'Contains'; break;
                  case 'Within': $stype = 'Within'; break; 
                  default: $stype = 'Intersects';break;                     
                  }
                  $sfor = new Zend_Db_Expr("GeomFromText('$sfor',4326)");
                  $query->where("$stype($scol,?)",$sfor);  
               }break;
               default: {
                  if (preg_match('/\,/',$sfor)) {
                     $sfors = preg_split('/\s*\,\s*/',$sfor); 
                     $query->where("$qsby IN(?)",$sfors);
                  } else {
                     $query->where("$qsby LIKE ?","%$sfor%");
                  }
               }break;
               }  
            }
            if ($settings) {
               $settings = (array) $settings;
               foreach($this->columns as $col => $type) {
                  $inherit = false;
                  if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $inherit = $params->Inherit;
                     $dtype = $params->DataType;
                     $itype = $params->TargetType;
                  } else {
                     $types = preg_split('/\:/',$type);
                     $dtype = $types[0];
                     $inherit = in_array('Inherit',$types);
                     if ($inherit) {
                        preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                        $itype = $matches[2];
                     } 
                  }
                  if ($inherit && isset($settings[$itype]) 
                           && isset($settings[$itype]->Current) 
                           && (isset($dtype) && ($dtype != 'LINKEDTOM'))) {
                     $query->where("$col = ?",$settings[$itype]->Current);
                  }
               }
            }
            $rowcount = $this->dbhandle->fetchCol($query);                  
         }
      }
      return $rowcount[0];   
   }
   function select($col,$val,$settings=null,$where=null,$page=null) {
      $result = array();
      if (isset($settings)) {
         $itype = $this->gettablename();
         $isettings = $settings->$itype;
         $sby = (isset($isettings->SearchBy))?$isettings->SearchBy:null;
         if (isset($sby)) $qsby = $this->dbhandle->quoteIdentifier($sby);  
         $sfor = (isset($isettings->SearchFor))?$isettings->SearchFor:null;
         $stype = (isset($isettings->SearchType))?$isettings->SearchType:null;
      }
      if (isset($col)&&isset($val)) {
         if ($this->exists()) {
            $query = $this->dbhandle->select();
            $cols = array();
            $ltmd = array();
            $order = null;
            foreach ($this->columns as $dcol => $type) {
               // don't select the column you already know. 
               // if this is a link it can lead to recursion
               if ($dcol != $col) {
                  if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $type = $params->DataType;         
                  } else $type = preg_replace('/\:.+/','',$type);
                  if (!isset($params->Hidden) || !$params->Hidden) {
                     switch ($type) {
                     case 'Version': {
                        $order = "$dcol ASC";
                        $cols[$dcol] = $dcol; 
                     } break;
                     case 'Number': {
                        $order = "$dcol ASC";
                        $cols[$dcol] = $dcol; 
                     } break;
                     case 'Date': {
                        $order = "$dcol DESC";
                        $cols[$dcol] = $dcol; 
                     } break;
                     case 'Map Data': {
                        $cols[$dcol] = new Zend_Db_Expr("AsText($dcol)");               
                     } break;
                     case 'LINKEDTOM': {
                        // insert linkedtom management
                        if (isset($params)) {
                           $ttab = $params->TargetType;
                           $tcol = $params->TargetField;
                           $p = (isset($params->Property))?$params->Property:$dcol;
                        } else {
                           preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                           $ttab = $matches[2];
                           $tcol = $matches[3];
                           $p = $dcol;
                        }
                        $pk = $this->getpk();
                        $id = $this->getid();
                        $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                        $obj = new stdClass();
                        $obj->IType = $ttab;
                        $obj->ListData = $ltm->selecttargets($pk,$id);
                        $ltmd[$dcol] = $obj;
                     } break;
                     default: {
                        $cols[$dcol] = $dcol; 
                     } break;
                     }
                  }
               }
            }
            $query->from($this->tablename, $cols);
            if (preg_match('/\%/',$val)) $query->where("$col LIKE ?",$val);
            else $query->where("$col = ?",$val);
            if (isset($where) && is_array($where)) {
               foreach($where as $col => $val){
                  $def = $this->getcoldef($col);
                  $type = $def->DataType;
                  switch($type) {
                  case "Map Data": {
                     $sfor = new Zend_Db_Expr("GeomFromText('$val')");
                     $query->where("Contains($col,?)",$sfor);
                     $settings = null;
                  }break;
                  default: $query->where("$col = ?",$val); break;
                  }  
               } 
            }
            if (isset($sby)) {
               $stype = "".$this->columns[$sby];
               if (preg_match('/^\{/',$stype)) {
                  $params = json_decode($stype);
                  $stype = $params->DataType;
               } else {
                  $bits = preg_split('/\:/',$stype);
                  $stype = $bits[0];
               }
               switch($stype) {
               case 'INTERFACE': {
                  $cache = new G_Cache();
                  $ints = $cache->select('Name',"%$sfor%");
                  $sfors = array();
                  foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
                  $query->where("$qsby IN(?)",$sfors);
               }break;
               case 'Taxa': {
                  $cache = new G_Cache();
                  $ints = $cache->select('Name',"%$sfor%");
                  $sfors = array();
                  foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
                  $query->where("$qsby IN(?)",$sfors);
               }break;
               case 'Date': {
                  if (preg_match('/([>,<]\=*)/',$sfor,$m)) $sop = $m[1];
                  else $sop = '=';
                  if ($sop == '') $sop = '=';
                  $sfor = preg_replace('/[>,<]\=*\s*/','',$sfor);
                  if (preg_match('/(\d{2})\/(\d{2})\/(\d{4})/',$sfor,$m)) $sfor = $m[3]."-".$m[2]."-".$m[1];
                  if (preg_match('/^\d{4}$/',$sfor)) $sby = new Zend_Db_Expr("YEAR($qsby)");
                  $scol = (preg_match('/^\d{4}$/',$sfor))?new Zend_Db_Expr("YEAR($qsby)"):$qsby;
                  $query->where("$scol $sop ?",$sfor);
               }break;
               /* linked data is handled separately */
               /* case 'LINKEDTO':break; */
               case 'LINKEDTO': {
                  if (isset($params)) {
                     $type = $params->DataType;
                     $table = $params->TargetType;
                     $column = $params->TargetField;
                     $link = null;
                     $link = new $table();
                     $lpk = $link->getpk();
                     $ints = $link->search("%$sfor%");
                     $sfors = array();
                     foreach($ints as $i => $int) $sfors[] = $int[$lpk]; 
                     $query->where("$qsby IN(?)",$sfors);
                  }   
               }break;
               case 'LINKEDTOM':break;
               case 'Map Data': {
                  switch($stype) {
                  case 'Contains': $stype = 'Contains'; break;
                  case 'Within': $stype = 'Within'; break; 
                  default: $stype = 'Intersects';break;                     
                  }
                  $sfor = new Zend_Db_Expr("GeomFromText('$sfor',4326)");
                  $query->where("$stype($scol,?)",$sfor);  
               }break;
               default: {
                  if (preg_match('/\,/',$sfor)) {
                     $sfors = preg_split('/\s*\,\s*/',$sfor); 
                     $query->where("$qsby IN(?)",$sfors);
                  } else {
                     $query->where("$qsby LIKE ?","%$sfor%");
                  }
               }break;
               }  
            }
            $ordered = false;
            if ($settings) {
               $settings = (array) $settings;
               foreach($this->columns as $col => $type) {
                  $inherit = false;
                  if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $inherit = $params->Inherit;
                     $dtype = $params->DataType;
                     $itype = $params->TargetType;
                  } else {
                     $types = preg_split('/\:/',$type);
                     $dtype = $types[0];
                     $inherit = in_array('Inherit',$types);
                     if ($inherit) {
                        preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                        $itype = $matches[2];
                     } 
                  }
                  if ($inherit && isset($settings[$itype]) 
                           && isset($settings[$itype]->Current) 
                           && (isset($dtype) && ($dtype != 'LINKEDTOM'))) {
                     $query->where("$col = ?",$settings[$itype]->Current);
                  }
                  if (isset($settings[$itype]) && isset($settings[$itype]->Orders)) {
                     $query->reset( Zend_Db_Select::ORDER );
                     foreach($settings[$itype]->Orders as $oobj) {
                        if ($this->columns[$oobj->Column]) {
                           $lcol = $oobj->Column;
                           $dirn = $oobj->Order;
                           switch($dirn) {
                           case 'Up':     $query->order("$lcol ASC");   break;
                           case 'Down':   $query->order("$lcol DESC");  break;
                           default:       $query->order("$lcol $dirn"); break;
                           }
                           $ordered = true;
                        }
                     }
                  }                  
               }
            }
            if (!$ordered) {
               if (isset($this->orders) && count($this->orders)>0) {
                  $query->reset( Zend_Db_Select::ORDER );
                  foreach($this->orders as $col => $dir){
                     switch ($dir) {
                     case 'Up':     $query->order("$col ASC");  break;
                     case 'Down':   $query->order("$col DESC"); break;
                     default:       $query->order("$col $dir"); break;
                     }
                     
                  }
               } else if (isset($order)) $query->order($order);
            } 
            if (isset($page)) $query->limitPage($page,$this->perpage);
	//error_log($query->__toString());
            $result = $this->dbhandle->fetchAll($query);
            foreach($result as $rownum => $row) {
               $match = true;
               foreach ($row as $col => $val) {
                  $type = "".$this->columns[$col];
                  if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $type = $params->DataType;
                  } else {
                     $bits = preg_split('/\:/',$type);
                     $type = $bits[0];
                  }
                  switch ($type) {                    
                  case 'LINKEDTO': {
                     if (isset($params)) {
                           if (!is_object($val)) {
                           $type = $params->DataType;
                           $table = $params->TargetType;
                           $column = $params->TargetField;
                           $link = null;
                           $link = new $table();
                           $link->loaddata($val,$column,false);
                           $obj = new stdClass();
                           $obj->Name = $link->getname();
                           $obj->Id = $link->getid();
                           $result[$rownum][$col] = $obj;
                           if (isset($sby) && $col == $sby) {
                              $match = preg_match("/$sfor/",$obj->Name); 
                           }
                        }
                     }   
                  }break;  
                  case 'LINKEDTOM': {
                     if (isset($params)) {
                        if (!is_object($val)) {
                           $type = $params->DataType;
                           $ttab = $params->TargetType;
                           $tcol = $params->TargetField;
                           $p = (isset($params->Property))?$params->Property:$col;
                           $pk = $this->getpk();
                           $id = $this->getid();
                           $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                           $obj = new stdClass();
                           $obj->IType = $ttab;
                           $obj->ListData = $ltm->selecttargets($pk,$row[$pk]);
                           $result[$rownum][$col] = $obj;
                           if (isset($sby) && $col == $sby) {
                              $match = false;
                              foreach($obj->ListData as $item => $liobj) {
                                 $match = $match || preg_match("/$sfor/",$liobj->Name);
                              } 
                           }
                        }
                     }   
                  }break;
                  default: {
                     $result[$rownum][$col] = $this->postprocessterm($col,$type,$val);
                  }break;
                  }
               }
               if (!$match) unset($result[$rownum]);
            }      
         }
      }
      $result = array_values($result);
      return $result;   
   }
   function selectAllRowCount($settings = null) {
      $rowcount = array(0);
      $t = $this->tablename;
      $sby = (isset($settings->$t->SearchBy))?$settings->$t->SearchBy:null;
      if (isset($sby)) $qsby = $this->dbhandle->quoteIdentifier($sby);                  
      $sfor = (isset($settings->$t->SearchFor))?$settings->$t->SearchFor:null;
      if ($this->exists()) {
         $query = $this->dbhandle->select();
         $cols = array('count' => 'COUNT(*)');
         $query->from($this->tablename, $cols);
         if (isset($sby)) {
            $stype = "".$this->columns[$sby];
            if (preg_match('/^\{/',$stype)) {
               $params = json_decode($stype);
               $stype = $params->DataType;
            } else {
               $bits = preg_split('/\:/',$stype);
               $stype = $bits[0];
            }
            switch($stype) {
            case 'INTERFACE': {
               $cache = new G_Cache();
               $ints = $cache->select('Name',"%$sfor%");
               $sfors = array();
               foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
               $query->where("$qsby IN(?)",$sfors);
            }break;
            case 'Taxa': {
               $cache = new G_Cache();
               $ints = $cache->select('Name',"%$sfor%");
               $sfors = array();
               foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
               $query->where("$qsby IN(?)",$sfors);
            }break;
            case 'Date': {
               if (preg_match('/([>,<]\=*)/',$sfor,$m)) $sop = $m[1];
               else $sop = '=';
               if ($sop == '') $sop = '=';
               $sfor = preg_replace('/[>,<]\=*\s*/','',$sfor);
               if (preg_match('/(\d{2})\/(\d{2})\/(\d{4})/',$sfor,$m)) $sfor = $m[3]."-".$m[2]."-".$m[1];
               $scol = (preg_match('/^\d{4}$/',$sfor))?new Zend_Db_Expr("YEAR($qsby)"):$qsby;
               $query->where("$scol $sop ?",$sfor);
            }break;
            /* linked data is handled separately */
            /* case 'LINKEDTO':break; */
            case 'LINKEDTO': {
               if (isset($params)) {
                  $type = $params->DataType;
                  $table = $params->TargetType;
                  $column = $params->TargetField;
                  $link = null;
                  $link = new $table();
                  $lpk = $link->getpk();
                  $ints = $link->search("%$sfor%");
                  $sfors = array();
                  foreach($ints as $i => $int) $sfors[] = $int[$lpk]; 
                  $query->where("$qsby IN(?)",$sfors);
               }   
            }break;
            case 'LINKEDTOM':break;
            default: {
               if (preg_match('/\,/',$sfor)) {
                  $sfors = preg_split('/\s*\,\s*/',$sfor); 
                  $query->where("$qsby IN(?)",$sfors);
               } else {
                  $query->where("$qsby LIKE ?","%$sfor%");
               }
            }break;
            }  
         }
         if ($settings) {
            $settings = (array) $settings;
            foreach($this->columns as $col => $type) {
               $inherit = false;
               if (preg_match('/^\{/',$type)) {
                  $params = json_decode($type);
                  $inherit = (isset($params->Inherit))?$params->Inherit:false;
                  $itype = (isset($params->TargetType))?$params->TargetType:null;
               } else {
                  $types = preg_split('/\:/',$type);
                  $inherit = in_array('Inherit',$types);
                  if ($inherit) {
                     preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                     $itype = $matches[2];
                  } 
               }
               if (($inherit) && isset($settings[$itype]) && isset($settings[$itype]->Current)) {
                  if (!isset($params) || ($params->DataType != 'LINKEDTOM'))
                     $query->where("$col = ?",$settings[$itype]->Current);
               }
            }
         }
         $rowcount = $this->dbhandle->fetchCol($query);
      }
      return $rowcount[0];
   }
   function selectAll($settings = null,$page = null) {
      $result = array();
      $t = $this->tablename;
      $sby = (isset($settings->$t->SearchBy))?$settings->$t->SearchBy:null;
      if (isset($sby)) $qsby = $this->dbhandle->quoteIdentifier($sby);
      $sfor = (isset($settings->$t->SearchFor))?$settings->$t->SearchFor:null;
      if ($this->exists()) {
         $query = $this->dbhandle->select();
         $cols = array();
         foreach ($this->columns as $dcol => $type) {
            if (preg_match('/^\{/',$type)) {
               $params = json_decode($type);
               $type = $params->DataType;
            } else $type = preg_replace('/\:.+/','',$type);
            if (!isset($params) || !isset($params->Hidden) || !$params->Hidden) {
               switch ($type) {
               case 'Map Data': {
                  $cols[$dcol] = new Zend_Db_Expr("AsText($dcol)");               
               } break;
               case 'LINKEDTOM': {
                  $ltmd[] = $dcol;
               } break;
               default: {
                  $cols[$dcol] = $dcol; 
               } break;
               }
            }   
         }
         $query->from($this->tablename, $cols);
         if (isset($sby)) {
            $stype = "".$this->columns[$sby];
            if (preg_match('/^\{/',$stype)) {
               $params = json_decode($stype);
               $stype = $params->DataType;
            } else {
               $bits = preg_split('/\:/',$stype);
               $stype = $bits[0];
            }
            switch($stype) {
            case 'INTERFACE': {
               $cache = new G_Cache();
               $ints = $cache->select('Name',"%$sfor%");
               $sfors = array();
               foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
               $query->where("$qsby IN(?)",$sfors);
            }break;
            case 'Taxa': {
               $cache = new G_Cache();
               $ints = $cache->select('Name',"%$sfor%");
               $sfors = array();
               foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
               $query->where("$qsby IN(?)",$sfors);
            }break;
            case 'Date': {
               if (preg_match('/([>,<]\=*)/',$sfor,$m)) $sop = $m[1];
               else $sop = '=';
               if ($sop == '') $sop = '=';
               $sfor = preg_replace('/[>,<]\=*\s*/','',$sfor);
               if (preg_match('/(\d{2})\/(\d{2})\/(\d{4})/',$sfor,$m)) $sfor = $m[3]."-".$m[2]."-".$m[1];
               $scol = (preg_match('/^\d{4}$/',$sfor))?new Zend_Db_Expr("YEAR($qsby)"):$qsby;
               $query->where("$scol $sop ?",$sfor);
            }break;
            /* linked data is handled separately */
            /* case 'LINKEDTO':break; */
            case 'LINKEDTO': {
               if (isset($params)) {
                  $type = $params->DataType;
                  $table = $params->TargetType;
                  $column = $params->TargetField;
                  $link = null;
                  $link = new $table();
                  $lpk = $link->getpk();
                  $ints = $link->search("%$sfor%");
                  $sfors = array();
                  foreach($ints as $i => $int) $sfors[] = $int[$lpk]; 
                  $query->where("$qsby IN(?)",$sfors);
               }   
            }break;
            case 'LINKEDTOM':break;
            default: {
               if (preg_match('/\,/',$sfor)) {
                  $sfors = preg_split('/\s*\,\s*/',$sfor); 
                  $query->where("$qsby IN(?)",$sfors);
               } else {
                  $query->where("$qsby LIKE ?","%$sfor%");
               }
            }break;
            }  
         }
         $ordered = false;
         if ($settings) {
            $settings = (array) $settings;
            foreach($this->columns as $col => $type) {
               $inherit = false;
               if (preg_match('/^\{/',$type)) {
                  $params = json_decode($type);
                  $inherit = (isset($params->Inherit))?$params->Inherit:false;
                  $itype = (isset($params->TargetType))?$params->TargetType:null;
               } else {
                  $types = preg_split('/\:/',$type);
                  $inherit = in_array('Inherit',$types);
                  if ($inherit) {
                     preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                     $itype = $matches[2];
                  } 
               }
               if (($inherit) && isset($settings[$itype]) && isset($settings[$itype]->Current)) {
                  if (!isset($params) || ($params->DataType != 'LINKEDTOM'))
                     $query->where("$col = ?",$settings[$itype]->Current);
               }
               if (isset($settings[$t]) && isset($settings[$t]->Orders)) {
                  $query->reset( Zend_Db_Select::ORDER );
                  foreach($settings[$t]->Orders as $oobj) {
                     if (isset($this->columns[$oobj->Column])) {
                        $lcol = $oobj->Column;
                        $dirn = $oobj->Order;
                        switch($dirn) {
                        case 'Up':     $query->order("$lcol ASC");   break;
                        case 'Down':   $query->order("$lcol DESC");  break;
                        default:       $query->order("$lcol $dirn"); break;
                        }
                        $ordered = true;
                     }
                  }
               }
            }
         }
         if (!$ordered) {
            if (isset($this->orders) && count($this->orders)>0) {
               $query->reset( Zend_Db_Select::ORDER );
               foreach($this->orders as $col => $dir) {
                  switch($dir) {
                  case 'Up':     $query->order("$col ASC");   break;
                  case 'Down':   $query->order("$col DESC");  break;
                  default:       $query->order("$col $dir");  break;
                  }
               }
            }
         }
         if (isset($page)) $query->limitPage($page,$this->perpage);
         $result = $this->dbhandle->fetchAll($query);
         foreach($result as $rownum => $row) {
            $match = true;
            foreach ($row as $col => $val) {
               $type = "".$this->columns[$col];
               if (preg_match('/^\{/',$type)) {
                  $params = json_decode($type);
                  $type = $params->DataType;
               } else {
                  $bits = preg_split('/\:/',$type);
                  $type = $bits[0];
               }
               switch ($type) {
               case 'LINKEDTO': {
                  if (isset($params)) {
                     if (!is_object($val)) {
                        $type = $params->DataType;
                        $table = $params->TargetType;
                        $column = $params->TargetField;
                        $link = null;
                        $link = new $table();
                        $link->loaddata($val,$column,false);
                        if (isset($settings[$table]->Expand)) {
                           $obj = $link->getdata();
                           $obj->Id = $this->getid();
                        } else {
                           $obj = new stdClass();
                           $obj->Name = $link->getname();
                           $obj->Id = $link->getid();
                        }
                        $result[$rownum][$col] = $obj;
                        if (isset($sby) && $col == $sby) {
                           $match = preg_match("/$sfor/",$obj->Name); 
                        }
                     }
                  }   
               }break;
               default: {
                  $result[$rownum][$col] = $this->postprocessterm($col,$type,$val);
               }break;
               }
            }
            if (isset($ltmd) && is_array($ltmd) && count($ltmd)>0) {
               foreach($ltmd as $col) {
                  $mtype = $this->columns[$col];
                  if (preg_match('/^\{/',$mtype)) {
                     $mparams = json_decode($mtype);
                     $type = $mparams->DataType;
                     $ttab = $mparams->TargetType;
                     $tcol = $mparams->TargetField;
                     $p = ($mparams->Property)?$mparams->Property:$col;
                  } else if(preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches)) {
                     $type = $matches[1];
                     $ttab = $matches[2];
                     $tcol = $matches[3];
                     $p = $col;
                  }
                  if (isset($ttab) && isset($tcol)) {
                     if (!is_object($val)) {
                        $pk = $this->getpk();
                        $id = $row[$pk];
                        $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                        $obj = new stdClass();
                        $obj->IType = $ttab;
                        $obj->Current = 'Multiple';
                        $obj->Key = $tcol; 
                        $obj->ListData = $ltm->selecttargets($pk,$id);
                        $result[$rownum][$col] = $obj;
                        if ($sby && $col == $sby) {
                           $match = false;
                           foreach($obj->ListData as $item => $liobj) {
                              $match = $match || preg_match("/$sfor/",$liobj->Name);
                           } 
                        }
                     }
                  }      
               }
            }
            if (!$match) unset($result[$rownum]);
            if (!count($result)) $result = $query->__toString();            
         }
      }
      $result = array_values($result);
      return $result;   
   }
   function dbupcode($col,$val) {
      $pk = $this->getpk();
      $id = $this->getid();
      if (isset($id)) {
         $where = $this->dbhandle->quoteInto("$pk = ?",$id);
         $enc = $this->dbencrypt($val);
         $data = array($col => $enc);
         $status = $this->dbhandle->update($this->tablename,$data,$where);
      }         
   }
   function getinterval($iv) {
      $n = preg_replace('/[^\d]+/','',$iv);
      $p = preg_replace('/[^A-Z]+/','',$iv);
      switch($p) {
      case 'H': $p = 'HOUR';break;
      case 'D': $p = 'DAY';break;
      case 'W': $p = 'WEEK';break;
      case 'M': $p = 'MONTH';break;
      case 'Y': $p = 'YEAR';break;
      }
      return "INTERVAL $n $p";
   }      
}
class DBT_FilterView extends DBT {
   protected $sourcetable;
   protected $filters = array();
   // eg $filters = array("Col1" => "Val1", "Col2" => "Val2");
   protected $orders = array();
   // eg $orders = array("Col1" => "ASC", "Col2" => "DESC");
   protected $indexon;
   protected $selectto;
   protected $domain = 'genera';
   function DBT_FilterView() {
      parent::DBT();
      $st = $this->sourcetable;
      if (isset($st)) {  
         $dbt = new $st();;
         if (!$dbt->exists()) $dbt->create();
         $this->columns = $dbt->getcolumns();
      }
   }
   function getuserpermissions() {
      $perms = array('View','Add','Edit','Delete');
      $granted = array();
      return $granted;      
   }
   function getpk() {
      return $this->indexon;
   }
   function create() {
      global $pending;
      if (!isset($pending[$this->tablename])) {
         $pending[$this->tablename] = true;
         $dbh = $this->dbhandle;
         $query = $dbh->select();
         $query->from($this->sourcetable);
         foreach($this->filters as $col => $val) {
            $query->where("$col = ?", $val);
         }               
         foreach($this->orders as $col => $dir) {
            $query->order("$col $dir");
         }               
         $select = $query->__toString();
         $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select"; 
         $dbh->exec($statement);
         if ($this->gs_publishme) $this->gs_publish();
         unset($pending[$this->tablename]);
      }                  
   }
   function exists() {
      $statement = "show tables like '$this->tablename'";
      $status = $this->dbhandle->fetchAll($statement);
      return count($status);
   }       
}
class DBT_ReportView extends DBT {
   protected $sourcetables;
   // eg $sourcetables = array("Table1" => "Alias1", "Table2" => "Alias2");
   protected $joins = array();
   // eg $joins = array ("TargetTable1" => "SourceTable1");
   protected $sourcecolumns;
   // eg $sourcetables = array(  "Table1" => array("AsAlias1" => "Col1","AsAlias2" => "Col2"), 
   //                            "Table2" => array("AsAlias1" => "Col1","AsAlias2" => "Col2"));
   protected $filters = array();
   // eg $filters = array("Col1" => "Val1", "Col2" => "Val2");
   protected $colmatch = array();
   protected $orders = array();
   // eg $orders = array("Col1" => "ASC", "Col2" => "DESC");
   protected $group = array();
   // eg $group = array("Col1");
   protected $indexon;
   protected $selectto;
   protected $nolist = array();
   protected $domain = 'genera';
   function DBT_ReportView() {
      parent::DBT();
      foreach($this->sourcetables as $table => $alias) {
         $link = new $table();
         if (!$link->exists()) $link->create();
         $scols = $link->getcolumns();
         if ($this->sourcecolumns[$table]) {
            foreach($this->sourcecolumns[$table] as $alias => $column) {
               $alias = (is_int($alias))?$column:$alias;
               if ($scols[$column] == 'Unique Key') {
                  $obj = new stdClass();
                  $obj->DataType = 'LINKEDTO';
                  $obj->TargetType = $table;
                  $obj->TargetField = $column;
                  if (in_array($alias,$this->nolist)) $obj->NoList = 1;
                  $this->columns[$alias] = json_encode($obj);
               } else if (preg_match('/^\{/',$scols[$column])) {
                  $obj = json_decode($scols[$column]);
                  if (in_array($alias,$this->nolist)) $obj->NoList = 1;
                  $this->columns[$alias] = json_encode($obj);
               } else {
                  $obj = new stdClass();
                  $obj->DataType = preg_replace('/\:.+/','',$scols[$column]);
                  if (in_array($alias,$this->nolist)) $obj->NoList = 1;
                  $this->columns[$alias] = json_encode($obj);
               }                            
            }
         }
      }
//print "<pre>"; print_r($this->columns); print "</pre>"; exit;   
   }
   function getversions($val=null) {return null;}
   function getuserpermissions() {
      $perms = array('View','Add','Edit','Delete');
      $granted = array();
      foreach($perms as $perm) {
         if ($this->isallowed($this->user,$perm)) $granted[] = $perm;      
      }
      return $granted;      
   }
   function getpk() {
      return $this->indexon;
   }
   function create() {
      global $pending;
      if (!isset($pending[$this->tablename])) {
         $pending[$this->tablename] = true;
         $dbh = $this->dbhandle;
         $query = $dbh->select();
         $count = 0;
         foreach($this->sourcetables as $table => $alias) {
            if ($count == 0) $query->from(array($alias => $table),$this->sourcecolumns[$table]);         
            else {
               $link = new $table();
               if (!$link->exists()) $link->create();
               $totable = $this->joins[$table];
               $toalias = $this->sourcetables[$totable];  
               list($incol,$tocol) = $link->getlinkedcolumn($totable);
               if (!isset($incol)) {
                  $link = new $totable();
                  if (!$link->exists()) $link->create();
                  list($tocol,$incol) = $link->getlinkedcolumn($table);
               }
               $join = "${alias}.${incol} = ${toalias}.${tocol}";
               $cols = (isset($this->sourcecolumns[$table]))?$this->sourcecolumns[$table]:array();
               foreach($cols as $col => $val) {
                  if (preg_match('/^\{/',$val)) {
                     $val = json_decode($val);
                     $val = new Zend_Db_Expr($val->Expression);
                     $cols[$col] = $val;
                  }
               }
               $query->joinLeft(array($alias => $table),$join,$cols);
            }
            $count++;
         }
         
         foreach($this->filters as $clause => $val) {
            if (preg_match('/^\{/',$val)) {
               $val = json_decode($val);
               $val = new Zend_Db_Expr($val->Expression);
            }
            $query->where($clause, $val);
         }               
         foreach($this->colmatch as $col1 => $col2) {
            $query->where("$col1 = $col2");
         }               
         foreach($this->orders as $col => $dir) {
            $query->order("$col $dir");
         }
         foreach($this->group as $col) {
            $query->group($col);
         }               
         $select = $query->__toString();
//print "$select";exit;
         $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
         $this->dbhandle->exec($statement);
         if ($this->gs_publishme) $this->gs_publish();
         unset($pending[$this->tablename]);
         
      }
   }
   function exists() {
      $statement = "show tables like '$this->tablename'";
      $status = $this->dbhandle->fetchAll($statement);
      return count($status);
   }   
}
class DBT_Analysis extends DBT {
   protected $domain = 'genera';
   function DBT_Analysis() {
      parent::DBT();
   }
   function exists() {
      $statement = "show tables like '$this->tablename'";
      $status = $this->dbhandle->fetchAll($statement);
      return count($status);
   }
}

$pending = array();
include_once('snc/S2_DBT_Lookup.php');



/*
class DBTest1 extends DBT {
   protected $tablename = 'DBTest1';
   protected $displayname = 'Database Test 1';
   // data types can be one of $this->datatypes or 
   // LINK:<tablename>.<columnname> and will pick up their 
   // datatype from that table. 
   // note that table will need to be included using 
   // include_once to ensure the definition can be read
   protected $columns = array(
      'DBTest1_ID'      => 'Unique Key',
      'name_column_1'   => 'Short Text',
      'value_column_1'  => 'Number'
   );
   protected $childtables = array('DBTest2');
}
class DBTest2 extends DBT {
   protected $tablename = 'DBTest2';
   protected $displayname = 'Database Test 2';
   protected $columns = array(
      'DBTest2_ID'      => 'Unique Key',
      'name_column_2'   => 'Short Text',
      'value_column_2'  => 'Number',
      'dbt1'            => 'LINKEDTO:DBTest1.DBTest1_ID:Optional'                  
   );
}
class DBTest3 extends DBT {
   protected $tablename = 'DBTest3';
   protected $displayname = 'Database Test 3';
   protected $columns = array(
      'DBTest3_ID'      => 'Unique Key',
      'name_column_3'   => 'Short Text',
      'value_column_3'  => 'Number',
      'dbt1'            => 'LINKEDTO:DBTest1.DBTest1_ID:Optional'                  
   );
}

$DBTest1 = new DBTest1();
$DBTest1->create();

$cols = array(
   'name_column_1' => 'Name Test 1',
   'value_column_1' => 'Value 1'   
);
$DBTest1->setdata($cols);
$status = $DBTest1->insert();
print "<h3>DBTest1 id = ".$DBTest1->getid()."</h3>";

$dbt1s = $DBTest1->selectAll();
//print_r($dbt1s);


$cols = array(
   'name_column_1' => 'Name Test 2',
   'value_column_1' => 'Value 1'   
);
$DBTest1->setdata($cols);
$status = $DBTest1->update('name_column_1', 'Name Test 1');
print "<h3>Update Status : $status</h3>";

$dbt1s = $DBTest1->selectAll();
//print_r($dbt1s);
$DBTest2 = new DBTest2();
$DBTest2->create();
$DBTest2->drop();
print $DBTest2->drawform();


print $DBTest1->drawtable();

print_r($DBTest1->exists());
$DBTest3 = new DBTest3();
print_r($DBTest3->exists());
*/
?>
