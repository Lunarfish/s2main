<?php

include_once('settings.conf');
include_once('snc/S2_DBT.php');
include_once('gen/G_Species.php');
include_once('ogc/ogc.php');

class ODS_Search_Profile extends DBT {
   protected $tablename = 'ODS_Search_Profile';
   protected $displayname = 'Search Profile';
   protected $perpage = 10;
   protected $show = array('ODS_Search_Profile_Buffer','ODS_Species_Profile_Section','ODS_Search');
   protected $alts = array('Layers'=>'ODS_Layer_Catalogue','Servers'=>'ODS_Data_Server','Searches'=>'ODS_Search');
   protected $view = '[
      {
         "Name":"Definition",
         "Data": [
            {
               "Name":"Definitions",
               "Table":"ODS_Search_Definition"
            }
         ]
      },
      {
         "Name":"Searches Using This Profile",
         "Data": [
            {
               "Name":"Searches",
               "Table":"ODS_Search"
            }
         ] 
      }
   ]';
   protected $columns = array(
      'ODS_Search_Profile_ID' => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text","Mandatory":1}',
      'Abbreviation'          => '{"DataType":"Short Text"}',
      'Description'           => '{"DataType":"Long Text"}',
      
   );
   protected $domain = 'ods';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   protected $defaultpermissions = array(
      'USR_Permissions' => '
"User_Id","Domain","Label","Value"
1,"ods","Search Full Resolution",1
1,"ods","Search Sensitive Access",1
1,"ods","Search Run",1
1,"ods","Search Administrator",1',
      'USR_Default_Permissions' => '
"Domain","Label","Description","Value","Anon" 
"ods","Search Administrator","Administer search data",0,0
"ods","Search Full Resolution","View original search data",0,0
"ods","Search Sensitive Access","View original search data",0,0
"ods","Search Run","Run searches based on existing profiles",0,0'   
   );
   function getshortname() {
      $sname = ($this->data['Abbreviation'] != null)
         ?$this->data['Abbreviation']
         :$this->data['Name'];
      return $sname;
   }
}
class ODS_Search_Definition extends DBT {
   protected $tablename = 'ODS_Search_Definition';
   protected $displayname = 'Search Definition';
   protected $perpage = 10;
   protected $show = array('ODS_Search_Profile_Buffer','ODS_Species_Profile_Section','ODS_Search');
   protected $orders = array('Number' => 'ASC');
   protected $view = '[
      {
         "Name":"Parameters",
         "Data": [
            {
               "Name":"Return Data From",
               "Table":"ODS_Search_Profile_Limit"
            },
            {
               "Name":"Buffers",
               "Table":"ODS_Search_Profile_Buffer"
            },
            {
               "Name":"Species Sections",
               "Table":"ODS_Species_Profile_Section"
            },
            {
               "Name":"Site Sections",
               "Table":"ODS_Site_Profile_Section"
            }
         ]
      },
      {
         "Name":"Searches Using This Definition",
         "Data": [
            {
               "Name":"Searches",
               "Table":"ODS_Search"
            }
         ] 
      }
   ]';
   protected $columns = array(
      'ODS_Search_Definition_ID' => 'Unique Key',
      'ODS_Search_Profile_ID'    => '{"DataType":"LINKEDTO","TargetType":"ODS_Search_Profile","TargetField":"ODS_Search_Profile_ID","Current":1,"Inherit":1}',
      'Applies_From'             => '{"DataType":"Date From"}',
      'Applies_Until'            => '{"DataType":"Date Until"}',
      'Number'                   => '{"DataType":"Count","CountOfType":"ODS_Search_Profile","CountOfField":"ODS_Search_Profile_ID"}',
      'Sources'                  => '{"DataType":"LINKEDTOM","TargetType":"ODS_Search_Profile_Source","TargetField":"ODS_Search_Profile_Source_ID","Property":"Sources"}'
   );
   protected $domain = 'ods';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Search_Profile_ID','Number'));   
   }
}
class ODS_Search_Profile_Source extends DBT {
   protected $tablename = 'ODS_Search_Profile_Source';
   protected $displayname = 'Search Profile Source';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Search_Profile_Source_ID'   => 'Unique Key',
      'Name'                           => '{"DataType":"Short Text"}',
      'Website'                        => '{"DataType":"URL"}',
      'Type'                           => '{"DataType":"Short Text","NoList":1}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
}
   
class ODS_Search_Profile_Limit extends DBT {
   protected $tablename = 'ODS_Search_Profile_Limit';
   protected $displayname = 'Return Data From';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Search_Profile_Limit_ID'    => 'Unique Key',
      'ODS_Search_Definition_ID'       => '{"DataType":"LINKEDTO","TargetType":"ODS_Search_Definition","TargetField":"ODS_Search_Definition_ID","Current":1,"Inherit":1}',
      'Last_X_Years'                   => '{"DataType":"Number"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('Last_X_Years'));   
   }
}
class ODS_Search_Profile_Buffer extends DBT {
   protected $tablename = 'ODS_Search_Profile_Buffer';
   protected $displayname = 'Profile Buffer';
   protected $domain = 'ods';
   protected $orders = array('Number' => 'ASC');
   protected $columns = array(
      'ODS_Search_Profile_Buffer_ID'   => 'Unique Key',
      'ODS_Search_Definition_ID'       => '{"DataType":"LINKEDTO","TargetType":"ODS_Search_Definition","TargetField":"ODS_Search_Definition_ID","Current":1,"Inherit":1}',
      'Buffer_Size'                    => '{"DataType":"Number"}',
      'Number'                         => '{"DataType":"Count","CountOfType":"ODS_Search_Definition","CountOfField":"ODS_Search_Definition_ID"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('Buffer_Size'));   
   }
}
class ODS_Species_Profile_Section extends DBT {
   protected $tablename = 'ODS_Species_Profile_Section';
   protected $displayname = 'Profile Species Section';
   protected $domain = 'ods';
   protected $orders = array('Number' => 'ASC');
   protected $show = array('ODS_Species_Filter','ODS_Species_Section_Action','ODS_Species_Output');
   protected $columns = array(
      'ODS_Species_Profile_Section_ID' => 'Unique Key',
      'ODS_Search_Definition_ID'       => '{"DataType":"LINKEDTO","TargetType":"ODS_Search_Definition","TargetField":"ODS_Search_Definition_ID","Current":1,"Inherit":1}',
      'Name'                           => '{"DataType":"Short Text"}',
      'Description'                    => '{"DataType":"Long Text","NoList":1}',
      'Buffer'                         => '{"DataType":"LINKEDTO","TargetType":"ODS_Search_Profile_Buffer","TargetField":"ODS_Search_Profile_Buffer_ID","Inherit":1}',
      'Return'                         => '{"DataType":"Short Text","Options":["All Species","Only Filter Species"]}',
      'Number'                         => '{"DataType":"Count","CountOfType":"ODS_SDef","CountOfField":"ODS_Search_Definition_ID"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('Name','Buffer'));   
   }
}
class ODS_Species_Section_Action extends DBT {
   protected $tablename = 'ODS_Species_Section_Action';
   protected $displayname = 'Species Section Action';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Species_Section_Action_ID'  => 'Unique Key',
      'ODS_Species_Profile_Section_ID' => '{"DataType":"LINKEDTO","TargetType":"ODS_Species_Profile_Section","TargetField":"ODS_Species_Profile_Section_ID","Current":1,"Inherit":1}',
      'Name'                           => '{"DataType":"Short Text"}',
      'Description'                    => '{"DataType":"Long Text"}',
      'ODS_Severity_Type_ID'           => '{"DataType":"LINKEDTO","TargetType":"ODS_Severity_Type","TargetField":"ODS_Severity_Type_ID"}',
      'Is_Default'                     => '{"DataType":"True or False"}',
      'Precision_Constraint'           => '{"DataType":"Number","NoList":1}',
      'Currency_Constraint'            => '{"DataType":"Number","NoList":1}',
      'Records_Constraint'             => '{"DataType":"Number","NoList":1}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );  
}

class ODS_Species_Filter extends DBT {
   protected $tablename = 'ODS_Species_Filter';
   protected $displayname = 'Species Data Filter';
   protected $domain = 'ods';
   protected $orders = array('Number' => 'ASC');
   protected $columns = array(
      'ODS_Species_Filter_ID'          => 'Unique Key',
      'ODS_Species_Profile_Section_ID' => '{"DataType":"LINKEDTO","TargetType":"ODS_Species_Profile_Section","TargetField":"ODS_Species_Profile_Section_ID","Current":1,"Inherit":1}',
      'List'                           => '{"DataType":"INTERFACE","Domain":"species","ValidTypes":["G_Species_List"],"Mandatory":1}',
      'Number'                         => '{"DataType":"Count","CountOfType":"ODS_Species_Profile_Section","CountOfField":"ODS_Species_Profile_Section_ID"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('List','Number'));   
   }
   function getlist($list) {
      global $client,$folder;
      $url = "http://${client}/${folder}/species/S2_query.php";
      $uinfo = $_REQUEST['UInfo'];
      $params     = array(
         "Action"       => "View",
         "IType"        => 'G_Species_List',
         "Current"      => $list['Value'],
         "ViewName"     => "List Members",
         "NoPaging"     => true,
         "UInfo"        => $uinfo
      );
      $res = json_decode(post($url,$params));
      unset($res->Interface);
      unset($res->IsAllowedTo);
      unset($res->Alternatives);
      unset($res->Views);
      unset($res->ViewName);
      unset($res->UserPermissions);
      return $res;      
   }
}
class ODS_Species_Output extends DBT {
   protected $tablename = 'ODS_Species_Output';
   protected $displayname = 'Species Data Output';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Species_Output_ID'          => 'Unique Key',
      'ODS_Species_Profile_Section_ID' => '{"DataType":"LINKEDTO","TargetType":"ODS_Species_Profile_Section","TargetField":"ODS_Species_Profile_Section_ID","Current":1,"Inherit":1}',
      'ODS_Species_Output_Type_ID'     => '{"DataType":"LINKEDTO","TargetType":"ODS_Species_Output_Type","TargetField":"ODS_Species_Output_Type_ID"}',
      'Is_Expanded'                    => '{"DataType":"True or False"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Species_Profile_Section_ID','ODS_Species_Output_Type_ID'));   
   }
}

class ODS_Species_Output_Type extends DBT {
   protected $tablename = 'ODS_Species_Output_Type';
   protected $displayname = 'Species Data Output Type';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Species_Output_Type_ID'    => 'Unique Key',
      'Name'                          => '{"DataType":"Short Text","Mandatory":1}',
      'Description'                   => '{"DataType":"Long Text"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   protected $primingdata = array(
      'ODS_Species_Output_Type' => '
"Name","Description"
"Summary","Brief analysis of returned data."
"Species List","Unique list of species showing first and last year recorded and number of records."
"Record List","Full list of returned records showing the grid references and recorders."
"Action","What should be done as a result of data appearing in this section."'
   );
}


class ODS_Search extends DBT {
   protected $tablename = 'ODS_Search';
   protected $displayname = 'Data Search';
   protected $show = array('ODS_Search_Buffer','ODS_Search_Output');
   protected $stats = array();
   protected $alts = array('Layers'=>'ODS_Layer_Catalogue','Servers'=>'ODS_Data_Server','Profiles'=>'ODS_Search_Profile');
   protected $view = '[
      {
         "Name":"Buffers",
         "Data": [
            {
               "Table":"ODS_Search_Buffer"
            }
         ]
      },
      {
         "Name":"Results",
         "Data": [
            {
               "Name":"Reports",
               "Table":"ODS_Search_Output",
               "Show":"Links"
            }
         ]
      }
   ]';
   protected $postprocess = '[
      {
         "Name":"Create Search"         
      },
      {
         "Name":"Create Buffers",
         "Method":"createbuffers"   
      },
      {
         "Name":"Retrieve Species Data",
         "Method":"retrievespeciesdata"         
      },
      {
         "Name":"Process Search Response",
         "Method":"processspeciesdata"   
      },
      {
         "Name":"Filter Species Data",
         "Method":"filterspeciesdata"
      },
      {
         "Name":"Retrieve Site Data",
         "Method":"retrievesitedata"  
      },
      {
         "Name":"Process Site Data",
         "Method":"processsitedata"  
      },
      {
         "Name":"Produce Site Report",
         "Method":"producesitereport"
      }      
   ]';
   protected $domain = 'ods';
   protected $orders = array('Number' => 'ASC');
   protected $columns = array(
      'ODS_Search_ID'            => 'Unique Key',
      'ODS_Search_Profile_ID'    => '{"DataType":"LINKEDTO","TargetType":"ODS_Search_Profile","TargetField":"ODS_Search_Profile_ID","Current":1,"Inherit":1,"Derives":[{"Column":"ODS_Search_Definition_ID","Method":"s2_getcurrentversion","Params":[{"Type":"Static","Value":"ODS_Search_Profile"},{"Type":"Value"},{"Type":"Static","Value":"Present"},{"Type":"Static","Value":"ODS_Search_Definition"}]}]}',
      'ODS_Search_Definition_ID' => '{"DataType":"LINKEDTO","TargetType":"ODS_Search_Definition","TargetField":"ODS_Search_Definition_ID","Current":1,"Inherit":1}',
      'Reference'                => '{"DataType":"Short Text"}',
      'Submitted'                => '{"DataType":"Date","NoList":1}',
      'Boundary'                 => '{"DataType":"Map Data"}',
      'Number'                   => '{"DataType":"Count","CountOfType":"ODS_Search_Profile","CountOfField":"ODS_Search_Profile_ID"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getshortname() {
      return (isset($this->data['Reference']) && ($this->data['Reference'] |= ""))
         ?$this->data['Reference']
         :$this->derivename(array('ODS_Search_Definition_ID','Submitted','Number'));
   }
   function getname() {
      return $this->derivename(array('Reference','ODS_Search_Definition_ID','Submitted','Number'));
   }
   // S T A R T P R O C E S S
   // S T E P 1
   function createbuffers() {
      global $epsg_native;
      $step = 2;
      $proc = $this->getprocessor();
      $proc->completestep($step-1);
      $ospc  = "ODS_Search_Profile";
      $osdc  = "ODS_Search_Definition";
      $ospbc = "ODS_Search_Profile_Buffer";
      $osbc  = "ODS_Search_Buffer"; 
      $poly = $this->data['Boundary'];
      $success = true;
      if (isset($poly)) {
         $osd = new $osdc();
         $ospb = new $ospbc();
         list($scol,$tcol) = $this->getlinkedcolumn($osdc);
         $sdid = $this->data[$scol]; 
         $osd->loaddata($sdid);
         $ospb = new $ospbc();
         list($scol,$tcol) = $ospb->getlinkedcolumn($osdc);
         $pbuffers = $ospb->select($scol,$sdid);
         $ssteps = count($pbuffers);
         $scompleted = 0;
         $pstep = $proc->createstep('Create Buffers',$step,$ssteps);
         $psid = $pstep->getid();
         $proc->setstep($psid);
         foreach($pbuffers as $bufnum => $buf) {
            $bsize = $buf['Buffer_Size'];
            $snum = $buf['Number'];
            $this->createbuffer($poly,$bsize,$snum);
            $scompleted++;
            $pstep->completestep($completed);
         }
         $pstep->complete();
      }
      $proc->completestep($step);
      return 1;
   }
   // S T E P 1.X
   function createbuffer($poly,$bsize,$snum) {
      global $epsg_native;
      $osbc  = "ODS_Search_Buffer"; 
      if ($bsize > 0) {
         $expr = new Zend_Db_Expr("st_astext(st_transform(st_buffer(st_transform(st_geomfromtext('${poly}',4326),${epsg_native}),${bsize}),4326))");
         $sel = "SELECT $expr AS buffer";
         //$buffer = $this->dbhandle->fetchOne($sel);
         $res = pg_query($this->dbpghandle,$sel);
         $res = pg_fetch_array($res);
         $buffer = $res['buffer'];
      } else $buffer = $poly;
      $osb = new $osbc();
      $data = array(
         'ODS_Search_ID'                  => $this->getid(),
         'Buffer_Size'                    => $bsize,
         'Buffer'                         => $buffer,
         'Number'                         => $snum   
      );
      $osb->setdata($data);
      $osb->insert();             
   }
   // S T E P 2.1
   function getlargestbuffer() {
      $osbc = "ODS_Search_Buffer";
      $osb = new $osbc();
      $buffers   = $osb->select($this->getpk(),$this->getid());
      // Start with un-buffered search area
      $boundary = $this->data['Boundary'];
      $number = 0;
      $buffer = 0;
      // Set boundary to largest buffer
      foreach($buffers as $bufnum => $buf) {
         $bufid      = $buf[$osb->getpk()];
         $bsize      = $buf['Buffer_Size'];
         if ($bsize > $buffer) {
            $buffer = $bsize;
            $boundary   = $buf['Buffer'];
            $number     = $buf['Number'];
         }
      }
      return array($boundary,$number);
   }
   // S T E P 2.2
   function getdaterange($sdid) {
      $osdc  = "ODS_Search_Definition";
      $osplc = "ODS_Search_Profile_Limit";
      $ospl = new $osplc();
      list($scol,$tcol) = $ospl->getlinkedcolumn($osdc);
      $sdid = $this->data[$scol]; 
      $ospl->loaddata($sdid,$scol);
      $ldata = $ospl->getdata();
      $limit = $ldata['Last_X_Years'];
      $now = gmmktime();
      $ny = intval(date("Y",$now));
      $nm = intval(date("m",$now));
      $nd = intval(date("d",$now));
      $data_to = date("d/m/Y",$now);
      $data_from = date("d/m/Y",mktime(0,0,0,1,1,($ny-$limit))); 
      return array($data_from,$data_to);
   }
   // S T E P 2
   function retrievespeciesdata() {
      global $configsettings,$server,$client,$folder;
      $step = 3;
      $proc = $this->getprocessor();
      $proc->completestep($step-1);
      $sid = $this->getid();
      $ordc  = "ODS_Raw_Data";
      $ospc  = "ODS_Search_Profile";
      $osdc  = "ODS_Search_Definition";
      $ospsc = "ODS_Search_Profile_Source";
      // before retrieving the raw data 
      $rdo = new $ordc();
      $rdo->delete($this->getpk(),$this->getid());
   
      $osd  = new $osdc();
      $osps = new $ospsc();
      list($scol,$tcol) = $this->getlinkedcolumn($osdc);
      $sdid = $this->data[$scol]; 
      $osd->loaddata($sdid);
      $ddata = $osd->getdata();
      $sources = $ddata['Sources']->ListData;
      $ssteps = count($sources);
      $pstep = $proc->createstep('Retrieve Species Data',$step,$ssteps+2);
      list($data_from,$data_to) = $this->getdaterange($sdid);
      $pstep->completestep(1,'Set Date Limits');      
      list($boundary,$number) = $this->getlargestbuffer();
      $pstep->completestep(2,'Set Search Boundary');      
      $scompleted = 2;
   
      foreach($sources as $snum => $src) {
         $this->getspeciessourcedata($src,$boundary,$data_from,$data_to);
         $scompleted++;
         $pstep->completestep($scompleted);                              
      }
      
      $pstep->complete();      
      $proc->completestep($step);
      return 1;         
   }
   // S T E P 2.X
   function getspeciessourcedata($src,$boundary,$data_from,$data_to) {
      global $configsettings,$server,$client,$folder;
      $ordc  = "ODS_Raw_Data";
      $ospc  = "ODS_Search_Profile";
      $osdc  = "ODS_Search_Definition";
      $ospsc = "ODS_Search_Profile_Source";
      // before retrieving the raw data 
      $rdo = new $ordc();
      $osd  = new $osdc();
      $osps = new $ospsc();
      
      $osps->loaddata($src->Id);
      $srcdata = $osps->getdata();
      $source = $srcdata['Type'];
      $creds = $configsettings[$source]; 
               
      $params     = array(
         "Action"       => "GetSpeciesList",
         "Source"       => $source,
         "Boundary"     => $boundary,
         "Start_Date"   => $data_from,
         "End_Date"     => $data_to,
         "Sections"     => '[{"Name":"Records","Output_Type":"Records"}]'
      );

      foreach($creds as $i => $p) $params["CR".($i+1)] = $p;
      $svr = "${server}/r6";
      $tgt = "/species2json.php";
   
      $cres = $this->sendrequest($svr,$tgt,$params);

      if (isset($cres)) {
         $fname = "ODS_Search_${sid}_${source}_SpeciesData.json";
         $type = 'text/json';
         $rdid = $this->saveresponse($fname,$type,$cres);
      }
      return 1;
   }
   // S T E P 3
   function processspeciesdata() {
      global $epsg_native;
      $step = 4;
      $proc = $this->getprocessor();
      $proc->completestep($step-1);
      chdir('..');
      $rdo = new ODS_Raw_Data();
      $srco = new ODS_Search_Record();
      $srco->delete($this->getpk(),$this->getid());
      $rawdatas = $rdo->select($this->getpk(),$this->getid());
      $ssteps = count($rawdatas);
      $scompleted = 0;
      $pstep = $proc->createstep('Process Species Data',$step,$ssteps);
      $psid = $pstep->getid();
      foreach($rawdatas as $rnum => $rdata) {
         $pstepcur = $scompleted + 1;
         $this->processspeciessource($rdata,$pstepcur,$psid);
         $scompleted++;
         $pstep->completestep($scompleted,'Processed source: '.($rnum+1));
      }
      $rdo->delete($this->getpk(),$this->getid());
      
      $pstep->complete();
      $proc->completestep($step);
      return 1;
   }
   // S T E P 3.X
   function processspeciessource($rdata,$pstepcur,$psid) {
      $proc = $this->getprocessor();
      $src = "ODS_Search_Record";
      $srco = new $src();
      
      $fid = $rdata['S2_File_ID'];
      $fid = (is_object($fid))?$fid->Id:$fid;
      $f = new S2_File();
      $f->loaddata($fid);
      $fdata = $f->getdata();
      $path = $fdata['Data'];
      $cres = s2_getfile($path);
      $jres = json_decode($cres);
      if (!$srco->exists()) $srco->create();
      if (isset($jres->Tables) && isset($jres->Tables->Records)) {
         $records = count($jres->Tables->Records);
         $ro = null;
         $tenpcent = (intval($records/10)>0)?intval($records/10):1;
         $istepcur = 0;
         $istep = $proc->createstep("Source $pstepcur Records",$pstepcur,10,$psid);
         $recnum = 0;
         foreach ($jres->Tables->Records as $record) {
            $rid = $this->addrecord($record);
            // update at 10% marker
            $recnum++;
            $istepcur = intval($recnum/$tenpcent);
            if (($istepcur > 0) && (($recnum%$tenpcent)==0)) $istep->completestep($istepcur,"Processed $recnum out of $records records.");
         }
         $istep->complete();
      }
      $f->delete($f->getpk(),$fid);
      return 1;   
   }
   // S T E P 3.X.Y
   function addrecord($record) {
      global $epsg_native;
      $name = $record->scientificname;
      $tvk = $record->gatewaykey;
      $jo = new stdClass();
      $jo->Name = $name;
      $jo->Value = $tvk;
      $tid = json_encode($jo);
      $wkt = null;
      $ngr = new NationalGridReference();
      $ngr->fromString($record->spatialref);
      $gr = conv_ngr_to_ings($ngr);
      $bb = new BoundingBox(
         ($gr->northing + $gr->accuracy),
         ($gr->easting + $gr->accuracy),
         $gr->northing,
         $gr->easting
      );
      $oswkt = $bb->asWKT();
      
      $expr = new Zend_Db_Expr("st_astext(st_transform(st_geomfromtext('${oswkt}',${epsg_native}),4326))");
      $sel = "SELECT $expr AS buffer";
      $wkt = $this->dbhandle->fetchOne($sel);
      
      //$wkt = p4p_wkt_conv($oswkt,'EPSG:27700','EPSG:4326');
      $ro = array(
         'ODS_Search_ID'         => $this->getid(),
         'G_Species_ID'          => $tid,
         'Place_Name'            => $record->location,
         'Grid_Reference'        => $record->spatialref,
         'Boundary'              => $wkt,
         'Recorder'              => $record->recordedby,
         'Start_Date'            => $record->sdate,
         'End_Date'              => $record->edate,
         'Comments'              => ((isset($record->comments))?$record->comments:null),
         'Details'               => ((isset($record->measurement))?"$record->measurement $record->measure":null),
         'Source'                => $record->custodian,
         'Survey'                => urlencode($record->survey)
      );
      $srco = new ODS_Search_Record();
      $srco->setdata($ro);
      $srco->insert();         
      return $srco->getid();
   }
   // S T E P 4
   function filterspeciesdata() {
//error_reporting(E_ALL);
      $step = 5;
      $proc = $this->getprocessor();
      $proc->completestep($step-1);
      $bc  = "ODS_Search_Buffer";
      $pc  = "ODS_Search_Profile";
      $dc  = "ODS_Search_Definition";
      $psc = "ODS_Species_Profile_Section";
      $pbc = "ODS_Search_Profile_Buffer";
      $fc  = "ODS_Species_Filter";
      $oc  = "ODS_Species_Output";
      $otc = "ODS_Species_Output_Type";
      $rc  = "ODS_Search_Record";
      $b   = new $bc();
      $d   = new $dc();
      $ps  = new $psc();
      $pb  = new $pbc();
      $f   = new $fc();
      $o   = new $oc();
      $ot  = new $otc();
      $r   = new $rc();
      $dlnk = $this->getlinkedcolumn($dc);
      $sections = $ps->select($dlnk[1],$this->data[$dlnk[0]]);
//print_r($sections);
      $blnk = $b->getlinkedcolumn($this->tablename);
      $buffers = $b->select($blnk[0],$this->data[$blnk[1]]);
//print_r($buffers);
      $ssteps = count($sections);
      $scompleted = 0;
      $pstep = $proc->createstep('Filter Species Data',$step,$ssteps);
   
      foreach($sections as $secnum => $psd) {
         $pstepcur = $scompleted + 1;
         $psid   = $psd[$ps->getpk()];
         $return = $psd['Return'];
         $blnk = $ps->getlinkedcolumn($pbc);
         $boundary = $this->data['Boundary'];
         $pbid = (isset($psd[$blnk[0]])
                  &&isset($psd[$blnk[0]]->Id))
                  ?$psd[$blnk[0]]->Id:0;
         if ($pbid > 0) {
            $pb->loaddata($pbid);
            $pbd = $pb->getdata();
            $bsize = $pbd['Buffer_Size'];
            $islargest = true;
            foreach($buffers as $bx) {
               if ($bx['Buffer_Size'] == $bsize) {
                  $bd = $bx;
                  $boundary = $bd['Buffer'];
               } else if ($bx['Buffer_Size'] > $bsize) $islargest = false;              
            }
         }
         
         $srecords = $r->select($this->getpk(),$this->getid());
         $istep = $proc->createstep("Section $pstepcur Filtering Records",1,4,$pstep->getid());
         $reccount = count($srecords);   
         if ($reccount>0) {
            if (!$islargest) $orecords = $this->spatialfilter($boundary,$srecords,$istep->getid());
            else $orecords = $srecords;
         }
         $istep->completestep(1,'Completed Spatial Filter');
         if (count($orecords) > 0) {
            $sectionlists = array();
            $flnk = $f->getlinkedcolumn($psc);
            $filters = $f->select($flnk[0],$psid);
            foreach($filters as $fi => $filter) {
               $list = $filter['List'];
               $listname = $list['Name'];
               $lst = $f->getlist($list);
               $lmem = $lst->LinkedData[0];
               $lmembers = $lmem->ListData;
               $ldata = $lst->Data;
               $lst = new stdClass();
               $lst->Cache = $list;
               $lst->Data = $ldata;
               $lst->Members = $lmembers;
               $gcids = array();
               foreach($lmembers as $lx => $lmember) {
                  $gcid = $lmember->G_Species_ID->Id;
                  if (!in_array($gcid,$gcids)) $gcids[] = $gcid; 
               }
               $lst->IDs = $gcids;
               $sectionlists[$listname] = $lst;
            }
            $species = array();
            $speciesrcount = array();
            $ismemberof = array();
            $firstyears = array();
            $lastyears = array();
            $lists = array();
            $listspecies = array();
            $listscount = array();
            $listrcount = array();
            $listscores = array();
            $rle = new ODS_Record_List_Entry();
            if (!$rle->exists())$rle->create();
            $reccount = count($orecords);
            $tenpcent = (intval($reccount/10)>0)?intval($reccount/10):1;
            $iistepcur = 0;
            $iistep = $proc->createstep("Section $pstepcur List Filter",2,10,$istep->getid());
            $rnum = 0;
            foreach($orecords as $rid => $rec) {
               $rid = $rec[$r->getpk()];
               $gcid = intval($rec['G_Species_ID']->Id);
               // update record count for species
               if (!isset($species[$gcid])) {
                  $species[$gcid] = $rec['G_Species_ID'];
                  $speciesrcount[$gcid] = 1;
               } else $speciesrcount[$gcid]++;     
               // update recorded date range for species
               $year = intval(substr($rec['Start_Date'],-4));
               if (!isset($firstyears[$gcid])) $firstyears[$gcid] = $year;
               else $firstyears[$gcid] = min($year,$firstyears[$gcid]); 
               if (!isset($lastyears[$gcid])) $lastyears[$gcid] = $year;
               else $lastyears[$gcid] = max($year,$lastyears[$gcid]); 
               // update list membership data
               foreach($sectionlists as $listname => $lst) {
                  $gcids  = $lst->IDs;
                  if (in_array($gcid,$gcids)) {
                     $ldata  = $lst->Data;
                     $labbr  = $ldata->Abbreviation;
                     $lcache = $lst->Cache;
                     $lists[$listname] = $lst->Cache; 
                     // initialise ismemberof array for species if it doesn't exist
                     if (!isset($ismemberof[$gcid])) $ismemberof[$gcid] = array();
                     if (!in_array($labbr,$ismemberof[$gcid])) $ismemberof[$gcid][] = $labbr;
                     // initialise list counting if first list member
                     if (!isset($listspecies[$listname])) {
                        $listspecies[$listname] = array();
                        $listscount[$listname] = 0;
                        $listrcount[$listname] = 0;
                        $listscores[$listname] = 0;
                     }
                     // check whether species has been recorded for list already
                     if (!in_array($gcid,$listspecies[$listname])) {
                        $listspecies[$listname][] = $gcid;
                        $listscount[$listname]++;
                        $score = 1;
                        foreach($lst->Members as $mid => $lmem) {
                           if($lmem->G_Species_ID->Id == $gcid) $score = (isset($lmem->Score))?$lmem->Score:1;
                        }
                        $listscores[$listname] += $score;
                     }
                     $listrcount[$listname]++;
                  }                                                           
               }
               // add to records list depending on filter settings
               if ($return == 'All Species' || (isset($ismemberof[$gcid]) && count($ismemberof[$gcid]) > 0)) {
                  $memberof = ($return == 'All Species')?null:join(', ',$ismemberof[$gcid]);
                  $data = array(
                     'ODS_Search_ID'                  => $this->getid(),
                     'ODS_Species_Profile_Section_ID' => $psid,
                     'ODS_Search_Record_ID'           => $rid, 
                     'Member_Of'                      => $memberof 
                  );
                  $rle->setdata($data);
                  $rle->insert(); 
               }
               $rnum++;
               $iistepcur = intval($rnum/$tenpcent);
               if (($iistepcur > 0) && (($rnum%$tenpcent)==0)) $iistep->completestep($iistepcur,"Processed $rnum of $reccount records.");                          
            }
            $iistep->complete();
            $istep->completestep(2,'Created Record List');
            $numlists = count($lists);
            $iistep = $proc->createstep('Create Summary Data',3,$numlists,$istep->getid());
            // add summary data
            if ($numlists>0) {
               $sse = new ODS_Species_Summary_Entry();
               if (!$sse->exists())$sse->create();
               $listnum = 0;
               foreach ($lists as $listname => $lc) {
                  $scount = $listscount[$listname];
                  $rcount = $listrcount[$listname];
                  $score  = $listscores[$listname];
                  $data = array(
                     'ODS_Search_ID'                  => $this->getid(),
                     'ODS_Species_Profile_Section_ID' => $psid,
                     'List'                           => $lc['G_Cache_ID'],
                     'Species_Count'                  => $scount,
                     'Record_Count'                   => $rcount,
                     'Score'                          => $score 
                  );
                  $sse->setdata($data);
                  $sse->insert();
                  $listnum++;
                  $iistep->completestep($listnum,"Summarized $listnum of $numlists lists.");
               }
            }
            $iistep->complete();
            $istep->completestep(3,'Created Summary Data');
            // add species list data
            $sle = new ODS_Species_List_Entry();
            if (!$sle->exists())$sle->create();
            $numspecies = count($species);
            $tenpcent = (intval($numspecies/10)>0)?intval($numspecies/10):1;
            $iistep = $proc->createstep('Create Species List',4,10,$istep->getid());
            $snum = 0;
            foreach ($species as $sdata) {
               $gcid = $sdata->Id;
               if ($return == 'All Species' || (isset($ismemberof[$gcid]) && count($ismemberof[$gcid]) > 0)) {
                  $fyear  = $firstyears[$gcid];
                  $lyear  = $lastyears[$gcid];
                  $rcount = $speciesrcount[$gcid];
                  $memberof = (!isset($ismemberof[$gcid]))?null:join(', ',$ismemberof[$gcid]);
                  $data = array(
                     'ODS_Search_ID'                  => $this->getid(),
                     'ODS_Species_Profile_Section_ID' => $psid,
                     'G_Species_ID'                   => $gcid,
                     'First_Year'                     => $fyear,
                     'Last_Year'                      => $lyear,
                     'Record_Count'                   => $rcount,
                     'Member_Of'                      => $memberof    
                  );
                  $sle->setdata($data);
                  $sle->insert();
               } 
               $snum++;
               $iistepcur = intval($snum/$tenpcent);
               if (($iistepcur > 0) && (($snum%$tenpcent)==0)) $iistep->completestep($iistepcur,"Processed $snum of $numspecies species.");
            }
            $iistep->complete();
         }
         $scompleted++;
         $istep->complete();
         $pstep->completestep($scompleted);                      
      }
      
      $pstep->complete();
      $proc->completestep($step);
      return 1;
   }
   function spatialfilter($boundary,$srecords,$parentstepid) {
      $proc = $this->getprocessor();
      $reccount = count($srecords);
      $tenpcent = (intval(count($srecords)/10)>0)?intval(count($srecords)/10):1;
      $iistep = $proc->createstep("Section $pstepcur Spatial Filter",1,10,$parentstepid);
      $orecords = array();
      foreach($srecords as $rid => $rec) {
         $poly = $rec['Boundary'];
         $expr = new Zend_Db_Expr("st_intersects(st_geomfromtext('$poly',4326),st_geomfromtext('$boundary',4326))");
         //$expr = $expr->__toString();
         $sel = "SELECT $expr AS isin";
         //$intersects = $this->dbhandle->fetchOne($sel);
         $res = pg_query($this->dbpghandle,$sel);
         $res = pg_fetch_array($res);
         // pg returns t or f as strings rather than 1 or 0;
         $intersects = ($res['isin'] == 't');
         if ($intersects) $orecords[] = $rec;   
         $iistepcur = intval($rid/$tenpcent);
         if (($iistepcur > 0) && (($rid%$tenpcent)==0)) $iistep->completestep($iistepcur,"Processed $rid out of $reccount records.");         
      }
      $iistep->complete();
      return $orecords;
   }

   function retrievesitedata() {
      global $configsettings,$server,$client,$folder;
      $step = 6;
      $proc = $this->getprocessor();
      $proc->completestep($step-1);
      
      $sid = $this->getid();
      
      $bc  = "ODS_Search_Buffer";
      $pc  = "ODS_Search_Profile";
      $dc  = "ODS_Search_Definition";
      $psc = "ODS_Site_Profile_Section";
      $pbc = "ODS_Search_Profile_Buffer";
      $ltc = "ODS_Layer_Type";
      $lcc = "ODS_Layer_Catalogue";
      $lac = "ODS_Layer_Attributes";
      $slc = "ODS_Server_Layer";
      $dsc = "ODS_Data_Server";
      $sfc = "ODS_Search_Site_File";
      $b   = new $bc();
      $d   = new $dc();
      $ps  = new $psc();
      $pb  = new $pbc();
      $lt  = new $ltc();
      $la  = new $lac();
      $cat = new $lcc();
      $sl  = new $slc();
      $ds  = new $dsc();
      $sf  = new $sfc();
      if (!$sf->exists())$sf->create();
      $dlnk = $this->getlinkedcolumn($dc);
      $sections = $ps->select($dlnk[1],$this->data[$dlnk[0]]);

      $blnk = $b->getlinkedcolumn($this->tablename);
      $buffers = $b->select($blnk[0],$this->data[$blnk[1]]);

      $ssteps = count($sections);
      $scompleted = 0;
      $pstep = $proc->createstep('Retrieve Site Data',$step,$ssteps);
      $psid = $pstep->getid();
      foreach($sections as $secnum => $section) {
         $blnk = $ps->getlinkedcolumn($pbc);
         $wkt = $this->data['Boundary'];
         $pbid = (isset($section[$blnk[0]])
                  &&isset($section[$blnk[0]]->Id))
                  ?$section[$blnk[0]]->Id:0;
         if ($pbid > 0) {
            $pb->loaddata($pbid);
            $bd = $pb->getdata();
            $bsize = $bd['Buffer_Size'];
            foreach($buffers as $bdata) { if ($bdata['Buffer_Size'] == $bd['Buffer_Size']) $wkt = $bdata['Buffer']; }
         }
         $slnk = $ps->getlinkedcolumn($ltc);
         $ltype = $section[$slnk[0]]->Id;
         $clnk = $cat->getlinkedcolumn($ltc);
         $layers = $cat->select($clnk[0],$ltype);
//print_r($layers); exit;
         $lcompleted = 0;
         $lsteps = count($layers);
         $lstep = $proc->createstep('Retrieve Layer Data',1,$lsteps,$psid);

         foreach($layers as $layer) {
//print_r($layer); exit;
            $this->retrievesitesourcedata($layer,$wkt);
            $lcompleted++;
            $lstep->completestep($lcompleted);
         }

         $scompleted++;
         $pstep->completestep($scompleted);                              
      }
      $pstep->complete();      
      $proc->completestep($step);
      return 1;
   }
   function retrievesitesourcedata($layer,$wkt) {
      global $configsettings,$server,$client,$folder;
      $k = new KLib($this->getsid());
      $keys = json_decode($k->getkeys());
      
      $bc  = "ODS_Search_Buffer";
      $pc  = "ODS_Search_Profile";
      $dc  = "ODS_Search_Definition";
      $psc = "ODS_Site_Profile_Section";
      $pbc = "ODS_Search_Profile_Buffer";
      $ltc = "ODS_Layer_Type";
      $lcc = "ODS_Layer_Catalogue";
      $lac = "ODS_Layer_Attributes";
      $slc = "ODS_Server_Layer";
      $dsc = "ODS_Data_Server";
      $sfc = "ODS_Search_Site_File";
      $b   = new $bc();
      $d   = new $dc();
      $ps  = new $psc();
      $pb  = new $pbc();
      $lt  = new $ltc();
      $la  = new $lac();
      $cat = new $lcc();
      $sl  = new $slc();
      $ds  = new $dsc();
      $sf  = new $sfc();
      
      $labbrev = $layer['Abbreviation'];
      $llnk = $cat->getlinkedcolumn($slc);
      $slid = $layer[$llnk[1]]->Id; 
      $sl->loaddata($slid);
      $slayer = $sl->getdata();
      $feature = $slayer['Feature_Name'];
      $slnk = $sl->getlinkedcolumn($dsc);
      $ds->loaddata($slayer[$slnk[0]]->Id);
      $sdata = $ds->getdata();

      $ogc = new OGCFilter();
      $alnk = $la->getlinkedcolumn($lcc); 
      $la->loaddata($layer[$alnk[1]]);
      $atts = $la->getdata();
      $geom = $atts['Geometry_Attribute'];
      $ftr_s = $ogc->define('Overlaps',array($geom,$wkt));
      if ($layer['Return'] == 'Only Live Sites') {
         $ftr_d = $ogc->define('PropertyIsEqualTo',array($atts['Status_Attribute'],$atts['Live_Status']));
         $ftr_c = $ogc->define('And',array($ftr_d,$ftr_s));
         $opts =  $ogc->define('Filter',array($ftr_c));
      } else $opts = $ogc->define('Filter',array($ftr_s));
      $params     = array(
         'Action'    => 'GetFeature',
         'Feature'   => $feature,
         'URL'       => $sdata['URL'],
         'CR1'       => decrypt($sdata['User_Name'],$keys->k1,$keys->k2),
         'CR2'       => decrypt($sdata['Password'],$keys->k1,$keys->k2), 
         'Options'   => addslashes(json_encode($opts))
      );

      $svr = "${server}/r6";
      $tgt = "/wfs2json.php"; 
      $cres = $this->sendrequest($svr,$tgt,$params);

      if (isset($cres) && ($cres != "null")) {
         $fname = "ODS_Search_${sid}_${labbrev}_SiteData.json";
         $type = 'text/json';
         $rdid = $this->saveresponse($fname,$type,$cres);
      }
            
      return 1;
   }
   function processsitedata() {
      global $folder;
      $step = 7;
      $proc = $this->getprocessor();
      $proc->completestep($step-1);
      chdir('..');
      
      $fc  = "S2_File";
      $pc  = "ODS_Search_Profile";
      $dc  = "ODS_Search_Definition";
      $rdc = "ODS_Raw_Data";
      $sfc = "ODS_Search_Site_File";
      $sec = "ODS_Site_Summary_Entry";
      $lec = "ODS_Site_List_Entry";
      $psc = "ODS_Site_Profile_Section";
      $ltc = "ODS_Layer_Type";
      $lcc = "ODS_Layer_Catalogue";
      $lac = "ODS_Layer_Attributes";
      $slc = "ODS_Server_Layer";
      $f   = new $fc();
      $p   = new $pc();
      $d   = new $dc();
      $rd  = new $rdc();
      $sf  = new $sfc();
      $se  = new $sec();
      $le  = new $lec();
      $ps  = new $psc();
      $lt  = new $ltc();
      $cat = new $lcc();
      $la  = new $lac();
      $sl  = new $slc();
      
      if (!$se->exists())$se->create();
      if (!$le->exists())$le->create();
      $se->delete($this->getpk(),$this->getid());
      $le->delete($this->getpk(),$this->getid());
      
      $layerdata = array();
      $rawdatas = $rd->select($this->getpk(),$this->getid());
      foreach($rawdatas as $rnum => $rdata) {
         $pstepcur = $scompleted + 1;
         try {
            $fid = $rdata['S2_File_ID'];
            $fid = (is_object($fid))?$fid->Id:$fid;
            $f = new S2_File();
            $f->loaddata($fid);
            $fdata = $f->getdata();
            $path = $fdata['Data'];
            $cres = s2_getfile($path);
            if ($cres != "null") {
            $jres = json_decode($cres);
            if (is_object($jres) && $jres->status == 1) {
               foreach($jres->data as $feature => $data) {
                  $lfeature = strtolower($feature);
                  $layerdata[$lfeature] = $data;
               }
            }
            }
         } catch(Exception $e) {}
      }
      $dlnk = $this->getlinkedcolumn($dc);
      $sections = $ps->select($dlnk[1],$this->data[$dlnk[0]]);
      $ssteps = count($sections);
      $scompleted = 0;
      $pstep = $proc->createstep('Read Site Data',$step,$ssteps);
      $psid = $pstep->getid();
      foreach($sections as $secnum => $section) {
         $secid = (is_object($section[$ps->getpk()]))?$section[$ps->getpk()]->Id:$section[$ps->getpk()];
         $slnk = $ps->getlinkedcolumn($ltc);
         $ltype = $section[$slnk[0]]->Id;
         $clnk = $cat->getlinkedcolumn($ltc);
         $layers = $cat->select($clnk[0],$ltype);
         $lcompleted = 0;
         $lsteps = count($layers);
         $lstep = $proc->createstep('Read Layer Data',1,$lsteps,$psid);
         $lsumarea = array();
         $lsumperm = array();
         $lsumnumb = array();
         foreach($layers as $layer) {
            $layid = $layer[$cat->getpk()];
            $labbrev = $layer['Abbreviation'];
            $llnk = $cat->getlinkedcolumn($slc);
            $slid = $layer[$llnk[1]]->Id; 
            $sl->loaddata($slid);
            $slayer = $sl->getdata();
            $feature = $slayer['Feature_Name'];
            $lfeature = strtolower($feature);
            $alnk = $la->getlinkedcolumn($lcc); 
            $la->loaddata($layer[$alnk[1]]);
            $atts = $la->getdata();
            
            if (isset($layerdata[$lfeature])) {
               foreach($layerdata[$lfeature] as $fnum => $fdata) {
//print "<h2>$fnum</h2><pre>"; print_r($fdata); print "</pre>";
                  $geomcol = $fdata->geom;
                  $poly = $fdata->data->$geomcol->wkt;
                  
                  $measures = $this->getpolygonmeasurements($poly);
                  // convert area from m2 to hectares to 2 decimal places
                  $area = floor(floatval($measures->Area) / 100) / 100;
                  // limit perimeter to no decimal places 
                  $peri = round(floatval($measures->Perimeter));
                   
                  foreach($atts as $atttype => $attname) {
                     if (isset($attname)) {
                        $attname = strtolower($attname); 
                        switch($atttype) {
                        case 'Name_Attribute':     {$name      = (isset($fdata->data->$attname))?$fdata->data->$attname:null;}break;
                        case 'Code_Attribute':     {$code      = (isset($fdata->data->$attname))?$fdata->data->$attname:null;}break;
                        case 'Status_Attribute':   {$status    = (isset($fdata->data->$attname))?$fdata->data->$attname:null;}break;
                        case 'Update_Attribute':   {$update    = (isset($fdata->data->$attname))?$fdata->data->$attname:null;}break;
                        case 'Split_On_Attribute': {$list_as   = (!isset($attname))?$labbrev:((isset($fdata->data->$attname))?$fdata->data->$attname:$labbrev); }break;
                        }
                     }
                  } 
                  
                  if (!isset($lsumarea[$list_as])) $lsumarea[$list_as] = $area;
                  else $lsumarea[$list_as] += $area;
                  if (!isset($lsumperm[$list_as])) $lsumperm[$list_as] = $peri;
                  else $lsumperm[$list_as] += $peri;
                  if (!isset($lsumnumb[$list_as])) $lsumnumb[$list_as] = 1;
                  else $lsumnumb[$list_as] += 1;
                  
                  $sitelistdata = array(
                     'ODS_Search_ID'               => $this->getid(),
                     'ODS_Site_Profile_Section_ID' => $secid,
                     'Category'                    => $list_as,
                     'Type'                        => $ltype,
                     'Layer'                       => $layid,
                     'Boundary'                    => $poly,
                     'Site_Name'                   => $name,                   
                     'Site_Code'                   => $code,
                     'Status'                      => $status,
                     'Updated'                     => $update,
                     'Hectares'                    => $area,
                     'Perimeter'                   => $peri
                  );
                  $le->setdata($sitelistdata);
                  $le->insert();
               }
               foreach ($lsumnumb as $list_as => $val) {
                  $sitesumdata = array(
                     'ODS_Search_ID'               => $this->getid(),
                     'ODS_Site_Profile_Section_ID' => $secid,
                     'Category'                    => $list_as,
                     'Type'                        => $ltype,
                     'Layer'                       => $layid,
                     'Sites'                       => $lsumnumb[$list_as],
                     'Total_Hectares'              => $lsumarea[$list_as],
                     'Total_Perimeter'             => $lsumperm[$list_as]
                  );
                  $se->setdata($sitesumdata);
                  $se->insert();
               }
            }
            $lcompleted++;
            $lstep->completestep($lcompleted,"Read layer: $lcompleted");
         }
      }
      $pstep->complete();
      $proc->completestep($step);
      return 1;
   }
   function producesitereport() {
      global $folder;
      $step = 8;
      $proc = $this->getprocessor();
      $proc->completestep($step-1);
      $pstep = $proc->createstep('Produce Report',$step,3);
      $psid = $pstep->getid(); 

      $sc = $this->tablename;
      $set = new stdClass();
      $sid = new stdClass();
      $sid->Current = $this->getid();
      $set->$sc = $sid; 
      
      $pc   = "ODS_Search_Profile";
      $dc   = "ODS_Search_Definition";
      $oc   = "ODS_Species_Output";
      $psc  = "ODS_Species_Profile_Section";
      $otc  = "ODS_Species_Output_Type";
      $rc   = "ODS_Search_Record";
      $ssec = "ODS_Species_Summary_Entry";
      $slec = "ODS_Species_List_Entry";
      $rlec = "ODS_Record_List_Entry";
      $soc  = "ODS_Search_Output";
      $bc   = "ODS_Search_Buffer";
      $ssac = "ODS_Species_Section_Action";

      $d    = new $dc();
      $o    = new $oc();
      $ps   = new $psc();
      $ot   = new $otc();
      $r    = new $rc();
      $sse  = new $ssec();
      $sle  = new $slec();
      $rle  = new $rlec();
      $b    = new $bc();
      $ssa  = new $ssac();
      
      $blnk = $b->getlinkedcolumn($this->tablename);
      $buffers = $b->select($blnk[0],$this->data[$blnk[1]]);
   
      $dlnk = $this->getlinkedcolumn($dc);
      $sections = $ps->select($dlnk[1],$this->data[$dlnk[0]]);
//print_r($sections);
      $sname = $this->getname();
      $date = date("d/m/Y",time());      
      $html = "<div id='s2rep_data-container'><h1 class='s2rep_search-name'>Search: ${sname} ~ Run:${date}</h1><hr/><div id='s2rep_data'>";
      $html.= "<h1>Species Data</h1>";
      
      $ssteps = count($sections);
      $scompleted = 0;
      $istep = $proc->createstep('Produce Species Report',$step,$ssteps,$psid);
      
      foreach($sections as $secnum => $psd) {
         $psecid    = $psd[$ps->getpk()];
         $secname = $psd['Name'];
         $psecnum = $secnum + 1;
         $html   .= "<h2>Section ${psecnum}: ${secname}</h2><div id='s2rep_sec-${secnum}' class='s2rep_section-container'>";
         $return  = $psd['Return'];
         
         $fc  = "ODS_Species_Filter";
         $f = new $fc();
         $flnk = $f->getlinkedcolumn($psc);
         $filters = $f->select($flnk[0],$psecid);
         $ex = (!count($filters)>0)?array('Member_Of'):array(); 
               
         $olnk    = $o->getlinkedcolumn($psc);
         $otlnk   = $o->getlinkedcolumn($otc);
         $outputs = $o->select($olnk[0],$psecid);
         $ohtm = null;
            
         $flag = false;
         $act = null; $sum = null; $lst = null; $rec = null;            
         foreach($outputs as $oind => $output) {
            $otype = $output[$otlnk[0]]->Name;
            $expand = ($output['Is_Expanded'])?'expand':'collapse';
            $ohtm = null;
            
            switch($otype) {
            case 'Action': {
               $ohtm = "";
               $lnk  = $ssa->getlinkedcolumn($psc);
               $rows = $ssa->select($lnk[0],$psecid,$set);
               foreach($rows as $actid => $act) {
                  $st = $act['ODS_Severity_Type_ID'];
                  $icon = (isset($st->Icon))?"<img class='s2icon' alt=' :$st->Name' src='$st->Icon'/>":null;
                  if ($act['Is_Default']) $ohtm .= "<div class='s2action'><h4>".$icon.$act['Name']."</h4><p>".$act['Description']."</p></div>";           
               }
               if ($ohtm == "") unset($ohtm);
            }break;
            case 'Summary': {
               $lnk  = $sse->getlinkedcolumn($psc);
               $rows = $sse->select($lnk[0],$psecid,$set);
               if (count($rows)>0) {$flag = true; $ohtm = $sse->tohtml($rows);}                                       
            }break;
            case 'Species List': {
               $lnk  = $sle->getlinkedcolumn($psc);
               $rows = $sle->select($lnk[0],$psecid,$set);
               if (count($rows)>0) {$flag = true; $ohtm = $sle->tohtml($rows,$ex);}
            }break;
            case 'Record List': {
               $lnk  = $rle->getlinkedcolumn($psc);
               $rows = $rle->select($lnk[0],$psecid,$set);
               foreach($rows as $rleid => $rlentry) {
                  $rlnk = $rle->getlinkedcolumn($rc);
                  $rid = $rlentry[$rlnk[0]]->Id; 
                  $r->loaddata($rid,$rlnk[1]);
                  $rdata = $r->getdata();
                  $rdata['Member_Of'] = $rlentry['Member_Of'];
                  $rows[$rleid] = $rdata;
               }
               if (count($rows)>0) {$flag = true; $ohtm = $r->tohtml($rows,$ex);}
            }break;
            }
            $sec = "<h3>$otype</h3><div id='s2rep_sec-${secnum}_out-${oind}' class='s2_collapsible,${expand}'>";
            if (!isset($ohtm)) $sec .= "<p>No data was returned for this section.</p>";  
            else $sec .= $ohtm;
            $sec .= "</div>";
            switch($otype) {
            case 'Action':       {$act = $sec;}break;
            case 'Summary':      {$sum = $sec;}break;
            case 'Species List': {$lst = $sec;}break;
            case 'Record List':  {$rec = $sec;}break;
            }                                                            
         }
         if ($flag) {
            if (isset($psd['Description']) && $psd['Description'] != '') $html .= "<p>".$psd['Description']."</p>";
            if (isset($act)) $html.= $act;
            if (isset($sum)) $html.= $sum;
            if (isset($lst)) $html.= $lst;
            if (isset($rec)) $html.= $rec;
         } else {
            $html .= "<p>No data was returned for this section.</p>";
         }
         
         $html.= "</div>";
         $scompleted++;
         $istep->completestep($scompleted);                                  
      } 
      
      $sse->delete($this->getpk(),$this->getid());
      $sle->delete($this->getpk(),$this->getid());
      $rle->delete($this->getpk(),$this->getid());
           
      $pstep->completestep(1);
      
      $oc   = "ODS_Site_Output";
      $psc  = "ODS_Site_Profile_Section";
      $otc  = "ODS_Site_Output_Type"; 
      $ssec = "ODS_Site_Summary_Entry";
      $slec = "ODS_Site_List_Entry";
      $soc  = "ODS_Search_Output";
      $ssac = "ODS_Site_Section_Action";
      $o    = new $oc();
      $ps   = new $psc();
      $ot   = new $otc();
      $sse  = new $ssec();
      $sle  = new $slec();
      $ssa  = new $ssac();
      
      $sections = $ps->select($dlnk[1],$this->data[$dlnk[0]]);
      $ssteps = count($sections);
      $scompleted = 0;
      $istep = $proc->createstep('Produce Site Report',1,$ssteps,$psid);
      $html.= "<h1>Site Data</h1>";
      
      $ohtm = null;
      foreach($sections as $secnum => $psd) {

         $psecid    = $psd[$ps->getpk()];
         $secname = $psd['Type']->Name;
         $psecnum = $secnum + 1;
         $html   .= "<h2>Section ${psecnum}: ${secname}</h2><div id='s2rep_sec-${secnum}' class='s2rep_section-container'>";  
         
         $olnk    = $o->getlinkedcolumn($psc);
         $otlnk   = $o->getlinkedcolumn($otc);
         $outputs = $o->select($olnk[0],$psecid);

         $flag = false;
         foreach($outputs as $oind => $output) {
            $otype = $output[$otlnk[0]]->Name;
            $expand = ($output['Is_Expanded'])?'expand':'collapse';
            $ohtm = null;
            $rows = array();
            switch($otype) {
            case 'Action': {
               $lnk  = $ssa->getlinkedcolumn($psc);
               $rows = $ssa->select($lnk[0],$psecid,$set);
               foreach($rows as $actid => $act) {
                  $st = $act['ODS_Severity_Type_ID'];
                  $icon = (isset($st->Icon))?"<img class='s2icon' alt=' :$st->Name' src='$st->Icon'/>":null;
                  if ($act['Is_Default']) $ohtm = "<div class='s2action'><h4>".$icon.$act['Name']."</h4><p>".$act['Description']."</p></div>";           
               }
            }break;
            case 'Summary': {
               $lnk  = $sse->getlinkedcolumn($psc);
               $rows = $sse->select($lnk[0],$psecid,$set);
               if (count($rows)>0) {$flag = true; $ohtm = $sse->tohtml($rows);}                                       
            }break;
            case 'Site List': {
               $lnk  = $sle->getlinkedcolumn($psc);
               $rows = $sle->select($lnk[0],$psecid,$set);
               if (count($rows)>0) {$flag = true; $ohtm = $sle->tohtml($rows,$ex);}
            }break;
            }
            $sec = "<h3>$otype</h3><div id='s2rep_sec-${secnum}_out-${oind}' class='s2_collapsible,${expand}'>";
            if (!isset($ohtm)) $sec .= "<p>No data was found to meet section requirements</p>";  
            else $sec .= $ohtm;
            $sec .= "</div>";
            switch($otype) {
            case 'Action':    {$act = $sec;}break;
            case 'Summary':   {$sum = $sec;}break;
            case 'Site List': {$lst = $sec;}break;
            }
         }
         if ($flag) {
            if (isset($psd['Description']) && $psd['Description'] != '') $html .= "<p>".$psd['Description']."</p>";  
            if (isset($act)) $html.= $act;
            if (isset($sum)) $html.= $sum;
            if (isset($lst)) $html.= $lst;
         } else {
            $html .= "<p>No data was returned for this section.</p>";
         }
         
         $html.= "</div>";
         $scompleted++;
         $istep->completestep($scompleted);                                  
      }
      $pstep->completestep(2);
      $html.= "</div></div>";
      
      chdir('..');
      $sid = $this->getid();
      $f = new S2_File();
      if (!$f->exists()) $f->create();
      $d = new stdClass();
      $d->Name = "ODS_Search_${sid}.html";
      $d->Type = "text/html"; 
      $d->Data = $html;
      $d = s2_savefile($d);
      $f->setdata((array)$d);
      $fid = $f->insert();
      $url = 'http://'.$_SERVER['SERVER_NAME']."/${folder}/echofile.php?a=echo&i=$fid";
      $d->Link = $url;
      $f->setdata((array)$d);
      $f->update($f->getpk(),$fid);
      
      $so = new $soc();
      $count = $so->getcount($this->getid());
      if (!$so->exists()) $so->create();
      $d = array(
         $this->getpk() => $this->getid(),
         $f->getpk()    => $fid,
         'Number'       => ($count+1)
      );
      $so->setdata($d);
      $name = $so->getname();
      $f->setdata(array('Name'=>$name));
      $f->update($f->getpk(),$fid);
      $soid = $so->insert();
      $url = 'http://'.$_SERVER['SERVER_NAME']."/${folder}/report?t=$soc&o=$soid";
      $d['Link'] = $url;
      $so->setdata((array)$d);
      $so->update($so->getpk(),$soid);
      $status = ($soid)?1:0;
      
      $sse->delete($this->getpk(),$this->getid()); 
      $sle->delete($this->getpk(),$this->getid());
      
      $pstep->completestep(3);
      $pstep->complete();
      $proc->completestep($step);
      return $status;
   }
   // E N D P R O C E S S
   function sendrequest($svr,$tgt,$params) {
      global $configsettings,$server,$client,$folder;
      $session = $this->getsid();
      $k = new KLib($session);
      $keys = json_decode($k->getkeys());
      $clt = "${client}/${folder}";
      $baseurl = "http://${svr}${tgt}";
      $req = new stdClass();
      $req->target = $baseurl;
      $req->request = $params;
      $req->server = $svr;
      $req->sid = $session;
//print "<br/>R</br/>"; print_r($req);
      $er = new stdClass();
      $er->domain = $clt;
      $er->session = $session;
      $er->etype = 'AB';            
      $er->request = encrypt(json_encode($req),$keys->k1,$keys->k2);
//print "<br/>E</br/>"; print_r($er);
      $jurl = "http://${clt}/jproxy.php";
      $jr = new stdClass();
      $jr->target = "http://${svr}/eproxy.php";
      $jr->request = json_encode($er);
//print "<br/>J</br/>"; print_r($jr);
      $res = post($jurl,$jr);
      $cres = decrypt($res,$keys->k1,$keys->k2);
      return $cres;
   }
   function saveresponse($fname,$type,$cres) {
      global $folder;
      $sid = $this->getid();
      $f = new S2_File();
      if (!$f->exists()) $f->create();
      $d = new stdClass();
      $d->Name = $fname;
      $d->Type = $type; 
      $d->Data = $cres;
      $d = s2_savefile($d);
      $f->setdata((array)$d);
      $fid = $f->insert();
      $url = 'http://'.$_SERVER['SERVER_NAME']."/${folder}/echofile.php?a=echo&i=$fid";
      $d->Link = $url;
      $f->setdata(array('Link'=>$url));
      $f->update($f->getpk(),$fid);
      
      $rdo = new ODS_Raw_Data();
      if (!$rdo->exists()) $rdo->create();
      $d = array(
         $this->getpk() => $this->getid(),
         $f->getpk()    => $fid
      );
      $rdo->setdata($d);
      $rdid = $rdo->insert();
      return $rdid;
   }
}

class ODS_Raw_Data extends DBT {
   protected $tablename = 'ODS_Raw_Data';
   protected $displayname = 'Search Raw Species Data';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Raw_Data_ID'       => 'Unique Key',
      'ODS_Search_ID'         => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1}',
      'S2_File_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_File","TargetField":"S2_File_ID","Current":0,"Inherit":0}' 
   ); 
   protected $permissions = array(
      'Def'    => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Search_ID','Run_On'));
   }
}

class ODS_Search_Record extends DBT {
   protected $tablename = 'ODS_Search_Record';
   protected $displayname = 'Search Species Record';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Search_Record_ID'  => 'Unique Key',
      'ODS_Search_ID'         => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1,"NoList":1}',
      'G_Species_ID'          => '{"DataType":"LINKEDTO","TargetType":"G_Species","TargetField":"G_Species_ID"}',
      'Place_Name'            => 'Short Text',
      'Grid_Reference'        => '{"DataType":"Short Text","Derives":[{"Column":"Boundary","Method":"s2_getgrsquare","Params":[{"Type":"Value"}]}]}',
      'Boundary'              => '{"DataType":"Map Data","NoList":1}',
      'Recorder'              => 'Short Text',
      'Start_Date'            => 'Date',
      'End_Date'              => 'Date',
      'Comments'              => 'Medium Text',
      'Details'               => 'Medium Text',
      'Source'                => 'Short Text',
      'Survey'                => 'Long Text' 
   ); 
   protected $permissions = array(
      'Def'    => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('G_Species_ID','Grid_Reference'));   
   }
   function insert() {
      $sids = array();
      if (preg_match('/^\[/',$this->data['G_Species_ID'])) {
         $svals = json_decode($this->data['G_Species_ID']);
         foreach ($svals as $x => $sx) {
            $s = new G_Species();
            $s->setdata(array('Latin_Name' => json_encode($sx)));
            $s->insert();
            $this->data['G_Species_ID'] = $s->getid();
            parent::insert();
            $sids[] = $this->getid();
         }
      } else if (preg_match('/\{/',$this->data['G_Species_ID'])) {
         $s = new G_Species();
         $s->setdata(array('Latin_Name' => $this->data['G_Species_ID']));
         $s->insert();
         $this->data['G_Species_ID'] = $s->getid();
         parent::insert();
         $sids[] = $this->getid();
      } else {
         parent::insert();
         $sids = $this->getid();
      }
      return $sids;
   }
}

class ODS_Search_Buffer extends DBT {
   protected $tablename = 'ODS_Search_Buffer';
   protected $displayname = 'Data Search Buffer';
   protected $domain = 'ods';
   protected $orders = array('Number' => 'ASC');
   protected $columns = array(
      'ODS_Search_Buffer_ID'           => 'Unique Key',
      'ODS_Search_ID'                  => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1,"NoList":1}',
      'Buffer_Size'                    => '{"DataType":"Number","NoList":1,"Derives":[{"Column":"Buffer","Method":"s2_buffergeom","Params":[{"Type":"Column","IType":"ODS_Search","Value":"ODS_Search_ID","Property":"Boundary"},{"Type":"Value"},{"Type":"Static","Value":27700}]}]}',
      'Buffer'                         => '{"DataType":"Map Data"}',
      'Number'                         => '{"DataType":"Count","CountOfType":"ODS_Search","CountOfField":"ODS_Search_ID"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Search_ID','Buffer_Size'));   
   }
}

class ODS_Species_Summary_Entry extends DBT {
   protected $tablename = 'ODS_Species_Summary_Entry';
   protected $displayname = 'Species Summary Entry';
   protected $domain = 'ods';
   protected $species = array();
   protected $scoringgroups = array();
   protected $columns = array(
      'ODS_Species_Summary_Entry_ID'   => 'Unique Key',
      'ODS_Search_ID'                  => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1,"NoList":1}',
      'ODS_Species_Profile_Section_ID' => '{"DataType":"LINKEDTO","TargetType":"ODS_Species_Profile_Section","TargetField":"ODS_Species_Profile_Section_ID","Current":1,"Inherit":1,"NoList":1}',
      'List'                           => '{"DataType":"INTERFACE","Domain":"species","ValidTypes":["G_Species_List"]}',
      'Species_Count'                  => '{"DataType":"Number"}',
      'Record_Count'                   => '{"DataType":"Number"}',
      'Score'                          => '{"DataType":"Decimal"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Search_ID','ODS_Species_Profile_Section_ID','List'));   
   }
}

class ODS_Species_List_Entry extends DBT {
   protected $tablename = 'ODS_Species_List_Entry';
   protected $displayname = 'Species List Entry';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Species_List_Entry_ID'      => 'Unique Key',
      'ODS_Search_ID'                  => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1,"NoList":1}',
      'ODS_Species_Profile_Section_ID' => '{"DataType":"LINKEDTO","TargetType":"ODS_Species_Profile_Section","TargetField":"ODS_Species_Profile_Section_ID","Current":1,"Inherit":1,"NoList":1}',
      'G_Species_ID'                   => '{"DataType":"LINKEDTO","TargetType":"G_Species","TargetField":"G_Species_ID"}',
      'First_Year'                     => '{"DataType":"Number"}',
      'Last_Year'                      => '{"DataType":"Number"}',
      'Record_Count'                   => '{"DataType":"Number"}',
      'Member_Of'                      => '{"DataType":"Long Text"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Search_ID','ODS_Species_Profile_Section_ID','List'));   
   }
}
class ODS_Record_List_Entry extends DBT {
   protected $tablename = 'ODS_Record_List_Entry';
   protected $displayname = 'Record List Entry';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Record_List_Entry_ID'       => 'Unique Key',
      'ODS_Search_ID'                  => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1,"NoList":1}',
      'ODS_Species_Profile_Section_ID' => '{"DataType":"LINKEDTO","TargetType":"ODS_Species_Profile_Section","TargetField":"ODS_Species_Profile_Section_ID","Current":1,"Inherit":1,"NoList":1}',
      'ODS_Search_Record_ID'           => '{"DataType":"LINKEDTO","TargetType":"ODS_Search_Record","TargetField":"ODS_Search_Record_ID","Current":1,"Inherit":1}',
      'Member_Of'                      => '{"DataType":"Long Text"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Search_ID','ODS_Species_Profile_Section_ID','List'));   
   }
}
class ODS_Search_Output extends DBT {
   protected $tablename = 'ODS_Search_Output';
   protected $displayname = 'Data Search Output';
   protected $domain = 'ods';
   protected $orders = array('Number' => 'ASC');
   protected $columns = array(
      'ODS_Search_Output_ID'  => 'Unique Key',
      'ODS_Search_ID'         => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1}',
      'S2_File_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_File","TargetField":"S2_File_ID","Current":0,"Inherit":0,"Show":0}',
      'Link'                  => '{"DataType":"URL"}',
      'Number'                => '{"DataType":"Count","CountOfType":"ODS_Search","CountOfField":"ODS_Search_ID"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Search_ID','Number'));   
   }
}

class ODS_Search_Dataset extends DBT {
   protected $tablename = 'ODS_Search_Dataset';
   protected $displayname = 'Data Search Dataset';
   protected $domain = 'ods';
   protected $orders = array('Number' => 'ASC');
   protected $columns = array(
      'ODS_Search_Dataset_ID' => 'Unique Key',
      'ODS_Search_ID'         => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1}',
      'Name'                  => '{"DataType":"Short Text"}',
      'Description'           => '{"DataType":"Long Text"}',
      'Records'               => '{"DataType":"Number"}',
      'Source'                => '{"DataType":"Short Text"}',
      'Number'                => '{"DataType":"Count","CountOfType":"ODS_Search","CountOfField":"ODS_Search_ID"}'
   );
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
}

class ODS_Source_Results extends DBT {
   protected $tablename = 'ODS_Source_Results';
   protected $displayname = 'Data Search Source Results';
   protected $domain = 'ods';
   protected $orders = array('Number' => 'ASC');
   protected $columns = array(
      'ODS_Search_Dataset_ID' => 'Unique Key',
      'ODS_Search_ID'         => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1}',
      'Name'                  => '{"DataType":"Short Text"}',
      'Description'           => '{"DataType":"Long Text"}',
      'Successful'            => '{"DataType":"True or False"}',
      'Records'               => '{"DataType":"Number"}',
      'Source'                => '{"DataType":"Short Text"}',
      'Number'                => '{"DataType":"Count","CountOfType":"ODS_Search","CountOfField":"ODS_Search_ID"}'
   );
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
}


class ODS_Data_Server extends DBT {
   protected $tablename = 'ODS_Data_Server';
   protected $displayname = 'Data Server';
   protected $domain = 'ods';
   protected $alts = array('Layers'=>'ODS_Layer_Catalogue','Searches'=>'ODS_Search','Profiles'=>'ODS_Search_Profile');
   protected $show = array('ODS_Server_Layer');
   protected $columns = array(
      'ODS_Data_Server_ID'    => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text"}',
      'Description'           => '{"DataType":"Long Text"}',
      'URL'                   => '{"DataType":"URL"}',
      'Requires_Login'        => '{"DataType":"True or False"}',
      'User_Name'             => '{"DataType":"Encrypted Text","NoList":1}',
      'Password'              => '{"DataType":"Encrypted Text","NoList":1}'
   );
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
}

class ODS_Server_Type extends DBT {
   protected $tablename = 'ODS_Server_Type';
   protected $displayname = 'Server Type';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Data_Server_ID'    => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text"}',
      'Description'           => '{"DataType":"Long Text"}'
   );
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   protected $primingdata = array(
      'ODS_Server_Type' => '
"Name","Description"
"WFS Server","Server conforming to the OGC Web Feature Service standard."
"S2 Server","Second Site implementation."'
   );
}

class ODS_Server_Layer extends DBT {
   protected $tablename = 'ODS_Server_Layer';
   protected $displayname = 'Server Layer';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Server_Layer_ID'   => 'Unique Key',
      'ODS_Data_Server_ID'    => '{"DataType":"LINKEDTO","TargetType":"ODS_Data_Server","TargetField":"ODS_Data_Server_ID","Current":1,"Inherit":1}',
      'Name'                  => '{"DataType":"Short Text"}',
      'Description'           => '{"DataType":"Long Text"}',
      'Feature_Name'          => '{"DataType":"Short Text"}'
   );
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
}

class ODS_Layer_Catalogue extends DBT {
   protected $tablename = 'ODS_Layer_Catalogue';
   protected $displayname = 'Layer Catalogue';
   protected $domain = 'ods';
   protected $show = array('ODS_Layer_Attributes');
   protected $alts = array('Servers'=>'ODS_Data_Server','Searches'=>'ODS_Search','Profiles'=>'ODS_Search_Profile');
   protected $columns = array(
      'ODS_Layer_Catalogue_ID'   => 'Unique Key',
      'ODS_Server_Layer_ID'      => '{"DataType":"LINKEDTO","TargetType":"ODS_Server_Layer","TargetField":"ODS_Server_Layer_ID","Current":1,"Inherit":1}',
      'ODS_Layer_Type_ID'        => '{"DataType":"LINKEDTO","TargetType":"ODS_Layer_Type","TargetField":"ODS_Layer_Type_ID","Current":1,"Inherit":1}',
      'Abbreviation'             => '{"DataType":"Short Text"}'
   );
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Server_Layer_ID'));
   }
}

class ODS_Layer_Attributes extends DBT {
   protected $tablename = 'ODS_Layer_Attributes';
   protected $displayname = 'Layer Attributes';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Layer_Attributes_ID'  => 'Unique Key',
      'ODS_Layer_Catalogue_ID'   => '{"DataType":"LINKEDTO","TargetType":"ODS_Layer_Catalogue","TargetField":"ODS_Layer_Catalogue_ID","Current":1,"Inherit":1}',
      'Geometry_Attribute'       => '{"DataType":"Short Text","NoList":1}', 
      'Name_Attribute'           => '{"DataType":"Short Text"}', 
      'Code_Attribute'           => '{"DataType":"Short Text"}', 
      'Status_Attribute'         => '{"DataType":"Short Text","NoList":1}', 
      'Update_Attribute'         => '{"DataType":"Short Text","NoList":1}',
      'Live_Status'              => '{"DataType":"Short Text"}',
      'Split_On_Attribute'       => '{"DataType":"Short Text","NoList":1}'
   );
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
}

class ODS_Layer_Type extends DBT {
   protected $tablename = 'ODS_Layer_Type';
   protected $displayname = 'Layer Type';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Layer_Type_ID'     => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text"}',
      'Description'           => '{"DataType":"Long Text"}'
   );
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );                                   
   protected $primingdata = array(
      'ODS_Layer_Type' => '
"Name","Description"
"Statutory Sites","Legally protected sites."
"Non-statutory Sites","Includes Local Sites and Nature Reserves."
"Habitats","BAP inventories, NVC, Phase I, Shimwell etc"
"Context","Layers like character areas."
"Administrative","Parishes, Wards, Districts.."
"Alert Layers","Layers showing known or potential distributions of species etc.."'
   );
}

class ODS_Site_Profile_Section extends DBT {
   protected $tablename = 'ODS_Site_Profile_Section';
   protected $displayname = 'Profile Site Section';
   protected $domain = 'ods';
   protected $show = array('ODS_Site_Section_Action','ODS_Site_Output');
   protected $orders = array('Number' => 'ASC');
   protected $columns = array(
      'ODS_Site_Profile_Section_ID' => 'Unique Key',
      'ODS_Search_Definition_ID'    => '{"DataType":"LINKEDTO","TargetType":"ODS_Search_Definition","TargetField":"ODS_Search_Definition_ID","Current":1,"Inherit":1}',
      'Type'                        => '{"DataType":"LINKEDTO","TargetType":"ODS_Layer_Type","TargetField":"ODS_Layer_Type_ID","Current":1,"Inherit":1}',
      'Description'                 => '{"DataType":"Long Text","NoList":1}',
      'Buffer'                      => '{"DataType":"LINKEDTO","TargetType":"ODS_Search_Profile_Buffer","TargetField":"ODS_Search_Profile_Buffer_ID","Inherit":1}',
      'Return'                      => '{"DataType":"Short Text","Options":["All Sites","Only Live Sites"]}',
      'Number'                      => '{"DataType":"Count","CountOfType":"ODS_Search_Definition","CountOfField":"ODS_Search_Definition_ID"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('Type','Buffer'));   
   }
}

class ODS_Site_Output extends DBT {
   protected $tablename = 'ODS_Site_Output';
   protected $displayname = 'Site Data Output';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Site_Output_ID'             => 'Unique Key',
      'ODS_Site_Profile_Section_ID'    => '{"DataType":"LINKEDTO","TargetType":"ODS_Site_Profile_Section","TargetField":"ODS_Site_Profile_Section_ID","Current":1,"Inherit":1}',
      'ODS_Site_Output_Type_ID'        => '{"DataType":"LINKEDTO","TargetType":"ODS_Site_Output_Type","TargetField":"ODS_Site_Output_Type_ID"}',
      'Is_Expanded'                    => '{"DataType":"True or False"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Site_Profile_Section_ID','ODS_Site_Output_Type_ID'));   
   }
}

class ODS_Site_Section_Action extends DBT {
   protected $tablename = 'ODS_Site_Section_Action';
   protected $displayname = 'Site Section Action';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Site_Section_Action_ID'     => 'Unique Key',
      'ODS_Site_Profile_Section_ID'    => '{"DataType":"LINKEDTO","TargetType":"ODS_Site_Profile_Section","TargetField":"ODS_Site_Profile_Section_ID","Current":1,"Inherit":1}',
      'Name'                           => '{"DataType":"Short Text"}',
      'Description'                    => '{"DataType":"Long Text"}',
      'ODS_Severity_Type_ID'           => '{"DataType":"LINKEDTO","TargetType":"ODS_Severity_Type","TargetField":"ODS_Severity_Type_ID"}',
      'Is_Default'                     => '{"DataType":"True or False"}',
      'Precision_Constraint'           => '{"DataType":"Number","NoList":1}',
      'Currency_Constraint'            => '{"DataType":"Number","NoList":1}',
      'Records_Constraint'             => '{"DataType":"Number","NoList":1}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );  
}

class ODS_Site_Output_Type extends DBT {
   protected $tablename = 'ODS_Site_Output_Type';
   protected $displayname = 'Site Data Output Type';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Site_Output_Type_ID'       => 'Unique Key',
      'Name'                          => '{"DataType":"Short Text","Mandatory":1}',
      'Description'                   => '{"DataType":"Long Text"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   protected $primingdata = array(
      'ODS_Site_Output_Type' => '
"Name","Description"
"Summary","List showing the area and number of sites matched from each layer of a given type."
"Site List","List showing individual polygons with unified attribution."
"Action","What should be done as a result of data appearing in this section."'
   );
}

class ODS_Search_Site_File extends DBT {
   protected $tablename = 'ODS_Search_Site_File';
   protected $displayname = 'Search Site File';
   protected $domain = 'ods';
   protected $species = array();
   protected $scoringgroups = array();
   protected $columns = array(
      'ODS_Search_Site_File_ID'     => 'Unique Key',
      'ODS_Search_ID'               => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1,"NoList":1}',
      'ODS_Site_Profile_Section_ID' => '{"DataType":"LINKEDTO","TargetType":"ODS_Site_Profile_Section","TargetField":"ODS_Site_Profile_Section_ID","Current":1,"Inherit":1,"NoList":1}',
      'Type'                        => '{"DataType":"LINKEDTO","TargetType":"ODS_Layer_Type","TargetField":"ODS_Layer_Type_ID","Current":1,"Inherit":1}',
      'S2_File_ID'                  => '{"DataType":"LINKEDTO","TargetType":"S2_File","TargetField":"S2_File_ID","Current":0,"Inherit":0,"Show":0}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('ODS_Search_ID','ODS_Site_Profile_Section_ID','S2_File_ID'));   
   }
}

class ODS_Site_Summary_Entry extends DBT {
   protected $tablename = 'ODS_Site_Summary_Entry';
   protected $displayname = 'Site Summary Entry';
   protected $domain = 'ods';
   protected $species = array();
   protected $scoringgroups = array();
   protected $columns = array(
      'ODS_Site_Summary_Entry_ID'   => 'Unique Key',
      'ODS_Search_ID'               => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1,"NoList":1}',
      'ODS_Site_Profile_Section_ID' => '{"DataType":"LINKEDTO","TargetType":"ODS_Site_Profile_Section","TargetField":"ODS_Site_Profile_Section_ID","Current":1,"Inherit":1,"NoList":1}',
      'Category'                    => '{"DataType":"Short Text"}',
      'Type'                        => '{"DataType":"LINKEDTO","TargetType":"ODS_Layer_Type","TargetField":"ODS_Layer_Type_ID","Current":1,"Inherit":1}',
      'Layer'                       => '{"DataType":"LINKEDTO","TargetType":"ODS_Layer_Catalogue","TargetField":"ODS_Layer_Catalogue_ID","Current":1,"Inherit":1,"NoList":1}',
      'Sites'                       => '{"DataType":"Number"}',
      'Total_Hectares'              => '{"DataType":"Decimal"}',
      'Total_Perimeter'             => '{"DataType":"Decimal"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return (isset($this->data['Category']))?$this->data['Category']:$this->derivename(array('Layer'));   
   }
}

class ODS_Site_List_Entry extends DBT {
   protected $tablename = 'ODS_Site_List_Entry';
   protected $displayname = 'Site List Entry';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Site_List_Entry_ID'      => 'Unique Key',
      'ODS_Search_ID'               => '{"DataType":"LINKEDTO","TargetType":"ODS_Search","TargetField":"ODS_Search_ID","Current":1,"Inherit":1,"NoList":1}',
      'ODS_Site_Profile_Section_ID' => '{"DataType":"LINKEDTO","TargetType":"ODS_Site_Profile_Section","TargetField":"ODS_Site_Profile_Section_ID","Current":1,"Inherit":1,"NoList":1}',
      'Category'                    => '{"DataType":"Short Text"}',
      'Type'                        => '{"DataType":"LINKEDTO","TargetType":"ODS_Layer_Type","TargetField":"ODS_Layer_Type_ID","Current":1,"Inherit":1}',
      'Layer'                       => '{"DataType":"LINKEDTO","TargetType":"ODS_Layer_Catalogue","TargetField":"ODS_Layer_Catalogue_ID","Current":1,"Inherit":1,"NoList":1}',
      'Boundary'                    => '{"DataType":"Map Data"}',
      'Site_Name'                   => '{"DataType":"Short Text"}',                   
      'Site_Code'                   => '{"DataType":"Short Text"}',
      'Status'                      => '{"DataType":"Long Text"}',
      'Updated'                     => '{"DataType":"Date"}',
      'Hectares'                    => '{"DataType":"Decimal"}',
      'Perimeter'                   => '{"DataType":"Decimal"}'
   ); 
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );
   function getname() {
      return $this->derivename(array('Site_Name'));   
   }
}

class ODS_Severity_Type extends DBT {
   protected $tablename = 'ODS_Severity_Type';
   protected $displayname = 'Severity Type';
   protected $domain = 'ods';
   protected $columns = array(
      'ODS_Severity_Type_ID'  => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text"}',
      'Description'           => '{"DataType":"Long Text"}',
      'Icon'                  => '{"DataType":"File Path"}',
      'Rank'                  => '{"DataType":"Number"}'
   );
   protected $permissions = array(
      'Def'    => 'Search Administrator',
      'List'   => 'Search Run',
      'View'   => 'Search Run'
   );   
   function geticon() {
      return $this->data['Icon'];
   }                                
}
?>