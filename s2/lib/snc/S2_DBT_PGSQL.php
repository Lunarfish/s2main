<?php    
//error_reporting(E_ALL);                           
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('cdm/cdm_access_functions.php');
include_once('users/users.inc');
include_once('gen/G_Cache.php');
include_once('gen/G_Reserve_Number.php');
include_once('klib.php');

/*
   echo $query->__toString();
   
   Derived Data - uses a javascript function to update a column 
   "Derives":[{"Column":"colname","Method":jsmethod,"Params":[{"Type":"Value"},{"Type":"Static","Value":"val"}]]
*/                           

class DBT extends DBT_Gen {
   protected $dbpghandle;
   function DBT() {
      parent::DBT_Gen();
      //$this->pgnativeconn();
      $this->dbpghandle = SnC_getPGNativeConnection($this->domain);
   }
   function pgnativeconn() {    
      global $dbConfig, $dbPlatform;
      
      // pg native uses user in the connection string rather than username;
      $pgConfig = array();
      foreach($dbConfig as $key => $val) {
         switch($key) {
         case 'username': $pgConfig['user'] = $val; break;
         default: $pgConfig[$key] = $val;
         }
      }      
      
      $pgcopts = array();
      foreach ($pgConfig as $key => $val) $pgcopts[] = "${key}=${val}";
      $pgcstring = join(' ',$pgcopts);
      $this->dbpghandle = pg_connect($pgcstring);
   }
   protected $datatypes = array(
      'Unique Key'         => 'SERIAL PRIMARY KEY',
      'UUID'               => 'VARCHAR(36)',                                    
      'Linked Key'         => 'INT',
      'Interface'          => 'INT',
      'Short Text'         => 'VARCHAR(100)',
      'Password'           => 'VARCHAR(100)',
      'Medium Text'        => 'VARCHAR(200)',
      'Long Text'          => 'TEXT',
      'Encrypted Text'     => 'TEXT',
      'Number'             => 'INTEGER',
      'Percentage'         => 'FLOAT',
      'Currency'           => 'FLOAT',
      'Count'              => 'INTEGER',
      'Decimal'            => 'FLOAT',
      'Version'            => 'FLOAT',
      'True or False'      => 'BOOLEAN',
      'Date'               => 'DATE',
      'Date From'          => 'DATE',
      'Date Until'         => 'DATE',
      'Time'               => 'TIME',
      'Date and Time'      => 'TIMESTAMP',
      'Map Data'           => 'GEOMETRY',
      'Taxa'               => 'INT',
      'URL'                => 'VARCHAR(500)',
      'Email'              => 'VARCHAR(100)',
      'File Path'          => 'VARCHAR(500)',
      'File'               => 'VARCHAR(500)',
      'Taxon Version Key'  => 'VARCHAR(16)',
      'TVK'                => 'VARCHAR(16)' 
   ); 
   function isduplicate() {
      $dbh = $this->dbhandle;                       
      $dbpg = $this->dbpghandle;
      $query = $dbh->select();
      // CreatedBy and CreatedOn have to be excluded from duplicate testing.
      // Automatically populated data has to be excluded as well. 
      $excludedcolumns = array('CreatedBy','CreatedOn', 'Count', 'Version', $this->getpk());
      $cols = array();
      foreach($this->columns as $col => $def) {
         $val = (isset($this->data[$col]))?$this->data[$col]:null;
         $lcol = strtolower($col);
         if (preg_match('/^\{/',$def)) {
            $params = json_decode($def);
            $dtype = $params->DataType;
         } else $dtype = preg_replace('/\:.*/','',$def);
         if ($dtype == 'Unique Key') $cols[$col] = $lcol;
         if (!in_array($col,$excludedcolumns) && isset($val)) {
            $cols[$col] = $lcol;
            switch ($dtype) {
            case 'Map Data': {
               //$val = new Zend_Db_Expr("st_geomfromtext('$val',4326)");
               $val = $this->insertterm($lcol,$dtype,$val);
               $query->where("$lcol = ?",$val);   
            }break;
            case 'LINKEDTO': {
               if (is_object($val)) $val = $val->Id;
               $query->where("$lcol = ?",$val);
            }break;
            case 'LINKEDTOM': {
               unset($cols[$col]);
            }break;
            case 'INTERFACE': {
               if (isset($this->data[$col]) && is_object($this->data[$col])) {
                  $val = $this->data[$col]->G_Cache_ID;
                  $query->where("$lcol = ?",$val);
               }
            }break;
            case 'Taxa': {
               if (isset($this->data[$col]) && is_object($this->data[$col])) {
                  $val = $this->data[$col]->G_Cache_ID;
                  $query->where("$lcol = ?",$val);
               } 
            }break;
            default: {
               $query->where("$lcol = ?",$val);
            }break;
            } 
         }
      }
      try {
         $query->from(strtolower($this->tablename),$cols);
         $sel = $query->__toString();
         $res = pg_query($dbpg,$sel);
         $result = pg_fetch_array($res);
         $ret = (is_array($result))?$result[$this->getpk()]:0;
      } catch (Exception $e) {
         $ret = 0;
      }      
      return $ret;      
   }
   function getversions($val) {
      $versions = array();
      foreach ($this->columns as $col => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $classname = (isset($params->VersionOfType))?$params->VersionOfType:null;
            $thiscol = (isset($params->VersionOfField))?$params->VersionOfField:null;
         } else list($type,$classname,$thiscol) = preg_split('/\:/',$type);
         if ($type == 'Version') {
            $link = null;
            $link = new $classname();
            if ($link->exists() && $val) {
               $versions['Column'] = $col;
               $link->loaddata($val,null,false);
               $column = $this->displayname;
               $column = preg_replace('/\s+/','_',$column);
               $cversion = $link->selectcolumnval($column);
               $cversion = (!$cversion>0)?1:$cversion;
               $versions['Current'] = floor($cversion*10)/10;
               
               if ($this->exists()) {
                  $query = $this->dbhandle->select();
                  $ltable = strtolower($this->tablename);
                  $lcol = strtolower($col);
                  $query->from($ltable, array('MaxVer' => new Zend_Db_Expr("Max($lcol)")));
                  $lthiscol = strtolower($thiscol);
                  $query->where("$lthiscol = ?",$val);
                  $result = $this->dbhandle->fetchRow($query);
                  if (isset($result['MaxVer'])) //$versions['Maximum'] = floor($result['MaxVer']*10)/10;
                     $versions['Maximum'] = floor(round($result['MaxVer'],1)*10)/10;
               }
            }               
         } 
      }
      return $versions;
   }
   function getcount($val) {
      $count = 0;
      foreach ($this->columns as $col => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $classname = (isset($params->CountOfType))?$params->CountOfType:null;
            $thiscol = (isset($params->CountOfField))?$params->CountOfField:null;
         } else list($type,$classname,$thiscol) = preg_split('/\:/',$type);
         if ($type == 'Count') {
            if ($val && $thiscol) {
               $query = $this->dbhandle->select();
               $lcol = strtolower($col);
               $lthiscol = strtolower($thiscol);
               $query->from(strtolower($this->tablename), array('MaxCount' => new Zend_Db_Expr("Max($lcol)")));
               $query->where("$lthiscol = ?",$val);
               $result = $this->dbhandle->fetchRow($query);
               if (isset($result['MaxCount'])) $count = $result['MaxCount'];
            }               
         } 
      }
      return $count;        
   } 
   function updateversion($id,$versionof,$version) {
      $pk = $this->getpk();
      $link = null;
      $link = new $versionof();
      $ucol = $link->getdisplayname();
      $ucol = preg_replace('/\s+/','_',$ucol);
      $lucol = strtolower($ucol);
      $data = array($lucol => $version);
      $lpk = strtolower($pk); 
      $where = $this->dbhandle->quoteInto("$lpk = ?",$id);
      $status = $this->dbhandle->update(strtolower($this->tablename),$data,$where);
      return $status;
   }
   function selectcolumnval($col) {
      $value = null;
      if (isset($this->data[$col])) {
         $value = $this->data[$col];
      } else
      if($this->exists()) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
         } else {
            $type = $this->columns[$col];
            $type = preg_replace('/\:.+/','',$type);
         }
         $query = $this->dbhandle->select();
         $lcol = strtolower($col);
         $query->from(strtolower($this->tablename), array($col=>$lcol));
         $pk = $this->getpk();
         $lpk = strtolower($pk);
         $id = $this->getid();
         $query->where("$lpk = ?",$id);
         $result = $this->dbhandle->fetchRow($query);
         $value = (isset($result[$col]))?$result[$col]:null;
         switch ($type) {
         case 'LINKEDTO': {
            if (isset($params)) {
               if (!is_object($value)) {
                  $type = $params->DataType;
                  $table = $params->TargetType;
                  $column = $params->TargetField;
                  $link = null;
                  $link = new $table();
                  $link->loaddata($value,$column);
                  $obj = new stdClass();
                  $obj->Name = $link->getname();
                  $obj->Id = $link->getid();
                  $icon = $link->geticon();
                  if (isset($icon)) $obj->Icon = $icon;
                  $value = $obj;
               }
            }   
         }break;
         case 'LINKEDTOM': {
            if (isset($params)) {
               if (!is_object($value)) {
                  $type = $params->DataType;
                  $table = $params->TargetType;
                  $column = $params->TargetField;
                  $pk = $this->getpk();
                  $id = $this->getid();
                  $p = (isset($params->Property))?$params->Property:$col;
                  $ltm = new DBT_Lookup($p,$this->tablename,$pk,$table,$column);
                  $obj = $ltm->selecttargets($value,$pk);
                  $value = $obj;
               }
            }   
         }break;
         default: {
            $value = $this->postprocessterm($col,$type,$value);
         }break;
         } 
      }
      return $value; 
   }
   function selectvalwhere($col,$where) {
      $query = $this->dbhandle->select();
      $lcol = strtolower($col);
      $query->from(strtolower($this->tablename), array($col=>$lcol));
      foreach($where as $wcol => $val) {
         $lwcol = strtolower($wcol);
         $query->where("$lwcol = ?",$val);
      }
//print $query->__toString();exit;
      $result = $this->dbhandle->fetchRow($query);
      $value = (isset($result[$col]))?$result[$col]:null;
      return $value;      
   }
   function loadderiveddata($crumbs,$icol=null,$ival=null) {
//print "loading data for ".$this->gettablename()."<br/>";
      $int = $this->getinterface();
      $query = $this->dbhandle->select();
      $ocols = array();
      foreach ($int as $col => $type) {
         if (!is_object($type) || $type->DataType != 'LINKEDTOM') $ocols[$col] = strtolower($col);
      }
      $query->from(strtolower($this->tablename),$ocols);
      $pk = $this->getpk();
      $crumbs = (array)$crumbs;
      foreach ($int as $col => $type) {
         if (is_object($type) && $type->DataType == 'LINKEDTO') {
            $lcol = strtolower($col);
            foreach($crumbs as $ind => $crumb) {
               if ($crumb->IType == $type->TargetType) {
                  //print "<pre>";print_r($type);print "</pre>";
                  $it = $crumb->IType;
                  switch ($crumb->Type) {
                  case 'SameForAll': {
                     $query->where("$lcol = ?", $crumb->Value);
                  }break;
                  case 'Link': {
                     $link = new $it();
                     $lid = $link->loadderiveddata($crumbs,$crumb->TColumn,$crumb->Value); 
                     $query->where("$lcol = ?",$lid);
                  }break;
                  } 
               }
            }
         }   
      }
      $licol = strtolower($icol);
      if (isset($icol) && isset($ival)) $query->where("$licol = ?",$ival);
//echo $query->__toString();
      $this->data = $this->dbhandle->fetchRow($query);
      return $this->getid();          
   }
   function exists() {
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from('pg_tables',array('tablename'=>'tablename'));
      $query->where('tablename = ?',strtolower($this->tablename));
      $resp = $db->fetchAll($query);
      return (count($resp)>0);
   }
   function drop() {
      $statement = "drop table $this->tablename";
      $status = $this->dbhandle->exec($statement);
      return $status;
   }
   function create() {
// CREATE TABLE $tablename(${tablename}_id serial primary key, gridref varchar(20), geom geometry, CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 27700));";      
      global $pending;
      
      if (!isset($pending[$this->tablename]) && isset($this->tablename) && (count($this->columns)>0)) {
         $pending[$this->tablename] = true;
      
         $statement = "CREATE TABLE $this->tablename (";
         $constraints = array();
         $pk = null;
         foreach($this->columns as $name => $type) {
            /* force these back to blank so previous values are not used. */
            $params = null;
            $bits = null;
            if (preg_match('/^\{/',$type)) {
               $params = json_decode($type);
               $type = $params->DataType;
            } else {
               $bits = preg_split('/\:/',$type);
               $type = $bits[0];         
            }
            $suppress = false;
            if ($type == 'Unique Key') $pk = $name;
            if ($type == 'Map Data') $constraints[] = "CONSTRAINT enforce_srid_geom CHECK (st_srid($name) = 4326)";
            $mand = false;
            if ($type == 'LINKEDTO') {
               if (isset($params)) {
                  $table = $params->TargetType;
                  $column = $params->TargetField;
               } else {
                  preg_match('/([^\.]+)\.(.+)/',$bits[1],$matches);
                  $table = $matches[1];
                  $column = $matches[2];
               }
               if ($table) {
                  $dbt = new $table();
                  $tcols = $dbt->getcolumns();
                  $type = $tcols[$column];
                  if ($type == 'Unique Key') $type = 'Linked Key';
               }
            }
            if ($type == 'LINKEDTOM') {
               if (isset($params)) {
                  $table = $params->TargetType;
                  $column = $params->TargetField;
                  $property = (isset($params->Property))?$params->Property:$name;
               } else {
                  preg_match('/([^\.]+)\.(.+)/',$bits[1],$matches);
                  $table = $matches[1];
                  $column = $matches[2];
                  $property = $name;
               }
               $suppress = true;
               $dbt = new $table();
               if (!$dbt->exists()) $dbt->create();
               $tcols = $dbt->getcolumns();
               $ttype = $tcols[$column];
               if (preg_match('/^\{/',$ttype)) {
                  $params = json_decode($ttype);
                  $ttype = $params->DataType;
               } else {
                  $bits = preg_split('/\:/',$ttype);
                  $ttype = $bits[0];         
               }
               $lt = new DBT_Lookup($property,$this->tablename,$pk,$table,$column);
               if (!$lt->exists()) $lt->create();
            }
            if ($type == 'INTERFACE') {
               $type = 'Interface';            
            } 
            $def = $this->datatypes[$type];
            if ((isset($params) && (isset($params->Mandatory)&&$params->Mandatory==1))||($bits[count($bits)-1] == 'Mandatory')) $def .= " NOT NULL";
            if (!$suppress) $statement .= "$name $def,";  
         }
         
         /* 
            have to add Version fields before issuing create table statement 
            but have to create link tables after creating the source table. 
         */
         foreach($this->show as $classname) {
            if (preg_match('/([^:]+)/',$classname,$m)) {
               $classname = $m[1];
               $link = new $classname();
               if (!$link->exists()) $link->create();            
               $cols = $link->getinterface();
               foreach ($cols as $col => $type) {
                  if (is_object($type)) { 
                     $params = $type;
                     $type = $params->DataType;
                  } else if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $type = $params->DataType;
                  } else $type = preg_replace('/\:.+$/','',$type);
                  switch($type) {
                  case 'LINKEDTO': {
                     $lc = $params->TargetType;
                     $l2 = new $lc;
                     if (!$l2->exists()) $l2->create(); 
                  }break;      
                  case 'Version': {
                     $column = $link->getdisplayname();
                     $column = preg_replace('/\s+/','_',$column);
                     $dtype = $this->datatypes['Version'];
                     $statement .= "$column $dtype,";
                  }break;
                  }
               }
            }            
         }
         
         $statement .= "CreatedBy INT,";
         $statement .= "CreatedOn TIMESTAMP";
         if (count($constraints)>0) {
            $cs = join(', ',$constraints);
            $statement .= ", $cs";
         }
         $statement .= ");";
         $status = $this->dbhandle->exec($statement);
                  
         if ($this->gs_publishme) $this->gs_publish();
         if ($this->primingdata) $this->prime();
         if ($this->deriveddata) {
            foreach($this->deriveddata as $view) {
               $link = new $view();
               if (!$link->exists()) $link->create();
            }
         }
         unset($pending[$this->tablename]);
      }                                    
      return $status; 
   }
   function prime() {
      if (isset($this->primingdata)) {
         $this->validated = true;
         foreach ($this->primingdata as $classname => $data) {
            $data = trim($data);
            $data = preg_split('/\n/',$data);
            foreach($data as $line => $text) {
               if ($line == 0) $cols = $this->parselinecsv($text);
               else {
                  $idata = array();
                  $vals = $this->parselinecsv($text); 
                  foreach($cols as $colnum => $col) {
                     $idata[$col] = $vals[$colnum]; 
                  }
                  if ($classname == $this->tablename) {
                     $this->setdata($idata);
                     $this->insert();                 
                  } else {
                     $link = null;
                     $link = new $classname();
                     if (isset($link)) {
                        if (!$link->exists()) $link->create();
                        $link->validated = true;
                        $link->setdata($idata);
                        $link->insert();
                     }
                  }
               }               
            }                              
         }
      }
      $this->data = array();
   }
   function initialise() {
      if ($this->defaultpermissions) {
         $this->validated = true;
         foreach ($this->defaultpermissions as $classname => $data) {
            $data = trim($data);
            $data = preg_split('/\n/',$data);
            foreach($data as $line => $text) {
               if ($line == 0) $cols = $this->parselinecsv($text);
               else {
                  $idata = array();
                  $vals = $this->parselinecsv($text); 
                  foreach($cols as $colnum => $col) {
                     $idata[$col] = $vals[$colnum]; 
                  }
                  if ($classname == $this->tablename) {
                     $this->setdata($idata);
                     if (!$this->isduplicate()) $this->insert();
                  } else {
                     $link = null;
                     $link = new $classname();
                     if (isset($link)) {
                        if (!$link->exists()) $link->create();
                        $link->validated = true;
                        $link->setdata($idata);
                        if (!$link->isduplicate()) $link->insert();
                     }
                  }
               }               
            }                              
         }
      }
      $this->data = array();
   }
   function insertterm($col,$type,$val) {
      switch($type) {
      case 'Map Data': {
         $val = new Zend_Db_Expr("st_geomfromtext('$val',4326)");
         //$val = new Zend_Db_Expr("st_geomfromewkt('SRID=4326;$val')");
      }break;
      case 'UUID': {
         if (!isst($val)) $val = gen_uuid();
      }break;
      case 'Short Text':   {$val = pg_escape_string($val);}break;
      case 'Medium Text':  {$val = pg_escape_string($val);}break;
      case 'Long Text':    {$val = pg_escape_string($val);}break;
      default: {
         if (isset($val) && is_object($val)) $val = json_encode($val);
      }break;
      }
      return $val;
   }
   function selectterm($col,$type) {
      $val = null;
      $lcol = strtolower($col);
      switch ($type) {
      case 'Map Data': {
         $val = new Zend_Db_Expr("st_astext($lcol)");
      }break;
      default: {
         $val = $lcol;   
      }break;
      }
      return $val;
   }
   function pgnativelastid() {
      $dbpg = $this->dbpghandle;
      $pk = $this->getpk();
      $sq = strtolower($this->tablename).'_'.strtolower($pk).'_seq';
      // pg sequence name gets truncated to 63 characters so 59 + _seq
      if (strlen($sq)>63) $sq = substr($sq,0,59).'_seq'; 
      $sql = "select currval('$sq') as lid;";
      $ret = pg_query($dbpg,$sql);
      if (!$ret) {print pg_last_error($dbpg);exit;}
      $res = null;
      if ($ret) {
         $row = pg_fetch_assoc($ret);
         $res = (isset($row) && isset($row['lid']))?$row['lid']:null;
      }
      return $res;       
   }
   function pgnativeinsert($data) {
   /*
      This function has been included because the PDO abstracted library fails 
      to insert geometry data. This could be removed if a later version of 
      PHP fixes this problem. 
   */
      $tname = strtolower($this->tablename);
      $dbpg = $this->dbpghandle;
      $res = null;
      if (isset($dbpg)) {
         $cols = array();
         $vals = array();
         foreach($data as $col => $val) {
            // get text statement from Zend_Db_Expr() instance //
            if (is_object($val)) $v = $val->__toString();
            // leave numeric data unquoted // 
            else if (is_numeric($val)) $v = $val; 
            else if (is_bool($val)) $v = ($val)?'true':'false';
            // and then quote text data for the insert statement //
            else {
               $v = (isset($val) && strlen($val)>0)?"'$val'":'null';
               //if (preg_match('/\\/',$v)) $v = "E$val";
            }
            
            $cols[] = $col;
            $vals[] = $v;
            // update data array for pg_insert call if using // 
            if (isset($v) && $v!='null') $data[$col] = (is_object($val))?$v:$val;
            else unset($data[$col]); 
         }
         $colstring = join(',',$cols);           
         $valstring = join(',',$vals);           
         $sql = "INSERT INTO $tname ($colstring) VALUES ($valstring);";
//print $sql; exit;
         $res = pg_query($dbpg,$sql);
//print_r($data);
         //$res = pg_insert($dbpg,$tname,array_change_key_case($data,CASE_LOWER));
         if ($res) {
            $res = $this->pgnativelastid();
            $this->setdata(array($this->getpk() => $res));
         } else {print pg_last_error($dbpg);}
      }
      return $res;
   }                                    
   function insert() {
      $updates = array();
      $ltmd = array();
      if (!$this->validated) $this->validate();
      $data = array();
      $multiples = array();
      $total = 1;
      $counts = array();
      foreach($this->columns as $col => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
         } else $type = preg_replace('/\:.+/','',$type);
         if (isset($this->data[$col]) && $this->data[$col] == 'null') unset($this->data[$col]);
         switch ($type) { 
         case 'Unique Key': unset($this->data[$col]);
         case 'Map Data': {
            if (isset($this->data[$col]) && is_string($this->data[$col])) {
               $wkt = $this->data[$col];
               //$this->data[$col] = new Zend_Db_Expr("ST_GeomFromText(\"$wkt\",4326)");
               $this->data[$col] = $this->insertterm($col,$type,$wkt);
            }
         }break;
         case 'Short Text':   {$this->data[$col] = $this->insertterm($col,$type,$this->data[$col]);}break;
         case 'Medium Text':  {$this->data[$col] = $this->insertterm($col,$type,$this->data[$col]);}break;
         case 'Long Text':    {$this->data[$col] = $this->insertterm($col,$type,$this->data[$col]);}break;
         case 'True or False': {
            if (is_numeric($this->data[$col])) $this->data[$col] = ($this->data[$col] == 1);
            //if (!is_bool($this->data[$col])) $this->data[$col] = (bool)$this->data[$col];
         }break;
         case 'INTERFACE': {
            if (isset($this->data[$col]) && is_object($this->data[$col])) {
               $obj = $this->data[$col];
               $cache = array();
               $cache['Data_Type'] = $obj->Data_Type;
               $cache['Name'] = $obj->Name;
               $cache['Value_Type'] = (is_integer($obj->Value))?'Number':'Short Text';
               $cache['Value'] = $obj->Value;
               $cache['Domain'] = $obj->In_Domain; 
               $cache['JSON'] = json_encode($obj);
               $c = new G_Cache();
               $c->setdata($cache);
               $interfaceid = $c->insert();
               $this->data[$col] = $interfaceid;
            }
         }break;
         case 'Taxa': {
            if (isset($this->data[$col]) && is_object($this->data[$col])) {
               $obj = $this->data[$col];
               $cache = array();
               $cache['Data_Type'] = $obj->Data_Type;
               $cache['Name'] = $obj->Name;
               $cache['Value_Type'] = (is_integer($obj->Value))?'Number':'Short Text';
               $cache['Value'] = $obj->Value;
               $cache['Domain'] = $obj->In_Domain; 
               $cache['JSON'] = json_encode($obj);
               $c = new G_Cache();
               $c->setdata($cache);
               $interfaceid = $c->insert();
               $this->data[$col] = $interfaceid;
            } 
         }break;
         case 'LINKEDTO': {
            if (isset($this->data[$col])) {
               if (is_object($this->data[$col])) {
                  $this->data[$col] = $this->data[$col]->Id;
               } else if (!is_array($this->data[$col]) && preg_match('/^\[.+\]$/',$this->data[$col])) {
                  $this->data[$col] = json_decode($this->data[$col]);
                  if(count($this->data[$col])>0) {
                     $multiples[] = $col;
                     $total *= count($this->data[$col]);
                  }
               } else if (is_array($this->data[$col]) && count($this->data[$col])>0) {
                  $multiples[] = $col;
                  $total *= count($this->data[$col]);
               } 
            }
         }break;
         case 'LINKEDTOM': {
         // Have to insert data in order to get the primary key before 
         // inserting the linked to data
            $ltmd[$col] = $this->data[$col];
            unset($this->data[$col]); 
         }break;
         case 'Password': {
            if (!$this->validated) {
               $k = new KLib($this->getsid());
               $keys = json_decode($k->getkeys());
               $this->data[$col] = decrypt($this->data[$col],$keys->k1,$keys->k2);
            } 
            $this->data[$col] = $this->dbencrypt($this->data[$col]);
         }break;
         case 'Encrypted Text': {
            if (!$this->validated) {
               $k = new KLib($this->getsid());
               $keys = json_decode($k->getkeys());
               $this->data[$col] = decrypt($this->data[$col],$keys->k1,$keys->k2);
            }
            $this->data[$col] = $this->dbencrypt($this->data[$col]);
         }break;
         case 'Version': {
            /* 
               If there is a version column you need to check whether there is
               a value already and if there isn't then you need to update the 
               parent VersionOf table with the newly inserted version.
            */
            if (isset($params->VersionOfType) && ($this->data[$col] == 1)) {
               $type = $params->VersionOfType;
               $field = $params->VersionOfField;
               $link = null; 
               $link = new $type();
               $link->updateversion($this->data[$field],$this->tablename,1);
            }
         }break;
         case 'UUID': {
            if(!isset($this->data[$col])) $this->data[$col] = gen_uuid();
         }break;
         }         
      }
      if ($total>1) {
         $inserts = array();
         $data = $this->data;
         for($i=0;$i<$total;$i++) {
            $tdata = $data;
            $divor = 1;
            foreach($multiples as $prop) {
               $moder = count($data[$prop]); 
               $ind = intval($i/$divor)%$moder;
               $tdata[$prop] = $data[$prop][$ind];
               $divor *= $moder;
            }
            $inserts[] = $tdata;
         }
         foreach($inserts as $data) {
            $this->setdata($data);
            $pk = $this->getpk();
            $sq = strtolower($this->tablename).'_'.strtolower($pk).'_seq';
            //$id = $this->dbhandle->nextSequenceId($sq);
            //$this->dbhandle->insert(strtolower($this->tablename),array_change_key_case($this->data,CASE_LOWER));
            $id = $this->pgnativeinsert($data);
               
            if (isset($id)) {
               $this->data[$pk] = $id;
               $this->id = $id;
               $status = $id;
               foreach($ltmd as $col => $val) {
                  $type = $this->columns[$col];
                  if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $type = $params->DataType;
                  } else $type = preg_replace('/\:.+/','',$type);
                  $pk = $this->getpk();
                  if (isset($params)) {
                     $ttab = $params->TargetType;
                     $tcol = $params->TargetField;
                     $p = (isset($params->Property))?$params->Property:$col;
                  } else {
                     preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                     $ttab = $matches[2];
                     $tcol = $matches[3];
                     $p = $col;
                  }
                  $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                  $ltm->delete($pk,$id);
                  $vals = array();
                  if (preg_match('/^\[.+\]$/',$val)) {$vals = json_decode($val);} 
                  else if (is_array($val)) {$vals = $val;} 
                  else {$vals[] = $val;}
                  foreach($vals as $v) { 
                     $data = array(
                        $pk   => $id,
                        $tcol => $v
                     );
                     $ltm->setdata($data);
                     $ltm->insert();
                  }
               }
            }
            $message = ($status==0)?'Insert data failed':'Data inserted successfully';
            //$obj = $this->getview($settings);
            //$obj->Status = $status;
            //$obj->Message = $message;
         }
      } else {
         $t = strtolower($this->tablename);
         $d = array_change_key_case($this->data,CASE_LOWER);
         $pk = $this->getpk();
         $sq = strtolower($this->tablename).'_'.strtolower($pk).'_seq';
         //$id = $this->dbhandle->nextSequenceId($sq);
         //$this->dbhandle->insert(strtolower($this->tablename),array_change_key_case($this->data,CASE_LOWER));
         $id = $this->pgnativeinsert($d);
                  
         if (isset($id)) {
            $this->data[$pk] = $id;
            $this->id = $id;
            $status = $id;
            foreach($ltmd as $col => $val) {
               $type = $this->columns[$col];
               if (preg_match('/^\{/',$type)) {
                  $params = json_decode($type);
                  $type = $params->DataType;
               } else $type = preg_replace('/\:.+/','',$type);
               $pk = $this->getpk();
               if (isset($params)) {
                  $ttab = $params->TargetType;
                  $tcol = $params->TargetField;
                  $p = (isset($params->Property))?$params->Property:$col;
               } else {
                  preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                  $ttab = $matches[2];
                  $tcol = $matches[3];
                  $p = $col;
               }
               $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
//print_r($ltm);exit;
               $ltm->delete($pk,$id);
               $vals = array();
               if (preg_match('/^\[.+\]$/',$val)) {$vals = json_decode($val);} 
               else if (is_array($val)) {$vals = $val;} 
               else {$vals[] = $val;}
               foreach($vals as $v) {
                  if ($id != null && $v != null) { 
                     $data = array(
                        $pk   => $id,
                        $tcol => $v
                     );
                     $ltm->setdata($data);
                     $ltm->insert();
                  }
               }
            }
         }
      }
      return $status;   
   }
   function delete($col=null,$val=null) {
      if (!$this->exists()) $this->create();
      $lcol = strtolower($col);
      if (isset($val)&&isset($col)) {
         $clause = $this->dbhandle->quoteInto("$lcol = ?",$val);
      } else {
         $pk = $this->getpk();
         $val = intval($this->data[$pk]);
         $lpk = strtolower($pk);
         if ($val > 0) $clause = $this->dbhandle->quoteInto("$lpk = ?",$val);
      }
      // if $col is set it has to exist as a column of the table.
      if (isset($clause) && (!isset($col)|| isset($this->columns[$col]))) {
         $this->cascadedelete();
         $status = $this->dbhandle->delete(strtolower($this->tablename),$clause);
      }
      return $status;
   }
   function deleteAll() {
      $status = $this->dbhandle->delete($this->tablename);
      return $status;
   }
   function pgnativeupdate($icol,$id,$data) {
   /*
      This function has been included because the PDO abstracted library fails 
      to insert geometry data. This could be removed if a later version of 
      PHP fixes this problem. 
   */
      $dbpg = $this->dbpghandle;
      $tname = strtolower($this->tablename);
      $res = null;
      if (isset($dbpg)) {
         $cols = array();
         $vals = array();
         $udts = array();
         unset($data[$this->getpk()]);
         foreach($data as $col => $val) {
            $cols[] = $col;
            // get text statement from Zend_Db_Expr() instance //
            if (is_object($val)) $v = (isset($val->Id))?$val->Id:$val->__toString();
            // leave numeric data unquoted // 
            else if (is_bool($val)) $v = ($val)?'true':'false';
            else if (is_numeric($val)) $v = $val; 
            // and then quote text data for the insert statement //
            else $v = (isset($val) && strlen($val)>0)?"'$val'":'null';
            $vals[] = $v;
            // update data array for pg_insert call if using // 
            $data[$col] = $v;
            $lcol = strtolower($col);
            $udts[] = "$lcol = $v";
         }
         $udtstring = join(',',$udts);     
         $licol = strtolower($icol);      
         $sql = "UPDATE $tname SET $udtstring WHERE($licol = $id);";
//print $sql;exit;
         $res = pg_query($dbpg,$sql);
         //$res = pg_update($dbpg,$tname,$data);
         //pg_close($dbpg);
      }
      return $res;
   }                                    
   function update($col,$val) {
      try {
         if (!$this->validated) $this->validate();
         if (isset($this->data['CreatedBy'])) unset($this->data['CreatedBy']);
         if (isset($this->data['CreatedOn'])) unset($this->data['CreatedOn']);
         foreach($this->columns as $dcol => $type) {
            if (preg_match('/^\{/',$type)) {
               $params = json_decode($type);
               $type = $params->DataType;         
            } else $type = preg_replace('/\:.+/','',$type);
            if (isset($params) && isset($params->Hidden)) {
               unset($this->data[$dcol]);
            } else {
               switch ($type) {
               case 'Unique Key': {
                  unset($this->data[$dcol]);
               }break;
               case 'Map Data': {
                  if (isset($this->data[$dcol]) && is_string($this->data[$dcol])) {
                     $wkt = $this->data[$dcol];
                     //$this->data[$dcol] = new Zend_Db_Expr("st_geomfromtext('$wkt',4326)");
                     $this->data[$dcol] = $this->insertterm($dcol,$type,$wkt);
                  }
               }break;
               case 'True or False': {
                  if (is_numeric($this->data[$dcol])) $this->data[$dcol] = ($this->data[$dcol] == 1);
               }break;
               case 'INTERFACE': {
                  if (isset($this->data[$dcol]) && is_object($this->data[$dcol])) {
                     $obj = $this->data[$dcol];
                     $cache = array();
                     $cache['Data_Type'] = $obj->Data_Type;
                     $cache['Name'] = $obj->Name;
                     $cache['Value_Type'] = (is_integer($obj->Value))?'Number':'Short Text';
                     $cache['Value'] = $obj->Value;
                     $cache['Domain'] = $obj->In_Domain; 
                     $cache['JSON'] = json_encode($obj);
                     $c = new G_Cache();
                     $c->setdata($cache);
                     $interfaceid = $c->insert();
                     $this->data[$dcol] = $interfaceid;
                  }
               }break;
               case 'Taxa': {
                  if (isset($this->data[$dcol]) && is_object($this->data[$dcol])) {
                     $obj = $this->data[$dcol];
                     $cache = array();
                     $cache['Data_Type'] = $obj->Data_Type;
                     $cache['Name'] = $obj->Name;
                     $cache['Value_Type'] = (is_integer($obj->Value))?'Number':'Short Text';
                     $cache['Value'] = $obj->Value;
                     $cache['Domain'] = $obj->In_Domain; 
                     $cache['JSON'] = json_encode($obj);
                     $c = new G_Cache();
                     $c->setdata($cache);
                     $interfaceid = $c->insert();
                     $this->data[$dcol] = $interfaceid;
                  }
               }break;
               case 'LINKEDTOM': {
                  $lookuptable = $this->tablename."_${dcol}";
                  $pk = $this->getpk();
                  if (isset($params)) {
                     $ttab = $params->TargetType;
                     $tcol = $params->TargetField;
                     $p = (isset($params->Property))?$params->Property:$dcol;
                  } else {
                     preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$dcol],$matches);
                     $ttab = $matches[2];
                     $tcol = $matches[3];
                     $p = $dcol;
                  }
                  $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                  $ltm->delete($pk,$val);
                  $data = array(
                     $pk   => $val,
                     $tcol => $this->data[$dcol]
                  );
                  if (is_array($data[$tcol])) {
                     $ivals = $data[$tcol];
                     foreach ($ivals as $ival) {
                        $idata = $data;
                        $idata[$tcol] = $ival;
                        if ($val != null && $ival != null) {
                           $ltm->setdata($idata);
                           $ltm->insert();
                        }
                     }
                  } else if (preg_match('/\[/',$data[$tcol])) {
                     $ivals = json_decode($data[$tcol]);
                     foreach ($ivals as $ival) {
                        $idata = $data;
                        $ival = (is_object($ival))?$ival->Id:$ival;
                        $idata[$tcol] = $ival;
                        if ($val != null && $ival != null) {
                           $ltm->setdata($idata);
                           $ltm->insert();
                        }
                     }
                  } else {
                     $ival = $this->data[$dcol]; 
                     if ($val != null && $ival != null) {
                        $ltm->setdata($data);
                        $ltm->insert();
                     }
                  }
                  unset($this->data[$dcol]); 
               }break;
               case 'Password': {
                  if (!$this->validated) {
                     $k = new KLib($this->getsid());
                     $keys = json_decode($k->getkeys());
                     $this->data[$dcol] = decrypt($this->data[$dcol],$keys->k1,$keys->k2);
                  }
                  $this->data[$dcol] = $this->dbencrypt($this->data[$dcol]);
               }break;
               case 'Encrypted Text': {
                  if (!$this->validated) {
                     $k = new KLib($this->getsid());
                     $keys = json_decode($k->getkeys());
                     $this->data[$dcol] = decrypt($this->data[$dcol],$keys->k1,$keys->k2);
                  }
                  $this->data[$dcol] = $this->dbencrypt($this->data[$dcol]);
               }break;
               case 'UUID': {
                  if(!isset($this->data[$dcol])) $this->data[$dcol] = gen_uuid();
               }break;
               }
            }
         }
         //$where = $this->dbhandle->quoteInto("$col = ?",$val);
         //$status = $this->dbhandle->update($this->tablename,$this->data,$where);
         $status = $this->pgnativeupdate($col,$val,$this->data);
         $this->data[$col] = $val;
         $return = 1;
      } catch (Exception $e) {
         $return = 0;
      }
      return $return;
   }
   function loaddata($id,$col=null,$innerdata=true) {
      $ismatched = false;
      if (!isset($col)) $col = $this->getpk();
      
      // NEED TO MAKE THIS WORK FOR {NAME,ID} AND FOR GENUINE OBJ SEARCH
      if (is_object($id)) $id = json_encode($id);
      if (isset($id) && $this->exists()) {
         $pk = $this->getpk();
         if (!$col) $col = $pk; 
         if (is_object($id)) $id = $id->Id;
         $query = $this->dbhandle->select();
         $cols = array();
         $ltmd = array();
         foreach ($this->columns as $dcol => $type) {
            if (preg_match('/^\{/',$type)) {
               $params = json_decode($type);
               $type = $params->DataType;         
            } else $type = preg_replace('/\:.+/','',$type);
            if (!isset($params->Hidden) || !$params->Hidden) {
               switch ($type) {
               case 'Map Data': {
                  //$cols[$dcol] = new Zend_Db_Expr("ST_AsText($dcol)");
                  $cols[$dcol] = $this->selectterm($dcol,$type);               
               } break;
               case 'LINKEDTOM': {
                  if ($innerdata) {
                     if (isset($params)) {
                        $ttab = $params->TargetType;
                        $tcol = $params->TargetField;
                        $p = (isset($params->Property))?$params->Property:$dcol;
                     } else {
                        preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$dcol],$matches);
                        $ttab = $matches[2];
                        $tcol = $matches[3];
                        $p = $dcol;
                     }
                     $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                     $obj = new stdClass();
                     $obj->IType = $ttab;
                     $obj->Current = 'Multiple';
                     $obj->Key = $tcol;
                     $obj->ListData = $ltm->selecttargets($pk,$id);
                     $ltmd[$dcol] = $obj;
                  }
               }break;
               default: {
                  //$cols[$dcol] = $dcol;
                  $cols[$dcol] = $this->selectterm($dcol,$type); 
               } break;
               }
            }   
         }
         $query->from(strtolower($this->tablename), $cols);
         $lcol = strtolower($col);
         $query->where("$lcol = ?",$id);
//echo $query->__toString();exit;         
         $this->data = $this->dbhandle->fetchRow($query);
         if (isset($ltmd) && count($ltmd)) {
            foreach($ltmd as $dcol => $dval) {
               $this->data[$dcol] = $dval;
            }
         }
         $ismatched = (is_array($this->data) && $this->getid()>0);
         if ($ismatched) {
            foreach($this->data as $col => $val) {
               $type = $this->columns[$col];
               if (preg_match('/^\{/',$type)) {
                  $params = json_decode($type);
                  $type = $params->DataType;         
               } else $type = preg_replace('/\:.+/','',$type);
               $this->data[$col] = $this->postprocessterm($col,$type,$val);   
            }
         }
         $this->id = $this->getid();
      }
//print_r($this->data);
      return $ismatched;
   }
   function loadone($id,$col=null,$vtype=null,$byval=null) {
      if (is_object($id)) $id = json_encode($id);
      if ($id > 0 && $this->exists()) {
         $pk = $this->getpk();
         if (!$col) $col = $pk;
         $lcol = strtolower($col); 
         //if (is_object($id)) $id = $id->Id;
         $query = $this->dbhandle->select();
         $cols = array();
         $ltmd = array();
         foreach ($this->columns as $dcol => $type) {
            $ldcol = strtolower($dcol);
            if (preg_match('/^\{/',$type)) {
               $params = json_decode($type);
               $type = $params->DataType;         
            } else $type = preg_replace('/\:.+/','',$type);
            if (!$params->Hidden) {
               switch ($type) {
               case 'Map Data': {
                  $cols[$dcol] = new Zend_Db_Expr("ST_AsText($ldcol)");               
               } break;
               case 'LINKEDTOM': {
                  // insert linkedtom management
                  if (isset($params)) {
                     $ttab = $params->TargetType;
                     $tcol = $params->TargetField;
                     $p = (isset($params->Property))?$params->Property:$dcol;
                  } else {
                     preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                     $ttab = $matches[2];
                     $tcol = $matches[3];
                     $p = $dcol;
                  }
                  $pk = $this->getpk();
                  $lid = $this->getid();
                  $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                  $obj = new stdClass();
                  $obj->IType = $ttab;
                  $obj->ListData = $ltm->selecttargets($pk,$lid);
                  $ltmd[$dcol] = $obj;
               } break;
               default: {
                  $cols[$dcol] = $ldcol; 
               } break;
               }
            }   
         }
            
         $query->from(strtolower($this->tablename), $cols);
         $query->where("$lcol = ?",$id);
         switch($vtype) {
         case 'Current': {
            $vercol = null;
            foreach ($this->columns as $dcol => $ctype) {
               if (preg_match('/^\{/',$ctype)) {
                  $cparams = json_decode($ctype);
                  $ctype = $cparams->DataType;         
               } else $ctype = preg_replace('/\:.+/','',$ctype);
               if ($ctype == 'Version') $vercol = $dcol;
            }
            if (isset($vercol)&&isset($byval)) {
            /* For some reason = will not always match float values 
               so it is necessary to add 2 clauses to sandwich the required 
               version number */ 
               $byval = floatval($byval);
               $lvercol = strtolower($vercol);
               $query->where("$lvercol > ?", $byval-0.05);
               $query->where("$lvercol < ?", $byval+0.05);
            }
         }break;
         case 'Last': {
            $countcol = null;
            foreach ($this->columns as $dcol => $ctype) {
               if (preg_match('/^\{/',$ctype)) {
                  $cparams = json_decode($ctype);
                  $ctype = $cparams->DataType;         
               } else $ctype = preg_replace('/\:.+/','',$ctype);
               if ($ctype == 'Count') $countcol = $dcol;
            }
            if (isset($countcol)) {
               $lcountcol = strtolower($countcol);
               $query->order("$lcountcol DESC");
            }
         }break;
         case 'Present': {
            $dfcol = null;
            $dtcol = null;
            foreach ($this->columns as $dcol => $ctype) {
               if (preg_match('/^\{/',$ctype)) {
                  $cparams = json_decode($ctype);
                  $ctype = $cparams->DataType;         
               } else $ctype = preg_replace('/\:.+/','',$ctype);
               if ($ctype == 'Date From')    $dfcol = $dcol;
               if ($ctype == 'Date Until')   $dtcol = $dcol;
            }
            if (isset($dfcol)) {   
               $query->where("$dfcol <= CURRENT_DATE");//?", new Zend_Db_Expr("CURDATE()"));
               $query->where("$dtcol >= CURRENT_DATE OR $dtcol IS NULL");//?", new Zend_Db_Expr("CURDATE()"));
            }
         }break;
         case 'Next': {
            $datecol = null;
            foreach ($this->columns as $dcol => $ctype) {
               if (preg_match('/^\{/',$ctype)) {
                  $cparams = json_decode($ctype);
                  $ctype = $cparams->DataType;         
               } else $ctype = preg_replace('/\:.+/','',$ctype);
               if ($ctype == 'Date') $datecol = $dcol;
            }
            if (isset($datecol)) {
               $ldatecol = strtolower($datecol);   
               $query->where("$ldatecol >= CURRENT_DATE");//?", new Zend_Db_Expr("CURDATE()"));
               $query->order("$ldatecol ASC");
            }
         }break;
         case 'Previous': {
            $datecol = null;
            foreach ($this->columns as $dcol => $ctype) {
               if (preg_match('/^\{/',$ctype)) {
                  $cparams = json_decode($ctype);
                  $ctype = $cparams->DataType;         
               } else $ctype = preg_replace('/\:.+/','',$ctype);
               if ($ctype == 'Date') $datecol = $dcol; 
            }
            if (isset($datecol)) {
               $ldatecol = strtolower($datecol);   
               $query->where("$ldatecol < CURRENT_DATE");//?", new Zend_Db_Expr("CURDATE()"));
               $query->order("$ldatecol DESC");
            }   
         }break;
         }
//echo $query->__toString();  
         $this->data = $this->dbhandle->fetchRow($query);
         foreach ($this->columns as $dcol => $ctype) {
            if (preg_match('/^\{/',$ctype)) {
               $cparams = json_decode($ctype);
               $ctype = $cparams->DataType;         
            } else $ctype = preg_replace('/\:.+/','',$ctype);
            if ($ctype == 'Date') $datecol = $dcol;
            switch($ctype) {
            case 'LINKEDTOM': {
               // insert linkedtom management
               if (isset($cparams)) {
                  $ttab = $cparams->TargetType;
                  $tcol = $cparams->TargetField;
                  $p = (isset($cparams->Property))?$cparams->Property:$dcol;
               } else {
                  preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                  $ttab = $matches[2];
                  $tcol = $matches[3];
                  $p = $dcol;
               }
               $pk = $this->getpk();
               $lid = $this->getid();
               $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
               $obj = new stdClass();
               $obj->IType = $ttab;
               $obj->ListData = $ltm->selecttargets($pk,$lid);
               $ltmd[$dcol] = $obj;
            } break;
            default: {
               $this->data[$col] = $this->postprocessterm($col,$type,$val);
            } break; 
            } 
         }
            
         if (count($ltmd)) {
            foreach($ltmd as $dcol => $dval) {
               $this->data[$dcol] = $dval;
            }
         }
         $this->id = $this->getid();
      }
   }
   function search($val) {
      $result = array();
      if ($this->exists()) {
         $ltab = strtolower($this->tablename);
         $query = $this->dbhandle->select();
         $pk = $this->getpk();
         $lpk = strtolower($pk); 
         $cols = array($pk => $lpk);
         $query->from($ltab, $cols);
         $matches = 0;
         foreach ($this->columns as $dcol => $type) {
            $lcol = strtolower($dcol);
            if (preg_match('/^\{/',$type)) {
               $params = json_decode($type);
               $type = $params->DataType;         
            } else $type = preg_replace('/\:.+/','',$type);
            switch($type) {
            case 'Short Text': {
               if ($matches == 0) $query->where("$lcol LIKE ?",$val);
               else $query->orwhere("$lcol LIKE ?",$val);
               $matches++;
            }break; 
            }
         }
         $result = $this->dbhandle->fetchAll($query);                           
      }
      return $result;      
   }
   function selectRowCount($col,$val,$settings=null) {
      $rowcount = array(0);
      if (isset($settings)) {
         $itype = $this->gettablename();
         $isettings = $settings->$itype;
         $sby = (isset($isettings->SearchBy))?$isettings->SearchBy:null;
         $sfor = (isset($isettings->SearchFor))?$isettings->SearchFor:null;
         $stype = (isset($isettings->SearchType))?$isettings->SearchType:null;
      }
      if (isset($col)&&isset($val)) {
         if ($this->exists()) {
            $query = $this->dbhandle->select();
            $cols = array('count' => 'COUNT(*)');
            $query->from(strtolower($this->tablename), $cols);
            $lcol = strtolower($col);
            if (preg_match('/\%/',$val)) $query->where("$lcol LIKE ?",$val);
            else $query->where("$lcol = ?",$val);
            if (isset($sby)) {
               $stype = "".$this->columns[$sby];
               if (preg_match('/^\{/',$stype)) {
                  $params = json_decode($stype);
                  $stype = $params->DataType;
               } else {
                  $bits = preg_split('/\:/',$stype);
                  $stype = $bits[0];
               }
               switch($stype) {
               case 'INTERFACE': {
                  $cache = new G_Cache();
                  $ints = $cache->select('Name',"%$sfor%");
                  $sfors = array();
                  foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
                  $query->where("$sby IN(?)",$sfors);
               }break;
               case 'Taxa': {
                  $cache = new G_Cache();
                  $ints = $cache->select('Name',"%$sfor%");
                  $sfors = array();
                  foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
                  $query->where("$sby IN(?)",$sfors);
               }break;
               case 'Date': {
                  if (preg_match('/([>,<]\=*)/',$sfor,$m)) $sop = $m[1];
                  else $sop = '=';
                  if ($sop == '') $sop = '=';
                  $sfor = preg_replace('/[>,<]\=*\s*/','',$sfor);
                  if (preg_match('/(\d{2})\/(\d{2})\/(\d{4})/',$sfor,$m)) $sfor = $m[3]."-".$m[2]."-".$m[1];
                  if (preg_match('/^\d{4}$/',$sfor)) $sby = new Zend_Db_Expr("year($sby)");
                  $scol = (preg_match('/^\d{4}$/',$sfor))?new Zend_Db_Expr("year($sby)"):$sby;
                  $query->where("$scol $sop ?",$sfor);
               }break;
               /* linked data is handled separately */
               /* case 'LINKEDTO':break; */
               case 'LINKEDTO': {
                  if (isset($params)) {
                     $type = $params->DataType;
                     $table = $params->TargetType;
                     $column = $params->TargetField;
                     $link = null;
                     $link = new $table();
                     $lpk = $link->getpk();
                     $ints = $link->search("%$sfor%");
                     $sfors = array();
                     $lsby = strtolower($sby);
                     foreach($ints as $i => $int) $sfors[] = $int[$lpk]; 
                     $query->where("$lsby IN(?)",$sfors);
                  }   
               }break;
               case 'LINKEDTOM':break;
               case 'Map Data': {
                  switch($stype) {
                  case 'Contains': $stype = 'st_contains'; break;
                  case 'Within': $stype = 'st_within'; break; 
                  default: $stype = 'st_intersects';break;                     
                  }
                  $sfor = new Zend_Db_Expr("st_geomfromtext('$sfor',4326)");
                  $query->where("$stype($scol,?)",$sfor);  
               }break;
               default: {
                  if (preg_match('/\,/',$sfor)) {
                     $sfors = preg_split('/\s*\,\s*/',$sfor); 
                     $query->where("$sby IN(?)",$sfors);
                  } else {
                     $query->where("$sby LIKE ?","%$sfor%");
                  }
               }break;
               }  
            }
            if ($settings) {
               $settings = (array) $settings;
               foreach($this->columns as $col => $type) {
                  $inherit = false;
                  if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $inherit = $params->Inherit;
                     $dtype = $params->DataType;
                     $itype = $params->TargetType;
                  } else {
                     $types = preg_split('/\:/',$type);
                     $dtype = $types[0];
                     $inherit = in_array('Inherit',$types);
                     if ($inherit) {
                        preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                        $itype = $matches[2];
                     } 
                  }
                  if ($inherit && isset($settings[$itype]) 
                           && isset($settings[$itype]->Current)
                           && (isset($dtype) && ($dtype != 'LINKEDTOM'))) {
                     $query->where("$col = ?",$settings[$itype]->Current);
                  }
               }
            }
            $rowcount = $this->dbhandle->fetchCol($query);
         }
      }
      return $rowcount[0];
   }
   function select($col,$val,$settings=null,$where=null,$page=null) {
      $result = array();
      if (isset($settings)) {
         $itype = $this->gettablename();
         $isettings = $settings->$itype;
         $sby = (isset($isettings->SearchBy))?$isettings->SearchBy:null;
         $sfor = (isset($isettings->SearchFor))?$isettings->SearchFor:null;
         $stype = (isset($isettings->SearchType))?$isettings->SearchType:null;
      }
      if (isset($col)&&isset($val)) {
         if ($this->exists()) {
            $query = $this->dbhandle->select();
            $cols = array();
            $ltmd = array();
            $order = null;
            foreach ($this->columns as $dcol => $type) {
               // don't select the column you already know. 
               // if this is a link it can lead to recursion
               if ($dcol != $col) {
                  if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $type = $params->DataType;         
                  } else $type = preg_replace('/\:.+/','',$type);
                  if (!isset($params->Hidden) || !$params->Hidden) {
                     $term = $this->selectterm($dcol,$type);
                     switch ($type) {
                     case 'Version': {
                        $order = "$term ASC";
                        $cols[$dcol] = $term; 
                     } break;
                     case 'Number': {
                        $order = "$term ASC";
                        $cols[$dcol] = $term; 
                     } break;
                     case 'Date': {
                        $order = "$term DESC";
                        $cols[$dcol] = $term; 
                     } break;
                     case 'Map Data': {
                        $cols[$dcol] = $term;               
                     } break;
                     case 'LINKEDTOM': {
                        // insert linkedtom management
                        if (isset($params)) {
                           $ttab = $params->TargetType;
                           $tcol = $params->TargetField;
                           $p = (isset($params->Property))?$params->Property:$dcol;
                        } else {
                           preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                           $ttab = $matches[2];
                           $tcol = $matches[3];
                           $p = $dcol;
                        }
                        $pk = $this->getpk();
                        $id = $this->getid();
                        $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                        $obj = new stdClass();
                        $obj->IType = $ttab;
                        $obj->ListData = $ltm->selecttargets($pk,$id);
                        $ltmd[$dcol] = $obj;
                     } break;
                     default: {
                        $cols[$dcol] = $term; 
                     } break;
                     }
                  }
               }
            }
            $query->from(strtolower($this->tablename), $cols);
            $lcol = strtolower($col);
            if (preg_match('/\%/',$val)) $query->where("$lcol LIKE ?",$val);
            else $query->where("$lcol = ?",$val);
            if (isset($where) && is_array($where)) {
               foreach($where as $col => $val){
				      $lcol = strtolower($col);
                  $def = $this->getcoldef($col);
                  $type = $def->DataType;
                  switch($type) {
                  case "Map Data": {
                     $sfor = new Zend_Db_Expr("st_geomfromtext('$val',4326)");
                     $query->where("st_contains($lcol,?)",$sfor);
               //echo $query->__toString();exit;
                     $settings = null;
                  }break;
                  default: $query->where("$lcol = ?",$val);break;
                  }  
               } 
            }
            if (isset($sby)) {
               $stype = "".$this->columns[$sby];
               if (preg_match('/^\{/',$stype)) {
                  $params = json_decode($stype);
                  $stype = $params->DataType;
               } else {
                  $bits = preg_split('/\:/',$stype);
                  $stype = $bits[0];
               }
               switch($stype) {
               case 'INTERFACE': {
                  $cache = new G_Cache();
                  $ints = $cache->select('Name',"%$sfor%");
                  $sfors = array();
                  foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
                  $query->where("$sby IN(?)",$sfors);
               }break;
               case 'Taxa': {
                  $cache = new G_Cache();
                  $ints = $cache->select('Name',"%$sfor%");
                  $sfors = array();
                  foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
                  $query->where("$sby IN(?)",$sfors);
               }break;
               case 'Date': {
                  if (preg_match('/([>,<]\=*)/',$sfor,$m)) $sop = $m[1];
                  else $sop = '=';
                  if ($sop == '') $sop = '=';
                  $sfor = preg_replace('/[>,<]\=*\s*/','',$sfor);
                  if (preg_match('/(\d{2})\/(\d{2})\/(\d{4})/',$sfor,$m)) $sfor = $m[3]."-".$m[2]."-".$m[1];
                  if (preg_match('/^\d{4}$/',$sfor)) $sby = new Zend_Db_Expr("year($sby)");
                  $scol = (preg_match('/^\d{4}$/',$sfor))?new Zend_Db_Expr("year($sby)"):$sby;
                  $query->where("$scol $sop ?",$sfor);
               }break;
               /* linked data is handled separately */
               /* case 'LINKEDTO':break; */
               case 'LINKEDTO': {
                  if (isset($params)) {
                     $type = $params->DataType;
                     $table = $params->TargetType;
                     $column = $params->TargetField;
                     $link = null;
                     $link = new $table();
                     $lpk = $link->getpk();
                     $ints = $link->search("%$sfor%");
                     $sfors = array();
                     $lsby = strtolower($sby);
                     foreach($ints as $i => $int) $sfors[] = $int[$lpk]; 
                     $query->where("$lsby IN(?)",$sfors);
                  }   
               }break;
               case 'LINKEDTOM':break;
               case 'Map Data': {
                  switch($stype) {
                  case 'Contains': $stype = 'st_contains'; break;
                  case 'Within': $stype = 'st_within'; break; 
                  default: $stype = 'st_intersects';break;                     
                  }
                  $sfor = new Zend_Db_Expr("st_geomfromtext('$sfor',4326)");
                  $query->where("$stype($scol,?)",$sfor);  
               }break;
               default: {
                  if (preg_match('/\,/',$sfor)) {
                     $sfors = preg_split('/\s*\,\s*/',$sfor); 
                     $query->where("$sby IN(?)",$sfors);
                  } else {
                     $query->where("$sby LIKE ?","%$sfor%");
                  }
               }break;
               }  
            }
            $ordered = false;
            if (isset($settings)) {
               $settings = (array) $settings;
               foreach($this->columns as $col => $type) {
                  $inherit = false;
                  if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $inherit = $params->Inherit;
                     $dtype = $params->DataType;
                     $itype = $params->TargetType;
                  } else {
                     $types = preg_split('/\:/',$type);
                     $dtype = $types[0];
                     $inherit = in_array('Inherit',$types);
                     if ($inherit) {
                        preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                        $itype = $matches[2];
                     } 
                  }
                  if ($inherit && isset($settings[$itype]) 
                           && isset($settings[$itype]->Current)
                           && (isset($dtype) && ($dtype != 'LINKEDTOM'))) {
                     $query->where("$col = ?",$settings[$itype]->Current);
                  }
                  if (isset($settings[$itype]) && isset($settings[$itype]->Orders)) {
                     $query->reset( Zend_Db_Select::ORDER );
                     foreach($settings[$itype]->Orders as $oobj) {
                        if ($this->columns[$oobj->Column]) {
                           $lcol = strtolower($oobj->Column);
                           $dirn = $oobj->Order;
                           switch($dirn) {
                           case 'Up':     $query->order("$lcol ASC");   break;
                           case 'Down':   $query->order("$lcol DESC");  break;
                           default:       $query->order("$lcol $dirn"); break;
                           }
                           $ordered = true;
                        }
                     }
                  }
               }
            }
            if (!$ordered) {
               if (isset($this->orders) && count($this->orders)>0) {
                  $query->reset( Zend_Db_Select::ORDER );
                  foreach ($this->orders as $col => $dir) {
                     $lcol = strtolower($col);
                     switch($dir) {
                     case 'Up':     $query->order("$lcol ASC");  break;
                     case 'Down':   $query->order("$lcol DESC"); break;   
                     default:       $query->order("$col $dir"); break;
                     }
                  }
               } else if (isset($order)) $query->order($order);
            }
            if (isset($page)) $query->limitPage($page,$this->perpage);
            $result = $this->dbhandle->fetchAll($query);
            foreach($result as $rownum => $row) {
               $match = true;
               foreach ($row as $col => $val) {
                  $type = "".$this->columns[$col];
                  if (preg_match('/^\{/',$type)) {
                     $params = json_decode($type);
                     $type = $params->DataType;
                  } else {
                     $bits = preg_split('/\:/',$type);
                     $type = $bits[0];
                  }
                  switch ($type) {
                  case 'LINKEDTO': {
                     if (isset($params)) {
                        if (!is_object($val)) {
                           $type = $params->DataType;
                           $table = $params->TargetType;
                           $column = $params->TargetField;
                           $link = null;
                           $link = new $table();
                           $link->loaddata($val,$column,false);
                           $obj = new stdClass();
                           $obj->Name = $link->getname();
                           $obj->Id = $link->getid();
                           $icon = $link->geticon();
                           if (isset($icon)) $obj->Icon = $icon;
                           $result[$rownum][$col] = $obj;
                           if (isset($sby) && $col == $sby) {
                              $match = preg_match("/$sfor/",$obj->Name); 
                           }
                        }
                     }   
                  }break;
                  case 'LINKEDTOM': {
                     if (isset($params)) {
                        if (!is_object($val)) {
                           $type = $params->DataType;
                           $ttab = $params->TargetType;
                           $tcol = $params->TargetField;
                           $p = (isset($params->Property))?$params->Property:$col;
                           $pk = $this->getpk();
                           $id = $this->getid();
                           $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                           $obj = new stdClass();
                           $obj->IType = $ttab;
                           $obj->ListData = $ltm->selecttargets($pk,$row[$pk]);
                           $result[$rownum][$col] = $obj;
                           $result[$rownum][$col] = $this->postprocessterm($col,$type,$val,$params);
                           if (isset($sby) && $col == $sby) {
                              $match = false;
                              foreach($obj->ListData as $item => $liobj) {
                                 $match = $match || preg_match("/$sfor/",$liobj->Name);
                              } 
                           }
                        }
                     }   
                  }break;
                  default: {
                     $result[$rownum][$col] = $this->postprocessterm($col,$type,$val);
                  }break;
                  }
               }
               if (!$match) unset($result[$rownum]);
            }      
         }
      }
      $result = array_values($result);
      return $result;   
   }
   function selectAllRowCount($settings = null) {
      $rowcount = array(0);
      $t = $this->tablename;
      $sby = (isset($settings->$t->SearchBy))?$settings->$t->SearchBy:null;
      $sfor = (isset($settings->$t->SearchFor))?$settings->$t->SearchFor:null;
      if ($this->exists()) {
         $query = $this->dbhandle->select();
         $cols = array('count' => 'COUNT(*)');
         $query->from(strtolower($this->tablename), $cols);
         if (isset($sby)) {
            $stype = "".$this->columns[$sby];
            if (preg_match('/^\{/',$stype)) {
               $params = json_decode($stype);
               $stype = $params->DataType;
            } else {
               $bits = preg_split('/\:/',$stype);
               $stype = $bits[0];
            }
            switch($stype) {
            case 'INTERFACE': {
               $cache = new G_Cache();
               $ints = $cache->select('Name',"%$sfor%");
               $sfors = array();
               foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
               $query->where("$sby IN(?)",$sfors);
            }break;
            case 'Taxa': {
               $cache = new G_Cache();
               $ints = $cache->select('Name',"%$sfor%");
               $sfors = array();
               foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
               $query->where("$sby IN(?)",$sfors);
            }break;
            case 'Date': {
               if (preg_match('/([>,<]\=*)/',$sfor,$m)) $sop = $m[1];
               else $sop = '=';
               if ($sop == '') $sop = '=';
               $sfor = preg_replace('/[>,<]\=*\s*/','',$sfor);
               if (preg_match('/(\d{2})\/(\d{2})\/(\d{4})/',$sfor,$m)) $sfor = $m[3]."-".$m[2]."-".$m[1];
               $scol = (preg_match('/^\d{4}$/',$sfor))?new Zend_Db_Expr("YEAR($sby)"):$sby;
               $query->where("$scol $sop ?",$sfor);
            }break;
            /* linked data is handled separately */
            /* case 'LINKEDTO':break; */
            case 'LINKEDTO': {
               if (isset($params)) {
                  $type = $params->DataType;
                  $table = $params->TargetType;
                  $column = $params->TargetField;
                  $link = null;
                  $link = new $table();
                  $lpk = $link->getpk();
                  $ints = $link->search("%$sfor%");
                  $sfors = array();
                  $lsby = strtolower($sby);
                  foreach($ints as $i => $int) $sfors[] = $int[$lpk]; 
                  $query->where("$lsby IN(?)",$sfors);
               }   
            }break;
            case 'LINKEDTOM':break;
            default: {
               if (preg_match('/\,/',$sfor)) {
                  $sfors = preg_split('/\s*\,\s*/',$sfor); 
                  $query->where("$sby IN(?)",$sfors);
               } else {
                  $query->where("$sby LIKE ?","%$sfor%");
               }
            }break;
            }  
         }
         if ($settings) {
            $settings = (array) $settings;
            foreach($this->columns as $col => $type) {
               $inherit = false;
               if (preg_match('/^\{/',$type)) {
                  $params = json_decode($type);
                  $inherit = (isset($params->Inherit))?$params->Inherit:0;
                  $itype = (isset($params->TargetType))?$params->TargetType:null;
               } else {
                  $types = preg_split('/\:/',$type);
                  $inherit = in_array('Inherit',$types);
                  if ($inherit) {
                     preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                     $itype = $matches[2];
                  } 
               }
               if (($inherit) && isset($settings[$itype]) && isset($settings[$itype]->Current)) {
                  if (!isset($params) || ($params->DataType != 'LINKEDTOM'))
                     $query->where("$col = ?",$settings[$itype]->Current);
               }
            }
         }
         $rowcount = $this->dbhandle->fetchCol($query);
      }
      return $rowcount[0];
   }
   function selectAll($settings = null,$page = null) {
      $result = array();
      $t = $this->tablename;
      $sby = (isset($settings->$t->SearchBy))?$settings->$t->SearchBy:null;
      $sfor = (isset($settings->$t->SearchFor))?$settings->$t->SearchFor:null;
      if ($this->exists()) {
         $query = $this->dbhandle->select();
         $cols = array();
         foreach ($this->columns as $dcol => $type) {
            if (preg_match('/^\{/',$type)) {
               $params = json_decode($type);
               $type = $params->DataType;
            } else $type = preg_replace('/\:.+/','',$type);
            if (!isset($params) || !isset($params->Hidden) || !$params->Hidden) {
               switch ($type) {
               case 'Map Data': {
                  //$cols[$dcol] = new Zend_Db_Expr("ST_AsText($dcol)");
                  $cols[$dcol] = $this->selectterm($dcol,$type);               
               } break;
               case 'LINKEDTOM': {
                  $ltmd[] = $dcol;
               } break;
               default: {
                  //$cols[$dcol] = $dcol; 
                  $cols[$dcol] = $this->selectterm($dcol,$type);               
               } break;
               }
            }   
         }
            
         $query->from(strtolower($this->tablename), $cols);
         if (isset($sby)) {
            $stype = "".$this->columns[$sby];
            if (preg_match('/^\{/',$stype)) {
               $params = json_decode($stype);
               $stype = $params->DataType;
            } else {
               $bits = preg_split('/\:/',$stype);
               $stype = $bits[0];
            }
            switch($stype) {
            case 'INTERFACE': {
               $cache = new G_Cache();
               $ints = $cache->select('Name',"%$sfor%");
               $sfors = array();
               foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
               $query->where("$sby IN(?)",$sfors);
            }break;
            case 'Taxa': {
               $cache = new G_Cache();
               $ints = $cache->select('Name',"%$sfor%");
               $sfors = array();
               foreach($ints as $i => $int) $sfors[] = $int['G_Cache_ID']; 
               $query->where("$sby IN(?)",$sfors);
            }break;
            case 'Date': {
               if (preg_match('/([>,<]\=*)/',$sfor,$m)) $sop = $m[1];
               else $sop = '=';
               if ($sop == '') $sop = '=';
               $sfor = preg_replace('/[>,<]\=*\s*/','',$sfor);
               if (preg_match('/(\d{2})\/(\d{2})\/(\d{4})/',$sfor,$m)) $sfor = $m[3]."-".$m[2]."-".$m[1];
               $scol = (preg_match('/^\d{4}$/',$sfor))?new Zend_Db_Expr("YEAR($sby)"):$sby;
               $query->where("$scol $sop ?",$sfor);
            }break;
            /* linked data is handled separately */
            /* case 'LINKEDTO':break; */
            case 'LINKEDTO': {
               if (isset($params)) {
                  $type = $params->DataType;
                  $table = $params->TargetType;
                  $column = $params->TargetField;
                  $link = null;
                  $link = new $table();
                  $lpk = $link->getpk();
                  $ints = $link->search("%$sfor%");
                  $sfors = array();
                  $lsby = strtolower($sby);
                  foreach($ints as $i => $int) $sfors[] = $int[$lpk]; 
                  $query->where("$lsby IN(?)",$sfors);
               }   
            }break;
            case 'LINKEDTOM':break;
            default: {
               if (preg_match('/\,/',$sfor)) {
                  $sfors = preg_split('/\s*\,\s*/',$sfor); 
                  $query->where("$sby IN(?)",$sfors);
               } else {
                  $query->where("$sby LIKE ?","%$sfor%");
               }
            }break;
            }  
         }
         $ordered = false;
         if ($settings) {
            $settings = (array) $settings;
            foreach($this->columns as $col => $type) {
               $inherit = false;
               if (preg_match('/^\{/',$type)) {
                  $params = json_decode($type);
                  $inherit = (isset($params->Inherit))?$params->Inherit:0;
                  $itype = (isset($params->TargetType))?$params->TargetType:null;
               } else {
                  $types = preg_split('/\:/',$type);
                  $inherit = in_array('Inherit',$types);
                  if ($inherit) {
                     preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches);
                     $itype = $matches[2];
                  } 
               }
               if (($inherit) && isset($settings[$itype]) && isset($settings[$itype]->Current)) {
                  if (!isset($params) || ($params->DataType != 'LINKEDTOM'))
                     $query->where("$col = ?",$settings[$itype]->Current);
               }
            }
            if (isset($settings[$t]) && isset($settings[$t]->Orders)) {
               $query->reset( Zend_Db_Select::ORDER );
               foreach($settings[$t]->Orders as $oobj) {
                  if (isset($this->columns[$oobj->Column])) {
                     $lcol = strtolower($oobj->Column);
                     $dirn = $oobj->Order;
                     switch($dirn) {
                     case 'Up':     $query->order("$lcol ASC");   break;
                     case 'Down':   $query->order("$lcol DESC");  break;
                     default:       $query->order("$lcol $dirn"); break;
                     }
                     $ordered = true;
                  }
               }
            }            
         }
         if (!$ordered) {
            if (isset($this->orders) && count($this->orders)>0) {
               $query->reset( Zend_Db_Select::ORDER );
               foreach($this->orders as $col => $dir) {
                  $lcol = strtolower($col);
                  switch($dir) {
                  case 'Up':     $query->order("$lcol ASC");  break;
                  case 'Down':   $query->order("$lcol DESC"); break;  
                  default:       $query->order("$lcol $dir"); break;
                  }
               }
            }
         }
         if (isset($page)) $query->limitPage($page,$this->perpage);
//print $query->__toString();exit;
         $result = $this->dbhandle->fetchAll($query);
         foreach($result as $rownum => $row) {
            $match = true;
            foreach ($row as $col => $val) {
               $type = "".$this->columns[$col];
               if (preg_match('/^\{/',$type)) {
                  $params = json_decode($type);
                  $type = $params->DataType;
               } else {
                  $bits = preg_split('/\:/',$type);
                  $type = $bits[0];
               }
               switch ($type) {
               case 'LINKEDTO': {
                  if (isset($params)) {
                     if (!is_object($val)) {
                        $type = $params->DataType;
                        $table = $params->TargetType;
                        $column = $params->TargetField;
                        $link = null;
                        $link = new $table();
                        $link->loaddata($val,$column,false);
                        $obj = new stdClass();
                        $obj->Name = $link->getname();
                        $obj->Id = $link->getid();
                        $icon = $link->geticon();
                        if (isset($icon)) $obj->Icon = $icon;
                        $result[$rownum][$col] = $obj;
                        if ($settings->SearchBy && $col == $settings->SearchBy) {
                           $sfor = $settings->SearchFor;   
                           $match = preg_match("/$sfor/",$obj->Name); 
                        }
                     }
                  }   
               }break;
               case 'INTERFACE': {
                  if ($val > 0) {
                     $cache = new G_Cache();
                     $cache->loadone($val);
                     $result[$rownum][$col] = $cache->getdata();                     
                  }
               }break;
               default: {
                  $result[$rownum][$col] = $this->postprocessterm($col,$type,$val);
               }break;
               } 
            }
            if (isset($ltmd) && is_array($ltmd) && count($ltmd)>0) {
               foreach($ltmd as $col) {
                  $mtype = $this->columns[$col];
                  if (preg_match('/^\{/',$mtype)) {
                     $mparams = json_decode($mtype);
                     $type = $mparams->DataType;
                     $ttab = $mparams->TargetType;
                     $tcol = $mparams->TargetField;
                     $p = ($mparams->Property)?$mparams->Property:$col;
                  } else if(preg_match('/([^\:]+)\:([^\.]+)\.([^\:]+).*/',$this->columns[$col],$matches)) {
                     $type = $matches[1];
                     $ttab = $matches[2];
                     $tcol = $matches[3];
                     $p = $col;
                  }
                  if (isset($ttab) && isset($tcol)) {
                     if (!is_object($val)) {
                        $pk = $this->getpk();
                        $id = $row[$pk];
                        $ltm = new DBT_Lookup($p,$this->tablename,$pk,$ttab,$tcol);
                        $obj = new stdClass();
                        $obj->IType = $ttab;
                        $obj->Current = 'Multiple';
                        $obj->Key = $tcol; 
                        $obj->ListData = $ltm->selecttargets($pk,$id);
                        $result[$rownum][$col] = $obj;
                        if ($sby && $col == $sby) {
                           $match = false;
                           foreach($obj->ListData as $item => $liobj) {
                              $match = $match || preg_match("/$sfor/",$liobj->Name);
                           } 
                        }
                     }
                  }      
               }
            }
            if (!$match) unset($result[$rownum]);
            if (!count($result)) $result = $query->__toString();            
         }
      }
      $result = array_values($result);
      return $result;   
   }
   function dbupcode($col,$val) {
      $pk = strtolower($this->getpk());
      $id = $this->getid();
      if (isset($id)) {
         $where = $this->dbhandle->quoteInto("$pk = ?",$id);
         $enc = $this->dbencrypt($val);
         $data = array(strtolower($col) => $enc);
         $status = $this->dbhandle->update(strtolower($this->tablename),$data,$where);
      }         
   }   
   function getpolygonmeasurements($poly) {
      $dbpg = SnC_getPGNativeConnection();
      $meas = new stdClass(); 
      $aexpr = new Zend_Db_Expr("st_area(st_transform(st_geomfromtext('$poly',4326),27700))");
      $pexpr = new Zend_Db_Expr("st_perimeter(st_transform(st_geomfromtext('$poly',4326),27700))");
      $sel = "SELECT $aexpr AS area, $pexpr AS peri";
      $res = pg_query($dbpg,$sel);
      $res = pg_fetch_array($res);
      $meas->Area = $res['area'];
      $meas->Perimeter = $res['peri'];
      return $meas;
   }
   function getinterval($iv) {
      $n = preg_replace('/[^\d]+/','',$iv);
      $p = preg_replace('/[^A-Z]+/','',$iv);
      switch($p) {
      case 'H': $p = 'HOUR';break;
      case 'D': $p = 'DAY';break;
      case 'W': $p = 'WEEK';break;
      case 'M': $p = 'MONTH';break;
      case 'Y': $p = 'YEAR';break;
      }
      return "INTERVAL '$n $p'";
   }
   
   function gs_createdatastore() {
      global $dbConfig;
      $url = $this->gs_getdatastoreurl($this->gs_datastore);
      $res = $this->gs_json_get($url);
      $created = (is_object($res) && isset($res->dataStore));
      if (!$created) {
         $pst = new stdClass();
         $pst->dataStore = new stdClass();
         $pst->dataStore->name   = $this->gs_datastore;
         $pst->dataStore->description = 'PostGIS database';
         $pst->dataStore->type = 'PostGIS';
         $pst->dataStore->enabled = true;
			$pst->dataStore->connectionParameters = new stdClass();
         $pst->dataStore->connectionParameters->entry = array();
         	$pst->dataStore->connectionParameters->entry[] = array('@key' => "port",'$' => "".$dbConfig['port']);
            $pst->dataStore->connectionParameters->entry[] = array('@key' => "Connection timeout", '$' => "20");
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "passwd", '$' => $dbConfig['password']);
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "dbtype", '$' => 'postgis');
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "validate connections", '$' => "true");
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "host", '$' => $dbConfig['host']);
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "encode functions", '$' => "false");
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "max connections", '$' => "10");
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "database", '$' => $dbConfig['dbname']);
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "schema", '$' => 'public');
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "Loose bbox", '$' => "true");
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "Expose primary keys", '$' => "false");
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "fetch size", '$' => "1000");
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "Max open prepared statements", '$' => "50");
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "preparedStatements", '$' => "false");
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "Estimated extends", '$' => "true");
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "user", '$' => $dbConfig['username']);
			   $pst->dataStore->connectionParameters->entry[] = array('@key' => "min connections", '$' => "1");
         $res = $this->gs_json_post($this->gs_getdatastoreurl(),$pst);
         $res = $this->gs_json_get($url);
         $created = (is_object($res) && isset($res->dataStore)); 
      }
      return $created;
   }
  
}

class DBT_FilterView extends DBT {
   protected $sourcetable;
   protected $filters = array();
   // eg $filters = array("Col1" => "Val1", "Col2" => "Val2");
   protected $orders = array();
   // eg $orders = array("Col1" => "ASC", "Col2" => "DESC");
   protected $indexon;
   protected $selectto;
   protected $domain = 'genera';
   function DBT_FilterView() {
      parent::DBT();
      $st = $this->sourcetable;
      if (isset($st)) {  
         $dbt = new $st();;
         if (!$dbt->exists()) $dbt->create();
         $this->columns = $dbt->getcolumns();
      }
   }
   function getuserpermissions() {
      $perms = array('View','Add','Edit','Delete');
      $granted = array();
      return $granted;      
   }
   function getpk() {
      return $this->indexon;
   }
   function create() {
      global $pending;
      if (!isset($pending[$this->tablename])) {
         $pending[$this->tablename] = true;
         $dbh = $this->dbhandle;
         $query = $dbh->select();
         $query->from($this->sourcetable);
         foreach($this->filters as $col => $val) {
            $query->where("$col = ?", $val);
         }               
         foreach($this->orders as $col => $dir) {
            $query->order("$col $dir");
         }               
         $select = $query->__toString();
         $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select"; 
         $dbh->exec($statement);
         if ($this->gs_publishme) $this->gs_publish();
         unset($pending[$this->tablename]);
      }                  
   }
   function exists() {
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from('pg_tables',array('tablename'=>'tablename'));
      $query->where('tablename = ?',strtolower($this->tablename));
      $resp = $db->fetchAll($query);
      return (count($resp)>0);
   }       
}
class DBT_ReportView extends DBT {
   protected $sourcetables;
   // eg $sourcetables = array("Table1" => "Alias1", "Table2" => "Alias2");
   protected $joins = array();
   // eg $joins = array ("TargetTable1" => "SourceTable1");
   protected $sourcecolumns;
   // eg $sourcetables = array(  "Table1" => array("AsAlias1" => "Col1","AsAlias2" => "Col2"), 
   //                            "Table2" => array("AsAlias1" => "Col1","AsAlias2" => "Col2"));
   protected $filters = array();
   // eg $filters = array("Col1" => "Val1", "Col2" => "Val2");
   protected $colmatch = array();
   protected $orders = array();
   // eg $orders = array("Col1" => "ASC", "Col2" => "DESC");
   protected $group = array();
   // eg $group = array("Col1");
   protected $indexon;
   protected $selectto;
   protected $nolist = array();
   protected $domain = 'genera';
   function DBT_ReportView() {
      parent::DBT();
      foreach($this->sourcetables as $table => $alias) {
         $link = new $table();
         if (!$link->exists()) $link->create();
         $scols = $link->getcolumns();
         if ($this->sourcecolumns[$table]) {
            foreach($this->sourcecolumns[$table] as $alias => $column) {
               $alias = (is_int($alias))?$column:$alias;
               if ($scols[$column] == 'Unique Key') {
                  $obj = new stdClass();
                  $obj->DataType = 'LINKEDTO';
                  $obj->TargetType = $table;
                  $obj->TargetField = $column;
                  if (in_array($alias,$this->nolist)) $obj->NoList = 1;
                  $this->columns[$alias] = json_encode($obj);
               } else if (preg_match('/^\{/',$scols[$column])) {
                  $obj = json_decode($scols[$column]);
                  if (in_array($alias,$this->nolist)) $obj->NoList = 1;
                  $this->columns[$alias] = json_encode($obj);
               } else {
                  $obj = new stdClass();
                  $obj->DataType = preg_replace('/\:.+/','',$scols[$column]);
                  if (in_array($alias,$this->nolist)) $obj->NoList = 1;
                  $this->columns[$alias] = json_encode($obj);
               }                            
            }
         }
      }
//print "<pre>"; print_r($this->columns); print "</pre>"; exit;   
   }
   function getversions($val) {return null;}
   function getuserpermissions() {
      $perms = array('View','Add','Edit','Delete');
      $granted = array();
      foreach($perms as $perm) {
         if ($this->isallowed($this->user,$perm)) $granted[] = $perm;      
      }
      return $granted;      
   }
   function getpk() {
      return $this->indexon;
   }
   function create() {
      global $pending;
      if (!isset($pending[$this->tablename])) {
         $pending[$this->tablename] = true;
         $dbh = $this->dbhandle;
         $query = $dbh->select();
         $count = 0;
         foreach($this->sourcetables as $table => $alias) {
            $ltable = strtolower($table);
            $lalias = strtolower($alias);
            $cols = array();
            if (isset($this->sourcecolumns[$table])) {
               foreach ($this->sourcecolumns[$table] as $k => $col) {
                  $lcol = strtolower($col);
                  $lk = strtolower($k);
                  if (is_numeric($k)) {
                     $cols[$lcol] = $lcol;
                  } else if (preg_match('/^\{/',$col)) {
                     $expr = json_decode($col);
                     $expr->Expression = strtolower($expr->Expression);
                     $cols[$lk] = json_encode($expr); 
                  } else {
                     $cols[$lk] = $lcol;
                  }
               }
            }
            if ($count == 0) $query->from(array($lalias => $ltable),$cols);         
            else {
               $link = new $table();
               if (!$link->exists()) $link->create();
               $totable = $this->joins[$table];
               $toalias = $this->sourcetables[$totable];
               $ltotable = strtolower($totable);  
               list($incol,$tocol) = $link->getlinkedcolumn($totable);
               if (!isset($incol)) {
                  $link = new $totable();
                  if (!$link->exists()) $link->create();
                  list($tocol,$incol) = $link->getlinkedcolumn($table);
               }
               $join = strtolower("${alias}.${incol} = ${toalias}.${tocol}");
               foreach($cols as $col => $val) {
                  if (preg_match('/^\{/',$val)) {
                     $val = json_decode($val);
                     $val = new Zend_Db_Expr($val->Expression);
                     $cols[$col] = $val;
                  }
               }
               $query->joinLeft(array($lalias => $ltable),$join,$cols);
            }
            $count++;
         }
         
         foreach($this->filters as $clause => $val) {
            if (preg_match('/^\{/',$val)) {
               $val = json_decode($val);
               $val = new Zend_Db_Expr($val->Expression);
            }
            $query->where($clause, $val);
         }               
         foreach($this->colmatch as $col1 => $col2) {
            $query->where(strtolower("$col1 = $col2"));
         }               
         foreach($this->orders as $col => $dir) {
            $query->order(strtolower("$col $dir"));
         }
         foreach($this->group as $col) {
            $query->group(strtolower($col));
         }               
         $select = $query->__toString();
//print "$select";exit;         
         $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
         $this->dbhandle->exec($statement);
         if ($this->gs_publishme) $this->gs_publish();
         unset($pending[$this->tablename]);
      }
   }
   function exists() {
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from('pg_views',array('viewname'=>'viewname'));
      $query->where('viewname = ?',strtolower($this->tablename));
      $resp = $db->fetchAll($query);
      return (count($resp)>0);
   }   
}
class DBT_Analysis extends DBT {
   protected $domain = 'genera';
   function DBT_Analysis() {
      parent::DBT();
   }
   function exists() {
      $db = $this->dbhandle;
      $query = $db->select();
      $query->from('pg_views',array('viewname'=>'viewname'));
      $query->where('viewname = ?',strtolower($this->tablename));
      $resp = $db->fetchAll($query);
      return (count($resp)>0);
   }
}

$pending = array();
include_once('snc/S2_DBT_Lookup.php');

/*
class DBTest1 extends DBT {
   protected $tablename = 'DBTest1';
   protected $displayname = 'Database Test 1';
   // data types can be one of $this->datatypes or 
   // LINK:<tablename>.<columnname> and will pick up their 
   // datatype from that table. 
   // note that table will need to be included using 
   // include_once to ensure the definition can be read
   protected $columns = array(
      'DBTest1_ID'      => 'Unique Key',
      'name_column_1'   => 'Short Text',
      'value_column_1'  => 'Number'
   );
   protected $childtables = array('DBTest2');
}
class DBTest2 extends DBT {
   protected $tablename = 'DBTest2';
   protected $displayname = 'Database Test 2';
   protected $columns = array(
      'DBTest2_ID'      => 'Unique Key',
      'name_column_2'   => 'Short Text',
      'value_column_2'  => 'Number',
      'dbt1'            => 'LINKEDTO:DBTest1.DBTest1_ID:Optional'                  
   );
}
class DBTest3 extends DBT {
   protected $tablename = 'DBTest3';
   protected $displayname = 'Database Test 3';
   protected $columns = array(
      'DBTest3_ID'      => 'Unique Key',
      'name_column_3'   => 'Short Text',
      'value_column_3'  => 'Number',
      'dbt1'            => 'LINKEDTO:DBTest1.DBTest1_ID:Optional'                  
   );
}

$DBTest1 = new DBTest1();
$DBTest1->create();

$cols = array(
   'name_column_1' => 'Name Test 1',
   'value_column_1' => 'Value 1'   
);
$DBTest1->setdata($cols);
$status = $DBTest1->insert();
print "<h3>DBTest1 id = ".$DBTest1->getid()."</h3>";

$dbt1s = $DBTest1->selectAll();
//print_r($dbt1s);


$cols = array(
   'name_column_1' => 'Name Test 2',
   'value_column_1' => 'Value 1'   
);
$DBTest1->setdata($cols);
$status = $DBTest1->update('name_column_1', 'Name Test 1');
print "<h3>Update Status : $status</h3>";

$dbt1s = $DBTest1->selectAll();
//print_r($dbt1s);
$DBTest2 = new DBTest2();
$DBTest2->create();
$DBTest2->drop();
print $DBTest2->drawform();


print $DBTest1->drawtable();

print_r($DBTest1->exists());
$DBTest3 = new DBTest3();
print_r($DBTest3->exists());
*/
?>