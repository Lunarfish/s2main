<?php
/*
   Add LINKEDTOM compartments to Management and Ownership
*/
include_once('settings.conf');
include_once('snc/S2_DBT.php');

class S2_Authority extends DBT {
   protected $tablename = 'S2_Authority';
   protected $displayname = 'Designating Authority or Partnership';
   protected $show = array('S2_Site','S2_Meeting','S2_Programme');
   protected $view = '[
      {
         "Name":"Sites",
         "Data": [
            {
               "Table":"S2_Site"
            },
            {
               "Name":"Site Overview Report",
               "Table":"S2_SiteMap",
               "Show":"All"
            },
            {
               "Name":"Priority Follow Up",
               "Table":"S2_High_Priority_Sites",
               "Show":"All"
            },
            {
               "Name":"Site Last Visited",
               "Table":"S2_Site_Last_Visited",
               "Show":"All"
            }
         ]
      },
      {
         "Name":"Meetings",
         "Data":[
            {
               "Name":"Next Meeting",
               "Table":"S2_Meeting",
               "Show":"Next"
            },
            {
               "Name":"Previous Meeting",
               "Table":"S2_Meeting",
               "Show":"Previous"
            },
            {
               "Name":"Meeting History",
               "Table":"S2_Meeting",
               "Show":"All"
            }
         ]
      },
      {
         "Name":"Settings",
         "Data": [
            {
               "Name":"Designations",
               "Table":"S2_Designation",
               "Show":"Every"
            },
            {
               "Name":"Statuses",
               "Table":"S2_Designation_Status",
               "Show":"Every"
            }
         ]
      },
      {
         "Name":"Supporting Documents",
         "Data": [
            {
               "Table":"S2_Supporting_Document"
            }
         ]
      }
   ]'; 
   protected $columns = array(
      'S2_Authority_ID' => 'Unique Key',
      'Name'            => 'Short Text',
      'Established'     => 'Date'
   );
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $defaultpermissions = array(
      'USR_Permissions' => '
"User_Id","Domain","Label","Value"
1,"sites","Registered User",1
1,"sites","Partnership Member",1
1,"sites","Partnership Administrator",1
1,"sites","System Administrator",1',
      'USR_Default_Permissions' => '
"Domain","Label","Description","Value","Anon" 
"sites","System Administrator","Add and edit partnerships",0,0
"sites","Partnership Administrator","Add and edit partnership data",0,0
"sites","Partnership Member","View partnership data",0,0
"sites","Registered User","View unprotected data",1,0'   
   );
}

/*
   Need a table for the overall stat submission for NI197 160-00 or whatever 
   it gets called next. 
*/


class S2_Site extends DBT {
   protected $tablename = 'S2_Site';
   protected $displayname = 'Site';
   protected $perpage = 10;
   protected $show = array('S2_Site_Designation','S2_Site_Boundary','S2_Site_Visit','S2_Site_Description','S2_Site_Broad_Habitat','S2_Site_Contact');
   protected $view = '[
      {
         "Name":"Overview",
         "Data":[
            {
               "Name":"Current Description",
               "Table":"S2_Site_Description",
               "Show":"Current"
            },
            {
               "Name":"Current Boundary",
               "Table":"S2_Site_Boundary",
               "Show":"Current"
            }            
         ]        
      },
      {
         "Name":"Descriptions",
         "Data":[
            {
               "Table":"S2_Site_Description"
            }
         ]
      },
      {
         "Name":"Designations",
         "Data":[
            {
               "Table":"S2_Site_Designation"
            }
         ]         
      },
      {
         "Name":"Compartments",
         "Data":[
            {
               "Table":"S2_Site_Compartment"
            }
         ]         
      },
      {
         "Name":"Boundaries",
         "Data":[
            {
               "Name":"Boundary History",
               "Table":"S2_Site_Boundary",
               "Show":"All"               
            }
         ]
      },
      {
         "Name":"Access",
         "Data":[
            {
               "Name":"Access",
               "Table":"S2_Site_Access"
            },
            {
               "Name":"Considerations",
               "Table":"S2_Site_Consideration"
            },
            {
               "Name":"Contacts",
               "Table":"S2_Site_Contact"
            },
            {
               "Name":"Notification",
               "Table":"S2_Site_Notification_Rule"
            }
         ]
      },
      {
         "Name":"Surveys",
         "Data": [
            {
               "Name":"Surveys",
               "Table":"S2_Site_Survey"
            },
            {
               "Name":"Notification Rules",
               "Table":"S2_Site_Notification_Rule"
            },
            {
               "Name":"Considerations",
               "Table":"S2_Site_Consideration"
            }
         ]
      },
      {
         "Name":"Monitoring",
         "Data":[
            {
               "Table":"S2_Site_Visit"
            },
            {
               "Table":"S2_Site_Follow_Up"
            }
         ]
      },
      {
         "Name":"Management Summary",
         "Data":[
            {
               "Table":"S2_Site_Appropriate_Management_Decision"
            },
            {
               "Table":"S2_Site_Management_Aim"
            },
            {
               "Table":"S2_Site_Management_Action"
            }
         ]
      },
      {
         "Name":"Management Documents",
         "Data":[
            {
               "Table":"S2_Site_Management_Scheme"
            },
            {
               "Table":"S2_Site_Management_Plan"
            },
            {
               "Table":"S2_Site_Management_Advice"
            },
            {
               "Table":"S2_Site_Management_BAP"
            }
         ]
      },
      {
         "Name":"Habitats",
         "Data":[
            {
               "Table":"S2_Site_Broad_Habitat"
            },
            {
               "Table":"S2_Site_UKBAP_Habitat"
            },
            {
               "Table":"S2_Supporting_Document"
            }
         ]
      },
      {
         "Name":"Documents",
         "Data": [
            {
               "Name":"Site Photos",
               "Table":"S2_Site_Photo"
            },
            {
               "Name":"Associated Documents",
               "Table":"S2_Site_Doc"
            }
         ]
      }
   ]'; 
   protected $columns = array(
      'S2_Site_ID'      => 'Unique Key',
      'S2_Authority_ID' => '{"DataType":"LINKEDTO","TargetType":"S2_Authority","TargetField":"S2_Authority_ID","Current":1,"Inherit":1}',
      'Name'            => '{"DataType":"Short Text","Mandatory":1}',
      'Zone'            => '{"DataType":"Short Text","Derives":[{"Column":"Site_Code","Method":"s2_nextsitecode","Params":[{"Type":"Static","Value":"S2_Site"},{"Type":"Static","Value":"Site_Code"},{"Type":"Value"}]}]}',
      'Site_Code'       => '{"DataType":"Short Text","Mandatory":1}',
      'Meetings'        => '{"DataType":"LINKEDTOM","TargetType":"S2_Meeting","TargetField":"S2_Meeting_ID","Property":"Meeting_Sites"}'
   );
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
   function getname() {
      return  $this->data['Site_Code'] . ":" . $this->data['Name'];
   }
   function getshortname() {
      return  $this->data['Site_Code'];
   }
}

class S2_Meeting extends DBT {
   protected $tablename = 'S2_Meeting';
   protected $displayname = 'Meeting';
   protected $columns = array(
      'S2_Meeting_ID'      => 'Unique Key',
      'S2_Authority_ID'    => '{"DataType":"LINKEDTO","TargetType":"S2_Authority","TargetField":"S2_Authority_ID","Current":1,"Inherit":1}',
      'Name'               => '{"DataType":"Short Text","Mandatory":1}',
      'Number'             => '{"DataType":"Count","CountOfType":"S2_Authority","CountOfField":"S2_Authority_ID"}',
      'On_Date'            => '{"DataType":"Date","Mandatory":1}',
      'Agenda'             => '{"DataType":"File Path","NoList":1}',    
      'Minutes'            => '{"DataType":"File Path","NoList":1}',
      'Documents'          => '{"DataType":"LINKEDTOM","TargetType":"S2_Supporting_Document","TargetField":"S2_Supporting_Document_ID","Property":"Meeting_Docs"}',
      'Sites'              => '{"DataType":"LINKEDTOM","TargetType":"S2_Site","TargetField":"S2_Site_ID","Property":"Meeting_Sites"}'    
   );
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
   function getname() {
      $name = $this->data['Name'];
      if (isset($this->data['Number'])) $name .= " " . $this->data['Number'];
      return $name;
   }
}

class S2_Supporting_Document extends DBT {
   protected $tablename = 'S2_Supporting_Document';
   protected $displayname = 'Supporting Document';
   protected $columns = array(
      'S2_Supporting_Document_ID'   => 'Unique Key',
      'S2_Authority_ID'             => '{"DataType":"LINKEDTO","TargetType":"S2_Authority","TargetField":"S2_Authority_ID","Current":1}',
      'Name'                        => '{"DataType":"Short Text","Mandatory":1}',
      'Received'                    => '{"DataType":"Date","Mandatory":1}',
      'File'                        => '{"DataType":"File Path","Mandatory":1,"NoList":1}',    
      'Meetings'                    => '{"DataType":"LINKEDTOM","TargetType":"S2_Meeting","TargetField":"S2_Meeting_ID","Property":"Meeting_Docs"}'
   );
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
}

class S2_Document extends DBT {
   protected $tablename = 'S2_Document';
   protected $displayname = 'Document';
   protected $show = array();
   protected $columns = array(
      'S2_Document_ID'      => 'Unique Key',
      'Name'                => '{"DataType":"Short Text","Mandatory":1}',
      'File'                => '{"DataType":"File Path","Mandatory":1}' 
   );
   protected $domain = 'sites';
   protected $permissions = array(
      'Def'    => 'Partnership Member'
   );
}

class S2_Designation extends DBT {
   protected $tablename = 'S2_Designation';
   protected $displayname = 'Designation Type';
   protected $alts = array('Authorities'=>'S2_Authority');
   protected $show = array('S2_Site_Designation');
   protected $view = '[
      {
         "Name":"Sites",
         "Table":"S2_Site_Designation"
      },
      {
         "Name":"Overview",
         "Table":"S2_Designation_Overview"
      }
   ]';
   protected $columns = array(
      'S2_Designation_ID'  => 'Unique Key',
      'S2_Authority_ID'    => '{"DataType":"LINKEDTO","TargetType":"S2_Authority","TargetField":"S2_Authority_ID","Current":1,"Inherit":1}',
      'Name'               => 'Short Text',
      'Abbreviation'       => 'Short Text',
      'Description'        => 'Long Text'
   );
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
   function getshortname() {
      return $this->data['Abbreviation'];
   }
}


class S2_Site_Designation extends DBT {
   protected $tablename = 'S2_Site_Designation';
   protected $displayname = 'Site Designation';
   protected $show = array('S2_Site_Designation_Status');
   protected $view = '[
      {
         "Name":"Status",
         "Data": [
            {
               "Table":"S2_Site_Designation_Status"
            }
         ]
      },
      {
         "Name":"Qualification",
         "Data": [
            {
               "Table":"S2_Site_Designation_Criteria"
            }
         ]
      },
      {
         "Name":"Citations",
         "Data":[
            {
               "Table":"S2_Site_Citation"
            }
         ]
      }
   ]';
   protected $columns = array(
      'S2_Site_Designation_ID'   => 'Unique Key',
      'S2_Site_ID'               => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1,"Inherit":1}',
      'S2_Designation_ID'        => '{"DataType":"LINKEDTO","TargetType":"S2_Designation","TargetField":"S2_Designation_ID","Mandatory":1}',
      'S2_Site_Boundary_ID'      => '{"DataType":"LINKEDTO","TargetType":"S2_Site_Boundary","TargetField":"S2_Site_Boundary_ID","Inherit":1}',
      'From_Date'                => 'Date',
      'Until_Date'               => 'Date'               
   ); 
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
   function getname() {
      $names = array();
      $columns = array('S2_Site_ID','S2_Designation_ID');
      foreach($columns as $col) {
         $type = $this->columns[$col];
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $table = $params->TargetType;
            $tcol = $params->TargetField;
         } else if (preg_match('/([^\:]+)\:([^\.]+)\.(.+)/',$type,$matches)) {
            $type = $matches[1];
            $table = $matches[2];
            $tcol = $matches[3];
         }
         if (isset($table) && isset($tcol)) {
            $link = null;
            $link = new $table();
            $id = (is_object($this->data[$col]))
                  ?$this->data[$col]->Id
                  :$this->data[$col];
            if ($id > 0) {
               $link->loaddata($id);
               $names[] = $link->getshortname();
            }
         }  
      }
      return join(':',$names);   
   }   
}

class S2_Designation_Status extends DBT {
   protected $tablename = 'S2_Designation_Status';
   protected $displayname = 'Designation Status';
   protected $columns = array(
      'S2_Designation_Status_ID'    => 'Unique Key',
      'Name'                        => 'Short Text',
      'Abbreviation'                => 'Short Text',
      'Description'                 => 'Long Text',
   );
   protected $domain = 'sites';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
   function getshortname() {
      return $this->data['Abbreviation'];
   }
}
class S2_Site_Designation_Status extends DBT {
   protected $tablename = 'S2_Site_Designation_Status';
   protected $displayname = 'Site Designation Status';
   protected $columns = array(
      'S2_Site_Designation_Status_ID'  => 'Unique Key',
      'S2_Site_Designation_ID'         => '{"DataType":"LINKEDTO","TargetType":"S2_Site_Designation","TargetField":"S2_Site_Designation_ID","Mandatory":1,"Current":1}',
      'S2_Designation_Status_ID'       => '{"DataType":"LINKEDTO","TargetType":"S2_Designation_Status","TargetField":"S2_Designation_Status_ID","Mandatory":1}',
      'From_Date'                      => 'Date',
      'Until_Date'                     => 'Date',               
      'Version'                        => '{"DataType":"Version","VersionOfType":"S2_Site_Designation","VersionOfField":"S2_Site_Designation_ID"}' 
   );
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
   function getname() {
      $names = array();
      $columns = array('S2_Site_Designation_ID','S2_Designation_Status_ID');
      foreach($columns as $col) {
         $type = $this->columns[$col];
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $table = $params->TargetType;
            $tcol = $params->TargetField;
         } else if (preg_match('/([^\:]+)\:([^\.]+)\.(.+)/',$type,$matches)) {
            $type = $matches[1];
            $table = $matches[2];
            $tcol = $matches[3];
         }
         if (isset($table) && isset($tcol)) {
            $link = null;
            $link = new $table();
            $id = (is_object($this->data[$col]))
                  ?$this->data[$col]->Id
                  :$this->data[$col];
            if ($id > 0) {
               $link->loaddata($id);
               $names[] = $link->getshortname();
            }                            
         }  
      }
      return join(':',$names);   
   }   
}

class S2_Site_Boundary extends DBT {
   protected $tablename = 'S2_Site_Boundary';
   protected $displayname = 'Boundary';
   protected $show = array();
   protected $columns = array(
      'S2_Site_Boundary_ID'      => 'Unique Key',
      'S2_Site_ID'               => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1,"Inherit":1}',
      'Review_Date'              => 'Date',
      'Boundary'                 => 'Map Data',
      'Notes'                    => 'Long Text',
      'Version'                  => '{"DataType":"Version","VersionOfType":"S2_Site","VersionOfField":"S2_Site_ID"}' 
   );
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );
   function getname() {
      return "V" . $this->data['Version'] . " (" . preg_replace('/(\d{4})\-(\d{2})\-(\d{2})/',"$3/$2/$1",$this->data['Review_Date']) . ")";
   }   
}

class S2_Site_Compartment extends DBT {
   protected $tablename = 'S2_Site_Compartment';
   protected $displayname = 'Compartment';
   protected $perpage = 10;
   protected $show = array('S2_Site_Compartment_Boundary');
   protected $columns = array(
      'S2_Site_Compartment_ID'   => 'Unique Key',
      'S2_Site_ID'               => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Current":1,"Inherit":1}',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Compartment_Code'         => '{"DataType":"Short Text","Mandatory":1}',
   );
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
   function getname() {
      return  $this->data['Compartment_Code'] . " : " . $this->data['Name'];
   }
   function getshortname() {
      return  $this->data['Compartment_Code'];
   }
}

class S2_Site_Compartment_Boundary extends DBT {
   protected $tablename = 'S2_Site_Compartment_Boundary';
   protected $displayname = 'Compartment Boundary';
   protected $show = array();
   protected $columns = array(
      'S2_Site_Compartment_Boundary_ID'   => 'Unique Key',
      'S2_Site_Compartment_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_Site_Compartment","TargetField":"S2_Site_Compartment_ID","Mandatory":1,"Current":1,"Inherit":1}',
      'Review_Date'                       => 'Date',
      'Boundary'                          => 'Map Data',
      'Notes'                             => 'Long Text',
      'Version'                           => '{"DataType":"Version","VersionOfType":"S2_Site_Compartment","VersionOfField":"S2_Site_Compartment_ID"}' 
   );
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );
   function getname() {
      return "V" . $this->data['Version'] . " (" . $this->data['Review_Date'] . ")";
   }   
}

class S2_Site_Visit extends DBT {
   protected $tablename = 'S2_Site_Visit';
   protected $displayname = 'Site Visit Report';
   protected $show = array('S2_Visit_Detail');
   protected $columns = array(
      'S2_Site_Visit_ID'      => 'Unique Key',
      'S2_Site_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1,"Inherit":1}',
      'S2_Visit_Type_ID'      => '{"DataType":"LINKEDTO","TargetType":"S2_Visit_Type","TargetField":"S2_Visit_Type_ID","Mandatory":1}',
      'Notes'                 => 'Long Text',
      'Survey_Date'           => 'Date',
      'Observed_Activity'     => '{"DataType":"LINKEDTOM","TargetType":"S2_Observed_Activity","TargetField":"S2_Observed_Activity_ID","Property":"Obs_Act"}',
      'Follow_Up'             => '{"DataType":"LINKEDTOM","TargetType":"S2_Follow_Up_Action","TargetField":"S2_Follow_Up_Action_ID","Property":"Fol_Act"}'  
   );
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      return preg_replace('/(\d{4})\-(\d{2})\-(\d{2})/',"$3/$2/$1",$this->data['Survey_Date']);
   }   
}
class S2_Visit_Type extends DBT {
   protected $tablename = 'S2_Visit_Type';
   protected $displayname = 'Visit Type';
   protected $show = array();
   protected $columns = array(
      'S2_Visit_Type_ID'      => 'Unique Key',
      'Name'                  => '{"DataType":"Short Text","Mandatory":1}',
      'Description'           => 'Long Text' 
   );
   protected $domain = 'sites';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'S2_Visit_Type' => '
"Name","Description"
"Full Survey","Full ecological survey"
"Monitoring Survey","Monitoring change of habitat, condition and use."
"Informal","Notes from passing observation."'
   );   
}

class S2_Visit_Detail extends DBT {
   protected $tablename = 'S2_Visit_Detail';
   protected $displayname = 'Visit Detail';
   protected $show = array();
   protected $columns = array(
      'S2_Visit_Detail_ID'      => 'Unique Key',
      'S2_Site_Visit_ID'         => '{"DataType":"LINKEDTO","TargetType":"S2_Site_Visit","TargetField":"S2_Site_Visit_ID","Mandatory":1,"Current":1}',
      'S2_Visit_Detail_Type_ID'  => '{"DataType":"LINKEDTO","TargetType":"S2_Visit_Detail_Type","TargetField":"S2_Visit_Detail_Type_ID","Mandatory":1}',
      'Notes'                    => 'Long Text',
      'Survey_Date'              => 'Date' 
   );
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );
   function getname() {
      return $this->data['Survey_Date'];
   }   
}
class S2_Visit_Detail_Type extends DBT {
   protected $tablename = 'S2_Visit_Detail_Type';
   protected $displayname = 'Visit Detail Type';
   protected $show = array();
   protected $columns = array(
      'S2_Visit_Detail_Type_ID'  => 'Unique Key',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Description'              => 'Long Text' 
   );
   protected $domain = 'sites';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'S2_Visit_Detail_Type' => '
"Name","Description"
"Adjacent Land Use","Obvious adjacent land use changes"
"Boundary Change","Changes to site boundary since last visit"
"Boundary Error","Evidence that a boundary change is necessary due to previous mapping error"
"Land Use","Land use changes within the site"
"Habitat","Significant habitat changes since last visit"
"Recommendations","Recommendations for further work"
"General Changes","Surveyors comments regarding site changes"
"Additional Species","Additional key/indicator species not previously noted"'
   );   
}

class S2_Observed_Activity extends DBT {
   protected $tablename = 'S2_Observed_Activity';
   protected $displayname = 'Observed Activity';
   protected $show = array();
   protected $columns = array(
      'S2_Observed_Activity_ID'  => 'Unique Key',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Description'              => 'Long Text',
      'Site_Visits'              => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Visit","TargetField":"S2_Site_Visit_ID","Property":"Obs_Act"}'   
   );
   protected $domain = 'sites';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'S2_Observed_Activity' => '
"Name"
"Recent Coppicing"	
"Fire"	
"Unmanaged (last 2 years)"	
"Overgrazing"	
"Grazing (animal unknown)"	
"Cattle grazing"	
"Horse grazing"	
"Sheep grazing"	
"Rabbit grazing"	
"Mowing (hay or silage)"	
"Recent (re)planting"	
"Fishing"	
"Managed for game"	
"Undergrazing"	
"Management Unknown"	
"Poaching"	
"Fly tipping"	
"Damage by public"	
"Invasive aliens"	
"Agrochemicals"	
"Storm damage"	
"Boating"	
"Car parking damage"	
"Pollution"	
"Caravans"	
"Bike/Car trails"	
"Amenity use"	
"Horse riding"	
"Shooting"	
"Timber"	
"Threats"	
"Damage"'
   );   
}
class S2_Site_Follow_Up extends DBT {
   protected $tablename = 'S2_Site_Follow_Up';
   protected $displayname = 'Follow Up Schedule';
   protected $show = array();
   protected $columns = array(
      'S2_Site_Follow_Up_ID'  => 'Unique Key',
      'S2_Site_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1}',
      'S2_Priority_ID'        => '{"DataType":"LINKEDTO","TargetType":"S2_Priority","TargetField":"S2_Priority_ID","Mandatory":1}',
      'From_Year'             => '{"DataType":"Number","Derives":[{"Column":"To_Year","Method":"s2_getyear","Params":[{"Type":"Static","Value":5},{"Type":"Value"}]}]}',
      'To_Year'               => '{"DataType":"Number"}'
   );
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}
class S2_Priority extends DBT {
   protected $tablename = 'S2_Priority';
   protected $displayname = 'Priority';
   protected $show = array();
   protected $columns = array(
      'S2_Priority_ID'  => 'Unique Key',
      'Name'            => '{"DataType":"Short Text","Mandatory":1}',
      'Description'     => 'Long Text' 
   );
   protected $domain = 'sites';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'S2_Priority' => '
"Name"
"High"
"Medium - high"
"Medium"
"Medium - low"
"Low"'
   );   
}

class S2_Follow_Up_Action extends DBT {
   protected $tablename = 'S2_Follow_Up_Action';
   protected $displayname = 'Follow Up Action';
   protected $show = array();
   protected $columns = array(
      'S2_Follow_Up_Action_ID'   => 'Unique Key',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Description'              => 'Long Text',
      'Site_Visits'              => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Visit","TargetField":"S2_Site_Visit_ID","Property":"Fol_Act"}'   
   );
   protected $domain = 'sites';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'S2_Follow_Up_Action' => '
"Name"
"Add site to annual full survey list"
"Add site to monitoring survey list"
"Pass details of damage to enforcement officer"
"Programme survey for other biotic group"'
   );
}

class S2_Site_Description extends DBT {
   protected $tablename = 'S2_Site_Description';
   protected $displayname = 'Description';
   protected $show = array();
   protected $columns = array(
      'S2_Site_Description_ID'    => 'Unique Key',
      'S2_Site_ID'               => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1}',
      'Survey_Date'              => 'Date',
      'Site_Description'         => 'Long Text',
      'Version'                  => '{"DataType":"Version","VersionOfType":"S2_Site","VersionOfField":"S2_Site_ID"}' 
   );
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      return "V" . $this->data['Version'] . " (" . $this->data['Survey_Date'] . ")";
   }   
}

class S2_Broad_Habitat extends DBT {
   protected $tablename = 'S2_Broad_Habitat';
   protected $displayname = 'Broad Habitat';
   protected $show = array();
   protected $columns = array(
      'S2_Broad_Habitat_ID'      => 'Unique Key',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Code'                     => 'Short Text',
      'Description'              => 'Long Text' 
   );
   protected $domain = 'sites';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'S2_Broad_Habitat' => '
"Name","Code"
"Broadleaved, mixed and yew woodland",1
"Coniferous woodland",2
"Boundary and linear features",3
"Arable and horticultural",4
"Improved grassland",5
"Neutral grassland",6
"Calcareous grassland",7
"Acid grassland",8
"Bracken",9
"Dwarf shrub heath",10
"Fen, marsh and swamp",11
"Bogs",12
"Standing open water and canals",13
"Rivers and streams",14
"Montane habitats",15
"Inland rock",16
"Built up areas and gardens",17
"Supralittoral rock",18
"Supralittoral sediment",19
"Littoral rock",20
"Littoral sediment",21
"Inshore sublittoral rock",22
"Inshore sublittoral sediment",23
"Offshore shelf rock",24
"Offshore shelf sediment",25
"Continental shelf slope",26
"Oceanic seas",27'
   );   
}

class S2_UKBAP_Habitat extends DBT {
   protected $tablename = 'S2_UKBAP_Habitat';
   protected $displayname = 'UK BAP Habitat';
   protected $show = array();
   protected $columns = array(
      'S2_UKBAP_Habitat_ID'      => 'Unique Key',
      'Name'                     => '{"DataType":"Short Text","Mandatory":1}',
      'Code'                     => 'Short Text',
      'Description'              => 'Long Text' 
   );
   protected $domain = 'sites';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'S2_UKBAP_Habitat' => '
"Name","Code"
"Aquifer Fed Naturally Fluctuating Water Bodies","AFNFWB"
"Arable Field Margins","AFM"
"Blanket Bog","BB"
"Blue mussel beds","BMB"
"Calaminarian Grasslands","CG"
"Carbonate mounds","CM"
"Coastal and Floodplain Grazing Marsh","CFGM"
"Coastal saltmarsh","CS"
"Coastal Sand Dunes","CSD"
"Coastal Vegetated Shingle","CVS"
"Cold-water coral reefs","CWCR"
"Deep-sea sponge communities","DSSC"
"Estuarine rocky habitats","ERH"
"Eutrophic Standing Waters","ESW"
"File shell beds","FSB"
"Fragile sponge & anthozoan communities on subtidal rocky habitats","FSACSRH"
"Hedgerows","H"
"Horse mussel beds","HMB"
"Inland Rock Outcrop and Scree Habitats","IROSH"
"Intertidal chalk","IC"
"Intertidal mudflats","IM"
"Intertidal underboulder communities","IUC"
"Limestone Pavements","LP"
"Lowland Beech and Yew Woodland","LBYW"
"Lowland Calcareous Grassland","LCG"
"Lowland Dry Acid Grassland","LDAG"
"Lowland Fens","LF"
"Lowland Heathland","LH"
"Lowland Meadows","LM"
"Lowland Mixed Deciduous Woodland","LMDW"
"Lowland Raised Bog","LRB"
"Machair","M"
"Maerl beds","MB"
"Maritime Cliff and Slopes","MCS"
"Mesotrophic Lakes","ML"
"Mountain Heaths and Willow Scrub","MHWS"
"Mud habitats in deep water","MHDW"
"Native Pine Woodlands","NPW"
"Oligotrophic and Dystrophic Lakes","ODL"
"Open Mosaic Habitats on Previously Developed Land","OMHPDL"
"Peat and clay exposures","PCE"
"Ponds","P"
"Purple Moor Grass and Rush Pastures","PMG"
"Reedbeds","RB"
"Rivers","R"
"Sabellaria alveolata reefs","SAR"
"Sabellaria spinulosa reefs","SSR"
"Saline lagoons","SL"
"Seagrass beds","SB"
"Seamount communities","SC"
"Serpulid reefs","SR"
"Sheltered muddy gravels","SMG"
"Subtidal chalk","STC"
"Subtidal sands and gravels","STSG"
"Tide-swept channels","TSC"
"Traditional Orchards","TO"
"Upland Birchwoods","UB"
"Upland Calcareous Grassland","UCG"
"Upland Flushes, Fens and Swamps","UFFS"
"Upland Hay Meadows","UHM"
"Upland Heathland","UH"
"Upland Mixed Ashwoods","UMA"
"Upland Oakwood","UO"
"Wet Woodland","WW"
"Wood-Pasture & Parkland","WPP"'
   );   
}



class S2_Site_Broad_Habitat extends DBT {
   protected $tablename = 'S2_Site_Broad_Habitat';
   protected $displayname = 'Broad Habitat';
   protected $show = array();
   protected $columns = array(
      'S2_Site_Broad_Habitat_ID'    => 'Unique Key',
      'S2_Site_ID'                  => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1}',
      'S2_Broad_Habitat_ID'         => '{"DataType":"LINKEDTO","TargetType":"S2_Broad_Habitat","TargetField":"S2_Broad_Habitat_ID","Mandatory":1}'
   );
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      $names = array();
      foreach($this->columns as $col => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $table = $params->TargetType;
            $tcol = $params->TargetField;
         } else if (preg_match('/([^\:]+)\:([^\.]+)\.(.+)/',$type,$matches)) {
            $type = $matches[1];
            $table = $matches[2];
            $tcol = $matches[3];
         }
         if (isset($table) && isset($tcol)) {
            $link = null;
            $link = new $table();
            $id = (is_object($this->data[$col]))
                  ?$this->data[$col]->Id
                  :$this->data[$col];
            $link->loaddata($id);
            $names[] = $link->getname();
         }  
      }
      return join(':',$names);   
   }   
}
class S2_Site_UKBAP_Habitat extends DBT {
   protected $tablename = 'S2_Site_UKBAP_Habitat';
   protected $displayname = 'UK BAP Habitat';
   protected $show = array();
   protected $columns = array(
      'S2_Site_UKBAP_Habitat_ID'    => 'Unique Key',
      'S2_Site_ID'                  => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1}',
      'S2_UKBAP_Habitat_ID'         => '{"DataType":"LINKEDTO","TargetType":"S2_UKBAP_Habitat","TargetField":"S2_UKBAP_Habitat_ID","Mandatory":1}'
   );
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      $names = array();
      foreach($this->columns as $col => $type) {
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $table = $params->TargetType;
            $tcol = $params->TargetField;
         } else if (preg_match('/([^\:]+)\:([^\.]+)\.(.+)/',$type,$matches)) {
            $type = $matches[1];
            $table = $matches[2];
            $tcol = $matches[3];
         }
         if (isset($table) && isset($tcol)) {
            $link = null;
            $link = new $table();
            $id = (is_object($this->data[$col]))
                  ?$this->data[$col]->Id
                  :$this->data[$col];
            $link->loaddata($id);
            $names[] = $link->getname();
         }  
      }
      return join(':',$names);   
   }   
}
/*
class S2_Site_Land_Owner extends DBT {
   protected $tablename = 'S2_Site_Land_Owner';
   protected $displayname = 'Land Owner';
   protected $show = array();
   protected $columns = array(
      'S2_Site_Land_Owner_ID' => 'Unique Key',
      'S2_Site_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1}',
      'Contact'               => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"],"Mandatory":1}',
      'From_Date'             => 'Date',
      'Until_Date'            => 'Date',               
      'Compartments'          => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Compartment","TargetField":"S2_Site_Compartment_ID","Property":"Compartments","Inherit":1}'
   );
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );
   function getname() {
      return $this->data['Contact']->Name;
   }   
}
*/

/*
CREATE OR REPLACE VIEW S2_SiteMap AS 
SELECT A.Name AS Authority, S.S2_Site_ID, S.Name AS Site_Name, S.Site_Code, B.Version, B.Boundary, D.Name AS Designation, DS.Name AS Status 
FROM S2_Site_Boundary AS B  
INNER JOIN S2_Site AS S ON (S.S2_Site_ID = B.S2_Site_ID)
INNER JOIN S2_Site_Designation AS SD ON SD.S2_Site_ID = B.S2_Site_ID
INNER JOIN S2_Designation AS D ON SD.S2_Designation_ID = D.S2_Designation_ID 
INNER JOIN S2_Site_Designation_Status AS SDS ON (SDS.S2_Site_Designation_ID = SD.S2_Site_Designation_ID) 
INNER JOIN S2_Designation_Status AS DS ON DS.S2_Designation_Status_ID = SDS.S2_Designation_Status_ID 
INNER JOIN S2_Authority AS A ON A.S2_Authority_ID = S.S2_Authority_ID
WHERE (S.Boundary = B.Version)
AND (SD.Site_Designation_Status = SDS.Version);
*/
switch ($dbPlatform) {
case 'MySQL': {
//MySQL
   class S2_SiteMap extends DBT_ReportView {
      protected $tablename = 'S2_SiteMap';
      protected $domain = 'sites';
      protected $instancedomain = true;
      protected $instanceof = 'S2_Authority';
      protected $indexon = 'S2_Site_ID';  
      protected $selectto = 'S2_Site';
      protected $perpage = 5;
      protected $sourcetables = array( 
                                       "S2_Site_Boundary" => "B", 
                                       "S2_Site" => "S",
                                       "S2_Site_Designation" => "SD",
                                       "S2_Designation" => "D",
                                       "S2_Site_Designation_Status" => "SDS",
                                       "S2_Designation_Status" => "DS",
                                       "S2_Authority" => "A"
                                    );
      protected $joins = array (
                                       "S2_Site" => "S2_Site_Boundary",
                                       "S2_Site_Designation" => "S2_Site",
                                       "S2_Designation" => "S2_Site_Designation",
                                       "S2_Site_Designation_Status" => "S2_Site_Designation",
                                       "S2_Designation_Status" => "S2_Site_Designation_Status",
                                       "S2_Authority" => "S2_Site"
                                 );
      protected $sourcecolumns = array(      
                                       "S2_Authority" => array("S2_Authority_ID", "Authority" => "Name"), 
                                       "S2_Site" => array("S2_Site_ID", "Site_Name" => "Name", "Site_Code"),
                                       "S2_Site_Boundary" => array("Boundary_Version" => "Version", "Boundary"),
                                       "S2_Designation" => array("Designation" => "Name"),
                                       "S2_Designation_Status" => array("Status" => "Name")
                                 );
      protected $colmatch = array(      
                                       "S.Boundary" => "B.Version",
                                       "SD.Site_Designation_Status" => "SDS.Version"
                                 );
      protected $orders = array(
                                       "Designation" => "ASC",
                                       "Status" => "ASC"
                                 );
      protected $nolist = array(       "Authority","Site_Name","Site_Code" );
      
   }
   
   class S2_Site_Last_Visited extends DBT_ReportView {
      protected $tablename = 'S2_Site_Last_Visited';
      protected $domain = 'sites';
      protected $instancedomain = true;
      protected $instanceof = 'S2_Authority';
      protected $indexon = 'S2_Site_ID';  
      protected $selectto = 'S2_Site';
      protected $perpage = 5;
      protected $sourcetables = array( 
                                       "S2_Site" => "S",
                                       "S2_Site_Visit" => "SV",
                                       "S2_Authority" => "A"
                                    );
      protected $joins = array (
                                       "S2_Site_Visit" => "S2_Site",
                                       "S2_Authority" => "S2_Site"
                                 );
      protected $sourcecolumns = array(      
                                       "S2_Authority" => array("S2_Authority_ID"), 
                                       "S2_Site" => array("S2_Site_ID"),
                                       "S2_Site_Visit" => array("Last_Visited" => '{"DataType":"Date","Expression":"Max(SV.Survey_Date)"}')
                                 );
      protected $group =   array(      "S.S2_Site_ID", "A.S2_Authority_ID"  );
      protected $filters = array(      "SV.S2_Site_Visit_ID > ?" => 0   );
   }
   
   class S2_High_Priority_Sites extends DBT_ReportView {
      protected $tablename = 'S2_High_Priority_Sites';
      protected $domain = 'sites';
      protected $instancedomain = true;
      protected $instanceof = 'S2_Authority';
      protected $indexon = 'S2_Site_ID';  
      protected $selectto = 'S2_Site';
      protected $perpage = 5;
      protected $sourcetables = array( 
                                       "S2_Site_Follow_Up" => "F",
                                       "S2_Site" => "S",
                                       "S2_Authority" => "A",
                                       "S2_Priority" => "P",
                                       "S2_Site_Last_Visited" => "V"
                                    );
      protected $joins = array (
                                       "S2_Site" => "S2_Site_Follow_Up",
                                       "S2_Priority" => "S2_Site_Follow_Up",
                                       "S2_Authority" => "S2_Site",
                                       "S2_Site_Last_Visited" => "S2_Site"
                                 );
      protected $sourcecolumns = array(      
                                       "S2_Site_Follow_Up" => array("S2_Priority_ID","From_Year","To_Year"),
                                       "S2_Site" => array("S2_Site_ID"),
                                       "S2_Authority" => array("S2_Authority_ID"), 
                                       "S2_Site_Last_Visited" => array("Last_Visited")
                                 );
      protected $orders =     array(   "S2_Priority_ID" => "DESC");
      protected $group =      array(   "S.S2_Site_ID"   );
      protected $filters =    array(   "F.S2_Priority_ID > ?" => 0,
                                       "F.From_Year > ?" => '{"Expression":"Year(V.Last_Visited)"}');
   }
   
   class S2_Designation_Overview extends DBT_Analysis {
      protected $tablename = 'S2_Designation_Overview';
      protected $displayname = 'Designation Overview';
      //protected $show = array('S2_Site_Management_Agreement');
      protected $domain = 'sites';
      protected $instancedomain = true;
      protected $instanceof = 'S2_Authority';
      protected $indexon = 'S2_Designation_ID';  
      protected $selectto = 'S2_Designation';
      protected $columns = array(
         'S2_Authority_ID'          => '{"DataType":"LINKEDTO","TargetType":"S2_Authority","TargetField":"S2_Authority_ID","Mandatory":1,"Current":1,"Hidden":1,"Inherit":1}',
         'Authority'                => '{"DataType":"Short Text","Hidden":1}',
         'S2_Designation_ID'        => '{"DataType":"LINKEDTO","TargetType":"S2_Designation","TargetField":"S2_Designation_ID","Mandatory":1,"Current":1}',
         'Designation'              => '{"DataType":"Short Text","Hidden":1}',
         'Sites'                    => '{"DataType":"Number"}',
         'Appropriate_Management'   => '{"DataType":"Number"}',
         'Percentage_In_Management' => '{"DataType":"Percentage"}',
         'Candidates'               => '{"DataType":"Number"}',
         'Designated'               => '{"DataType":"Number"}',
         'Deleted'                  => '{"DataType":"Number"}',                    
      ); 
      protected $relieson = array(  
         "S2_Authority",
         "S2_Designation",
         "S2_Site",
         "S2_Site_Designation",
         "S2_Site_Appropriate_Management_Decision",
         "S2_Designation_Status",
         "S2_Site_Designation_Status"
      );
      protected $permissions = array(
         'Def'    => 'Partnership Administrator',
         'List'   => 'Partnership Member',
         'View'   => 'Partnership Member'
      );
      function S2_Designation_Overview() {
         parent::DBT_Analysis();
      }
      function create() {
         foreach($this->relieson as $lclass) {
            $link = new $lclass();
            if ($link && !$link->exists()) $link->create();
         }
            
         $select = "SELECT 
   A.S2_Authority_ID, A.Name AS Authority, 
   D.S2_Designation_ID, D.Name AS Designation, 
   COUNT( * ) AS Sites,
   SUM(SAMD.Management_Is_Appropriate) AS Appropriate_Management,
   (SUM(SAMD.Management_Is_Appropriate)/SUM(DS.Name = 'Designated')) As Percentage_In_Management, 
   SUM(DS.Name = 'Candidate') AS Candidates,
   SUM(DS.Name = 'Designated') AS Designated, 
   SUM(DS.Name = 'Deleted') AS Deleted  
   FROM S2_Designation AS D
   INNER JOIN S2_Site_Designation AS SD 
   ON D.S2_Designation_ID = SD.S2_Designation_ID
   INNER JOIN S2_Site AS S 
   ON S.S2_Site_ID = SD.S2_Site_ID
   INNER JOIN S2_Authority AS A 
   ON S.S2_Authority_ID = A.S2_Authority_ID
   LEFT JOIN S2_Site_Appropriate_Management_Decision AS SAMD 
   ON SD.S2_Site_Designation_ID = SAMD.S2_Site_Designation_ID
   LEFT JOIN S2_Site_Designation_Status AS SDS 
   ON SD.S2_Site_Designation_ID = SDS.S2_Site_Designation_ID AND SD.Site_Designation_Status = SDS.Version 
   LEFT JOIN S2_Designation_Status DS 
   ON SDS.S2_Designation_Status_ID = DS.S2_Designation_Status_ID
   GROUP BY A.S2_Authority_ID, D.S2_Designation_ID
   ORDER BY Authority,Designation"; 
         $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
         $this->dbhandle->exec($statement);
         unset($pending[$this->tablename]);
      }
      function getuserpermissions() {
         return false;      
      }   
   }
// MySQL end
} break;
case 'PGSQL': {
// PGSQL
   class S2_SiteMap extends DBT_ReportView {
      protected $tablename = 'S2_SiteMap';
      protected $domain = 'sites';
      protected $instancedomain = true;
      protected $instanceof = 'S2_Authority';
      protected $indexon = 'S2_Site_ID';  
      protected $selectto = 'S2_Site';
      protected $perpage = 5;
      protected $sourcetables = array( 
                                       "S2_Site_Boundary" => "B", 
                                       "S2_Site" => "S",
                                       "S2_Site_Designation" => "SD",
                                       "S2_Designation" => "D",
                                       "S2_Site_Designation_Status" => "SDS",
                                       "S2_Designation_Status" => "DS",
                                       "S2_Authority" => "A"
                                    );
      protected $joins = array (
                                       "S2_Site" => "S2_Site_Boundary",
                                       "S2_Site_Designation" => "S2_Site",
                                       "S2_Designation" => "S2_Site_Designation",
                                       "S2_Site_Designation_Status" => "S2_Site_Designation",
                                       "S2_Designation_Status" => "S2_Site_Designation_Status",
                                       "S2_Authority" => "S2_Site"
                                 );
      protected $sourcecolumns = array(      
                                       "S2_Authority" => array("S2_Authority_ID", "Authority" => "Name"), 
                                       "S2_Site" => array("S2_Site_ID", "Site_Name" => "Name", "Site_Code"),
                                       "S2_Site_Boundary" => array("Boundary_Version" => "Version", "Boundary"),
                                       "S2_Designation" => array("Designation" => "Name"),
                                       "S2_Designation_Status" => array("Status" => "Name")
                                 );
      protected $colmatch = array(      
                                       "S.Boundary" => "B.Version",
                                       "SD.Site_Designation_Status" => "SDS.Version"
                                 );
      protected $orders = array(
                                       "Designation" => "ASC",
                                       "Status" => "ASC"
                                 );
      protected $nolist = array(       "Authority","Site_Name","Site_Code" );
      
   }
   
   class S2_Site_Last_Visited extends DBT_ReportView {
      protected $tablename = 'S2_Site_Last_Visited';
      protected $domain = 'sites';
      protected $instancedomain = true;
      protected $instanceof = 'S2_Authority';
      protected $indexon = 'S2_Site_ID';  
      protected $selectto = 'S2_Site';
      protected $perpage = 5;
      protected $sourcetables = array( 
                                       "S2_Site" => "S",
                                       "S2_Site_Visit" => "SV",
                                       "S2_Authority" => "A"
                                    );
      protected $joins = array (
                                       "S2_Site_Visit" => "S2_Site",
                                       "S2_Authority" => "S2_Site"
                                 );
      protected $sourcecolumns = array(      
                                       "S2_Authority" => array("S2_Authority_ID"), 
                                       "S2_Site" => array("S2_Site_ID"),
                                       "S2_Site_Visit" => array("Last_Visited" => '{"DataType":"Date","Expression":"Max(SV.Survey_Date)"}')
                                 );
      protected $group =   array(      "S.S2_Site_ID","A.S2_Authority_ID" );
      protected $filters = array(      "SV.S2_Site_Visit_ID > ?" => 0   );
   }
   
   class S2_High_Priority_Sites extends DBT_ReportView {
      protected $tablename = 'S2_High_Priority_Sites';
      protected $domain = 'sites';
      protected $instancedomain = true;
      protected $instanceof = 'S2_Authority';
      protected $indexon = 'S2_Site_ID';  
      protected $selectto = 'S2_Site';
      protected $perpage = 5;
      protected $sourcetables = array( 
                                       "S2_Site_Follow_Up" => "F",
                                       "S2_Site" => "S",
                                       "S2_Authority" => "A",
                                       "S2_Priority" => "P",
                                       "S2_Site_Last_Visited" => "V"
                                    );
      protected $joins = array (
                                       "S2_Site" => "S2_Site_Follow_Up",
                                       "S2_Priority" => "S2_Site_Follow_Up",
                                       "S2_Authority" => "S2_Site",
                                       "S2_Site_Last_Visited" => "S2_Site"
                                 );
      protected $sourcecolumns = array(      
                                       "S2_Site_Follow_Up" => array("S2_Priority_ID","From_Year","To_Year"),
                                       "S2_Site" => array("S2_Site_ID"),
                                       "S2_Authority" => array("S2_Authority_ID"), 
                                       "S2_Site_Last_Visited" => array("Last_Visited")
                                 );
      protected $orders =     array(   "S2_Priority_ID" => "DESC");
      //protected $group =      array(   "S.S2_Site_ID"   );
      protected $filters =    array(   "F.S2_Priority_ID > ?" => 0,
                                       "F.From_Year > ?" => '{"Expression":"date_part(\'year\',V.Last_Visited)"}');
   }
   class S2_Designation_Overview extends DBT_Analysis {
      protected $tablename = 'S2_Designation_Overview';
      protected $displayname = 'Designation Overview';
      //protected $show = array('S2_Site_Management_Agreement');
      protected $domain = 'sites';
      protected $instancedomain = true;
      protected $instanceof = 'S2_Authority';
      protected $indexon = 'S2_Designation_ID';  
      protected $selectto = 'S2_Designation';
      protected $columns = array(
         'S2_Authority_ID'          => '{"DataType":"LINKEDTO","TargetType":"S2_Authority","TargetField":"S2_Authority_ID","Mandatory":1,"Current":1,"Hidden":1,"Inherit":1}',
         'Authority'                => '{"DataType":"Short Text","Hidden":1}',
         'S2_Designation_ID'        => '{"DataType":"LINKEDTO","TargetType":"S2_Designation","TargetField":"S2_Designation_ID","Mandatory":1,"Current":1}',
         'Designation'              => '{"DataType":"Short Text","Hidden":1}',
         'Sites'                    => '{"DataType":"Number"}',
         'Appropriate_Management'   => '{"DataType":"Number"}',
         'Percentage_In_Management' => '{"DataType":"Percentage"}',
         'Candidates'               => '{"DataType":"Number"}',
         'Designated'               => '{"DataType":"Number"}',
         'Deleted'                  => '{"DataType":"Number"}',                    
      ); 
      protected $relieson = array(  
         "S2_Authority",
         "S2_Designation",
         "S2_Site",
         "S2_Site_Designation",
         "S2_Site_Appropriate_Management_Decision",
         "S2_Designation_Status",
         "S2_Site_Designation_Status"
      );
      protected $permissions = array(
         'Def'    => 'Partnership Administrator',
         'List'   => 'Partnership Member',
         'View'   => 'Partnership Member'
      );
      function S2_Designation_Overview() {
         parent::DBT_Analysis();
      }
      function create() {
         global $pending;
         $prereqsmet = true;
         foreach($this->relieson as $lclass) {
            $link = new $lclass();
            if (!$link->exists()) $prereqsmet = false;  
         }
         if ($prereqsmet) {
            $select = "SELECT 
      A.s2_authority_id AS S2_Authority_ID, 
      A.name AS Authority, 
      D.s2_designation_id AS S2_Designation_ID, 
      D.name AS Designation, 
      COUNT( * ) AS Sites,
      SUM(SAMD.management_is_appropriate) AS Appropriate_Management,
      (SUM(SAMD.management_is_appropriate)/SUM(DS.name = 'Designated')) As Percentage_In_Management, 
      SUM(DS.name = 'Candidate') AS Candidates,
      SUM(DS.name = 'Designated') AS Designated, 
      SUM(DS.name = 'Deleted') AS Deleted  
      FROM s2_designation AS D
      INNER JOIN s2_site_designation AS SD 
      ON D.s2_designation_id = SD.s2_designation_id
      INNER JOIN s2_site AS S 
      ON S.s2_site_id = SD.s2_site_id
      INNER JOIN s2_authority AS A 
      ON S.s2_authority_id = A.s2_authority_id
      LEFT JOIN s2_site_appropriate_management_decision AS SAMD 
      ON SD.s2_site_designation_id = SAMD.s2_site_designation_id
      LEFT JOIN s2_site_designation_status AS SDS 
      ON SD.s2_site_designation_id = SDS.s2_site_designation_id AND SD.site_designation_status = SDS.version 
      LEFT JOIN s2_designation_status DS 
      ON SDS.s2_designation_status_id = DS.s2_designation_status_id
      GROUP BY A.s2_authority_id, D.s2_designation_id
      ORDER BY Authority,Designation"; 
            $statement = "CREATE OR REPLACE VIEW $this->tablename AS $select";
            $this->dbhandle->exec($statement);
            unset($pending[$this->tablename]);
         }
      }
      function getuserpermissions() {
         return false;      
      }   
   }
// PGSQL end
}break;
}



class S2_Management_Prescription extends DBT {
   protected $tablename = 'S2_Management_Prescription';
   protected $displayname = 'Management Prescription';
   protected $show = array();
   protected $columns = array(
      'S2_Management_Prescription_ID'  => 'Unique Key',
      'Name'                           => '{"DataType":"Short Text","Mandatory":1}',
      'Code'                           => 'Short Text',
      'Description'                    => 'Long Text' 
   );
   protected $domain = 'sites';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator'
   );
}


class S2_Site_Management_Aim extends DBT {
   protected $tablename = 'S2_Site_Management_Aim';
   protected $displayname = 'Management Aim';
   //protected $show = array('S2_Site_Management_Requirement_Habitat');
   protected $columns = array(
      'S2_Site_Management_Aim_ID'    => 'Unique Key',
      'S2_Site_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1}',
      'Name'                  => 'Short Text',               
      'Description'           => '{"DataType":"Long Text","NoList":1}',               
      'On_Date'               => 'Date',   
      'Compartments'          => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Compartment","TargetField":"S2_Site_Compartment_ID","Property":"Compartments","Inherit":1}'
   ); 
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
}


class S2_Site_Management_Action extends DBT {
   protected $tablename = 'S2_Site_Management_Action';
   protected $displayname = 'Management Action';
   protected $show = array('S2_Site_Management_Action_Status');
   protected $columns = array(
      'S2_Site_Management_Action_ID'    => 'Unique Key',
      'S2_Site_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1}',
      'Prescription'          => '{"DataType":"LINKEDTO","TargetType":"S2_Management_Prescription","TargetField":"S2_Management_Prescription_ID"}',               
      'Notes'                 => '{"DataType":"Long Text","NoList":1}',               
      'Dated'                 => '{"DataType":"Date","Mandatory":1,"Derives":[{"Column":"Target_Date","Method":"s2_targetdate","Params":[{"Type":"Value"},{"Type":"Static","Value":"+5WD"}]}]}',
      'Target_Date'           => '{"DataType":"Date","Derives":[{"Column":"Completed","Method":"s2_getcompletionstatus","Params":[{"Type":"Value"},{"Type":"Column","Value":"Completion_Date"}]}]}',
      'Completion_Date'       => '{"DataType":"Date","Derives":[{"Column":"Completed","Method":"s2_getcompletionstatus","Params":[{"Type":"Column","Value":"Target_Date"},{"Type":"Value"}]}]}',
      'Completed'             => 'Short Text',
      'Compartments'          => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Compartment","TargetField":"S2_Site_Compartment_ID","Property":"Compartments","Inherit":1}'
   ); 
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
   function getname() {
      $names = array();
      $columns = array('S2_Management_Prescription_ID');
      foreach($columns as $col) {
         $type = $this->columns[$col];
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $table = $params->TargetType;
            $tcol = $params->TargetField;
         } else if (preg_match('/([^\:]+)\:([^\.]+)\.(.+)/',$type,$matches)) {
            $type = $matches[1];
            $table = $matches[2];
            $tcol = $matches[3];
         }
         if (isset($table) && isset($tcol)) {
            $link = null;
            $link = new $table();
            $id = (is_object($this->data[$col]))
                  ?$this->data[$col]->Id
                  :$this->data[$col];
            if ($id > 0) {
               $link->loaddata($id);
               $names[] = $link->getshortname();
            }                            
         }  
      }
      return join(':',$names);   
   }
}

class S2_Site_Management_Action_Status extends DBT {
   protected $tablename = 'S2_Site_Management_Action_Status';
   protected $displayname = 'Action Status';
   //protected $show = array('S2_Site_Management_Aim_Status');
   protected $columns = array(
      'S2_Site_Management_Action_Status_ID'  => 'Unique Key',
      'S2_Site_Management_Action_ID'         => '{"DataType":"LINKEDTO","TargetType":"S2_Site_Management_Aim","TargetField":"S2_Site_Management_Aim_ID","Mandatory":1,"Current":1}',
      'Name'                              => 'Short Text',               
      'Description'                       => 'Long Text',               
      'On_Date'                           => 'Date'   
   ); 
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
}
class S2_Site_Management_Scheme extends DBT {
   protected $tablename = 'S2_Site_Management_Scheme';
   protected $displayname = 'Management Scheme';
   //protected $show = array('S2_Site_Management_Agreement');
   protected $columns = array(
      'S2_Site_Management_Scheme_ID'    => 'Unique Key',
      'S2_Site_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1}',
      'Name'                  => 'Short Text',               
      'Description'           => '{"DataType":"Long Text","NoList":1}',               
      'From_Date'             => '{"DataType":"Date","Derives":[{"Column":"Valid","Method":"s2_isvalid","Params":[{"Type":"Value"},{"Type":"Column","Value":"Expiry"}]}]}',
      'Until_Date'            => '{"DataType":"Date","Derives":[{"Column":"Expiry","Method":"s2_targetdate","Params":[{"Type":"Value"},{"Type":"Static","Value":"+5Y"}]}]}',
      'Expiry'                => '{"DataType":"Date","Derives":[{"Column":"Valid","Method":"s2_isvalid","Params":[{"Type":"Column","Value":"From_Date"},{"Type":"Value"}]}]}',
      'Valid'                 => 'Short Text',
      'Compartments'          => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Compartment","TargetField":"S2_Site_Compartment_ID","Property":"Compartments","Inherit":1}',
      'Applies_To'            => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Designation","TargetField":"S2_Site_Designation_ID","Property":"Applies_To","Inherit":1}',
      'Unsuitable_For'        => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Designation","TargetField":"S2_Site_Designation_ID","Property":"Unsuited_To","Inherit":1}',
      'Source_Datasets'       => '{"DataType":"LINKEDTOM","TargetType":"S2_Supporting_Document","TargetField":"S2_Supporting_Document_ID","Property":"SDoc"}'
   ); 
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
}
class S2_Site_Management_Plan extends DBT {
   protected $tablename = 'S2_Site_Management_Plan';
   protected $displayname = 'Management Plan';
   //protected $show = array('S2_Site_Management_Agreement');
   protected $columns = array(
      'S2_Site_Management_Plan_ID'    => 'Unique Key',
      'S2_Site_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1}',
      'Name'                  => 'Short Text',               
      'Description'           => '{"DataType":"Long Text","NoList":1}',               
      'From_Date'             => '{"DataType":"Date","Derives":[{"Column":"Valid","Method":"s2_isvalid","Params":[{"Type":"Value"},{"Type":"Column","Value":"Expiry"}]}]}',
      'Until_Date'            => '{"DataType":"Date","Derives":[{"Column":"Expiry","Method":"s2_targetdate","Params":[{"Type":"Value"},{"Type":"Static","Value":"+5Y"}]}]}',
      'Expiry'                => '{"DataType":"Date","Derives":[{"Column":"Valid","Method":"s2_isvalid","Params":[{"Type":"Column","Value":"From_Date"},{"Type":"Value"}]}]}',
      'Valid'                 => 'Short Text',
      'Compartments'          => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Compartment","TargetField":"S2_Site_Compartment_ID","Property":"Compartments","Inherit":1}',
      'Applies_To'            => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Designation","TargetField":"S2_Site_Designation_ID","Property":"Applies_To","Inherit":1}',
      'Unsuitable_For'        => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Designation","TargetField":"S2_Site_Designation_ID","Property":"Unsuited_To","Inherit":1}'
   ); 
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
}
class S2_Site_Management_BAP extends DBT {
   protected $tablename = 'S2_Site_Management_BAP';
   protected $displayname = 'Biodiversity Action Plan';
   //protected $show = array('S2_Site_Management_Agreement');
   protected $columns = array(
      'S2_Site_Management_BAP_ID'    => 'Unique Key',
      'S2_Site_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1}',
      'Name'                  => 'Short Text',               
      'Description'           => '{"DataType":"Long Text","NoList":1}',               
      'From_Date'             => '{"DataType":"Date","Derives":[{"Column":"Valid","Method":"s2_isvalid","Params":[{"Type":"Value"},{"Type":"Column","Value":"Expiry"}]}]}',
      'Until_Date'            => '{"DataType":"Date","Derives":[{"Column":"Expiry","Method":"s2_targetdate","Params":[{"Type":"Value"},{"Type":"Static","Value":"+5Y"}]}]}',
      'Expiry'                => '{"DataType":"Date","Derives":[{"Column":"Valid","Method":"s2_isvalid","Params":[{"Type":"Column","Value":"From_Date"},{"Type":"Value"}]}]}',
      'Valid'                 => 'Short Text',
      'Compartments'          => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Compartment","TargetField":"S2_Site_Compartment_ID","Property":"Compartments","Inherit":1}',
      'Applies_To'            => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Designation","TargetField":"S2_Site_Designation_ID","Property":"Applies_To","Inherit":1}',
      'Unsuitable_For'        => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Designation","TargetField":"S2_Site_Designation_ID","Property":"Unsuited_To","Inherit":1}'
   ); 
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
}




class S2_Site_Appropriate_Management_Decision extends DBT {
   protected $tablename = 'S2_Site_Appropriate_Management_Decision';
   protected $displayname = 'Appropriate Management Decision';
   //protected $show = array('S2_Site_Management_Agreement');
   protected $columns = array(
      'S2_Site_Appropriate_Management_Decision_ID'    => 'Unique Key',
      'S2_Site_ID'                  => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1}',
      'S2_Site_Designation_ID'      => '{"DataType":"LINKEDTO","TargetType":"S2_Site_Designation","TargetField":"S2_Site_Designation_ID","Mandatory":1,"Inherit":1}',
      'Management_Is_Appropriate'   => 'True or False',               
      'On_Date'                     => 'Date',
      'Applies_From'                => 'Date',
      'Applies_Until'               => 'Date'   
   ); 
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
   function getname() {
      $names = array();
      $columns = array('S2_Site_Designation_ID');
      foreach($columns as $col) {
         $type = $this->columns[$col];
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $table = $params->TargetType;
            $tcol = $params->TargetField;
         } else if (preg_match('/([^\:]+)\:([^\.]+)\.(.+)/',$type,$matches)) {
            $type = $matches[1];
            $table = $matches[2];
            $tcol = $matches[3];
         }
         if (isset($table) && isset($tcol)) {
            $link = null;
            $link = new $table();
            $id = (is_object($this->data[$col]))
                  ?$this->data[$col]->Id
                  :$this->data[$col];
            if ($id > 0) {
               $link->loaddata($id);
               $names[] = $link->getshortname();
            }                            
         }  
      }
      return join(':',$names);   
   }
}
class S2_Site_Management_Advice extends DBT {
   protected $tablename = 'S2_Site_Management_Advice';
   protected $displayname = 'Management Advice';
   //protected $show = array('S2_Site_Management_Agreement');
   protected $columns = array(
      'S2_Site_Management_Advice_ID'    => 'Unique Key',
      'S2_Site_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1}',
      'Contact'               => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Conversation"],"Mandatory":1}',
      'Sent_Date'             => '{"DataType":"Date","Derives":[{"Column":"Target_Date","Method":"s2_targetdate","Params":[{"Type":"Value"},{"Type":"Static","Value":"+4W"}]}]}',
      'Target_Date'           => 'Date',
      'Response_Date'         => 'Date',
      'Advice_Accepted'       => 'True or False',         
      'Compartments'          => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Compartment","TargetField":"S2_Site_Compartment_ID","Property":"Compartments","Inherit":1}'
   ); 
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
}


/*
class Example extends DBT {
   //
   // CreatedBy and CreatedOn are added and populated automatically
   //
   protected $tablename = 'ExampleTable1';
   protected $displayname = 'Example Table'; // Describes how the table will appear on the screen. 
   protected $columns = array(
      'Example_ID'      => 'Unique Key',
      'Name'            => 'Short Text',
      'Value'           => 'Number',
      'Link'            => 'LINKEDTO:DBTableName1.DBColumnName:Optional'                  
   );
}
*/

class S2_Criteria extends DBT {
   protected $tablename = 'S2_Criteria';
   protected $displayname = 'Criteria';
   protected $alts = array('Authorities'=>'S2_Authority','Designations'=>'S2_Designations');
   protected $show = array('S2_Site_Designation_Criteria');
   protected $columns = array(
      'S2_Criteria_ID'  => 'Unique Key',
      'Name'            => 'Short Text',
      'Code'            => 'Short Text',
      'Description'     => 'Long Text'
   );
   protected $domain = 'sites';
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
   function getshortname() {
      return $this->data['Code'];
   }
}

class S2_Site_Designation_Criteria extends DBT {
   protected $tablename = 'S2_Site_Designation_Criteria';
   protected $displayname = 'Site Designation Criteria';
   protected $columns = array(
      'S2_Site_Designation_Criteria_ID'   => 'Unique Key',
      'S2_Site_Designation_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_Site_Designation","TargetField":"S2_Site_Designation_ID","Mandatory":1,"Current":1}',
      'S2_Criteria_ID'                    => '{"DataType":"LINKEDTO","TargetType":"S2_Criteria","TargetField":"S2_Criteria_ID","Mandatory":1}',
      'Score'                             => 'Decimal',
      'From_Date'                         => 'Date',
      'Until_Date'                        => 'Date',
      'Compartments'                      => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Compartment","TargetField":"S2_Site_Compartment_ID","Property":"Compartments","Inherit":1}'               
   ); 
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
   function getname() {
      $names = array();
      $columns = array('S2_Site_ID','S2_Criteria_ID');
      foreach($columns as $col) {
         $type = $this->columns[$col];
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $table = $params->TargetType;
            $tcol = $params->TargetField;
         } else if (preg_match('/([^\:]+)\:([^\.]+)\.(.+)/',$type,$matches)) {
            $type = $matches[1];
            $table = $matches[2];
            $tcol = $matches[3];
         }
         if (isset($table) && isset($tcol)) {
            $link = null;
            $link = new $table();
            $id = (is_object($this->data[$col]))
                  ?$this->data[$col]->Id
                  :$this->data[$col];
            if ($id > 0) {
               $link->loaddata($id);
               $names[] = $link->getshortname();
            }
         }  
      }
      return join(':',$names);   
   }   
}

// Citations can be applied to the site or to a compartment so there is a 
// separate compartment citation table

// The citation table contains the settings and the citation_reports table is a 
// derived view which produces the citation report to the screen.

/* ,"Derives":[
      {
         "Column":"To_Year",
         "Method":"s2_getyear",
         "Params":[
            {
               "Type":"Static",
               "Value":5
            },
            {
               "Type":"Value"
            }
         ]
      }
   ]
*/  
class S2_Site_Citation extends DBT {
   protected $tablename = 'S2_Site_Citation';
   protected $displayname = 'Citation';
   protected $view = '[
      {
         "Name":"Candidate",
         "Data": [
            {
               "Table":"S2_Site_Citation_Report",
               "Show":"This",
               "View":"Candidate"
            }                                    
         ]
      },
      {
         "Name":"Land Owner",
         "Data": [
            {
               "Table":"S2_Site_Citation_Report",
               "Show":"This",
               "View":"Land Owner"
            }                                    
         ]
      },
      {
         "Name":"Commercial User",
         "Data": [
            {
               "Table":"S2_Site_Citation_Report",
               "Show":"This",
               "View":"Commercial User"
            }                                    
         ]
      },
      {
         "Name":"Statutory Body",
         "Data": [
            {
               "Table":"S2_Site_Citation_Report",
               "Show":"This",
               "View":"Statutory Body"
            }                                    
         ]
      }
   ]';
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $columns = array(
      'S2_Site_Citation_ID'   => 'Unique Key',
      'S2_Site_Designation_ID'=> '{"DataType":"LINKEDTO","TargetType":"S2_Site_Designation","TargetField":"S2_Site_Designation_ID","Mandatory":1,"Current":1}',
      'S2_Site_Boundary_ID'   => '{"DataType":"LINKEDTO","TargetType":"S2_Site_Boundary","TargetField":"S2_Site_Boundary_ID","Inherit":1}',
      'S2_Site_Visit_ID'      => '{"DataType":"LINKEDTO","TargetType":"S2_Site_Visit","TargetField":"S2_Site_Visit_ID","Inherit":1,"Derives":[{"Column":"Data_From","Method":"s2_getdatecolumnvalue","Params":[{"Type":"Static","Value":"S2_Site_Visit"},{"Type":"Value"},{"Type":"Static","Value":"Survey_Date"}]},{"Column":"Data_To","Method":"s2_getdatecolumnvalue","Params":[{"Type":"Static","Value":"S2_Site_Visit"},{"Type":"Value"},{"Type":"Static","Value":"Survey_Date"}]}]}',
      'Data_From'             => '{"DataType":"Date","NoList":1}',
      'Data_To'               => '{"DataType":"Date","NoList":1}',
      'Number'                => '{"DataType":"Count","CountOfType":"S2_Site_Designation","CountOfField":"S2_Site_Designation_ID"}',
      'Source'                => '{"DataType":"Short Text","NoList":1,"Options":["Recorder 6","NBN Gateway","WFS Server"]}'
   ); 
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
   function getname() {
      $names = array();
      $columns = array('S2_Site_Designation_ID','Number');
      foreach($columns as $col) {
         $type = $this->columns[$col];
         if (preg_match('/^\{/',$type)) {
            $params = json_decode($type);
            $type = $params->DataType;
            $table = $params->TargetType;
            $tcol = $params->TargetField;
         } else if (preg_match('/([^\:]+)\:([^\.]+)\.(.+)/',$type,$matches)) {
            $type = $matches[1];
            $table = $matches[2];
            $tcol = $matches[3];
         }
         switch($type) {
         case 'LINKEDTO': {
            if (isset($table) && isset($tcol)) {
               $link = null;
               $link = new $table();
               $id = (is_object($this->data[$col]))
                     ?$this->data[$col]->Id
                     :$this->data[$col];
               if ($id > 0) {
                  $link->loaddata($id);
                  $names[] = $link->getshortname();
               }
            }
         }break;
         default: {
            $names[] = $this->data[$col];
         }break;
         }  
      }
      return join(':',$names);   
   }
}

class S2_Site_Citation_Report extends DBT_ReportView {
   protected $tablename = 'S2_Site_Citation_Report';
   protected $displayname = 'Citation Report';
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $indexon = 'S2_Site_Citation_ID';  
   protected $selectto = 'S2_Site_Citation';
   protected $perpage = 5;
   protected $sourcetables = array( 
                                    "S2_Site_Citation" => "SC",
                                    "S2_Site_Designation" => "SD",
                                    "S2_Designation" => "D",
                                    "S2_Site" => "S",
                                    "S2_Site_Description" => "SD2",
                                    "S2_Site_Boundary" => "SB",
                                    "S2_Authority" => "A"
                                 );
   protected $joins = array (
                                    "S2_Site_Designation" => "S2_Site_Citation",     
                                    "S2_Site" => "S2_Site_Designation",
                                    "S2_Site_Description" => "S2_Site",
                                    "S2_Designation" => "S2_Site_Designation",
                                    "S2_Authority"       => "S2_Site",
                                    "S2_Site_Boundary"   => "S2_Site_Citation"
                              );
   protected $sourcecolumns = array(      
                                    "S2_Authority"          => array("S2_Authority_ID", "Authority" => "Name"), 
                                    "S2_Designation"        => array("Designation" => "Abbreviation"), 
                                    "S2_Site"               => array("S2_Site_ID", "Site_Name" => "Name", "Site_Code"),
                                    "S2_Site_Citation"      => array("S2_Site_Citation_ID","Number","Data_From","Data_To","Source"),
                                    "S2_Site_Designation"   => array("S2_Site_Designation_ID"), 
                                    "S2_Site_Description"   => array("S2_Site_Description_ID","Site_Description","Version"), 
                                    "S2_Site_Boundary"      => array("S2_Site_Boundary_ID","Boundary_Version" => "Version","Boundary")
                              );
   protected $orders =   array(     "SC.Number" => "ASC");
   protected $colmatch = array(      
                                    "S.Description" => "SD2.Version"
                              );
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
   protected $view = '[
      {
         "Name":"Candidate",
         "Data": [
            {
               "Name":"Site",
               "Table":"S2_Site",
               "ReadOnly":true,
               "Columns":["Name","Site_Code"],
               "Show":"This",
               "Link":"S2_Site_ID"
            },
            {
               "Name":"Designation",
               "Table":"S2_Site_Designation",
               "Columns":["S2_Designation_ID","From_Date","Until_Date"],
               "Show":"This",
               "Link":"S2_Site_Designation_ID"
            },
            {
               "Name":"Site Description",
               "Table":"S2_Site_Description",
               "Columns":["S2_Description_ID","Site_Description","Version"],
               "Show":"This",
               "Link":"S2_Site_Description_ID"
            }
         ],
         "External": [
            {
               "Name":"Species Data",
               "Server":"data.yhedn.org.uk/r6",
               "Target":"/species2json.php",
               "Credentials":{
                  "Parameter":"Source",
                  "Encrypt":true
               },
               "Encrypt":true,
               "Parameters": [
                  {"Name":"Action","Type":"Static","Value":"GetSpeciesList"},
                  {"Name":"Source","Type":"Column","Value":"Source"},
                  {"Name":"Authority","Type":"Column","Value":"Authority"},
                  {"Name":"Site_Code","Type":"Column","Value":"Site_Code"},
                  {"Name":"Boundary","Type":"Column","Value":"Boundary"},
                  {"Name":"Start_Date","Type":"Column","Value":"Data_From"},
                  {"Name":"End_Date","Type":"Column","Value":"Data_To"},
                  {
                     "Name":"Sections",
                     "Type":"Static",
                     "Value":[
                        {
                           "Name":"Assessment",
                           "Output_Type":"Summary",
                           "Filter":"Indicator"
                        },
                        {
                           "Name":"Indicator Species",
                           "Output_Type":"Status",
                           "Filter":"Indicator"
                        },
                        {
                           "Name":"Notable Species",
                           "Output_Type":"Species List",
                           "Filter":"Indicator",
                           "Highlight":true,
                           "Remove":false
                        }
                     ]
                  }
               ]
            }
         ]
      },
      {
         "Name":"Land Owner",
         "Data": [
            {
               "Name":"Site",
               "Table":"S2_Site",
               "ReadOnly":true,
               "Columns":["Name","Site_Code"],
               "Show":"This",
               "Link":"S2_Site_ID"
            },
            {
               "Name":"Designation",
               "Table":"S2_Site_Designation",
               "Columns":["S2_Designation_ID","From_Date"],
               "Show":"This",
               "Link":"S2_Site_Designation_ID"
            },
            {
               "Name":"Site Description",
               "Table":"S2_Site_Description",
               "Columns":["S2_Description_ID","Site_Description","Version"],
               "Show":"This",
               "Link":"S2_Site_Description_ID"
            }
         ],
         "External": [
            {
               "Name":"Species Data",
               "Server":"data.yhedn.org.uk/r6",
               "Target":"/species2json.php",
               "Credentials":{
                  "Parameter":"Source",
                  "Encrypt":true
               },
               "Encrypt":true,
               "Parameters": [
                  {"Name":"Action","Type":"Static","Value":"GetSpeciesList"},
                  {"Name":"Source","Type":"Column","Value":"Source"},
                  {"Name":"Authority","Type":"Column","Value":"Authority"},
                  {"Name":"Site_Code","Type":"Column","Value":"Site_Code"},
                  {"Name":"Boundary","Type":"Column","Value":"Boundary"},
                  {"Name":"Start_Date","Type":"Column","Value":"Data_From"},
                  {"Name":"End_Date","Type":"Column","Value":"Data_To"},
                  {
                     "Name":"Sections",
                     "Type":"Static",
                     "Value":[
                        {
                           "Name":"Notable Species",
                           "Output_Type":"Species List",
                           "Filter":"Indicator",
                           "Remove":true
                        }
                     ]
                  }
               ]
            }
         ]
      },
      {
         "Name":"Commercial User",
         "Data": [
            {
               "Name":"Site",
               "Table":"S2_Site",
               "ReadOnly":true,
               "Columns":["Name","Site_Code"],
               "Show":"This",
               "Link":"S2_Site_ID"
            },
            {
               "Name":"Designation",
               "Table":"S2_Site_Designation",
               "Columns":["S2_Designation_ID","From_Date","Until_Date"],
               "Show":"This",
               "Link":"S2_Site_Designation_ID"
            },
            {
               "Name":"Site Description",
               "Table":"S2_Site_Description",
               "Columns":["S2_Description_ID","Site_Description","Version"],
               "Show":"This",
               "Link":"S2_Site_Description_ID"
            }
         ],
         "External": [
            {
               "Name":"Species Data",
               "Server":"data.yhedn.org.uk/r6",
               "Target":"/species2json.php",
               "Credentials":{
                  "Parameter":"Source",
                  "Encrypt":true
               },
               "Encrypt":true,
               "Parameters": [
                  {"Name":"Action","Type":"Static","Value":"GetSpeciesList"},
                  {"Name":"Source","Type":"Column","Value":"Source"},
                  {"Name":"Authority","Type":"Column","Value":"Authority"},
                  {"Name":"Site_Code","Type":"Column","Value":"Site_Code"},
                  {"Name":"Boundary","Type":"Column","Value":"Boundary"},
                  {"Name":"Start_Date","Type":"Column","Value":"Data_From"},
                  {"Name":"End_Date","Type":"Column","Value":"Data_To"},
                  {
                     "Name":"Sections",
                     "Type":"Static",
                     "Value":[
                        {
                           "Name":"Full Species List",
                           "Output_Type":"Species List",
                           "Filter":"Indicator",
                           "Highlight":true,
                           "Remove":false
                        }
                     ]
                  }
               ]
            }
         ]
      },
      {
         "Name":"Statutory Body",
         "Data": [
            {
               "Name":"Site",
               "Table":"S2_Site",
               "ReadOnly":true,
               "Columns":["Name","Site_Code"],
               "Show":"This",
               "Link":"S2_Site_ID"
            },
            {
               "Name":"Designation",
               "Table":"S2_Site_Designation",
               "Columns":["S2_Designation_ID","From_Date","Until_Date"],
               "Show":"This",
               "Link":"S2_Site_Designation_ID"
            },
            {
               "Name":"Site Description",
               "Table":"S2_Site_Description",
               "Columns":["S2_Description_ID","Site_Description","Version"],
               "Show":"This",
               "Link":"S2_Site_Description_ID"
            }
         ],
         "External": [
            {
               "Name":"Species Data",
               "Server":"data.yhedn.org.uk/r6",
               "Target":"/species2json.php",
               "Credentials":{
                  "Parameter":"Source",
                  "Encrypt":true
               },
               "Encrypt":true,
               "Parameters": [
                  {"Name":"Action","Type":"Static","Value":"GetSpeciesList"},
                  {"Name":"Source","Type":"Column","Value":"Source"},
                  {"Name":"Authority","Type":"Column","Value":"Authority"},
                  {"Name":"Site_Code","Type":"Column","Value":"Site_Code"},
                  {"Name":"Boundary","Type":"Column","Value":"Boundary"},
                  {"Name":"Start_Date","Type":"Column","Value":"Data_From"},
                  {"Name":"End_Date","Type":"Column","Value":"Data_To"},
                  {
                     "Name":"Sections",
                     "Type":"Static",
                     "Value":[
                        {
                           "Name":"Assessment",
                           "Output_Type":"Summary",
                           "Filter":"Indicator"
                        },
                        {
                           "Name":"Indicator Species",
                           "Output_Type":"Status",
                           "Filter":"Indicator"
                        },
                        {
                           "Name":"Notable Species",
                           "Output_Type":"Species List",
                           "Filter":"Indicator",
                           "Highlight":true,
                           "Remove":false
                        }
                     ]
                  }
               ]
            }
         ]
      }
   ]';
}


/* 

   NEW ACCESS TEMPLATE
    
*/
class S2_Programme extends DBT {
   protected $tablename = 'S2_Programme';
   protected $displayname = 'Survey Programme';
   protected $alts = array('Sites'=>'S2_Site');
   protected $show = array('S2_Programme_Surveyor','S2_Site_Survey');
   protected $columns = array(
      'S2_Programme_ID'    => 'Unique Key',
      'Name'               => 'Short Text',
      'Established'        => 'Date'
   );
   protected $domain = 'sites';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );     
}
class S2_Programme_Surveyor extends DBT {
   protected $tablename = 'S2_Programme_Surveyor';
   protected $displayname = 'Surveyor';
   //protected $alts = array('Programme'=>'S2_Programme','Sites'=>'S2_Site');
   protected $columns = array(
      'S2_Programme_Surveyor_ID'    => 'Unique Key',
      'S2_Programme_ID'             => '{"DataType":"LINKEDTO","TargetType":"S2_Programme","TargetField":"S2_Programme_ID","Mandatory":1,"Current":1}',
      'Surveyor'                    => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"],"Mandatory":1}',
      'Contracted'                  => 'Date'
   );
   protected $domain = 'sites';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      //return $this->data['Surveyor']->Name;
      return $this->derivename(array('S2_Programme_ID','Surveyor'));
   }      
}


class S2_Site_Survey extends DBT {
   protected $tablename = 'S2_Site_Survey';
   protected $displayname = 'Site Survey';
   //protected $alts = array('Programme'=>'S2_Programme','Sites'=>'S2_Site');
   protected $show = array('S2_Survey_Notification','S2_Site_Surveyor');
   protected $columns = array(
      'S2_Site_Survey_ID'     => 'Unique Key',
      'S2_Programme_ID'       => '{"DataType":"LINKEDTO","TargetType":"S2_Programme","TargetField":"S2_Programme_ID","Current":1}',
      'S2_Site_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Current":1}',
      'Date_From'             => 'Date',
      'Date_To'               => 'Date',      
      'Compartments'          => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Compartment","TargetField":"S2_Site_Compartment_ID","Property":"Compartments","Inherit":1}'
   );
   protected $domain = 'sites';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
}

class S2_Site_Surveyor extends DBT {
   protected $tablename = 'S2_Site_Surveyor';
   protected $displayname = 'Site Surveyor';
   //protected $alts = array('Programme'=>'S2_Programme','Sites'=>'S2_Site');
   //protected $show = array('S2_Survey_Notification');
   protected $columns = array(
      'S2_Site_Surveyor_ID'         => 'Unique Key',
      'S2_Programme_Surveyor_ID'    => '{"DataType":"LINKEDTO","TargetType":"S2_Programme_Surveyor","TargetField":"S2_Programme_Surveyor_ID","Inherit":1}',
      'S2_Site_Survey_ID'           => '{"DataType":"LINKEDTO","TargetType":"S2_Site_Survey","TargetField":"S2_Site_Survey_ID","Current":1}',
      'Contracted'                  => 'Date',
      'Pack_Sent'                   => 'Date',
      'Data_Received'               => 'Date'
   );
   protected $domain = 'sites';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      return $this->derivename(array('S2_Programme_Surveyor_ID','Contracted'));
   }   
}

class S2_Survey_Notification extends DBT {
   protected $tablename = 'S2_Survey_Notification';
   protected $displayname = 'Survey Notification';
   //protected $alts = array('Programme'=>'S2_Programme','Sites'=>'S2_Site');
   protected $columns = array(
      'S2_Survey_Notification_ID'   => 'Unique Key',
      'S2_Site_Survey_ID'           => '{"DataType":"LINKEDTO","TargetType":"S2_Site_Survey","TargetField":"S2_Site_Survey_ID","Current":1,"Inherit":1}',
      'Sent'                        => 'Date',
      'Acknowledged'                => 'Date',
      'Access_Granted'              => 'True or False'
   );
   protected $domain = 'sites';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );      
}

class S2_Site_Access extends DBT {
   protected $tablename = 'S2_Site_Access';
   protected $displayname = 'Site Access';
   //protected $alts = array('Programme'=>'S2_Programme','Sites'=>'S2_Site');
   protected $columns = array(
      'S2_Site_Access_ID'    => 'Unique Key',
      'S2_Site_ID'           => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Current":1,"Inherit":1}',
      'S2_Access_Type_ID'    => '{"DataType":"LINKEDTO","TargetType":"S2_Access_Type","TargetField":"S2_Access_Type_ID"}'
   );
   protected $domain = 'sites';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );   
}

class S2_Access_Type extends DBT {
   protected $tablename = 'S2_Access_Type';
   protected $displayname = 'Access Type';
   protected $alts = array('Programme'=>'S2_Programme','Sites'=>'S2_Site');
   protected $columns = array(
      'S2_Access_Type_ID'  => 'Unique Key',
      'Name'               => 'Short Text',
      'Description'        => 'Long Text' 
   );
   protected $domain = 'sites';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'S2_Access_Type' => '
"Name","Description"
"Public Access","Road verges and such."
"Open Access","Private land but with public right of way."
"Private (Unknown)","Private land where contact has not been made with land owner."
"Private (Agreed)","Private land where survey rights have been granted."
"Private (Agreed - With Notification)","Private land where survey rights have been granted but only with prior notification."
"Private (Refused)","Private land where permission to survey has been refused."'
   );         
}

class S2_Site_Consideration extends DBT {
   protected $tablename = 'S2_Site_Consideration';
   protected $displayname = 'Access Consideration';
   //protected $alts = array('Programme'=>'SRV_Programme','Sites'=>'SRV_Site');
   protected $columns = array(
      'S2_Site_Consideration_Type_ID'  => 'Unique Key',
      'S2_Site_ID'                     => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Current":1,"Inherit":1}',
      'S2_Consideration_Type_ID'       => '{"DataType":"LINKEDTO","TargetType":"S2_Consideration_Type","TargetField":"S2_Consideration_Type_ID"}',
      'Name'                           => 'Short Text',
      'Description'                    => 'Long Text' 
   );
   protected $domain = 'sites';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      return $this->derivename(array('Name','S2_Consideration_Type_ID'));
   }      
}

class S2_Consideration_Type extends DBT {
   protected $tablename = 'S2_Consideration_Type';
   protected $displayname = 'Consideration Type';
   protected $alts = array('Programme'=>'S2_Programme','Sites'=>'S2_Site');
   protected $columns = array(
      'S2_Consideration_Type_ID'    => 'Unique Key',
      'Name'                        => 'Short Text',
      'Description'                 => 'Long Text' 
   );
   protected $domain = 'sites';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'S2_Consideration_Type' => '
"Name","Description"
"Restriction","Site unavailable for survey between given dates."
"Hazard","Electric fences, Steep banks, Barbed wire ..."
"Copy Data","Send copy of survey date to land owner."
"Condition","Other condition."'
   );            
}

class S2_Site_Contact extends DBT {
   protected $tablename = 'S2_Site_Contact';
   protected $displayname = 'Site Contact';
   protected $show = array();
   protected $columns = array(
      'S2_Site_Contact_ID'    => 'Unique Key',
      'S2_Site_ID'            => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1}',
      'Contact'               => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"],"Mandatory":1}',
      'Type'                  => '{"DataType":"LINKEDTO","TargetType":"S2_Contact_Type","TargetField":"S2_Contact_Type_ID"}',
      'From_Date'             => 'Date',
      'Until_Date'            => 'Date',               
      'Compartments'          => '{"DataType":"LINKEDTOM","TargetType":"S2_Site_Compartment","TargetField":"S2_Site_Compartment_ID","Property":"Compartments","Inherit":1}'
   );
   protected $domain = 'sites';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      return $this->derivename(array('Contact','Type'));
   }      
}

class S2_Contact_Type extends DBT {
   protected $tablename = 'S2_Contact_Type';
   protected $displayname = 'Contact Type';
   protected $alts = array('Programme'=>'S2_Programme','Sites'=>'S2_Site');
   protected $columns = array(
      'S2_Contact_Type_ID'    => 'Unique Key',
      'Name'                  => 'Short Text',
      'Description'           => 'Long Text' 
   );
   protected $domain = 'sites';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'S2_Contact_Type' => '
"Name","Description"
"Owner",""
"Tenant",""
"Agent",""
"Manager",""
"Game Keeper",""
"Warden",""'
   );         
}

class S2_Site_Notification_Rule extends DBT {
   protected $tablename = 'S2_Site_Notification_Rule';
   protected $displayname = 'Site Notification Rule';
   //protected $alts = array('Programme'=>'S2_Programme','Sites'=>'S2_Site');
   protected $columns = array(
      'S2_Site_Notification_Rule_ID'   => 'Unique Key',
      'S2_Site_ID'                     => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1}',
      'S2_Site_Contact_ID'             => '{"DataType":"LINKEDTO","TargetType":"S2_Site_Contact","TargetField":"S2_Site_Contact_ID","Inherit":1}',
      'Type'                           => '{"DataType":"LINKEDTO","TargetType":"S2_Notification_Type","TargetField":"S2_Notification_Type_ID"}',
      'Media'                          => '{"DataType":"LINKEDTO","TargetType":"S2_Media_Type","TargetField":"S2_Media_Type_ID"}',
      'Notice_Period_Days'             => 'Number',        
   );
   protected $domain = 'sites';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      return $this->derivename(array('Media','Type'));
   }
}

class S2_Notification_Type extends DBT {
   protected $tablename = 'S2_Notification_Type';
   protected $displayname = 'Notification Type';
   protected $alts = array('Programme'=>'S2_Programme','Sites'=>'S2_Site');
   protected $columns = array(
      'S2_Notification_Type_ID' => 'Unique Key',
      'Name'               => 'Short Text',
      'Description'        => 'Long Text' 
   );
   protected $domain = 'sites';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'S2_Notification_Type' => '
"Name","Description"
"Required","Surveyors cannot access land without prior notification."
"Courtesy","Land owner would like to know that people are on their land."
"Preferred","Land owner would prefer notification in advance but it is not a requirement."'
   );         
}
class S2_Media_Type extends DBT {
   protected $tablename = 'S2_Media_Type';
   protected $displayname = 'Media';
   protected $alts = array('Programme'=>'S2_Programme','Sites'=>'S2_Site');
   protected $columns = array(
      'S2_Media_Type_ID'   => 'Unique Key',
      'Name'               => 'Short Text',
      'Description'        => 'Long Text' 
   );
   protected $domain = 'sites';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   protected $primingdata = array(
      'S2_Media_Type' => '
"Name","Description"
"Written (Post)",""
"Written (Email)",""
"Phone",""
"Person","Knock at address before carrying out survey."'
   );         
}

class S2_Site_Photo extends DBT {
   protected $tablename = 'S2_Site_Photo';
   protected $displayname = 'Photo';
   protected $columns = array(
      'S2_Site_Photo_ID'   => 'Unique Key',
      'S2_Site_ID'         => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Current":1}',
      'Title'              => '{"DataType":"Short Text"}',
      'Description'        => '{"DataType":"Long Text","NoList":1}',
      'Photographer'       => '{"DataType":"INTERFACE","Domain":"contacts","ValidTypes":["S2_Person","S2_Organisation"]}',
      'Received'           => '{"DataType":"Date","NoList":1}',
      'File'               => '{"DataType":"File Path","Mandatory":1}'
   );
   protected $domain = 'sites';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      return $this->derivename(array('Title','Photographer'));
   }
}
class S2_Site_Doc extends DBT {
   protected $tablename = 'S2_Site_Doc';
   protected $displayname = 'Site Document';
   protected $columns = array(
      'S2_Site_Doc_ID'     => 'Unique Key',
      'S2_Site_ID'         => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Current":1}',
      'Title'              => '{"DataType":"Short Text"}',
      'Description'        => '{"DataType":"Long Text","NoList":1}',
      'Received'           => '{"DataType":"Date","NoList":1}',
      'File'               => '{"DataType":"File Path","Mandatory":1}'
   );
   protected $domain = 'sites';
   protected $instancedomain = false;
   protected $permissions = array(
      'Def'    => 'System Administrator',
      'List'   => 'Registered User',
      'View'   => 'Registered User'
   );
   function getname() {
      return $this->derivename(array('Title','Photographer'));
   }
}   
?>