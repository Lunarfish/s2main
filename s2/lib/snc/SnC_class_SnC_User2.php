<?php
include_once('SnC_class_SnC_Session2.php');
/*
CREATE TABLE SnC_User (
   Snc_User_Key INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   UserName VARCHAR(100),
   SnCPassword VARCHAR(20),
   CreatedOn DATE NOT NULL 
);
*/
class SnC_User {
   private $table = 'ADM_User';
   private $ADM_User_Id;
   private $UserName;
   private $Email;
   private $Password;
   function setUserName($user) {
      $this->UserName = $user;
   }
   function getUserName() {
      return $this->UserName;
   }
   function setEmail($email) {
      $this->Email = $email;
   }
   function getEmail() {
      return $this->Email;
   }
   function setPassword($pass) {
      $this->Password = $pass;
   }
   function getPassword() {
      return $this->Password;
   }
   function login($session,$username,$password) {
      $this->UserName = $username;
      $this->retrieve();
      $status = (md5($password) == $this->Password);
   //print "$password ".md5($password)." $this->Password"; 
      if ($status) {
         $session = new SnC_Session($session);
         $status = $session->setUser($this->ADM_User_Id);         
      }
      return $this->ADM_User_Id;   
   }
   function logout($session) {
      $session = new SnC_Session($session);
      $status = $session->setUser(null);         
      return $status;   
   }
   function retrieve() {
      $dbh = $GLOBALS['dbh'];
      $query = $dbh->select();
      $query->from($this->table,
            array("ADM_User_Id"  => "ADM_User_Id",
                  "UserName"     => "UserName",
                  "Email"        => "Email",
                  "Password"     => "Password"));
      $query->where('UserName = ?',$this->UserName);
      $rows = $dbh->fetchAll($query);
      for ($i=0;$i<count($rows);$i++) {
         $record = $rows[$i];
         $this->ADM_User_Id   = $record["ADM_User_Id"];
         $this->UserName      = $record["UserName"];
         $this->Email         = $record["Email"];
         $this->Password      = $record["Password"];
      }      
   }
   function register($session,$username,$email,$password) {
      $dbh = $GLOBALS['dbh'];
      $data = array(
         'UserName'     => $username,
         'Email'        => $email,
         'Password'     => md5($password),
         'CreatedDate'  => new Zend_Db_Expr('CURRENT_DATE')
      );
      $status = $dbh->insert($this->table, $data);
      if ($status) {
         $this->ADM_User_Id = $dbh->lastInsertId();
         $session = new SnC_Session($session);
         $status = $session->setUser($this->ADM_User_Id);
         $status = $this->update($this->ADM_User_Id,"CreatedBy",$this->ADM_User_Id);         
      }
      return $this->ADM_User_Id;      
   }
   function update($userid,$field,$value) {
      $dbh = $GLOBALS['dbh'];
      $data = array($field => $value);
      return $dbh->update($this->table, $data, 'ADM_User_Id = '.$this->ADM_User_Id);               
   }
}
?>