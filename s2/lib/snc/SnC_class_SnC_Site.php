<?php
/*
CREATE TABLE SnC_System (
   Snc_System_Key INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   Snc_System_Name VACHAR(50) NOT NULL,
   Snc_Designation VARCHAR(10) NOT NULL   
)
*/
class SnC_Site {
   private $table = 'SnC_Site';
   // name and value are public properties used for options in an HTML select 
   // element
   // the designation is also made public so that it can be passed back encoded 
   // in JSON
   public $Name;
   public $Value;
   private $Snc_Site_Key;
   private $Snc_System_Key;
   public $Site_Name;
   public $Site_Code;
   public $Designation_Status;
   public $Management_Status;
   function SnC_Site($k=null,$s=null,$n=null,$c=null) {
      $this->Snc_Site_Key = $k;
      $this->Value = $this->getValue();
      $this->Site_Name = $n;
      $this->Site_Code = $c;
      $this->Name = $this->getName();
   }
   function getValue() {
      return $this->Snc_Site_Key;
   }
   function getName() {
      return "$this->Site_Name ($this->Site_Code)";
   }
   function insert() {
   
   }
   function update() {
   
   }
   function addSite() {
   
   }
   function getMeetingList() {
      
   }
   function addMeeting() {
   
   }
   function getSite($system=null,$key=null) {
      $s = null;
      $dbh = $GLOBALS['dbh'];
      
      $query = $dbh->select();
      $query->from($this->table,
            array("Snc_Site_Key"    => "Snc_Site_Key",
                  "Snc_System_Key"  => "Snc_System_Key",
                  "Site_Name"       => "Site_Name",
                  "Site_Code"       => "Site_Code"));
      if (isset($system)) $query->where("Snc_System_Key = ?",$system);
      if (isset($key)) $query->where("Snc_Site_Key = ?",$key);
      $rows = $dbh->fetchAll($query);
      for ($i=0;$i<count($rows);$i++) {
         $record = $rows[$i];
         $s = new SnC_Site($record["Snc_Site_Key"],
                           $record["Snc_System_Key"],
                           $record["Site_Name"],
                           $record["Site_Code"] );
      }
      return $s;
   }
   function getSites($system=null,$site=null,$code=null) {
      $sites = array();
      $dbh = $GLOBALS['dbh'];
      
      $query = $dbh->select();
      $query->from($this->table,
            array("Snc_Site_Key"    => "Snc_Site_Key",
                  "Snc_System_Key"  => "Snc_System_Key",
                  "Site_Name"       => "Site_Name",
                  "Site_Code"       => "Site_Code"));
      if (isset($system)) $query->where("SnC_System_Key = ?",$system);
      if (isset($site)) $query->where("Site_Name Like ?","%$site%");
      if (isset($code)) $query->where("Site_Code Like ?","%$code%");
      $rows = $dbh->fetchAll($query);
      for ($i=0;$i<count($rows);$i++) {
         $record = $rows[$i];
         $s = new SnC_Site($record["Snc_Site_Key"],
                           $record["Snc_System_Key"],
                           $record["Site_Name"],
                           $record["Site_Code"] );
         $sites[] = $s;
      }
      return $sites;
   }
   function getUserAccess($user,$system) {
      $available = $this->getUserSystems($user);
      $response = array();
      for ($i=0;$i<count($available);$i++) {
         $sys = $available[$i];
         if ($sys->Value == $system) {
            return $sys;
         } 
      }
      return false;
   }
}
?>