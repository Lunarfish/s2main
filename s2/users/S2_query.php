<?php
//error_reporting(E_ALL);
$paths = array(
   '../',
   '../lib/',
   '../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);

include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_DBT.php');       
include_once('gen/G_Users.php');
$users = true;

$itype = (isset($_REQUEST['IType']))?$_REQUEST['IType']:'USR_Users';
$current = (isset($_REQUEST['Current']))?$_REQUEST['Current']:null;
$settings = (isset($_REQUEST['CurrentSettings']))?json_decode($_REQUEST['CurrentSettings']):null;
if (isset($_REQUEST['Action'])) $action = $_REQUEST['Action'];   
else if ($_CURRENT_USER->is_anonymous) $action = 'GetForm';
else if ($_CURRENT_USER->can_list_users) $action = 'List';
else $action = 'View';         
if ($action == 'View' && !isset($current)) $current = $_CURRENT_USER->getid();

include_once('gen/G_query.php');
?>