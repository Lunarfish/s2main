<?php
error_reporting(E_ERROR);
set_time_limit(300);
ini_set('memory_limit','64M');
$paths = array(
   '../',
   '../lib/',
   '../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_ODS_v1.4.php');       
include_once('snc/S2_DBT.php');       
include_once('snc/S2_Files_v1.1.php');
include('htmlhead_reports.php');
?>
<body onload='s2_formatdatatables();s2_makecollapsible();s2_tooltipit();s2_enableexporter();completed();' class='landscape'>
<div id='main'>
<center>
<div id='report-page'>
<div class='report-liner'>
<div class='headerdiv'>
<img id='s2logo' src='http://data.yhedn.org.uk/s2/images/S2Georgia_40px.gif' alt='s2'/>
<img class='s2logo' src='http://data.yhedn.org.uk/s2/images/eroylogo.gif' alt='East Riding of Yorkshire District Council'/>
<h1 id='s2heading'>Second Site ... <span style='font-size: smaller;'>Reports</span></h1>
</div>
<?php
chdir('..');
$oid = intval($_REQUEST['o']);
$oc = $_REQUEST['t'];
$t = new $oc();
$t->loaddata($oid);
$tdata = $t->getdata();
$fid = $tdata['S2_File_ID'];
$fid = (is_object($fid))?$fid->Id:$fid; 
$f = new S2_File();
$f->loaddata($fid);
$fdata = $f->getdata();
$path = $fdata['Data'];
$html = s2_getfile($path);
echo $html;
?>
</div>
<div class='footerdiv'>
<hr style='clear:both;'/>

<div id='logo-div'>
<a target='yhedn' id='yhednlink' class='imagelink' href='http://www.yhedn.org.uk'>
<img id='yhednlogo' class='imagelink' src='http://data.yhedn.org.uk/s2/images/yhedn_with_text_onright_30px.jpg' alt='Built by YHEDN'/></a>
</div>

<p>This document has been produced as part of a pilot to trial a new screening 
tool developed to get good quality ecological data into the planning application 
validation process. The system is being trialled and is still under development.</p>
<p>Please refer any issues with the report content, formatting or function to 
XXXX at ZZZZZ.</p>
<p>Absence of data does not necessarily imply the absence of a species.</p>
<hr style='clear:both;'/>
</div>

</div>


<div id='screen'></div>

<div id='dialog-container' style='display:none;'>
<div id='dialog'>
<div class='navbar' onmousedown='follow(this.parentNode,event);' onmouseup='follow(this.parentNode,event);'>
<button class='close' onclick='hidedialog();'>&times;</button>
</div>
<div class='liner' id='dialog-liner'>
<h2 id='dialog-title'></h2>
</div>
</div>


</center>
</div>
</body>
</html>