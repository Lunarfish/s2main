<?php 
error_reporting(E_ERROR);
$paths = array(
   '../',
   '../lib/',
   '../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_DBT.php');       
include_once('snc/S2_Process.php');       
include_once('surveillance/SRV_Surveillance.php');

$itype = (isset($_REQUEST['IType']))?$_REQUEST['IType']:'SRV_Site';
$action = (isset($_REQUEST['Action']))?$_REQUEST['Action']:'List';
$current = (isset($_REQUEST['Current']))?$_REQUEST['Current']:null;
$settings = (isset($_REQUEST['CurrentSettings']))?json_decode($_REQUEST['CurrentSettings']):null;
//print_r(json_decode($_REQUEST['CurrentSettings']));

include_once('gen/G_query.php');
?>