<?php
error_reporting(E_ERROR);
set_time_limit(300);
ini_set('memory_limit','64M');
$paths = array(
   './',
   './lib/',
   './interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_DBT.php');       
include_once('snc/S2_Files_v1.1.php');
include_once('proj4php/proj4php.php');
$action = (isset($_REQUEST['a']))?$_REQUEST['a']:null;
switch($action) {
case 'echo': {
   $id = $_REQUEST['i'];
   $file = new S2_File();
   $file->loaddata($id);
   $data = $file->getdata();
   preg_match('/\.([^\.]+$)/',$data['Name'],$matches);
   $ext = $matches[1];
   if (!s2_is_image($ext)) header('Content-Disposition: attachment; filename="'.$data['Name'].'"');
   header('Content-Type: '.$data['Type']);
   echo s2_getfile($data['Data']);
}break;
case 'getcolumnheadings': {
   $id = $_REQUEST['i'];
   $file = new S2_File();
   $file->loaddata($id);
   $data = $file->getdata();
   $filecontents = s2_getfile($data['Data']);
   preg_match('/\.([^\.]+$)/',$data['Name'],$matches);
   $ext = strtolower($matches[1]);
   switch ($ext) {
   case 'csv': {
      $lines = preg_split('/\n/',$filecontents);
      $headline = $lines[0];
      if (preg_match('/\"/',$headline)) {
         $headline = preg_replace('/^\"/','',$headline);
         $headline = preg_replace('/\"\s*$/','',$headline);
         $headings = preg_split('/\"\s*,\s*\"/',$headline);
      } else {
         $headings = preg_split('/\s*,\s*/',$headline);
      }
      $resp = new stdClass();
      $resp->Headings = $headings;
      $resp->Type = 'csv';
      $resp->DataFile = $id;
      echo json_encode($resp);
   }break;      
   case 'xls': {
      $allow_url_override = 1; // Set to 0 to not allow changed VIA POST or GET
      if(!$allow_url_override || !isset($file_to_include)) $file_to_include = "LookupTables.xls";
      if(!$allow_url_override || !isset($max_rows)) $max_rows = 0; //USE 0 for no max
      if(!$allow_url_override || !isset($max_cols)) $max_cols = 0; //USE 0 for no max
      if(!$allow_url_override || !isset($debug)) $debug = 0;  //1 for on 0 for off
      require_once 'phpExcelReader/Excel/reader.php';
      $data = new Spreadsheet_Excel_Reader();
      $data->setOutputEncoding('CPa25a');
      $data->load($filecontents);
      $resp = new stdClass();
      $resp->Headings = array();
      $resp->Sheets = array();
      $resp->Type = 'xls';
      $resp->DataFile = $id;
      for($sheet=0;$sheet<count($data->sheets);$sheet++) {
         $sheetname = $data->boundsheets[$sheet]['name'];
         $resp->Sheets[] = $sheetname;
         $headings = $data->sheets[$sheet]['cells'][1];
         $resp->Headings[$sheetname] = $headings;
      } 
      echo json_encode($resp);
   }break;      
   case 'mif': {
      require_once 'spatial/spatialFunctions_v1_1.php';
      $lines = preg_split('/\n/',$filecontents);
      $resp = new stdClass();
      $resp->Headings = getMIFColumnHeadings($lines);
      array_unshift($resp->Headings,'polygon');
      $resp->Type = 'mif';
      $resp->MapFile = $id;
      $resp->DataFile = $_REQUEST['d'];
      echo json_encode($resp);         
   }break;      
   }
}break;
case 'getdata': {
   $id = $_REQUEST['i'];
   $file = new S2_File();
   $file->loaddata($id);
   $data = $file->getdata();
   $filecontents = s2_getfile($data['Data']);
   preg_match('/\.([^\.]+$)/',$data['Name'],$matches);
   $ext = strtolower($matches[1]);
   switch ($ext) {
   case 'csv': {
      $lines = preg_split('/\n/',$filecontents);
      $headline = array_shift($lines);
      if (preg_match('/\"/',$headline)) {
         $headline = preg_replace('/^\"/','',$headline);
         $headline = preg_replace('/\"\s*$/','',$headline);
         $headings = preg_split('/\"\s*,\s*\"/',$headline);
      } else {
         $headings = preg_split('/\s*,\s*/',$headline);
      }
      $resp = new stdClass();
      $resp->Headings = $headings;
      $resp->Type = 'csv';      
      $data = array();
      while (count($lines)>0) {
         $line = array_shift($lines);
         if (!preg_match('/^\s*$/',$line)) {
            $lineitems = parsecsvline($line);
            $linedata = array();
            foreach($headings as $i => $heading) {
               $linedata[$heading] = htmlentities($lineitems[$i]);
            }
            array_push($data,$linedata);    
         }               
      }
      $resp->Data = $data;
      $resp->DataFile = $id;
      echo json_encode($resp);
   }break;      
   case 'xls': {
      $tsheet = $_REQUEST['s'];
   
      $allow_url_override = 1; // Set to 0 to not allow changed VIA POST or GET
      if(!$allow_url_override || !isset($file_to_include)) $file_to_include = "LookupTables.xls";
      if(!$allow_url_override || !isset($max_rows)) $max_rows = 0; //USE 0 for no max
      if(!$allow_url_override || !isset($max_cols)) $max_cols = 0; //USE 0 for no max
      if(!$allow_url_override || !isset($debug)) $debug = 0;  //1 for on 0 for off
      require_once 'phpExcelReader/Excel/reader.php';
      $data = new Spreadsheet_Excel_Reader();
      $data->setOutputEncoding('CPa25a');
      $data->load($filecontents);
      $resp = new stdClass();
      $resp->Type = 'xls';
      $odata = array();
      for($sheet=0;$sheet<count($data->sheets);$sheet++) {
         $sheetname = $data->boundsheets[$sheet]['name'];
         if ($sheetname == $tsheet) {
            $headings = $data->sheets[$sheet]['cells'][1];
            $resp->Headings = $headings;
            for ($row=2;$row<=$data->sheets[$sheet]['numRows']&&($row<=$max_rows||$max_rows==0);$row++) {
	            $linedata = array();
               for ($col=1;$col<=$data->sheets[$sheet]['numCols']&&($col<=$max_cols||$max_cols==0);$col++) {
            	   $linedata[$headings[$col]] = htmlentities($data->sheets[$sheet]['cells'][$row][$col]);                   
	            }
	            array_push($odata,$linedata);
	         }
	      }
      }
      $resp->Data = $odata; 
      $resp->DataFile = $id;
      echo json_encode($resp);
   }break;      
   case 'mif': {
      require_once 'spatial/spatialFunctions_v1_1.php';
      $lines = preg_split('/\n/',$filecontents);
      $resp = new stdClass();
      $resp->Headings = getMIFColumnHeadings($lines);
      $ret = convertMIFRegionsToWKT($lines);
      $polygons = $ret->polygons;
      $resp->EPSG = $ret->epsg;
      $resp->Type = 'mif';
      $resp->MapFile = $id;
      $resp->DataFile = $_REQUEST['d'];
      $file = new S2_File();
      $file->loaddata($resp->DataFile);
      $data = $file->getdata();
      $dfilecontents = s2_getfile($data['Data']);
      $dlines = preg_split('/\n/',$dfilecontents);
      $odata = array();
      while(count($polygons)>0) {
         $poly = array_shift($polygons);
         if ($dbPlatform == 'PGSQL' && $ret->epsg != 4326) {
            $dbpg = SnC_getPGNativeConnection();
            $poly = preg_replace('/SRID=\d+\;/','',$poly);
            $expr = new Zend_Db_Expr("st_astext(st_transform(st_geomfromtext('$poly',$ret->epsg),4326))");
            $sel = "SELECT $expr as wgs_geom";
            $res = pg_query($dbpg,$sel);
            $res = pg_fetch_array($res);
            $poly = $res['wgs_geom'];
         }  
         $line = array_shift($dlines);
         $lineitems = parsecsvline($line);
//print "<p>$line</p>";
//print "<pre>"; print_r($lineitems); print "<pre>";
         $linedata = array();
         $linedata['polygon'] = $poly;
         foreach($resp->Headings as $i => $heading) {
            $linedata[$heading] = htmlentities($lineitems[$i]);
         }
//print "<pre>"; print_r($linedata); print "</pre>";
         array_push($odata,$linedata);
      }
//exit;
      array_unshift($resp->Headings,'polygon');
      $resp->Data = $odata;
      //echo json_encode($resp); 
      echo json_encode($resp, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);        
   }break;      
   }
}break;
case 'details': {
   $id = $_REQUEST['i'];
   $file = new S2_File();
   $file->loaddata($id);
   $data = $file->getdata();
   echo json_encode($data);      
}break;
case 'list': {
   $file = new S2_File();
   $files = $file->selectall();
   $f = array();
   foreach($files as $file) {
      $url = $file['Link'];
      $fname = $file['Name'];
      //$date = $file['CreatedOn'];
      $date = $file['Data'];
      $date = preg_replace('/^[^\/]+\//','',$date);
      $date = preg_replace('/\/[^\/]+$/','',$date);
       
      $y = substr($date,0,4);
      $m = substr($date,4,2);
      $d = substr($date,6,2);
      if (!isset($f[$y])) $f[$y] = array();
      if (!isset($f[$y][$m])) $f[$y][$m] = array();
      if (!isset($f[$y][$m][$d])) $f[$y][$m][$d] = array();
      //print "<li><a href='$url'>$y $m $d $fname</a></li>";
      $f[$y][$m][$d][] = $file;      
   }
   echo json_encode($f);             
}break;
default: {
   $file = new S2_File();
   if (!$file->exists()) $file->create();   
   if (isset($_GET['qqfile'])) {
      $obj = s2_inputfile_byget($file);
      $id = $file->getid();
      $name = $file->getname();
      $obj = s2_savefile($obj); 
      $file->setdata((array)$obj);
      $file->update($file->getpk(),$id);
      $url = $obj->Link;
      $obj = new stdClass();      
      $obj->success = true;
      $obj->url = $url;
      $obj->id = $id;
      $obj->data = $file->getdata();         
   } else if (isset($_FILES['qqfile'])) {
      $obj = s2_inputfile_byfiles($file);
      $id = $file->getid();
      $name = $file->getname();
      $obj = s2_savefile($obj);   
      $file->setdata((array)$obj);
      $file->update($file->getpk(),$id);
      $url = $obj->Link;
      $obj = new stdClass();      
      $obj->success = true;
      $obj->url = $url;
      $obj->id = $id;         
      $obj->data = $file->getdata();         
   } else {
      $obj = new stdClass();
      $obj->success = false;
   }
   echo json_encode($obj);
}break;
}
function parsecsvline($string) {
   $elements = array();
   if (preg_match("/\"/",$string)) {
      $bits = preg_split('/\"/',$string);
      for ($i=0;$i<count($bits);$i++) {
         $bit = $bits[$i];
         $bit = preg_replace("/^\s+$/","",$bit);
         if ($i%2==1) {
            $elements[] = $bit;
         } else if ($bit != ',' && strlen($bit)>0) {
            $s = $bit;
            $s = preg_replace("/^,/","",$s);
            $s = preg_replace("/,$/","",$s);
            $elements = array_merge($elements,split(",",$s));
         }
      }
   } else {
      $elements = split(",",$string);
   }
   return $elements;
}
?>                
