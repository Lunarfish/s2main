<?php
error_reporting(E_ALL);
set_time_limit(300);
ini_set('memory_limit','128M');
$paths = array(
   './',
   './lib/',
   './interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);

//print "<pre>"; print_r($_REQUEST); print "</pre>"; exit;

$kmlname = urldecode($_REQUEST['name']); 
$ext = strtolower(pathinfo($kmlname, PATHINFO_EXTENSION));
$kmlfile = urldecode($_REQUEST['data']);

switch ($ext) {
case 'kml': {
//print "iskml";
   $contents = file_get_contents($kmlfile);    
}break;
case 'kmz': {
//print "iskmz";
   $zip = new ZipArchive;
   if ($zip->open($kmlfile)) {
      $contents = '';
      $kmlfile = null;
      for( $i = 0; $i < $zip->numFiles; $i++ ){ 
         $zfil = $zip->statIndex( $i ); 
         if (preg_match('/kml$/',$zfil['name'])) $kmlinfile = $zfil['name']; 
      }
      if (isset($kmlinfile)) {
         $fp = $zip->getStream($kmlinfile);
         if(!$fp) exit("failed\n");
         while (!feof($fp)) {
            $contents .= fread($fp, 2);
         }
         fclose($fp);
      }
   }
}break;
}
$resp = new stdClass();
if (isset($contents)) {
//echo $contents; exit;
   try {
      $xml      = new SimpleXMLElement($contents);
      //$poly    = $xml->Document->Folder->Placemark->MultiGeometry->Polygon;
      list($path,$foundply) = findXMLElement($xml,'Polygon');
      if (!$foundply) 
         list($path,$foundpnt) = findXMLElement($xml,'Point');
      if (!($foundply||$foundpnt)) 
         list($path,$foundlin) = findXMLElement($xml,'LineString');
      
      if ($foundply) $geomtype = 'poly';
      else if ($foundpnt) $geomtype = 'point';
      else if ($foundlin) $geomtype = 'line';
      else $geomtype = null;
      if (isset($geomtype)) $feat = getXMLElementByPath($xml,$path);
      if (isset($feat)) {
         switch($geomtype) {
         case 'poly': {
            if (is_array($feat)) {
               foreach($feat as $ix => $px) {
                  $coords = getXMLPolygonCoords($px);
                  $ring = wktRingFromCoords($coords);
                  $rings[] = $ring;            
               }
               $wkt = 'MULTIPOLYGON(('.join('),(',$rings).'))';
            } else {
               $coords = getXMLPolygonCoords($feat);
               $ring = wktRingFromCoords($coords);
               $wkt = 'POLYGON(' . $ring. ')';
            }
         }break;
         case 'line': {
            $coords = getXMLCoords($feat->coordinates);
            $ring = wktRingFromCoords($coords);
            $wkt = 'LINESTRING' . $ring;
         }break;
         case 'point': {
            $coords = getXMLCoords($feat->coordinates);
            $wkt = 'POINT(' . $coords[0][0] . ' ' . $coords[0][1] . ')';              
         }break;
         }
         $resp->status = 1;
         $resp->message = "Read $ext file contents";
         $resp->data = $wkt;
      } else {
         $resp->status = 0;
         $resp->message = "No feature in file";
      }
   } catch (Exception $e) {
      $resp->status = 0;
      $resp->message = "Unable to decode $ext file contents";   
   }
} else {
   $resp->status = 0;
   $resp->message = "Unable to read $ext file contents";
}
echo json_encode($resp);


function wktRingFromCoords($coords) {
   $ring = '(';
   foreach ($coords as $coord) {
      $x = $coord[0]; $y = $coord[1];
      $ring .= "$x $y,";
   }
   $ring = rtrim($ring,',') . ')';
   return $ring;
}
function getXMLCoords($coordinateselement) {
   $value = (string)$coordinateselement;
   $values   = preg_split('/\s+/', trim($value));
   $coords   = array();
   foreach($values as $value) {    
      $args     = preg_split('/\,/', $value);
      $coords[] = array($args[0], $args[1]);
   }
   return $coords;
}
function getXMLPolygonCoords($poly) {
   $coords = getXMLCoords($poly->outerBoundaryIs->LinearRing->coordinates);
   return $coords;
}
function findXMLElement($xml,$elementname,$path=array()) {
   $found = false;
   foreach($xml as $prop => $inner) {
      if ($prop == $elementname) {
         $path[] = $elementname;
         $found = true;
         break;
      } else if (isset($inner) && is_object($inner)) {
         list($ipath,$found) = findXMLElement($inner,$elementname,$path);
         if ($found) {
            $path[] = $prop;
            $path = array_merge($path,$ipath); break; 
         }
      } 
   }
   return array($path,$found);
}
function getXMLElementByPath($xml,$path) {
   $el = $xml;
   while ($prop = array_shift($path)) {
      $el = ((array)$el);
      $el = $el[$prop];
   }
   return $el;
}

?>