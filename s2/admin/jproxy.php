<?php
error_reporting(E_ERROR);
/*
   jproxy.php uses an envelope similar to soap to redirect a request to 
   a different target. 
   
   the response is left unaltered and simply passed back to the requester 
   
   jproxy can't know about the encryption settings as these have to done in 
   JavaScript at the client end and only decoded when the request reaches the 
   encryption proxy. eproxy.php
   
   the jenv request contains the request to be forwarded as an object property
   and the target for where the request should be forwarded to.
   
*/  
//print $_REQUEST['request'];
$request = json_decode(stripslashes($_REQUEST['request']));
//print_r($request);
//exit;
//$url = $request->target;
$url = $_REQUEST['target'];
// Open the Curl session
$curl = curl_init($url);
if (isset($_POST)) {
   $post = (array)$request;
   curl_setopt ($curl, CURLOPT_POST, true);
	curl_setopt ($curl, CURLOPT_POSTFIELDS, $post);
}
// Don't return HTTP headers. Do return the contents of the call
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
// Make the call
$response = curl_exec($curl);
curl_close($curl);
header('Content-Type: text/plain');
echo $response;
?>