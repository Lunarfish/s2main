<?php
$paths = array(
   './',
   './lib/',
   './interfaces/'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include('settings.conf');
//require_once('nbn/nbntaxa.php');
require_once('nbn/nbntaxa_rest.php');

$query_type = (isset($_REQUEST['type']))?$_REQUEST['type']:null;
$value = $_REQUEST['searchfor'];
switch($query_type) {
case 'tvk': {
   $property = 'tax:TaxonVersionKey';   
}break;
default: {
   $property = 'SearchTerm';
   // allow for missing apostrophes eg Crested dogs tail instead of Crested dog's tail 
   $value = preg_replace('/s\s/','%s ',$value);
   // replace for spaces, asterisks, apostrophes and hyphens with wildcard characters 
   // eg Gold-finch will still match Goldfinch 
   $value = preg_replace('/[\s+,\',\-,\*]/','%',$value);
   // replace multiple wildcards with 1.
   $value = preg_replace('/%+/','%',$value);
}break;
}
$taxonlist = querynbntaxa($property,$value);
if (count($taxonlist)>0)  {
   $obj = new stdClass();
   $obj->Status = 1;
   $obj->Matches = $taxonlist;
   echo json_encode($obj);
} else {
   $obj = new stdClass();
   $obj->Status = 0;
   $obj->Message = "The search returned no matches for name: $value."; 
   echo json_encode($obj); 
}
?>