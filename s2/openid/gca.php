<?php
$paths = array(
   '../',
   '../lib/',
   '../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
require_once('gopenid.php');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_DBT.php');       
include_once('gen/G_Users.php');

$googleResponse = GoogleOpenID::getResponse();

$acc = new stdClass();
$acc->success = $googleResponse->success();//true or false
$acc->userid = $googleResponse->identity();//the user's ID
$acc->email = $googleResponse->email();//the user's email
if ($acc->success) {
   //setcookie('guser',$acc->email);
   
   $u = new USR_Users();
   $status = $u->googleauthenticate($acc->email);
      
}
$tgt = (isset($_COOKIE['target']))?$_COOKIE['target']:'../users';
header("Location: $tgt");
?>