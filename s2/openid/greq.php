<?php
require_once('gopenid.php');
$tgt = urldecode($_GET['tgt']);
setcookie('target',$tgt);

if (!isset($_COOKIE['gahand'])) {
   $handle = GoogleOpenID::getAssociationHandle(); // <--save this! valid for two weeks!
   $d14 = time()+60*60*24*14; 
   setcookie('gahand',$handle,$d14);
} else {
   $handle = $_COOKIE['gahand'];
}
$path = getcurrentpath().'/gca.php';
$googleGateway = GoogleOpenID::createRequest($path, $handle, true);
$googleGateway->redirect();

function getcurrentpath() {
   $path = $_SERVER['PHP_SELF'];
   $folders = preg_split('/\//',$path);
   array_pop($folders);
   $path = join('/',$folders);
   return $path;
}
?>