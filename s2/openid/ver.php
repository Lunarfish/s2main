<?php
$vars = array();
$vars['to'] = $this->username;
$vars['from'] = 'info@yhedn.org.uk';
$vars['subject'] = 'Yorkshire and Humber Environmental Data Network : Account activation details';
$message .= "<img src='http://www.yhedn.org.uk/images/yhedn_with_text_onright.jpg' alt='yhedn.org.uk'/>\n\n";
$message .= "<p>An account has been set up for this email address at <a href='http://www.yhedn.org.uk'>http://www.yhedn.org.uk</a>. </p>\n\n";
$message .= "<p>Please follow the link below to activate your account. Your activation code is <em>$verify</em> </p>\n\n";
$message .= "<p>If the automatic link below fails for any reason you can activate your account manually through the activation page. </p>\n\n";
$message .= "<p><em>automatic</em> <a href='http://www.yhedn.org.uk/activateAccount.php?username=" . $this->username . "&activation=" . $verify . "'>activate account</a> </p>\n\n";
$message .= "<p><em>manual</em> <a href='http://www.yhedn.org.uk/activation.php'>activation page</a> </p>\n\n";
$message .= "<p style='padding:10px;'>http://www.yhedn.org.uk/activation.php </p>\n\n";
$message .= "<p>If this has been done in error please reply to this message requesting that the account be deactivated. </p>\n\n";
$vars['message'] = $message;
$success = sendEmail($vars);


function sendEmail ($vars,$plain=null) {
    $to = $vars['to'];
    $from = $vars['from'];
    $from = ($from) ? $from : $vars['who:email'];
    /* To send HTML mail, you can set the Content-type header. */
    $headers  = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
    /* additional headers */
    $headers .= "From: $from\r\n";
    //$headers .= "Cc: \r\n";
    //$headers .= "Bcc: \r\n";
    $subject = stripslashes($vars['subject']);
    if ($plain != null) {
      $message = stripslashes($vars['message']);
    } else {
      $myFile = "./mail/mailformatting.css";
      $fh = fopen($myFile, 'r');
      $theData = fread($fh, filesize($myFile));
      fclose($fh);
      $message .= '<style type="text/css">';
      $message .= $theData;
      $message .= '</style>';
      $message .= '<div class="email">';
      $message .= '<div class="section-top">';
      $message .= '<div class="explanation">';
      $message .= '<table>';
      foreach ($vars as $key => $value) {
        if (!preg_match("/message|subject|^to|pageName|who:email|from/", $key)) {
            $message .= "<tr><th>$key</th>";
            $message .= "<td>$value</td></tr>\n";
//print "$key  :  $value <br />";
        }
      }
      $message .= '</table>';
      $message .= stripslashes($vars['message']);
      $message .= '</div></div></div>';
    }
    $return = mail($to, $subject, $message, $headers);
    /* 
        failure case - code 13
        "Send email failed"
    */
    if (!$return) {
        // do something
    }
    return true;
}

?>