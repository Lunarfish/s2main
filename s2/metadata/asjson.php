<?php
error_reporting(E_ERROR);
$paths = array(
   '../',
   '../lib/',
   '../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_DBT.php');       
include_once('gen/GEM_UKGemini.php');


$iurl = stripslashes($_SERVER['REQUEST_URI']);
$iscr = preg_replace('/\//','.',stripslashes($_SERVER['SCRIPT_NAME']));
$endpoint = preg_replace("/$iscr/","",$iurl);
$ep = preg_replace('/[\/]/','.',$endpoint);
$iscr = preg_replace("/$ep/","",$iurl);  

preg_match('/\/([^\.]+)/',$endpoint,$m);
$uuid = (isset($m[1])) ?$m[1] :null;

$class = 'GEM_Dataset';
$dbt = new $class();
   
switch($uuid) {
case null: {
   // without ID list catalogue
   $data = $dbt->selectAll();
   echo json_encode($data);
}break;
default: {
   // with an ID load data including child table data
   $dbt->loaddata($uuid,'UUID',false);
   echo $dbt->asjson();
}break;
}
?>