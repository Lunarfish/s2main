<?php 
error_reporting(E_ALL);
$paths = array(
   '../',
   '../lib/',
   '../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_DBT.php');       
include_once('gen/GEM_UKGemini.php');

$itype = (isset($_REQUEST['IType']))?$_REQUEST['IType']:'GEM_Dataset';
$action = (isset($_REQUEST['Action']))?$_REQUEST['Action']:'List';
$current = (isset($_REQUEST['Current']))?$_REQUEST['Current']:null;
$settings = (isset($_REQUEST['CurrentSettings']))?json_decode(stripslashes($_REQUEST['CurrentSettings'])):null;

include_once('gen/G_query.php');
?>