CREATE TABLE IF NOT EXISTS `cdm_client_domain_referral` (
  `cdm_domain_referral_id` int(10) unsigned NOT NULL,
  `domain_name` varchar(200) NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `database_name` varchar(50) NOT NULL,
  `root_class` varchar(50) NOT NULL,
  PRIMARY KEY (`cdm_domain_referral_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `cdm_client_session` (
  `cdm_session_id` int(10) unsigned DEFAULT NULL,
  `session` varchar(36) NOT NULL,
  `server` varchar(100) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `project_id` int(10) unsigned DEFAULT NULL,
  `access_level` int(10) unsigned DEFAULT NULL,
  `key1` varchar(36) DEFAULT NULL,
  `key2` varchar(36) DEFAULT NULL,
  `session_started` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `session_laccess` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`session`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `cdm_server_domain_referral` (
  `cdm_domain_referral_id` int(10) unsigned NOT NULL,
  `domain_name` varchar(200) NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `database_name` varchar(50) NOT NULL,
  `root_class` varchar(50) NOT NULL,
  PRIMARY KEY (`cdm_domain_referral_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `cdm_server_session` (
  `cdm_session_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session` varchar(36) NOT NULL,
  `client` varchar(100) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `project_id` int(10) unsigned DEFAULT NULL,
  `access_level` int(10) unsigned DEFAULT NULL,
  `key1` varchar(36) DEFAULT NULL,
  `key2` varchar(36) DEFAULT NULL,
  `session_started` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `session_laccess` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`cdm_session_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;



-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 05, 2012 at 02:17 PM
-- Server version: 5.1.37
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `snc2`
--

-- --------------------------------------------------------

--
-- Table structure for table `usr_default_permissions`
--

CREATE TABLE IF NOT EXISTS `usr_default_permissions` (
  `USR_Default_Permissions_Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Domain` varchar(100) DEFAULT NULL,
  `Label` varchar(100) DEFAULT NULL,
  `Description` text,
  `Value` tinyint(1) DEFAULT NULL,
  `Anon` tinyint(1) DEFAULT NULL,
  `CreatedBy` int(10) unsigned DEFAULT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`USR_Default_Permissions_Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `usr_default_permissions`
--

INSERT INTO `usr_default_permissions` (`USR_Default_Permissions_Id`, `Domain`, `Label`, `Description`, `Value`, `Anon`, `CreatedBy`, `CreatedOn`) VALUES
(1, 'users', 'list_users', 'View existing user accounts. Registered users can see a list of other users but non-logged in people can''t.', 1, 0, NULL, '2011-08-23 10:51:07'),
(2, 'users', 'edit_self', 'Edit personal info including password', 1, 0, NULL, '2011-02-21 16:42:48'),
(3, 'users', 'edit_users', 'Edit other users', 0, 0, NULL, '2011-02-21 16:42:48'),
(4, 'users', 'set_perm', 'Set permissions', 0, 0, NULL, '2011-02-21 16:42:48'),
(5, 'users', 'edit_perm', 'Edit permissions', 0, 0, NULL, '2011-02-21 16:42:48'),
(6, 'users', 'change_other_pass', 'Change others user password', 0, 0, NULL, '2011-02-21 16:42:48'),
(7, 'sites', 'System Administrator', 'Add and edit partnerships', 0, 0, NULL, '2011-02-21 16:42:48'),
(8, 'sites', 'Partnership Administrator', 'Add and edit partnership data', 0, 0, NULL, '2011-02-21 16:42:48'),
(9, 'sites', 'Partnership Member', 'View partnership data', 0, 0, NULL, '2011-02-21 16:42:48'),
(10, 'sites', 'Registered User', 'View unprotected data', 1, 0, NULL, '2011-02-21 16:42:48'),
(11, 'projects', 'System Administrator', 'Add and edit projects', 0, 0, NULL, '2011-03-24 13:57:10'),
(12, 'projects', 'Partnership Administrator', 'Add and edit project data', 0, 0, NULL, '2011-03-24 13:57:10'),
(13, 'projects', 'Partnership Member', 'View project data', 0, 0, NULL, '2011-03-24 13:57:10'),
(14, 'projects', 'Registered User', 'View unprotected data', 1, 0, NULL, '2011-03-24 13:57:10'),
(19, 'sites', 'View Recorder 6 Data', 'Ability to view species data from Recorder', 0, 0, 1, '2011-08-11 17:56:39'),
(20, 'species', 'Registered User', NULL, 1, 0, 1, '2011-08-15 13:51:31'),
(22, 'keysites', 'System Administrator', 'Add and edit projects', 0, 0, NULL, '2011-08-15 14:00:37'),
(23, 'keysites', 'Key Sites Administrator', 'Add and edit key site data', 0, 0, NULL, '2011-08-15 14:00:37'),
(24, 'keysites', 'Key Sites Member', 'View key site data', 0, 0, NULL, '2011-08-15 14:00:37'),
(25, 'keysites', 'Registered User', 'View unprotected data', 1, 0, NULL, '2011-08-15 14:00:37'),
(26, 'admin', 'Registered User', NULL, 1, 0, 1, '2011-08-19 10:10:22'),
(27, 'contacts', 'Contact Editor', 'Add and edit individuals, organisations and personal data.', 0, 0, 1, '2011-08-22 14:50:58'),
(28, 'contacts', 'Registered User', 'View organisations and non DPA data', 1, 0, 1, '2011-08-19 17:25:15'),
(29, 'contacts', 'DPA Access', 'View personal information', 0, 0, 1, '2011-08-22 14:51:07'),
(30, 'habitats', 'Habitat Administrator', 'Add and edit projects', 0, 0, NULL, '2011-10-28 15:51:54'),
(31, 'habitats', 'Registered User', 'View unprotected data', 1, 0, NULL, '2011-10-28 15:51:55'),
(32, 'habitats', 'Habitat Administrator', 'Add and edit projects', 0, 0, NULL, '2011-10-28 15:52:40'),
(33, 'habitats', 'Registered User', 'View unprotected data', 1, 0, NULL, '2011-10-28 15:52:40'),
(34, 'habitats', 'Habitat Administrator', 'Add and edit projects', 0, 0, NULL, '2011-10-28 15:53:06'),
(35, 'habitats', 'Registered User', 'View unprotected data', 1, 0, NULL, '2011-10-28 15:53:06'),
(45, 'ods', 'Search Sensitive Access', 'View original search data', 0, 0, NULL, '2012-01-05 09:38:39'),
(43, 'ods', 'Search Administrator', 'Administer search data', 0, 0, NULL, '2012-01-05 09:38:39'),
(44, 'ods', 'Search Full Resolution', 'View original search data', 0, 0, NULL, '2012-01-05 09:38:39'),
(46, 'ods', 'Search Run', 'Run searches based on existing profiles', 0, 0, NULL, '2012-01-05 09:38:39');

-- --------------------------------------------------------

--
-- Table structure for table `usr_login_tokens`
--

CREATE TABLE IF NOT EXISTS `usr_login_tokens` (
  `USR_Login_Tokens_Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `User_Id` int(10) unsigned DEFAULT NULL,
  `Token` varchar(100) DEFAULT NULL,
  `CreatedBy` int(10) unsigned DEFAULT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`USR_Login_Tokens_Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `usr_login_tokens`
--

INSERT INTO `usr_login_tokens` (`USR_Login_Tokens_Id`, `User_Id`, `Token`, `CreatedBy`, `CreatedOn`) VALUES
(1, 3, '1298377712::3', NULL, '2011-02-22 12:28:08');

-- --------------------------------------------------------

--
-- Table structure for table `usr_permissions`
--

CREATE TABLE IF NOT EXISTS `usr_permissions` (
  `USR_Permissions_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `User_Id` int(10) unsigned DEFAULT NULL,
  `Domain` varchar(100) DEFAULT NULL,
  `Label` varchar(100) DEFAULT NULL,
  `Description` text,
  `Value` tinyint(1) DEFAULT NULL,
  `CreatedBy` int(10) unsigned DEFAULT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`USR_Permissions_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=300 ;

--
-- Dumping data for table `usr_permissions`
--

INSERT INTO `usr_permissions` (`USR_Permissions_ID`, `User_Id`, `Domain`, `Label`, `Description`, `Value`, `CreatedBy`, `CreatedOn`) VALUES
(1, 1, 'users', 'change_other_pass', NULL, 1, NULL, '2011-02-21 16:42:48'),
(2, 1, 'users', 'edit_perm', NULL, 1, NULL, '2011-02-21 16:42:48'),
(3, 1, 'users', 'set_perm', NULL, 1, NULL, '2011-02-21 16:42:48'),
(4, 1, 'users', 'edit_users', NULL, 1, NULL, '2011-02-21 16:42:48'),
(5, 1, 'users', 'edit_self', NULL, 1, NULL, '2011-02-21 16:42:48'),
(6, 1, 'users', 'list_users', NULL, 1, NULL, '2011-02-21 16:42:48'),
(7, 1, 'sites', 'Registered User', NULL, 1, NULL, '2011-02-21 16:42:48'),
(8, 1, 'sites', 'Partnership Member', NULL, 1, NULL, '2011-02-21 16:42:48'),
(9, 1, 'sites', 'Partnership Administrator', NULL, 1, NULL, '2011-02-21 16:42:48'),
(10, 1, 'sites', 'System Administrator', NULL, 1, NULL, '2011-02-21 16:42:48'),
(11, 1, 'contacts', 'System Administrator', NULL, 1, NULL, '2011-02-21 16:42:48'),
(12, 1, 'contacts', 'DPA Access', NULL, 1, NULL, '2011-02-21 16:42:48'),
(13, 1, 'contacts', 'Contact Editor', NULL, 1, NULL, '2011-02-21 16:42:48'),
(14, 1, 'contacts', 'Registered User', NULL, 1, NULL, '2011-02-21 16:42:48'),
(15, 1, 'North Yorkshire SINC Panel', 'System Administrator', NULL, 1, NULL, '2011-02-21 16:42:48'),
(16, 1, 'North Yorkshire SINC Panel', 'Partnership Administrator', NULL, 1, NULL, '2011-02-21 16:42:48'),
(17, 1, 'North Yorkshire SINC Panel', 'Partnership Member', NULL, 1, NULL, '2011-02-21 16:42:48'),
(18, 1, 'North Yorkshire SINC Panel', 'Registered User', NULL, 1, NULL, '2011-02-21 16:42:48'),
(19, 1, 'North Yorkshire SINC Panel', 'View Recorder 6 Data', NULL, 1, NULL, '2011-02-21 16:42:48'),
(20, 2, 'users', 'list_users', NULL, 0, NULL, '2011-02-21 16:42:48'),
(21, 2, 'users', 'edit_self', NULL, 0, NULL, '2011-02-21 16:42:48'),
(22, 2, 'users', 'edit_users', NULL, 0, NULL, '2011-02-21 16:42:48'),
(23, 2, 'users', 'set_perm', NULL, 0, NULL, '2011-02-21 16:42:48'),
(24, 2, 'users', 'edit_perm', NULL, 0, NULL, '2011-02-21 16:42:48'),
(25, 2, 'North Yorkshire SINC Panel', 'System Administrator', NULL, 0, NULL, '2011-02-21 16:42:48'),
(26, 2, 'North Yorkshire SINC Panel', 'Partnership Administrator', NULL, 0, NULL, '2011-02-21 16:42:48'),
(27, 2, 'North Yorkshire SINC Panel', 'Partnership Member', NULL, 0, NULL, '2011-02-21 16:42:48'),
(28, 3, 'sites', 'Partnership Administrator', NULL, 0, NULL, '2011-02-22 12:28:08'),
(29, 3, 'sites', 'Partnership Member', NULL, 0, NULL, '2011-02-22 12:28:08'),
(30, 3, 'sites', 'Registered User', NULL, 0, NULL, '2011-02-22 12:28:08'),
(31, 3, 'sites', 'System Administrator', NULL, 0, NULL, '2011-02-22 12:28:08'),
(32, 3, 'users', 'change_other_pass', NULL, 0, NULL, '2011-02-22 12:28:08'),
(33, 3, 'users', 'edit_perm', NULL, 0, NULL, '2011-02-22 12:28:08'),
(34, 3, 'users', 'edit_self', NULL, 0, NULL, '2011-02-22 12:28:08'),
(35, 3, 'users', 'edit_users', NULL, 0, NULL, '2011-02-22 12:28:08'),
(36, 3, 'users', 'list_users', 'View existing user accounts. Registered users can see a list of other users but non-logged in people can''t.', 1, 1, '2011-11-10 15:36:15'),
(37, 3, 'users', 'set_perm', NULL, 0, NULL, '2011-02-22 12:28:08'),
(38, 1, 'projects', 'Registered User', 'NULL', 1, NULL, '2011-03-24 13:57:10'),
(39, 1, 'projects', 'Partnership Member', 'NULL', 1, NULL, '2011-03-24 13:57:10'),
(40, 1, 'projects', 'Partnership Administrator', 'NULL', 1, NULL, '2011-03-24 13:57:10'),
(41, 1, 'projects', 'System Administrator', 'NULL', 1, NULL, '2011-03-24 13:57:10'),
(46, 1, 'sites_1', 'System Administrator', 'Add and edit partnerships', 0, 1, '2011-03-28 09:37:22'),
(47, 1, 'sites_1', 'Partnership Administrator', NULL, 0, 1, '2011-03-28 09:35:28'),
(48, 1, 'sites_1', 'Partnership Member', NULL, 0, 1, '2011-03-28 09:35:28'),
(49, 1, 'sites_1', 'Registered User', NULL, 1, 1, '2011-03-28 09:35:28'),
(50, 3, 'sites_1', 'System Administrator', NULL, 0, 1, '2011-03-28 09:35:51'),
(51, 3, 'sites_1', 'Partnership Administrator', NULL, 0, 1, '2011-03-28 09:35:51'),
(52, 3, 'sites_1', 'Partnership Member', NULL, 0, 1, '2011-03-28 09:35:51'),
(53, 3, 'sites_1', 'Registered User', NULL, 1, 1, '2011-03-28 09:35:51'),
(54, 1, 'sites_4', 'System Administrator', NULL, 0, 1, '2011-08-01 10:05:06'),
(55, 1, 'sites_4', 'Partnership Administrator', NULL, 0, 1, '2011-08-01 10:05:06'),
(56, 1, 'sites_4', 'Partnership Member', NULL, 0, 1, '2011-08-01 10:05:06'),
(57, 1, 'sites_4', 'Registered User', NULL, 1, 1, '2011-08-01 10:05:06'),
(58, 1, 'sites_5', 'System Administrator', NULL, 0, 1, '2011-08-11 16:36:04'),
(59, 1, 'sites_5', 'Partnership Administrator', NULL, 0, 1, '2011-08-11 16:36:04'),
(60, 1, 'sites_5', 'Partnership Member', NULL, 0, 1, '2011-08-11 16:36:04'),
(61, 1, 'sites_5', 'Registered User', NULL, 1, 1, '2011-08-11 16:36:04'),
(62, 1, 'sites_3', 'System Administrator', NULL, 0, 1, '2011-08-11 17:48:42'),
(63, 1, 'sites_3', 'Partnership Administrator', NULL, 0, 1, '2011-08-11 17:48:42'),
(64, 1, 'sites_3', 'Partnership Member', NULL, 0, 1, '2011-08-11 17:48:42'),
(65, 1, 'sites_3', 'Registered User', NULL, 1, 1, '2011-08-11 17:48:42'),
(66, 1, 'sites', 'View Recorder 6 Data', NULL, 0, 1, '2011-08-11 17:56:41'),
(67, 1, 'sites_3', 'View Recorder 6 Data', 'Ability to view species data from Recorder', 1, 1, '2011-08-11 17:57:24'),
(68, 1, 'species', 'Registered User', NULL, 1, 1, '2011-08-15 13:53:07'),
(69, 1, 'keysites', 'Registered User', 'NULL', 1, NULL, '2011-08-15 14:00:37'),
(70, 1, 'keysites', 'Key Sites Member', 'NULL', 1, NULL, '2011-08-15 14:00:37'),
(71, 1, 'keysites', 'Key Sites Administrator', 'NULL', 1, NULL, '2011-08-15 14:00:37'),
(72, 1, 'keysites', 'System Administrator', 'NULL', 1, NULL, '2011-08-15 14:00:37'),
(98, 1, 'sites_5', 'View Recorder 6 Data', NULL, 0, 1, '2011-08-18 15:21:27'),
(219, 12, 'keysites', 'Key Sites Administrator', NULL, NULL, 1, '2011-08-18 21:38:29'),
(220, 12, 'keysites', 'Key Sites Member', NULL, NULL, NULL, '2011-08-18 21:38:29'),
(221, 12, 'keysites', 'Registered User', NULL, NULL, NULL, '2011-08-18 21:38:29'),
(222, 12, 'keysites', 'System Administrator', NULL, NULL, NULL, '2011-08-18 21:38:29'),
(223, 12, 'projects', 'Partnership Administrator', NULL, NULL, NULL, '2011-08-18 21:38:29'),
(253, 12, 'contacts', 'DPA Access', 'View personal information', 1, 1, '2011-08-22 15:58:33'),
(225, 12, 'projects', 'Partnership Member', NULL, NULL, NULL, '2011-08-18 21:38:29'),
(227, 12, 'projects', 'Registered User', NULL, NULL, NULL, '2011-08-18 21:38:29'),
(252, 12, 'contacts', 'Registered User', NULL, 1, 1, '2011-08-22 15:58:09'),
(229, 12, 'projects', 'System Administrator', NULL, NULL, NULL, '2011-08-18 21:38:29'),
(251, 12, 'contacts', 'Contact Editor', 'Add and edit individuals, organisations and personal data.', 1, 1, '2011-08-22 15:58:21'),
(231, 12, 'sites', 'Partnership Administrator', NULL, NULL, NULL, '2011-08-18 21:38:29'),
(232, 12, 'sites', 'Partnership Member', 'View partnership data', 1, 1, '2011-08-22 14:37:43'),
(233, 12, 'sites', 'Registered User', 'View unprotected data', 1, 1, '2011-08-18 21:40:39'),
(234, 12, 'sites', 'System Administrator', NULL, NULL, NULL, '2011-08-18 21:38:29'),
(235, 12, 'sites', 'View Recorder 6 Data', NULL, NULL, NULL, '2011-08-18 21:38:29'),
(236, 12, 'species', 'Registered User', NULL, NULL, NULL, '2011-08-18 21:38:29'),
(250, 12, 'admin', 'Registered User', NULL, 1, 1, '2011-08-22 15:58:09'),
(238, 12, 'users', 'change_other_pass', NULL, NULL, NULL, '2011-08-18 21:38:29'),
(239, 12, 'users', 'edit_perm', NULL, NULL, NULL, '2011-08-18 21:38:29'),
(240, 12, 'users', 'edit_self', NULL, NULL, NULL, '2011-08-18 21:38:29'),
(241, 12, 'users', 'edit_users', NULL, NULL, NULL, '2011-08-18 21:38:29'),
(242, 12, 'users', 'list_users', 'View existing user accounts', 1, 1, '2011-08-18 21:38:38'),
(243, 12, 'users', 'set_perm', NULL, NULL, NULL, '2011-08-18 21:38:29'),
(244, 12, 'sites_5', 'System Administrator', 'Add and edit partnerships', 1, 1, '2011-08-18 21:42:56'),
(245, 12, 'sites_5', 'Partnership Administrator', 'Add and edit partnership data', 1, 1, '2011-08-18 21:39:14'),
(246, 12, 'sites_5', 'Partnership Member', 'View partnership data', 1, 1, '2011-08-18 21:39:24'),
(247, 12, 'sites_5', 'Registered User', NULL, 1, 1, '2011-08-18 21:39:01'),
(248, 12, 'sites_5', 'View Recorder 6 Data', 'Ability to view species data from Recorder', 1, 1, '2011-08-18 21:39:33'),
(249, 1, 'admin', 'Registered User', NULL, 1, 1, '2011-08-19 10:10:25'),
(254, 3, 'sites_5', 'System Administrator', NULL, 0, 1, '2011-10-05 16:47:30'),
(255, 3, 'sites_5', 'Partnership Administrator', NULL, 0, 1, '2011-10-05 16:47:30'),
(256, 3, 'sites_5', 'Partnership Member', NULL, 0, 1, '2011-10-05 16:47:30'),
(257, 3, 'sites_5', 'Registered User', NULL, 1, 1, '2011-10-05 16:47:30'),
(258, 3, 'sites_5', 'View Recorder 6 Data', NULL, 0, 1, '2011-10-05 16:47:30'),
(259, 1, 'habitats', 'Registered User', 'NULL', 1, NULL, '2011-10-28 15:51:55'),
(260, 1, 'habitats', 'Habitat Administrator', 'NULL', 1, NULL, '2011-10-28 15:51:55'),
(261, 1, 'habitats', 'Registered User', 'NULL', 1, NULL, '2011-10-28 15:52:40'),
(262, 1, 'habitats', 'Habitat Administrator', 'NULL', 1, NULL, '2011-10-28 15:52:40'),
(263, 1, 'habitats', 'Registered User', 'NULL', 1, NULL, '2011-10-28 15:53:06'),
(264, 1, 'habitats', 'Habitat Administrator', 'NULL', 1, NULL, '2011-10-28 15:53:06'),
(265, 3, 'projects', 'System Administrator', NULL, 0, 1, '2011-11-10 15:36:05'),
(266, 3, 'projects', 'Partnership Administrator', NULL, 0, 1, '2011-11-10 15:36:05'),
(267, 3, 'projects', 'Partnership Member', NULL, 0, 1, '2011-11-10 15:36:05'),
(268, 3, 'projects', 'Registered User', NULL, 1, 1, '2011-11-10 15:36:05'),
(269, 3, 'sites', 'View Recorder 6 Data', NULL, 0, 1, '2011-11-10 15:36:05'),
(270, 3, 'species', 'Registered User', NULL, 1, 1, '2011-11-10 15:36:05'),
(271, 3, 'keysites', 'System Administrator', NULL, 0, 1, '2011-11-10 15:36:05'),
(272, 3, 'keysites', 'Key Sites Administrator', NULL, 0, 1, '2011-11-10 15:36:05'),
(273, 3, 'keysites', 'Key Sites Member', NULL, 0, 1, '2011-11-10 15:36:05'),
(274, 3, 'keysites', 'Registered User', NULL, 1, 1, '2011-11-10 15:36:05'),
(275, 3, 'admin', 'Registered User', NULL, 1, 1, '2011-11-10 15:36:05'),
(276, 3, 'contacts', 'Contact Editor', NULL, 0, 1, '2011-11-10 15:36:05'),
(277, 3, 'contacts', 'Registered User', NULL, 1, 1, '2011-11-10 15:36:05'),
(278, 3, 'contacts', 'DPA Access', NULL, 0, 1, '2011-11-10 15:36:05'),
(279, 3, 'habitats', 'Habitat Administrator', NULL, 0, 1, '2011-11-10 15:36:05'),
(280, 3, 'habitats', 'Registered User', NULL, 1, 1, '2011-11-10 15:36:05'),
(281, 3, 'habitats', 'Habitat Administrator', NULL, 0, 1, '2011-11-10 15:36:05'),
(282, 3, 'habitats', 'Registered User', NULL, 1, 1, '2011-11-10 15:36:05'),
(283, 3, 'habitats', 'Habitat Administrator', NULL, 0, 1, '2011-11-10 15:36:05'),
(284, 3, 'habitats', 'Registered User', NULL, 1, 1, '2011-11-10 15:36:05'),
(293, 1, 'ods', 'Search Sensitive Access', NULL, 1, NULL, '2012-01-05 09:38:39'),
(292, 1, 'ods', 'Search Full Resolution', NULL, 1, NULL, '2012-01-05 09:38:39'),
(294, 1, 'ods', 'Search Run', NULL, 1, NULL, '2012-01-05 09:38:39'),
(295, 1, 'ods', 'Search Administrator', NULL, 1, NULL, '2012-01-05 09:38:39');

-- --------------------------------------------------------

--
-- Table structure for table `usr_profiles`
--

CREATE TABLE IF NOT EXISTS `usr_profiles` (
  `userId` int(10) NOT NULL DEFAULT '0',
  `department` varchar(100) DEFAULT '',
  `firstName` varchar(50) DEFAULT '',
  `lastName` varchar(50) DEFAULT '',
  `title` varchar(35) DEFAULT '',
  `im_type` varchar(20) DEFAULT '',
  `im_userid` varchar(100) DEFAULT '',
  `phone1` varchar(20) DEFAULT NULL,
  `phone1Type` enum('home','mobile','pager','work') DEFAULT NULL,
  `phone2Type` enum('home','mobile','pager','work') DEFAULT NULL,
  `phone2` varchar(20) DEFAULT NULL,
  `phone3Type` enum('home','mobile','pager','work') DEFAULT NULL,
  `phone3` varchar(20) DEFAULT NULL,
  `lastModified` datetime DEFAULT NULL,
  `biography` text,
  `startDate` date DEFAULT NULL,
  `photoUrl` varchar(100) DEFAULT NULL,
  `photoWidth` int(10) DEFAULT NULL,
  `photoHeight` int(10) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=1;

--
-- Dumping data for table `usr_profiles`
--


-- --------------------------------------------------------

--
-- Table structure for table `usr_users`
--

CREATE TABLE IF NOT EXISTS `usr_users` (
  `User_Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) DEFAULT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `Google_Account` varchar(200) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `Status` varchar(100) DEFAULT NULL,
  `GStatus` varchar(100) DEFAULT NULL,
  `Seclev` int(11) DEFAULT NULL,
  `CreatedBy` int(10) unsigned DEFAULT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`User_Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `usr_users`
--

INSERT INTO `usr_users` (`User_Id`, `Name`, `Email`, `Google_Account`, `Password`, `Status`, `GStatus`, `Seclev`, `CreatedBy`, `CreatedOn`) VALUES
(1, 'Administrator', 'root@localhost', NULL, 'changeme', 'enabled', 'null', 0, 1, '2011-08-19 10:39:02'),
(2, 'Guest', 'guest', NULL, 'guest', 'enabled', NULL, 9000, NULL, '2011-02-21 16:42:47'),
(3, 'Dan Jones', 'dan.jones@humber-edc.org.uk', 'innumo@googlemail.com', 'rw010647', 'enabled', 'unconfirmed', 9000, NULL, '2011-08-19 11:22:42'),
(12, 'Carolyn Barber', 'carolyn.barber@rotherham.gov.uk', NULL, 'felix50', 'enabled', NULL, NULL, 1, '2011-08-18 21:38:29');
