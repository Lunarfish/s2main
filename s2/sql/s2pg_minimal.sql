CREATE TABLE CDM_Client_Domain_Referral (
  CDM_Domain_Referral_Id SERIAL PRIMARY KEY,
  Domain_Name VARCHAR(200) NOT NULL,
  Project_Id INT NOT NULL,
  Database_Name VARCHAR(50) NOT NULL,
  Root_Class VARCHAR(50) NOT NULL
);

CREATE TABLE CDM_Client_Session (
  Cdm_Session_Id SERIAL PRIMARY KEY,
  Session VARCHAR(36) NOT NULL,
  Server VARCHAR(100) DEFAULT NULL,
  Username VARCHAR(200) DEFAULT NULL,
  Project_Id INT DEFAULT NULL,
  Access_Level INT DEFAULT NULL,
  Key1 VARCHAR(36) DEFAULT NULL,
  Key2 VARCHAR(36) DEFAULT NULL,
  Session_Started TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  Session_Laccess TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE CDM_Server_Domain_Referral (
  CDM_Domain_Referral_Id SERIAL PRIMARY KEY,
  Domain_Name VARCHAR(200) NOT NULL,
  Project_Id INT NOT NULL,
  Database_Name VARCHAR(50) NOT NULL,
  Root_Class VARCHAR(50) NOT NULL
);

CREATE TABLE CDM_Server_Session (
  CDM_Session_Id SERIAL PRIMARY KEY,
  Session VARCHAR(36) NOT NULL,
  Client VARCHAR(100) DEFAULT NULL,
  Username VARCHAR(200) DEFAULT NULL,
  Project_Id INT DEFAULT NULL,
  Access_Level INT DEFAULT NULL,
  Key1 VARCHAR(36) DEFAULT NULL,
  Key2 VARCHAR(36) DEFAULT NULL,
  Session_Started TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  Session_Laccess TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);


CREATE TABLE USR_Default_Permissions (
  USR_Default_Permissions_Id SERIAL PRIMARY KEY,
  Domain VARCHAR(100) DEFAULT NULL,
  Label VARCHAR(100) DEFAULT NULL,
  Description TEXT,
  Value BOOLEAN DEFAULT NULL,
  Anon BOOLEAN DEFAULT NULL,
  CreatedBy INT DEFAULT NULL,
  CreatedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

--
-- Dumping data for table `usr_default_permissions`
--

INSERT INTO USR_Default_Permissions (Domain, Label, Description, Value, Anon, CreatedBy, CreatedOn) VALUES
('users', 'list_users', 'View existing user accounts. Registered users can see a list of other users but non-logged in people can''t.', TRUE, FALSE, NULL, '2011-08-23 10:51:07'),
('users', 'edit_self', 'Edit personal info including password', TRUE, FALSE, NULL, '2011-02-21 16:42:48'),
('users', 'edit_users', 'Edit other users', FALSE, FALSE, NULL, '2011-02-21 16:42:48'),
('users', 'set_perm', 'Set permissions', FALSE, FALSE, NULL, '2011-02-21 16:42:48'),
('users', 'edit_perm', 'Edit permissions', FALSE, FALSE, NULL, '2011-02-21 16:42:48'),
('users', 'change_other_pass', 'Change others user password', FALSE, FALSE, NULL, '2011-02-21 16:42:48');

CREATE TABLE USR_Login_Tokens (
  USR_Login_Tokens_Id SERIAL PRIMARY KEY,
  User_Id INT DEFAULT NULL,
  Token VARCHAR(100) DEFAULT NULL,
  CreatedBy INT DEFAULT NULL,
  CreatedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE USR_Permissions (
  USR_Permissions_ID SERIAL PRIMARY KEY,
  User_Id INT DEFAULT NULL,
  Domain VARCHAR(100) DEFAULT NULL,
  Label VARCHAR(100) DEFAULT NULL,
  Description TEXT,
  Value BOOLEAN DEFAULT NULL,
  CreatedBy INT DEFAULT NULL,
  CreatedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO USR_Permissions (User_Id, Domain, Label, Description, Value, CreatedBy, CreatedOn) VALUES
(1, 'users', 'change_other_pass', NULL, TRUE, NULL, '2011-02-21 16:42:48'),
(1, 'users', 'edit_perm', NULL, TRUE, NULL, '2011-02-21 16:42:48'),
(1, 'users', 'set_perm', NULL, TRUE, NULL, '2011-02-21 16:42:48'),
(1, 'users', 'edit_users', NULL, TRUE, NULL, '2011-02-21 16:42:48'),
(1, 'users', 'edit_self', NULL, TRUE, NULL, '2011-02-21 16:42:48'),
(1, 'users', 'list_users', NULL, TRUE, NULL, '2011-02-21 16:42:48');

CREATE TABLE USR_Users (
  User_Id SERIAL PRIMARY KEY,
  Name VARCHAR(100) DEFAULT NULL,
  Email VARCHAR(200) DEFAULT NULL,
  Google_Account VARCHAR(200) DEFAULT NULL,
  Password VARCHAR(100) DEFAULT NULL,
  Status VARCHAR(100) DEFAULT NULL,
  GStatus VARCHAR(100) DEFAULT NULL,
  Seclev INT DEFAULT NULL,
  CreatedBy INT DEFAULT NULL,
  CreatedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO USR_Users (Name, Email, Google_Account, Password, Status, GStatus, Seclev, CreatedBy, CreatedOn) VALUES
('Administrator', 'root@localhost', NULL, 'changeme', 'enabled', 'null', 0, 1, '2011-08-19 10:39:02'),
('Guest', 'guest', NULL, 'guest', 'enabled', NULL, 9000, NULL, '2011-02-21 16:42:47');