/*
   Non obfuscated code for generating keys from session ids.
*/
function KLib() {
   this.k1;
   this.k2;
   this.init = function(sid) {
      var salpha = sid.split('');
      var alphastring = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      var alpha = alphastring.split('');
      var numer = array_flip(alpha);
      var stotal = 0;
      var index;
      for(index=0;index<salpha.length;index++) {
         stotal += parseInt(numer[salpha[index]]);
      }
      var date_obj = new Date();
      var year = date_obj.getUTCFullYear();
      var month = date_obj.getUTCMonth()+1;
      month = (month<10)?new String('0'+month):month;
      var day = date_obj.getUTCDate(); 
      day = (day<10)?new String('0'+day):day;
      var dnum = Math.floor(parseInt(new String(year)+new String(month)+new String(day),10)/parseInt(new String(day)+new String(month),10))%alpha.length;
      alpha = alpha.slice(dnum,alpha.length).concat(alpha.slice(0,dnum));
      var pos1 = 0;
      var pos2 = 0;
      var inter;
      for (index=0;index<alpha.length;index++) {
         pos2 = (stotal*numer[alpha[index]])%alpha.length; 
         inter = alpha[pos2];
         alpha[pos2] = alpha[pos1];
         alpha[pos1] = inter;
         pos1 = (pos2+1)%alpha.length;            
      }
      numer = array_flip(alpha);
      dnum = dnum%salpha.length;
      salpha = salpha.slice(dnum,salpha.length).concat(salpha.slice(0,dnum));
      kli = 0;
      k1 = '';
      k2 = '';
      z01 = null;
      z02 = null;                   
      while(kli<8) {
         z1 = new Zri(numer[salpha[(kli*2)]],numer[salpha[((kli*2)+1)]]);
         if (z01) z1.add(z01);
         z2 = new Zri(numer[salpha[(salpha.length-((kli*2)+1))]],numer[salpha[(salpha.length-((kli*2)+2))]]);
         if (z02) z2.add(z02);
         k1 += alpha[(Math.floor(z1.size())%(alpha.length))];
         k2 += alpha[(Math.floor(z2.size())%(alpha.length))];
         var powr = ((numer[salpha[(kli*2)]]%3)+1);
         var oper = (numer[salpha[(kli*2+1)]]%2);
         if (z01) {
            switch(oper) {
            case 0: z01 = z1.add(z01);break;
            case 1: z01 = z1.sub(z01);break;
            }
         } else {
            switch(powr) {
            case 1: z01 = z1;break;
            case 2: z01 = z1.sqd();break;
            case 3: z01 = z1.cbd();break;
            }
         }
         powr = (((numer[salpha[(salpha.length-((kli*2)+1))]])%3)+1);
         oper = ((numer[salpha[(salpha.length-((kli*2)+2))]])%2);
         if (z02) {
            switch(oper) {
            case 0: z02 = z2.add(z02);break;
            case 1: z02 = z2.sub(z02);break;
            }
         } else {
            switch(powr) {
            case 1: z02 = z2;break;
            case 2: z02 = z2.sqd();break;
            case 3: z02 = z2.cbd();break;
            }
         }
         kli++;
      }
      this.k1 = k1;
      this.k2 = k2;
   };
   this.getkeys = function() {   
      jobj = new Object();
      jobj.k1 = this.k1;
      jobj.k2 = this.k2;
      return JSON.stringify(jobj);
   };      
}
function Zri(rind,iind) {
   this.rind = rind;
   this.iind = iind;
   this.get = function(part) {
      switch(part) {
      case 'r': return this.rind;break;
      case 'i': return this.iind;break;
      default: return new Array(this.rind,this.iind);break;
      }
   };
   this.sqd = function() {
      var rind = this.rind*1;
      this.rind = ((Math.pow(rind,2)) - (Math.pow(this.iind,2)));
      this.iind = (2 * rind * this.iind);
   };
   this.cbd = function() {
      var rind = this.rind*1;
      var iind = this.iind*1;
      this.rind = (Math.pow(rind,3) - (rind * Math.pow(iind,2)) - (2 * rind * Math.pow(iind,2)));
      this.iind = ((Math.pow(rind,2)*iind) - Math.pow(iind,3) + (2 * Math.pow(rind,2) * iind));     
   };
   this.add = function(z1) {
      this.rind = (this.rind*1 + z1.rind*1);
      this.iind = (this.iind*1 + z1.iind*1);  
   };
   this.sub = function(z1) {
      this.rind = (this.rind*1 - z1.rind*1);
      this.iind = (this.iind*1 - z1.iind*1);
   };
   this.mult = function(z1) {
      this.rind = (this.rind * z1.rind)-(this.iind * z1.iind);
      this.iind = (this.rind * z1.iind)+(this.iind * z1.rind);
   };
   this.dist = function(z1) {
      return Math.sqrt(Math.pow((this.rind*1 - z1.rind*1),2) + Math.pow((this.iind*1 - z1.iind*1),2));
   };
   this.size = function() {
      return Math.sqrt(Math.pow(this.rind*1,2)+Math.pow(this.iind*1,2));
   };
}

function array_flip(iarr) {
   var oarr = new Array();
   var index;
   for (index in iarr) {
      oarr[iarr[index]] = index;
   }
   return oarr;
}