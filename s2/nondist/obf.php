<?php
include('./G_imanipulate_obf.php'); 
$file = './G_imanipulator_d.js';
$save = '../lib/G_imanipulator_o.js';

$ihandle = fopen("${file}", "r");
$ohandle = fopen("${save}", "wb");
$ofilestring = '';  
while ($line = fgets($ihandle)) {
   // get rid of one line comments
   $line = preg_replace('/\/\/.*$/','',$line);
   $line = preg_replace('/\s*=\s*/','=',$line);
   $line = preg_replace('/\s+$/','',$line);
   $line = preg_replace('/^\s+/','',$line);
   $ofilestring .= $line; 
}
/* get rid of multi-line comments */
$ofilestring = preg_replace('/\/\*[^\*]*\*\//','',$ofilestring);
foreach($replaces as $replace => $with) {
   $ofilestring = preg_replace("/$replace/","$with",$ofilestring);
}
fwrite($ohandle,$ofilestring);
fclose($ihandle);
fclose($ohandle);
?>