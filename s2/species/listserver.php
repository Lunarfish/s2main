<?php
$paths = array(
   '../',
   '../lib/',
   '../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_DBT.php');       
include_once('gen/G_Species.php');
include_once('proj4php/proj4php.php');
include_once('spatial/coordinateFunctions.php');
include_once('spatial/spatialFunctions.php');

$itype = (isset($_REQUEST['IType']))?$_REQUEST['IType']:'G_Dataset';
$current = (isset($_REQUEST['Current']))?$_REQUEST['Current']:null;

$dbt = new $itype();
$sid = $dbt->getsid(); 
if (!isset($sid) && isset($_REQUEST['SID'])) $dbt->setsid($_REQUEST['SID']);
$dbt->loaddata($current);

/* 
   Get membership works slightly different for a taxa than it does for a species
   list, type or class. 
*/
switch ($itype) {
case 'G_Species': {
   $dbt_tx = new G_Taxonomy();
   $data = $dbt->getdata();
   $rank = $data['Rank'];
   switch($rank) {
   case 'kingdom':      $rankcol = 'Rank_Kingdom'; break;                
   case 'phylum':       $rankcol = 'Rank_Phylum_or_Division'; break;
   case 'division':     $rankcol = 'Rank_Phylum_or_Division'; break;
   case 'class':        $rankcol = 'Rank_'.ucwords($rank); break;                
   case 'order':        $rankcol = 'Rank_'.ucwords($rank); break;                
   case 'family':       $rankcol = 'Rank_'.ucwords($rank); break;                
   case 'genus':        $rankcol = 'Rank_'.ucwords($rank); break;                
   case 'species':      $rankcol = 'Rank_'.ucwords($rank); break;  
   }
   $taxalist = $dbt_tx->select($rankcol,$current);
   $listname = $data['Name'];
   $membership = array();
   $lists = array();
   $listnames = array();
   foreach ($taxalist as $tx => $taxa) {
      $gspecies = $taxa['Recorded_As'];
      $membership[$gspecies->Id] = array($listname);
      /*
      // this bit sticks all the higher taxonomy into the 
      // member of lists for the species
      foreach((array)$taxa as $col => $val) {
         if (isset($val->Name) && !in_array($listnames,$val->Name)) {
            $listnames[] = $val->Name;
            $lists[] = $val;
         } 
         if (isset($val->Name) && preg_match('/Rank\_/',$col)) {
            $membership[$gspecies->Id][] = $val->Name;
         }
      }
      */
   } 
   $list = new stdClass();
   $list->Name = $listname;
   $obj = new stdClass();
   $obj->Lists = array($list);
   $obj->Memberships = $membership;
   echo json_encode($obj);
   //print "<pre>"; print_r($obj); print "</pre>";   
}break;
default: {
   $dbt_lm = new G_List_Member();
   $obj = $dbt_lm->getmembership($itype,$current);
   echo json_encode($obj);
   //print "<pre>"; print_r($membership); print "</pre>";
}break;
}

?>