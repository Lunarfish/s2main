<?php
session_start();
//if (!isset($_COOKIE['PHPSESSID'])) header("Location: ".$_SERVER['PHP_SELF']);
set_time_limit(0);
ini_set('memory_limit','64M');
$paths = array(
   '../',
   '../lib/',
   '../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include('cdm/cdm_use_encryption_v1_1.php');
include_once('users/users.inc');
include('../htmlhead.php');
?>
<body onload='completed();' onunload='s2_savestorage();'>

<div id='main'>
<div class='userscontainer'>
<?php //include_once('users/fragments/usermenubar.php'); ?>
<?php include_once('G_usermenubar.php'); ?>
</div>
<?php 
if ($encrypt) {
   $imgurl = '../images/padlock_closed.gif';
   $imgalt = 'encrypted'; 
} else {
   $imgurl = '../images/padlock_open.gif';
   $imgalt = 'open'; 
}
?>
<button id='flip_encrypt' onclick='s2_flip_encryption(this.id);' class='flipencrypt'><img src='<?php print $imgurl; ?>' alt='<?php print $imgalt; ?>'/></button>
<center>
<div id='page'>
<div class='liner'>
<?php //print "<pre>";print_r(get_included_files());print "</pre>"; ?>
<div class='headerdiv'>
<img id='s2logo' src='../images/S2Georgia_40px.gif' alt='s2'/>
<!--<img id='s2logo' src='../images/s2_svgexp.gif' alt='s2'/>-->
<!--<img style='float:right;height:40px;margin:0 0 10px 0' src='../images/s2_v2-100px.gif' alt='s2'/>-->
<!--<img style='float:right;height:40px;margin:0 0 10px 0' src='images/2ndsite.png' alt='2nd site'/>-->
<h1 id='s2heading'>Second Site ... <span style='font-size: smaller;'>Species Records</span></h1>
</div>
<!--<h3 style='clear:both;margin-bottom:0;padding-bottom:0;'>Current settings</h3>
<hr style='clear:both;'/>
<div id='top-menus'></div>
-->
<hr style='clear:both;'/>
<div id='isettings'>
<h3>Species List</h3>
<textarea id='slist' style='width:100%;height:300px;'></textarea>
<button onclick="s2_cyclelookup('slist');">Lookup</button>
<button onclick="s2_cycleinsert('slist');">Import</button>
</div>
<div id='ioutput'>
</div>
<script type='text/javascript'>
var prefix = 'G';
var stable = 'G_Species';
var slists = 'G_Species_List';
var nodename;
var listmembers;
var lmx = 0
var lmc;
var listname;
var listid;
function s2_checkforknownlist(listname) {
   var req,par;
   req = new Object();
   req.target = './S2_query.php';
   req.sync = false;
   req.responder = s2_isknownlist;
   par = new Object();
   par.Action = 'List';
   par.IType = prefix+'_Species_List';
   par.Name = listname;
   par.CurrentSettings = {slists:{'SearchBy':'Name','SearchFor':listname}};
   req.request = par; 
   snc_send(req);   
}
function s2_isknownlist(json) {
   var jobj;
   var listnode,list,req,res,par,tvk;
   try {
      jobj = JSON.parse(json);
   } catch(err) {
      alert('Action Failed:\nServer Communication Error');
      completed();
   }
   if (jobj && jobj.ListData) {
      listid = jobj.ListData[0][prefix+'_Species_List_ID'];
      s2_deleteexistingmembers();
   } else {
      listid = 0;
      s2_createlist(listname);
   }
}
function s2_deleteexistingmembers() {
   var req,par;
   req = new Object();
   req.target = './S2_query.php';
   req.sync = false;
   par = new Object();
   par.Action = 'Delete';
   par.IType = prefix+'_List_Member';
   par.Column = prefix+'_Species_List_ID';
   par.Value = listid;
   var user = s2_getcookie('u_info');
   if (user) par.UInfo = user; 
   var sett = s2_getcurrent();
   par.CurrentSettings = sett;
   req.request = par;
   req.responder = s2_fromdelete;
   res = snc_send(req);
}
function s2_fromdelete(json) {
   completed();
   s2_setlistheading();
   s2_cyclelist(nodename);
} 
function s2_createlist(listname) {
   var req,par;
   req = new Object();
   req.target = './S2_query.php';
   req.sync = false;
   par = new Object();
   par.Action = 'Add';
   par.IType = prefix+'_Species_List';
   par.Name = listname;
   var user = s2_getcookie('u_info');
   if (user) par.UInfo = user; 
   var sett = s2_getcurrent();
   par.CurrentSettings = sett;
   req.request = par;
   req.responder = s2_reportlist;
   res = snc_send(req);         
}
function s2_setlistheading() {
   var text,h2,onode;
   onode = document.getElementById('ioutput');
   text = (listid > 0)
      ?'Importing: ' + listname + ' (' + listid + ')'
      :text = 'Import failed: Unable to create or update list entries';
   h2 = document.createElement('H2');
   h2.appendChild(document.createTextNode(text));
   onode.appendChild(h2);
}
function s2_reportlist(json) {
//alert(json);
   completed();
   var jobj;
   try {
      jobj = JSON.parse(json);
   } catch(err) {
      alert('Action Failed:\nServer Communication Error');
      completed();
   }
   if (jobj && jobj.Status) {
      s2_setlistheading();
      if (listid > 0) s2_cyclelist(nodename);
   }
}
function s2_createlistmember(latin,tvk,score,group) {
   var req,par,o;
   req = new Object();
   req.target = './S2_query.php';
   req.sync = false;
   par = new Object();
   par.Action = 'Add';
   par.IType = prefix+'_List_Member';
   o = new Object();
   o.Name = latin;
   o.Value = tvk;
   par.G_Species_ID = o;
   par.G_Species_List_ID = listid;
   par.Score = (score)?score:1;
   par.G_List_Scoring_Group_ID = (group)?group:0;
   var user = s2_getcookie('u_info');
   if (user) par.UInfo = user; 
   var sett = s2_getcurrent();
   par.CurrentSettings = sett;
   req.request = par;
   req.responder = s2_reportlistmember;
   res = snc_send(req);         
}
function s2_reportlistmember(json) {
   completed();
   var jobj,tbody,tr,td;
   tbody = document.getElementById('ilistmembers');
   try {
      jobj = JSON.parse(json);
   } catch(err) {
      alert('Action Failed:\nServer Communication Error');
      completed();
   }
   if (jobj && jobj.Status) {
      tr = document.createElement('TR');
      td = document.createElement('TD');
      td.appendChild(document.createTextNode(jobj.Data.G_Species_ID.Name));
      tr.appendChild(td);
      td = document.createElement('TD');
      td.appendChild(document.createTextNode('Imported'));
      tr.appendChild(td);
      tbody.appendChild(tr);
   }
}
function s2_createspecies(latin,tvk) {
   var req,par,o;
   req = new Object();
   req.target = './S2_query.php';
   req.sync = false;
   par = new Object();
   par.Action = 'Add';
   par.IType = prefix+'_Species';
   o = new Object();
   o.Name = latin;
   o.Value = tvk;
   par.Latin_Name = o;
   var user = s2_getcookie('u_info');
   if (user) par.UInfo = user; 
   var sett = s2_getcurrent();
   par.CurrentSettings = sett;
   req.request = par;
   req.responder = s2_reportspeciesadd;
   res = snc_send(req);         
}
function s2_reportspeciesadd(json) {
   completed();
   var jobj,tbody,tr,td;
   tbody = document.getElementById('ilistmembers');
   try {
      jobj = JSON.parse(json);
   } catch(err) {
      alert('Action Failed:\nServer Communication Error');
      completed();
   }
   if (jobj && jobj.Status) {
      tr = document.createElement('TR');
      td = document.createElement('TD');
      td.appendChild(document.createTextNode(jobj.Data.Latin_Name));
      tr.appendChild(td);
      td = document.createElement('TD');
      td.appendChild(document.createTextNode('Imported'));
      tr.appendChild(td);
      tbody.appendChild(tr);
   }
}

function s2_importlist(elid) {
   //document.getElementById('isettings').style.height = '20px';
   var listnode,list,req,res,par,tvk;
   nodename = elid;
   listnode = document.getElementById(elid + '-name');
   listname = listnode.value;
   s2_checkforknownlist(listname);
}
function s2_cycleinsert(elid) { 
   var listnode,list,req,res,par,tvk,table,thead,tbody,tr,th,td;
   listnode = document.getElementById(elid);
   list = listnode.value;
   listmembers = list.split(/(\s*)\n(\s*)/);
   //listnode.parentNode.removeChild(listnode);
   //alert(listmembers.length);
   table = document.createElement('TABLE');
   thead = document.createElement('THEAD');
   tbody = document.createElement('TBODY');
   tbody.id = 'ilistmembers';
   tr = document.createElement('TR');
   var c,col,cols,bits,lat;
   cols = ['Species','Imported'];
   for (c in cols) {
      col = cols[c];
      th = document.createElement('TH');
      th.appendChild(document.createTextNode(col));
      tr.appendChild(th);
   }
   thead.appendChild(tr);
   table.appendChild(thead);
   table.appendChild(tbody);
   document.getElementById('ioutput').appendChild(table);
   for (lmx=0;lmx<listmembers.length;lmx++) {
      lmc = listmembers[lmx];
      if (lmc && lmc.match(/\=/)) {
         bits = lmc.split(/\=/);
         tvk = bits.pop();
         lat = bits.join('=');
         //s2_createlistmember(lat,tvk);
         s2_createspecies(lat,tvk);
      }
   }
   completed();
}
function s2_cyclelookup(elid) { 
   var listnode,list,req,res,par,tvk,table,thead,tbody,tr,th,td;
   listnode = document.getElementById(elid);
   list = listnode.value;
   listmembers = list.split(/(\s*)\n(\s*)/);
   //listnode.parentNode.removeChild(listnode);
   //alert(listmembers.length);
   table = document.createElement('TABLE');
   thead = document.createElement('THEAD');
   tbody = document.createElement('TBODY');
   tbody.id = 'ilistmembers';
   tr = document.createElement('TR');
   var c,col,cols;
   cols = ['Species','Imported'];
   for (c in cols) {
      col = cols[c];
      th = document.createElement('TH');
      th.appendChild(document.createTextNode(col));
      tr.appendChild(th);
   }
   thead.appendChild(tr);
   table.appendChild(thead);
   table.appendChild(tbody);
   document.getElementById('ioutput').appendChild(table);
   for (lmx=0;lmx<listmembers.length;lmx++) {
      lmc = listmembers[lmx];
      if (lmc && !lmc.match(/\=/)) {
         req = new Object();
         req.target = '../nbntaxondict.php';
         req.sync = true;
         par = new Object();
         par.searchfor = lmc;
         req.request = par;
         res = snc_send(req);
         //alert(res);
         var tlist = JSON.parse(res);
         var taxa;
         if (tlist && tlist.Status) {
            for(var i in tlist.Matches) {
               taxa = tlist.Matches[i];
               if (taxa.Name == lmc) {
                  tvk = taxa.TaxonVersionKey;
                  listmembers[lmx] = lmc + '=' + tvk;
                  //s2_createlistmember(lmc,tvk);
               }
            }
         }
         list = listmembers.join('\n');
         //document.getElementById(listid).value = list;
         listnode.value = list;
      }
   }
   completed();
}
</script>


<!-- all content gets generated in here -->
<!--<div id='cookiediv'></div>-->
<div id='content'></div>
<?php //print "session id &gt;".session_id() . "&lt;";?>
<?php //print "<pre>";print_r($cdm);print "</pre>";?>
</div>
<div class='footerdiv'>
<hr style='clear:both;'/>
<div id='logo-div'>
<a target='yhedn' id='yhednlink' class='imagelink' href='http://www.yhedn.org.uk'>
<img id='yhednlogo' class='imagelink' src='../images/yhedn_with_text_onright_30px.jpg' alt='Built by YHEDN'/></a>
</div>

<p>Providing an audit trail for 2nd-tier wildlife and geological site partnerships.</p>
<hr style='clear:both;'/>
</div>
</div>
</div>

<div id='screen'></div>

<div id='dialog-container' style='display:none;'>
<div id='dialog'>
<div class='navbar' onmousedown='follow(this.parentNode,event);' onmouseup='follow(this.parentNode,event);'>
<button class='close' onclick='hidedialog();'>&times;</button>
</div>
<div class='liner' id='dialog-liner'>
<h2 id='dialog-title'></h2>
</div>
</div>
</center>

</div>
</body>
</html>