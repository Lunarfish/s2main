<?php
error_reporting(E_ERROR);
//ini_set('memory_limit','64M');
//ini_set('post_max_size','500M');
//set_time_limit(0);

$paths = array(
   '../',
   '../lib/',
   '../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_DBT.php');       
include_once('snc/S2_Sites_v1.4.php');
include_once('snc/S2_Files_v1.1.php');
include_once('custom/S2_Custom.php');

$itype = (isset($_REQUEST['IType']))?$_REQUEST['IType']:'S2_Authority';
$action = (isset($_REQUEST['Action']))?$_REQUEST['Action']:'List';
$current = (isset($_REQUEST['Current']))?$_REQUEST['Current']:null;
$settings = (isset($_REQUEST['CurrentSettings']))?json_decode($_REQUEST['CurrentSettings']):null;

include_once('gen/G_query.php');
?>
