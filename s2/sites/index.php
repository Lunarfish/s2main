<?php
session_start();
//if (!isset($_COOKIE['PHPSESSID'])) header("Location: ".$_SERVER['PHP_SELF']);
set_time_limit(0);
ini_set('memory_limit','64M');
$paths = array(
   '../',
   '../lib/',
   '../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include('cdm/cdm_use_encryption_v1_1.php');
include_once('users/users.inc');
include('../htmlhead.php');
?>
<body onload='s2_start();'>

<div id='main'>
<div class='userscontainer' onunload='s2_savestorage();'>
<?php include_once('G_usermenubar.php'); ?>
</div>
<?php 
if ($encrypt) {
   $imgurl = '../images/padlock_closed.gif';
   $imgalt = 'encrypted'; 
} else {
   $imgurl = '../images/padlock_open.gif';
   $imgalt = 'open'; 
}
?>
<button id='flip_encrypt' onclick='s2_flip_encryption(this.id);' class='flipencrypt'><img src='<?php print $imgurl; ?>' alt='<?php print $imgalt; ?>'/></button>
<center>
<div id='page'>
<div class='liner'>
<?php //print "<pre>";print_r(get_included_files());print "</pre>"; ?>
<div class='headerdiv'>
<img id='s2logo' src='../images/S2Georgia_40px.gif' alt='s2'/>
<!--<img id='s2logo' src='../images/s2_svgexp.gif' alt='s2'/>-->
<!--<img style='float:right;height:40px;margin:0 0 10px 0' src='../images/s2_v2-100px.gif' alt='s2'/>-->
<!--<img style='float:right;height:40px;margin:0 0 10px 0' src='images/2ndsite.png' alt='2nd site'/>-->
<h1 id='s2heading'>Second Site ... <span style='font-size: smaller;'>Local Sites</span></h1>
</div>
<!--<h3 style='clear:both;margin-bottom:0;padding-bottom:0;'>Current settings</h3>
<hr style='clear:both;'/>
<div id='top-menus'></div>
-->
<hr style='clear:both;'/>
<div id='incontentdialog'></div>
<div id='left-menus'></div>

<!-- all content gets generated in here -->
<!--<div id='cookiediv'></div>-->
<div id='content'></div>
<?php 
//print "session id &gt;".session_id() . "&lt;";
//print "<pre>";print_r($cdm);print "</pre>";
//exit;
?>
</div>
<div class='footerdiv'>
<hr style='clear:both;'/>
<div id='logo-div'>
<a target='yhedn' id='yhednlink' class='imagelink' href='http://www.yhedn.org.uk'>
<img id='yhednlogo' class='imagelink' src='../images/yhedn_with_text_onright_30px.jpg' alt='Built by YHEDN'/></a>
</div>

<p>Providing an audit trail for 2nd-tier wildlife and geological site partnerships.</p>
<hr style='clear:both;'/>
</div>
</div>
</div>

<div id='screen'></div>

<div id='dialog-container' style='display:none;'>
<div id='dialog'>
<div class='navbar' onmousedown='follow(this.parentNode,event);' onmouseup='follow(this.parentNode,event);'>
<button class='close' onclick='hidedialog();'>&times;</button>
</div>
<div class='liner' id='dialog-liner'>
<h2 id='dialog-title'></h2>
</div>
</div>
</center>

</div>
</body>
</html>
