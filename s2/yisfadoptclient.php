<?php
session_start();
error_reporting(E_ALL);

//print "<pre>"; print_r($_REQUEST); print "</pre>"; exit;

$url = 'http://localhost/s2dev.1.5/public/S2_query.php';

include_once('./lib/cdm/cdm_access_functions.php');
include_once('./lib/klib.php');

$e = (isset($_REQUEST['e']))?$_REQUEST['e']:null;
$s = (isset($_REQUEST['s']))?$_REQUEST['s']:null;

$a = (isset($_REQUEST['a']))?$_REQUEST['a']:null;
$i = (isset($_REQUEST['i']))?$_REQUEST['i']:null;
$decodes = enc_unravel($e,$s);

if ($decodes) {
   // forward request to S2_query
   switch($a) {
   case 'geteventsbyyear': {
      $y = (isset($_REQUEST['y']))?$_REQUEST['y']:null;
      $set = array();
      $sby = array();
      $sby = array('SearchBy' => 'From_Date','SearchFor' => $y);
      $set['PUB_Survey'] = $sby;
      // ONLY GET EVENTS OF TYPE=BioBlitz
      $set['PUB_Survey_Type'] = (object)array('IType'=>'PUB_Survey_Type','Current'=>1);
      $req = array();
      $req['Action'] = 'List';
      $req['IType'] = 'PUB_Survey';
      $req['CurrentSettings'] = $set;
      $req['NoPaging'] = 1;
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      $events = array();
      foreach($resp->ListData as $elid => $data) {
         $z = new stdClass();
         $z->id = $data->PUB_Survey_ID;
         $z->name = $data->Name;
         $z->date = $data->From_Date;
         $z->from = $data->From_Date;
         $z->to = $data->To_Date;
         $events[] = $z;
      }
      $resp = json_encode($events);
   } break;
   case 'getrecordingzonebyname': {
      $n = (isset($_REQUEST['n']))?$_REQUEST['n']:null;
      $set = array();
      $set['PUB_Survey'] = (object)array('IType'=>'PUB_Survey','Current'=>$i);
      $sby = array();
      $sby = array('SearchBy' => 'Name','SearchFor' => $n);
      $set['PUB_Recording_Zone'] = $sby;
      
      $req = array();
      $req['Action'] = 'List';
      $req['IType'] = 'PUB_Recording_Zone';
      $req['CurrentSettings'] = $set;
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      $zones = array();
      foreach($resp->ListData as $elid => $data) {
         $z = new stdClass();
         $z->id = $data->PUB_Recording_Zone_ID;
         $z->name = $data->Name;
         $z->grid = $data->Grid_Reference;
         $zones[] = $z;
      }
      $resp = (isset($zones[0]))?json_encode($zones[0]):0;
   } break;
   case 'getrecordingzone': {
      $g = (isset($_REQUEST['g']))?$_REQUEST['g']:null;
      $set = array();
      $set['PUB_Survey'] = (object)array('IType'=>'PUB_Survey','Current'=>$i);
      $sby = array();
      $sby = array('SearchBy' => 'Grid_Reference','SearchFor' => $g);
      $set['PUB_Recording_Zone'] = $sby;
      
      $req = array();
      $req['Action'] = 'List';
      $req['IType'] = 'PUB_Recording_Zone';
      $req['CurrentSettings'] = $set;
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      $zones = array();
      foreach($resp->ListData as $elid => $data) {
         $z = new stdClass();
         $z->id = $data->PUB_Recording_Zone_ID;
         $z->name = $data->Name;
         $z->grid = $data->Grid_Reference;
         $zones[] = $z;
      }
      $resp = (isset($zones[0]))?json_encode($zones[0]):0;
   } break;
   case 'getrecordingzones': {
      $set = array();
      $set['PUB_Survey'] = (object)array('IType'=>'PUB_Survey','Current'=>$i);
      
      $req = array();
      $req['Action'] = 'List';
      $req['IType'] = 'PUB_Recording_Zone';
      $req['CurrentSettings'] = $set;
      $req['NoPaging'] = 1;
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      $zones = array();
      foreach($resp->ListData as $elid => $data) {
         $z = new stdClass();
         $z->id = $data->PUB_Recording_Zone_ID;
         $z->name = $data->Name;
         $z->grid = $data->Grid_Reference;
         $zones[] = $z;
      }
      $resp = json_encode($zones);
   } break;
   case 'getcontributorbyname': {
      $n = (isset($_REQUEST['n']))?$_REQUEST['n']:null;
      $set = array();
      $set['PUB_Survey'] = (object)array('IType'=>'PUB_Survey','Current'=>$i);
      $sby = array();
      $sby = array('SearchBy' => 'Name','SearchFor' => $n);
      $set['PUB_Contributor'] = $sby;
      
      $req = array();
      $req['Action'] = 'List';
      $req['IType'] = 'PUB_Contributor';
      $req['CurrentSettings'] = $set;
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      $ctrbs = array();
      foreach($resp->ListData as $elid => $data) {
         $z = new stdClass();
         $z->id = $data->PUB_Contributor_ID;
         $z->name = $data->Name;
         $z->use_my_data = $data->Use_My_Data;
         $z->is_trusted_recorder = $data->Is_Trusted_Recorder;
         $ctrbs[] = $z;
      }
      $resp = (isset($ctrbs[0]))?json_encode($ctrbs[0]):0;
   } break;
   case 'addcontributor': {
      $c = (isset($_REQUEST['c']))?$_REQUEST['c']:null;
      $contributor = json_decode($c);
      $req = array();
      $req['Action'] = 'Add';
      $req['IType'] = 'PUB_Contributor';
      $req['PUB_Survey_ID'] = $i;
      $req['Name'] = $contributor->name;
      $req['Use_My_Data'] = $contributor->use_my_data;
      if($req['Use_My_Data']) {
         $req['Phone'] = $contributor->phone;
         $req['Email'] = $contributor->email;
      }
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      $resp = ($resp->Current)?$resp->Current:0;
   } break;
   case 'addrecords': {
      $r = (isset($_REQUEST['r']))?$_REQUEST['r']:null;
      $data = json_decode($r);
      $recs = count($data->Records);
      $req = array();
      $req['Action'] = 'Add';
      $req['IType'] = 'PUB_Species_Record';
      $req['PUB_Survey_ID'] = $i;
      $req['PUB_Contributor_ID'] = $data->Contributor;
      $req['PUB_Species_ID'] = $data->Records;                                                                 
      $req['PUB_Recording_Zone_ID'] = $data->Zone;
      $req['Location'] = $data->Location;
      $req['Grid_Reference'] = $data->Grid_Reference;
      $req['On_Map'] = $data->On_Map;
      $req['On_Date'] = $data->Date;
      $req['At_Time'] = $data->Time;
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      
      if (count($resp) > 0) {
         $obj = new stdClass();
         $obj->Status = 1;
         $obj->Records = count($resp);
         $obj->Message = 'Records saved';  
      }  else {
         $obj = new stdClass();
         $obj->Status = 0;
         $obj->Message = 'Save failed';  
      }
      $resp = json_encode($obj);
   } break;
   case 'getrecords':      {
   
   } break; 
   case 'getrecentspecies': {
      $set = array();
      $set['PUB_Survey'] = (object)array('IType'=>'PUB_Survey','Current'=>$i);
      $set['PUB_Species'] = (object)array('IType'=>'PUB_Species','Expand'=>1);
      $req = array();
      $req['Action'] = 'List';
      $req['IType'] = 'PUB_Species_First_Record';
      $req['CurrentSettings'] = $set;
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      $species = array();
      $returncount = 12;
      foreach($resp->ListData as $elid => $data) {
         $sid = $data->PUB_Species_ID; 
         if (count($species) < $returncount && !in_array($sid,$species)) $species[] = $sid;
      }
      $resp = json_encode($species);
   } break;   
   case 'getspecieslist': {
      $set = array();
      $set['PUB_Survey'] = (object)array('IType'=>'PUB_Survey','Current'=>$i);
      $set['PUB_Species'] = (object)array('IType'=>'PUB_Species','Expand'=>1);
      $req = array();
      $req['Action'] = 'List';
      $req['NoPaging'] = 1;
      $req['IType'] = 'PUB_Species_First_Record';
      $req['CurrentSettings'] = $set;
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      $species1 = array();$tvks = array();
      foreach($resp->ListData as $elid => $data) {
         $sid = $data->PUB_Species_ID; 
         if (!in_array($sid,$species1)) {
			   if (is_object($sid) && isset($sid->TVK)) $tvks[] = $sid->TVK;
			   $species1[] = $sid;
		   }		 
      }
	   //$species2 = $species1;
	  
	   $req = array('s' => json_encode($tvks));
	   $url2 = "http://ybb.yhedn.org.uk/taxonomy/";
      $json = s2refer($url2,$req);
      $resp = json_decode($json);
	   $species2 = array();
	   foreach($resp as $i => $tax) {
         foreach($species1 as $j => $sp1) {
		      if ($tax->TVK == $sp1->TVK) {
               if (!$sp1->Common_Name) $sp1->Common_Name = $tax->COMMON;
			      $species2[] = $sp1;
               $species1[$j] = null; 
            }
		   }
      }
      foreach($species1 as $j => $sp1) {
         if ($sp1 != null) {
            $sp1->Unsorted = true;
            $species2[] = $sp1;
         }
      }
	   $resp = json_encode($species2);
   } break;      
   case 'getspecieshistory': {
      $set = array();
      $set['PUB_Survey'] = (object)array('IType'=>'PUB_Survey','Current'=>$i);
      $set['PUB_Species_By_Hour'] = (object)array('IType'=>'PUB_Species_By_Hour','NoPaging'=>1);
      $req = array();
      $req['Action'] = 'List';
      $req['IType'] = 'PUB_Species_By_Hour';
      $req['CurrentSettings'] = $set;
      $req['NoPaging'] = 1;
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      $zones = array();
      foreach($resp->ListData as $elid => $data) {
         $z = new stdClass();
         $z->date = $data->On_Date;
         $z->hour = $data->At_Hour;
         $z->species = $data->Species;
         $zones[] = $z;
      }
      $resp = json_encode($zones);
   } break;         
   case 'getrecordshistory': {
      $set = array();
      $set['PUB_Survey'] = (object)array('IType'=>'PUB_Survey','Current'=>$i);
      $set['PUB_Records_By_Hour'] = (object)array('IType'=>'PUB_Records_By_Hour','NoPaging'=>1);
      $req = array();
      $req['Action'] = 'List';
      $req['IType'] = 'PUB_Records_By_Hour';
      $req['CurrentSettings'] = $set;
      $req['NoPaging'] = 1;
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      $zones = array();
      foreach($resp->ListData as $elid => $data) {
         $z = new stdClass();
         $z->date = $data->On_Date;
         $z->hour = $data->At_Hour;
         $z->records = $data->Records;
         $zones[] = $z;
      }
      $resp = json_encode($zones);
   } break;
   case 'getstats': {
      $ranks = array(
         'Kingdom'   => 'Kingdoms',
         'Phylum'    => 'Phyla',
         'Class'     => 'Classes',
         'Order'     => 'Orders',
         'Family'    => 'Families',
         'Genus'     => 'Genera',
         'Species'   => 'Species'
      );
      $rank = (isset($_REQUEST['r']))?$_REQUEST['r']:null;
      $taxa = (isset($_REQUEST['t']))?$_REQUEST['t']:null;
      $is_next = false;
      $subrank = null;
      foreach ($ranks as $rs => $rp) { 
         if ($is_next) { 
            $subranksing = $rs;
            $subrankplur = $rp; 
            $is_next = false; 
         }
         if ($rank == $rs) {
            $ranksing = $rs;
            $rankplur = $rp;
            $is_next = true;
         }
      }
      $req = array();
      $req['Action'] = 'List';
      $set = array();
      $set['PUB_Survey'] = (object)array('IType'=>'PUB_Survey','Current'=>$i); 
      if (isset($taxa) && $taxa != null) {
         $table = "PUB_Recorded_${subranksing}";
         $set[$table] = array('IType'=>$table,'SearchBy'=>$rank,'SearchFor'=>$taxa,'NoPaging'=>1);  
      } else { 
         $table = "PUB_Recorded_${ranksing}"; 
         $set[$table] = array('IType'=>$table,'NoPaging'=>1);  
      }
      $req['IType'] = $table;
      $req['CurrentSettings'] = $set;
      $req['NoPaging'] = 1;
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      $resp = json_encode($resp->ListData);
   } break;
   case 'getspecies':      {
   
   } break;
   case 'getbingoforms':   {
      $set = array();
      $set['PUB_Survey'] = (object)array('IType'=>'PUB_Survey','Current'=>$i);
      $req = array();
      $req['Action'] = 'List';
      $req['IType'] = 'PUB_Bingo_Form';
      $req['CurrentSettings'] = $set;
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      $forms = array();
      foreach($resp->ListData as $elid => $data) {
         $z = new stdClass();
         $z->id = $data->PUB_Bingo_Form_ID;
         $z->name = $data->Name;
         $forms[] = $z;
      }
      $resp = json_encode($forms);
   } break;
   case 'getbingospecies': {
      $f = (isset($_REQUEST['f']))?$_REQUEST['f']:null;
      $set = array();
      $set['PUB_Survey'] = (object)array('IType'=>'PUB_Survey','Current'=>$i);
      $set['PUB_Bingo_Form'] = (object)array('IType'=>'PUB_Bingo_Form','Current'=>$f);
      $set['PUB_Species'] = (object)array('IType'=>'PUB_Species','Expand'=>1);
      
      $req = array();
      $req['Action'] = 'List';
      $req['IType'] = 'PUB_Bingo_Species';
      $req['CurrentSettings'] = $set;
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      $species = array();
      foreach($resp->ListData as $elid => $data) {$species[] = $data->PUB_Species_ID;}
      $resp = json_encode($species);
   } break;
   case 'addbingorecord':  {
      $b = (isset($_REQUEST['b']))?$_REQUEST['b']:null;
      $c = (isset($_REQUEST['c']))?$_REQUEST['c']:null;
      $d = (isset($_REQUEST['d']))?$_REQUEST['d']:null;
      $data = json_decode($r);
      $req = array();
      $req['Action'] = 'Add';
      $req['IType'] = 'PUB_Bingo_Record';
      $req['PUB_Survey_ID'] = $i;
      $req['PUB_Bingo_Form_ID'] = $b;
      $req['PUB_Contributor_ID'] = $c;
      $req['On_Date'] = $d;
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      if (count($resp) > 0) {
         $obj = new stdClass();
         $obj->Status = 1;
         $obj->Records = count($resp);
         $obj->Message = 'Records saved';  
      }  else {
         $obj = new stdClass();
         $obj->Status = 0;
         $obj->Message = 'Save failed';  
      }
      $resp = json_encode($obj);
   } break;
   default: {
      $req = array();
      $req['Action'] = 'List';
      $req['IType'] = 'PUB_Survey';
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      $events = array();
      foreach($resp->ListData as $elid => $data) {
         $e = new stdClass();
         $e->id = $data->PUB_Survey_ID;
         $e->name = $data->Name;
         $events[] = $e; 
      }
//print "<pre>"; print_r($events); print "</pre>";
      $resp = json_encode($events);              
   }break;
   case 'getcontributorstats': {
      $set = array();
      $set['PUB_Survey'] = (object)array('IType'=>'PUB_Survey','Current'=>$i);
      $req = array();
      $req['Action'] = 'List';
      $req['IType'] = 'PUB_Contributor_Species';
      $req['CurrentSettings'] = $set;
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      $ctrbs = array();
      foreach($resp->ListData as $elid => $data) {
         $z = new stdClass();
         $z->contributor   = $data->PUB_Contributor_ID->Name;
         $z->species       = $data->Species;
         $z->records       = $data->Records;
         $ctrbs[] = $z;
      }
      $resp = json_encode($ctrbs);
   } break;
   case 'getzonestats': {
      $set = array();
      $set['PUB_Survey'] = (object)array('IType'=>'PUB_Survey','Current'=>$i);
      $req = array();
      $req['Action'] = 'List';
      $req['IType'] = 'PUB_Zone_Species';
      $req['CurrentSettings'] = $set;
      $json = s2refer($url,$req);
      $resp = json_decode($json);
      $zones = array();
      foreach($resp->ListData as $elid => $data) {
         $z = new stdClass();
         $z->zone   = $data->PUB_Recording_Zone_ID->Name;
         $z->species       = $data->Species;
         $z->records       = $data->Records;
         $zones[] = $z;
      }
      $resp = json_encode($zones);
   } break;
   }
   echo $resp;
}
                               
function s2refer($url,$request) {
   $curl = curl_init($url);
   if (isset($_POST)) {
      $post = (array)$request;
      foreach ($post as $key => $val)  {
         switch (gettype($val)) {
         case 'object': $post[$key] = json_encode($val);break;
         case 'array': $post[$key] = json_encode($val);break;
         }
      }
      curl_setopt ($curl, CURLOPT_POST, true);
   	curl_setopt ($curl, CURLOPT_POSTFIELDS, $post);
   }
   // Don't return HTTP headers. Do return the contents of the call
   curl_setopt($curl, CURLOPT_HEADER, false);
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
   // Make the call
   $response = curl_exec($curl);
   curl_close($curl);
   return $response;
}

function enc_unravel($k3,$s=null) {
   if (!isset($s)) $s = session_id();
   $k = new KLib($s);
   $ks = json_decode($k->getkeys());
   $i = 0;$k1='';$k2='';
   while($i < strlen($k3)) {
      $k1 .= substr($k3,($i*3),1);
      $k2 .= substr($k3,(($i*3)+1),2);
      $i++; 
   }
   $k2d = decrypt($k2,$ks->k1,$ks->k2);
   return ($k1 == $k2d);
}
?>