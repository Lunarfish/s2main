<?php
$server = $_SERVER['PHP_SELF'];
$serverpaths = preg_split('/\//',$server);
array_pop($serverpaths);
$thisdomain = $_SERVER['SERVER_NAME'].join('/',$serverpaths); 
$action = $_REQUEST['a'];
error_reporting(E_ALL);
$paths = array(
   './',
   './lib/',
   './interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('Zend/Db.php');
include('cdm/CDMClient_class2.php');
switch($action) {
case 'handshake': {
   $session = $_REQUEST['s'];
   $domain = $_REQUEST['d'];
   $cdm_session_id = $_REQUEST['c'];
   $cdm = new CDMClient();
   $cdm->setServer($domain);
   $cdm->populate($session);
   $cdm->update('cdm_session_id',$cdm_session_id);
   $cdm->update('session_laccess',new Zend_Db_Expr("NOW()"));
   $k1 = $cdm->getKey(1);
   $k2 = $cdm->getKey(2);
   $json = array();
   $json['username'] = $cdm->getUsername();
   $encoded = json_encode($json);
   $encrypted = $cdm->encrypt($encoded);
   $json = array();
   $json['s'] = $session;
   $json['k'] = $k2;
   $json['e'] = $encrypted;
   echo json_encode($json);
}break;
case 'checkin': {
   $session = $_REQUEST['s'];
   $domain = $thisdomain;
   $cdm = new CDMClient();
   $cdm->setServer($domain);
   $cdm->populate($session);
   $cdm->checkin();
   $cdm->setKey($_REQUEST['k'],1);
   $k2 = $cdm->getKey(2);
   $json = array();
   $json['k'] = $k2;
   echo json_encode($json);
}break;
}
?>
