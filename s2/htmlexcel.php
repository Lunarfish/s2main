<?php
session_start();
/*
   The idea of this file is to take HTML as an input and to create an Excel file 
   where each tab in Excel represents a table from the Excel file.
*/
error_reporting(E_ERROR);
set_time_limit(300);
ini_set('include_path', ini_get('include_path').';../Classes/');
ini_set('memory_limit','64M');
$paths = array(
   './',
   './lib/',
   './interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('PHPExcel.php');
include_once('PHPExcel/Writer/Excel2007.php');

$tables = (isset($_REQUEST['data']))?json_decode(stripslashes($_REQUEST['data'])):null;
//print "<pre>"; print_r($tables); print "</pre>"; exit;

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Set properties
/*
$objPHPExcel->getProperties()->setCreator($enteredby);
$objPHPExcel->getProperties()->setLastModifiedBy($enteredby);
$objPHPExcel->getProperties()->setTitle("$unit $date");
$objPHPExcel->getProperties()->setSubject("YDNPA Species Data");
$objPHPExcel->getProperties()->setDescription("YDNPA Species Data for $unit on $date");
*/

foreach($tables as $t => $tdata) {
   $snum = $t+1;
   if ($t > 0) $objPHPExcel->createSheet($t);
   $objPHPExcel->setActiveSheetIndex($t);   
   $title = (isset($tdata->title))?$tdata->title:"Sheet $snum";
   $objPHPExcel->getActiveSheet()->setTitle($title);
   foreach($tdata->rows as $y => $rdata) {
      $cy = $y + 1;
      foreach($rdata as $x => $cdata) {
         $cx = $x + 1;
         $cellref = colnamefromindex($cx) . $cy;
         if ($cdata->t == 'h') {
             $style_header = array('font' => array('bold' => true),'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'CCC')));
             $objPHPExcel->getActiveSheet()->getStyle($cellref)->applyFromArray( $style_header );
         }
         if (isset($cdata->v)) $objPHPExcel->getActiveSheet()->SetCellValue($cellref, $cdata->v);         
      }
   }

}
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$sid = session_id();
$filepath = "temp/$sid.xlsx"; 
$objWriter->save($filepath);

$filedata = ex_getfile($filepath);
$mimetype = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';//'application/vnd.ms-excel';
ex_echofile($mimetype,$filedata);
ex_delfile($filepath);

function colnamefromindex($x) {
   $x -= 1;
   $n = chr(($x%26) + 65);
   if ($x >= 26) {
      $x1 = floor($x/26) -1; 
      $n1 = chr(($x1%26) + 65);
      $n = $n1.$n;
      if ($x >= (27*26)) {
         $x1 = floor($x/(27*26)) -1;
         $n1 = chr(($x1%26) + 65);
         $n = $n1.$n;  
      }
   }
   return $n;
}
function ex_echofile($type,$data) {
   header('Content-Disposition: attachment; filename="download.xlsx"');
   header('Content-Type: '.$type);
   echo $data; 
}
function ex_getfile($path) {
   $data = file_get_contents($path);
   return $data;
}
function ex_delfile($path) {
   return unlink($path);
}
?>