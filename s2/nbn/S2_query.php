<?php 
error_reporting(E_ALL);
$paths = array(
   '../',
   '../lib/',
   '../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_DBT.php');       
include_once('nbn/NBN_Cache.php');
include_once('snc/S2_Files_v1.1.php');       

$itype = (isset($_REQUEST['IType']))?$_REQUEST['IType']:'NBN_Local_Species_List';
$action = (isset($_REQUEST['Action']))?$_REQUEST['Action']:'List';
$current = (isset($_REQUEST['Current']))?$_REQUEST['Current']:null;
$settings = (isset($_REQUEST['CurrentSettings']))?json_decode(stripslashes($_REQUEST['CurrentSettings'])):null;
//print_r(json_decode($_REQUEST['CurrentSettings']));

include_once('gen/G_query.php');
?>