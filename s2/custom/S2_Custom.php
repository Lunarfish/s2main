<?php
/*
class S2_Site_Appropriate_Management_Decision extends DBT {
   protected $tablename = 'S2_Site_Appropriate_Management_Decision';
   protected $displayname = 'Site Appropriate Management Decision';
   //protected $show = array('S2_Site_Management_Agreement');
   protected $columns = array(
      'S2_Site_Appropriate_Management_Decision_ID'    => 'Unique Key',
      'S2_Site_ID'                  => '{"DataType":"LINKEDTO","TargetType":"S2_Site","TargetField":"S2_Site_ID","Mandatory":1,"Current":1}',
      'Management_Is_Appropriate'   => 'True or False',               
      'On_Date'                     => 'Date',
      'Applies_From'                => 'Date',
      'Applies_Until'               => 'Date'   
   ); 
   protected $domain = 'sites';
   protected $instancedomain = true;
   protected $instanceof = 'S2_Authority';
   protected $permissions = array(
      'Def'    => 'Partnership Administrator',
      'List'   => 'Partnership Member',
      'View'   => 'Partnership Member'
   );
}
*/
?>