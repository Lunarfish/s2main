<?php
error_reporting(E_ERROR);
ini_set('memory_limit','64M');
//ini_set('post_max_size','500M');
//set_time_limit(0);

$paths = array(
   '../',
   '../lib/',
   '../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_DBT.php');
       
include_once('snc/S2_Sites.php');
include_once('snc/S2_Files.php');
include_once('snc/S2_Contacts.php');
include_once('snc/S2_Habitats.php');
include_once('snc/S2_Projects.php');

include_once('gen/G_Admin.php');
include_once('gen/G_Species.php');

include_once('keysites/KS_Key_Sites.php');

include_once('custom/S2_Custom.php');
 
function get_subclasses_of($parentClassName) {
   $classes = array();
   foreach (get_declared_classes() as $className) {
      if (is_subclass_of($className, $parentClassName)) $classes[] = $className;
   }
   return $classes;
}
function get_class_hierarchy($root) {
   $cs = get_subclasses_of($root);
   //print_r($cs);exit;
   $cs2 = array();
   foreach($cs as $c) {
      $i = new $c();
      $d = $i->getdomain();
      if ($d!='genera') {
         if (!isset($cs2[$d])) $cs2[$d] = array();
         $cs2[$d][] = $c;
      }
   }
   return $cs2;
}
function get_itype_definition($itype) {
   $obj = null;
   if (isset($itype)) {
      $cs = get_class_hierarchy('DBT');
      
      $dbt = new $itype();
      $cobj = new stdClass();
      $cobj->IType = $itype;
      $cobj->DisplayName = $dbt->getdisplayname();
      $cobj->TableName = $dbt->gettablename();
      $cobj->Domain = $dbt->getorigdomain();
      $cobj->InstanceOf = $dbt->getinstanceof();
      $cobj->Columns = $dbt->getcolumns();
      foreach($cobj->Columns as $col => $def) {
         if (preg_match('/^\{/',$def)) $cobj->Columns[$col] = json_decode($def);
      } 
      $cobj->Views = $dbt->getviewdefs();
      $cobj->Show = $dbt->getshowlist();
      $cobj->Permissions = $dbt->getpermissions();      
      
      $obj = new stdClass();
      $obj->Current = $cobj;
      $obj->DataTypes = $dbt->getdatatypes();
      $obj->ShowOpts = $dbt->getshowopts();
      $obj->AvailableClasses = $cs;
      $obj->DefaultPermissions = get_domain_permission_names($cobj->Domain);
      $obj->Actions = $dbt->getactions();
      
   }
   return $obj;
}

$action = (isset($_REQUEST['Action']))?$_REQUEST['Action']:null;
$itype = (isset($_REQUEST['IType']))?$_REQUEST['IType']:null;
switch($action) {
case 'Define': {
   $res = (isset($itype))?get_itype_definition($itype):null;
   echo json_encode($res);
}break;
case 'Update': {

}break;
default: {
   $cs = get_class_hierarchy('DBT');
   echo json_encode($cs);
}break;
}
?>