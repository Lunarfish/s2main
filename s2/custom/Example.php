<?php

class Example extends DBT {
   /*
      There are quite a lot of other things you can do with a table in 
      Second Site but this is the bare minimum.
   */
   protected $tablename = 'ExampleTable'; // The table name has to conform to database standards.
   protected $displayname = 'Example Table'; // Describes how the table will appear on the screen. 
   
   //
   // CreatedBy and CreatedOn are added and populated automatically
   //
   protected $columns = array(
      'Example_ID'      => 'Unique Key',
      'Name'            => 'Short Text',
      'Value'           => 'Number',
      'Link'            => '{"DataType":"LINKEDTO","TargetType":"DBTableName1","TargetField":"DBColumnName"}'                  
   );
}
?>