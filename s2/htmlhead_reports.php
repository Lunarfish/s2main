<?php //error_reporting(E_ALL); 

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="ISO-8859-1"/>
<meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1"/>
<title>Second Site @ YHEDN</title>
<link rel="shortcut icon" href="http://s2.yhedn.org.uk/favicon.ico"/>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places,geometry&sensor=true"></script>
<?php 
$stylesheets = array(
   '../styles/usb.css'              => 'screen',
   '../styles/usb2.css'             => 'screen',
   '../styles/cdm.css'              => 'screen',
   '../styles/snc.css'              => 'screen',
   '../styles/s2_gmap.css'          => 'screen',
   '../styles/fileuploader.css'     => 'screen',
   '../styles/s2_color_border.css'  => 'screen',
   '../styles/user_styles.css'      => 'screen',
   '../styles/s2_print.css'         => 'print'   
);
foreach($stylesheets as $stylesheet => $media) {
   print "<style type='text/css' media='$media'>";
   include($stylesheet);
   print "</style>";
}
$javascripts = array(
   '../lib/htmlentities.js',
   '../lib/json/json2.js',
   '../lib/calendarButtons.js',
   '../lib/proj4js/proj4js-combined.js',
   "../lib/proj4js/defs/EPSG27700.js",
   "../lib/proj4js/defs/EPSG29901.js",
   "../lib/proj4js/defs/EPSG29902.js",
   "../lib/proj4js/defs/EPSG4326.js",
   '../lib/coordinateConversions.js',
   '../lib/genericInterface.js',
   '../lib/screen.js',
   '../lib/s2_v1_5.js',
   '../lib/s2_ordering.js',
   '../lib/s2_exporter.js',
   '../lib/cdm/cdm_dialog.js',
   '../lib/tablemanager.js',
   '../lib/s2_reports.js',
   '../lib/s2_gmapv3_v1_5.js',
   '../lib/s2_gmapv3areas_v1_5.js'
);
foreach($javascripts as $script) {
   print "<script type='text/javascript'>";
   include($script);
   print "</script>";
}

?>
<!--[if gte IE 5]>
<style type='text/css' media='screen'>
div#page {
   width: 90%;
}
div#dialog {
   width: 80%;
}
</style>  
<![endif]-->
</head>
