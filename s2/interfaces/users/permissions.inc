<?php
/*
print getcwd(); exit;
$paths = array(
   '../',
   '../../',
   '../../lib/'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
*/
include_once('settings.conf');
include_once('gen/G_Users.php');
#
# Permissions Configuration File
#

# Defines all default permissions in a domain
# Syntax for new permission options:
# $__PERMISSIONS[<realm>][<permission label>] = 0 or 1;

# php_users Permissions
//connect_to_users_db();
function get_default_permissions() {
   global $USERS_TABLES;
   $table = $USERS_TABLES['Default_Permissions'];
   $link = new $table();
   //if (!$link->exists()) $link->create();
   $PERMISSIONS = $link->getperms();
   return $PERMISSIONS;      
}
function get_domain_permission_names($domain) {
   $allperms = get_default_permissions();
   $domainnames = array();
   foreach($allperms[$domain] as $perm => $def) $domainnames[] = $perm;
   return $domainnames;
}
function get_default_permission($domain,$label) {
   global $USERS_TABLES;
   $table = $USERS_TABLES['Default_Permissions'];
   $link = new $table();
   //if (!$link->exists()) $link->create();
   $col = 'Value';
   $where = array();
   $where['Domain'] = $domain;
   $where['Label'] = $label;
   $value = $link->selectvalwhere($col,$where);
   return $value;   
}

function get_anonymous_permissions() {
   global $USERS_TABLES;
   $table = $USERS_TABLES['Default_Permissions'];
   $link = new $table();
   //if (!$link->exists()) $link->create();
   $PERMISSIONS = $link->getanonperms();
   return $PERMISSIONS;   
}
$__PERMISSIONS = get_anonymous_permissions();

//print_r($__PERMISSIONS);exit;
?>
