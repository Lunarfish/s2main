<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Copyright (c) 1997-2002 The PHP Group                                |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the PHP license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at                              |
// | http://www.php.net/license/2_02.txt.                                 |
// | If you did not receive a copy of the PHP license and are unable to   |
// | obtain it through the world-wide-web, please send a note to          |
// | license@php.net so we can mail you a copy immediately.               |
// +----------------------------------------------------------------------+
// | Authors: Byrne Reese <byrne at majordojo dot com                     |
// +----------------------------------------------------------------------+
//
// $Id: users_header.php,v 1.1.1.1 2003/06/03 14:12:24 byrnereese Exp $

include_once("users/users.conf");  
if (!isset($PAGE_TITLE)) { 
  $PAGE_TITLE = "Users"; 
} 
?>
<html>

<head>
  <title><?=$PAGE_TITLE?></title>
  <link rel="shortcut icon" href="http://localhost/s2/favicon.ico"/>
  <link rel="stylesheet" type="text/css" href="<?php echo $USERS_BASE_URL;?>/styles.css">
</head>

<body>
<div class='userscontainer'>
<?php include("G_usermenubar.php");?>
</div>

<center><div style='width:70%;'>


