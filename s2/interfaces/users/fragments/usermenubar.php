<div style='clear:both;background-color: #ccc; padding: 5px 10px 5px 10px;text-align:center; border-bottom: 1px solid #999;'>
<a href='../users' style='float:left;font-weight:bold;'>Access Controls</a>
<?php
if ($_CURRENT_USER->is_anonymous) {
?>
<a class="buttonLink" href="<?php echo $USERS_BASE_URL;?>/login.php?returnto=<?php echo returnto_url_enc();?>">[ Login ]</a>
<?php
} else {
  if ($_CURRENT_USER->can_edit_self()) {
?>
<a class="buttonLink" href="<?php echo $USERS_BASE_URL;?>/users_edit.php?returnto=<?php echo returnto_url_enc();?>">[ My Settings ]</a>&nbsp;&nbsp;
<?php
  }
  if ($_CURRENT_USER->can_list_users()) {
?>
<a class="buttonLink" href="<?php echo $USERS_BASE_URL;?>/users.php?returnto=<?php echo returnto_url_enc();?>">[ Users ]</a>&nbsp;&nbsp;
<?php
  }
?>
<a class="buttonLink" href="<?php echo $USERS_BASE_URL;?>/logoffconfirm.php?returnto=<?php echo returnto_url_enc();?>">[ Logoff ]</a>
<?php
}
?>
</div>