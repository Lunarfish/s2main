<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Copyright (c) 1997-2002 The PHP Group                                |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the PHP license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at                              |
// | http://www.php.net/license/2_02.txt.                                 |
// | If you did not receive a copy of the PHP license and are unable to   |
// | obtain it through the world-wide-web, please send a note to          |
// | license@php.net so we can mail you a copy immediately.               |
// +----------------------------------------------------------------------+
// | Authors: Byrne Reese <byrne at majordojo dot com                     |
// +----------------------------------------------------------------------+
//
// $Id: users.inc,v 1.1.1.1 2003/06/03 14:12:24 byrnereese Exp $

include_once("users.conf");
require_once("utils.inc");
include_once('klib.php');
include_once('Zend/Db.php');

function require_login() {
   global $USERS_BASE_URL;
   Header("Location: $USERS_BASE_URL/login.php?returnto=".returnto_url_enc());
}

function connect_to_users_db() {
   /*
   global $USERS_DBHOST,$USERS_DB, $USERS_DBUSER, $USERS_DBPASS;
   if (!mysql_connect($USERS_DBHOST, $USERS_DBUSER, $USERS_DBPASS)) {
    // couldn't connect
    echo "could not connect ($USERS_DBHOST, $USERS_DBUSER, $USERS_DBPASS)"; 	
   }
   if (!mysql_select_db($USERS_DB)) {
    // couldn't connect
    echo "could not select ($USERS_DB)"; 	
   }
   */
   global $dbConfig, $dbPlatform;
   $dbh = Zend_Db::factory("Pdo_${dbPlatform}", $dbConfig);
   return $dbh;
}

function send_confirmation($email,$token) {
  global $COMPANY_NAME,$USERS_BASE_URL,$WEB_MASTER,$WEB_MASTER_EMAIL;
  
//  mail("$email, $WEB_MASTER_EMAIL", "Welcome to $COMPANY_NAME", "Thank you for signing up. This email has been sent to you automatically. Please click the link below in order to confirm your account.\n\n$USERS_BASE_URL/confirm_account.php?token=".base64_encode($token)."\n\nEmail: $email\nToken: ".base64_encode($token)."\n\nThanks,\n$WEB_MASTER","From: $WEB_MASTER <$WEB_MASTER_EMAIL>\n");    
  mail("$email, $WEB_MASTER_EMAIL", 
  "Welcome to $COMPANY_NAME", 
  "Thank you for signing up. This email has been sent to you automatically. 
  Please click the link below in order to confirm your account.\n\n
  $USERS_BASE_URL/?Action=GetForm&Form=Confirm&Token=".base64_encode($token)."\n\n
  Email: $email\nToken: ".base64_encode($token)."\n\n
  Thanks,\n
  $WEB_MASTER",
  "From: $WEB_MASTER <$WEB_MASTER_EMAIL>\n");    
}

class User {
   protected $id;
   public $name;
   public $email;
   var $seclev;
   var $status;
   var $is_anonymous;
  
   var $_PERMISSION_DATA;

   #  function isAdmin() {
   #    return ($this->utype == "admin");
   #  }
   function setid($id) {
      $this->id = $id;
   }   
   function getid() {
      return $this->id;
   }
   function getusername() {
      return (isset($this->name))?$this->name:$this->email;
   }
   
   function load_from_db() {
      global $USERS_DB,$USERS_TABLES;
      $table = $USERS_TABLES['Users'];
      
      /*$sql = "SELECT Name,Email,Status,Seclev 
         FROM $table WHERE User_Id=".$this->id;
      $query = mysql_query($sql) 
         or die ("The query failed! (".mysql_error()."): $sql"); 
      if ($query && (mysql_num_rows($query) > 0)) {
         list(
         $this->name,
         $this->email,
   	   $this->status,
   	   $this->seclev) = mysql_fetch_row($query);
      }*/
      $link = new $table();
      //if (!$link->exists()) $link->create();
      $link->loaddata($this->id);
      $data = $link->getdata();
      $this->name = $data['Name'];
      $this->email = $data['Email'];
      $this->status = $data['Status'];
      $this->seclev = $data['Seclev'];      
   }

  function init($cookie) {
    //global $USER_COOKIE,$_COOKIE,$_REQUEST,$COOKIE_KEY;
    global $COOKIE_KEY;
    if (isset($cookie)) {
       $this->is_anonymous = 0;
       $cookie = udec($cookie);
//print $cookie;exit;
       $a = preg_split('/\&/', $cookie);
       $i = 0;
       $cvals = array();
       while ($i < count($a)) {
         $b = preg_split ('/\=/', $a[$i]);
         $key = urldecode($b[0]);
         if (preg_match('/^\w+$/',$key)) {
            $value = urldecode($b[1]);
            eval("\$this->$key = \$value;");
         }
         $i++;
       }
    } else {
      $this->is_anonymous = 1;
    }
    if (!isset($this->name) || !isset($this->email) || !isset($this->uid)) {
       $this->is_anonymous = 1;
       $this->unset_user_cookie();   
    } else {
       $this->id = $this->uid;
       $this->set_user_cookie();
    } 
  }

  function can_edit_users() {
    return $this->get_permission('users','edit_users');
  }
  function can_list_users() {
    return $this->get_permission('users','list_users');
  }
  function can_edit_self() {
    return $this->get_permission('users','edit_self');
  }
  function can_set_perm() {
    return $this->get_permission('users','set_perm');
  }
  function can_edit_perm() {
    return $this->get_permission('users','edit_perm');
  }
  function can_change_others_password() {
    return $this->get_permission('users','change_other_pass');
  }
  
  function set_user_cookie() {
    global $USER_COOKIE,$COOKIE_PATH,$COOKIE_DOMAIN,$COOKIE_KEY;
    $cookie = "email=".urlencode($this->email)."&name=".$this->name."&uid=$this->id&seclev=$this->seclev";
    $cookie = uenc($cookie);
    //$expire = (time() + (3600 * 24 * 365 * 5));
    $expire = (time() + (3600 * 3));
    setcookie($USER_COOKIE,$cookie,$expire,$COOKIE_PATH,$COOKIE_DOMAIN,0);
  }
  
  function unset_user_cookie() {
    global $USER_COOKIE,$COOKIE_PATH,$COOKIE_DOMAIN;
    setcookie($USER_COOKIE,"",(time() - 3600),$COOKIE_PATH,$COOKIE_DOMAIN);
  }
  function set_defaultpermissions($user_id) {
    global $__PERMISSIONS,$USERS_DB,$USERS_TABLES;
      foreach($__PERMISSIONS as $domain => $permissionlabels) {
         foreach($permissionlabels as $label => $default) {
            /*
            $table = $USERS_DB.".".$USERS_TABLES['Permissions'];
            $sql = "
               INSERT 
               INTO $table (User_Id,Domain,Label,Value)
               VALUES ('$user_id','$domain','$label',$default);";
            $query = mysql_query($sql) or die ("The query failed! (".mysql_error()."): <pre><tt>$sql</tt></pre>");
            */
            $table = $USERS_TABLES['Permissions'];
            $link = new $table();
            $data = array(
               'User_Id'   =>$user_id,
               'Domain'    =>$domain,
               'Label'     =>$label,
               'Default'   =>$default
            );
            $link->setdata($data);
            $link->insert();                                
         }
      } 
  }
  function get_permission($domain,$label) {
    global $__PERMISSIONS;
    if ($this->is_anonymous) {
      return $__PERMISSIONS[$domain][$label];
    }
    // fetch permissions in database if they have not already been fetched
    if (!isset($this->_PERMISSION_DATA)) {
      $this->_PERMISSION_DATA = get_permissions_for_user($this->id);
    }
    if (isset($this->_PERMISSION_DATA[$domain][$label])) {
      return $this->_PERMISSION_DATA[$domain][$label];
    } else {
      // if permission has not been set return the default permission
      //return $__PERMISSIONS[$domain][$label];
      return get_default_permission($domain,$label);
    }
  } 
}

function get_permissions_for_user($user_id) {
   global $USERS_DB,$USERS_TABLES;
   //connect_to_users_db();
   if (isset($user_id)) {
      /*
      $table = $USERS_DB.".".$USERS_TABLES['Permissions'];
      $sql = "
      SELECT Domain,Label,Description,Value
         FROM $table 
      WHERE User_Id=$user_id 
      ORDER BY Domain,Label";
      $query = mysql_query($sql) or die ("The query failed! (".mysql_error()."): <pre><tt>$sql</tt></pre>");
 
      while ($query && (list($domain,$label,$description,$value) = mysql_fetch_row($query))) {
   #     $PERMS[<domain>][<permission lavel>] = <value>;
         $PERMS[$domain][$label] = $value;
      }
      */
      $table = $USERS_TABLES['Permissions'];
      $link = new $table();
      $perms = $link->select('User_Id',$user_id);
      foreach($perms as $row => $parray) {
         $d = $parray['Domain'];
         $l = $parray['Label'];
         $v = $parray['Value'];
         $PERMS[$d][$l] = $v;
      } 
   } else {
      $PERMS = get_default_permissions();
   }
   return $PERMS;
}

function uenc($clr) {
   global $COOKIE_KEY;
   if (isset($COOKIE_KEY)) {
      $iv = mcrypt_create_iv(mcrypt_get_iv_size (MCRYPT_BLOWFISH, MCRYPT_MODE_ECB), MCRYPT_RAND);
      $enc = base64_encode(mcrypt_encrypt (MCRYPT_BLOWFISH, $COOKIE_KEY, $clr, MCRYPT_MODE_ECB, $iv));
   } else $enc = $clr;
   return $enc;   
}
function udec($enc) {
   global $COOKIE_KEY;
   if (isset($COOKIE_KEY)) {
      $b64d = base64_decode($enc);
      $iv = mcrypt_create_iv(mcrypt_get_iv_size (MCRYPT_BLOWFISH, MCRYPT_MODE_ECB), MCRYPT_RAND);
      $clr = mcrypt_decrypt(MCRYPT_BLOWFISH, $COOKIE_KEY, $b64d, MCRYPT_MODE_ECB, $iv);
   } else $clr = $enc;
   $clr = rtrim($clr);
   return $clr;
}

// This code is run every single time and initializes the
// $_CURRENT_USER object
$_CURRENT_USER = new User; 
$_CURRENT_USER->is_anonymous = 1;

if (isset($_COOKIE[$USER_COOKIE])||isset($_REQUEST['UInfo'])) {
  $_CURRENT_USER->init((isset($_COOKIE[$USER_COOKIE]))?$_COOKIE[$USER_COOKIE]:preg_replace('/\s/','+',$_REQUEST['UInfo']));
}
/*
print_r($_CURRENT_USER);
print "USER ID:".$_CURRENT_USER->getid();
exit;
//*/
?>