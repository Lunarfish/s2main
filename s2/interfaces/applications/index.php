<?php
session_start();
//if (!isset($_COOKIE['PHPSESSID'])) header("Location: ".$_SERVER['PHP_SELF']);
set_time_limit(0);
ini_set('memory_limit','64M');
$paths = array(
   '../',
   '../lib/',
   '../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include('cdm/cdm_use_encryption_v1_1.php');
include_once('users/users.inc');
?>
<html>
<head>
<title>Second Site - Local Sites data @ YHEDN</title>
<link rel="shortcut icon" href="../favicon.ico"/>
<link rel='stylesheet' href='../styles/usb.css' media='screen'/>
<link rel='stylesheet' href='../styles/usb2.css' media='screen'/>
<link rel='stylesheet' href='../styles/cdm.css' media='screen' />
<link rel='stylesheet' href='../styles/snc.css' media='screen' />
<link rel='stylesheet' href='../styles/s2_gmap.css' media='screen' />
<link rel='stylesheet' href='../styles/fileuploader.css' media='screen' />
<link rel='stylesheet' href='../styles/s2_print.css' media='print' />
<link rel='stylesheet' href='../styles/user_styles.css' media='screen'/>

<script type='text/javascript' src='../lib/htmlentities.js'></script>
<script type='text/javascript' src='../lib/msword.js'></script>
<script type='text/javascript' src='../lib/json/json2.js'></script>
<script type='text/javascript' src='../lib/calendarButtons.js'></script>
<script type='text/javascript' src='../lib/coordinateConversions.js'></script>
<script type='text/javascript' src='../lib/genericInterface.js'></script>
<script type='text/javascript' src='../lib/screen.js'></script>
<script type='text/javascript' src='../lib/cdm/cdm_encrypt.js'></script>
<script type='text/javascript' src='../lib/cdm/cdm_authenticate2.js'></script>
<!--script type='text/javascript' src='../lib/cdm/cdm_request2.js'></script-->
<script type='text/javascript' src='../lib/cdm/cdm_orequest.js'></script>
<script type='text/javascript' src='../lib/cdm/cdm_dialog.js'></script>
<script type='text/javascript' src='../lib/tablemanager.js'></script>
<script type='text/javascript' src='../lib/s2_mapdrawingtools.js'></script>
<script type='text/javascript' src='../lib/s2_mapcreate.js'></script>
<script type='text/javascript' src='../lib/fileuploader.js'></script>
<script type='text/javascript' src='http://maps.google.com/maps?file=api&v=2&key=<?php print $gmapapikey;?>'></script>
<script type='text/javascript' src='../lib/s2.js'></script>
<script type='text/javascript' src='../lib/G_imanipulator_o.js'></script>
<!--[if gte IE 5]>
<style type='text/css' media='screen'>
div#page {
   width: 90%;
}
</style>  
<![endif]-->


</head>
<body onload='s2_start();' onunload='s2_savestorage();'>

<div id='main'>
<div class='userscontainer'>
<?php //include_once('users/fragments/usermenubar.php'); ?>
<?php include_once('G_usermenubar.php'); ?>
</div>
<?php 
if ($encrypt) {
   $imgurl = '../images/padlock_closed.gif';
   $imgalt = 'encrypted'; 
} else {
   $imgurl = '../images/padlock_open.gif';
   $imgalt = 'open'; 
}
?>
<button id='flip_encrypt' onclick='s2_flip_encryption(this.id);' class='flipencrypt'><img src='<?php print $imgurl; ?>' alt='<?php print $imgalt; ?>'/></button>
<center>
<div id='page'>
<div class='liner'>
<?php //print "<pre>";print_r(get_included_files());print "</pre>"; ?>
<div class='headerdiv'>
<img id='s2logo' src='../images/S2Georgia_40px.gif' alt='s2'/>
<!--<img id='s2logo' src='../images/s2_svgexp.gif' alt='s2'/>-->
<!--<img style='float:right;height:40px;margin:0 0 10px 0' src='../images/s2_v2-100px.gif' alt='s2'/>-->
<!--<img style='float:right;height:40px;margin:0 0 10px 0' src='images/2ndsite.png' alt='2nd site'/>-->
<h1 id='s2heading'>Second Site ...</h1>
</div>
<!--<h3 style='clear:both;margin-bottom:0;padding-bottom:0;'>Current settings</h3>
<hr style='clear:both;'/>
<div id='top-menus'></div>
-->
<hr style='clear:both;'/>
<div id='incontentdialog'></div>
<div id='left-menus'></div>

<!-- all content gets generated in here -->
<!--<div id='cookiediv'></div>-->
<div id='content'></div>
<?php //print "session id &gt;".session_id() . "&lt;";?>
<?php //print "<pre>";print_r($cdm);print "</pre>";?>
</div>
<div class='footerdiv'>
<hr style='clear:both;'/>
<div id='logo-div'>
<a target='yhedn' id='yhednlink' class='imagelink' href='http://www.yhedn.org.uk'>
<img id='yhednlogo' class='imagelink' src='../images/yhedn_with_text_onright_30px.jpg' alt='Built by YHEDN'/></a>
</div>

<p>Providing an audit trail for 2nd-tier wildlife and geological site partnerships.</p>
<hr style='clear:both;'/>
</div>
</div>
</div>

<div id='screen'></div>

<div id='dialog-container' style='display:none;'>
<div id='dialog'>
<div class='navbar' onmousedown='follow(this.parentNode,event);' onmouseup='follow(this.parentNode,event);'>
<button class='close' onclick='hidedialog();'>&times;</button>
</div>
<div class='liner' id='dialog-liner'>
<h2 id='dialog-title'></h2>
</div>
</div>
</center>

</div>
</body>
</html>
