<?php
$paths = array(
   '../',
   '../lib/',
   '../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('users/users.inc');
$authority = $_REQUEST['Authority'];
$source = $_REQUEST['Source'];
switch($source) {
case 'R6': {
   $haspermission = $_CURRENT_USER->get_permission($authority,'View Recorder 6 Data');
   $server = 'data.yhedn.org.uk/r6';
   //$server = 'localhost/r6';
   $url = '/r62json.php';
   if ($haspermission) { 
      switch ($authority) {
      case "Rotherham BRC Data Search": {
         $dsn="R6_RBRC1";
         $username="R6_RBRC_VIEW";
         $password="N8NU5er";
      }break;
      case "North Yorkshire SINC Panel": { 
         
         $dsn="R6_NEYEDC1";
         $username="R6_NEYEDC_VIEW";
         $password="N8NU5er";
         /*
         $dsn="R6_NEYEDC_LOCAL";
         $username="NBNUser";
         $password="NBNPassword";
         */
      }break;
      case "East Riding of Yorkshire SINC Panel": { 
         $dsn="R6_NEYEDC1";
         $username="R6_NEYEDC_VIEW";
         $password="N8NU5er";
      }break;
      default: { 
         $dsn="R6_NEYEDC1";
         $username="R6_NEYEDC_VIEW";
         $password="N8NU5er";
      }break;
      }
      $j = new stdClass();
      $j->Status = $haspermission;
      $j->Authority = $authority;
      $j->Source= $source;
      $j->Server = $server;
      $j->URL = $url;
      $j->Database = $dsn;
      $j->Username = $username;
      $j->Password = $password;      
      echo json_encode($j);  
   } else {
      $j = new stdClass();
      $j->Status = $haspermission;
      $j->Message = 'You have not been granted permission to view Recorder data';
      echo json_encode($j);
   }
}break;
}   
?>