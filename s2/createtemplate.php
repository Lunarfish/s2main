<?php 
$templatename = 'GENERA';
$version = "V0.1";
$tables = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22);
$includedatatypes = true;
$includecoredata = true;
error_reporting(E_ALL);
$cdmpath = "./lib/";
ini_set('include_path', $cdmpath);
include_once('jsonprofiles/jsonprofile_functions.php');
include_once('Zend/Db.php');
include_once('snc/SnCDatabaseConnect2.php');

$db = SnC_getDatabaseConnection();

$template = new stdClass();
$template->Template = $templatename;
$template->Version = $version;
$template->Actions = array();
$action = new stdClass();
$action->Name = 'Creating Tables';
$action->Method = 'Define';
$template->Actions[] = $action;
if ($includecoredata) {
   $action = new stdClass();
   $action->Name = 'Inserting Core Data';
   $action->Method = 'Insert';
   $action->Type = 'CoreData';
   $template->Actions[] = $action;
}
$action = new stdClass();
$action->Name = 'Updating Genera Database';
$action->Method = 'UpdateGenera';
$template->Actions[] = $action;
$action = new stdClass();
$action->Name = 'Updating Permissions Database';
$action->Method = 'Update Permissions';
$template->Actions[] = $action;

$query = $db->select();
$query->from('DEV_DataType');
$qdtypes = $db->fetchAll($query);

$query = $db->select();
$query->from('ADM_Table');
$query->where('ADM_Table_Id IN (?)',$tables);
$qtables = $db->fetchAll($query);
foreach ($qtables as $qt => $t) {
   $table = $t['Name'];
   $tid = $t['ADM_Table_Id']; 
   $bits = preg_split('/_/',$t['Name']);
   $t['Prefix'] = array_shift($bits)."_";
   $t['Name'] = join('_',$bits);
   unset($t['ADM_Table_Id']);
   unset($t['CreatedOn']);
   unset($t['CreatedBy']);
   $query = $db->select();
   $query->from('ADM_Column');
   $query->where('ADM_Table_Id = ?',$tid);
   $qcolumns = $db->fetchAll($query);
   foreach($qcolumns as $qc => $c) {
      unset($c['ADM_Table_Id']);
      unset($c['ADM_Column_Id']);
      unset($c['CreatedOn']);
      unset($c['CreatedBy']);
      foreach($qdtypes as $qd => $d) {
         if ($d['DEV_DataType_Id'] == $c['DEV_DataType_Id']) {
            $c['DataType'] = $d['Name'];
         }
      }
      unset($c['DEV_DataType_Id']);
      if ($c['DataType'] == 'Linked Data') {
         $query = $db->select();
         $query->from('ADM_TableForeignKey');
         $query->where('LinkFromTable = ?',$table);
         $query->where('LinkFromColumn = ?',$c['Name']);
         $qfkeys = $db->fetchAll($query);
         foreach($qfkeys as $qf => $f) {
            $c['ToTable'] = $f['LinkToTable'];
         }
      }         
      $qcolumns[$qc] = $c;
   }
   $t['Columns'] = $qcolumns;
   $query = $db->select();
   $query->from($table);
   $coredata = $db->fetchAll($query);
   if (count($coredata)) $t['CoreData'] = $coredata;
   $qtables[$qt] = $t;
} 
$template->Tables = $qtables;
$json = format_json(json_encode($template));
print "<pre>$json</pre>";

function format_json($json) {
/*
   debugging code to print the returned json readably formatted to the screen  
   
   for some reason the last close bracket indent doesn't get removed.
*/
   $indent = 0;
   $tabs = '';
   for ($i=0;$i<strlen($json);$i++) {
      switch(substr($json,$i,1)) {
      case ',': {
         $tabs = "\n";
         for($j=0;$j<$indent;$j++) $tabs = $tabs . "\t";          
         $json = substr($json,0,$i+1) . $tabs . substr($json,$i+1);
         $i += strlen($tabs);      
      }break;
      case '{': {
         $indent++;
         $tabs = "\n";
         for($j=0;$j<$indent;$j++) $tabs = $tabs . "\t";          
         $json = substr($json,0,$i+1) . $tabs . substr($json,$i+1);
         $i += strlen($tabs);         
      }break;
      case '[': {
         $indent++;
         $tabs = "\n";
         for($j=0;$j<$indent;$j++) $tabs = $tabs . "\t";          
         $json = substr($json,0,$i+1) . $tabs . substr($json,$i+1);
         $i += strlen($tabs);
      }break;
      case '}': {
         if (substr($json,$i+1,1) == ',') {
            $indent--;
            $tabs = "\n";
            for($j=0;$j<$indent;$j++) $tabs = $tabs . "\t";          
            $json = substr($json,0,$i) . $tabs . substr($json,$i,2) . $tabs . substr($json,$i+2);
            $i += strlen($tabs) * 2;
         } else {
            $indent-=2;
            $tabs = "\n";
            $extratab = ($indent >= 0)?"\t":''; 
            for($j=0;$j<$indent;$j++) $tabs = $tabs . "\t";
            $json = substr($json,0,$i) . $tabs . $extratab . substr($json,$i,1) . $tabs . substr($json,$i+1);
            $i += (strlen($tabs) + 1) * 2; // extra 2 for the extra \t tab
         }      
      }break;
      case ']': {
         if (substr($json,$i+1,1) == ',') {
            $indent--;
            $tabs = "\n";
            for($j=0;$j<$indent;$j++) $tabs = $tabs . "\t";          
            $json = substr($json,0,$i) . $tabs . substr($json,$i,2) . $tabs . substr($json,$i+2);
            $i += strlen($tabs) * 2;
         } else {
            $indent-=2;
            $tabs = "\n";
            $extratab = ($indent >= 0)?"\t":''; 
            for($j=0;$j<$indent;$j++) $tabs = $tabs + "\t";
            $json = substr($json,0,$i) . $tabs . $extratab . substr($json,$i,1) . $tabs . substr($json,$i+1);
            $i += (strlen($tabs) + 1) * 2; // extra 2 for the extra \t tab
         }
      }break;
      }      
   }   
   return $json;
}
?>