var bgsbaseurl = 'http://dan.lunarfish.co.uk/bgs/';
var bgsroot = 'http://data.bgs.ac.uk/ref/';
var bgsloading = 0;
var bgsloadlist = [];
var bgsfileloadinprogress = false;

var bgs_cells = [];
var bgs_tcols = [];
var bgs_ctab;
var bgs_show;
var bgscache = [];
 
function s2_bgsify() {
   var tabs,t,tab; 
   tabs = document.getElementsByTagName('TABLE');
   for(t=0;t<tabs.length;t++) {
      tab = tabs[t];
      if (!tab.id) tab.id = 'tab_'+t; 
      s2_bgsifytab(tab);
   }
}
function s2_bgs_geturicols(tab) {
   var tr,th,bgscols;
   bgscols = [];
   tr = tab.tHead.rows[0];
   for(cx=0;cx<tr.cells.length;cx++) {
      th = tr.cells[cx];
      if (th.className && th.className.match(/BGS/)) bgscols.push(cx); 
   }
   return bgscols; 
}
function s2_bgsifytab(tab) {
   var o,bgscols,addcols,tbody,thead,tr,th,td,nde,tvk,tx,rx,cx,row,col,tid,cid,addCN,addTG;
   tbody = tab.tBodies[0];
   o = (tab.tHead)?'h':'v'; // list tables have headings across the top 
   switch(o) {
   case 'h': {
      bgscols = s2_bgs_geturicols(tab);
      for(rx=0;rx<tbody.rows.length;rx++) {
         tr = tbody.rows[rx];
         for(tx=0;tx<bgscols.length;tx++) {
            cx = bgscols[tx];
            td = tr.cells[cx];
            if (!td.id) td.id = tab.id+'r'+rx+'c'+cx;
            s2_bgs_loadcell(td);
         }
      }
   }break;
   case 'v': {
      for(rx=0;rx<tbody.rows.length;rx++) {
         tr = tbody.rows[rx];
         for(cx=0;cx<tr.cells.length;cx++) {
            td = tr.cells[cx];
            if (td.nodeName == 'TD' && td.className.match(/BGS/)) {
               if (!td.id) td.id = tab.id+'r'+rx+'c'+cx;
               s2_bgs_loadcell(td);
            }
         }
      }     
   }break;
   }
}
function s2_bgs_send(url,responder,label) {
   var ac,resp;
   ac = new AjaxClass();
   ac.url = url;
   ac.responder = responder;
   ac.Method = 'GET';
   ac.Async = true;
   ac.label = label;
   ac.init();
   resp = ac.send();            
}
function s2_bgs_getname(uri) {
   var url;
   url = bgsbaseurl + 'api.php?a=find&q='+uri;
   if (!bgscache[uri] || bgscache[uri] == 'null') { 
      s2_bgs_send(url,s2_bgs_loadurijson);
   } else {
      s2_bgs_loadurijson(bgscache[uri]);
   }            
}
function s2_bgs_loadurijson(json) {
   var bgsobj,bgsuri,curi,label,nx,cx,cid,cel;
   bgsobj = JSON.parse(json);
   bgsuri = bgsobj.data[0].concept;
   label = bgsobj.data[0].label;
   if (!bgscache[bgsuri] || bgscache[bgsuri] == 'null') bgscache[bgsuri] = json;
   for(cid in bgs_cells) {
      curi = bgs_cells[cid];
      if (curi == bgsuri) {
         cel = $("#"+cid);
         if (cel && cel.prop("nodeName")) s2_bgs_loaduri(cel,bgsobj);
         else delete bgs_cells[cid];
      }   
   }
}
function s2_bgs_loaduri(cel,bgsobj) {
   var lnk,txt,act,tab,addcols,node;
   if (cel) {
      node = cel.children().last();
      //while(!node.prop("nodeName") && node.prev()) node = node.prev();
      lnk = $('<a/>');
      if (!cel.closest('table').hasClass('listform')) 
         lnk.css("padding-left","5px");
      lnk.append(bgsobj.data[0].label); 
      lnk.attr("id",node.attr("id")+"_BGS-Link");
      lnk.attr("href",bgsobj.data[0].concept);
      lnk.on("click", function(event) {
         s2_showbgsdialog(event.target);
         event.stopPropagation();
         event.preventDefault();
         return false;
      });
      tag = node.prop("nodeName");//(node.nodeName)?node.nodeName: 
      if (tag) {
         switch(tag.toUpperCase()) {
         case 'INPUT': {
            node.attr("type","hidden"); 
            node.attr("value",bgsobj.data[0].concept);
            node.before(lnk);
         }break;
         default: node.replaceWith(lnk); break;
         }
      }
   }
   delete bgs_cells[cel.id];
}
function s2_bgs_loadcell(cel) {
   var uri;
   uri = s2_getndeval(cel);
   if (!bgs_cells[cel.id]) bgs_cells[cel.id] = uri;
   s2_bgs_getname(uri);      
}
function s2_showbgsdialog(lnk) {
   var uri,id,ti,node,text,name,btn,p,prop,val,suppress,lid,nid,namespace,args,tag;
   var tab,tabid,tab,tr,th,td,btn,img,o;
   try {

      lid = lnk.id;
      uri = lnk.href;

      args = s2_disectid(lid);

      if (args.Current) namespace = document.getElementById('Form-'+args.Form+'_IType-'+args.IType+'_Property-'+args.Property+'_Current-'+args.Current+'_Namespace').value;
      else namespace = document.getElementById('Form-'+args.Form+'_IType-'+args.IType+'_Property-'+args.Property+'_Namespace').value;

      cleardialog('dialog-liner');
      dialog = document.getElementById('dialog-liner');

      node = document.createElement('input');
      node.type = 'hidden';
      node.id = 'bgs-property';
      node.value = lid;
      dialog.appendChild(node);
      
      node = document.createElement('input');
      node.type = 'hidden';
      node.id = 'bgs-namespace';
      node.value = namespace;
      dialog.appendChild(node);
      
      node = document.createElement('input');
      node.type = 'text';
      node.id = 'bgs-search';
      dialog.appendChild(node);
      
      node = document.createElement('button');
      node.appendChild(document.createTextNode('Search'));
      node.onclick = s2_bgs_search;
      dialog.appendChild(node);
      
      node = document.createElement('div');
      node.id = 'bgs-results';
      tag = (lnk.nodeName)?lnk.nodeName:lnk.prop("nodeName");
      // if id is set load item

      switch(tag.toUpperCase()) {
      case 'A': s2_bgs_showitem(lnk.href); break;
      }

      dialog.appendChild(node);
      // add event handler to #bgs-results div links
      s2_bgs_pageload();
      showdialog();   
   } catch (e) {
      alert('Unable to draw dialog for item');
   } 
}

function s2_bgs_useval(btn) {
   var lnk,uri,lab,pid,tgt,tag,newlink,parent,input,args,lid,iid;
   lnk = btn.previousSibling;
   uri = lnk.href;
   lab = lnk.firstChild.nodeValue;
   pid = $("#bgs-property").val();
   args = s2_disectid(pid);
   iid = 'Form-'+args.Form+'_IType-'+args.IType+'_Property-'+args.Property;
   lid = iid + '_BGS-Link'; 
   newlink = $("<a/>");
   newlink.attr("href",uri);
   newlink.attr("id",lid);
   newlink.append(lab);
   newlink.on("click",function(event) {
      s2_showbgsdialog(event.target);
      event.stopPropagation();
      event.preventDefault();
      return false;
   });
   $("#"+pid).replaceWith(newlink);
   input = $("<input/>");
   input.attr("type","hidden");
   input.attr("id",iid);
   input.attr("value",uri);
   $("#"+lid).before(input);
   //alert(lab + '\n' + uri + '\n' + tag);
   hidedialog();
}



function s2_bgs_pageload() {
   //var tid = $("#bgs-namespace").val();
   //s2_bgs_preload(tid);
   $("#bgs-results").on("click","a",function(event) {
      s2_bgs_showitem(event.target);
      event.stopPropagation();
      event.preventDefault();
      return false;
   });
}
function s2_bgs_empty() {
   var qs,url,req,res,jsn;
   qs = 'api.php?a=empty';
   url = bgsbaseurl + qs;
   req = {target:url,sync:true,responder:null};
   jsn = snc_send(req);
   res = JSON.parse(jsn);
   return res.status;
}
function s2_bgs_loadallfiles() {
   var qs,url,req,res,jsn;
   s2_bgs_empty();
   qs = 'api.php?a=getfiles';
   url = bgsbaseurl + qs;
   req = {target:url,sync:true,responder:null};
   jsn = snc_send(req);
   res = JSON.parse(jsn);
   if (res.status) {
      bgsloadlist = res.data;
      s2_bgs_processloadlist();
   }
}
function s2_bgs_addtoloadlist(files) {
   var file,i,j,item,isloaded,isadded;
   for (i in files) {
      file = files[i];
      isadded = false;
      for (j in bgsloadlist) {
         item = bgsloadlist[j];
         if (file == item) isadded = true;
      }
      isloaded = s2_bgs_isloaded(file); 
      if (!isloaded && !isadded) bgsloadlist.push(file);
      if (isloaded) bgs_loaded[file] = true;
   }
   s2_bgs_processloadlist();
}
function s2_bgs_processloadlist() {
   var file,filecount;
   filecount = bgsloadlist.length; 
   if (!bgsfileloadinprogress && filecount>0) {
      file = bgsloadlist.shift();
      bgsfileloadinprogress = true;
      s2_bgs_loadfile(file);
   }
}
function s2_bgs_parseid(id) {
   var space,classname,concept,item,escroot,bgs_obj;
   space = id.replace(/\/id\//,'/ref/');
   path = space.replace(bgsroot,'');
   paths = path.split(/\//);
   bgs_obj = {};
   bgs_obj.id = id;
   if (paths[0]) bgs_obj.classname = paths[0];
   if (paths[1]) bgs_obj.concept = paths[1];
   if (paths[2]) bgs_obj.item = paths[2];
   if (bgs_obj.item) space = space.replace(/item/,'');
   if (bgs_obj.concept) spave = space.replace(/bgs_obj.concept\//,'');
   bgs_obj.space = space;
   return bgs_obj;     
}
function s2_bgs_preload(space) {
   var files;
   files = [];
   bgs_obj = s2_bgs_parseid(space);
   files.push(bgs_obj.classname + '.nt');
   if (bgs_obj.concept) files.push(bgs_obj.classname + '_' + bgs_obj.concept + '.nt');
   //s2_bgs_load(files);
   s2_bgs_addtoloadlist(files);
   return files; 
}
function s2_bgs_isloaded(file) {
   var req,jsn,res,url,qs,isloaded;
   isloaded = 0;
   if (bgs_loaded[file]) isloaded = true;
   else {
      try {
         qs = 'api.php?a=isloaded&q='+file;
         url = bgsbaseurl + qs;
         req = {target:url,sync:true,responder:null};
         jsn = snc_send(req);
         res = JSON.parse(jsn);
         if (res.status) isloaded = res.data[file];
      } catch (e) {}
   }
   return isloaded;
}
var bgs_loaded = {};
function s2_bgs_load(files) {
   var i,file,isloaded,allloaded;
   allloaded = true;
   for (i in files) {
      file = files[i];
      isloaded = s2_bgs_isloaded(file);
      allloaded &= isloaded;
      if (!isloaded) {
         s2_bgs_loadfile(file);
         bgsloading++;
      } else bgs_loaded[file] = true;            
   }
} 
function s2_bgs_loadfile(file) {
   var req,jsn,res,url,qs,isloaded;
   isloaded = 0;
   try {
      qs = 'api.php?a=load&q='+file;
      url = bgsbaseurl + qs;
      req = {target:url,responder:s2_bgs_afterload};
      snc_send(req);
   } catch (e) {}
   return isloaded;
}
function s2_bgs_afterload(jsn) {
   var res,i,j,file,ident;
   //alert(jsn);
   res = JSON.parse(jsn);
   if (res.status == 1) {
      loaded = res.data[0];
      bgsfileloadinprogress = false;
      s2_bgs_processoutstanding(loaded);
      s2_bgs_processloadlist();
   } else bgsfileloadinprogress = false;
}
function s2_bgs_processoutstanding(loaded) {
   for (i in bgs_outstanding) {
      ident = bgs_outstanding[i];
      for (j in ident.waitingon) {
         file = ident.waitingon[j];
         if (bgs_loaded[file] == true || loaded.match(/file/)) ident.waitingon.splice(j,1);
      }
      bgs_outstanding[i] = ident;
      if (ident.waitingon.length == 0) {
         switch(ident.action) {
         case 'find': s2_bgs_find(ident.id); break;
         }
         bgs_outstanding.splice(i,1);
      } 
   }
}
var bgs_outstanding = [];
function s2_bgs_loadandfind(id) {
   var ident,files;
   ident = s2_bgs_parseid(id);
   files = s2_bgs_preload(ident.space);
   for (file in bgs_loaded) {
      for (i in files) if (files[i] == file) files.splice(i,1);
   } 
   ident.action = 'find';
   ident.waitingon = files;
   if (files.length > 0) bgs_outstanding.push(ident);
   else s2_bgs_find(id);
}
function s2_bgs_find(id) {
   var url,qs,q,ns;
   qs = 'api.php?a=find&q='+id;
   url = bgsbaseurl + qs;
   d3.json(url, function(error, graph) {
      if (error) alert(error);  
      else s2_bgs_showlink(graph); 
   });
}
function s2_bgs_showlink(res) {
   var id,ident,eidclass,lnk;
   if (res.status == 1 && res.data && res.data.length > 0) {
      id = res.data[0].concept;
      ident = s2_bgs_parseid(id);
      eidclass = '.' + s2_bgs_identtoid(ident);
      $(eidclass).empty();//.children().remove();
      $(eidclass).append(res.data[0].label);
   }   
}
function s2_bgs_search(q) {
   var url,qs,q,ns;
   q =  $("#bgs-search").val();
   ns = $("#bgs-namespace").val();
   qs = 'api.php?q='+escape(q)+'&s='+ns;
   url = bgsbaseurl + qs;
   inprogress();
   d3.json(url, function(error, graph) {
      completed();
      if (error) alert(error);  
      else s2_bgs_showresults(graph); 
   });
}
function s2_bgs_showresults(res) {
   //alert(JSON.stringify(res));
   var cont,list,i,row,lnk,nsref,nsid;
   nsref = $("#bgs-namespace").val();
   nsid = nsref.replace(/\/ref\//,'/id/');
   $("#bgs-results").empty();//.children().remove();
   cont = $("#bgs-results");
   if (res.status == 1) {
      list = $("<ul></ul>");
      list.attr("id","bgs-list");
      for (i in res.data) {
         row = res.data[i];
         path = row.concept.split(/\//);
         path.pop();
         concepttype = path.pop().replace(/([a-z])([A-Z])/g,'$1 $2');
         li = $("<li></li>");
         li.append(concepttype + ': ');
         lnk = $("<a></a>");
         lnk.append(row.label);
         lnk.attr("href",row.concept);
         lnk.attr("target","bgs_data");
         li.append(lnk);
         li.css("clear","both");
         if (row.concept.match(nsid)) {
            btn = $("<button/>");
            btn.append('Use');
            btn.on("click",function() {s2_bgs_useval(this);});
            li.append(btn);
         }
         list.append(li);
      }
      cont.append(list);
      cont.append("<p>"+res.acknowledgements+"</p>");      
   } else {
      cont.append("<p>"+res.message+"</p>");
   }
}
function s2_bgs_showitem(id) {
   var url,qs,q,ns;
   qs = 'api.php?q='+escape(id)+'&a=getitem';
   url = bgsbaseurl + qs;
   inprogress();
   d3.json(url, function(error, graph) {
      completed();
      if (error) alert(error);  
      else s2_bgs_drawitem(graph); 
   });
}
function s2_bgs_drawitem(res) {
   var cont,list,li,p,i,row,lnk,btn,tab,tr,th,td,prop,propname,ident,eid,finds,nsref,nsid;
   nsref = $("#bgs-namespace").val();
   nsid = nsref.replace(/\/ref\//,'/id/');
   
   finds = [];
   $("#bgs-results").empty();//.children().remove();
   cont = $("#bgs-results");
   if (res.status == 1) {
      cont.append("<h4>Properties</h4>");
      tab = $("<table></table>");
      tr = $("<tr></tr>");
      th = $("<th></th>");
      th.append('Property');
      tr.append(th);
      th = $("<th></th>");
      th.append('Value');
      tr.append(th);
      tab.append(tr);
         
      if (res.data.item.length > 0) {
         tr = $("<tr/>");
         tr.append("<th>item</th>");
         td = $("<td/>");
         for(i in res.data.item) {
            p = $("<p/>");
            lnk = $("<a/>");
            lnk.append(res.data.item[i].label);
            lnk.attr("id","bgs-item-"+i);
            lnk.attr("href",res.data.item[i].concept);
            lnk.attr("target","bgs_data");
            //lnk.css("display","block");
            p.append(lnk);
            p.css("clear","both");
            // only allow property set for current namespace
            if (res.data.item[i].concept.match(nsid)) {
               btn = $("<button/>");
               btn.append('Use');
               btn.on("click",function() {s2_bgs_useval(this);});
               p.append(btn);
            }
            td.append(p);
         }
         tr.append(td);
         tab.append(tr);
      }
      if (res.data.parent && res.data.parent.length > 0) {
         tr = $("<tr/>");
         tr.append("<th>parent</th>");
         td = $("<td/>");
         for(i in res.data.parent) {
            p = $("<p/>");        
            lnk = $("<a/>");
            lnk.append(res.data.parent[i].label);
            lnk.attr("id","bgs-parent-"+i);
            lnk.attr("href",res.data.parent[i].concept);
            lnk.attr("target","bgs_data");
            //lnk.css("display","block");
            p.append(lnk);
            p.css("clear","both");
            // only allow property set for current namespace
            if (res.data.parent[i].concept.match(nsid)) {
               btn = $("<button/>");
               btn.append('Use');
               btn.on("click",function() {s2_bgs_useval(this);});
               p.append(btn);
            }
            td.append(p);
         }
         tr.append(td);
         tab.append(tr);
      }
      if (res.data.children.length > 0) {
         tr = $("<tr/>");
         tr.append("<th>children</th>");
         td = $("<td/>");
         for(i in res.data.children) {
            p = $("<p/>");
            p.append((parseInt(i)+1)+': ');        
            lnk = $("<a/>");
            lnk.append(res.data.children[i].label);
            lnk.attr("id","bgs-children-"+i);
            lnk.attr("href",res.data.children[i].concept);
            lnk.attr("target","bgs_data");
            //lnk.css("display","block");
            p.append(lnk);
            p.css("clear","both");
            // only allow property set for current namespace
            if (res.data.children[i].concept.match(nsid)) {
               btn = $("<button/>");
               btn.append('Use');
               btn.on("click",function() {s2_bgs_useval(this);});
               p.append(btn);
            }
            td.append(p);
         }
         tr.append(td);
         tab.append(tr);
      }
      if (typeof res.data.properties == 'object') {
         for(propname in res.data.properties) {
            prop = res.data.properties[propname];
            tr = $("<tr></tr>");
            th = $("<th></th>");
            th.append(s2_bgs_proptotext(propname));
            //th.append(prop.label);
            tr.append(th);
            td = $("<td></td>");
            switch(prop.type) {
            case 'uri': {
               ident = s2_bgs_parseid(prop.value);
               eid = s2_bgs_identtoid(ident);
               lnk = $("<a></a>");
               lnk.attr("href",prop.value);
               lnk.addClass(eid);
               lnk.attr("target","bgsld");
               lnk.append(prop.value);
               td.append(lnk);
               // You can't use a ? as an identifier since it identifies a 
               // query string but it appears in the BGS data 
               // Geochronology / Division concept  
               if (ident.item != '?') finds.push(prop.value);          
            }break; 
            default: td.append(prop.value); break;
            }
            tr.append(td);
            tab.append(tr);
         }
         cont.append(tab);         
      }
      cont.append("<p>"+res.acknowledgements+"</p>");      
   } else {
      cont.append("<p>"+res.message+"</p>");
   } 
   //cont.append(JSON.stringify(res));
   for (i in finds) s2_bgs_find(finds[i]);//s2_bgs_loadandfind(finds[i]);
}
function s2_bgs_identtoid(ident) {
   return 'bgs-' + ident.classname + '-' + ident.concept + '-' + ident.item;
}
function s2_bgs_proptotext(prop) {
   return prop.replace(/([a-z])([A-Z])/g,'$1 $2').toLowerCase(); 
}
;function htmlentities(string) {
   for (var i in entity_table) {
      if (i != 38) {
         string = string.replace(new RegExp(entity_table[i], "g"), String.fromCharCode(i));
      }
   }
   var p1,p2;
   string = string.replace(new RegExp("&#(x?)(\\d+);", "g"), String.fromCharCode( ( (p1 == 'x') ? parseInt(p2, 16) : p2 ) ) );
   string = string.replace(new RegExp(entity_table[38], "g"), String.fromCharCode(38));
   return string;
} 


var entity_table = {
  //	34: "&quot;",		// Quotation mark. Not required
  38: "&amp;",		// Ampersand. Applied before everything else in the application
  60: "&lt;",		// Less-than sign
  62: "&gt;",		// Greater-than sign
  //	63: "&#63;",		// Question mark
  //	111: "&#111;",		// Latin small letter o
  160: "&nbsp;",		// Non-breaking space
  161: "&iexcl;",		// Inverted exclamation mark
  162: "&cent;",		// Cent sign
  163: "&pound;",		// Pound sign
  164: "&curren;",	// Currency sign
  165: "&yen;",		// Yen sign
  166: "&brvbar;",	// Broken vertical bar
  167: "&sect;",		// Section sign
  168: "&uml;",		// Diaeresis
  169: "&copy;",		// Copyright sign
  170: "&ordf;",		// Feminine ordinal indicator
  171: "&laquo;",		// Left-pointing double angle quotation mark
  172: "&not;",		// Not sign
  173: "&shy;",		// Soft hyphen
  174: "&reg;",		// Registered sign
  175: "&macr;",		// Macron
  176: "&deg;",		// Degree sign
  177: "&plusmn;",	// Plus-minus sign
  178: "&sup2;",		// Superscript two
  179: "&sup3;",		// Superscript three
  180: "&acute;",		// Acute accent
  181: "&micro;",		// Micro sign
  182: "&para;",		// Pilcrow sign
  183: "&middot;",	// Middle dot
  184: "&cedil;",		// Cedilla
  185: "&sup1;",		// Superscript one
  186: "&ordm;",		// Masculine ordinal indicator
  187: "&raquo;",		// Right-pointing double angle quotation mark
  188: "&frac14;",	// Vulgar fraction one-quarter
  189: "&frac12;",	// Vulgar fraction one-half
  190: "&frac34;",	// Vulgar fraction three-quarters
  191: "&iquest;",	// Inverted question mark
  192: "&Agrave;",	// A with grave
  193: "&Aacute;",	// A with acute
  194: "&Acirc;",		// A with circumflex
  195: "&Atilde;",	// A with tilde
  196: "&Auml;",		// A with diaeresis
  197: "&Aring;",		// A with ring above
  198: "&AElig;",		// AE
  199: "&Ccedil;",	// C with cedilla
  200: "&Egrave;",	// E with grave
  201: "&Eacute;",	// E with acute
  202: "&Ecirc;",		// E with circumflex
  203: "&Euml;",		// E with diaeresis
  204: "&Igrave;",	// I with grave
  205: "&Iacute;",	// I with acute
  206: "&Icirc;",		// I with circumflex
  207: "&Iuml;",		// I with diaeresis
  208: "&ETH;",		// Eth
  209: "&Ntilde;",	// N with tilde
  210: "&Ograve;",	// O with grave
  211: "&Oacute;",	// O with acute
  212: "&Ocirc;",		// O with circumflex
  213: "&Otilde;",	// O with tilde
  214: "&Ouml;",		// O with diaeresis
  215: "&times;",		// Multiplication sign
  216: "&Oslash;",	// O with stroke
  217: "&Ugrave;",	// U with grave
  218: "&Uacute;",	// U with acute
  219: "&Ucirc;",		// U with circumflex
  220: "&Uuml;",		// U with diaeresis
  221: "&Yacute;",	// Y with acute
  222: "&THORN;",		// Thorn
  223: "&szlig;",		// Sharp s. Also known as ess-zed
  224: "&agrave;",	// a with grave
  225: "&aacute;",	// a with acute
  226: "&acirc;",		// a with circumflex
  227: "&atilde;",	// a with tilde
  228: "&auml;",		// a with diaeresis
  229: "&aring;",		// a with ring above
  230: "&aelig;",		// ae. Also known as ligature ae
  231: "&ccedil;",	// c with cedilla
  232: "&egrave;",	// e with grave
  233: "&eacute;",	// e with acute
  234: "&ecirc;",		// e with circumflex
  235: "&euml;",		// e with diaeresis
  236: "&igrave;",	// i with grave
  237: "&iacute;",	// i with acute
  238: "&icirc;",		// i with circumflex
  239: "&iuml;",		// i with diaeresis
  240: "&eth;",		// eth
  241: "&ntilde;",	// n with tilde
  242: "&ograve;",	// o with grave
  243: "&oacute;",	// o with acute
  244: "&ocirc;",		// o with circumflex
  245: "&otilde;",	// o with tilde
  246: "&ouml;",		// o with diaeresis
  247: "&divide;",	// Division sign
  248: "&oslash;",	// o with stroke. Also known as o with slash
  249: "&ugrave;",	// u with grave
  250: "&uacute;",	// u with acute
  251: "&ucirc;",		// u with circumflex
  252: "&uuml;",		// u with diaeresis
  253: "&yacute;",	// y with acute
  254: "&thorn;",		// thorn
  255: "&yuml;",		// y with diaeresis
  264: "&#264;",		// Latin capital letter C with circumflex
  265: "&#265;",		// Latin small letter c with circumflex
  338: "&OElig;",		// Latin capital ligature OE
  339: "&oelig;",		// Latin small ligature oe
  352: "&Scaron;",	// Latin capital letter S with caron
  353: "&scaron;",	// Latin small letter s with caron
  372: "&#372;",		// Latin capital letter W with circumflex
  373: "&#373;",		// Latin small letter w with circumflex
  374: "&#374;",		// Latin capital letter Y with circumflex
  375: "&#375;",		// Latin small letter y with circumflex
  376: "&Yuml;",		// Latin capital letter Y with diaeresis
  402: "&fnof;",		// Latin small f with hook, function, florin
  710: "&circ;",		// Modifier letter circumflex accent
  732: "&tilde;",		// Small tilde
  913: "&Alpha;",		// Alpha
  914: "&Beta;",		// Beta
  915: "&Gamma;",		// Gamma
  916: "&Delta;",		// Delta
  917: "&Epsilon;",	// Epsilon
  918: "&Zeta;",		// Zeta
  919: "&Eta;",		// Eta
  920: "&Theta;",		// Theta
  921: "&Iota;",		// Iota
  922: "&Kappa;",		// Kappa
  923: "&Lambda;",	// Lambda
  924: "&Mu;",		// Mu
  925: "&Nu;",		// Nu
  926: "&Xi;",		// Xi
  927: "&Omicron;",	// Omicron
  928: "&Pi;",		// Pi
  929: "&Rho;",		// Rho
  931: "&Sigma;",		// Sigma
  932: "&Tau;",		// Tau
  933: "&Upsilon;",	// Upsilon
  934: "&Phi;",		// Phi
  935: "&Chi;",		// Chi
  936: "&Psi;",		// Psi
  937: "&Omega;",		// Omega
  945: "&alpha;",		// alpha
  946: "&beta;",		// beta
  947: "&gamma;",		// gamma
  948: "&delta;",		// delta
  949: "&epsilon;",	// epsilon
  950: "&zeta;",		// zeta
  951: "&eta;",		// eta
  952: "&theta;",		// theta
  953: "&iota;",		// iota
  954: "&kappa;",		// kappa
  955: "&lambda;",	// lambda
  956: "&mu;",		// mu
  957: "&nu;",		// nu
  958: "&xi;",		// xi
  959: "&omicron;",	// omicron
  960: "&pi;",		// pi
  961: "&rho;",		// rho
  962: "&sigmaf;",	// sigmaf
  963: "&sigma;",		// sigma
  964: "&tau;",		// tau
  965: "&upsilon;",	// upsilon
  966: "&phi;",		// phi
  967: "&chi;",		// chi
  968: "&psi;",		// psi
  969: "&omega;",		// omega
  977: "&thetasym;",	// Theta symbol
  978: "&upsih;",		// Greek upsilon with hook symbol
  982: "&piv;",		// Pi symbol
  8194: "&ensp;",		// En space
  8195: "&emsp;",		// Em space
  8201: "&thinsp;",	// Thin space
  8204: "&zwnj;",		// Zero width non-joiner
  8205: "&zwj;",		// Zero width joiner
  8206: "&lrm;",		// Left-to-right mark
  8207: "&rlm;",		// Right-to-left mark
  8211: "&ndash;",	// En dash
  8212: "&mdash;",	// Em dash
  8216: "&lsquo;",	// Left single quotation mark
  8217: "&rsquo;",	// Right single quotation mark
  8218: "&sbquo;",	// Single low-9 quotation mark
  8220: "&ldquo;",	// Left double quotation mark
  8221: "&rdquo;",	// Right double quotation mark
  8222: "&bdquo;",	// Double low-9 quotation mark
  8224: "&dagger;",	// Dagger
  8225: "&Dagger;",	// Double dagger
  8226: "&bull;",		// Bullet
  8230: "&hellip;",	// Horizontal ellipsis
  8240: "&permil;",	// Per mille sign
  8242: "&prime;",	// Prime
  8243: "&Prime;",	// Double Prime
  8249: "&lsaquo;",	// Single left-pointing angle quotation
  8250: "&rsaquo;",	// Single right-pointing angle quotation
  8254: "&oline;",	// Overline
  8260: "&frasl;",	// Fraction Slash
  8364: "&euro;",		// Euro sign
  8472: "&weierp;",	// Script capital
  8465: "&image;",	// Blackletter capital I
  8476: "&real;",		// Blackletter capital R
  8482: "&trade;",	// Trade mark sign
  8501: "&alefsym;",	// Alef symbol
  8592: "&larr;",		// Leftward arrow
  8593: "&uarr;",		// Upward arrow
  8594: "&rarr;",		// Rightward arrow
  8595: "&darr;",		// Downward arrow
  8596: "&harr;",		// Left right arrow
  8629: "&crarr;",	// Downward arrow with corner leftward. Also known as carriage return
  8656: "&lArr;",		// Leftward double arrow. ISO 10646 does not say that lArr is the same as the 'is implied by' arrow but also does not have any other character for that function. So ? lArr can be used for 'is implied by' as ISOtech suggests
  8657: "&uArr;",		// Upward double arrow
  8658: "&rArr;",		// Rightward double arrow. ISO 10646 does not say this is the 'implies' character but does not have another character with this function so ? rArr can be used for 'implies' as ISOtech suggests
  8659: "&dArr;",		// Downward double arrow
  8660: "&hArr;",		// Left-right double arrow
  // Mathematical Operators
  8704: "&forall;",	// For all
  8706: "&part;",		// Partial differential
  8707: "&exist;",	// There exists
  8709: "&empty;",	// Empty set. Also known as null set and diameter
  8711: "&nabla;",	// Nabla. Also known as backward difference
  8712: "&isin;",		// Element of
  8713: "&notin;",	// Not an element of
  8715: "&ni;",		// Contains as member
  8719: "&prod;",		// N-ary product. Also known as product sign. Prod is not the same character as U+03A0 'greek capital letter pi' though the same glyph might be used for both
  8721: "&sum;",		// N-ary summation. Sum is not the same character as U+03A3 'greek capital letter sigma' though the same glyph might be used for both
  8722: "&minus;",	// Minus sign
  8727: "&lowast;",	// Asterisk operator
  8729: "&#8729;",	// Bullet operator
  8730: "&radic;",	// Square root. Also known as radical sign
  8733: "&prop;",		// Proportional to
  8734: "&infin;",	// Infinity
  8736: "&ang;",		// Angle
  8743: "&and;",		// Logical and. Also known as wedge
  8744: "&or;",		// Logical or. Also known as vee
  8745: "&cap;",		// Intersection. Also known as cap
  8746: "&cup;",		// Union. Also known as cup
  8747: "&int;",		// Integral
  8756: "&there4;",	// Therefore
  8764: "&sim;",		// tilde operator. Also known as varies with and similar to. The tilde operator is not the same character as the tilde, U+007E, although the same glyph might be used to represent both
  8773: "&cong;",		// Approximately equal to
  8776: "&asymp;",	// Almost equal to. Also known as asymptotic to
  8800: "&ne;",		// Not equal to
  8801: "&equiv;",	// Identical to
  8804: "&le;",		// Less-than or equal to
  8805: "&ge;",		// Greater-than or equal to
  8834: "&sub;",		// Subset of
  8835: "&sup;",		// Superset of. Note that nsup, 'not a superset of, U+2283' is not covered by the Symbol font encoding and is not included.
  8836: "&nsub;",		// Not a subset of
  8838: "&sube;",		// Subset of or equal to
  8839: "&supe;",		// Superset of or equal to
  8853: "&oplus;",	// Circled plus. Also known as direct sum
  8855: "&otimes;",	// Circled times. Also known as vector product
  8869: "&perp;",		// Up tack. Also known as orthogonal to and perpendicular
  8901: "&sdot;",		// Dot operator. The dot operator is not the same character as U+00B7 middle dot
  // Miscellaneous Technical
  8968: "&lceil;",	// Left ceiling. Also known as an APL upstile
  8969: "&rceil;",	// Right ceiling
  8970: "&lfloor;",	// left floor. Also known as APL downstile
  8971: "&rfloor;",	// Right floor
  9001: "&lang;",		// Left-pointing angle bracket. Also known as bra. Lang is not the same character as U+003C 'less than'or U+2039 'single left-pointing angle quotation mark'
  9002: "&rang;",		// Right-pointing angle bracket. Also known as ket. Rang is not the same character as U+003E 'greater than' or U+203A 'single right-pointing angle quotation mark'
  // Geometric Shapes
  9642: "&#9642;",	// Black small square
  9643: "&#9643;",	// White small square
  9674: "&loz;",		// Lozenge
  // Miscellaneous Symbols
  9702: "&#9702;",	// White bullet
  9824: "&spades;",	// Black (filled) spade suit
  9827: "&clubs;",	// Black (filled) club suit. Also known as shamrock
  9829: "&hearts;",	// Black (filled) heart suit. Also known as shamrock
  9830: "&diams;"   // Black (filled) diamond suit
}

;/**
 * A Javascript object to encode and/or decode html characters
 * @Author R Reid
 * source: http://www.strictly-software.com/htmlencode
 * Licence: GPL
 * 
 * Revision:
 *  2011-07-14, Jacques-Yves Bleau: 
 *       - fixed conversion error with capitalized accentuated characters
 *       + converted arr1 and arr2 to object property to remove redundancy
 */

Encoder = {

	// When encoding do we convert characters into html or numerical entities
	EncodeType : "entity",  // entity OR numerical

	isEmpty : function(val){
		if(val){
			return ((val===null) || val.length==0 || /^\s+$/.test(val));
		}else{
			return true;
		}
	},
	arr1: new Array('&nbsp;','&iexcl;','&cent;','&pound;','&curren;','&yen;','&brvbar;','&sect;','&uml;','&copy;','&ordf;','&laquo;','&not;','&shy;','&reg;','&macr;','&deg;','&plusmn;','&sup2;','&sup3;','&acute;','&micro;','&para;','&middot;','&cedil;','&sup1;','&ordm;','&raquo;','&frac14;','&frac12;','&frac34;','&iquest;','&Agrave;','&Aacute;','&Acirc;','&Atilde;','&Auml;','&Aring;','&Aelig;','&Ccedil;','&Egrave;','&Eacute;','&Ecirc;','&Euml;','&Igrave;','&Iacute;','&Icirc;','&Iuml;','&ETH;','&Ntilde;','&Ograve;','&Oacute;','&Ocirc;','&Otilde;','&Ouml;','&times;','&Oslash;','&Ugrave;','&Uacute;','&Ucirc;','&Uuml;','&Yacute;','&THORN;','&szlig;','&agrave;','&aacute;','&acirc;','&atilde;','&auml;','&aring;','&aelig;','&ccedil;','&egrave;','&eacute;','&ecirc;','&euml;','&igrave;','&iacute;','&icirc;','&iuml;','&eth;','&ntilde;','&ograve;','&oacute;','&ocirc;','&otilde;','&ouml;','&divide;','&Oslash;','&ugrave;','&uacute;','&ucirc;','&uuml;','&yacute;','&thorn;','&yuml;','&quot;','&amp;','&lt;','&gt;','&oelig;','&oelig;','&scaron;','&scaron;','&yuml;','&circ;','&tilde;','&ensp;','&emsp;','&thinsp;','&zwnj;','&zwj;','&lrm;','&rlm;','&ndash;','&mdash;','&lsquo;','&rsquo;','&sbquo;','&ldquo;','&rdquo;','&bdquo;','&dagger;','&dagger;','&permil;','&lsaquo;','&rsaquo;','&euro;','&fnof;','&alpha;','&beta;','&gamma;','&delta;','&epsilon;','&zeta;','&eta;','&theta;','&iota;','&kappa;','&lambda;','&mu;','&nu;','&xi;','&omicron;','&pi;','&rho;','&sigma;','&tau;','&upsilon;','&phi;','&chi;','&psi;','&omega;','&alpha;','&beta;','&gamma;','&delta;','&epsilon;','&zeta;','&eta;','&theta;','&iota;','&kappa;','&lambda;','&mu;','&nu;','&xi;','&omicron;','&pi;','&rho;','&sigmaf;','&sigma;','&tau;','&upsilon;','&phi;','&chi;','&psi;','&omega;','&thetasym;','&upsih;','&piv;','&bull;','&hellip;','&prime;','&prime;','&oline;','&frasl;','&weierp;','&image;','&real;','&trade;','&alefsym;','&larr;','&uarr;','&rarr;','&darr;','&harr;','&crarr;','&larr;','&uarr;','&rarr;','&darr;','&harr;','&forall;','&part;','&exist;','&empty;','&nabla;','&isin;','&notin;','&ni;','&prod;','&sum;','&minus;','&lowast;','&radic;','&prop;','&infin;','&ang;','&and;','&or;','&cap;','&cup;','&int;','&there4;','&sim;','&cong;','&asymp;','&ne;','&equiv;','&le;','&ge;','&sub;','&sup;','&nsub;','&sube;','&supe;','&oplus;','&otimes;','&perp;','&sdot;','&lceil;','&rceil;','&lfloor;','&rfloor;','&lang;','&rang;','&loz;','&spades;','&clubs;','&hearts;','&diams;'),
	arr2: new Array('&#160;','&#161;','&#162;','&#163;','&#164;','&#165;','&#166;','&#167;','&#168;','&#169;','&#170;','&#171;','&#172;','&#173;','&#174;','&#175;','&#176;','&#177;','&#178;','&#179;','&#180;','&#181;','&#182;','&#183;','&#184;','&#185;','&#186;','&#187;','&#188;','&#189;','&#190;','&#191;','&#192;','&#193;','&#194;','&#195;','&#196;','&#197;','&#198;','&#199;','&#200;','&#201;','&#202;','&#203;','&#204;','&#205;','&#206;','&#207;','&#208;','&#209;','&#210;','&#211;','&#212;','&#213;','&#214;','&#215;','&#216;','&#217;','&#218;','&#219;','&#220;','&#221;','&#222;','&#223;','&#224;','&#225;','&#226;','&#227;','&#228;','&#229;','&#230;','&#231;','&#232;','&#233;','&#234;','&#235;','&#236;','&#237;','&#238;','&#239;','&#240;','&#241;','&#242;','&#243;','&#244;','&#245;','&#246;','&#247;','&#248;','&#249;','&#250;','&#251;','&#252;','&#253;','&#254;','&#255;','&#34;','&#38;','&#60;','&#62;','&#338;','&#339;','&#352;','&#353;','&#376;','&#710;','&#732;','&#8194;','&#8195;','&#8201;','&#8204;','&#8205;','&#8206;','&#8207;','&#8211;','&#8212;','&#8216;','&#8217;','&#8218;','&#8220;','&#8221;','&#8222;','&#8224;','&#8225;','&#8240;','&#8249;','&#8250;','&#8364;','&#402;','&#913;','&#914;','&#915;','&#916;','&#917;','&#918;','&#919;','&#920;','&#921;','&#922;','&#923;','&#924;','&#925;','&#926;','&#927;','&#928;','&#929;','&#931;','&#932;','&#933;','&#934;','&#935;','&#936;','&#937;','&#945;','&#946;','&#947;','&#948;','&#949;','&#950;','&#951;','&#952;','&#953;','&#954;','&#955;','&#956;','&#957;','&#958;','&#959;','&#960;','&#961;','&#962;','&#963;','&#964;','&#965;','&#966;','&#967;','&#968;','&#969;','&#977;','&#978;','&#982;','&#8226;','&#8230;','&#8242;','&#8243;','&#8254;','&#8260;','&#8472;','&#8465;','&#8476;','&#8482;','&#8501;','&#8592;','&#8593;','&#8594;','&#8595;','&#8596;','&#8629;','&#8656;','&#8657;','&#8658;','&#8659;','&#8660;','&#8704;','&#8706;','&#8707;','&#8709;','&#8711;','&#8712;','&#8713;','&#8715;','&#8719;','&#8721;','&#8722;','&#8727;','&#8730;','&#8733;','&#8734;','&#8736;','&#8743;','&#8744;','&#8745;','&#8746;','&#8747;','&#8756;','&#8764;','&#8773;','&#8776;','&#8800;','&#8801;','&#8804;','&#8805;','&#8834;','&#8835;','&#8836;','&#8838;','&#8839;','&#8853;','&#8855;','&#8869;','&#8901;','&#8968;','&#8969;','&#8970;','&#8971;','&#9001;','&#9002;','&#9674;','&#9824;','&#9827;','&#9829;','&#9830;'),
		
	// Convert HTML entities into numerical entities
	HTML2Numerical : function(s){
		return this.swapArrayVals(s,this.arr1,this.arr2);
	},	

	// Convert Numerical entities into HTML entities
	NumericalToHTML : function(s){
		return this.swapArrayVals(s,this.arr2,this.arr1);
	},


	// Numerically encodes all unicode characters
	numEncode : function(s){
		
		if(this.isEmpty(s)) return "";

		var e = "";
		for (var i = 0; i < s.length; i++)
		{
			var c = s.charAt(i);
			if (c < " " || c > "~")
			{
				c = "&#" + c.charCodeAt() + ";";
			}
			e += c;
		}
		return e;
	},
	
	// HTML Decode numerical and HTML entities back to original values
	htmlDecode : function(s){

		var c,m,d = s;
		
		if(this.isEmpty(d)) return "";

		// convert HTML entites back to numerical entites first
		d = this.HTML2Numerical(d);
		
		// look for numerical entities &#34;
		arr=d.match(/&#[0-9]{1,5};/g);
		
		// if no matches found in string then skip
		if(arr!=null){
			for(var x=0;x<arr.length;x++){
				m = arr[x];
				c = m.substring(2,m.length-1); //get numeric part which is refernce to unicode character
				// if its a valid number we can decode
				if(c >= -32768 && c <= 65535){
					// decode every single match within string
					d = d.replace(m, String.fromCharCode(c));
				}else{
					d = d.replace(m, ""); //invalid so replace with nada
				}
			}			
		}

		return d;
	},		

	// encode an input string into either numerical or HTML entities
	htmlEncode : function(s,dbl){
			
		if(this.isEmpty(s)) return "";

		// do we allow double encoding? E.g will &amp; be turned into &amp;amp;
		dbl = dbl || false; //default to prevent double encoding
		
		// if allowing double encoding we do ampersands first
		if(dbl){
			if(this.EncodeType=="numerical"){
				s = s.replace(/&/g, "&#38;");
			}else{
				s = s.replace(/&/g, "&amp;");
			}
		}

		// convert the xss chars to numerical entities ' " < >
		s = this.XSSEncode(s,false);
		
		if(this.EncodeType=="numerical" || !dbl){
			// Now call function that will convert any HTML entities to numerical codes
			s = this.HTML2Numerical(s);
		}

		// Now encode all chars above 127 e.g unicode
		s = this.numEncode(s);

		// now we know anything that needs to be encoded has been converted to numerical entities we
		// can encode any ampersands & that are not part of encoded entities
		// to handle the fact that I need to do a negative check and handle multiple ampersands &&&
		// I am going to use a placeholder

		// if we don't want double encoded entities we ignore the & in existing entities
		if(!dbl){
			s = s.replace(/&#/g,"##AMPHASH##");
		
			if(this.EncodeType=="numerical"){
				s = s.replace(/&/g, "&#38;");
			}else{
				s = s.replace(/&/g, "&amp;");
			}

			s = s.replace(/##AMPHASH##/g,"&#");
		}
		
		// replace any malformed entities
		s = s.replace(/&#\d*([^\d;]|$)/g, "$1");

		if(!dbl){
			// safety check to correct any double encoded &amp;
			s = this.correctEncoding(s);
		}

		// now do we need to convert our numerical encoded string into entities
		if(this.EncodeType=="entity"){
			s = this.NumericalToHTML(s);
		}

		return s;					
	},

	// Encodes the basic 4 characters used to malform HTML in XSS hacks
	XSSEncode : function(s,en){
		if(!this.isEmpty(s)){
			en = en || true;
			// do we convert to numerical or html entity?
			if(en){
				s = s.replace(/\'/g,"&#39;"); //no HTML equivalent as &apos is not cross browser supported
				s = s.replace(/\"/g,"&quot;");
				s = s.replace(/</g,"&lt;");
				s = s.replace(/>/g,"&gt;");
			}else{
				s = s.replace(/\'/g,"&#39;"); //no HTML equivalent as &apos is not cross browser supported
				s = s.replace(/\"/g,"&#34;");
				s = s.replace(/</g,"&#60;");
				s = s.replace(/>/g,"&#62;");
			}
			return s;
		}else{
			return "";
		}
	},

	// returns true if a string contains html or numerical encoded entities
	hasEncoded : function(s){
		if(/&#[0-9]{1,5};/g.test(s)){
			return true;
		}else if(/&[A-Z]{2,6};/gi.test(s)){
			return true;
		}else{
			return false;
		}
	},

	// will remove any unicode characters
	stripUnicode : function(s){
		return s.replace(/[^\x20-\x7E]/g,"");
		
	},

	// corrects any double encoded &amp; entities e.g &amp;amp;
	correctEncoding : function(s){
		return s.replace(/(&amp;)(amp;)+/,"$1");
	},


	// Function to loop through an array swaping each item with the value from another array e.g swap HTML entities with Numericals
	swapArrayVals : function(s,arr1,arr2){
		if(this.isEmpty(s)) return "";
		var re;
		if(arr1 && arr2){
			//ShowDebug("in swapArrayVals arr1.length = " + arr1.length + " arr2.length = " + arr2.length)
			// array lengths must match
			if(arr1.length == arr2.length){
				for(var x=0,i=arr1.length;x<i;x++){
					re = new RegExp(arr1[x], 'g');
					s = s.replace(re,arr2[x]); //swap arr1 item with matching item from arr2	
				}
			}
		}
		return s;
	},

	inArray : function( item, arr ) {
		for ( var i = 0, x = arr.length; i < x; i++ ){
			if ( arr[i] === item ){
				return i;
			}
		}
		return -1;
	}

};function mswordit(element) {
   var cleaned = element.cloneNode(true);
   cleaned = removeNodes(cleaned);
   /*
   var url = "msword.php?element=" + escape(cleaned.innerHTML);
   var msword = window.open(url,"msword","status=1,width=350,height=150");
   */
   document.getElementById('wordcontent').value = cleaned.innerHTML;
   document.msword.submit();
   return false;
}
function removeNodes(element) {
   for (var e=0;e<element.childNodes.length;e++) {
      var el = element.childNodes[e];
      switch (el.nodeName) {
      case 'A': {
         if (!el.href) element.removeChild(el);
      } break;
      case 'BUTTON': {
         element.removeChild(el);
      } break;
      case 'INPUT': {
         var p = document.createElement('P');
         var tn = document.createTextNode(el.value);
         p.appendChild(tn);
         element.replaceChild(p,el);
      } break;
      case 'TEXTAREA': {
         var p = document.createElement('P');
         var tn = document.createTextNode(el.value);
         p.appendChild(tn);
         element.replaceChild(p,el);
      } break;
      case 'IMG': {
         var tn = document.createTextNode(el.alt);
         element.replaceChild(tn,el);
      } break;
      case 'TABLE': {
         el.style.border = 1;
         el.style.borderColor = "#ccc";
         el.border = 1;
         el.bordercolor = "#ccc";
      } break;
      }
      if (el.childNodes.length > 0) {
         el = removeNodes(el);
      } 
   }
   return element;
}
;/*
    http://www.JSON.org/json2.js
    2008-11-19

    Public Domain.

    NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.

    See http://www.JSON.org/js.html

    This file creates a global JSON object containing two methods: stringify
    and parse.

        JSON.stringify(value, replacer, space)
            value       any JavaScript value, usually an object or array.

            replacer    an optional parameter that determines how object
                        values are stringified for objects. It can be a
                        function or an array of strings.

            space       an optional parameter that specifies the indentation
                        of nested structures. If it is omitted, the text will
                        be packed without extra whitespace. If it is a number,
                        it will specify the number of spaces to indent at each
                        level. If it is a string (such as '\t' or '&nbsp;'),
                        it contains the characters used to indent at each level.

            This method produces a JSON text from a JavaScript value.

            When an object value is found, if the object contains a toJSON
            method, its toJSON method will be called and the result will be
            stringified. A toJSON method does not serialize: it returns the
            value represented by the name/value pair that should be serialized,
            or undefined if nothing should be serialized. The toJSON method
            will be passed the key associated with the value, and this will be
            bound to the object holding the key.

            For example, this would serialize Dates as ISO strings.

                Date.prototype.toJSON = function (key) {
                    function f(n) {
                        // Format integers to have at least two digits.
                        return n < 10 ? '0' + n : n;
                    }

                    return this.getUTCFullYear()   + '-' +
                         f(this.getUTCMonth() + 1) + '-' +
                         f(this.getUTCDate())      + 'T' +
                         f(this.getUTCHours())     + ':' +
                         f(this.getUTCMinutes())   + ':' +
                         f(this.getUTCSeconds())   + 'Z';
                };

            You can provide an optional replacer method. It will be passed the
            key and value of each member, with this bound to the containing
            object. The value that is returned from your method will be
            serialized. If your method returns undefined, then the member will
            be excluded from the serialization.

            If the replacer parameter is an array of strings, then it will be
            used to select the members to be serialized. It filters the results
            such that only members with keys listed in the replacer array are
            stringified.

            Values that do not have JSON representations, such as undefined or
            functions, will not be serialized. Such values in objects will be
            dropped; in arrays they will be replaced with null. You can use
            a replacer function to replace those with JSON values.
            JSON.stringify(undefined) returns undefined.

            The optional space parameter produces a stringification of the
            value that is filled with line breaks and indentation to make it
            easier to read.

            If the space parameter is a non-empty string, then that string will
            be used for indentation. If the space parameter is a number, then
            the indentation will be that many spaces.

            Example:

            text = JSON.stringify(['e', {pluribus: 'unum'}]);
            // text is '["e",{"pluribus":"unum"}]'


            text = JSON.stringify(['e', {pluribus: 'unum'}], null, '\t');
            // text is '[\n\t"e",\n\t{\n\t\t"pluribus": "unum"\n\t}\n]'

            text = JSON.stringify([new Date()], function (key, value) {
                return this[key] instanceof Date ?
                    'Date(' + this[key] + ')' : value;
            });
            // text is '["Date(---current time---)"]'


        JSON.parse(text, reviver)
            This method parses a JSON text to produce an object or array.
            It can throw a SyntaxError exception.

            The optional reviver parameter is a function that can filter and
            transform the results. It receives each of the keys and values,
            and its return value is used instead of the original value.
            If it returns what it received, then the structure is not modified.
            If it returns undefined then the member is deleted.

            Example:

            // Parse the text. Values that look like ISO date strings will
            // be converted to Date objects.

            myData = JSON.parse(text, function (key, value) {
                var a;
                if (typeof value === 'string') {
                    a =
/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
                    if (a) {
                        return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4],
                            +a[5], +a[6]));
                    }
                }
                return value;
            });

            myData = JSON.parse('["Date(09/09/2001)"]', function (key, value) {
                var d;
                if (typeof value === 'string' &&
                        value.slice(0, 5) === 'Date(' &&
                        value.slice(-1) === ')') {
                    d = new Date(value.slice(5, -1));
                    if (d) {
                        return d;
                    }
                }
                return value;
            });


    This is a reference implementation. You are free to copy, modify, or
    redistribute.

    This code should be minified before deployment.
    See http://javascript.crockford.com/jsmin.html

    USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
    NOT CONTROL.
*/

/*jslint evil: true */

/*global JSON */

/*members "", "\b", "\t", "\n", "\f", "\r", "\"", JSON, "\\", apply,
    call, charCodeAt, getUTCDate, getUTCFullYear, getUTCHours,
    getUTCMinutes, getUTCMonth, getUTCSeconds, hasOwnProperty, join,
    lastIndex, length, parse, prototype, push, replace, slice, stringify,
    test, toJSON, toString, valueOf
*/

// Create a JSON object only if one does not already exist. We create the
// methods in a closure to avoid creating global variables.

if (!this.JSON) {
    JSON = {};
}
(function () {

    function f(n) {
        // Format integers to have at least two digits.
        return n < 10 ? '0' + n : n;
    }

    if (typeof Date.prototype.toJSON !== 'function') {

        Date.prototype.toJSON = function (key) {

            return this.getUTCFullYear()   + '-' +
                 f(this.getUTCMonth() + 1) + '-' +
                 f(this.getUTCDate())      + 'T' +
                 f(this.getUTCHours())     + ':' +
                 f(this.getUTCMinutes())   + ':' +
                 f(this.getUTCSeconds())   + 'Z';
        };

        String.prototype.toJSON =
        Number.prototype.toJSON =
        Boolean.prototype.toJSON = function (key) {
            return this.valueOf();
        };
    }

    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        gap,
        indent,
        meta = {    // table of character substitutions
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        },
        rep;


    function quote(string) {

// If the string contains no control characters, no quote characters, and no
// backslash characters, then we can safely slap some quotes around it.
// Otherwise we must also replace the offending characters with safe escape
// sequences.

        escapable.lastIndex = 0;
        return escapable.test(string) ?
            '"' + string.replace(escapable, function (a) {
                var c = meta[a];
                return typeof c === 'string' ? c :
                    '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
            }) + '"' :
            '"' + string + '"';
    }


    function str(key, holder) {

// Produce a string from holder[key].

        var i,          // The loop counter.
            k,          // The member key.
            v,          // The member value.
            length,
            mind = gap,
            partial,
            value = holder[key];

// If the value has a toJSON method, call it to obtain a replacement value.

        if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }

// If we were called with a replacer function, then call the replacer to
// obtain a replacement value.

        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        }

// What happens next depends on the value's type.

        switch (typeof value) {
        case 'string':
            return quote(value);

        case 'number':

// JSON numbers must be finite. Encode non-finite numbers as null.

            return isFinite(value) ? String(value) : 'null';

        case 'boolean':
        case 'null':

// If the value is a boolean or null, convert it to a string. Note:
// typeof null does not produce 'null'. The case is included here in
// the remote chance that this gets fixed someday.

            return String(value);

// If the type is 'object', we might be dealing with an object or an array or
// null.

        case 'object':

// Due to a specification blunder in ECMAScript, typeof null is 'object',
// so watch out for that case.

            if (!value) {
                return 'null';
            }

// Make an array to hold the partial results of stringifying this object value.

            gap += indent;
            partial = [];

// Is the value an array?

            if (Object.prototype.toString.apply(value) === '[object Array]') {

// The value is an array. Stringify every element. Use null as a placeholder
// for non-JSON values.

                length = value.length;
                for (i = 0; i < length; i += 1) {
                    partial[i] = str(i, value) || 'null';
                }

// Join all of the elements together, separated with commas, and wrap them in
// brackets.

                v = partial.length === 0 ? '[]' :
                    gap ? '[\n' + gap +
                            partial.join(',\n' + gap) + '\n' +
                                mind + ']' :
                          '[' + partial.join(',') + ']';
                gap = mind;
                return v;
            }

// If the replacer is an array, use it to select the members to be stringified.

            if (rep && typeof rep === 'object') {
                length = rep.length;
                for (i = 0; i < length; i += 1) {
                    k = rep[i];
                    if (typeof k === 'string') {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            } else {

// Otherwise, iterate through all of the keys in the object.

                for (k in value) {
                    if (Object.hasOwnProperty.call(value, k)) {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            }

// Join all of the member texts together, separated with commas,
// and wrap them in braces.

            v = partial.length === 0 ? '{}' :
                gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' +
                        mind + '}' : '{' + partial.join(',') + '}';
            gap = mind;
            return v;
        }
    }

// If the JSON object does not yet have a stringify method, give it one.

    if (typeof JSON.stringify !== 'function') {
        JSON.stringify = function (value, replacer, space) {

// The stringify method takes a value and an optional replacer, and an optional
// space parameter, and returns a JSON text. The replacer can be a function
// that can replace values, or an array of strings that will select the keys.
// A default replacer method can be provided. Use of the space parameter can
// produce text that is more easily readable.

            var i;
            gap = '';
            indent = '';

// If the space parameter is a number, make an indent string containing that
// many spaces.

            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                }

// If the space parameter is a string, it will be used as the indent string.

            } else if (typeof space === 'string') {
                indent = space;
            }

// If there is a replacer, it must be a function or an array.
// Otherwise, throw an error.

            rep = replacer;
            if (replacer && typeof replacer !== 'function' &&
                    (typeof replacer !== 'object' ||
                     typeof replacer.length !== 'number')) {
                throw new Error('JSON.stringify');
            }

// Make a fake root object containing our value under the key of ''.
// Return the result of stringifying the value.

            return str('', {'': value});
        };
    }


// If the JSON object does not yet have a parse method, give it one.

    if (typeof JSON.parse !== 'function') {
        JSON.parse = function (text, reviver) {

// The parse method takes a text and an optional reviver function, and returns
// a JavaScript value if the text is a valid JSON text.

            var j;

            function walk(holder, key) {

// The walk method is used to recursively walk the resulting structure so
// that modifications can be made.

                var k, v, value = holder[key];
                if (value && typeof value === 'object') {
                    for (k in value) {
                        if (Object.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }


// Parsing happens in four stages. In the first stage, we replace certain
// Unicode characters with escape sequences. JavaScript handles many characters
// incorrectly, either silently deleting them, or treating them as line endings.

            cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function (a) {
                    return '\\u' +
                        ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                });
            }

// In the second stage, we run the text against regular expressions that look
// for non-JSON patterns. We are especially concerned with '()' and 'new'
// because they can cause invocation, and '=' because it can cause mutation.
// But just to be safe, we want to reject all unexpected forms.

// We split the second stage into 4 regexp operations in order to work around
// crippling inefficiencies in IE's and Safari's regexp engines. First we
// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
// replace all simple value tokens with ']' characters. Third, we delete all
// open brackets that follow a colon or comma or that begin the text. Finally,
// we look to see that the remaining characters are only whitespace or ']' or
// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

            if (/^[\],:{}\s]*$/.
test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').
replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

// In the third stage we use the eval function to compile the text into a
// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
// in JavaScript: it can begin a block or an object literal. We wrap the text
// in parens to eliminate the ambiguity.

                j = eval('(' + text + ')');

// In the optional fourth stage, we recursively walk the new structure, passing
// each name/value pair to a reviver function for possible transformation.

                return typeof reviver === 'function' ?
                    walk({'': j}, '') : j;
            }

// If the text is not JSON parseable, then a SyntaxError is thrown.

            throw new SyntaxError('JSON.parse');
        };
    }
})();
;var currentday;
var currentmonth;
var currentyear;
var xmlhttp;
var events = new Array();
function loadCalendar(smonth,syear,type) {
   var d = new Date();
   if (smonth!=null || syear!=null) {
      if (smonth!=null) {
         if (syear) syear = (smonth<0)?syear-1:(smonth>11)?syear+1:syear;
         smonth = smonth % 12;
         if (smonth<0) smonth = 12 + smonth;
         d.setUTCMonth(smonth);   
         d.setUTCFullYear(syear);
      }
   }
   if(smonth==null) smonth = d.getUTCMonth();
   syear = d.getUTCFullYear();
   var table,node,tn;
   var cal = document.getElementById('calendar');
   while(cal.childNodes.length>0) cal.removeChild(cal.childNodes[0]);
   type = (type==null)?"":type;
   switch (type.toLowerCase()) {
   default: {
      table = getCalendarTable(smonth,syear,type);
      cal.appendChild(table);
      node = document.createElement('BUTTON');
      node.id = 'cancel';
      node.className = 'dbutton';
      node.onclick = function() {hidedialog();};
      tn = document.createTextNode('Cancel');
      node.appendChild(tn);
      cal.appendChild(node);
      node = document.createElement('BUTTON');
      node.id = 'usedate';
      node.className = 'dbutton';
      node.onclick = function() {useDate();};
      tn = document.createTextNode('OK');
      node.appendChild(tn);
      cal.appendChild(node);
   }break;
   }
   return true;
}
function getCalendarTable(smonth,syear,type) {
   var d = new Date();
   var today = d.getUTCDate();
   d.setUTCDate(1);
   var thismonth = d.getUTCMonth();
   var thisyear = d.getUTCFullYear();
   if (smonth!=null || syear!=null) {
      if (smonth!=null) {
         d.setUTCMonth(smonth);
         if (syear) syear = (smonth<0)?syear-1:(smonth>11)?syear+1:syear;
      }
      if (syear!=null) d.setUTCFullYear(syear);
   }
   currentday = d.getUTCDate();
   currentmonth = d.getUTCMonth();
   currentyear = d.getUTCFullYear();
   var monnum = currentmonth;
   var mfirst = new Date();
   mfirst.setTime(d.getTime());
   d.setUTCMonth(currentmonth+1);
   var time = d.getTime();
   var time = time - timeMillis(1,'day');
   d.setTime(time);
   var mlast = new Date();
   mlast.setTime(d.getTime());
   var days = new Array('M','T','W','T','F','S','S');
   var months = new Array( 'Jan','Feb','Mar',
                           'Apr','May','Jun',
                           'Jul','Aug','Sep',
                           'Oct','Nov','Dec');
   var node,table,thead,tr,th,tbody,td,tn,button;
   //var monnum = d.getUTCMonth();
   var month = months[monnum];
   var year = d.getUTCFullYear();
   var firstday = mfirst.getUTCDay();
   firstday = (firstday==0)?6:firstday-1;
   var daysinmonth = mlast.getUTCDate();
   table = document.createElement('TABLE');
   table.id = 'calendartab';
   table.className = 'options';
   thead = document.createElement('THEAD');
   tr = document.createElement('TR');
   // back a year
   th = document.createElement('TH');
   th.className = 'day';
   node = document.createElement('BUTTON');
   node.onclick = function () {
      //loadCalendar(monnum,year-1,type);
      var cal = document.getElementById('calendartab');
      var par = cal.parentNode;
      par.replaceChild(getCalendarTable(monnum,year-1,type),cal);
      return true;
   };
   tn = document.createTextNode("<Y");
   node.appendChild(tn);
   th.appendChild(node);
   tr.appendChild(th);
   // back a month
   th = document.createElement('TH');
   th.className = 'day';
   node = document.createElement('BUTTON');
   node.onclick = function () {
      //loadCalendar(monnum-1,year,type);
      var cal = document.getElementById('calendartab');
      var par = cal.parentNode;
      par.replaceChild(getCalendarTable(monnum-1,year,type),cal);
      return true;
   };
   tn = document.createTextNode("<M");
   node.appendChild(tn);
   th.appendChild(node);
   tr.appendChild(th);
   th = document.createElement('TH');
   th.className = 'day';
   node = document.createElement('H3');
   tn = document.createTextNode(month + ' ' + year);
   node.appendChild(tn);
   th.appendChild(node);
   th.colSpan = 3;
   tr.appendChild(th);
   // forward a month
   th = document.createElement('TH');
   th.className = 'day';
   node = document.createElement('BUTTON');
   node.onclick = function () {
      //loadCalendar(monnum+1,year,type);
      var cal = document.getElementById('calendartab');
      var par = cal.parentNode;
      par.replaceChild(getCalendarTable(monnum+1,year,type),cal);
      return true;
   };
   tn = document.createTextNode("M>");
   node.appendChild(tn);
   th.appendChild(node);
   tr.appendChild(th);
   // forward a year
   th = document.createElement('TH');
   th.className = 'day';
   node = document.createElement('BUTTON');
   node.onclick = function () {
      //loadCalendar(monnum,year+1,type);
      var cal = document.getElementById('calendartab');
      var par = cal.parentNode;
      par.replaceChild(getCalendarTable(monnum,year+1,type),cal);
      return true;
   };
   tn = document.createTextNode("Y>");
   node.appendChild(tn);
   th.appendChild(node);
   tr.appendChild(th);
   thead.appendChild(tr);
   tr = document.createElement('TR');
   var x,y;
   for(x=0;x<=6;x++) {
      th = document.createElement('TH');
      th.className = 'day';
      tn = document.createTextNode(days[x]);
      th.appendChild(tn);
      tr.appendChild(th);
   }
   thead.appendChild(tr);
   table.appendChild(thead);
   tbody = document.createElement('TBODY');
   var daynum = 1;
   for (y=0;y<=5;y++) {
      if (daynum<=daysinmonth) {
         tr = document.createElement('TR');
         for (x=0;x<=6;x++) {
            td = document.createElement('TD');
            if (monnum == thismonth && year == thisyear && daynum == today) td.className = 'today';
            else td.className = 'day';
            if (!((y==0 && x<firstday)||(daynum>daysinmonth))) {
               tn = document.createTextNode(daynum);
               td.appendChild(tn);
               td.id = 'day_'+daynum;
               td.onclick = function() {changeDay(this.id);};
               daynum++;
            } else {
               td.className = 'autodata';
            }
            tr.appendChild(td);
         }
         tbody.appendChild(tr);
      }
   }
   table.appendChild(tbody);
   return table;
}
function timeMillis(x,t) {
   var millis;
   t = t.replace(/s$/,"");
   switch(t.toLowerCase()) {
   case 'second': millis = x*1000;break;
   case 'minute': millis = x*60*1000;break;
   case 'hour':   millis = x*60*60*1000;break;
   case 'day':    millis = x*24*60*60*1000;break;
   case 'week':   millis = x*7*24*60*60*1000;break;
   case 'year':   millis = x*365*24*60*60*1000;break;
   }
   return millis;
}
function changeDay(id) {
    var oldDayNode = document.getElementById('day_'+currentday);
    if (oldDayNode.className != 'today') {
        oldDayNode.className = 'day';
    }
    var newDayNode = document.getElementById(id);
    if (newDayNode.className != 'today') {
        newDayNode.className = 'select';
    }
    id = id.substring(4);
    currentday = parseInt(id);
    return false;
}
function loadXMLEvents(url) {
   xmlhttp=null;
   //url.replace("/csv/","xml");
   if (window.XMLHttpRequest) {// code for all new browsers
      xmlhttp=new XMLHttpRequest();
   } else if (window.ActiveXObject) {// code for IE5 and IE6
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
   }
   if (xmlhttp!=null) {
      xmlhttp.onreadystatechange=readEventsFile;
      xmlhttp.open("GET",url,true);
      xmlhttp.send(null);
   } 
}
function readEventsFile()  {
   var eventdate,y,m,d,v;
   var xml,i,j,parent,element,credit,lines,line,vals,filename,headings,heading;
   if (xmlhttp.readyState == 4) {
      var response = xmlhttp.responseText;
      lines = response.split(/\n/);
      headings = getCSVValues(lines[0]);
      for (i=1;i<lines.length;i++) {
         vals = getCSVValues(lines[i]);
         d = vals[0];
         eventdate = d.split(/\//);
         y = eventdate[2];
         m = eventdate[1];
         d = eventdate[0];
         eventname = vals[1];
         events[filename] = new Array();
         for(j=2;j<vals.length;j++) {
            v = vals[j];
            v = v.toString();
            heading = headings[j];
            if (v.length>0) v.replace(/^\s+$/,'');
            if (!events[y]) events[y] = new Array();
            if (!events[y][m]) events[y][m] = new Array();
            if (!events[y][m][d]) events[y][m][d] = new Array();
            if (!events[y][m][d][eventname]) events[y][m][d][eventname] = new Array();
            if (!events[y][m][d][eventname][heading]) events[y][m][d][eventname][heading] = new Array();
            if (vals[j].length > 0) events[y][m][d][eventname][heading] = v;
         }
      }
      loadCalendar(null,null,'events');
   }
   return true;
}
function eventList(month,year) {
   var table,tbody,thead,tr,th,td,node,tn,a,d,m,y,e,h,v,dayevents,monthevents,thisevent;
   d = new Date();
   if (month==null) month = d.getUTCMonth();
   if (year==null) year = d.getUTCFullYear();
   month = (month*1) + 1;
   if (month < 10) month = "0"+month;
   if (events[year] && events[year][month]) {
      monthevents = events[year][month];
      table = document.createElement('TABLE');
      tbody = document.createElement('TBODY');
      for (d in monthevents) {
         dayevents = monthevents[d];
         document.getElementById(d*1).style.borderColor = '#f00';
         for (e in dayevents) {
            thisevent = dayevents[e];
            if (!thead) {
               thead = document.createElement('THEAD');
               tr = document.createElement('TR');
               th = document.createElement('TH');
               th.className = 'top';
               tn = document.createTextNode('Date');
               th.appendChild(tn);
               tr.appendChild(th);
               th = document.createElement('TH');
               th.className = 'top';
               tn = document.createTextNode('Event');
               th.appendChild(tn);
               tr.appendChild(th);
               for(h in thisevent) {
                  th = document.createElement('TH');
                  th.className = 'top';
                  tn = document.createTextNode(h);
                  th.appendChild(tn);
                  tr.appendChild(th);
               }
               thead.appendChild(tr);
            }
            tr = document.createElement('TR');
            td = document.createElement('TD');
            td.className = 'lined';
            tn = document.createTextNode(d+'/'+month+'/'+year);
            td.appendChild(tn);
            tr.appendChild(td);
            td = document.createElement('TD');
            td.className = 'lined';
            tn = document.createTextNode(e);
            td.appendChild(tn);
            tr.appendChild(td);
            for(h in thisevent) {
               v = thisevent[h];
               td = document.createElement('TD');
               td.className = 'lined';
               if (v.match('http')||v.match('mailto')) {
                  a = document.createElement('A');
                  a.href = v;
                  a.target = 'new';
                  tn = document.createTextNode(v);
                  a.appendChild(tn);
                  td.appendChild(a);
               } else {
                  v = replaceHTMLEntities(v);
                  v = v.replace(/EMPTY/,"");
                  tn = document.createTextNode(v);
                  td.appendChild(tn);
               }
               tr.appendChild(td);
            }
            tbody.appendChild(tr);
         }  
      }
      if (thead) table.appendChild(thead);
      if (tbody) table.appendChild(tbody);
      return table;
   } else {
      return null;
   }
}
function getCSVValues(string) {
   /*
      JavaScript push will overwrite elements assigned as null. 
      In order to deal with empty ( ..",,".. ) values in the CSV file you need 
      to assign them with a specific value and replace it later hence EMPTY
   */
   var elements = new Array();
   var bits,s;
   if (string.match(/\"/)) {
      bits = string.split('"');
      for (var i=0;i<bits.length;i++) {
         bit = bits[i];
         bit = bit.replace(/^[\s+]$/,"");
         if (i%2==1) {
            elements.push(bit);
         } else if (bit != ',' && bit.length>0) {
            s = bit;
            s = s.replace(/^\,/,"");
            s = s.replace(/\,$/,"");
            if (s.match(/\,/)) elements.concat(s.split(/\,/));
            else if (!s.length) elements.push("EMPTY");
            else elements.push(s);
         }
      }
   } else {
      elements = string.split(/\,/);
   }
   return elements;
}
function replaceHTMLEntities(val) {
   val=val.replace(/&/g,'\&');
   val=val.replace(/"/g,'\"');
   val=val.replace(/'/g,'\'');
   val=val.replace(/£/g,'\£');
   return val;
}
function addslashes(str) {
   str=str.replace(/\'/g,'\\\'');
   str=str.replace(/\"/g,'\\"');
   str=str.replace(/\\/g,'\\\\');
   str=str.replace(/\0/g,'\\0');
   return str;
}
function pausecomp(millis) {
   var date = new Date();
   var curDate = null;
   do { curDate = new Date(); }
   while(curDate-date < millis);
} 
function getObjectClass(obj) {  
   if (obj && obj.constructor && obj.constructor.toString) {  
      var arr = obj.constructor.toString().match(  
         /function\s*(\w+)/);  
      if (arr && arr.length == 2) {  
         return arr[1];  
      }  
   }  
   return undefined;  
}
  
function s2_padnum(number, length) {   
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }   
    return str;
}

function s2_readdate(dstring) {
   var mats,dobj,d,m,y;
   mats = dstring.match(/(\d{2})\/(\d{2})\/(\d{4})/);
   if (mats && mats.length >= 4) {
      d = mats[1]*1;
      m = mats[2]*1;
      m -= 1;
      y = mats[3]*1;
      dobj = new Date(y,m,d);
   }
   return dobj;
}

Date.prototype.addBusDays = function(dd) {
   var wks = Math.floor(dd/5);
   //var dys = dd.mod(5);
   var dys = dd % 5;
   var dy = this.getDay();
   if (dy === 6 && dys > -1) {
      if (dys === 0) {dys-=2; dy+=2;}
      dys++; dy -= 6;
   }
   if (dy === 0 && dys < 1) {
      if (dys === 0) {dys+=2; dy-=2;}
      dys--; dy += 6;
   }
   if (dy + dys > 5) dys += 2;
   if (dy + dys < 1) dys -= 2;
   this.setDate(this.getDate()+wks*7+dys);
};/*
  proj4js.js -- Javascript reprojection library. 
  
  Authors:      Mike Adair madairATdmsolutions.ca
                Richard Greenwood richATgreenwoodmap.com
                Didier Richard didier.richardATign.fr
                Stephen Irons
  License:      LGPL as per: http://www.gnu.org/copyleft/lesser.html 
                Note: This program is an almost direct port of the C library
                Proj4.
*/
/* ======================================================================
    proj4js.js
   ====================================================================== */

/*
Author:       Mike Adair madairATdmsolutions.ca
              Richard Greenwood rich@greenwoodmap.com
License:      LGPL as per: http://www.gnu.org/copyleft/lesser.html

$Id: Proj.js 2956 2007-07-09 12:17:52Z steven $
*/

/**
 * Namespace: Proj4js
 *
 * Proj4js is a JavaScript library to transform point coordinates from one 
 * coordinate system to another, including datum transformations.
 *
 * This library is a port of both the Proj.4 and GCTCP C libraries to JavaScript. 
 * Enabling these transformations in the browser allows geographic data stored 
 * in different projections to be combined in browser-based web mapping 
 * applications.
 * 
 * Proj4js must have access to coordinate system initialization strings (which
 * are the same as for PROJ.4 command line).  Thes can be included in your 
 * application using a <script> tag or Proj4js can load CS initialization 
 * strings from a local directory or a web service such as spatialreference.org.
 *
 * Similarly, Proj4js must have access to projection transform code.  These can
 * be included individually using a <script> tag in your page, built into a 
 * custom build of Proj4js or loaded dynamically at run-time.  Using the
 * -combined and -compressed versions of Proj4js includes all projection class
 * code by default.
 *
 * Note that dynamic loading of defs and code happens ascynchrously, check the
 * Proj.readyToUse flag before using the Proj object.  If the defs and code
 * required by your application are loaded through script tags, dynamic loading
 * is not required and the Proj object will be readyToUse on return from the 
 * constructor.
 * 
 * All coordinates are handled as points which have a .x and a .y property
 * which will be modified in place.
 *
 * Override Proj4js.reportError for output of alerts and warnings.
 *
 * See http://trac.osgeo.org/proj4js/wiki/UserGuide for full details.
*/

/**
 * Global namespace object for Proj4js library
 */
Proj4js = {

    /**
     * Property: defaultDatum
     * The datum to use when no others a specified
     */
    defaultDatum: 'WGS84',                  //default datum

    /** 
    * Method: transform(source, dest, point)
    * Transform a point coordinate from one map projection to another.  This is
    * really the only public method you should need to use.
    *
    * Parameters:
    * source - {Proj4js.Proj} source map projection for the transformation
    * dest - {Proj4js.Proj} destination map projection for the transformation
    * point - {Object} point to transform, may be geodetic (long, lat) or
    *     projected Cartesian (x,y), but should always have x,y properties.
    */
    transform: function(source, dest, point) {
        if (!source.readyToUse) {
            this.reportError("Proj4js initialization for:"+source.srsCode+" not yet complete");
            return point;
        }
        if (!dest.readyToUse) {
            this.reportError("Proj4js initialization for:"+dest.srsCode+" not yet complete");
            return point;
        }
        
        // Workaround for Spherical Mercator
        if ((source.srsProjNumber =="900913" && dest.datumCode != "WGS84" && !dest.datum_params) ||
            (dest.srsProjNumber == "900913" && source.datumCode != "WGS84" && !source.datum_params)) {
            var wgs84 = Proj4js.WGS84;
            this.transform(source, wgs84, point);
            source = wgs84;
        }

        // DGR, 2010/11/12
        if (source.axis!="enu") {
            this.adjust_axis(source,false,point);
        }

        // Transform source points to long/lat, if they aren't already.
        if ( source.projName=="longlat") {
            point.x *= Proj4js.common.D2R;  // convert degrees to radians
            point.y *= Proj4js.common.D2R;
        } else {
            if (source.to_meter) {
                point.x *= source.to_meter;
                point.y *= source.to_meter;
            }
            source.inverse(point); // Convert Cartesian to longlat
        }

        // Adjust for the prime meridian if necessary
        if (source.from_greenwich) { 
            point.x += source.from_greenwich; 
        }

        // Convert datums if needed, and if possible.
        point = this.datum_transform( source.datum, dest.datum, point );

        // Adjust for the prime meridian if necessary
        if (dest.from_greenwich) {
            point.x -= dest.from_greenwich;
        }

        if( dest.projName=="longlat" ) {             
            // convert radians to decimal degrees
            point.x *= Proj4js.common.R2D;
            point.y *= Proj4js.common.R2D;
        } else  {               // else project
            dest.forward(point);
            if (dest.to_meter) {
                point.x /= dest.to_meter;
                point.y /= dest.to_meter;
            }
        }

        // DGR, 2010/11/12
        if (dest.axis!="enu") {
            this.adjust_axis(dest,true,point);
        }

        return point;
    }, // transform()

    /** datum_transform()
      source coordinate system definition,
      destination coordinate system definition,
      point to transform in geodetic coordinates (long, lat, height)
    */
    datum_transform : function( source, dest, point ) {

      // Short cut if the datums are identical.
      if( source.compare_datums( dest ) ) {
          return point; // in this case, zero is sucess,
                    // whereas cs_compare_datums returns 1 to indicate TRUE
                    // confusing, should fix this
      }

      // Explicitly skip datum transform by setting 'datum=none' as parameter for either source or dest
      if( source.datum_type == Proj4js.common.PJD_NODATUM
          || dest.datum_type == Proj4js.common.PJD_NODATUM) {
          return point;
      }

      // If this datum requires grid shifts, then apply it to geodetic coordinates.
      if( source.datum_type == Proj4js.common.PJD_GRIDSHIFT )
      {
        alert("ERROR: Grid shift transformations are not implemented yet.");
        /*
          pj_apply_gridshift( pj_param(source.params,"snadgrids").s, 0,
                              point_count, point_offset, x, y, z );
          CHECK_RETURN;

          src_a = SRS_WGS84_SEMIMAJOR;
          src_es = 0.006694379990;
        */
      }

      if( dest.datum_type == Proj4js.common.PJD_GRIDSHIFT )
      {
        alert("ERROR: Grid shift transformations are not implemented yet.");
        /*
          dst_a = ;
          dst_es = 0.006694379990;
        */
      }

      // Do we need to go through geocentric coordinates?
      if( source.es != dest.es || source.a != dest.a
          || source.datum_type == Proj4js.common.PJD_3PARAM
          || source.datum_type == Proj4js.common.PJD_7PARAM
          || dest.datum_type == Proj4js.common.PJD_3PARAM
          || dest.datum_type == Proj4js.common.PJD_7PARAM)
      {

        // Convert to geocentric coordinates.
        source.geodetic_to_geocentric( point );
        // CHECK_RETURN;

        // Convert between datums
        if( source.datum_type == Proj4js.common.PJD_3PARAM || source.datum_type == Proj4js.common.PJD_7PARAM ) {
          source.geocentric_to_wgs84(point);
          // CHECK_RETURN;
        }

        if( dest.datum_type == Proj4js.common.PJD_3PARAM || dest.datum_type == Proj4js.common.PJD_7PARAM ) {
          dest.geocentric_from_wgs84(point);
          // CHECK_RETURN;
        }

        // Convert back to geodetic coordinates
        dest.geocentric_to_geodetic( point );
          // CHECK_RETURN;
      }

      // Apply grid shift to destination if required
      if( dest.datum_type == Proj4js.common.PJD_GRIDSHIFT )
      {
        alert("ERROR: Grid shift transformations are not implemented yet.");
        // pj_apply_gridshift( pj_param(dest.params,"snadgrids").s, 1, point);
        // CHECK_RETURN;
      }
      return point;
    }, // cs_datum_transform

    /**
     * Function: adjust_axis
     * Normalize or de-normalized the x/y/z axes.  The normal form is "enu"
     * (easting, northing, up).
     * Parameters:
     * crs {Proj4js.Proj} the coordinate reference system
     * denorm {Boolean} when false, normalize
     * point {Object} the coordinates to adjust
     */
    adjust_axis: function(crs, denorm, point) {
        var xin= point.x, yin= point.y, zin= point.z || 0.0;
        var v, t;
        for (var i= 0; i<3; i++) {
            if (denorm && i==2 && point.z===undefined) { continue; }
                 if (i==0) { v= xin; t= 'x'; }
            else if (i==1) { v= yin; t= 'y'; }
            else           { v= zin; t= 'z'; }
            switch(crs.axis[i]) {
            case 'e':
                point[t]= v;
                break;
            case 'w':
                point[t]= -v;
                break;
            case 'n':
                point[t]= v;
                break;
            case 's':
                point[t]= -v;
                break;
            case 'u':
                if (point[t]!==undefined) { point.z= v; }
                break;
            case 'd':
                if (point[t]!==undefined) { point.z= -v; }
                break;
            default :
                alert("ERROR: unknow axis ("+crs.axis[i]+") - check definition of "+src.projName);
                return null;
            }
        }
        return point;
    },

    /**
     * Function: reportError
     * An internal method to report errors back to user. 
     * Override this in applications to report error messages or throw exceptions.
     */
    reportError: function(msg) {
      //console.log(msg);
    },

/**
 *
 * Title: Private Methods
 * The following properties and methods are intended for internal use only.
 *
 * This is a minimal implementation of JavaScript inheritance methods so that 
 * Proj4js can be used as a stand-alone library.
 * These are copies of the equivalent OpenLayers methods at v2.7
 */
 
/**
 * Function: extend
 * Copy all properties of a source object to a destination object.  Modifies
 *     the passed in destination object.  Any properties on the source object
 *     that are set to undefined will not be (re)set on the destination object.
 *
 * Parameters:
 * destination - {Object} The object that will be modified
 * source - {Object} The object with properties to be set on the destination
 *
 * Returns:
 * {Object} The destination object.
 */
    extend: function(destination, source) {
      destination = destination || {};
      if(source) {
          for(var property in source) {
              var value = source[property];
              if(value !== undefined) {
                  destination[property] = value;
              }
          }
      }
      return destination;
    },

/**
 * Constructor: Class
 * Base class used to construct all other classes. Includes support for 
 *     multiple inheritance. 
 *  
 */
    Class: function() {
      var Class = function() {
          this.initialize.apply(this, arguments);
      };
  
      var extended = {};
      var parent;
      for(var i=0; i<arguments.length; ++i) {
          if(typeof arguments[i] == "function") {
              // get the prototype of the superclass
              parent = arguments[i].prototype;
          } else {
              // in this case we're extending with the prototype
              parent = arguments[i];
          }
          Proj4js.extend(extended, parent);
      }
      Class.prototype = extended;
      
      return Class;
    },

    /**
     * Function: bind
     * Bind a function to an object.  Method to easily create closures with
     *     'this' altered.
     * 
     * Parameters:
     * func - {Function} Input function.
     * object - {Object} The object to bind to the input function (as this).
     * 
     * Returns:
     * {Function} A closure with 'this' set to the passed in object.
     */
    bind: function(func, object) {
        // create a reference to all arguments past the second one
        var args = Array.prototype.slice.apply(arguments, [2]);
        return function() {
            // Push on any additional arguments from the actual function call.
            // These will come after those sent to the bind call.
            var newArgs = args.concat(
                Array.prototype.slice.apply(arguments, [0])
            );
            return func.apply(object, newArgs);
        };
    },
    
/**
 * The following properties and methods handle dynamic loading of JSON objects.
 */
 
    /**
     * Property: scriptName
     * {String} The filename of this script without any path.
     */
    scriptName: "proj4js-combined.js",

    /**
     * Property: defsLookupService
     * AJAX service to retreive projection definition parameters from
     */
    defsLookupService: 'http://spatialreference.org/ref',

    /**
     * Property: libPath
     * internal: http server path to library code.
     */
    libPath: null,

    /**
     * Function: getScriptLocation
     * Return the path to this script.
     *
     * Returns:
     * Path to this script
     */
    getScriptLocation: function () {
        if (this.libPath) return this.libPath;
        var scriptName = this.scriptName;
        var scriptNameLen = scriptName.length;

        var scripts = document.getElementsByTagName('script');
        for (var i = 0; i < scripts.length; i++) {
            var src = scripts[i].getAttribute('src');
            if (src) {
                var index = src.lastIndexOf(scriptName);
                // is it found, at the end of the URL?
                if ((index > -1) && (index + scriptNameLen == src.length)) {
                    this.libPath = src.slice(0, -scriptNameLen);
                    break;
                }
            }
        }
        return this.libPath||"";
    },

    /**
     * Function: loadScript
     * Load a JS file from a URL into a <script> tag in the page.
     * 
     * Parameters:
     * url - {String} The URL containing the script to load
     * onload - {Function} A method to be executed when the script loads successfully
     * onfail - {Function} A method to be executed when there is an error loading the script
     * loadCheck - {Function} A boolean method that checks to see if the script 
     *            has loaded.  Typically this just checks for the existance of
     *            an object in the file just loaded.
     */
    loadScript: function(url, onload, onfail, loadCheck) {
      var script = document.createElement('script');
      script.defer = false;
      script.type = "text/javascript";
      script.id = url;
      script.src = url;
      script.onload = onload;
      script.onerror = onfail;
      script.loadCheck = loadCheck;
      if (/MSIE/.test(navigator.userAgent)) {
        script.onreadystatechange = this.checkReadyState;
      }
      document.getElementsByTagName('head')[0].appendChild(script);
    },
    
    /**
     * Function: checkReadyState
     * IE workaround since there is no onerror handler.  Calls the user defined 
     * loadCheck method to determine if the script is loaded.
     * 
     */
    checkReadyState: function() {
      if (this.readyState == 'loaded') {
        if (!this.loadCheck()) {
          this.onerror();
        } else {
          this.onload();
        }
      }
    }
};

/**
 * Class: Proj4js.Proj
 *
 * Proj objects provide transformation methods for point coordinates
 * between geodetic latitude/longitude and a projected coordinate system. 
 * once they have been initialized with a projection code.
 *
 * Initialization of Proj objects is with a projection code, usually EPSG codes,
 * which is the key that will be used with the Proj4js.defs array.
 * 
 * The code passed in will be stripped of colons and converted to uppercase
 * to locate projection definition files.
 *
 * A projection object has properties for units and title strings.
 */
Proj4js.Proj = Proj4js.Class({

  /**
   * Property: readyToUse
   * Flag to indicate if initialization is complete for this Proj object
   */
  readyToUse: false,   
  
  /**
   * Property: title
   * The title to describe the projection
   */
  title: null,  
  
  /**
   * Property: projName
   * The projection class for this projection, e.g. lcc (lambert conformal conic,
   * or merc for mercator).  These are exactly equivalent to their Proj4 
   * counterparts.
   */
  projName: null,
  /**
   * Property: units
   * The units of the projection.  Values include 'm' and 'degrees'
   */
  units: null,
  /**
   * Property: datum
   * The datum specified for the projection
   */
  datum: null,
  /**
   * Property: x0
   * The x coordinate origin
   */
  x0: 0,
  /**
   * Property: y0
   * The y coordinate origin
   */
  y0: 0,
  /**
   * Property: localCS
   * Flag to indicate if the projection is a local one in which no transforms
   * are required.
   */
  localCS: false,

  /**
  * Property: queue
  * Buffer (FIFO) to hold callbacks waiting to be called when projection loaded.
  */
  queue: null,

  /**
  * Constructor: initialize
  * Constructor for Proj4js.Proj objects
  *
  * Parameters:
  * srsCode - a code for map projection definition parameters.  These are usually
  * (but not always) EPSG codes.
  */
  initialize: function(srsCode, callback) {
      this.srsCodeInput = srsCode;
      
      //Register callbacks prior to attempting to process definition
      this.queue = [];
      if( callback ){
           this.queue.push( callback );
      }
      
      //check to see if this is a WKT string
      if ((srsCode.indexOf('GEOGCS') >= 0) ||
          (srsCode.indexOf('GEOCCS') >= 0) ||
          (srsCode.indexOf('PROJCS') >= 0) ||
          (srsCode.indexOf('LOCAL_CS') >= 0)) {
            this.parseWKT(srsCode);
            this.deriveConstants();
            this.loadProjCode(this.projName);
            return;
      }
      
      // DGR 2008-08-03 : support urn and url
      if (srsCode.indexOf('urn:') == 0) {
          //urn:ORIGINATOR:def:crs:CODESPACE:VERSION:ID
          var urn = srsCode.split(':');
          if ((urn[1] == 'ogc' || urn[1] =='x-ogc') &&
              (urn[2] =='def') &&
              (urn[3] =='crs')) {
              srsCode = urn[4]+':'+urn[urn.length-1];
          }
      } else if (srsCode.indexOf('http://') == 0) {
          //url#ID
          var url = srsCode.split('#');
          if (url[0].match(/epsg.org/)) {
            // http://www.epsg.org/#
            srsCode = 'EPSG:'+url[1];
          } else if (url[0].match(/RIG.xml/)) {
            //http://librairies.ign.fr/geoportail/resources/RIG.xml#
            //http://interop.ign.fr/registers/ign/RIG.xml#
            srsCode = 'IGNF:'+url[1];
          }
      }
      this.srsCode = srsCode.toUpperCase();
      if (this.srsCode.indexOf("EPSG") == 0) {
          this.srsCode = this.srsCode;
          this.srsAuth = 'epsg';
          this.srsProjNumber = this.srsCode.substring(5);
      // DGR 2007-11-20 : authority IGNF
      } else if (this.srsCode.indexOf("IGNF") == 0) {
          this.srsCode = this.srsCode;
          this.srsAuth = 'IGNF';
          this.srsProjNumber = this.srsCode.substring(5);
      // DGR 2008-06-19 : pseudo-authority CRS for WMS
      } else if (this.srsCode.indexOf("CRS") == 0) {
          this.srsCode = this.srsCode;
          this.srsAuth = 'CRS';
          this.srsProjNumber = this.srsCode.substring(4);
      } else {
          this.srsAuth = '';
          this.srsProjNumber = this.srsCode;
      }
      
      this.loadProjDefinition();
  },
  
/**
 * Function: loadProjDefinition
 *    Loads the coordinate system initialization string if required.
 *    Note that dynamic loading happens asynchronously so an application must 
 *    wait for the readyToUse property is set to true.
 *    To prevent dynamic loading, include the defs through a script tag in
 *    your application.
 *
 */
    loadProjDefinition: function() {
      //check in memory
      if (Proj4js.defs[this.srsCode]) {
        this.defsLoaded();
        return;
      }

      //else check for def on the server
      var url = Proj4js.getScriptLocation() + 'defs/' + this.srsAuth.toUpperCase() + this.srsProjNumber + '.js';
      Proj4js.loadScript(url, 
                Proj4js.bind(this.defsLoaded, this),
                Proj4js.bind(this.loadFromService, this),
                Proj4js.bind(this.checkDefsLoaded, this) );
    },

/**
 * Function: loadFromService
 *    Creates the REST URL for loading the definition from a web service and 
 *    loads it.
 *
 */
    loadFromService: function() {
      //else load from web service
      var url = Proj4js.defsLookupService +'/' + this.srsAuth +'/'+ this.srsProjNumber + '/proj4js/';
      Proj4js.loadScript(url, 
            Proj4js.bind(this.defsLoaded, this),
            Proj4js.bind(this.defsFailed, this),
            Proj4js.bind(this.checkDefsLoaded, this) );
    },

/**
 * Function: defsLoaded
 * Continues the Proj object initilization once the def file is loaded
 *
 */
    defsLoaded: function() {
      this.parseDefs();
      this.loadProjCode(this.projName);
    },
    
/**
 * Function: checkDefsLoaded
 *    This is the loadCheck method to see if the def object exists
 *
 */
    checkDefsLoaded: function() {
      if (Proj4js.defs[this.srsCode]) {
        return true;
      } else {
        return false;
      }
    },

 /**
 * Function: defsFailed
 *    Report an error in loading the defs file, but continue on using WGS84
 *
 */
   defsFailed: function() {
      Proj4js.reportError('failed to load projection definition for: '+this.srsCode);
      Proj4js.defs[this.srsCode] = Proj4js.defs['WGS84'];  //set it to something so it can at least continue
      this.defsLoaded();
    },

/**
 * Function: loadProjCode
 *    Loads projection class code dynamically if required.
 *     Projection code may be included either through a script tag or in
 *     a built version of proj4js
 *
 */
    loadProjCode: function(projName) {
      if (Proj4js.Proj[projName]) {
        this.initTransforms();
        return;
      }

      //the URL for the projection code
      var url = Proj4js.getScriptLocation() + 'projCode/' + projName + '.js';
      Proj4js.loadScript(url, 
              Proj4js.bind(this.loadProjCodeSuccess, this, projName),
              Proj4js.bind(this.loadProjCodeFailure, this, projName), 
              Proj4js.bind(this.checkCodeLoaded, this, projName) );
    },

 /**
 * Function: loadProjCodeSuccess
 *    Loads any proj dependencies or continue on to final initialization.
 *
 */
    loadProjCodeSuccess: function(projName) {
      if (Proj4js.Proj[projName].dependsOn){
        this.loadProjCode(Proj4js.Proj[projName].dependsOn);
      } else {
        this.initTransforms();
      }
    },

 /**
 * Function: defsFailed
 *    Report an error in loading the proj file.  Initialization of the Proj
 *    object has failed and the readyToUse flag will never be set.
 *
 */
    loadProjCodeFailure: function(projName) {
      Proj4js.reportError("failed to find projection file for: " + projName);
      //TBD initialize with identity transforms so proj will still work?
    },
    
/**
 * Function: checkCodeLoaded
 *    This is the loadCheck method to see if the projection code is loaded
 *
 */
    checkCodeLoaded: function(projName) {
      if (Proj4js.Proj[projName]) {
        return true;
      } else {
        return false;
      }
    },

/**
 * Function: initTransforms
 *    Finalize the initialization of the Proj object
 *
 */
    initTransforms: function() {
      Proj4js.extend(this, Proj4js.Proj[this.projName]);
      this.init();
      this.readyToUse = true;
      if( this.queue ) {
        var item;
        while( (item = this.queue.shift()) ) {
          item.call( this, this );
        }
      }
  },

/**
 * Function: parseWKT
 * Parses a WKT string to get initialization parameters
 *
 */
 wktRE: /^(\w+)\[(.*)\]$/,
 parseWKT: function(wkt) {
    var wktMatch = wkt.match(this.wktRE);
    if (!wktMatch) return;
    var wktObject = wktMatch[1];
    var wktContent = wktMatch[2];
    var wktTemp = wktContent.split(",");
    var wktName;
    if (wktObject.toUpperCase() == "TOWGS84") {
      wktName = wktObject;  //no name supplied for the TOWGS84 array
    } else {
      wktName = wktTemp.shift();
    }
    wktName = wktName.replace(/^\"/,"");
    wktName = wktName.replace(/\"$/,"");
    
    /*
    wktContent = wktTemp.join(",");
    var wktArray = wktContent.split("],");
    for (var i=0; i<wktArray.length-1; ++i) {
      wktArray[i] += "]";
    }
    */
    
    var wktArray = new Array();
    var bkCount = 0;
    var obj = "";
    for (var i=0; i<wktTemp.length; ++i) {
      var token = wktTemp[i];
      for (var j=0; j<token.length; ++j) {
        if (token.charAt(j) == "[") ++bkCount;
        if (token.charAt(j) == "]") --bkCount;
      }
      obj += token;
      if (bkCount === 0) {
        wktArray.push(obj);
        obj = "";
      } else {
        obj += ",";
      }
    }
    
    //do something based on the type of the wktObject being parsed
    //add in variations in the spelling as required
    switch (wktObject) {
      case 'LOCAL_CS':
        this.projName = 'identity'
        this.localCS = true;
        this.srsCode = wktName;
        break;
      case 'GEOGCS':
        this.projName = 'longlat'
        this.geocsCode = wktName;
        if (!this.srsCode) this.srsCode = wktName;
        break;
      case 'PROJCS':
        this.srsCode = wktName;
        break;
      case 'GEOCCS':
        break;
      case 'PROJECTION':
        this.projName = Proj4js.wktProjections[wktName]
        break;
      case 'DATUM':
        this.datumName = wktName;
        break;
      case 'LOCAL_DATUM':
        this.datumCode = 'none';
        break;
      case 'SPHEROID':
        this.ellps = wktName;
        this.a = parseFloat(wktArray.shift());
        this.rf = parseFloat(wktArray.shift());
        break;
      case 'PRIMEM':
        this.from_greenwich = parseFloat(wktArray.shift()); //to radians?
        break;
      case 'UNIT':
        this.units = wktName;
        this.unitsPerMeter = parseFloat(wktArray.shift());
        break;
      case 'PARAMETER':
        var name = wktName.toLowerCase();
        var value = parseFloat(wktArray.shift());
        //there may be many variations on the wktName values, add in case
        //statements as required
        switch (name) {
          case 'false_easting':
            this.x0 = value;
            break;
          case 'false_northing':
            this.y0 = value;
            break;
          case 'scale_factor':
            this.k0 = value;
            break;
          case 'central_meridian':
            this.long0 = value*Proj4js.common.D2R;
            break;
          case 'latitude_of_origin':
            this.lat0 = value*Proj4js.common.D2R;
            break;
          case 'more_here':
            break;
          default:
            break;
        }
        break;
      case 'TOWGS84':
        this.datum_params = wktArray;
        break;
      //DGR 2010-11-12: AXIS
      case 'AXIS':
        var name= wktName.toLowerCase();
        var value= wktArray.shift();
        switch (value) {
          case 'EAST' : value= 'e'; break;
          case 'WEST' : value= 'w'; break;
          case 'NORTH': value= 'n'; break;
          case 'SOUTH': value= 's'; break;
          case 'UP'   : value= 'u'; break;
          case 'DOWN' : value= 'd'; break;
          case 'OTHER':
          default     : value= ' '; break;//FIXME
        }
        if (!this.axis) { this.axis= "enu"; }
        switch(name) {
          case 'X': this.axis=                         value + this.axis.substr(1,2); break;
          case 'Y': this.axis= this.axis.substr(0,1) + value + this.axis.substr(2,1); break;
          case 'Z': this.axis= this.axis.substr(0,2) + value                        ; break;
          default : break;
        }
      case 'MORE_HERE':
        break;
      default:
        break;
    }
    for (var i=0; i<wktArray.length; ++i) {
      this.parseWKT(wktArray[i]);
    }
 },

/**
 * Function: parseDefs
 * Parses the PROJ.4 initialization string and sets the associated properties.
 *
 */
  parseDefs: function() {
      this.defData = Proj4js.defs[this.srsCode];
      var paramName, paramVal;
      if (!this.defData) {
        return;
      }
      var paramArray=this.defData.split("+");

      for (var prop=0; prop<paramArray.length; prop++) {
          var property = paramArray[prop].split("=");
          paramName = property[0].toLowerCase();
          paramVal = property[1];

          switch (paramName.replace(/\s/gi,"")) {  // trim out spaces
              case "": break;   // throw away nameless parameter
              case "title":  this.title = paramVal; break;
              case "proj":   this.projName =  paramVal.replace(/\s/gi,""); break;
              case "units":  this.units = paramVal.replace(/\s/gi,""); break;
              case "datum":  this.datumCode = paramVal.replace(/\s/gi,""); break;
              case "nadgrids": this.nagrids = paramVal.replace(/\s/gi,""); break;
              case "ellps":  this.ellps = paramVal.replace(/\s/gi,""); break;
              case "a":      this.a =  parseFloat(paramVal); break;  // semi-major radius
              case "b":      this.b =  parseFloat(paramVal); break;  // semi-minor radius
              // DGR 2007-11-20
              case "rf":     this.rf = parseFloat(paramVal); break; // inverse flattening rf= a/(a-b)
              case "lat_0":  this.lat0 = paramVal*Proj4js.common.D2R; break;        // phi0, central latitude
              case "lat_1":  this.lat1 = paramVal*Proj4js.common.D2R; break;        //standard parallel 1
              case "lat_2":  this.lat2 = paramVal*Proj4js.common.D2R; break;        //standard parallel 2
              case "lat_ts": this.lat_ts = paramVal*Proj4js.common.D2R; break;      // used in merc and eqc
              case "lon_0":  this.long0 = paramVal*Proj4js.common.D2R; break;       // lam0, central longitude
              case "alpha":  this.alpha =  parseFloat(paramVal)*Proj4js.common.D2R; break;  //for somerc projection
              case "lonc":   this.longc = paramVal*Proj4js.common.D2R; break;       //for somerc projection
              case "x_0":    this.x0 = parseFloat(paramVal); break;  // false easting
              case "y_0":    this.y0 = parseFloat(paramVal); break;  // false northing
              case "k_0":    this.k0 = parseFloat(paramVal); break;  // projection scale factor
              case "k":      this.k0 = parseFloat(paramVal); break;  // both forms returned
              case "r_a":    this.R_A = true; break;                 // sphere--area of ellipsoid
              case "zone":   this.zone = parseInt(paramVal); break;  // UTM Zone
              case "south":   this.utmSouth = true; break;  // UTM north/south
              case "towgs84":this.datum_params = paramVal.split(","); break;
              case "to_meter": this.to_meter = parseFloat(paramVal); break; // cartesian scaling
              case "from_greenwich": this.from_greenwich = paramVal*Proj4js.common.D2R; break;
              // DGR 2008-07-09 : if pm is not a well-known prime meridian take
              // the value instead of 0.0, then convert to radians
              case "pm":     paramVal = paramVal.replace(/\s/gi,"");
                             this.from_greenwich = Proj4js.PrimeMeridian[paramVal] ?
                                Proj4js.PrimeMeridian[paramVal] : parseFloat(paramVal);
                             this.from_greenwich *= Proj4js.common.D2R; 
                             break;
              // DGR 2010-11-12: axis
              case "axis":   paramVal = paramVal.replace(/\s/gi,"");
                             var legalAxis= "ewnsud";
                             if (paramVal.length==3 &&
                                 legalAxis.indexOf(paramVal.substr(0,1))!=-1 &&
                                 legalAxis.indexOf(paramVal.substr(1,1))!=-1 &&
                                 legalAxis.indexOf(paramVal.substr(2,1))!=-1) {
                                this.axis= paramVal;
                             } //FIXME: be silent ?
                             break
              case "no_defs": break; 
              default: //alert("Unrecognized parameter: " + paramName);
          } // switch()
      } // for paramArray
      this.deriveConstants();
  },

/**
 * Function: deriveConstants
 * Sets several derived constant values and initialization of datum and ellipse
 *     parameters.
 *
 */
  deriveConstants: function() {
      if (this.nagrids == '@null') this.datumCode = 'none';
      if (this.datumCode && this.datumCode != 'none') {
        var datumDef = Proj4js.Datum[this.datumCode];
        if (datumDef) {
          this.datum_params = datumDef.towgs84 ? datumDef.towgs84.split(',') : null;
          this.ellps = datumDef.ellipse;
          this.datumName = datumDef.datumName ? datumDef.datumName : this.datumCode;
        }
      }
      if (!this.a) {    // do we have an ellipsoid?
          var ellipse = Proj4js.Ellipsoid[this.ellps] ? Proj4js.Ellipsoid[this.ellps] : Proj4js.Ellipsoid['WGS84'];
          Proj4js.extend(this, ellipse);
      }
      if (this.rf && !this.b) this.b = (1.0 - 1.0/this.rf) * this.a;
      if (Math.abs(this.a - this.b)<Proj4js.common.EPSLN) {
        this.sphere = true;
        this.b= this.a;
      }
      this.a2 = this.a * this.a;          // used in geocentric
      this.b2 = this.b * this.b;          // used in geocentric
      this.es = (this.a2-this.b2)/this.a2;  // e ^ 2
      this.e = Math.sqrt(this.es);        // eccentricity
      if (this.R_A) {
        this.a *= 1. - this.es * (Proj4js.common.SIXTH + this.es * (Proj4js.common.RA4 + this.es * Proj4js.common.RA6));
        this.a2 = this.a * this.a;
        this.b2 = this.b * this.b;
        this.es = 0.;
      }
      this.ep2=(this.a2-this.b2)/this.b2; // used in geocentric
      if (!this.k0) this.k0 = 1.0;    //default value
      //DGR 2010-11-12: axis
      if (!this.axis) { this.axis= "enu"; }

      this.datum = new Proj4js.datum(this);
  }
});

Proj4js.Proj.longlat = {
  init: function() {
    //no-op for longlat
  },
  forward: function(pt) {
    //identity transform
    return pt;
  },
  inverse: function(pt) {
    //identity transform
    return pt;
  }
};
Proj4js.Proj.identity = Proj4js.Proj.longlat;

/**
  Proj4js.defs is a collection of coordinate system definition objects in the 
  PROJ.4 command line format.
  Generally a def is added by means of a separate .js file for example:

    < SCRIPT type="text/javascript" src="defs/EPSG26912.js" >< /SCRIPT >

  def is a CS definition in PROJ.4 WKT format, for example:
    +proj="tmerc"   //longlat, etc.
    +a=majorRadius
    +b=minorRadius
    +lat0=somenumber
    +long=somenumber
*/
Proj4js.defs = {
  // These are so widely used, we'll go ahead and throw them in
  // without requiring a separate .js file
  'WGS84': "+title=long/lat:WGS84 +proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees",
  'EPSG:4326': "+title=long/lat:WGS84 +proj=longlat +a=6378137.0 +b=6356752.31424518 +ellps=WGS84 +datum=WGS84 +units=degrees",
  'EPSG:4269': "+title=long/lat:NAD83 +proj=longlat +a=6378137.0 +b=6356752.31414036 +ellps=GRS80 +datum=NAD83 +units=degrees",
  'EPSG:3785': "+title= Google Mercator +proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +no_defs"
};
Proj4js.defs['GOOGLE'] = Proj4js.defs['EPSG:3785'];
Proj4js.defs['EPSG:900913'] = Proj4js.defs['EPSG:3785'];
Proj4js.defs['EPSG:102113'] = Proj4js.defs['EPSG:3785'];

Proj4js.common = {
  PI : 3.141592653589793238, //Math.PI,
  HALF_PI : 1.570796326794896619, //Math.PI*0.5,
  TWO_PI : 6.283185307179586477, //Math.PI*2,
  FORTPI : 0.78539816339744833,
  R2D : 57.29577951308232088,
  D2R : 0.01745329251994329577,
  SEC_TO_RAD : 4.84813681109535993589914102357e-6, /* SEC_TO_RAD = Pi/180/3600 */
  EPSLN : 1.0e-10,
  MAX_ITER : 20,
  // following constants from geocent.c
  COS_67P5 : 0.38268343236508977,  /* cosine of 67.5 degrees */
  AD_C : 1.0026000,                /* Toms region 1 constant */

  /* datum_type values */
  PJD_UNKNOWN  : 0,
  PJD_3PARAM   : 1,
  PJD_7PARAM   : 2,
  PJD_GRIDSHIFT: 3,
  PJD_WGS84    : 4,   // WGS84 or equivalent
  PJD_NODATUM  : 5,   // WGS84 or equivalent
  SRS_WGS84_SEMIMAJOR : 6378137.0,  // only used in grid shift transforms

  // ellipoid pj_set_ell.c
  SIXTH : .1666666666666666667, /* 1/6 */
  RA4   : .04722222222222222222, /* 17/360 */
  RA6   : .02215608465608465608, /* 67/3024 */
  RV4   : .06944444444444444444, /* 5/72 */
  RV6   : .04243827160493827160, /* 55/1296 */

// Function to compute the constant small m which is the radius of
//   a parallel of latitude, phi, divided by the semimajor axis.
// -----------------------------------------------------------------
  msfnz : function(eccent, sinphi, cosphi) {
      var con = eccent * sinphi;
      return cosphi/(Math.sqrt(1.0 - con * con));
  },

// Function to compute the constant small t for use in the forward
//   computations in the Lambert Conformal Conic and the Polar
//   Stereographic projections.
// -----------------------------------------------------------------
  tsfnz : function(eccent, phi, sinphi) {
    var con = eccent * sinphi;
    var com = .5 * eccent;
    con = Math.pow(((1.0 - con) / (1.0 + con)), com);
    return (Math.tan(.5 * (this.HALF_PI - phi))/con);
  },

// Function to compute the latitude angle, phi2, for the inverse of the
//   Lambert Conformal Conic and Polar Stereographic projections.
// ----------------------------------------------------------------
  phi2z : function(eccent, ts) {
    var eccnth = .5 * eccent;
    var con, dphi;
    var phi = this.HALF_PI - 2 * Math.atan(ts);
    for (var i = 0; i <= 15; i++) {
      con = eccent * Math.sin(phi);
      dphi = this.HALF_PI - 2 * Math.atan(ts *(Math.pow(((1.0 - con)/(1.0 + con)),eccnth))) - phi;
      phi += dphi;
      if (Math.abs(dphi) <= .0000000001) return phi;
    }
    alert("phi2z has NoConvergence");
    return (-9999);
  },

/* Function to compute constant small q which is the radius of a 
   parallel of latitude, phi, divided by the semimajor axis. 
------------------------------------------------------------*/
  qsfnz : function(eccent,sinphi) {
    var con;
    if (eccent > 1.0e-7) {
      con = eccent * sinphi;
      return (( 1.0- eccent * eccent) * (sinphi /(1.0 - con * con) - (.5/eccent)*Math.log((1.0 - con)/(1.0 + con))));
    } else {
      return(2.0 * sinphi);
    }
  },

/* Function to eliminate roundoff errors in asin
----------------------------------------------*/
  asinz : function(x) {
    if (Math.abs(x)>1.0) {
      x=(x>1.0)?1.0:-1.0;
    }
    return Math.asin(x);
  },

// following functions from gctpc cproj.c for transverse mercator projections
  e0fn : function(x) {return(1.0-0.25*x*(1.0+x/16.0*(3.0+1.25*x)));},
  e1fn : function(x) {return(0.375*x*(1.0+0.25*x*(1.0+0.46875*x)));},
  e2fn : function(x) {return(0.05859375*x*x*(1.0+0.75*x));},
  e3fn : function(x) {return(x*x*x*(35.0/3072.0));},
  mlfn : function(e0,e1,e2,e3,phi) {return(e0*phi-e1*Math.sin(2.0*phi)+e2*Math.sin(4.0*phi)-e3*Math.sin(6.0*phi));},

  srat : function(esinp, exp) {
    return(Math.pow((1.0-esinp)/(1.0+esinp), exp));
  },

// Function to return the sign of an argument
  sign : function(x) { if (x < 0.0) return(-1); else return(1);},

// Function to adjust longitude to -180 to 180; input in radians
  adjust_lon : function(x) {
    x = (Math.abs(x) < this.PI) ? x: (x - (this.sign(x)*this.TWO_PI) );
    return x;
  },

// IGNF - DGR : algorithms used by IGN France

// Function to adjust latitude to -90 to 90; input in radians
  adjust_lat : function(x) {
    x= (Math.abs(x) < this.HALF_PI) ? x: (x - (this.sign(x)*this.PI) );
    return x;
  },

// Latitude Isometrique - close to tsfnz ...
  latiso : function(eccent, phi, sinphi) {
    if (Math.abs(phi) > this.HALF_PI) return +Number.NaN;
    if (phi==this.HALF_PI) return Number.POSITIVE_INFINITY;
    if (phi==-1.0*this.HALF_PI) return -1.0*Number.POSITIVE_INFINITY;

    var con= eccent*sinphi;
    return Math.log(Math.tan((this.HALF_PI+phi)/2.0))+eccent*Math.log((1.0-con)/(1.0+con))/2.0;
  },

  fL : function(x,L) {
    return 2.0*Math.atan(x*Math.exp(L)) - this.HALF_PI;
  },

// Inverse Latitude Isometrique - close to ph2z
  invlatiso : function(eccent, ts) {
    var phi= this.fL(1.0,ts);
    var Iphi= 0.0;
    var con= 0.0;
    do {
      Iphi= phi;
      con= eccent*Math.sin(Iphi);
      phi= this.fL(Math.exp(eccent*Math.log((1.0+con)/(1.0-con))/2.0),ts)
    } while (Math.abs(phi-Iphi)>1.0e-12);
    return phi;
  },

// Needed for Gauss Schreiber
// Original:  Denis Makarov (info@binarythings.com)
// Web Site:  http://www.binarythings.com
  sinh : function(x)
  {
    var r= Math.exp(x);
    r= (r-1.0/r)/2.0;
    return r;
  },

  cosh : function(x)
  {
    var r= Math.exp(x);
    r= (r+1.0/r)/2.0;
    return r;
  },

  tanh : function(x)
  {
    var r= Math.exp(x);
    r= (r-1.0/r)/(r+1.0/r);
    return r;
  },

  asinh : function(x)
  {
    var s= (x>= 0? 1.0:-1.0);
    return s*(Math.log( Math.abs(x) + Math.sqrt(x*x+1.0) ));
  },

  acosh : function(x)
  {
    return 2.0*Math.log(Math.sqrt((x+1.0)/2.0) + Math.sqrt((x-1.0)/2.0));
  },

  atanh : function(x)
  {
    return Math.log((x-1.0)/(x+1.0))/2.0;
  },

// Grande Normale
  gN : function(a,e,sinphi)
  {
    var temp= e*sinphi;
    return a/Math.sqrt(1.0 - temp*temp);
  }

};

/** datum object
*/
Proj4js.datum = Proj4js.Class({

  initialize : function(proj) {
    this.datum_type = Proj4js.common.PJD_WGS84;   //default setting
    if (proj.datumCode && proj.datumCode == 'none') {
      this.datum_type = Proj4js.common.PJD_NODATUM;
    }
    if (proj && proj.datum_params) {
      for (var i=0; i<proj.datum_params.length; i++) {
        proj.datum_params[i]=parseFloat(proj.datum_params[i]);
      }
      if (proj.datum_params[0] != 0 || proj.datum_params[1] != 0 || proj.datum_params[2] != 0 ) {
        this.datum_type = Proj4js.common.PJD_3PARAM;
      }
      if (proj.datum_params.length > 3) {
        if (proj.datum_params[3] != 0 || proj.datum_params[4] != 0 ||
            proj.datum_params[5] != 0 || proj.datum_params[6] != 0 ) {
          this.datum_type = Proj4js.common.PJD_7PARAM;
          proj.datum_params[3] *= Proj4js.common.SEC_TO_RAD;
          proj.datum_params[4] *= Proj4js.common.SEC_TO_RAD;
          proj.datum_params[5] *= Proj4js.common.SEC_TO_RAD;
          proj.datum_params[6] = (proj.datum_params[6]/1000000.0) + 1.0;
        }
      }
    }
    if (proj) {
      this.a = proj.a;    //datum object also uses these values
      this.b = proj.b;
      this.es = proj.es;
      this.ep2 = proj.ep2;
      this.datum_params = proj.datum_params;
    }
  },

  /****************************************************************/
  // cs_compare_datums()
  //   Returns 1 (TRUE) if the two datums match, otherwise 0 (FALSE).
  compare_datums : function( dest ) {
    if( this.datum_type != dest.datum_type ) {
      return false; // false, datums are not equal
    } else if( this.a != dest.a || Math.abs(this.es-dest.es) > 0.000000000050 ) {
      // the tolerence for es is to ensure that GRS80 and WGS84
      // are considered identical
      return false;
    } else if( this.datum_type == Proj4js.common.PJD_3PARAM ) {
      return (this.datum_params[0] == dest.datum_params[0]
              && this.datum_params[1] == dest.datum_params[1]
              && this.datum_params[2] == dest.datum_params[2]);
    } else if( this.datum_type == Proj4js.common.PJD_7PARAM ) {
      return (this.datum_params[0] == dest.datum_params[0]
              && this.datum_params[1] == dest.datum_params[1]
              && this.datum_params[2] == dest.datum_params[2]
              && this.datum_params[3] == dest.datum_params[3]
              && this.datum_params[4] == dest.datum_params[4]
              && this.datum_params[5] == dest.datum_params[5]
              && this.datum_params[6] == dest.datum_params[6]);
    } else if( this.datum_type == Proj4js.common.PJD_GRIDSHIFT ) {
      return strcmp( pj_param(this.params,"snadgrids").s,
                     pj_param(dest.params,"snadgrids").s ) == 0;
    } else {
      return true; // datums are equal
    }
  }, // cs_compare_datums()

  /*
   * The function Convert_Geodetic_To_Geocentric converts geodetic coordinates
   * (latitude, longitude, and height) to geocentric coordinates (X, Y, Z),
   * according to the current ellipsoid parameters.
   *
   *    Latitude  : Geodetic latitude in radians                     (input)
   *    Longitude : Geodetic longitude in radians                    (input)
   *    Height    : Geodetic height, in meters                       (input)
   *    X         : Calculated Geocentric X coordinate, in meters    (output)
   *    Y         : Calculated Geocentric Y coordinate, in meters    (output)
   *    Z         : Calculated Geocentric Z coordinate, in meters    (output)
   *
   */
  geodetic_to_geocentric : function(p) {
    var Longitude = p.x;
    var Latitude = p.y;
    var Height = p.z ? p.z : 0;   //Z value not always supplied
    var X;  // output
    var Y;
    var Z;

    var Error_Code=0;  //  GEOCENT_NO_ERROR;
    var Rn;            /*  Earth radius at location  */
    var Sin_Lat;       /*  Math.sin(Latitude)  */
    var Sin2_Lat;      /*  Square of Math.sin(Latitude)  */
    var Cos_Lat;       /*  Math.cos(Latitude)  */

    /*
    ** Don't blow up if Latitude is just a little out of the value
    ** range as it may just be a rounding issue.  Also removed longitude
    ** test, it should be wrapped by Math.cos() and Math.sin().  NFW for PROJ.4, Sep/2001.
    */
    if( Latitude < -Proj4js.common.HALF_PI && Latitude > -1.001 * Proj4js.common.HALF_PI ) {
        Latitude = -Proj4js.common.HALF_PI;
    } else if( Latitude > Proj4js.common.HALF_PI && Latitude < 1.001 * Proj4js.common.HALF_PI ) {
        Latitude = Proj4js.common.HALF_PI;
    } else if ((Latitude < -Proj4js.common.HALF_PI) || (Latitude > Proj4js.common.HALF_PI)) {
      /* Latitude out of range */
      Proj4js.reportError('geocent:lat out of range:'+Latitude);
      return null;
    }

    if (Longitude > Proj4js.common.PI) Longitude -= (2*Proj4js.common.PI);
    Sin_Lat = Math.sin(Latitude);
    Cos_Lat = Math.cos(Latitude);
    Sin2_Lat = Sin_Lat * Sin_Lat;
    Rn = this.a / (Math.sqrt(1.0e0 - this.es * Sin2_Lat));
    X = (Rn + Height) * Cos_Lat * Math.cos(Longitude);
    Y = (Rn + Height) * Cos_Lat * Math.sin(Longitude);
    Z = ((Rn * (1 - this.es)) + Height) * Sin_Lat;

    p.x = X;
    p.y = Y;
    p.z = Z;
    return Error_Code;
  }, // cs_geodetic_to_geocentric()


  geocentric_to_geodetic : function (p) {
/* local defintions and variables */
/* end-criterium of loop, accuracy of sin(Latitude) */
var genau = 1.E-12;
var genau2 = (genau*genau);
var maxiter = 30;

    var P;        /* distance between semi-minor axis and location */
    var RR;       /* distance between center and location */
    var CT;       /* sin of geocentric latitude */
    var ST;       /* cos of geocentric latitude */
    var RX;
    var RK;
    var RN;       /* Earth radius at location */
    var CPHI0;    /* cos of start or old geodetic latitude in iterations */
    var SPHI0;    /* sin of start or old geodetic latitude in iterations */
    var CPHI;     /* cos of searched geodetic latitude */
    var SPHI;     /* sin of searched geodetic latitude */
    var SDPHI;    /* end-criterium: addition-theorem of sin(Latitude(iter)-Latitude(iter-1)) */
    var At_Pole;     /* indicates location is in polar region */
    var iter;        /* # of continous iteration, max. 30 is always enough (s.a.) */

    var X = p.x;
    var Y = p.y;
    var Z = p.z ? p.z : 0.0;   //Z value not always supplied
    var Longitude;
    var Latitude;
    var Height;

    At_Pole = false;
    P = Math.sqrt(X*X+Y*Y);
    RR = Math.sqrt(X*X+Y*Y+Z*Z);

/*      special cases for latitude and longitude */
    if (P/this.a < genau) {

/*  special case, if P=0. (X=0., Y=0.) */
        At_Pole = true;
        Longitude = 0.0;

/*  if (X,Y,Z)=(0.,0.,0.) then Height becomes semi-minor axis
 *  of ellipsoid (=center of mass), Latitude becomes PI/2 */
        if (RR/this.a < genau) {
            Latitude = Proj4js.common.HALF_PI;
            Height   = -this.b;
            return;
        }
    } else {
/*  ellipsoidal (geodetic) longitude
 *  interval: -PI < Longitude <= +PI */
        Longitude=Math.atan2(Y,X);
    }

/* --------------------------------------------------------------
 * Following iterative algorithm was developped by
 * "Institut f�r Erdmessung", University of Hannover, July 1988.
 * Internet: www.ife.uni-hannover.de
 * Iterative computation of CPHI,SPHI and Height.
 * Iteration of CPHI and SPHI to 10**-12 radian resp.
 * 2*10**-7 arcsec.
 * --------------------------------------------------------------
 */
    CT = Z/RR;
    ST = P/RR;
    RX = 1.0/Math.sqrt(1.0-this.es*(2.0-this.es)*ST*ST);
    CPHI0 = ST*(1.0-this.es)*RX;
    SPHI0 = CT*RX;
    iter = 0;

/* loop to find sin(Latitude) resp. Latitude
 * until |sin(Latitude(iter)-Latitude(iter-1))| < genau */
    do
    {
        iter++;
        RN = this.a/Math.sqrt(1.0-this.es*SPHI0*SPHI0);

/*  ellipsoidal (geodetic) height */
        Height = P*CPHI0+Z*SPHI0-RN*(1.0-this.es*SPHI0*SPHI0);

        RK = this.es*RN/(RN+Height);
        RX = 1.0/Math.sqrt(1.0-RK*(2.0-RK)*ST*ST);
        CPHI = ST*(1.0-RK)*RX;
        SPHI = CT*RX;
        SDPHI = SPHI*CPHI0-CPHI*SPHI0;
        CPHI0 = CPHI;
        SPHI0 = SPHI;
    }
    while (SDPHI*SDPHI > genau2 && iter < maxiter);

/*      ellipsoidal (geodetic) latitude */
    Latitude=Math.atan(SPHI/Math.abs(CPHI));

    p.x = Longitude;
    p.y = Latitude;
    p.z = Height;
    return p;
  }, // cs_geocentric_to_geodetic()

  /** Convert_Geocentric_To_Geodetic
   * The method used here is derived from 'An Improved Algorithm for
   * Geocentric to Geodetic Coordinate Conversion', by Ralph Toms, Feb 1996
   */
  geocentric_to_geodetic_noniter : function (p) {
    var X = p.x;
    var Y = p.y;
    var Z = p.z ? p.z : 0;   //Z value not always supplied
    var Longitude;
    var Latitude;
    var Height;

    var W;        /* distance from Z axis */
    var W2;       /* square of distance from Z axis */
    var T0;       /* initial estimate of vertical component */
    var T1;       /* corrected estimate of vertical component */
    var S0;       /* initial estimate of horizontal component */
    var S1;       /* corrected estimate of horizontal component */
    var Sin_B0;   /* Math.sin(B0), B0 is estimate of Bowring aux variable */
    var Sin3_B0;  /* cube of Math.sin(B0) */
    var Cos_B0;   /* Math.cos(B0) */
    var Sin_p1;   /* Math.sin(phi1), phi1 is estimated latitude */
    var Cos_p1;   /* Math.cos(phi1) */
    var Rn;       /* Earth radius at location */
    var Sum;      /* numerator of Math.cos(phi1) */
    var At_Pole;  /* indicates location is in polar region */

    X = parseFloat(X);  // cast from string to float
    Y = parseFloat(Y);
    Z = parseFloat(Z);

    At_Pole = false;
    if (X != 0.0)
    {
        Longitude = Math.atan2(Y,X);
    }
    else
    {
        if (Y > 0)
        {
            Longitude = Proj4js.common.HALF_PI;
        }
        else if (Y < 0)
        {
            Longitude = -Proj4js.common.HALF_PI;
        }
        else
        {
            At_Pole = true;
            Longitude = 0.0;
            if (Z > 0.0)
            {  /* north pole */
                Latitude = Proj4js.common.HALF_PI;
            }
            else if (Z < 0.0)
            {  /* south pole */
                Latitude = -Proj4js.common.HALF_PI;
            }
            else
            {  /* center of earth */
                Latitude = Proj4js.common.HALF_PI;
                Height = -this.b;
                return;
            }
        }
    }
    W2 = X*X + Y*Y;
    W = Math.sqrt(W2);
    T0 = Z * Proj4js.common.AD_C;
    S0 = Math.sqrt(T0 * T0 + W2);
    Sin_B0 = T0 / S0;
    Cos_B0 = W / S0;
    Sin3_B0 = Sin_B0 * Sin_B0 * Sin_B0;
    T1 = Z + this.b * this.ep2 * Sin3_B0;
    Sum = W - this.a * this.es * Cos_B0 * Cos_B0 * Cos_B0;
    S1 = Math.sqrt(T1*T1 + Sum * Sum);
    Sin_p1 = T1 / S1;
    Cos_p1 = Sum / S1;
    Rn = this.a / Math.sqrt(1.0 - this.es * Sin_p1 * Sin_p1);
    if (Cos_p1 >= Proj4js.common.COS_67P5)
    {
        Height = W / Cos_p1 - Rn;
    }
    else if (Cos_p1 <= -Proj4js.common.COS_67P5)
    {
        Height = W / -Cos_p1 - Rn;
    }
    else
    {
        Height = Z / Sin_p1 + Rn * (this.es - 1.0);
    }
    if (At_Pole == false)
    {
        Latitude = Math.atan(Sin_p1 / Cos_p1);
    }

    p.x = Longitude;
    p.y = Latitude;
    p.z = Height;
    return p;
  }, // geocentric_to_geodetic_noniter()

  /****************************************************************/
  // pj_geocentic_to_wgs84( p )
  //  p = point to transform in geocentric coordinates (x,y,z)
  geocentric_to_wgs84 : function ( p ) {

    if( this.datum_type == Proj4js.common.PJD_3PARAM )
    {
      // if( x[io] == HUGE_VAL )
      //    continue;
      p.x += this.datum_params[0];
      p.y += this.datum_params[1];
      p.z += this.datum_params[2];

    }
    else if (this.datum_type == Proj4js.common.PJD_7PARAM)
    {
      var Dx_BF =this.datum_params[0];
      var Dy_BF =this.datum_params[1];
      var Dz_BF =this.datum_params[2];
      var Rx_BF =this.datum_params[3];
      var Ry_BF =this.datum_params[4];
      var Rz_BF =this.datum_params[5];
      var M_BF  =this.datum_params[6];
      // if( x[io] == HUGE_VAL )
      //    continue;
      var x_out = M_BF*(       p.x - Rz_BF*p.y + Ry_BF*p.z) + Dx_BF;
      var y_out = M_BF*( Rz_BF*p.x +       p.y - Rx_BF*p.z) + Dy_BF;
      var z_out = M_BF*(-Ry_BF*p.x + Rx_BF*p.y +       p.z) + Dz_BF;
      p.x = x_out;
      p.y = y_out;
      p.z = z_out;
    }
  }, // cs_geocentric_to_wgs84

  /****************************************************************/
  // pj_geocentic_from_wgs84()
  //  coordinate system definition,
  //  point to transform in geocentric coordinates (x,y,z)
  geocentric_from_wgs84 : function( p ) {

    if( this.datum_type == Proj4js.common.PJD_3PARAM )
    {
      //if( x[io] == HUGE_VAL )
      //    continue;
      p.x -= this.datum_params[0];
      p.y -= this.datum_params[1];
      p.z -= this.datum_params[2];

    }
    else if (this.datum_type == Proj4js.common.PJD_7PARAM)
    {
      var Dx_BF =this.datum_params[0];
      var Dy_BF =this.datum_params[1];
      var Dz_BF =this.datum_params[2];
      var Rx_BF =this.datum_params[3];
      var Ry_BF =this.datum_params[4];
      var Rz_BF =this.datum_params[5];
      var M_BF  =this.datum_params[6];
      var x_tmp = (p.x - Dx_BF) / M_BF;
      var y_tmp = (p.y - Dy_BF) / M_BF;
      var z_tmp = (p.z - Dz_BF) / M_BF;
      //if( x[io] == HUGE_VAL )
      //    continue;

      p.x =        x_tmp + Rz_BF*y_tmp - Ry_BF*z_tmp;
      p.y = -Rz_BF*x_tmp +       y_tmp + Rx_BF*z_tmp;
      p.z =  Ry_BF*x_tmp - Rx_BF*y_tmp +       z_tmp;
    } //cs_geocentric_from_wgs84()
  }
});

/** point object, nothing fancy, just allows values to be
    passed back and forth by reference rather than by value.
    Other point classes may be used as long as they have
    x and y properties, which will get modified in the transform method.
*/
Proj4js.Point = Proj4js.Class({

    /**
     * Constructor: Proj4js.Point
     *
     * Parameters:
     * - x {float} or {Array} either the first coordinates component or
     *     the full coordinates
     * - y {float} the second component
     * - z {float} the third component, optional.
     */
    initialize : function(x,y,z) {
      if (typeof x == 'object') {
        this.x = x[0];
        this.y = x[1];
        this.z = x[2] || 0.0;
      } else if (typeof x == 'string' && typeof y == 'undefined') {
        var coords = x.split(',');
        this.x = parseFloat(coords[0]);
        this.y = parseFloat(coords[1]);
        this.z = parseFloat(coords[2]) || 0.0;
      } else {
        this.x = x;
        this.y = y;
        this.z = z || 0.0;
      }
    },

    /**
     * APIMethod: clone
     * Build a copy of a Proj4js.Point object.
     *
     * Return:
     * {Proj4js}.Point the cloned point.
     */
    clone : function() {
      return new Proj4js.Point(this.x, this.y, this.z);
    },

    /**
     * APIMethod: toString
     * Return a readable string version of the point
     *
     * Return:
     * {String} String representation of Proj4js.Point object. 
     *           (ex. <i>"x=5,y=42"</i>)
     */
    toString : function() {
        return ("x=" + this.x + ",y=" + this.y);
    },

    /** 
     * APIMethod: toShortString
     * Return a short string version of the point.
     *
     * Return:
     * {String} Shortened String representation of Proj4js.Point object. 
     *         (ex. <i>"5, 42"</i>)
     */
    toShortString : function() {
        return (this.x + ", " + this.y);
    }
});

Proj4js.PrimeMeridian = {
    "greenwich": 0.0,               //"0dE",
    "lisbon":     -9.131906111111,   //"9d07'54.862\"W",
    "paris":       2.337229166667,   //"2d20'14.025\"E",
    "bogota":    -74.080916666667,  //"74d04'51.3\"W",
    "madrid":     -3.687938888889,  //"3d41'16.58\"W",
    "rome":       12.452333333333,  //"12d27'8.4\"E",
    "bern":        7.439583333333,  //"7d26'22.5\"E",
    "jakarta":   106.807719444444,  //"106d48'27.79\"E",
    "ferro":     -17.666666666667,  //"17d40'W",
    "brussels":    4.367975,        //"4d22'4.71\"E",
    "stockholm":  18.058277777778,  //"18d3'29.8\"E",
    "athens":     23.7163375,       //"23d42'58.815\"E",
    "oslo":       10.722916666667   //"10d43'22.5\"E"
};

Proj4js.Ellipsoid = {
  "MERIT": {a:6378137.0, rf:298.257, ellipseName:"MERIT 1983"},
  "SGS85": {a:6378136.0, rf:298.257, ellipseName:"Soviet Geodetic System 85"},
  "GRS80": {a:6378137.0, rf:298.257222101, ellipseName:"GRS 1980(IUGG, 1980)"},
  "IAU76": {a:6378140.0, rf:298.257, ellipseName:"IAU 1976"},
  "airy": {a:6377563.396, b:6356256.910, ellipseName:"Airy 1830"},
  "APL4.": {a:6378137, rf:298.25, ellipseName:"Appl. Physics. 1965"},
  "NWL9D": {a:6378145.0, rf:298.25, ellipseName:"Naval Weapons Lab., 1965"},
  "mod_airy": {a:6377340.189, b:6356034.446, ellipseName:"Modified Airy"},
  "andrae": {a:6377104.43, rf:300.0, ellipseName:"Andrae 1876 (Den., Iclnd.)"},
  "aust_SA": {a:6378160.0, rf:298.25, ellipseName:"Australian Natl & S. Amer. 1969"},
  "GRS67": {a:6378160.0, rf:298.2471674270, ellipseName:"GRS 67(IUGG 1967)"},
  "bessel": {a:6377397.155, rf:299.1528128, ellipseName:"Bessel 1841"},
  "bess_nam": {a:6377483.865, rf:299.1528128, ellipseName:"Bessel 1841 (Namibia)"},
  "clrk66": {a:6378206.4, b:6356583.8, ellipseName:"Clarke 1866"},
  "clrk80": {a:6378249.145, rf:293.4663, ellipseName:"Clarke 1880 mod."},
  "CPM": {a:6375738.7, rf:334.29, ellipseName:"Comm. des Poids et Mesures 1799"},
  "delmbr": {a:6376428.0, rf:311.5, ellipseName:"Delambre 1810 (Belgium)"},
  "engelis": {a:6378136.05, rf:298.2566, ellipseName:"Engelis 1985"},
  "evrst30": {a:6377276.345, rf:300.8017, ellipseName:"Everest 1830"},
  "evrst48": {a:6377304.063, rf:300.8017, ellipseName:"Everest 1948"},
  "evrst56": {a:6377301.243, rf:300.8017, ellipseName:"Everest 1956"},
  "evrst69": {a:6377295.664, rf:300.8017, ellipseName:"Everest 1969"},
  "evrstSS": {a:6377298.556, rf:300.8017, ellipseName:"Everest (Sabah & Sarawak)"},
  "fschr60": {a:6378166.0, rf:298.3, ellipseName:"Fischer (Mercury Datum) 1960"},
  "fschr60m": {a:6378155.0, rf:298.3, ellipseName:"Fischer 1960"},
  "fschr68": {a:6378150.0, rf:298.3, ellipseName:"Fischer 1968"},
  "helmert": {a:6378200.0, rf:298.3, ellipseName:"Helmert 1906"},
  "hough": {a:6378270.0, rf:297.0, ellipseName:"Hough"},
  "intl": {a:6378388.0, rf:297.0, ellipseName:"International 1909 (Hayford)"},
  "kaula": {a:6378163.0, rf:298.24, ellipseName:"Kaula 1961"},
  "lerch": {a:6378139.0, rf:298.257, ellipseName:"Lerch 1979"},
  "mprts": {a:6397300.0, rf:191.0, ellipseName:"Maupertius 1738"},
  "new_intl": {a:6378157.5, b:6356772.2, ellipseName:"New International 1967"},
  "plessis": {a:6376523.0, rf:6355863.0, ellipseName:"Plessis 1817 (France)"},
  "krass": {a:6378245.0, rf:298.3, ellipseName:"Krassovsky, 1942"},
  "SEasia": {a:6378155.0, b:6356773.3205, ellipseName:"Southeast Asia"},
  "walbeck": {a:6376896.0, b:6355834.8467, ellipseName:"Walbeck"},
  "WGS60": {a:6378165.0, rf:298.3, ellipseName:"WGS 60"},
  "WGS66": {a:6378145.0, rf:298.25, ellipseName:"WGS 66"},
  "WGS72": {a:6378135.0, rf:298.26, ellipseName:"WGS 72"},
  "WGS84": {a:6378137.0, rf:298.257223563, ellipseName:"WGS 84"},
  "sphere": {a:6370997.0, b:6370997.0, ellipseName:"Normal Sphere (r=6370997)"}
};

Proj4js.Datum = {
  "WGS84": {towgs84: "0,0,0", ellipse: "WGS84", datumName: "WGS84"},
  "GGRS87": {towgs84: "-199.87,74.79,246.62", ellipse: "GRS80", datumName: "Greek_Geodetic_Reference_System_1987"},
  "NAD83": {towgs84: "0,0,0", ellipse: "GRS80", datumName: "North_American_Datum_1983"},
  "NAD27": {nadgrids: "@conus,@alaska,@ntv2_0.gsb,@ntv1_can.dat", ellipse: "clrk66", datumName: "North_American_Datum_1927"},
  "potsdam": {towgs84: "606.0,23.0,413.0", ellipse: "bessel", datumName: "Potsdam Rauenberg 1950 DHDN"},
  "carthage": {towgs84: "-263.0,6.0,431.0", ellipse: "clark80", datumName: "Carthage 1934 Tunisia"},
  "hermannskogel": {towgs84: "653.0,-212.0,449.0", ellipse: "bessel", datumName: "Hermannskogel"},
  "ire65": {towgs84: "482.530,-130.596,564.557,-1.042,-0.214,-0.631,8.15", ellipse: "mod_airy", datumName: "Ireland 1965"},
  "nzgd49": {towgs84: "59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993", ellipse: "intl", datumName: "New Zealand Geodetic Datum 1949"},
  "OSGB36": {towgs84: "446.448,-125.157,542.060,0.1502,0.2470,0.8421,-20.4894", ellipse: "airy", datumName: "Airy 1830"}
};

Proj4js.WGS84 = new Proj4js.Proj('WGS84');
Proj4js.Datum['OSB36'] = Proj4js.Datum['OSGB36']; //as returned from spatialreference.org

//lookup table to go from the projection name in WKT to the Proj4js projection name
//build this out as required
Proj4js.wktProjections = {
  "Lambert Tangential Conformal Conic Projection": "lcc",
  "Mercator": "merc",
  "Popular Visualisation Pseudo Mercator": "merc",
  "Transverse_Mercator": "tmerc",
  "Transverse Mercator": "tmerc",
  "Lambert Azimuthal Equal Area": "laea",
  "Universal Transverse Mercator System": "utm"
};


/* ======================================================================
    projCode/aea.js
   ====================================================================== */

/*******************************************************************************
NAME                     ALBERS CONICAL EQUAL AREA 

PURPOSE:	Transforms input longitude and latitude to Easting and Northing
		for the Albers Conical Equal Area projection.  The longitude
		and latitude must be in radians.  The Easting and Northing
		values will be returned in meters.

PROGRAMMER              DATE
----------              ----
T. Mittan,       	Feb, 1992

ALGORITHM REFERENCES

1.  Snyder, John P., "Map Projections--A Working Manual", U.S. Geological
    Survey Professional Paper 1395 (Supersedes USGS Bulletin 1532), United
    State Government Printing Office, Washington D.C., 1987.

2.  Snyder, John P. and Voxland, Philip M., "An Album of Map Projections",
    U.S. Geological Survey Professional Paper 1453 , United State Government
    Printing Office, Washington D.C., 1989.
*******************************************************************************/


Proj4js.Proj.aea = {
  init : function() {

    if (Math.abs(this.lat1 + this.lat2) < Proj4js.common.EPSLN) {
       Proj4js.reportError("aeaInitEqualLatitudes");
       return;
    }
    this.temp = this.b / this.a;
    this.es = 1.0 - Math.pow(this.temp,2);
    this.e3 = Math.sqrt(this.es);

    this.sin_po=Math.sin(this.lat1);
    this.cos_po=Math.cos(this.lat1);
    this.t1=this.sin_po;
    this.con = this.sin_po;
    this.ms1 = Proj4js.common.msfnz(this.e3,this.sin_po,this.cos_po);
    this.qs1 = Proj4js.common.qsfnz(this.e3,this.sin_po,this.cos_po);

    this.sin_po=Math.sin(this.lat2);
    this.cos_po=Math.cos(this.lat2);
    this.t2=this.sin_po;
    this.ms2 = Proj4js.common.msfnz(this.e3,this.sin_po,this.cos_po);
    this.qs2 = Proj4js.common.qsfnz(this.e3,this.sin_po,this.cos_po);

    this.sin_po=Math.sin(this.lat0);
    this.cos_po=Math.cos(this.lat0);
    this.t3=this.sin_po;
    this.qs0 = Proj4js.common.qsfnz(this.e3,this.sin_po,this.cos_po);

    if (Math.abs(this.lat1 - this.lat2) > Proj4js.common.EPSLN) {
      this.ns0 = (this.ms1 * this.ms1 - this.ms2 *this.ms2)/ (this.qs2 - this.qs1);
    } else {
      this.ns0 = this.con;
    }
    this.c = this.ms1 * this.ms1 + this.ns0 * this.qs1;
    this.rh = this.a * Math.sqrt(this.c - this.ns0 * this.qs0)/this.ns0;
  },

/* Albers Conical Equal Area forward equations--mapping lat,long to x,y
  -------------------------------------------------------------------*/
  forward: function(p){

    var lon=p.x;
    var lat=p.y;

    this.sin_phi=Math.sin(lat);
    this.cos_phi=Math.cos(lat);

    var qs = Proj4js.common.qsfnz(this.e3,this.sin_phi,this.cos_phi);
    var rh1 =this.a * Math.sqrt(this.c - this.ns0 * qs)/this.ns0;
    var theta = this.ns0 * Proj4js.common.adjust_lon(lon - this.long0); 
    var x = rh1 * Math.sin(theta) + this.x0;
    var y = this.rh - rh1 * Math.cos(theta) + this.y0;

    p.x = x; 
    p.y = y;
    return p;
  },


  inverse: function(p) {
    var rh1,qs,con,theta,lon,lat;

    p.x -= this.x0;
    p.y = this.rh - p.y + this.y0;
    if (this.ns0 >= 0) {
      rh1 = Math.sqrt(p.x *p.x + p.y * p.y);
      con = 1.0;
    } else {
      rh1 = -Math.sqrt(p.x * p.x + p.y *p.y);
      con = -1.0;
    }
    theta = 0.0;
    if (rh1 != 0.0) {
      theta = Math.atan2(con * p.x, con * p.y);
    }
    con = rh1 * this.ns0 / this.a;
    qs = (this.c - con * con) / this.ns0;
    if (this.e3 >= 1e-10) {
      con = 1 - .5 * (1.0 -this.es) * Math.log((1.0 - this.e3) / (1.0 + this.e3))/this.e3;
      if (Math.abs(Math.abs(con) - Math.abs(qs)) > .0000000001 ) {
          lat = this.phi1z(this.e3,qs);
      } else {
          if (qs >= 0) {
             lat = .5 * PI;
          } else {
             lat = -.5 * PI;
          }
      }
    } else {
      lat = this.phi1z(e3,qs);
    }

    lon = Proj4js.common.adjust_lon(theta/this.ns0 + this.long0);
    p.x = lon;
    p.y = lat;
    return p;
  },
  
/* Function to compute phi1, the latitude for the inverse of the
   Albers Conical Equal-Area projection.
-------------------------------------------*/
  phi1z: function (eccent,qs) {
    var con, com, dphi;
    var phi = Proj4js.common.asinz(.5 * qs);
    if (eccent < Proj4js.common.EPSLN) return phi;
    
    var eccnts = eccent * eccent; 
    for (var i = 1; i <= 25; i++) {
        sinphi = Math.sin(phi);
        cosphi = Math.cos(phi);
        con = eccent * sinphi; 
        com = 1.0 - con * con;
        dphi = .5 * com * com / cosphi * (qs / (1.0 - eccnts) - sinphi / com + .5 / eccent * Math.log((1.0 - con) / (1.0 + con)));
        phi = phi + dphi;
        if (Math.abs(dphi) <= 1e-7) return phi;
    }
    Proj4js.reportError("aea:phi1z:Convergence error");
    return null;
  }
  
};



/* ======================================================================
    projCode/sterea.js
   ====================================================================== */


Proj4js.Proj.sterea = {
  dependsOn : 'gauss',

  init : function() {
    Proj4js.Proj['gauss'].init.apply(this);
    if (!this.rc) {
      Proj4js.reportError("sterea:init:E_ERROR_0");
      return;
    }
    this.sinc0 = Math.sin(this.phic0);
    this.cosc0 = Math.cos(this.phic0);
    this.R2 = 2.0 * this.rc;
    if (!this.title) this.title = "Oblique Stereographic Alternative";
  },

  forward : function(p) {
    p.x = Proj4js.common.adjust_lon(p.x-this.long0); /* adjust del longitude */
    Proj4js.Proj['gauss'].forward.apply(this, [p]);
    sinc = Math.sin(p.y);
    cosc = Math.cos(p.y);
    cosl = Math.cos(p.x);
    k = this.k0 * this.R2 / (1.0 + this.sinc0 * sinc + this.cosc0 * cosc * cosl);
    p.x = k * cosc * Math.sin(p.x);
    p.y = k * (this.cosc0 * sinc - this.sinc0 * cosc * cosl);
    p.x = this.a * p.x + this.x0;
    p.y = this.a * p.y + this.y0;
    return p;
  },

  inverse : function(p) {
    var lon,lat;
    p.x = (p.x - this.x0) / this.a; /* descale and de-offset */
    p.y = (p.y - this.y0) / this.a;

    p.x /= this.k0;
    p.y /= this.k0;
    if ( (rho = Math.sqrt(p.x*p.x + p.y*p.y)) ) {
      c = 2.0 * Math.atan2(rho, this.R2);
      sinc = Math.sin(c);
      cosc = Math.cos(c);
      lat = Math.asin(cosc * this.sinc0 + p.y * sinc * this.cosc0 / rho);
      lon = Math.atan2(p.x * sinc, rho * this.cosc0 * cosc - p.y * this.sinc0 * sinc);
    } else {
      lat = this.phic0;
      lon = 0.;
    }

    p.x = lon;
    p.y = lat;
    Proj4js.Proj['gauss'].inverse.apply(this,[p]);
    p.x = Proj4js.common.adjust_lon(p.x + this.long0); /* adjust longitude to CM */
    return p;
  }
};

/* ======================================================================
    projCode/poly.js
   ====================================================================== */

/* Function to compute, phi4, the latitude for the inverse of the
   Polyconic projection.
------------------------------------------------------------*/
function phi4z (eccent,e0,e1,e2,e3,a,b,c,phi) {
	var sinphi, sin2ph, tanph, ml, mlp, con1, con2, con3, dphi, i;

	phi = a;
	for (i = 1; i <= 15; i++) {
		sinphi = Math.sin(phi);
		tanphi = Math.tan(phi);
		c = tanphi * Math.sqrt (1.0 - eccent * sinphi * sinphi);
		sin2ph = Math.sin (2.0 * phi);
		/*
		ml = e0 * *phi - e1 * sin2ph + e2 * sin (4.0 *  *phi);
		mlp = e0 - 2.0 * e1 * cos (2.0 *  *phi) + 4.0 * e2 *  cos (4.0 *  *phi);
		*/
		ml = e0 * phi - e1 * sin2ph + e2 * Math.sin (4.0 *  phi) - e3 * Math.sin (6.0 * phi);
		mlp = e0 - 2.0 * e1 * Math.cos (2.0 *  phi) + 4.0 * e2 * Math.cos (4.0 *  phi) - 6.0 * e3 * Math.cos (6.0 *  phi);
		con1 = 2.0 * ml + c * (ml * ml + b) - 2.0 * a *  (c * ml + 1.0);
		con2 = eccent * sin2ph * (ml * ml + b - 2.0 * a * ml) / (2.0 *c);
		con3 = 2.0 * (a - ml) * (c * mlp - 2.0 / sin2ph) - 2.0 * mlp;
		dphi = con1 / (con2 + con3);
		phi += dphi;
		if (Math.abs(dphi) <= .0000000001 ) return(phi);   
	}
	Proj4js.reportError("phi4z: No convergence");
	return null;
}


/* Function to compute the constant e4 from the input of the eccentricity
   of the spheroid, x.  This constant is used in the Polar Stereographic
   projection.
--------------------------------------------------------------------*/
function e4fn(x) {
	var con, com;
	con = 1.0 + x;
	com = 1.0 - x;
	return (Math.sqrt((Math.pow(con,con))*(Math.pow(com,com))));
}





/*******************************************************************************
NAME                             POLYCONIC 

PURPOSE:	Transforms input longitude and latitude to Easting and
		Northing for the Polyconic projection.  The
		longitude and latitude must be in radians.  The Easting
		and Northing values will be returned in meters.

PROGRAMMER              DATE
----------              ----
T. Mittan		Mar, 1993

ALGORITHM REFERENCES

1.  Snyder, John P., "Map Projections--A Working Manual", U.S. Geological
    Survey Professional Paper 1395 (Supersedes USGS Bulletin 1532), United
    State Government Printing Office, Washington D.C., 1987.

2.  Snyder, John P. and Voxland, Philip M., "An Album of Map Projections",
    U.S. Geological Survey Professional Paper 1453 , United State Government
    Printing Office, Washington D.C., 1989.
*******************************************************************************/

Proj4js.Proj.poly = {

	/* Initialize the POLYCONIC projection
	  ----------------------------------*/
	init: function() {
		var temp;			/* temporary variable		*/
		if (this.lat0=0) this.lat0=90;//this.lat0 ca

		/* Place parameters in static storage for common use
		  -------------------------------------------------*/
		this.temp = this.b / this.a;
		this.es = 1.0 - Math.pow(this.temp,2);// devait etre dans tmerc.js mais n y est pas donc je commente sinon retour de valeurs nulles 
		this.e = Math.sqrt(this.es);
		this.e0 = Proj4js.common.e0fn(this.es);
		this.e1 = Proj4js.common.e1fn(this.es);
		this.e2 = Proj4js.common.e2fn(this.es);
		this.e3 = Proj4js.common.e3fn(this.es);
		this.ml0 = Proj4js.common.mlfn(this.e0, this.e1,this.e2, this.e3, this.lat0);//si que des zeros le calcul ne se fait pas
		//if (!this.ml0) {this.ml0=0;}
	},


	/* Polyconic forward equations--mapping lat,long to x,y
	  ---------------------------------------------------*/
	forward: function(p) {
		var sinphi, cosphi;	/* sin and cos value				*/
		var al;				/* temporary values				*/
		var c;				/* temporary values				*/
		var con, ml;		/* cone constant, small m			*/
		var ms;				/* small m					*/
		var x,y;

		var lon=p.x;
		var lat=p.y;	

		con = Proj4js.common.adjust_lon(lon - this.long0);
		if (Math.abs(lat) <= .0000001) {
			x = this.x0 + this.a * con;
			y = this.y0 - this.a * this.ml0;
		} else {
			sinphi = Math.sin(lat);
			cosphi = Math.cos(lat);	   

			ml = Proj4js.common.mlfn(this.e0, this.e1, this.e2, this.e3, lat);
			ms = Proj4js.common.msfnz(this.e,sinphi,cosphi);
			con = sinphi;
			x = this.x0 + this.a * ms * Math.sin(con)/sinphi;
			y = this.y0 + this.a * (ml - this.ml0 + ms * (1.0 - Math.cos(con))/sinphi);
		}

		p.x=x;
		p.y=y;   
		return p;
	},


	/* Inverse equations
	-----------------*/
	inverse: function(p) {
		var sin_phi, cos_phi;	/* sin and cos value				*/
		var al;					/* temporary values				*/
		var b;					/* temporary values				*/
		var c;					/* temporary values				*/
		var con, ml;			/* cone constant, small m			*/
		var iflg;				/* error flag					*/
		var lon,lat;
		p.x -= this.x0;
		p.y -= this.y0;
		al = this.ml0 + p.y/this.a;
		iflg = 0;

		if (Math.abs(al) <= .0000001) {
			lon = p.x/this.a + this.long0;
			lat = 0.0;
		} else {
			b = al * al + (p.x/this.a) * (p.x/this.a);
			iflg = phi4z(this.es,this.e0,this.e1,this.e2,this.e3,this.al,b,c,lat);
			if (iflg != 1) return(iflg);
			lon = Proj4js.common.adjust_lon((Proj4js.common.asinz(p.x * c / this.a) / Math.sin(lat)) + this.long0);
		}

		p.x=lon;
		p.y=lat;
		return p;
	}
};



/* ======================================================================
    projCode/equi.js
   ====================================================================== */

/*******************************************************************************
NAME                             EQUIRECTANGULAR 

PURPOSE:	Transforms input longitude and latitude to Easting and
		Northing for the Equirectangular projection.  The
		longitude and latitude must be in radians.  The Easting
		and Northing values will be returned in meters.

PROGRAMMER              DATE
----------              ----
T. Mittan		Mar, 1993

ALGORITHM REFERENCES

1.  Snyder, John P., "Map Projections--A Working Manual", U.S. Geological
    Survey Professional Paper 1395 (Supersedes USGS Bulletin 1532), United
    State Government Printing Office, Washington D.C., 1987.

2.  Snyder, John P. and Voxland, Philip M., "An Album of Map Projections",
    U.S. Geological Survey Professional Paper 1453 , United State Government
    Printing Office, Washington D.C., 1989.
*******************************************************************************/
Proj4js.Proj.equi = {

  init: function() {
    if(!this.x0) this.x0=0;
    if(!this.y0) this.y0=0;
    if(!this.lat0) this.lat0=0;
    if(!this.long0) this.long0=0;
    ///this.t2;
  },



/* Equirectangular forward equations--mapping lat,long to x,y
  ---------------------------------------------------------*/
  forward: function(p) {

    var lon=p.x;				
    var lat=p.y;			

    var dlon = Proj4js.common.adjust_lon(lon - this.long0);
    var x = this.x0 +this. a * dlon *Math.cos(this.lat0);
    var y = this.y0 + this.a * lat;

    this.t1=x;
    this.t2=Math.cos(this.lat0);
    p.x=x;
    p.y=y;
    return p;
  },  //equiFwd()



/* Equirectangular inverse equations--mapping x,y to lat/long
  ---------------------------------------------------------*/
  inverse: function(p) {

    p.x -= this.x0;
    p.y -= this.y0;
    var lat = p.y /this. a;

    if ( Math.abs(lat) > Proj4js.common.HALF_PI) {
        Proj4js.reportError("equi:Inv:DataError");
    }
    var lon = Proj4js.common.adjust_lon(this.long0 + p.x / (this.a * Math.cos(this.lat0)));
    p.x=lon;
    p.y=lat;
  }//equiInv()
};


/* ======================================================================
    projCode/merc.js
   ====================================================================== */

/*******************************************************************************
NAME                            MERCATOR

PURPOSE:	Transforms input longitude and latitude to Easting and
		Northing for the Mercator projection.  The
		longitude and latitude must be in radians.  The Easting
		and Northing values will be returned in meters.

PROGRAMMER              DATE
----------              ----
D. Steinwand, EROS      Nov, 1991
T. Mittan		Mar, 1993

ALGORITHM REFERENCES

1.  Snyder, John P., "Map Projections--A Working Manual", U.S. Geological
    Survey Professional Paper 1395 (Supersedes USGS Bulletin 1532), United
    State Government Printing Office, Washington D.C., 1987.

2.  Snyder, John P. and Voxland, Philip M., "An Album of Map Projections",
    U.S. Geological Survey Professional Paper 1453 , United State Government
    Printing Office, Washington D.C., 1989.
*******************************************************************************/

//static double r_major = a;		   /* major axis 				*/
//static double r_minor = b;		   /* minor axis 				*/
//static double lon_center = long0;	   /* Center longitude (projection center) */
//static double lat_origin =  lat0;	   /* center latitude			*/
//static double e,es;		           /* eccentricity constants		*/
//static double m1;		               /* small value m			*/
//static double false_northing = y0;   /* y offset in meters			*/
//static double false_easting = x0;	   /* x offset in meters			*/
//scale_fact = k0 

Proj4js.Proj.merc = {
  init : function() {
	//?this.temp = this.r_minor / this.r_major;
	//this.temp = this.b / this.a;
	//this.es = 1.0 - Math.sqrt(this.temp);
	//this.e = Math.sqrt( this.es );
	//?this.m1 = Math.cos(this.lat_origin) / (Math.sqrt( 1.0 - this.es * Math.sin(this.lat_origin) * Math.sin(this.lat_origin)));
	//this.m1 = Math.cos(0.0) / (Math.sqrt( 1.0 - this.es * Math.sin(0.0) * Math.sin(0.0)));
    if (this.lat_ts) {
      if (this.sphere) {
        this.k0 = Math.cos(this.lat_ts);
      } else {
        this.k0 = Proj4js.common.msfnz(this.es, Math.sin(this.lat_ts), Math.cos(this.lat_ts));
      }
    }
  },

/* Mercator forward equations--mapping lat,long to x,y
  --------------------------------------------------*/

  forward : function(p) {	
    //alert("ll2m coords : "+coords);
    var lon = p.x;
    var lat = p.y;
    // convert to radians
    if ( lat*Proj4js.common.R2D > 90.0 && 
          lat*Proj4js.common.R2D < -90.0 && 
          lon*Proj4js.common.R2D > 180.0 && 
          lon*Proj4js.common.R2D < -180.0) {
      Proj4js.reportError("merc:forward: llInputOutOfRange: "+ lon +" : " + lat);
      return null;
    }

    var x,y;
    if(Math.abs( Math.abs(lat) - Proj4js.common.HALF_PI)  <= Proj4js.common.EPSLN) {
      Proj4js.reportError("merc:forward: ll2mAtPoles");
      return null;
    } else {
      if (this.sphere) {
        x = this.x0 + this.a * this.k0 * Proj4js.common.adjust_lon(lon - this.long0);
        y = this.y0 + this.a * this.k0 * Math.log(Math.tan(Proj4js.common.FORTPI + 0.5*lat));
      } else {
        var sinphi = Math.sin(lat);
        var ts = Proj4js.common.tsfnz(this.e,lat,sinphi);
        x = this.x0 + this.a * this.k0 * Proj4js.common.adjust_lon(lon - this.long0);
        y = this.y0 - this.a * this.k0 * Math.log(ts);
      }
      p.x = x; 
      p.y = y;
      return p;
    }
  },


  /* Mercator inverse equations--mapping x,y to lat/long
  --------------------------------------------------*/
  inverse : function(p) {	

    var x = p.x - this.x0;
    var y = p.y - this.y0;
    var lon,lat;

    if (this.sphere) {
      lat = Proj4js.common.HALF_PI - 2.0 * Math.atan(Math.exp(-y / this.a * this.k0));
    } else {
      var ts = Math.exp(-y / (this.a * this.k0));
      lat = Proj4js.common.phi2z(this.e,ts);
      if(lat == -9999) {
        Proj4js.reportError("merc:inverse: lat = -9999");
        return null;
      }
    }
    lon = Proj4js.common.adjust_lon(this.long0+ x / (this.a * this.k0));

    p.x = lon;
    p.y = lat;
    return p;
  }
};


/* ======================================================================
    projCode/utm.js
   ====================================================================== */

/*******************************************************************************
NAME                            TRANSVERSE MERCATOR

PURPOSE:	Transforms input longitude and latitude to Easting and
		Northing for the Transverse Mercator projection.  The
		longitude and latitude must be in radians.  The Easting
		and Northing values will be returned in meters.

ALGORITHM REFERENCES

1.  Snyder, John P., "Map Projections--A Working Manual", U.S. Geological
    Survey Professional Paper 1395 (Supersedes USGS Bulletin 1532), United
    State Government Printing Office, Washington D.C., 1987.

2.  Snyder, John P. and Voxland, Philip M., "An Album of Map Projections",
    U.S. Geological Survey Professional Paper 1453 , United State Government
    Printing Office, Washington D.C., 1989.
*******************************************************************************/


/**
  Initialize Transverse Mercator projection
*/

Proj4js.Proj.utm = {
  dependsOn : 'tmerc',

  init : function() {
    if (!this.zone) {
      Proj4js.reportError("utm:init: zone must be specified for UTM");
      return;
    }
    this.lat0 = 0.0;
    this.long0 = ((6 * Math.abs(this.zone)) - 183) * Proj4js.common.D2R;
    this.x0 = 500000.0;
    this.y0 = this.utmSouth ? 10000000.0 : 0.0;
    this.k0 = 0.9996;

    Proj4js.Proj['tmerc'].init.apply(this);
    this.forward = Proj4js.Proj['tmerc'].forward;
    this.inverse = Proj4js.Proj['tmerc'].inverse;
  }
};
/* ======================================================================
    projCode/eqdc.js
   ====================================================================== */

/*******************************************************************************
NAME                            EQUIDISTANT CONIC 

PURPOSE:	Transforms input longitude and latitude to Easting and Northing
		for the Equidistant Conic projection.  The longitude and
		latitude must be in radians.  The Easting and Northing values
		will be returned in meters.

PROGRAMMER              DATE
----------              ----
T. Mittan		Mar, 1993

ALGORITHM REFERENCES

1.  Snyder, John P., "Map Projections--A Working Manual", U.S. Geological
    Survey Professional Paper 1395 (Supersedes USGS Bulletin 1532), United
    State Government Printing Office, Washington D.C., 1987.

2.  Snyder, John P. and Voxland, Philip M., "An Album of Map Projections",
    U.S. Geological Survey Professional Paper 1453 , United State Government
    Printing Office, Washington D.C., 1989.
*******************************************************************************/

/* Variables common to all subroutines in this code file
  -----------------------------------------------------*/

Proj4js.Proj.eqdc = {

/* Initialize the Equidistant Conic projection
  ------------------------------------------*/
  init: function() {

    /* Place parameters in static storage for common use
      -------------------------------------------------*/

    if(!this.mode) this.mode=0;//chosen default mode
    this.temp = this.b / this.a;
    this.es = 1.0 - Math.pow(this.temp,2);
    this.e = Math.sqrt(this.es);
    this.e0 = Proj4js.common.e0fn(this.es);
    this.e1 = Proj4js.common.e1fn(this.es);
    this.e2 = Proj4js.common.e2fn(this.es);
    this.e3 = Proj4js.common.e3fn(this.es);

    this.sinphi=Math.sin(this.lat1);
    this.cosphi=Math.cos(this.lat1);

    this.ms1 = Proj4js.common.msfnz(this.e,this.sinphi,this.cosphi);
    this.ml1 = Proj4js.common.mlfn(this.e0, this.e1, this.e2,this.e3, this.lat1);

    /* format B
    ---------*/
    if (this.mode != 0) {
      if (Math.abs(this.lat1 + this.lat2) < Proj4js.common.EPSLN) {
            Proj4js.reportError("eqdc:Init:EqualLatitudes");
            //return(81);
       }
       this.sinphi=Math.sin(this.lat2);
       this.cosphi=Math.cos(this.lat2);   

       this.ms2 = Proj4js.common.msfnz(this.e,this.sinphi,this.cosphi);
       this.ml2 = Proj4js.common.mlfn(this.e0, this.e1, this.e2, this.e3, this.lat2);
       if (Math.abs(this.lat1 - this.lat2) >= Proj4js.common.EPSLN) {
         this.ns = (this.ms1 - this.ms2) / (this.ml2 - this.ml1);
       } else {
          this.ns = this.sinphi;
       }
    } else {
      this.ns = this.sinphi;
    }
    this.g = this.ml1 + this.ms1/this.ns;
    this.ml0 = Proj4js.common.mlfn(this.e0, this.e1,this. e2, this.e3, this.lat0);
    this.rh = this.a * (this.g - this.ml0);
  },


/* Equidistant Conic forward equations--mapping lat,long to x,y
  -----------------------------------------------------------*/
  forward: function(p) {
    var lon=p.x;
    var lat=p.y;

    /* Forward equations
      -----------------*/
    var ml = Proj4js.common.mlfn(this.e0, this.e1, this.e2, this.e3, lat);
    var rh1 = this.a * (this.g - ml);
    var theta = this.ns * Proj4js.common.adjust_lon(lon - this.long0);

    var x = this.x0  + rh1 * Math.sin(theta);
    var y = this.y0 + this.rh - rh1 * Math.cos(theta);
    p.x=x;
    p.y=y;
    return p;
  },

/* Inverse equations
  -----------------*/
  inverse: function(p) {
    p.x -= this.x0;
    p.y  = this.rh - p.y + this.y0;
    var con, rh1;
    if (this.ns >= 0) {
       var rh1 = Math.sqrt(p.x *p.x + p.y * p.y); 
       var con = 1.0;
    } else {
       rh1 = -Math.sqrt(p.x *p. x +p. y * p.y); 
       con = -1.0;
    }
    var theta = 0.0;
    if (rh1 != 0.0) theta = Math.atan2(con *p.x, con *p.y);
    var ml = this.g - rh1 /this.a;
    var lat = this.phi3z(ml,this.e0,this.e1,this.e2,this.e3);
    var lon = Proj4js.common.adjust_lon(this.long0 + theta / this.ns);

     p.x=lon;
     p.y=lat;  
     return p;
    },
    
/* Function to compute latitude, phi3, for the inverse of the Equidistant
   Conic projection.
-----------------------------------------------------------------*/
  phi3z: function(ml,e0,e1,e2,e3) {
    var phi;
    var dphi;

    phi = ml;
    for (var i = 0; i < 15; i++) {
      dphi = (ml + e1 * Math.sin(2.0 * phi) - e2 * Math.sin(4.0 * phi) + e3 * Math.sin(6.0 * phi))/ e0 - phi;
      phi += dphi;
      if (Math.abs(dphi) <= .0000000001) {
        return phi;
      }
    }
    Proj4js.reportError("PHI3Z-CONV:Latitude failed to converge after 15 iterations");
    return null;
  }

    
};
/* ======================================================================
    projCode/tmerc.js
   ====================================================================== */

/*******************************************************************************
NAME                            TRANSVERSE MERCATOR

PURPOSE:	Transforms input longitude and latitude to Easting and
		Northing for the Transverse Mercator projection.  The
		longitude and latitude must be in radians.  The Easting
		and Northing values will be returned in meters.

ALGORITHM REFERENCES

1.  Snyder, John P., "Map Projections--A Working Manual", U.S. Geological
    Survey Professional Paper 1395 (Supersedes USGS Bulletin 1532), United
    State Government Printing Office, Washington D.C., 1987.

2.  Snyder, John P. and Voxland, Philip M., "An Album of Map Projections",
    U.S. Geological Survey Professional Paper 1453 , United State Government
    Printing Office, Washington D.C., 1989.
*******************************************************************************/


/**
  Initialize Transverse Mercator projection
*/

Proj4js.Proj.tmerc = {
  init : function() {
    this.e0 = Proj4js.common.e0fn(this.es);
    this.e1 = Proj4js.common.e1fn(this.es);
    this.e2 = Proj4js.common.e2fn(this.es);
    this.e3 = Proj4js.common.e3fn(this.es);
    this.ml0 = this.a * Proj4js.common.mlfn(this.e0, this.e1, this.e2, this.e3, this.lat0);
  },

  /**
    Transverse Mercator Forward  - long/lat to x/y
    long/lat in radians
  */
  forward : function(p) {
    var lon = p.x;
    var lat = p.y;

    var delta_lon = Proj4js.common.adjust_lon(lon - this.long0); // Delta longitude
    var con;    // cone constant
    var x, y;
    var sin_phi=Math.sin(lat);
    var cos_phi=Math.cos(lat);

    if (this.sphere) {  /* spherical form */
      var b = cos_phi * Math.sin(delta_lon);
      if ((Math.abs(Math.abs(b) - 1.0)) < .0000000001)  {
        Proj4js.reportError("tmerc:forward: Point projects into infinity");
        return(93);
      } else {
        x = .5 * this.a * this.k0 * Math.log((1.0 + b)/(1.0 - b));
        con = Math.acos(cos_phi * Math.cos(delta_lon)/Math.sqrt(1.0 - b*b));
        if (lat < 0) con = - con;
        y = this.a * this.k0 * (con - this.lat0);
      }
    } else {
      var al  = cos_phi * delta_lon;
      var als = Math.pow(al,2);
      var c   = this.ep2 * Math.pow(cos_phi,2);
      var tq  = Math.tan(lat);
      var t   = Math.pow(tq,2);
      con = 1.0 - this.es * Math.pow(sin_phi,2);
      var n   = this.a / Math.sqrt(con);
      var ml  = this.a * Proj4js.common.mlfn(this.e0, this.e1, this.e2, this.e3, lat);

      x = this.k0 * n * al * (1.0 + als / 6.0 * (1.0 - t + c + als / 20.0 * (5.0 - 18.0 * t + Math.pow(t,2) + 72.0 * c - 58.0 * this.ep2))) + this.x0;
      y = this.k0 * (ml - this.ml0 + n * tq * (als * (0.5 + als / 24.0 * (5.0 - t + 9.0 * c + 4.0 * Math.pow(c,2) + als / 30.0 * (61.0 - 58.0 * t + Math.pow(t,2) + 600.0 * c - 330.0 * this.ep2))))) + this.y0;

    }
    p.x = x; p.y = y;
    return p;
  }, // tmercFwd()

  /**
    Transverse Mercator Inverse  -  x/y to long/lat
  */
  inverse : function(p) {
    var con, phi;  /* temporary angles       */
    var delta_phi; /* difference between longitudes    */
    var i;
    var max_iter = 6;      /* maximun number of iterations */
    var lat, lon;

    if (this.sphere) {   /* spherical form */
      var f = Math.exp(p.x/(this.a * this.k0));
      var g = .5 * (f - 1/f);
      var temp = this.lat0 + p.y/(this.a * this.k0);
      var h = Math.cos(temp);
      con = Math.sqrt((1.0 - h * h)/(1.0 + g * g));
      lat = Proj4js.common.asinz(con);
      if (temp < 0)
        lat = -lat;
      if ((g == 0) && (h == 0)) {
        lon = this.long0;
      } else {
        lon = Proj4js.common.adjust_lon(Math.atan2(g,h) + this.long0);
      }
    } else {    // ellipsoidal form
      var x = p.x - this.x0;
      var y = p.y - this.y0;

      con = (this.ml0 + y / this.k0) / this.a;
      phi = con;
      for (i=0;true;i++) {
        delta_phi=((con + this.e1 * Math.sin(2.0*phi) - this.e2 * Math.sin(4.0*phi) + this.e3 * Math.sin(6.0*phi)) / this.e0) - phi;
        phi += delta_phi;
        if (Math.abs(delta_phi) <= Proj4js.common.EPSLN) break;
        if (i >= max_iter) {
          Proj4js.reportError("tmerc:inverse: Latitude failed to converge");
          return(95);
        }
      } // for()
      if (Math.abs(phi) < Proj4js.common.HALF_PI) {
        // sincos(phi, &sin_phi, &cos_phi);
        var sin_phi=Math.sin(phi);
        var cos_phi=Math.cos(phi);
        var tan_phi = Math.tan(phi);
        var c = this.ep2 * Math.pow(cos_phi,2);
        var cs = Math.pow(c,2);
        var t = Math.pow(tan_phi,2);
        var ts = Math.pow(t,2);
        con = 1.0 - this.es * Math.pow(sin_phi,2);
        var n = this.a / Math.sqrt(con);
        var r = n * (1.0 - this.es) / con;
        var d = x / (n * this.k0);
        var ds = Math.pow(d,2);
        lat = phi - (n * tan_phi * ds / r) * (0.5 - ds / 24.0 * (5.0 + 3.0 * t + 10.0 * c - 4.0 * cs - 9.0 * this.ep2 - ds / 30.0 * (61.0 + 90.0 * t + 298.0 * c + 45.0 * ts - 252.0 * this.ep2 - 3.0 * cs)));
        lon = Proj4js.common.adjust_lon(this.long0 + (d * (1.0 - ds / 6.0 * (1.0 + 2.0 * t + c - ds / 20.0 * (5.0 - 2.0 * c + 28.0 * t - 3.0 * cs + 8.0 * this.ep2 + 24.0 * ts))) / cos_phi));
      } else {
        lat = Proj4js.common.HALF_PI * Proj4js.common.sign(y);
        lon = this.long0;
      }
    }
    p.x = lon;
    p.y = lat;
    return p;
  } // tmercInv()
};
/* ======================================================================
    defs/GOOGLE.js
   ====================================================================== */

Proj4js.defs["GOOGLE"]="+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +no_defs";
Proj4js.defs["EPSG:900913"]=Proj4js.defs["GOOGLE"];
/* ======================================================================
    projCode/gstmerc.js
   ====================================================================== */

Proj4js.Proj.gstmerc = {
  init : function() {

    // array of:  a, b, lon0, lat0, k0, x0, y0
      var temp= this.b / this.a;
      this.e= Math.sqrt(1.0 - temp*temp);
      this.lc= this.long0;
      this.rs= Math.sqrt(1.0+this.e*this.e*Math.pow(Math.cos(this.lat0),4.0)/(1.0-this.e*this.e));
      var sinz= Math.sin(this.lat0);
      var pc= Math.asin(sinz/this.rs);
      var sinzpc= Math.sin(pc);
      this.cp= Proj4js.common.latiso(0.0,pc,sinzpc)-this.rs*Proj4js.common.latiso(this.e,this.lat0,sinz);
      this.n2= this.k0*this.a*Math.sqrt(1.0-this.e*this.e)/(1.0-this.e*this.e*sinz*sinz);
      this.xs= this.x0;
      this.ys= this.y0-this.n2*pc;

      if (!this.title) this.title = "Gauss Schreiber transverse mercator";
    },


    // forward equations--mapping lat,long to x,y
    // -----------------------------------------------------------------
    forward : function(p) {

      var lon= p.x;
      var lat= p.y;

      var L= this.rs*(lon-this.lc);
      var Ls= this.cp+(this.rs*Proj4js.common.latiso(this.e,lat,Math.sin(lat)));
      var lat1= Math.asin(Math.sin(L)/Proj4js.common.cosh(Ls));
      var Ls1= Proj4js.common.latiso(0.0,lat1,Math.sin(lat1));
      p.x= this.xs+(this.n2*Ls1);
      p.y= this.ys+(this.n2*Math.atan(Proj4js.common.sinh(Ls)/Math.cos(L)));
      return p;
    },

  // inverse equations--mapping x,y to lat/long
  // -----------------------------------------------------------------
  inverse : function(p) {

    var x= p.x;
    var y= p.y;

    var L= Math.atan(Proj4js.common.sinh((x-this.xs)/this.n2)/Math.cos((y-this.ys)/this.n2));
    var lat1= Math.asin(Math.sin((y-this.ys)/this.n2)/Proj4js.common.cosh((x-this.xs)/this.n2));
    var LC= Proj4js.common.latiso(0.0,lat1,Math.sin(lat1));
    p.x= this.lc+L/this.rs;
    p.y= Proj4js.common.invlatiso(this.e,(LC-this.cp)/this.rs);
    return p;
  }

};
/* ======================================================================
    projCode/ortho.js
   ====================================================================== */

/*******************************************************************************
NAME                             ORTHOGRAPHIC 

PURPOSE:	Transforms input longitude and latitude to Easting and
		Northing for the Orthographic projection.  The
		longitude and latitude must be in radians.  The Easting
		and Northing values will be returned in meters.

PROGRAMMER              DATE
----------              ----
T. Mittan		Mar, 1993

ALGORITHM REFERENCES

1.  Snyder, John P., "Map Projections--A Working Manual", U.S. Geological
    Survey Professional Paper 1395 (Supersedes USGS Bulletin 1532), United
    State Government Printing Office, Washington D.C., 1987.

2.  Snyder, John P. and Voxland, Philip M., "An Album of Map Projections",
    U.S. Geological Survey Professional Paper 1453 , United State Government
    Printing Office, Washington D.C., 1989.
*******************************************************************************/

Proj4js.Proj.ortho = {

  /* Initialize the Orthographic projection
    -------------------------------------*/
  init: function(def) {
    //double temp;			/* temporary variable		*/

    /* Place parameters in static storage for common use
      -------------------------------------------------*/;
    this.sin_p14=Math.sin(this.lat0);
    this.cos_p14=Math.cos(this.lat0);	
  },


  /* Orthographic forward equations--mapping lat,long to x,y
    ---------------------------------------------------*/
  forward: function(p) {
    var sinphi, cosphi;	/* sin and cos value				*/
    var dlon;		/* delta longitude value			*/
    var coslon;		/* cos of longitude				*/
    var ksp;		/* scale factor					*/
    var g;		
    var lon=p.x;
    var lat=p.y;	
    /* Forward equations
      -----------------*/
    dlon = Proj4js.common.adjust_lon(lon - this.long0);

    sinphi=Math.sin(lat);
    cosphi=Math.cos(lat);	

    coslon = Math.cos(dlon);
    g = this.sin_p14 * sinphi + this.cos_p14 * cosphi * coslon;
    ksp = 1.0;
    if ((g > 0) || (Math.abs(g) <= Proj4js.common.EPSLN)) {
      var x = this.a * ksp * cosphi * Math.sin(dlon);
      var y = this.y0 + this.a * ksp * (this.cos_p14 * sinphi - this.sin_p14 * cosphi * coslon);
    } else {
      Proj4js.reportError("orthoFwdPointError");
    }
    p.x=x;
    p.y=y;
    return p;
  },


  inverse: function(p) {
    var rh;		/* height above ellipsoid			*/
    var z;		/* angle					*/
    var sinz,cosz;	/* sin of z and cos of z			*/
    var temp;
    var con;
    var lon , lat;
    /* Inverse equations
      -----------------*/
    p.x -= this.x0;
    p.y -= this.y0;
    rh = Math.sqrt(p.x * p.x + p.y * p.y);
    if (rh > this.a + .0000001) {
      Proj4js.reportError("orthoInvDataError");
    }
    z = Proj4js.common.asinz(rh / this.a);

    sinz=Math.sin(z);
    cosz=Math.cos(z);

    lon = this.long0;
    if (Math.abs(rh) <= Proj4js.common.EPSLN) {
      lat = this.lat0; 
    }
    lat = Proj4js.common.asinz(cosz * this.sin_p14 + (p.y * sinz * this.cos_p14)/rh);
    con = Math.abs(this.lat0) - Proj4js.common.HALF_PI;
    if (Math.abs(con) <= Proj4js.common.EPSLN) {
       if (this.lat0 >= 0) {
          lon = Proj4js.common.adjust_lon(this.long0 + Math.atan2(p.x, -p.y));
       } else {
          lon = Proj4js.common.adjust_lon(this.long0 -Math.atan2(-p.x, p.y));
       }
    }
    con = cosz - this.sin_p14 * Math.sin(lat);
    p.x=lon;
    p.y=lat;
    return p;
  }
};


/* ======================================================================
    projCode/somerc.js
   ====================================================================== */

/*******************************************************************************
NAME                       SWISS OBLIQUE MERCATOR

PURPOSE:	Swiss projection.
WARNING:  X and Y are inverted (weird) in the swiss coordinate system. Not
   here, since we want X to be horizontal and Y vertical.

ALGORITHM REFERENCES
1. "Formules et constantes pour le Calcul pour la
 projection cylindrique conforme à axe oblique et pour la transformation entre
 des systèmes de référence".
 http://www.swisstopo.admin.ch/internet/swisstopo/fr/home/topics/survey/sys/refsys/switzerland.parsysrelated1.31216.downloadList.77004.DownloadFile.tmp/swissprojectionfr.pdf

*******************************************************************************/

Proj4js.Proj.somerc = {

  init: function() {
    var phy0 = this.lat0;
    this.lambda0 = this.long0;
    var sinPhy0 = Math.sin(phy0);
    var semiMajorAxis = this.a;
    var invF = this.rf;
    var flattening = 1 / invF;
    var e2 = 2 * flattening - Math.pow(flattening, 2);
    var e = this.e = Math.sqrt(e2);
    this.R = this.k0 * semiMajorAxis * Math.sqrt(1 - e2) / (1 - e2 * Math.pow(sinPhy0, 2.0));
    this.alpha = Math.sqrt(1 + e2 / (1 - e2) * Math.pow(Math.cos(phy0), 4.0));
    this.b0 = Math.asin(sinPhy0 / this.alpha);
    this.K = Math.log(Math.tan(Math.PI / 4.0 + this.b0 / 2.0))
            - this.alpha
            * Math.log(Math.tan(Math.PI / 4.0 + phy0 / 2.0))
            + this.alpha
            * e / 2
            * Math.log((1 + e * sinPhy0)
            / (1 - e * sinPhy0));
  },


  forward: function(p) {
    var Sa1 = Math.log(Math.tan(Math.PI / 4.0 - p.y / 2.0));
    var Sa2 = this.e / 2.0
            * Math.log((1 + this.e * Math.sin(p.y))
            / (1 - this.e * Math.sin(p.y)));
    var S = -this.alpha * (Sa1 + Sa2) + this.K;

        // spheric latitude
    var b = 2.0 * (Math.atan(Math.exp(S)) - Math.PI / 4.0);

        // spheric longitude
    var I = this.alpha * (p.x - this.lambda0);

        // psoeudo equatorial rotation
    var rotI = Math.atan(Math.sin(I)
            / (Math.sin(this.b0) * Math.tan(b) +
               Math.cos(this.b0) * Math.cos(I)));

    var rotB = Math.asin(Math.cos(this.b0) * Math.sin(b) -
                         Math.sin(this.b0) * Math.cos(b) * Math.cos(I));

    p.y = this.R / 2.0
            * Math.log((1 + Math.sin(rotB)) / (1 - Math.sin(rotB)))
            + this.y0;
    p.x = this.R * rotI + this.x0;
    return p;
  },

  inverse: function(p) {
    var Y = p.x - this.x0;
    var X = p.y - this.y0;

    var rotI = Y / this.R;
    var rotB = 2 * (Math.atan(Math.exp(X / this.R)) - Math.PI / 4.0);

    var b = Math.asin(Math.cos(this.b0) * Math.sin(rotB)
            + Math.sin(this.b0) * Math.cos(rotB) * Math.cos(rotI));
    var I = Math.atan(Math.sin(rotI)
            / (Math.cos(this.b0) * Math.cos(rotI) - Math.sin(this.b0)
            * Math.tan(rotB)));

    var lambda = this.lambda0 + I / this.alpha;

    var S = 0.0;
    var phy = b;
    var prevPhy = -1000.0;
    var iteration = 0;
    while (Math.abs(phy - prevPhy) > 0.0000001)
    {
      if (++iteration > 20)
      {
        Proj4js.reportError("omercFwdInfinity");
        return;
      }
      //S = Math.log(Math.tan(Math.PI / 4.0 + phy / 2.0));
      S = 1.0
              / this.alpha
              * (Math.log(Math.tan(Math.PI / 4.0 + b / 2.0)) - this.K)
              + this.e
              * Math.log(Math.tan(Math.PI / 4.0
              + Math.asin(this.e * Math.sin(phy))
              / 2.0));
      prevPhy = phy;
      phy = 2.0 * Math.atan(Math.exp(S)) - Math.PI / 2.0;
    }

    p.x = lambda;
    p.y = phy;
    return p;
  }
};
/* ======================================================================
    projCode/stere.js
   ====================================================================== */


// Initialize the Stereographic projection

Proj4js.Proj.stere = {
  ssfn_: function(phit, sinphi, eccen) {
  	sinphi *= eccen;
  	return (Math.tan (.5 * (Proj4js.common.HALF_PI + phit)) * Math.pow((1. - sinphi) / (1. + sinphi), .5 * eccen));
  },
  TOL:	1.e-8,
  NITER:	8,
  CONV:	1.e-10,
  S_POLE:	0,
  N_POLE:	1,
  OBLIQ:	2,
  EQUIT:	3,

  init : function() {
  	this.phits = this.lat_ts ? this.lat_ts : Proj4js.common.HALF_PI;
    var t = Math.abs(this.lat0);
  	if ((Math.abs(t) - Proj4js.common.HALF_PI) < Proj4js.common.EPSLN) {
  		this.mode = this.lat0 < 0. ? this.S_POLE : this.N_POLE;
  	} else {
  		this.mode = t > Proj4js.common.EPSLN ? this.OBLIQ : this.EQUIT;
    }
  	this.phits = Math.abs(this.phits);
  	if (this.es) {
  		var X;

  		switch (this.mode) {
  		case this.N_POLE:
  		case this.S_POLE:
  			if (Math.abs(this.phits - Proj4js.common.HALF_PI) < Proj4js.common.EPSLN) {
  				this.akm1 = 2. * this.k0 / Math.sqrt(Math.pow(1+this.e,1+this.e)*Math.pow(1-this.e,1-this.e));
  			} else {
          t = Math.sin(this.phits);
  				this.akm1 = Math.cos(this.phits) / Proj4js.common.tsfnz(this.e, this.phits, t);
  				t *= this.e;
  				this.akm1 /= Math.sqrt(1. - t * t);
  			}
  			break;
  		case this.EQUIT:
  			this.akm1 = 2. * this.k0;
  			break;
  		case this.OBLIQ:
  			t = Math.sin(this.lat0);
  			X = 2. * Math.atan(this.ssfn_(this.lat0, t, this.e)) - Proj4js.common.HALF_PI;
  			t *= this.e;
  			this.akm1 = 2. * this.k0 * Math.cos(this.lat0) / Math.sqrt(1. - t * t);
  			this.sinX1 = Math.sin(X);
  			this.cosX1 = Math.cos(X);
  			break;
  		}
  	} else {
  		switch (this.mode) {
  		case this.OBLIQ:
  			this.sinph0 = Math.sin(this.lat0);
  			this.cosph0 = Math.cos(this.lat0);
  		case this.EQUIT:
  			this.akm1 = 2. * this.k0;
  			break;
  		case this.S_POLE:
  		case this.N_POLE:
  			this.akm1 = Math.abs(this.phits - Proj4js.common.HALF_PI) >= Proj4js.common.EPSLN ?
  			   Math.cos(this.phits) / Math.tan(Proj4js.common.FORTPI - .5 * this.phits) :
  			   2. * this.k0 ;
  			break;
  		}
  	}
  }, 

// Stereographic forward equations--mapping lat,long to x,y
  forward: function(p) {
    var lon = p.x;
    lon = Proj4js.common.adjust_lon(lon - this.long0);
    var lat = p.y;
    var x, y;
    
    if (this.sphere) {
    	var  sinphi, cosphi, coslam, sinlam;

    	sinphi = Math.sin(lat);
    	cosphi = Math.cos(lat);
    	coslam = Math.cos(lon);
    	sinlam = Math.sin(lon);
    	switch (this.mode) {
    	case this.EQUIT:
    		y = 1. + cosphi * coslam;
    		if (y <= Proj4js.common.EPSLN) {
          F_ERROR;
        }
        y = this.akm1 / y;
    		x = y * cosphi * sinlam;
        y *= sinphi;
    		break;
    	case this.OBLIQ:
    		y = 1. + this.sinph0 * sinphi + this.cosph0 * cosphi * coslam;
    		if (y <= Proj4js.common.EPSLN) {
          F_ERROR;
        }
        y = this.akm1 / y;
    		x = y * cosphi * sinlam;
    		y *= this.cosph0 * sinphi - this.sinph0 * cosphi * coslam;
    		break;
    	case this.N_POLE:
    		coslam = -coslam;
    		lat = -lat;
        //Note  no break here so it conitnues through S_POLE
    	case this.S_POLE:
    		if (Math.abs(lat - Proj4js.common.HALF_PI) < this.TOL) {
          F_ERROR;
        }
        y = this.akm1 * Math.tan(Proj4js.common.FORTPI + .5 * lat);
    		x = sinlam * y;
    		y *= coslam;
    		break;
    	}
    } else {
    	coslam = Math.cos(lon);
    	sinlam = Math.sin(lon);
    	sinphi = Math.sin(lat);
    	if (this.mode == this.OBLIQ || this.mode == this.EQUIT) {
        X = 2. * Math.atan(this.ssfn_(lat, sinphi, this.e));
    		sinX = Math.sin(X - Proj4js.common.HALF_PI);
    		cosX = Math.cos(X);
    	}
    	switch (this.mode) {
    	case this.OBLIQ:
    		A = this.akm1 / (this.cosX1 * (1. + this.sinX1 * sinX + this.cosX1 * cosX * coslam));
    		y = A * (this.cosX1 * sinX - this.sinX1 * cosX * coslam);
    		x = A * cosX;
    		break;
    	case this.EQUIT:
    		A = 2. * this.akm1 / (1. + cosX * coslam);
    		y = A * sinX;
    		x = A * cosX;
    		break;
    	case this.S_POLE:
    		lat = -lat;
    		coslam = - coslam;
    		sinphi = -sinphi;
    	case this.N_POLE:
    		x = this.akm1 * Proj4js.common.tsfnz(this.e, lat, sinphi);
    		y = - x * coslam;
    		break;
    	}
    	x = x * sinlam;
    }
    p.x = x*this.a + this.x0;
    p.y = y*this.a + this.y0;
    return p;
  },


//* Stereographic inverse equations--mapping x,y to lat/long
  inverse: function(p) {
    var x = (p.x - this.x0)/this.a;   /* descale and de-offset */
    var y = (p.y - this.y0)/this.a;
    var lon, lat;

    var cosphi, sinphi, tp=0.0, phi_l=0.0, rho, halfe=0.0, pi2=0.0;
    var i;

    if (this.sphere) {
    	var  c, rh, sinc, cosc;

      rh = Math.sqrt(x*x + y*y);
      c = 2. * Math.atan(rh / this.akm1);
    	sinc = Math.sin(c);
    	cosc = Math.cos(c);
    	lon = 0.;
    	switch (this.mode) {
    	case this.EQUIT:
    		if (Math.abs(rh) <= Proj4js.common.EPSLN) {
    			lat = 0.;
    		} else {
    			lat = Math.asin(y * sinc / rh);
        }
    		if (cosc != 0. || x != 0.) lon = Math.atan2(x * sinc, cosc * rh);
    		break;
    	case this.OBLIQ:
    		if (Math.abs(rh) <= Proj4js.common.EPSLN) {
    			lat = this.phi0;
    		} else {
    			lat = Math.asin(cosc * sinph0 + y * sinc * cosph0 / rh);
        }
        c = cosc - sinph0 * Math.sin(lat);
    		if (c != 0. || x != 0.) {
    			lon = Math.atan2(x * sinc * cosph0, c * rh);
        }
    		break;
    	case this.N_POLE:
    		y = -y;
    	case this.S_POLE:
    		if (Math.abs(rh) <= Proj4js.common.EPSLN) {
    			lat = this.phi0;
    		} else {
    			lat = Math.asin(this.mode == this.S_POLE ? -cosc : cosc);
        }
    		lon = (x == 0. && y == 0.) ? 0. : Math.atan2(x, y);
    		break;
    	}
        p.x = Proj4js.common.adjust_lon(lon + this.long0);
        p.y = lat;
    } else {
    	rho = Math.sqrt(x*x + y*y);
    	switch (this.mode) {
    	case this.OBLIQ:
    	case this.EQUIT:
        tp = 2. * Math.atan2(rho * this.cosX1 , this.akm1);
    		cosphi = Math.cos(tp);
    		sinphi = Math.sin(tp);
        if( rho == 0.0 ) {
    		  phi_l = Math.asin(cosphi * this.sinX1);
        } else {
    		  phi_l = Math.asin(cosphi * this.sinX1 + (y * sinphi * this.cosX1 / rho));
        }

    		tp = Math.tan(.5 * (Proj4js.common.HALF_PI + phi_l));
    		x *= sinphi;
    		y = rho * this.cosX1 * cosphi - y * this.sinX1* sinphi;
    		pi2 = Proj4js.common.HALF_PI;
    		halfe = .5 * this.e;
    		break;
    	case this.N_POLE:
    		y = -y;
    	case this.S_POLE:
        tp = - rho / this.akm1;
    		phi_l = Proj4js.common.HALF_PI - 2. * Math.atan(tp);
    		pi2 = -Proj4js.common.HALF_PI;
    		halfe = -.5 * this.e;
    		break;
    	}
    	for (i = this.NITER; i--; phi_l = lat) { //check this
    		sinphi = this.e * Math.sin(phi_l);
    		lat = 2. * Math.atan(tp * Math.pow((1.+sinphi)/(1.-sinphi), halfe)) - pi2;
    		if (Math.abs(phi_l - lat) < this.CONV) {
    			if (this.mode == this.S_POLE) lat = -lat;
    			lon = (x == 0. && y == 0.) ? 0. : Math.atan2(x, y);
          p.x = Proj4js.common.adjust_lon(lon + this.long0);
          p.y = lat;
    			return p;
    		}
    	}
    }
  }
}; 
/* ======================================================================
    projCode/nzmg.js
   ====================================================================== */

/*******************************************************************************
NAME                            NEW ZEALAND MAP GRID

PURPOSE:	Transforms input longitude and latitude to Easting and
		Northing for the New Zealand Map Grid projection.  The
		longitude and latitude must be in radians.  The Easting
		and Northing values will be returned in meters.


ALGORITHM REFERENCES

1.  Department of Land and Survey Technical Circular 1973/32
      http://www.linz.govt.nz/docs/miscellaneous/nz-map-definition.pdf

2.  OSG Technical Report 4.1
      http://www.linz.govt.nz/docs/miscellaneous/nzmg.pdf


IMPLEMENTATION NOTES

The two references use different symbols for the calculated values. This
implementation uses the variable names similar to the symbols in reference [1].

The alogrithm uses different units for delta latitude and delta longitude.
The delta latitude is assumed to be in units of seconds of arc x 10^-5.
The delta longitude is the usual radians. Look out for these conversions.

The algorithm is described using complex arithmetic. There were three
options:
   * find and use a Javascript library for complex arithmetic
   * write my own complex library
   * expand the complex arithmetic by hand to simple arithmetic

This implementation has expanded the complex multiplication operations
into parallel simple arithmetic operations for the real and imaginary parts.
The imaginary part is way over to the right of the display; this probably
violates every coding standard in the world, but, to me, it makes it much
more obvious what is going on.

The following complex operations are used:
   - addition
   - multiplication
   - division
   - complex number raised to integer power
   - summation

A summary of complex arithmetic operations:
   (from http://en.wikipedia.org/wiki/Complex_arithmetic)
   addition:       (a + bi) + (c + di) = (a + c) + (b + d)i
   subtraction:    (a + bi) - (c + di) = (a - c) + (b - d)i
   multiplication: (a + bi) x (c + di) = (ac - bd) + (bc + ad)i
   division:       (a + bi) / (c + di) = [(ac + bd)/(cc + dd)] + [(bc - ad)/(cc + dd)]i

The algorithm needs to calculate summations of simple and complex numbers. This is
implemented using a for-loop, pre-loading the summed value to zero.

The algorithm needs to calculate theta^2, theta^3, etc while doing a summation.
There are three possible implementations:
   - use Math.pow in the summation loop - except for complex numbers
   - precalculate the values before running the loop
   - calculate theta^n = theta^(n-1) * theta during the loop
This implementation uses the third option for both real and complex arithmetic.

For example
   psi_n = 1;
   sum = 0;
   for (n = 1; n <=6; n++) {
      psi_n1 = psi_n * psi;       // calculate psi^(n+1)
      psi_n = psi_n1;
      sum = sum + A[n] * psi_n;
   }


TEST VECTORS

NZMG E, N:         2487100.638      6751049.719     metres
NZGD49 long, lat:      172.739194       -34.444066  degrees

NZMG E, N:         2486533.395      6077263.661     metres
NZGD49 long, lat:      172.723106       -40.512409  degrees

NZMG E, N:         2216746.425      5388508.765     metres
NZGD49 long, lat:      169.172062       -46.651295  degrees

Note that these test vectors convert from NZMG metres to lat/long referenced
to NZGD49, not the more usual WGS84. The difference is about 70m N/S and about
10m E/W.

These test vectors are provided in reference [1]. Many more test
vectors are available in
   http://www.linz.govt.nz/docs/topography/topographicdata/placenamesdatabase/nznamesmar08.zip
which is a catalog of names on the 260-series maps.


EPSG CODES

NZMG     EPSG:27200
NZGD49   EPSG:4272

http://spatialreference.org/ defines these as
  Proj4js.defs["EPSG:4272"] = "+proj=longlat +ellps=intl +datum=nzgd49 +no_defs ";
  Proj4js.defs["EPSG:27200"] = "+proj=nzmg +lat_0=-41 +lon_0=173 +x_0=2510000 +y_0=6023150 +ellps=intl +datum=nzgd49 +units=m +no_defs ";


LICENSE
  Copyright: Stephen Irons 2008
  Released under terms of the LGPL as per: http://www.gnu.org/copyleft/lesser.html

*******************************************************************************/


/**
  Initialize New Zealand Map Grip projection
*/

Proj4js.Proj.nzmg = {

  /**
   * iterations: Number of iterations to refine inverse transform.
   *     0 -> km accuracy
   *     1 -> m accuracy -- suitable for most mapping applications
   *     2 -> mm accuracy
   */
  iterations: 1,

  init : function() {
    this.A = new Array();
    this.A[1]  = +0.6399175073;
    this.A[2]  = -0.1358797613;
    this.A[3]  = +0.063294409;
    this.A[4]  = -0.02526853;
    this.A[5]  = +0.0117879;
    this.A[6]  = -0.0055161;
    this.A[7]  = +0.0026906;
    this.A[8]  = -0.001333;
    this.A[9]  = +0.00067;
    this.A[10] = -0.00034;

    this.B_re = new Array();        this.B_im = new Array();
    this.B_re[1] = +0.7557853228;   this.B_im[1] =  0.0;
    this.B_re[2] = +0.249204646;    this.B_im[2] = +0.003371507;
    this.B_re[3] = -0.001541739;    this.B_im[3] = +0.041058560;
    this.B_re[4] = -0.10162907;     this.B_im[4] = +0.01727609;
    this.B_re[5] = -0.26623489;     this.B_im[5] = -0.36249218;
    this.B_re[6] = -0.6870983;      this.B_im[6] = -1.1651967;

    this.C_re = new Array();        this.C_im = new Array();
    this.C_re[1] = +1.3231270439;   this.C_im[1] =  0.0;
    this.C_re[2] = -0.577245789;    this.C_im[2] = -0.007809598;
    this.C_re[3] = +0.508307513;    this.C_im[3] = -0.112208952;
    this.C_re[4] = -0.15094762;     this.C_im[4] = +0.18200602;
    this.C_re[5] = +1.01418179;     this.C_im[5] = +1.64497696;
    this.C_re[6] = +1.9660549;      this.C_im[6] = +2.5127645;

    this.D = new Array();
    this.D[1] = +1.5627014243;
    this.D[2] = +0.5185406398;
    this.D[3] = -0.03333098;
    this.D[4] = -0.1052906;
    this.D[5] = -0.0368594;
    this.D[6] = +0.007317;
    this.D[7] = +0.01220;
    this.D[8] = +0.00394;
    this.D[9] = -0.0013;
  },

  /**
    New Zealand Map Grid Forward  - long/lat to x/y
    long/lat in radians
  */
  forward : function(p) {
    var lon = p.x;
    var lat = p.y;

    var delta_lat = lat - this.lat0;
    var delta_lon = lon - this.long0;

    // 1. Calculate d_phi and d_psi    ...                          // and d_lambda
    // For this algorithm, delta_latitude is in seconds of arc x 10-5, so we need to scale to those units. Longitude is radians.
    var d_phi = delta_lat / Proj4js.common.SEC_TO_RAD * 1E-5;       var d_lambda = delta_lon;
    var d_phi_n = 1;  // d_phi^0

    var d_psi = 0;
    for (n = 1; n <= 10; n++) {
      d_phi_n = d_phi_n * d_phi;
      d_psi = d_psi + this.A[n] * d_phi_n;
    }

    // 2. Calculate theta
    var th_re = d_psi;                                              var th_im = d_lambda;

    // 3. Calculate z
    var th_n_re = 1;                                                var th_n_im = 0;  // theta^0
    var th_n_re1;                                                   var th_n_im1;

    var z_re = 0;                                                   var z_im = 0;
    for (n = 1; n <= 6; n++) {
      th_n_re1 = th_n_re*th_re - th_n_im*th_im;                     th_n_im1 = th_n_im*th_re + th_n_re*th_im;
      th_n_re = th_n_re1;                                           th_n_im = th_n_im1;
      z_re = z_re + this.B_re[n]*th_n_re - this.B_im[n]*th_n_im;    z_im = z_im + this.B_im[n]*th_n_re + this.B_re[n]*th_n_im;
    }

    // 4. Calculate easting and northing
    x = (z_im * this.a) + this.x0;
    y = (z_re * this.a) + this.y0;

    p.x = x; p.y = y;

    return p;
  },


  /**
    New Zealand Map Grid Inverse  -  x/y to long/lat
  */
  inverse : function(p) {

    var x = p.x;
    var y = p.y;

    var delta_x = x - this.x0;
    var delta_y = y - this.y0;

    // 1. Calculate z
    var z_re = delta_y / this.a;                                              var z_im = delta_x / this.a;

    // 2a. Calculate theta - first approximation gives km accuracy
    var z_n_re = 1;                                                           var z_n_im = 0;  // z^0
    var z_n_re1;                                                              var z_n_im1;

    var th_re = 0;                                                            var th_im = 0;
    for (n = 1; n <= 6; n++) {
      z_n_re1 = z_n_re*z_re - z_n_im*z_im;                                    z_n_im1 = z_n_im*z_re + z_n_re*z_im;
      z_n_re = z_n_re1;                                                       z_n_im = z_n_im1;
      th_re = th_re + this.C_re[n]*z_n_re - this.C_im[n]*z_n_im;              th_im = th_im + this.C_im[n]*z_n_re + this.C_re[n]*z_n_im;
    }

    // 2b. Iterate to refine the accuracy of the calculation
    //        0 iterations gives km accuracy
    //        1 iteration gives m accuracy -- good enough for most mapping applications
    //        2 iterations bives mm accuracy
    for (i = 0; i < this.iterations; i++) {
       var th_n_re = th_re;                                                      var th_n_im = th_im;
       var th_n_re1;                                                             var th_n_im1;

       var num_re = z_re;                                                        var num_im = z_im;
       for (n = 2; n <= 6; n++) {
         th_n_re1 = th_n_re*th_re - th_n_im*th_im;                               th_n_im1 = th_n_im*th_re + th_n_re*th_im;
         th_n_re = th_n_re1;                                                     th_n_im = th_n_im1;
         num_re = num_re + (n-1)*(this.B_re[n]*th_n_re - this.B_im[n]*th_n_im);  num_im = num_im + (n-1)*(this.B_im[n]*th_n_re + this.B_re[n]*th_n_im);
       }

       th_n_re = 1;                                                              th_n_im = 0;
       var den_re = this.B_re[1];                                                var den_im = this.B_im[1];
       for (n = 2; n <= 6; n++) {
         th_n_re1 = th_n_re*th_re - th_n_im*th_im;                               th_n_im1 = th_n_im*th_re + th_n_re*th_im;
         th_n_re = th_n_re1;                                                     th_n_im = th_n_im1;
         den_re = den_re + n * (this.B_re[n]*th_n_re - this.B_im[n]*th_n_im);    den_im = den_im + n * (this.B_im[n]*th_n_re + this.B_re[n]*th_n_im);
       }

       // Complex division
       var den2 = den_re*den_re + den_im*den_im;
       th_re = (num_re*den_re + num_im*den_im) / den2;                           th_im = (num_im*den_re - num_re*den_im) / den2;
    }

    // 3. Calculate d_phi              ...                                    // and d_lambda
    var d_psi = th_re;                                                        var d_lambda = th_im;
    var d_psi_n = 1;  // d_psi^0

    var d_phi = 0;
    for (n = 1; n <= 9; n++) {
       d_psi_n = d_psi_n * d_psi;
       d_phi = d_phi + this.D[n] * d_psi_n;
    }

    // 4. Calculate latitude and longitude
    // d_phi is calcuated in second of arc * 10^-5, so we need to scale back to radians. d_lambda is in radians.
    var lat = this.lat0 + (d_phi * Proj4js.common.SEC_TO_RAD * 1E5);
    var lon = this.long0 +  d_lambda;

    p.x = lon;
    p.y = lat;

    return p;
  }
};
/* ======================================================================
    projCode/mill.js
   ====================================================================== */

/*******************************************************************************
NAME                    MILLER CYLINDRICAL 

PURPOSE:	Transforms input longitude and latitude to Easting and
		Northing for the Miller Cylindrical projection.  The
		longitude and latitude must be in radians.  The Easting
		and Northing values will be returned in meters.

PROGRAMMER              DATE            
----------              ----           
T. Mittan		March, 1993

This function was adapted from the Lambert Azimuthal Equal Area projection
code (FORTRAN) in the General Cartographic Transformation Package software
which is available from the U.S. Geological Survey National Mapping Division.
 
ALGORITHM REFERENCES

1.  "New Equal-Area Map Projections for Noncircular Regions", John P. Snyder,
    The American Cartographer, Vol 15, No. 4, October 1988, pp. 341-355.

2.  Snyder, John P., "Map Projections--A Working Manual", U.S. Geological
    Survey Professional Paper 1395 (Supersedes USGS Bulletin 1532), United
    State Government Printing Office, Washington D.C., 1987.

3.  "Software Documentation for GCTP General Cartographic Transformation
    Package", U.S. Geological Survey National Mapping Division, May 1982.
*******************************************************************************/

Proj4js.Proj.mill = {

/* Initialize the Miller Cylindrical projection
  -------------------------------------------*/
  init: function() {
    //no-op
  },


  /* Miller Cylindrical forward equations--mapping lat,long to x,y
    ------------------------------------------------------------*/
  forward: function(p) {
    var lon=p.x;
    var lat=p.y;
    /* Forward equations
      -----------------*/
    var dlon = Proj4js.common.adjust_lon(lon -this.long0);
    var x = this.x0 + this.a * dlon;
    var y = this.y0 + this.a * Math.log(Math.tan((Proj4js.common.PI / 4.0) + (lat / 2.5))) * 1.25;

    p.x=x;
    p.y=y;
    return p;
  },//millFwd()

  /* Miller Cylindrical inverse equations--mapping x,y to lat/long
    ------------------------------------------------------------*/
  inverse: function(p) {
    p.x -= this.x0;
    p.y -= this.y0;

    var lon = Proj4js.common.adjust_lon(this.long0 + p.x /this.a);
    var lat = 2.5 * (Math.atan(Math.exp(0.8*p.y/this.a)) - Proj4js.common.PI / 4.0);

    p.x=lon;
    p.y=lat;
    return p;
  }//millInv()
};
/* ======================================================================
    projCode/gnom.js
   ====================================================================== */

/*****************************************************************************
NAME                             GNOMONIC

PURPOSE:	Transforms input longitude and latitude to Easting and
		Northing for the Gnomonic Projection.
                Implementation based on the existing sterea and ortho
                implementations.

PROGRAMMER              DATE
----------              ----
Richard Marsden         November 2009

ALGORITHM REFERENCES

1.  Snyder, John P., "Flattening the Earth - Two Thousand Years of Map 
    Projections", University of Chicago Press 1993

2.  Wolfram Mathworld "Gnomonic Projection"
    http://mathworld.wolfram.com/GnomonicProjection.html
    Accessed: 12th November 2009
******************************************************************************/

Proj4js.Proj.gnom = {

  /* Initialize the Gnomonic projection
    -------------------------------------*/
  init: function(def) {

    /* Place parameters in static storage for common use
      -------------------------------------------------*/
    this.sin_p14=Math.sin(this.lat0);
    this.cos_p14=Math.cos(this.lat0);
    // Approximation for projecting points to the horizon (infinity)
    this.infinity_dist = 1000 * this.a;
    this.rc = 1;
  },


  /* Gnomonic forward equations--mapping lat,long to x,y
    ---------------------------------------------------*/
  forward: function(p) {
    var sinphi, cosphi;	/* sin and cos value				*/
    var dlon;		/* delta longitude value			*/
    var coslon;		/* cos of longitude				*/
    var ksp;		/* scale factor					*/
    var g;		
    var lon=p.x;
    var lat=p.y;	
    /* Forward equations
      -----------------*/
    dlon = Proj4js.common.adjust_lon(lon - this.long0);

    sinphi=Math.sin(lat);
    cosphi=Math.cos(lat);	

    coslon = Math.cos(dlon);
    g = this.sin_p14 * sinphi + this.cos_p14 * cosphi * coslon;
    ksp = 1.0;
    if ((g > 0) || (Math.abs(g) <= Proj4js.common.EPSLN)) {
      x = this.x0 + this.a * ksp * cosphi * Math.sin(dlon) / g;
      y = this.y0 + this.a * ksp * (this.cos_p14 * sinphi - this.sin_p14 * cosphi * coslon) / g;
    } else {
      Proj4js.reportError("orthoFwdPointError");

      // Point is in the opposing hemisphere and is unprojectable
      // We still need to return a reasonable point, so we project 
      // to infinity, on a bearing 
      // equivalent to the northern hemisphere equivalent
      // This is a reasonable approximation for short shapes and lines that 
      // straddle the horizon.

      x = this.x0 + this.infinity_dist * cosphi * Math.sin(dlon);
      y = this.y0 + this.infinity_dist * (this.cos_p14 * sinphi - this.sin_p14 * cosphi * coslon);

    }
    p.x=x;
    p.y=y;
    return p;
  },


  inverse: function(p) {
    var rh;		/* Rho */
    var z;		/* angle */
    var sinc, cosc;
    var c;
    var lon , lat;

    /* Inverse equations
      -----------------*/
    p.x = (p.x - this.x0) / this.a;
    p.y = (p.y - this.y0) / this.a;

    p.x /= this.k0;
    p.y /= this.k0;

    if ( (rh = Math.sqrt(p.x * p.x + p.y * p.y)) ) {
      c = Math.atan2(rh, this.rc);
      sinc = Math.sin(c);
      cosc = Math.cos(c);

      lat = Proj4js.common.asinz(cosc*this.sin_p14 + (p.y*sinc*this.cos_p14) / rh);
      lon = Math.atan2(p.x*sinc, rh*this.cos_p14*cosc - p.y*this.sin_p14*sinc);
      lon = Proj4js.common.adjust_lon(this.long0+lon);
    } else {
      lat = this.phic0;
      lon = 0.0;
    }
 
    p.x=lon;
    p.y=lat;
    return p;
  }
};


/* ======================================================================
    projCode/sinu.js
   ====================================================================== */

/*******************************************************************************
NAME                  		SINUSOIDAL

PURPOSE:	Transforms input longitude and latitude to Easting and
		Northing for the Sinusoidal projection.  The
		longitude and latitude must be in radians.  The Easting
		and Northing values will be returned in meters.

PROGRAMMER              DATE            
----------              ----           
D. Steinwand, EROS      May, 1991     

This function was adapted from the Sinusoidal projection code (FORTRAN) in the 
General Cartographic Transformation Package software which is available from 
the U.S. Geological Survey National Mapping Division.
 
ALGORITHM REFERENCES

1.  Snyder, John P., "Map Projections--A Working Manual", U.S. Geological
    Survey Professional Paper 1395 (Supersedes USGS Bulletin 1532), United
    State Government Printing Office, Washington D.C., 1987.

2.  "Software Documentation for GCTP General Cartographic Transformation
    Package", U.S. Geological Survey National Mapping Division, May 1982.
*******************************************************************************/

Proj4js.Proj.sinu = {

	/* Initialize the Sinusoidal projection
	  ------------------------------------*/
	init: function() {
		/* Place parameters in static storage for common use
		  -------------------------------------------------*/
		this.R = 6370997.0; //Radius of earth
	},

	/* Sinusoidal forward equations--mapping lat,long to x,y
	-----------------------------------------------------*/
	forward: function(p) {
		var x,y,delta_lon;	
		var lon=p.x;
		var lat=p.y;	
		/* Forward equations
		-----------------*/
		delta_lon = Proj4js.common.adjust_lon(lon - this.long0);
		x = this.R * delta_lon * Math.cos(lat) + this.x0;
		y = this.R * lat + this.y0;

		p.x=x;
		p.y=y;	
		return p;
	},

	inverse: function(p) {
		var lat,temp,lon;	

		/* Inverse equations
		  -----------------*/
		p.x -= this.x0;
		p.y -= this.y0;
		lat = p.y / this.R;
		if (Math.abs(lat) > Proj4js.common.HALF_PI) {
		    Proj4js.reportError("sinu:Inv:DataError");
		}
		temp = Math.abs(lat) - Proj4js.common.HALF_PI;
		if (Math.abs(temp) > Proj4js.common.EPSLN) {
			temp = this.long0+ p.x / (this.R *Math.cos(lat));
			lon = Proj4js.common.adjust_lon(temp);
		} else {
			lon = this.long0;
		}
		  
		p.x=lon;
		p.y=lat;
		return p;
	}
};


/* ======================================================================
    projCode/vandg.js
   ====================================================================== */

/*******************************************************************************
NAME                    VAN DER GRINTEN 

PURPOSE:	Transforms input Easting and Northing to longitude and
		latitude for the Van der Grinten projection.  The
		Easting and Northing must be in meters.  The longitude
		and latitude values will be returned in radians.

PROGRAMMER              DATE            
----------              ----           
T. Mittan		March, 1993

This function was adapted from the Van Der Grinten projection code
(FORTRAN) in the General Cartographic Transformation Package software
which is available from the U.S. Geological Survey National Mapping Division.
 
ALGORITHM REFERENCES

1.  "New Equal-Area Map Projections for Noncircular Regions", John P. Snyder,
    The American Cartographer, Vol 15, No. 4, October 1988, pp. 341-355.

2.  Snyder, John P., "Map Projections--A Working Manual", U.S. Geological
    Survey Professional Paper 1395 (Supersedes USGS Bulletin 1532), United
    State Government Printing Office, Washington D.C., 1987.

3.  "Software Documentation for GCTP General Cartographic Transformation
    Package", U.S. Geological Survey National Mapping Division, May 1982.
*******************************************************************************/

Proj4js.Proj.vandg = {

/* Initialize the Van Der Grinten projection
  ----------------------------------------*/
	init: function() {
		this.R = 6370997.0; //Radius of earth
	},

	forward: function(p) {

		var lon=p.x;
		var lat=p.y;	

		/* Forward equations
		-----------------*/
		var dlon = Proj4js.common.adjust_lon(lon - this.long0);
		var x,y;

		if (Math.abs(lat) <= Proj4js.common.EPSLN) {
			x = this.x0  + this.R * dlon;
			y = this.y0;
		}
		var theta = Proj4js.common.asinz(2.0 * Math.abs(lat / Proj4js.common.PI));
		if ((Math.abs(dlon) <= Proj4js.common.EPSLN) || (Math.abs(Math.abs(lat) - Proj4js.common.HALF_PI) <= Proj4js.common.EPSLN)) {
			x = this.x0;
			if (lat >= 0) {
				y = this.y0 + Proj4js.common.PI * this.R * Math.tan(.5 * theta);
			} else {
				y = this.y0 + Proj4js.common.PI * this.R * - Math.tan(.5 * theta);
			}
			//  return(OK);
		}
		var al = .5 * Math.abs((Proj4js.common.PI / dlon) - (dlon / Proj4js.common.PI));
		var asq = al * al;
		var sinth = Math.sin(theta);
		var costh = Math.cos(theta);

		var g = costh / (sinth + costh - 1.0);
		var gsq = g * g;
		var m = g * (2.0 / sinth - 1.0);
		var msq = m * m;
		var con = Proj4js.common.PI * this.R * (al * (g - msq) + Math.sqrt(asq * (g - msq) * (g - msq) - (msq + asq) * (gsq - msq))) / (msq + asq);
		if (dlon < 0) {
		 con = -con;
		}
		x = this.x0 + con;
		con = Math.abs(con / (Proj4js.common.PI * this.R));
		if (lat >= 0) {
		 y = this.y0 + Proj4js.common.PI * this.R * Math.sqrt(1.0 - con * con - 2.0 * al * con);
		} else {
		 y = this.y0 - Proj4js.common.PI * this.R * Math.sqrt(1.0 - con * con - 2.0 * al * con);
		}
		p.x = x;
		p.y = y;
		return p;
	},

/* Van Der Grinten inverse equations--mapping x,y to lat/long
  ---------------------------------------------------------*/
	inverse: function(p) {
		var dlon;
		var xx,yy,xys,c1,c2,c3;
		var al,asq;
		var a1;
		var m1;
		var con;
		var th1;
		var d;

		/* inverse equations
		-----------------*/
		p.x -= this.x0;
		p.y -= this.y0;
		con = Proj4js.common.PI * this.R;
		xx = p.x / con;
		yy =p.y / con;
		xys = xx * xx + yy * yy;
		c1 = -Math.abs(yy) * (1.0 + xys);
		c2 = c1 - 2.0 * yy * yy + xx * xx;
		c3 = -2.0 * c1 + 1.0 + 2.0 * yy * yy + xys * xys;
		d = yy * yy / c3 + (2.0 * c2 * c2 * c2 / c3 / c3 / c3 - 9.0 * c1 * c2 / c3 /c3) / 27.0;
		a1 = (c1 - c2 * c2 / 3.0 / c3) / c3;
		m1 = 2.0 * Math.sqrt( -a1 / 3.0);
		con = ((3.0 * d) / a1) / m1;
		if (Math.abs(con) > 1.0) {
			if (con >= 0.0) {
				con = 1.0;
			} else {
				con = -1.0;
			}
		}
		th1 = Math.acos(con) / 3.0;
		if (p.y >= 0) {
			lat = (-m1 *Math.cos(th1 + Proj4js.common.PI / 3.0) - c2 / 3.0 / c3) * Proj4js.common.PI;
		} else {
			lat = -(-m1 * Math.cos(th1 + Proj4js.common.PI / 3.0) - c2 / 3.0 / c3) * Proj4js.common.PI;
		}

		if (Math.abs(xx) < Proj4js.common.EPSLN) {
			lon = this.long0;
		}
		lon = Proj4js.common.adjust_lon(this.long0 + Proj4js.common.PI * (xys - 1.0 + Math.sqrt(1.0 + 2.0 * (xx * xx - yy * yy) + xys * xys)) / 2.0 / xx);

		p.x=lon;
		p.y=lat;
		return p;
	}
};
/* ======================================================================
    projCode/cea.js
   ====================================================================== */

/*******************************************************************************
NAME                    LAMBERT CYLINDRICAL EQUAL AREA

PURPOSE:	Transforms input longitude and latitude to Easting and
		Northing for the Lambert Cylindrical Equal Area projection.
                This class of projection includes the Behrmann and 
                Gall-Peters Projections.  The
		longitude and latitude must be in radians.  The Easting
		and Northing values will be returned in meters.

PROGRAMMER              DATE            
----------              ----
R. Marsden              August 2009
Winwaed Software Tech LLC, http://www.winwaed.com

This function was adapted from the Miller Cylindrical Projection in the Proj4JS
library.

Note: This implementation assumes a Spherical Earth. The (commented) code 
has been included for the ellipsoidal forward transform, but derivation of 
the ellispoidal inverse transform is beyond me. Note that most of the 
Proj4JS implementations do NOT currently support ellipsoidal figures. 
Therefore this is not seen as a problem - especially this lack of support 
is explicitly stated here.
 
ALGORITHM REFERENCES

1.  "Cartographic Projection Procedures for the UNIX Environment - 
     A User's Manual" by Gerald I. Evenden, USGS Open File Report 90-284
    and Release 4 Interim Reports (2003)

2.  Snyder, John P., "Flattening the Earth - Two Thousand Years of Map 
    Projections", Univ. Chicago Press, 1993
*******************************************************************************/

Proj4js.Proj.cea = {

/* Initialize the Cylindrical Equal Area projection
  -------------------------------------------*/
  init: function() {
    //no-op
  },


  /* Cylindrical Equal Area forward equations--mapping lat,long to x,y
    ------------------------------------------------------------*/
  forward: function(p) {
    var lon=p.x;
    var lat=p.y;
    /* Forward equations
      -----------------*/
    dlon = Proj4js.common.adjust_lon(lon -this.long0);
    var x = this.x0 + this.a * dlon * Math.cos(this.lat_ts);
    var y = this.y0 + this.a * Math.sin(lat) / Math.cos(this.lat_ts);
   /* Elliptical Forward Transform
      Not implemented due to a lack of a matchign inverse function
    {
      var Sin_Lat = Math.sin(lat);
      var Rn = this.a * (Math.sqrt(1.0e0 - this.es * Sin_Lat * Sin_Lat ));
      x = this.x0 + this.a * dlon * Math.cos(this.lat_ts);
      y = this.y0 + Rn * Math.sin(lat) / Math.cos(this.lat_ts);
    }
   */


    p.x=x;
    p.y=y;
    return p;
  },//ceaFwd()

  /* Cylindrical Equal Area inverse equations--mapping x,y to lat/long
    ------------------------------------------------------------*/
  inverse: function(p) {
    p.x -= this.x0;
    p.y -= this.y0;

    var lon = Proj4js.common.adjust_lon( this.long0 + (p.x / this.a) / Math.cos(this.lat_ts) );

    var lat = Math.asin( (p.y/this.a) * Math.cos(this.lat_ts) );

    p.x=lon;
    p.y=lat;
    return p;
  }//ceaInv()
};
/* ======================================================================
    projCode/eqc.js
   ====================================================================== */

/* similar to equi.js FIXME proj4 uses eqc */
Proj4js.Proj.eqc = {
  init : function() {

      if(!this.x0) this.x0=0;
      if(!this.y0) this.y0=0;
      if(!this.lat0) this.lat0=0;
      if(!this.long0) this.long0=0;
      if(!this.lat_ts) this.lat_ts=0;
      if (!this.title) this.title = "Equidistant Cylindrical (Plate Carre)";

      this.rc= Math.cos(this.lat_ts);
    },


    // forward equations--mapping lat,long to x,y
    // -----------------------------------------------------------------
    forward : function(p) {

      var lon= p.x;
      var lat= p.y;

      var dlon = Proj4js.common.adjust_lon(lon - this.long0);
      var dlat = Proj4js.common.adjust_lat(lat - this.lat0 );
      p.x= this.x0 + (this.a*dlon*this.rc);
      p.y= this.y0 + (this.a*dlat        );
      return p;
    },

  // inverse equations--mapping x,y to lat/long
  // -----------------------------------------------------------------
  inverse : function(p) {

    var x= p.x;
    var y= p.y;

    p.x= Proj4js.common.adjust_lon(this.long0 + ((x - this.x0)/(this.a*this.rc)));
    p.y= Proj4js.common.adjust_lat(this.lat0  + ((y - this.y0)/(this.a        )));
    return p;
  }

};
/* ======================================================================
    projCode/cass.js
   ====================================================================== */

/*******************************************************************************
NAME                            CASSINI

PURPOSE:	Transforms input longitude and latitude to Easting and
		Northing for the Cassini projection.  The
		longitude and latitude must be in radians.  The Easting
		and Northing values will be returned in meters.
    Ported from PROJ.4.


ALGORITHM REFERENCES

1.  Snyder, John P., "Map Projections--A Working Manual", U.S. Geological
    Survey Professional Paper 1395 (Supersedes USGS Bulletin 1532), United
    State Government Printing Office, Washington D.C., 1987.

2.  Snyder, John P. and Voxland, Philip M., "An Album of Map Projections",
    U.S. Geological Survey Professional Paper 1453 , United State Government
*******************************************************************************/


//Proj4js.defs["EPSG:28191"] = "+proj=cass +lat_0=31.73409694444445 +lon_0=35.21208055555556 +x_0=170251.555 +y_0=126867.909 +a=6378300.789 +b=6356566.435 +towgs84=-275.722,94.7824,340.894,-8.001,-4.42,-11.821,1 +units=m +no_defs";

// Initialize the Cassini projection
// -----------------------------------------------------------------

Proj4js.Proj.cass = {
  init : function() {
    if (!this.sphere) {
      this.en = this.pj_enfn(this.es)
      this.m0 = this.pj_mlfn(this.lat0, Math.sin(this.lat0), Math.cos(this.lat0), this.en);
    }
  },

  C1:	.16666666666666666666,
  C2:	.00833333333333333333,
  C3:	.04166666666666666666,
  C4:	.33333333333333333333,
  C5:	.06666666666666666666,


/* Cassini forward equations--mapping lat,long to x,y
  -----------------------------------------------------------------------*/
  forward: function(p) {

    /* Forward equations
      -----------------*/
    var x,y;
    var lam=p.x;
    var phi=p.y;
    lam = Proj4js.common.adjust_lon(lam - this.long0);
    
    if (this.sphere) {
      x = Math.asin(Math.cos(phi) * Math.sin(lam));
      y = Math.atan2(Math.tan(phi) , Math.cos(lam)) - this.phi0;
    } else {
        //ellipsoid
      this.n = Math.sin(phi);
      this.c = Math.cos(phi);
      y = this.pj_mlfn(phi, this.n, this.c, this.en);
      this.n = 1./Math.sqrt(1. - this.es * this.n * this.n);
      this.tn = Math.tan(phi); 
      this.t = this.tn * this.tn;
      this.a1 = lam * this.c;
      this.c *= this.es * this.c / (1 - this.es);
      this.a2 = this.a1 * this.a1;
      x = this.n * this.a1 * (1. - this.a2 * this.t * (this.C1 - (8. - this.t + 8. * this.c) * this.a2 * this.C2));
      y -= this.m0 - this.n * this.tn * this.a2 * (.5 + (5. - this.t + 6. * this.c) * this.a2 * this.C3);
    }
    
    p.x = this.a*x + this.x0;
    p.y = this.a*y + this.y0;
    return p;
  },//cassFwd()

/* Inverse equations
  -----------------*/
  inverse: function(p) {
    p.x -= this.x0;
    p.y -= this.y0;
    var x = p.x/this.a;
    var y = p.y/this.a;
    
    if (this.sphere) {
      this.dd = y + this.lat0;
      phi = Math.asin(Math.sin(this.dd) * Math.cos(x));
      lam = Math.atan2(Math.tan(x), Math.cos(this.dd));
    } else {
      /* ellipsoid */
      ph1 = this.pj_inv_mlfn(this.m0 + y, this.es, this.en);
      this.tn = Math.tan(ph1); 
      this.t = this.tn * this.tn;
      this.n = Math.sin(ph1);
      this.r = 1. / (1. - this.es * this.n * this.n);
      this.n = Math.sqrt(this.r);
      this.r *= (1. - this.es) * this.n;
      this.dd = x / this.n;
      this.d2 = this.dd * this.dd;
      phi = ph1 - (this.n * this.tn / this.r) * this.d2 * (.5 - (1. + 3. * this.t) * this.d2 * this.C3);
      lam = this.dd * (1. + this.t * this.d2 * (-this.C4 + (1. + 3. * this.t) * this.d2 * this.C5)) / Math.cos(ph1);
    }
    p.x = Proj4js.common.adjust_lon(this.long0+lam);
    p.y = phi;
    return p;
  },//lamazInv()


  //code from the PROJ.4 pj_mlfn.c file;  this may be useful for other projections
  pj_enfn: function(es) {
    en = new Array();
    en[0] = this.C00 - es * (this.C02 + es * (this.C04 + es * (this.C06 + es * this.C08)));
    en[1] = es * (this.C22 - es * (this.C04 + es * (this.C06 + es * this.C08)));
    var t = es * es;
    en[2] = t * (this.C44 - es * (this.C46 + es * this.C48));
    t *= es;
    en[3] = t * (this.C66 - es * this.C68);
    en[4] = t * es * this.C88;
    return en;
  },
  
  pj_mlfn: function(phi, sphi, cphi, en) {
    cphi *= sphi;
    sphi *= sphi;
    return(en[0] * phi - cphi * (en[1] + sphi*(en[2]+ sphi*(en[3] + sphi*en[4]))));
  },
  
  pj_inv_mlfn: function(arg, es, en) {
    k = 1./(1.-es);
    phi = arg;
    for (i = Proj4js.common.MAX_ITER; i ; --i) { /* rarely goes over 2 iterations */
      s = Math.sin(phi);
      t = 1. - es * s * s;
      //t = this.pj_mlfn(phi, s, Math.cos(phi), en) - arg;
      //phi -= t * (t * Math.sqrt(t)) * k;
      t = (this.pj_mlfn(phi, s, Math.cos(phi), en) - arg) * (t * Math.sqrt(t)) * k;
      phi -= t;
      if (Math.abs(t) < Proj4js.common.EPSLN)
        return phi;
    }
    Proj4js.reportError("cass:pj_inv_mlfn: Convergence error");
    return phi;
  },

/* meridinal distance for ellipsoid and inverse
**	8th degree - accurate to < 1e-5 meters when used in conjuction
**		with typical major axis values.
**	Inverse determines phi to EPS (1e-11) radians, about 1e-6 seconds.
*/
  C00: 1.0,
  C02: .25,
  C04: .046875,
  C06: .01953125,
  C08: .01068115234375,
  C22: .75,
  C44: .46875,
  C46: .01302083333333333333,
  C48: .00712076822916666666,
  C66: .36458333333333333333,
  C68: .00569661458333333333,
  C88: .3076171875

}
/* ======================================================================
    projCode/gauss.js
   ====================================================================== */


Proj4js.Proj.gauss = {

  init : function() {
    sphi = Math.sin(this.lat0);
    cphi = Math.cos(this.lat0);  
    cphi *= cphi;
    this.rc = Math.sqrt(1.0 - this.es) / (1.0 - this.es * sphi * sphi);
    this.C = Math.sqrt(1.0 + this.es * cphi * cphi / (1.0 - this.es));
    this.phic0 = Math.asin(sphi / this.C);
    this.ratexp = 0.5 * this.C * this.e;
    this.K = Math.tan(0.5 * this.phic0 + Proj4js.common.FORTPI) / (Math.pow(Math.tan(0.5*this.lat0 + Proj4js.common.FORTPI), this.C) * Proj4js.common.srat(this.e*sphi, this.ratexp));
  },

  forward : function(p) {
    var lon = p.x;
    var lat = p.y;

    p.y = 2.0 * Math.atan( this.K * Math.pow(Math.tan(0.5 * lat + Proj4js.common.FORTPI), this.C) * Proj4js.common.srat(this.e * Math.sin(lat), this.ratexp) ) - Proj4js.common.HALF_PI;
    p.x = this.C * lon;
    return p;
  },

  inverse : function(p) {
    var DEL_TOL = 1e-14;
    var lon = p.x / this.C;
    var lat = p.y;
    num = Math.pow(Math.tan(0.5 * lat + Proj4js.common.FORTPI)/this.K, 1./this.C);
    for (var i = Proj4js.common.MAX_ITER; i>0; --i) {
      lat = 2.0 * Math.atan(num * Proj4js.common.srat(this.e * Math.sin(p.y), -0.5 * this.e)) - Proj4js.common.HALF_PI;
      if (Math.abs(lat - p.y) < DEL_TOL) break;
      p.y = lat;
    }	
    /* convergence failed */
    if (!i) {
      Proj4js.reportError("gauss:inverse:convergence failed");
      return null;
    }
    p.x = lon;
    p.y = lat;
    return p;
  }
};

/* ======================================================================
    projCode/omerc.js
   ====================================================================== */

/*******************************************************************************
NAME                       OBLIQUE MERCATOR (HOTINE) 

PURPOSE:	Transforms input longitude and latitude to Easting and
		Northing for the Oblique Mercator projection.  The
		longitude and latitude must be in radians.  The Easting
		and Northing values will be returned in meters.

PROGRAMMER              DATE
----------              ----
T. Mittan		Mar, 1993

ALGORITHM REFERENCES

1.  Snyder, John P., "Map Projections--A Working Manual", U.S. Geological
    Survey Professional Paper 1395 (Supersedes USGS Bulletin 1532), United
    State Government Printing Office, Washington D.C., 1987.

2.  Snyder, John P. and Voxland, Philip M., "An Album of Map Projections",
    U.S. Geological Survey Professional Paper 1453 , United State Government
    Printing Office, Washington D.C., 1989.
*******************************************************************************/

Proj4js.Proj.omerc = {

  /* Initialize the Oblique Mercator  projection
    ------------------------------------------*/
  init: function() {
    if (!this.mode) this.mode=0;
    if (!this.lon1)   {this.lon1=0;this.mode=1;}
    if (!this.lon2)   this.lon2=0;
    if (!this.lat2)    this.lat2=0;

    /* Place parameters in static storage for common use
      -------------------------------------------------*/
    var temp = this.b/ this.a;
    var es = 1.0 - Math.pow(temp,2);
    var e = Math.sqrt(es);

    this.sin_p20=Math.sin(this.lat0);
    this.cos_p20=Math.cos(this.lat0);

    this.con = 1.0 - this.es * this.sin_p20 * this.sin_p20;
    this.com = Math.sqrt(1.0 - es);
    this.bl = Math.sqrt(1.0 + this.es * Math.pow(this.cos_p20,4.0)/(1.0 - es));
    this.al = this.a * this.bl * this.k0 * this.com / this.con;
    if (Math.abs(this.lat0) < Proj4js.common.EPSLN) {
       this.ts = 1.0;
       this.d = 1.0;
       this.el = 1.0;
    } else {
       this.ts = Proj4js.common.tsfnz(this.e,this.lat0,this.sin_p20);
       this.con = Math.sqrt(this.con);
       this.d = this.bl * this.com / (this.cos_p20 * this.con);
       if ((this.d * this.d - 1.0) > 0.0) {
          if (this.lat0 >= 0.0) {
             this.f = this.d + Math.sqrt(this.d * this.d - 1.0);
          } else {
             this.f = this.d - Math.sqrt(this.d * this.d - 1.0);
          }
       } else {
         this.f = this.d;
       }
       this.el = this.f * Math.pow(this.ts,this.bl);
    }

    //this.longc=52.60353916666667;

    if (this.mode != 0) {
       this.g = .5 * (this.f - 1.0/this.f);
       this.gama = Proj4js.common.asinz(Math.sin(this.alpha) / this.d);
       this.longc= this.longc - Proj4js.common.asinz(this.g * Math.tan(this.gama))/this.bl;

       /* Report parameters common to format B
       -------------------------------------*/
       //genrpt(azimuth * R2D,"Azimuth of Central Line:    ");
       //cenlon(lon_origin);
      // cenlat(lat_origin);

       this.con = Math.abs(this.lat0);
       if ((this.con > Proj4js.common.EPSLN) && (Math.abs(this.con - Proj4js.common.HALF_PI) > Proj4js.common.EPSLN)) {
            this.singam=Math.sin(this.gama);
            this.cosgam=Math.cos(this.gama);

            this.sinaz=Math.sin(this.alpha);
            this.cosaz=Math.cos(this.alpha);

            if (this.lat0>= 0) {
               this.u =  (this.al / this.bl) * Math.atan(Math.sqrt(this.d*this.d - 1.0)/this.cosaz);
            } else {
               this.u =  -(this.al / this.bl) *Math.atan(Math.sqrt(this.d*this.d - 1.0)/this.cosaz);
            }
          } else {
            Proj4js.reportError("omerc:Init:DataError");
          }
       } else {
       this.sinphi =Math. sin(this.at1);
       this.ts1 = Proj4js.common.tsfnz(this.e,this.lat1,this.sinphi);
       this.sinphi = Math.sin(this.lat2);
       this.ts2 = Proj4js.common.tsfnz(this.e,this.lat2,this.sinphi);
       this.h = Math.pow(this.ts1,this.bl);
       this.l = Math.pow(this.ts2,this.bl);
       this.f = this.el/this.h;
       this.g = .5 * (this.f - 1.0/this.f);
       this.j = (this.el * this.el - this.l * this.h)/(this.el * this.el + this.l * this.h);
       this.p = (this.l - this.h) / (this.l + this.h);
       this.dlon = this.lon1 - this.lon2;
       if (this.dlon < -Proj4js.common.PI) this.lon2 = this.lon2 - 2.0 * Proj4js.common.PI;
       if (this.dlon > Proj4js.common.PI) this.lon2 = this.lon2 + 2.0 * Proj4js.common.PI;
       this.dlon = this.lon1 - this.lon2;
       this.longc = .5 * (this.lon1 + this.lon2) -Math.atan(this.j * Math.tan(.5 * this.bl * this.dlon)/this.p)/this.bl;
       this.dlon  = Proj4js.common.adjust_lon(this.lon1 - this.longc);
       this.gama = Math.atan(Math.sin(this.bl * this.dlon)/this.g);
       this.alpha = Proj4js.common.asinz(this.d * Math.sin(this.gama));

       /* Report parameters common to format A
       -------------------------------------*/

       if (Math.abs(this.lat1 - this.lat2) <= Proj4js.common.EPSLN) {
          Proj4js.reportError("omercInitDataError");
          //return(202);
       } else {
          this.con = Math.abs(this.lat1);
       }
       if ((this.con <= Proj4js.common.EPSLN) || (Math.abs(this.con - HALF_PI) <= Proj4js.common.EPSLN)) {
           Proj4js.reportError("omercInitDataError");
                //return(202);
       } else {
         if (Math.abs(Math.abs(this.lat0) - Proj4js.common.HALF_PI) <= Proj4js.common.EPSLN) {
            Proj4js.reportError("omercInitDataError");
            //return(202);
         }
       }

       this.singam=Math.sin(this.gam);
       this.cosgam=Math.cos(this.gam);

       this.sinaz=Math.sin(this.alpha);
       this.cosaz=Math.cos(this.alpha);  


       if (this.lat0 >= 0) {
          this.u =  (this.al/this.bl) * Math.atan(Math.sqrt(this.d * this.d - 1.0)/this.cosaz);
       } else {
          this.u = -(this.al/this.bl) * Math.atan(Math.sqrt(this.d * this.d - 1.0)/this.cosaz);
       }
     }
  },


  /* Oblique Mercator forward equations--mapping lat,long to x,y
    ----------------------------------------------------------*/
  forward: function(p) {
    var theta;		/* angle					*/
    var sin_phi, cos_phi;/* sin and cos value				*/
    var b;		/* temporary values				*/
    var c, t, tq;	/* temporary values				*/
    var con, n, ml;	/* cone constant, small m			*/
    var q,us,vl;
    var ul,vs;
    var s;
    var dlon;
    var ts1;

    var lon=p.x;
    var lat=p.y;
    /* Forward equations
      -----------------*/
    sin_phi = Math.sin(lat);
    dlon = Proj4js.common.adjust_lon(lon - this.longc);
    vl = Math.sin(this.bl * dlon);
    if (Math.abs(Math.abs(lat) - Proj4js.common.HALF_PI) > Proj4js.common.EPSLN) {
       ts1 = Proj4js.common.tsfnz(this.e,lat,sin_phi);
       q = this.el / (Math.pow(ts1,this.bl));
       s = .5 * (q - 1.0 / q);
       t = .5 * (q + 1.0/ q);
       ul = (s * this.singam - vl * this.cosgam) / t;
       con = Math.cos(this.bl * dlon);
       if (Math.abs(con) < .0000001) {
          us = this.al * this.bl * dlon;
       } else {
          us = this.al * Math.atan((s * this.cosgam + vl * this.singam) / con)/this.bl;
          if (con < 0) us = us + Proj4js.common.PI * this.al / this.bl;
       }
    } else {
       if (lat >= 0) {
          ul = this.singam;
       } else {
          ul = -this.singam;
       }
       us = this.al * lat / this.bl;
    }
    if (Math.abs(Math.abs(ul) - 1.0) <= Proj4js.common.EPSLN) {
       //alert("Point projects into infinity","omer-for");
       Proj4js.reportError("omercFwdInfinity");
       //return(205);
    }
    vs = .5 * this.al * Math.log((1.0 - ul)/(1.0 + ul)) / this.bl;
    us = us - this.u;
    var x = this.x0 + vs * this.cosaz + us * this.sinaz;
    var y = this.y0 + us * this.cosaz - vs * this.sinaz;

    p.x=x;
    p.y=y;
    return p;
  },

  inverse: function(p) {
    var delta_lon;	/* Delta longitude (Given longitude - center 	*/
    var theta;		/* angle					*/
    var delta_theta;	/* adjusted longitude				*/
    var sin_phi, cos_phi;/* sin and cos value				*/
    var b;		/* temporary values				*/
    var c, t, tq;	/* temporary values				*/
    var con, n, ml;	/* cone constant, small m			*/
    var vs,us,q,s,ts1;
    var vl,ul,bs;
    var dlon;
    var  flag;

    /* Inverse equations
      -----------------*/
    p.x -= this.x0;
    p.y -= this.y0;
    flag = 0;
    vs = p.x * this.cosaz - p.y * this.sinaz;
    us = p.y * this.cosaz + p.x * this.sinaz;
    us = us + this.u;
    q = Math.exp(-this.bl * vs / this.al);
    s = .5 * (q - 1.0/q);
    t = .5 * (q + 1.0/q);
    vl = Math.sin(this.bl * us / this.al);
    ul = (vl * this.cosgam + s * this.singam)/t;
    if (Math.abs(Math.abs(ul) - 1.0) <= Proj4js.common.EPSLN)
       {
       lon = this.longc;
       if (ul >= 0.0) {
          lat = Proj4js.common.HALF_PI;
       } else {
         lat = -Proj4js.common.HALF_PI;
       }
    } else {
       con = 1.0 / this.bl;
       ts1 =Math.pow((this.el / Math.sqrt((1.0 + ul) / (1.0 - ul))),con);
       lat = Proj4js.common.phi2z(this.e,ts1);
       //if (flag != 0)
          //return(flag);
       //~ con = Math.cos(this.bl * us /al);
       theta = this.longc - Math.atan2((s * this.cosgam - vl * this.singam) , con)/this.bl;
       lon = Proj4js.common.adjust_lon(theta);
    }
    p.x=lon;
    p.y=lat;
    return p;
  }
};
/* ======================================================================
    projCode/lcc.js
   ====================================================================== */

/*******************************************************************************
NAME                            LAMBERT CONFORMAL CONIC

PURPOSE:	Transforms input longitude and latitude to Easting and
		Northing for the Lambert Conformal Conic projection.  The
		longitude and latitude must be in radians.  The Easting
		and Northing values will be returned in meters.


ALGORITHM REFERENCES

1.  Snyder, John P., "Map Projections--A Working Manual", U.S. Geological
    Survey Professional Paper 1395 (Supersedes USGS Bulletin 1532), United
    State Government Printing Office, Washington D.C., 1987.

2.  Snyder, John P. and Voxland, Philip M., "An Album of Map Projections",
    U.S. Geological Survey Professional Paper 1453 , United State Government
*******************************************************************************/


//<2104> +proj=lcc +lat_1=10.16666666666667 +lat_0=10.16666666666667 +lon_0=-71.60561777777777 +k_0=1 +x0=-17044 +x0=-23139.97 +ellps=intl +units=m +no_defs  no_defs

// Initialize the Lambert Conformal conic projection
// -----------------------------------------------------------------

//Proj4js.Proj.lcc = Class.create();
Proj4js.Proj.lcc = {
  init : function() {

    // array of:  r_maj,r_min,lat1,lat2,c_lon,c_lat,false_east,false_north
    //double c_lat;                   /* center latitude                      */
    //double c_lon;                   /* center longitude                     */
    //double lat1;                    /* first standard parallel              */
    //double lat2;                    /* second standard parallel             */
    //double r_maj;                   /* major axis                           */
    //double r_min;                   /* minor axis                           */
    //double false_east;              /* x offset in meters                   */
    //double false_north;             /* y offset in meters                   */

      if (!this.lat2){this.lat2=this.lat0;}//if lat2 is not defined
      if (!this.k0) this.k0 = 1.0;

    // Standard Parallels cannot be equal and on opposite sides of the equator
      if (Math.abs(this.lat1+this.lat2) < Proj4js.common.EPSLN) {
        Proj4js.reportError("lcc:init: Equal Latitudes");
        return;
      }

      var temp = this.b / this.a;
      this.e = Math.sqrt(1.0 - temp*temp);

      var sin1 = Math.sin(this.lat1);
      var cos1 = Math.cos(this.lat1);
      var ms1 = Proj4js.common.msfnz(this.e, sin1, cos1);
      var ts1 = Proj4js.common.tsfnz(this.e, this.lat1, sin1);

      var sin2 = Math.sin(this.lat2);
      var cos2 = Math.cos(this.lat2);
      var ms2 = Proj4js.common.msfnz(this.e, sin2, cos2);
      var ts2 = Proj4js.common.tsfnz(this.e, this.lat2, sin2);

      var ts0 = Proj4js.common.tsfnz(this.e, this.lat0, Math.sin(this.lat0));

      if (Math.abs(this.lat1 - this.lat2) > Proj4js.common.EPSLN) {
        this.ns = Math.log(ms1/ms2)/Math.log(ts1/ts2);
      } else {
        this.ns = sin1;
      }
      this.f0 = ms1 / (this.ns * Math.pow(ts1, this.ns));
      this.rh = this.a * this.f0 * Math.pow(ts0, this.ns);
      if (!this.title) this.title = "Lambert Conformal Conic";
    },


    // Lambert Conformal conic forward equations--mapping lat,long to x,y
    // -----------------------------------------------------------------
    forward : function(p) {

      var lon = p.x;
      var lat = p.y;

    // convert to radians
      if ( lat <= 90.0 && lat >= -90.0 && lon <= 180.0 && lon >= -180.0) {
        //lon = lon * Proj4js.common.D2R;
        //lat = lat * Proj4js.common.D2R;
      } else {
        Proj4js.reportError("lcc:forward: llInputOutOfRange: "+ lon +" : " + lat);
        return null;
      }

      var con  = Math.abs( Math.abs(lat) - Proj4js.common.HALF_PI);
      var ts, rh1;
      if (con > Proj4js.common.EPSLN) {
        ts = Proj4js.common.tsfnz(this.e, lat, Math.sin(lat) );
        rh1 = this.a * this.f0 * Math.pow(ts, this.ns);
      } else {
        con = lat * this.ns;
        if (con <= 0) {
          Proj4js.reportError("lcc:forward: No Projection");
          return null;
        }
        rh1 = 0;
      }
      var theta = this.ns * Proj4js.common.adjust_lon(lon - this.long0);
      p.x = this.k0 * (rh1 * Math.sin(theta)) + this.x0;
      p.y = this.k0 * (this.rh - rh1 * Math.cos(theta)) + this.y0;

      return p;
    },

  // Lambert Conformal Conic inverse equations--mapping x,y to lat/long
  // -----------------------------------------------------------------
  inverse : function(p) {

    var rh1, con, ts;
    var lat, lon;
    var x = (p.x - this.x0)/this.k0;
    var y = (this.rh - (p.y - this.y0)/this.k0);
    if (this.ns > 0) {
      rh1 = Math.sqrt (x * x + y * y);
      con = 1.0;
    } else {
      rh1 = -Math.sqrt (x * x + y * y);
      con = -1.0;
    }
    var theta = 0.0;
    if (rh1 != 0) {
      theta = Math.atan2((con * x),(con * y));
    }
    if ((rh1 != 0) || (this.ns > 0.0)) {
      con = 1.0/this.ns;
      ts = Math.pow((rh1/(this.a * this.f0)), con);
      lat = Proj4js.common.phi2z(this.e, ts);
      if (lat == -9999) return null;
    } else {
      lat = -Proj4js.common.HALF_PI;
    }
    lon = Proj4js.common.adjust_lon(theta/this.ns + this.long0);

    p.x = lon;
    p.y = lat;
    return p;
  }
};




/* ======================================================================
    projCode/laea.js
   ====================================================================== */

/*******************************************************************************
NAME                  LAMBERT AZIMUTHAL EQUAL-AREA
 
PURPOSE:	Transforms input longitude and latitude to Easting and
		Northing for the Lambert Azimuthal Equal-Area projection.  The
		longitude and latitude must be in radians.  The Easting
		and Northing values will be returned in meters.

PROGRAMMER              DATE            
----------              ----           
D. Steinwand, EROS      March, 1991   

This function was adapted from the Lambert Azimuthal Equal Area projection
code (FORTRAN) in the General Cartographic Transformation Package software
which is available from the U.S. Geological Survey National Mapping Division.
 
ALGORITHM REFERENCES

1.  "New Equal-Area Map Projections for Noncircular Regions", John P. Snyder,
    The American Cartographer, Vol 15, No. 4, October 1988, pp. 341-355.

2.  Snyder, John P., "Map Projections--A Working Manual", U.S. Geological
    Survey Professional Paper 1395 (Supersedes USGS Bulletin 1532), United
    State Government Printing Office, Washington D.C., 1987.

3.  "Software Documentation for GCTP General Cartographic Transformation
    Package", U.S. Geological Survey National Mapping Division, May 1982.
*******************************************************************************/

Proj4js.Proj.laea = {
  S_POLE: 1,
  N_POLE: 2,
  EQUIT: 3,
  OBLIQ: 4,


/* Initialize the Lambert Azimuthal Equal Area projection
  ------------------------------------------------------*/
  init: function() {
    var t = Math.abs(this.lat0);
    if (Math.abs(t - Proj4js.common.HALF_PI) < Proj4js.common.EPSLN) {
      this.mode = this.lat0 < 0. ? this.S_POLE : this.N_POLE;
    } else if (Math.abs(t) < Proj4js.common.EPSLN) {
      this.mode = this.EQUIT;
    } else {
      this.mode = this.OBLIQ;
    }
    if (this.es > 0) {
      var sinphi;
  
      this.qp = Proj4js.common.qsfnz(this.e, 1.0);
      this.mmf = .5 / (1. - this.es);
      this.apa = this.authset(this.es);
      switch (this.mode) {
        case this.N_POLE:
        case this.S_POLE:
          this.dd = 1.;
          break;
        case this.EQUIT:
          this.rq = Math.sqrt(.5 * this.qp);
          this.dd = 1. / this.rq;
          this.xmf = 1.;
          this.ymf = .5 * this.qp;
          break;
        case this.OBLIQ:
          this.rq = Math.sqrt(.5 * this.qp);
          sinphi = Math.sin(this.lat0);
          this.sinb1 = Proj4js.common.qsfnz(this.e, sinphi) / this.qp;
          this.cosb1 = Math.sqrt(1. - this.sinb1 * this.sinb1);
          this.dd = Math.cos(this.lat0) / (Math.sqrt(1. - this.es * sinphi * sinphi) * this.rq * this.cosb1);
          this.ymf = (this.xmf = this.rq) / this.dd;
          this.xmf *= this.dd;
          break;
      }
    } else {
      if (this.mode == this.OBLIQ) {
        this.sinph0 = Math.sin(this.lat0);
        this.cosph0 = Math.cos(this.lat0);
      }
    }
  },

/* Lambert Azimuthal Equal Area forward equations--mapping lat,long to x,y
  -----------------------------------------------------------------------*/
  forward: function(p) {

    /* Forward equations
      -----------------*/
    var x,y;
    var lam=p.x;
    var phi=p.y;
    lam = Proj4js.common.adjust_lon(lam - this.long0);
    
    if (this.sphere) {
        var coslam, cosphi, sinphi;
      
        sinphi = Math.sin(phi);
        cosphi = Math.cos(phi);
        coslam = Math.cos(lam);
        switch (this.mode) {
          case this.OBLIQ:
          case this.EQUIT:
            y = (this.mode == this.EQUIT) ? 1. + cosphi * coslam : 1. + this.sinph0 * sinphi + this.cosph0 * cosphi * coslam;
            if (y <= Proj4js.common.EPSLN) {
              Proj4js.reportError("laea:fwd:y less than eps");
              return null;
            }
            y = Math.sqrt(2. / y);
            x = y * cosphi * Math.sin(lam);
            y *= (this.mode == this.EQUIT) ? sinphi : this.cosph0 * sinphi - this.sinph0 * cosphi * coslam;
            break;
          case this.N_POLE:
            coslam = -coslam;
          case this.S_POLE:
            if (Math.abs(phi + this.phi0) < Proj4js.common.EPSLN) {
              Proj4js.reportError("laea:fwd:phi < eps");
              return null;
            }
            y = Proj4js.common.FORTPI - phi * .5;
            y = 2. * ((this.mode == this.S_POLE) ? Math.cos(y) : Math.sin(y));
            x = y * Math.sin(lam);
            y *= coslam;
            break;
        }
    } else {
        var coslam, sinlam, sinphi, q, sinb=0.0, cosb=0.0, b=0.0;
      
        coslam = Math.cos(lam);
        sinlam = Math.sin(lam);
        sinphi = Math.sin(phi);
        q = Proj4js.common.qsfnz(this.e, sinphi);
        if (this.mode == this.OBLIQ || this.mode == this.EQUIT) {
          sinb = q / this.qp;
          cosb = Math.sqrt(1. - sinb * sinb);
        }
        switch (this.mode) {
          case this.OBLIQ:
            b = 1. + this.sinb1 * sinb + this.cosb1 * cosb * coslam;
            break;
          case this.EQUIT:
            b = 1. + cosb * coslam;
            break;
          case this.N_POLE:
            b = Proj4js.common.HALF_PI + phi;
            q = this.qp - q;
            break;
          case this.S_POLE:
            b = phi - Proj4js.common.HALF_PI;
            q = this.qp + q;
            break;
        }
        if (Math.abs(b) < Proj4js.common.EPSLN) {
            Proj4js.reportError("laea:fwd:b < eps");
            return null;
        }
        switch (this.mode) {
          case this.OBLIQ:
          case this.EQUIT:
            b = Math.sqrt(2. / b);
            if (this.mode == this.OBLIQ) {
              y = this.ymf * b * (this.cosb1 * sinb - this.sinb1 * cosb * coslam);
            } else {
              y = (b = Math.sqrt(2. / (1. + cosb * coslam))) * sinb * this.ymf;
            }
            x = this.xmf * b * cosb * sinlam;
            break;
          case this.N_POLE:
          case this.S_POLE:
            if (q >= 0.) {
              x = (b = Math.sqrt(q)) * sinlam;
              y = coslam * ((this.mode == this.S_POLE) ? b : -b);
            } else {
              x = y = 0.;
            }
            break;
        }
    }

    //v 1.0
    /*
    var sin_lat=Math.sin(lat);
    var cos_lat=Math.cos(lat);

    var sin_delta_lon=Math.sin(delta_lon);
    var cos_delta_lon=Math.cos(delta_lon);

    var g =this.sin_lat_o * sin_lat +this.cos_lat_o * cos_lat * cos_delta_lon;
    if (g == -1.0) {
      Proj4js.reportError("laea:fwd:Point projects to a circle of radius "+ 2.0 * R);
      return null;
    }
    var ksp = this.a * Math.sqrt(2.0 / (1.0 + g));
    var x = ksp * cos_lat * sin_delta_lon + this.x0;
    var y = ksp * (this.cos_lat_o * sin_lat - this.sin_lat_o * cos_lat * cos_delta_lon) + this.y0;
    */
    p.x = this.a*x + this.x0;
    p.y = this.a*y + this.y0;
    return p;
  },//lamazFwd()

/* Inverse equations
  -----------------*/
  inverse: function(p) {
    p.x -= this.x0;
    p.y -= this.y0;
    var x = p.x/this.a;
    var y = p.y/this.a;
    
    if (this.sphere) {
        var  cosz=0.0, rh, sinz=0.0;
      
        rh = Math.sqrt(x*x + y*y);
        var phi = rh * .5;
        if (phi > 1.) {
          Proj4js.reportError("laea:Inv:DataError");
          return null;
        }
        phi = 2. * Math.asin(phi);
        if (this.mode == this.OBLIQ || this.mode == this.EQUIT) {
          sinz = Math.sin(phi);
          cosz = Math.cos(phi);
        }
        switch (this.mode) {
        case this.EQUIT:
          phi = (Math.abs(rh) <= Proj4js.common.EPSLN) ? 0. : Math.asin(y * sinz / rh);
          x *= sinz;
          y = cosz * rh;
          break;
        case this.OBLIQ:
          phi = (Math.abs(rh) <= Proj4js.common.EPSLN) ? this.phi0 : Math.asin(cosz * sinph0 + y * sinz * cosph0 / rh);
          x *= sinz * cosph0;
          y = (cosz - Math.sin(phi) * sinph0) * rh;
          break;
        case this.N_POLE:
          y = -y;
          phi = Proj4js.common.HALF_PI - phi;
          break;
        case this.S_POLE:
          phi -= Proj4js.common.HALF_PI;
          break;
        }
        lam = (y == 0. && (this.mode == this.EQUIT || this.mode == this.OBLIQ)) ? 0. : Math.atan2(x, y);
    } else {
        var cCe, sCe, q, rho, ab=0.0;
      
        switch (this.mode) {
          case this.EQUIT:
          case this.OBLIQ:
            x /= this.dd;
            y *=  this.dd;
            rho = Math.sqrt(x*x + y*y);
            if (rho < Proj4js.common.EPSLN) {
              p.x = 0.;
              p.y = this.phi0;
              return p;
            }
            sCe = 2. * Math.asin(.5 * rho / this.rq);
            cCe = Math.cos(sCe);
            x *= (sCe = Math.sin(sCe));
            if (this.mode == this.OBLIQ) {
              ab = cCe * this.sinb1 + y * sCe * this.cosb1 / rho
              q = this.qp * ab;
              y = rho * this.cosb1 * cCe - y * this.sinb1 * sCe;
            } else {
              ab = y * sCe / rho;
              q = this.qp * ab;
              y = rho * cCe;
            }
            break;
          case this.N_POLE:
            y = -y;
          case this.S_POLE:
            q = (x * x + y * y);
            if (!q ) {
              p.x = 0.;
              p.y = this.phi0;
              return p;
            }
            /*
            q = this.qp - q;
            */
            ab = 1. - q / this.qp;
            if (this.mode == this.S_POLE) {
              ab = - ab;
            }
            break;
        }
        lam = Math.atan2(x, y);
        phi = this.authlat(Math.asin(ab), this.apa);
    }

    /*
    var Rh = Math.Math.sqrt(p.x *p.x +p.y * p.y);
    var temp = Rh / (2.0 * this.a);

    if (temp > 1) {
      Proj4js.reportError("laea:Inv:DataError");
      return null;
    }

    var z = 2.0 * Proj4js.common.asinz(temp);
    var sin_z=Math.sin(z);
    var cos_z=Math.cos(z);

    var lon =this.long0;
    if (Math.abs(Rh) > Proj4js.common.EPSLN) {
       var lat = Proj4js.common.asinz(this.sin_lat_o * cos_z +this. cos_lat_o * sin_z *p.y / Rh);
       var temp =Math.abs(this.lat0) - Proj4js.common.HALF_PI;
       if (Math.abs(temp) > Proj4js.common.EPSLN) {
          temp = cos_z -this.sin_lat_o * Math.sin(lat);
          if(temp!=0.0) lon=Proj4js.common.adjust_lon(this.long0+Math.atan2(p.x*sin_z*this.cos_lat_o,temp*Rh));
       } else if (this.lat0 < 0.0) {
          lon = Proj4js.common.adjust_lon(this.long0 - Math.atan2(-p.x,p.y));
       } else {
          lon = Proj4js.common.adjust_lon(this.long0 + Math.atan2(p.x, -p.y));
       }
    } else {
      lat = this.lat0;
    }
    */
    //return(OK);
    p.x = Proj4js.common.adjust_lon(this.long0+lam);
    p.y = phi;
    return p;
  },//lamazInv()
  
/* determine latitude from authalic latitude */
  P00: .33333333333333333333,
  P01: .17222222222222222222,
  P02: .10257936507936507936,
  P10: .06388888888888888888,
  P11: .06640211640211640211,
  P20: .01641501294219154443,
  
  authset: function(es) {
    var t;
    var APA = new Array();
    APA[0] = es * this.P00;
    t = es * es;
    APA[0] += t * this.P01;
    APA[1] = t * this.P10;
    t *= es;
    APA[0] += t * this.P02;
    APA[1] += t * this.P11;
    APA[2] = t * this.P20;
    return APA;
  },
  
  authlat: function(beta, APA) {
    var t = beta+beta;
    return(beta + APA[0] * Math.sin(t) + APA[1] * Math.sin(t+t) + APA[2] * Math.sin(t+t+t));
  }
  
};



/* ======================================================================
    projCode/aeqd.js
   ====================================================================== */

Proj4js.Proj.aeqd = {

  init : function() {
    this.sin_p12=Math.sin(this.lat0);
    this.cos_p12=Math.cos(this.lat0);
  },

  forward: function(p) {
    var lon=p.x;
    var lat=p.y;
    var ksp;

    var sinphi=Math.sin(p.y);
    var cosphi=Math.cos(p.y); 
    var dlon = Proj4js.common.adjust_lon(lon - this.long0);
    var coslon = Math.cos(dlon);
    var g = this.sin_p12 * sinphi + this.cos_p12 * cosphi * coslon;
    if (Math.abs(Math.abs(g) - 1.0) < Proj4js.common.EPSLN) {
       ksp = 1.0;
       if (g < 0.0) {
         Proj4js.reportError("aeqd:Fwd:PointError");
         return;
       }
    } else {
       var z = Math.acos(g);
       ksp = z/Math.sin(z);
    }
    p.x = this.x0 + this.a * ksp * cosphi * Math.sin(dlon);
    p.y = this.y0 + this.a * ksp * (this.cos_p12 * sinphi - this.sin_p12 * cosphi * coslon);
    return p;
  },

  inverse: function(p){
    p.x -= this.x0;
    p.y -= this.y0;

    var rh = Math.sqrt(p.x * p.x + p.y *p.y);
    if (rh > (2.0 * Proj4js.common.HALF_PI * this.a)) {
       Proj4js.reportError("aeqdInvDataError");
       return;
    }
    var z = rh / this.a;

    var sinz=Math.sin(z);
    var cosz=Math.cos(z);

    var lon = this.long0;
    var lat;
    if (Math.abs(rh) <= Proj4js.common.EPSLN) {
      lat = this.lat0;
    } else {
      lat = Proj4js.common.asinz(cosz * this.sin_p12 + (p.y * sinz * this.cos_p12) / rh);
      var con = Math.abs(this.lat0) - Proj4js.common.HALF_PI;
      if (Math.abs(con) <= Proj4js.common.EPSLN) {
        if (lat0 >= 0.0) {
          lon = Proj4js.common.adjust_lon(this.long0 + Math.atan2(p.x , -p.y));
        } else {
          lon = Proj4js.common.adjust_lon(this.long0 - Math.atan2(-p.x , p.y));
        }
      } else {
        con = cosz - this.sin_p12 * Math.sin(lat);
        if ((Math.abs(con) < Proj4js.common.EPSLN) && (Math.abs(p.x) < Proj4js.common.EPSLN)) {
           //no-op, just keep the lon value as is
        } else {
          var temp = Math.atan2((p.x * sinz * this.cos_p12), (con * rh));
          lon = Proj4js.common.adjust_lon(this.long0 + Math.atan2((p.x * sinz * this.cos_p12), (con * rh)));
        }
      }
    }

    p.x = lon;
    p.y = lat;
    return p;
  } 
};
/* ======================================================================
    projCode/moll.js
   ====================================================================== */

/*******************************************************************************
NAME                            MOLLWEIDE

PURPOSE:	Transforms input longitude and latitude to Easting and
		Northing for the MOllweide projection.  The
		longitude and latitude must be in radians.  The Easting
		and Northing values will be returned in meters.

PROGRAMMER              DATE
----------              ----
D. Steinwand, EROS      May, 1991;  Updated Sept, 1992; Updated Feb, 1993
S. Nelson, EDC		Jun, 2993;	Made corrections in precision and
					number of iterations.

ALGORITHM REFERENCES

1.  Snyder, John P. and Voxland, Philip M., "An Album of Map Projections",
    U.S. Geological Survey Professional Paper 1453 , United State Government
    Printing Office, Washington D.C., 1989.

2.  Snyder, John P., "Map Projections--A Working Manual", U.S. Geological
    Survey Professional Paper 1395 (Supersedes USGS Bulletin 1532), United
    State Government Printing Office, Washington D.C., 1987.
*******************************************************************************/

Proj4js.Proj.moll = {

  /* Initialize the Mollweide projection
    ------------------------------------*/
  init: function(){
    //no-op
  },

  /* Mollweide forward equations--mapping lat,long to x,y
    ----------------------------------------------------*/
  forward: function(p) {

    /* Forward equations
      -----------------*/
    var lon=p.x;
    var lat=p.y;

    var delta_lon = Proj4js.common.adjust_lon(lon - this.long0);
    var theta = lat;
    var con = Proj4js.common.PI * Math.sin(lat);

    /* Iterate using the Newton-Raphson method to find theta
      -----------------------------------------------------*/
    for (var i=0;true;i++) {
       var delta_theta = -(theta + Math.sin(theta) - con)/ (1.0 + Math.cos(theta));
       theta += delta_theta;
       if (Math.abs(delta_theta) < Proj4js.common.EPSLN) break;
       if (i >= 50) {
          Proj4js.reportError("moll:Fwd:IterationError");
         //return(241);
       }
    }
    theta /= 2.0;

    /* If the latitude is 90 deg, force the x coordinate to be "0 + false easting"
       this is done here because of precision problems with "cos(theta)"
       --------------------------------------------------------------------------*/
    if (Proj4js.common.PI/2 - Math.abs(lat) < Proj4js.common.EPSLN) delta_lon =0;
    var x = 0.900316316158 * this.a * delta_lon * Math.cos(theta) + this.x0;
    var y = 1.4142135623731 * this.a * Math.sin(theta) + this.y0;

    p.x=x;
    p.y=y;
    return p;
  },

  inverse: function(p){
    var theta;
    var arg;

    /* Inverse equations
      -----------------*/
    p.x-= this.x0;
    //~ p.y -= this.y0;
    var arg = p.y /  (1.4142135623731 * this.a);

    /* Because of division by zero problems, 'arg' can not be 1.0.  Therefore
       a number very close to one is used instead.
       -------------------------------------------------------------------*/
    if(Math.abs(arg) > 0.999999999999) arg=0.999999999999;
    var theta =Math.asin(arg);
    var lon = Proj4js.common.adjust_lon(this.long0 + (p.x / (0.900316316158 * this.a * Math.cos(theta))));
    if(lon < (-Proj4js.common.PI)) lon= -Proj4js.common.PI;
    if(lon > Proj4js.common.PI) lon= Proj4js.common.PI;
    arg = (2.0 * theta + Math.sin(2.0 * theta)) / Proj4js.common.PI;
    if(Math.abs(arg) > 1.0)arg=1.0;
    var lat = Math.asin(arg);
    //return(OK);

    p.x=lon;
    p.y=lat;
    return p;
  }
};

;Proj4js.defs["EPSG:27700"] = "+title=British National Grid +proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717 +x_0=400000 +y_0=-100000 +ellps=airy +datum=OSGB36 +units=m +no_defs ";;Proj4js.defs["EPSG:29901"] = "+title=Northern Irish Grid +proj=tmerc +lat_0=53.5 +lon_0=-8 +k=1 +x_0=200000 +y_0=250000 +ellps=airy +towgs84=482.5,-130.6,564.6,-1.042,-0.214,-0.631,8.15 +units=m +no_defs ";;Proj4js.defs["EPSG:29902"] = "+title=Irish National Grid +proj=tmerc +lat_0=53.5 +lon_0=-8 +k=1.000035 +x_0=200000 +y_0=250000 +a=6377340.189 +b=6356034.447938534 +units=m +no_defs ";;Proj4js.defs["EPSG:4326"] = "+title=EPSG4326 +proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs ";;<!--
// JavaScript Document
/*
    Location details for Barton
    OSGB36
    TA 02963 22784
    
    WGS84 
    W 00 26 26
    N 53 41 28
    
    functions: ecc, ll_to_cartesin, cartesian_to_ll,
               switch_ellisoids, wgs84_osgb36, 
               find_gridsquare and ngr_data
    are adapted from http://www.carabus.co.uk/lltongr.html
    
    dms_to_dec concatenating and giving two decimal points.
*/
var deg2rad = Math.PI / 180;
var rad2deg = 180.0 / Math.PI;
function DegreesMinutesSeconds (pos, deg, min, sec) {
   this.positive = Number(pos);
   this.degrees = Number(deg);
   this.minutes = Number(min);
   this.seconds = Number(sec);
}
function DMSCoordinate (lat_dms_obj, lon_dms_obj) {
   this.lat_dms_obj = lat_dms_obj;
   this.lon_dms_obj = lon_dms_obj;
}
function DegreesDecimalCoordinate (lat, lon) {
   this.latitude  = Number(lat);
   this.longitude = Number(lon);
   this.accuracy = 1;
   this.asString = function() {
      var len = 7 - new String(this.accuracy).length;
      var acc = Math.pow(10,len); 
      lat = Math.floor(this.latitude * acc)/acc;          
      lon = Math.floor(this.longitude * acc)/acc;          
      return lat + ', ' + lon;
   };
   this.lat = function() {
      return this.latitude;
   }
   this.lng = function() {
      return this.longitude;
   }
}
function GridReference (eing, ning) {
   this.easting = Number(eing);
   this.northing = Number(ning);
   this.accuracy = 1;
}
function NationalGridReference (sq, eing, ning) {
   this.square = sq.toUpperCase();
   this.easting = Number(eing);
   this.northing = Number(ning);
   this.accuracy = 1;
   this.fromString = function(grid) {
      var length,frag,acc,e,n,x,y;
      this.square = grid.substring(0,2);
      length = grid.length();
      frag = Math.floor((length - 2) / 2);
      acc = Math.pow(10,5-frag);
      e = grid.substring(2,frag) * acc;
      n = grid.substring((frag+2),frag) * acc;
      if ((length % 2) == 1) {
         tetrads = tetradArray();
         tetrad = grid.substring(length-1);
         for (x=0;x<=5;x++) {
            for (y=0;y<=5;y++) {
               if (tetrad == tetrads[x][y]) {
                  e += (x * 2000);
                  n += (y * 2000);
                  acc = 2000;
               }
            }
         }
      }
      this.easting = e;
      this.northing = n;
      this.accuracy = acc;
   };
   this.asString = function() {
      var e,n,acc,m,suffix,x,y;
      acc = new String(this.accuracy);
      m = parseInt(acc.substr(0,1));
      var length = 5 - (String(acc).length - 1);
      switch(m) {
      case 1: {
         suffix = '';
      }break;
      case 2: {
         var tetrads = tetradArray();
         length -= 1;         
         x = Math.floor(parseInt(formatToXPlaces(this.easting,5,0).substr(length,1))/2);
         y = Math.floor(parseInt(formatToXPlaces(this.northing,5,0).substr(length,1))/2);
         acc = Math.pow(10,acc.length);
         suffix = tetrads[x][y];                        
      }break;
      case 5: {
         var sq5km = sq5kmArray();
         length -= 1;         
         x = Math.floor(parseInt(formatToXPlaces(this.easting,5,0).substr(length,1))/5);
         y = Math.floor(parseInt(formatToXPlaces(this.northing,5,0).substr(length,1))/5);         
         acc = Math.pow(10,acc.length);
         suffix = sq5km[x][y];               
      }break;
      default: {
         acc = Math.pow(10,acc.length);
         length -= 1;
         suffix = '';
      }break;
      }
      e = formatToXPlaces(Math.floor(this.easting / acc),length,0);
      n = formatToXPlaces(Math.floor(this.northing / acc),length,0);
      return this.square + e + n + suffix;      
   };
}
function CartesianCoordinate (x, y, z) {
    this.x = Number(x);
    this.y = Number(y);
    this.z = Number(z);
}
function Extents (top,left,bottom,right) {
   this.top = top;
   this.left = left;
   this.bottom = bottom;
   this.right = right;
}

function ecc(major, minor) {
	return (major*major - minor*minor) / (major*major);
}
/*
    var min_AXIS = 6356256.910;
    var maj_AXIS = 6377563.396;
    ECCENTRIC = ecc(maj_AXIS, min_AXIS);
*/
function ll_to_cartesian(lat, lon, axis, ecc, height) {
    var v = axis / (Math.sqrt (1 - ecc * (Math.pow (Math.sin(lat), 2))));
    var x = (v + height) * Math.cos(lat) * Math.cos(lon);
    var y = (v + height) * Math.cos(lat) * Math.sin(lon);
    var z = ((1 - ecc) * v + height) * Math.sin(lat);
    var cart = new CartesianCoordinate (x, y, z );
    return (cart);
}
function cartesian_to_ll(x, y, z, ecc, axis) {
    var lon = Math.atan(y / x);
    var p = Math.sqrt((x * x) + (y * y));
    var lat = Math.atan(z / (p * (1 - ecc)));
    var v = axis / (Math.sqrt(1 - ecc * (Math.sin(lat) * Math.sin(lat))));
    var errvalue = 1.0;
    var lat0 = 0;
    while (errvalue > 0.001)
    {
      lat0 = Math.atan((z + ecc * v * Math.sin(lat)) / p);  
      errvalue = Math.abs(lat0 - lat);
      lat=lat0;
    }
    var height = p / Math.cos(lat) - v;
    var geo = new DegreesDecimalCoordinate(lat, lon);
    return(geo);
}
function switch_ellipsoids  (dd_coord_obj) {
    var lat = dd_coord_obj.latitude;
    var lon = dd_coord_obj.longitude;
    lat *= deg2rad;  // GRS80/WGS84 input in radians
    lon *= deg2rad;  // ditto
    var axis = 6378137.0;         // GRS80/WGS84 major axis
    var ecc = 0.00669438;         // ditto eccentricity  
    var height = 0;               // height above datum
    var cart = ll_to_cartesian(lat, lon, axis, ecc, height);
    var x = cart.x - 371;
    var y = cart.y + 112;
    var z = cart.z - 434;
    axis = 6377563.396;
    ecc = 0.00667054;
    geo = cartesian_to_ll(x, y, z, ecc, axis);
    lat = geo.latitude * rad2deg; 
    lon = geo.longitude * rad2deg;//alert ('lat:' + lat + '\nlon:' + lon);
    var dd_coord = new DegreesDecimalCoordinate(lat,lon);
    return (dd_coord);
}
/*
function wgs84_to_osgb36(dd_coord_obj) {
    dd_coord_obj = switch_ellipsoids(dd_coord_obj);
    var lat = dd_coord_obj.latitude;
    var lon = dd_coord_obj.longitude;
    var phip = lat * deg2rad;      // convert latitude to radians
    var lambdap = lon * deg2rad;   // convert longitude to radians
    var AXIS = 6377563.396;        // the Airy major axis
    var ECCENTRIC = 0.00667054;    // Airy eccentricity
    var LAT_ORIG = 49 * deg2rad;   
    var lon_ORIG = -2 * deg2rad;
    var FALSE_EAST = 400000.0;
    var FALSE_NORTH = -100000.0;
    var SCALE = 0.9996012717;
    var east = '';                 // variable to hold NGR Eastings
    var north = '';                 // variable to hold NGR Northings
    //
    // The five uncommented constants relate to the British National Grid. This has a
    // reference point (origin) based on 49 degrees north, 2 degrees west (west is negative). 
    // The FALSE_EAST and FALSE_NORTH are just offsets to ensure that the grid reference is
    // always positive.
    // Now we convert the latitude and longitude to National Grid Northings and Eastings.
    //
    var j = (lambdap - lon_ORIG) * Math.cos(phip);
    var nsqd = ECCENTRIC * (Math.cos(phip) * Math.cos(phip)) / (1 - ECCENTRIC);
    var tsqd = Math.pow(Math.sin(phip) / (Math.cos(phip)), 2);
    var up = AXIS / Math.sqrt(1 - (ECCENTRIC * (Math.sin(phip) * Math.sin(phip))));
    var p = ECCENTRIC / 8.0;
    var a = AXIS * (1 - (2 * p) - (3 * p * p) - (10 * p * p * p));
    var b = AXIS * ((6 * p) + (12 * p * p) + (45 * p * p * p)) / 2;
    var c = AXIS * ((15 * p * p) + (90 * p * p * p)) / 4;
    var mo = (a * LAT_ORIG) - (b * Math.sin(2 * LAT_ORIG)) + (c * Math.sin(4 * LAT_ORIG));
    var mp = (a * phip) - (b * Math.sin(2 * phip)) + (c * Math.sin(4 * phip));
    east = 0.5 + FALSE_EAST + ((SCALE * up) * (j + ((j * j * j) / 6) * (1 - tsqd + nsqd)));
    north = 0.5 + FALSE_NORTH + (SCALE * (mp - mo)) + (SCALE * (up) * ((Math.sin(phip)) / (Math.cos(phip)))) * (((j * j)/ 2) + ((j * j * j * j) / 24 * (5 - tsqd)));
    east = Math.round(east);       // round to whole number
    north = Math.round(north);     // round to whole number
    var gr_obj = new GridReference (east, north);
    gr_obj.accuracy = dd_coord_obj.accuracy;
    return gr_obj;
}
*/
function find_gridsquare (gr_obj) {
    var east = Math.floor(gr_obj.easting);
    var north = Math.floor(gr_obj.northing);
    var eX = east / 500000;
    var nX = north / 500000;
    var tmp = Math.floor(eX)-5.0 * Math.floor(nX)+17.0;
    nX = 5 * (nX - Math.floor(nX));
    eX = 20 - 5.0 * Math.floor(nX) + Math.floor(5.0 * (eX - Math.floor(eX)));
    if (eX > 7.5)
      eX = eX + 1;
    if (tmp > 7.5)
      tmp = tmp + 1;
    var eing = String(east);
    var ning = String(north);
    var lnth = eing.length;
    eing = eing.substring(lnth - 5, lnth);
    lnth = ning.length;
    ning = ning.substring(lnth - 5, lnth);
    var sq = String.fromCharCode(tmp + 65) + String.fromCharCode(eX + 65);
    var ngr_obj = new NationalGridReference(sq, eing, ning);
    ngr_obj.accuracy = gr_obj.accuracy;
    return ngr_obj;
}
function conv_ngr_to_ings(ngr_obj) {
    var sq = ngr_obj.square;
    var north = ngr_obj.northing;
    var east = ngr_obj.easting;

    var t1 = sq.charCodeAt(0) - 65;
    
    if (t1 > 8) t1 = t1 -1;
    var t2 = Math.floor(t1 / 5);
    north = north + 500000 * (3 - t2);
    east = east + 500000 * (t1 - 5 * t2 - 2);

    t1 = sq.charCodeAt(1) - 65;
    if (t1 > 8) t1 = t1 - 1;
    t2 = Math.floor(t1 / 5);
    north = north + 100000 * ( 4 - t2);
    east = east + 100000 * ( t1 - 5 * t2);
    gr_obj = new GridReference(east,north);
    gr_obj.accuracy = ngr_obj.accuracy;
    return (gr_obj);
}
/*
function conv_uk_ings_to_ll(gr_obj) {
    var east = gr_obj.easting;
    var north = gr_obj.northing;
    var wgs_axis = 6378137;
    var wgs_eccent = 0.00669438;
    var latorig = 49 * deg2rad;
    var lonorig = -2 * deg2rad;
    var scale = 0.9996012717;
    var falseeast = 400000;
    var falsenorth = -100000;
    var axis = 6377563.396;
    var eccent = 0.00667054;
    var ep = Number(east);
    var np = Number(north);
    var p = eccent / 8;
    var a = axis * (1 - (2 * p) - (3 * p * p) - (10 * p * p * p));
    var b = axis * ((6 * p) + (12 * p * p) + (45 * p * p * p)) / 2;
    var c = axis * ((15 * p * p) + (90 * p * p * p)) / 4;
    var mo = a * latorig - b * Math.sin(2 * latorig) + c * Math.sin(4 * latorig);
    var mp = mo + ((np - falsenorth) / scale);
    var phidash = mp / a;
    var phif = phidash + ((b * Math.sin(2 * phidash)) - (c * Math.sin(4 * phidash))) / (a - (2 * b * Math.cos(2 * phidash)));

    var uf = axis / Math.sqrt(1 - (eccent * (Math.sin(phif) * Math.sin(phif))));
    var h = (ep - falseeast) / (scale * uf);

    var nsqd = eccent * (Math.cos(phif) * Math.cos(phif)) / (1 - eccent);
    var tsqd = Math.pow(Math.sin(phif) / Math.cos(phif), 2);

    var lambdap = lonorig + ( 1 / Math.cos(phif)) * ((h - ((h * h * h) / 6) * (1 + (2 * tsqd) + nsqd)));
    var phip = phif - ((1 + nsqd) * (Math.sin(phif)/Math.cos(phif)) * (((h * h) / 2) - ((h * h * h * h) / 24) * (5 + 3 * tsqd))); 

    var cart = ll_to_cartesian(phip, lambdap, axis, eccent, 0);

    var x = cart.x + 371;
    var y = cart.y - 112;
    var z = cart.z + 434;

    var geo = cartesian_to_ll(x, y, z, wgs_eccent, wgs_axis);
    var lat = geo.latitude * rad2deg;
    var lon = geo.longitude * rad2deg;
    lat = formatToXPlaces(lat,2,6);
    lon = formatToXPlaces(lon,2,6);
    var ddc = new DegreesDecimalCoordinate (lat,lon);
    ddc.accuracy = gr_obj.accuracy;
    return (ddc);
}
*/
function dms_coord_to_dec_coord (dms_coord_obj) {
    var lat = dms_to_dec(dms_coord_obj.lat_dms_obj);
    var lon = dms_to_dec(dms_coord_obj.lon_dms_obj);
    var dd_coord = new DegreesDecimalCoordinate(lat, lon);
    return (dd_coord);
}
function dms_to_dec (dms_obj) {
    var positive = dms_obj.positive;
    var deg = dms_obj.degrees;
    var min = dms_obj.minutes;
    var sec = dms_obj.seconds;
    var a_sixtieth = 1/60;
    var min_dec = (min * a_sixtieth);
    var sec_dec = (sec * (Math.pow(a_sixtieth,2)));
    var dec = deg.valueOf() + min_dec.valueOf() + sec_dec.valueOf();
    if (!positive) {
        dec *= -1;
    }
    dec = formatToXPlaces(dec, 2, 6);
    return dec;
}
function formatToXPlaces (number, leftOfDecimal, rightOfDecimal) {
//alert ("FUNCTION: formatToXPlaces()");
//alert("number: " + number);
   
   number = Math.round(number*Math.pow(10,rightOfDecimal))/Math.pow(10,rightOfDecimal);
   var string = String(number);
   var decimal = string.indexOf('.');
   if (decimal == -1) {
       string = string + '.';
       decimal = string.length - 1;
   }
   for (i=0;i<(leftOfDecimal-decimal);i++) {
       string = '0' + string;
   }
   var desiredLength = leftOfDecimal + rightOfDecimal + 1;
   var length = string.length;
   if (length > desiredLength) {
      string = string.substring(0,desiredLength);
   } else {
      for (i=length;i<desiredLength;i++) {
         string = string + '0';
      }
   }
   if (rightOfDecimal == 0) {
      // remove decimal point
      string = string.substring(0, string.length-1);
   }
//alert("formatted: " + string);
   return string;
}

/* ROUNDS DOWN
   order = 2 rounds number to nearest 100, 
   order = -1 rounds number to nearest 0.1 
   
   multiplier is for rounding to the nearest 0.2 or 300 
   so roundToOrder (713, 3, 3) should return 600 */
function roundToOrder(number,multiplier,order) {
   var i;
//alert(number);
   number /= multiplier;
   if (order < 0) {
      for (i=order;i<0;i++) {
         number *= 10;
      } 
      number = Math.floor(number);
      for (i=order;i<0;i++) {
         number /= 10;
      }
   } else {
      for (i=0;i<order;i++) {
         number /= 10;
//alert(number + ' i:' + i + ' order:' + order);
      }
      number = Math.floor(number);
      for (i=0;i<order;i++) {
         number *= 10;
      } 
   }
//alert(number);
   number *= multiplier;
//alert(number);
   return number;
}

/* ROUNDS TO NEAREST */
function roundToNearestOrder(number,multiplier,order) {
   var i;
//alert(number);
   number /= multiplier;
   if (order < 0) {
      for (i=order;i<0;i++) {
         number *= 10;
      } 
      number = Math.round(number);
      for (i=order;i<0;i++) {
         number /= 10;
      }
   } else {
      for (i=0;i<order;i++) {
         number /= 10;
//alert(number + ' i:' + i + ' order:' + order);
      }
      number = Math.round(number);
      for (i=0;i<order;i++) {
         number *= 10;
      } 
   }
//alert(number);
   number *= multiplier;
//alert(number);
   return number;
}

function dec_coord_to_dms_coord(dd_coord) {
    var lat_dms_obj = dec_to_dms(dd_coord.latitude);
    var lon_dms_obj = dec_to_dms(dd_coord.longitude);
    var dms_coord = new DMSCoordinate (lat_dms_obj, lon_dms_obj);
    return (dms_coord);
}
function dec_to_dms (dec) {
    var pos = (dec > 0);
    dec = Math.abs(dec);
    var deg = Math.floor(dec);
    var remains = dec - deg;
    remains *= 60;
    var min = Math.floor(remains);
    remains -= min;
    remains *= 60;
    var sec = Math.round(remains);
    var dms_obj = new DegreesMinutesSeconds(pos, deg, min, sec);
    return (dms_obj);
}
function update_coords(id) {
    var tokens = id.split(':');
    if (tokens[1] == 'OSGB36') {
        var valid = validate_os_data();
        if (valid) {
            var ngr_obj = get_os_data();
            var gr_obj = conv_ngr_to_ings(ngr_obj);
            var dd_coord_obj = conv_uk_ings_to_ll(gr_obj);
            set_dec_data(dd_coord_obj);
            var dms_coord_obj = dec_coord_to_dms_coord(dd_coord_obj);
            set_dms_coord(dms_coord_obj);
        }
    } else if (tokens[tokens.length-1] != 'decimal') {
        validate_dms_data();
        var dms_coord_obj = get_dms_coordinate();
        var dd_coord_obj = dms_coord_to_dec_coord (dms_coord_obj);
        set_dec_data(dd_coord);
        update_os_coords(dd_coord_obj);
    } else {
        validate_dec_data();
        var dd_coord_obj = get_dec_coord();
        var dms_coord_obj = dec_coord_to_dms_coord(dd_coord_obj);
        set_dms_coord(dms_coord_obj);
        update_os_coords(dd_coord_obj);
    }
}
function update_os_coords (dd_coord_obj) {
    var gr_obj = wgs84_to_osgb36(dd_coord_obj);
    var ngr_obj = find_gridsquare(gr_obj);
    var sq = ngr_obj.square;
    if (!sq.match(/[A-Z]{2}/)) {
        ngr_obj = new NationalGridReference('',0,0);
    }
    set_os_data(ngr_obj);
}
function get_dms_coordinate () {
    var lat_dms_obj = get_dms_data('latitude');
    var lon_dms_obj = get_dms_data('longitude');
    var dms_coord_obj = new DMSCoordinate(lat_dms_obj, lon_dms_obj);
    return dms_coord_obj;
} 
function get_dms_data (axis) {
    var pos = 0;
    var positive = document.getElementById('input:' + axis + ':positive').value;
    if (axis == 'longitude') {
        positive.match(/E/i)?pos=1:pos=0;
    } else {
        positive.match(/N/i)?pos=1:pos=0;
    }
    var deg = document.getElementById('input:' + axis + ':degrees').value;
    var min = document.getElementById('input:' + axis + ':minutes').value;
    var sec = document.getElementById('input:' + axis + ':seconds').value;
    var dms_obj = new DegreesMinutesSeconds (pos, deg, min, sec);
    return dms_obj;
}
function get_dec_coord () {
    var lat = document.getElementById('input:latitude:decimal').value;
    var lon = document.getElementById('input:longitude:decimal').value;
    var dd_coord = new DegreesDecimalCoordinate(lat,lon);
    return (dd_coord);
}
function get_dec_data (axis) {
   if (axis = 'longitude') {
      return document.getElementById('input:longitude:decimal').value;
   } else {
      return document.getElementById('input:latitude:decimal').value;
   }
}
function get_os_data () {
   var sq = document.getElementById('input:OSGB36:gridsquare').value;
   var east = document.getElementById('input:OSGB36:easting').value;
   var north = document.getElementById('input:OSGB36:northing').value;
   var precision = 5;
   if (String(east).length != String(north).length) {
      alert ('Please enter both references in the same number of digits.');
      return false;
   } else {
      if (!sq || sq == '') {
         precision = 6;
      }
      while (String(east).length < precision 
               && String(north).length < precision) {
         east*=10;
         north*=10;
      }
   }
   var ngr_obj = new NationalGridReference(sq, east, north);
   return ngr_obj;
}
function set_dec_data(dd_coord) {
    var lat = dd_coord.latitude;
    var lon = dd_coord.longitude;
    document.getElementById('input:longitude:decimal').value = lon;
    document.getElementById('input:latitude:decimal').value = lat;
}
function set_dms_coord(dms_coord_obj) {
    set_dms_data('latitude', dms_coord_obj.lat_dms_obj);
    set_dms_data('longitude', dms_coord_obj.lon_dms_obj);
}
function set_dms_data(axis, dms_obj) {
    var pos = dms_obj.positive;
    if (axis == 'longitude') {
        if(pos) {pos='E';} else {pos='W';}
    } else if (axis == 'latitude') {
        if (pos) {pos='N';} else {pos='S';}
    }
    document.getElementById('input:' + axis + ':positive').value = pos;
    document.getElementById('input:' + axis + ':degrees').value = dms_obj.degrees;
    document.getElementById('input:' + axis + ':minutes').value = dms_obj.minutes;
    document.getElementById('input:' + axis + ':seconds').value = dms_obj.seconds;
}
function set_os_data(ngr_obj) {
    var sq = ngr_obj.square;
    var east = ngr_obj.easting;
    var north = ngr_obj.northing;
    document.getElementById('input:OSGB36:gridsquare').value = sq;
    east = formatToXPlaces(east, 5, 0);
    north = formatToXPlaces(north, 5, 0);
    document.getElementById('input:OSGB36:easting').value = east;
    document.getElementById('input:OSGB36:northing').value = north;
}
function validate_dec_data () {
    var lon_max = 180;
    var lat_max = 90;
    var lon_dec = Math.abs(get_dec_data('longitude'));
    var lat_dec = Math.abs(get_dec_data('latitude'));
    var alertString = '';
    if (lon_dec > lon_max) {
        alertString += "Longitude as a decimal cannot be greated than " + lon_max + "&deg;\n";
    }
    if (lat_dec > lat_max) {
        alertString += "Latitude as a decimal cannot be greater than " + lat_max + "&deg;\n";
    }
}
function validate_dms_data () {
    var lon_deg_max = 180;
    var lat_deg_max = 90;
    var min_sec_max = 59;
    var lon_dms_obj = get_dms_data('longitude');
    var isEastOfMeridian = lon_dms_obj.positive;
    var lon_deg = lon_dms_obj.degrees;
    var lon_min = lon_dms_obj.minutes;
    var lon_sec = lon_dms_obj.seconds;
    var alertString = '';
    if (lon_deg >lon_deg_max) {
        alertString += '\nLongitude degrees must not exceed ' + lon_deg_max + '\n';    
    }
    if (lon_min > min_sec_max || lon_sec > min_sec_max) {
        alertString += '\nMinutes and seconds cannot exceed ' + min_sec_max + '\n';
    }
    //set_dms_data('longitude', [isEastOfMeridian, lon_deg, lon_min, lon_sec]);
    var lat_dms_obj = get_dms_data('latitude');
    var isNorthOfEquator = lat_dms_obj.positive;
    var lat_deg = lat_dms_obj.degrees;
    var lat_min = lat_dms_obj.minutes;
    var lat_sec = lat_dms_obj.seconds;
    if (lat_deg >lon_deg_max) {
        alertString += '\nLongitude degrees must not exceed ' + lon_deg_max + '\n';    
    }
    if (lat_min > min_sec_max || lat_sec > min_sec_max) {
        alertString += '\nMinutes and seconds cannot exceed ' + min_sec_max + '\n';
    }
    //set_dms_data('latitude', [isNorthOfEquator, lat_deg, lat_min, lat_sec]);
    if (alertString != '') {
        alert(alertString);
    }
}
function validate_os_data () {
   var ngr_obj = get_os_data();
   var sq = ngr_obj.square;
   var east = ngr_obj.easting;
   var north = ngr_obj.northing;
   var tens,i;
   if (east == '' || north == '') return false;
   if (!sq && String(east).length == 6) {
      gr_obj = new GridReference(ngr_obj.easting, ngr_obj.northing);
      ngr_obj = find_gridsquare(gr_obj);
      sq = ngr_obj.square;
      east = ngr_obj.easting;
      north = ngr_obj.northing;
   } else if (!sq.match(/[A-Z]{2}/)) {
      alert('Grid square must be two uppercase characters.\n' + 
            'Please re-enter coordinates');
      return false;
   }
   ngr_obj = new NationalGridReference(sq, east, north);
   set_os_data(ngr_obj);
   return true;
}
function validate_coords () {
    validate_os_data();
    validate_dms_data();
    validate_dec_data();
}
/*
conv_ngr_to_ings(ngr_obj);
conv_uk_ings_to_ll(gr_obj);
*/
function tetradArray() {
   var tk = new Array();
   tk[0] = new Array();
   tk[1] = new Array();
   tk[2] = new Array();
   tk[3] = new Array();
   tk[4] = new Array();
   tk[0][4] = 'E';tk[1][4] = 'J';tk[2][4] = 'P';tk[3][4] = 'U';tk[4][4] = 'Z';
   tk[0][3] = 'D';tk[1][3] = 'I';tk[2][3] = 'N';tk[3][3] = 'T';tk[4][3] = 'Y';
   tk[0][2] = 'C';tk[1][2] = 'H';tk[2][2] = 'M';tk[3][2] = 'S';tk[4][2] = 'X';
   tk[0][1] = 'B';tk[1][1] = 'G';tk[2][1] = 'L';tk[3][1] = 'R';tk[4][1] = 'W';
   tk[0][0] = 'A';tk[1][0] = 'F';tk[2][0] = 'K';tk[3][0] = 'Q';tk[4][0] = 'V';
   return tk;
}
function sq5kmArray() {
   var sq = new Array();
   sq[0] = new Array();
   sq[1] = new Array();
   sq[0][1] = 'NW';sq[1][1] = 'NE';
   sq[0][0] = 'SW';sq[1][0] = 'SE';
   return sq;
}

/* 
   NEW COORDINATE TRANSFORMATION SERVICE USES Proj4js CODE TO DO THE 
   TRANSFORMATIONS MEANING YOU CAN ALSO TRANSFORM INTO OTHER COORDINATE 
   SYSTEMS. NEED TO IMPLEMENT SOMETHING TO FIGURE OUT HOW TO SHOW GRID REFS 
*/
function ctransform(sproj,dproj,crd,dec) {
   var src = new Proj4js.Proj(sproj);
   var dst = new Proj4js.Proj(dproj);
   var fact = Math.pow(10,dec);
   Proj4js.transform(src, dst, crd);
   crd.x = parseInt(crd.x*fact)/fact;
   crd.y = parseInt(crd.y*fact)/fact;
   return crd;
}
function cdef(x,y) {
   var c = new Proj4js.Point(x+','+y);
   return c;   
}
function wgs84_to_osgb36(dd_coord_obj) {
   var c,gr_obj;
   c = cdef(dd_coord_obj.longitude,dd_coord_obj.latitude);
   c = ctransform('EPSG:4326','EPSG:27700',c,1);
   gr_obj = new GridReference (c.x, c.y);
   gr_obj.accuracy = dd_coord_obj.accuracy;
   return gr_obj;
}
function conv_uk_ings_to_ll(gr_obj) {
   var c,ddc;
   c = cdef(gr_obj.easting,gr_obj.northing);
   c = ctransform('EPSG:27700','EPSG:4326',c,6);
   ddc = new DegreesDecimalCoordinate (c.y, c.x);
   ddc.accuracy = gr_obj.accuracy;
   return (ddc);
}

//-->
;<!--
function refocus() {
   var requiresreload = readCookie('requiresreload');
   if (requiresreload) {
      window.location.reload();
      eraseCookie('requiresreload');
   }
   return true;
}
function removeElements(nodeids) {
   var parent;
   var child;
   for (var i=0;i<nodeids.length;i++) {
      while(child = document.getElementById(nodeids[i])) {
         parent = child.parentNode;
         parent.removeChild(child);
      }
   }
   return true;
}
function setreload() {
   createCookie('requiresreload',1,5);
}

/*
   For IE6 the cookie limitations are quite restrictive so you have to use the 
   userData method to persist larger volumes of data. 
   
   On non-IE browsers the try fails at the addBehavior step so there's not a 
   huge penalty to running this code.
   
   noud (no user data) can be set to true to force IE6 to use the 
   document.cookie rather than userData behavior storage. 
   
   noud is specified on all the authentication settings.    
*/
function createCookie(name,value,mins,noud) {
//alert('Saving cookie: '+name+'\n'+value);
   var dt,err,node;
   var expires;
   if (noud) {
      if (mins) {
   		dt = new Date();
   		dt.setTime(dt.getTime()+(mins*60000));
   		expires = "; expires="+dt.toGMTString();
   	}
   	else expires = "";
   	document.cookie = name+"="+value+expires+"; path=/";
   } else {
   	try {
         node = document.createElement('TEXTAREA');
         node.style.display = 'none';
         node.id = 's2ud_'+name;
         document.lastChild.appendChild(node);
         node.addBehavior("#default#userData");
         if (mins != -1) node.setAttribute(node.id,value);
         else node.removeAttribute(node.id);
         node.save(node.id);
         document.lastChild.removeChild(node);
      } catch (err) {
         if (node) document.lastChild.removeChild(node);
         if (mins) {
      		dt = new Date();
      		dt.setTime(dt.getTime()+(mins*60000));
      		expires = "; expires="+dt.toGMTString();
      	}
      	else expires = "";
      	document.cookie = name+"="+value+expires+"; path=/";
      }
   }
	return true;
}

function readCookie(name) {
	var cval,err,node;
	try {
      node = document.createElement('TEXTAREA');
      node.id = 's2ud_'+name;
      node.style.display = 'none';
      document.lastChild.appendChild(node);
      node.addBehavior("#default#userData");
      node.load(node.id);
      cval = node.getAttribute(node.id);
      document.lastChild.removeChild(node);
   } catch (err) {
      if (node) document.lastChild.removeChild(node);
   }
   // since some cookies are set by PHP they may not all use the userdata workaround
   if (!cval) {
   	var nameEQ = name + "=";
   	var ca = document.cookie.split(';');
   	for(var i=0;i < ca.length;i++) {
   		var c = ca[i];
   		while (c.charAt(0)==' ') c = c.substring(1,c.length);
   		if (c.indexOf(nameEQ) == 0) cval = c.substring(nameEQ.length,c.length);
   	}
	}
	return cval;
}

function eraseCookie(name) {
   try {
      node = document.createElement('TEXTAREA');
      node.id = 's2ud_'+name;
      node.style.display = 'none';
      document.lastChild.appendChild(node);
      node.addBehavior("#default#userData");
      node.load(node.id);
      node.removeAttribute(node.id);
      node.save(node.id);
      document.lastChild.removeChild(node);
   } catch (err) {
      if (node) document.lastChild.removeChild(node);
   	createCookie(name,0,-1);
   }
}
function getQueryVariable(variable,url) {
//alert("FUNCTION: getQueryVariable");
   var query,vars,pair;
   if (url) query = url.replace(/.+\?/,'');
   else query = window.location.search.substring(1);
   vars = query.split("&");
   for (var i=0;i<vars.length;i++) {
      pair = vars[i].split("=");
      if (pair[0] && pair[0] == variable) {
         return pair[1];
      }
   } 
   //alert('Query Variable ' + variable + ' not found');
   return false;
}
function getSessionId() {
	var nameEQ = "PHPSESSID=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) {
	      return c.substring(nameEQ.length,c.length);
      }
	}
	return null;
}

function carryQueryVariables() {
//alert("FUNCTION: carryQueryVariables");
   var query = window.location.search.substring(1);
   var vars = query.split("&");
   var pairs = new Array();
   for (var i=0;i< vars.length ; i++) {
      pairs = vars[i].split('=');
      if (pairs[0].indexOf('mapImage')== -1)
         document.getElementById(pairs[0]).value = unescape(pairs[1]);
   }
   return true;
}
function setBanner() {
    var banner_dir = './images/banners/';
    var banner_images = new Array('banner_khp_chunks.jpg',
                                  'banner_nettle_chunks.jpg',
                                  'banner_orchid_chunks.jpg',
                                  'banner_cloud_chunks.jpg',
                                  'banner_wave_chunks.jpg'/*,
                                  'banner_reeds_chunks.jpg'*/);
    var now = new Date();
    var rand = now % banner_images.length;
    var el_banner = document.getElementById('banner');
    var src_image = 'url(' + banner_dir + banner_images[rand] + ')';
    el_banner.style.backgroundImage = src_image;
    //alert (rand + " : " + src_image);
    return true;
}
function setCurrent() {
    node = document.getElementById('operation');
    value = node.value;
    document.getElementById(value).className = 'current';
}
function launchDialog(url, x, y) {
//alert ('FUNCTION: launchDialog');
    var popWin = null;
    var bits = url.split('?');
    var wname = bits[0];
    /*
      apparently internet explorer doesn't like . or / in the 
      window name so the next two lines take the page url and strip
      the ./ from the front and .php from the end.
      
      not sure whether it will work with directory/page urls.   
    */
    wname = wname.replace(/\//g, '_');
    wname = wname.replace(/\./, '');
    wname = wname.replace(/\.php/,'');
    if (!popWin || !popWin.open) {
        if (!x || x == '') x = 740;
        if (!y || y == '') y = 400;
        var attributes = "width=" + x + "px,height=" + y + "px,toolbar=0,status=0,scrollbars=1,resizable=1,title=0";
//alert(url);
        popWin = window.open(url, wname, attributes);
    }
    popWin.focus();
    return false;
}

function launchImageViewer (image,title) {
   var popWin = null;
   var url = './imageServer.php?image=' + image;
   attributes = "toolbar=0,status=0,scrollbars=1,resizable=1,title=1"
   popWin = window.open(url,title,attributes);
   popWin.focus();
   return false;
}
function launchPointDialog (url, id) {
    node = document.getElementById('select:coordinateSystem');
    tokens = id.split(':');
    pointNumber = tokens[tokens.length-1];
    index = node.selectedIndex;
    options = node.options;
    coords = (options[index]).value;
    url += '?coords=' + coords;
    url += '&point=' + pointNumber;
    launchDialog(url, id);
}
/* 
    this function should have a url and can then be used by anything to
    create child locations, organisations, individuals, classifications etc
    
    name change to launchDefinitionDialog(url, parentNamespace, id)
*/
function launchDefinitionDialog(page, namespace) {
    var url = page;
    var org_id = '';
    var checked = getCheckedList(namespace);
    for (i=0;i<checked.length;i++) {
        org_id += checked[i].id + ':';
    }
    // remove last : from string to avoid null last element
    org_id = org_id.substr(0,(org_id.length-1));
    url += '?org_id=' + org_id;
    launchDialog(url);
}
function showDiv(id) {
    var node = document.getElementById(id);
    node.style.visibility = 'visible';
    node.style.height = 'auto';
    node.style.width = 'auto';
    
}
function hideDiv(id) {
    var node = document.getElementById(id);
    node.style.visibility = 'hidden';
    node.style.height = '0px';
    node.style.width = '0px';
}
function addPoint(id) {
    parent = document.getElementById('boundaries');
    node = parent.lastChild;
    newNode = node.cloneNode(true);
    points = parent.childNodes.length;
    newNode.id = "point:" + points;
    newNode.lastChild.id = 'delete:' + points;
    parent.appendChild(newNode);
    document.getElementById(id).blur();
    return false;
}
function deletePoint(id) {
    tokens = id.split(':');
    pointNumber = tokens[1];
    pointId = 'point:' + pointNumber;
    parent = document.getElementById('boundaries');
    if (parent.childNodes.length > 1) {
        child = document.getElementById(pointId);
        parent.removeChild(child);
    } else {
        alert('Locations must have at least one defined boundary point. This can be as simple as a name with no coordinate reference.');
    }
    return false;
}
function definePoint() {
    validate_coords();
}
function updateElement(elementId,value) {
   document.getElementById(elementId).value = value;
   return true;
}
function shiftElement(div,margin) {
   document.getElementById(div).style.marginLeft = margin;
   return true;
}
//-->
;function bodyheight() {
   var D = document;
   return Math.max(
      Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
      Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
      Math.max(D.body.clientHeight, D.documentElement.clientHeight)
   );
}
function bodywidth() {
   var D = document;
   return Math.max(
      Math.max(D.body.scrollWidth, D.documentElement.scrollWidth),
      Math.max(D.body.offsetWidth, D.documentElement.offsetWidth),
      Math.max(D.body.clientWidth, D.documentElement.clientWidth)
   );
}
function getDocDimensions() {
   return new Array(bodywidth(),bodyheight());
} 
function placescreen() {
   var p,screen,e;
   try {
      p = getDocDimensions();
      screen = document.getElementById('screen');
      screen.style.width = p[0];
      screen.style.height = p[1];
   }catch (e) {}
   return true;
}
function setscreenres() {
   var e;
   try {
      var p = getDocDimensions();
      var page = document.getElementById('page');
      var d = document.getElementById('incontentdialog');
       
      if (p[0] <= 1100) {
         page.style.width = (p[0] - 40) + 'px';
         /*page.style.marginLeft = '8px';*/        
      } else {
         page.style.width = '1000px';
         /*page.style.marginLeft = '15%';*/        
      }
      var w = page.offsetWidth;
      d.style.width = (w-260)+'px';
   } catch (e) {}
   return true;
};/* 
   Move storage from cookies into JS object to avoid unnecessary things being 
   transmitted in new XMLHttpRequest model
*/
var s2storage = new Object();
function s2_getexpiry(mins) {
   var d = new Date();
   d.setMinutes ( d.getMinutes() + mins );
   return d.valueOf();
}
function s2_getstorageobject(value,expires) {
   var o,defexp;
   defexp = 360; // set default expiry to 6 hours
   if (!expires) expires = defexp;
   o = new Object();
   o.value = value;
   o.expiry = expires;
   o.expires = s2_getexpiry(expires);
   return o;
}
function s2_setstorage(name,value,expires) {
   if (name) s2storage[name] = s2_getstorageobject(value,expires);
   return true;   
}
function s2_getstorage(name) {
   var o,e,d,v;
   try {
      d = new Date();
      o = s2storage[name];
      // reset expiry each time you retrieve a value;
      if (o) {
         if (d.valueOf() <= o.expires) {
            v = o.value;
            o.expires = s2_getexpiry(o.expiry);
            s2storage[name] = o;
         } else {
            delete s2storage[name] 
            v = null;
         }
      } 
   } catch (e) { /* do I need to do something with this */ }
   return v;
}
function s2_delstorage(name) {
   delete s2storage[name];
}
function s2_emptystorage() {
   s2storage = new Object();
}
function s2_loadstorage() {
   var s2s,s2sc,s2e,s2ec,e,s2encrypt,prop;
   try {
      // prefer local sessionStorage object to cookies
      if (typeof sessionStorage == undefined) s2s = s2_getcookie('s2s');
      else s2s = sessionStorage.getItem('s2s');
      s2sc = cdm_decrypt(s2s,'AB');
      s2storage = JSON.parse(s2sc);
      s2_delcookie('s2s');
   } catch (e) { /* do I need to do something with this */ }
   try {
      s2e = s2_getcookie('s2e');
      s2ec = cdm_decrypt(s2e,'AB');
      s2encrypt = JSON.parse(s2ec);
      for(prop in s2encrypt) s2storage[prop] = s2_getstorageobject(s2encrypt[prop]); 
      s2_delcookie('s2e');         
   } catch(e) { /* do I need to do something with this */ }
}
function s2_savestorage() {
   var s2s,e,s2se;
   try {
      s2s = JSON.stringify(s2storage);
      s2se = cdm_encrypt(s2s,'AB');
      // prefer local sessionStorage object to cookies
      if (typeof sessionStorage == undefined) s2_setcookie('s2s',s2se,60);
      else sessionStorage.setItem('s2s', s2se);
   } catch(e) {
      alert('Unable to save settings');
   }
}

function s2_movedomain() {
   s2_delstorage('S2Interim');
   s2_savestorage();
   return true;
}

function s2_flip_encryption(buttonid) {            
   var txt,img,onoff,button;
   if (!buttonid) {
      onoff = (!s2_getstorage('encrypt'))?'encrypted':'open';
      button = document.getElementById('flip_encrypt');   
   } else {
      button = document.getElementById(buttonid);
      onoff = button.firstChild.alt;
   }
   switch (onoff) {
   case 'encrypted': {
      txt = 'open';
      img = new Image();
      img.src = '../images/padlock_open.gif'
      img.alt = txt;
      button.replaceChild(img,button.firstChild);
      // set encryption settings to last 2 weeks
      s2_setstorage('encrypt',0,20160);
   }break;
   case 'open': {
      txt = 'encrypted';
      img = new Image();
      img.src = '../images/padlock_closed.gif';
      img.alt = txt;
      button.replaceChild(img,button.firstChild); 
      cdm_clientinit();
   }break;     
   }
   return true;   
}
function s2_ihelpdiv() {
   div = document.createElement('DIV');
   div.className = 's2_iHelp';
   button = document.createElement('BUTTON');
   button.onclick = function() {s2_ihelpshowhide(this);};
   img = document.createElement('IMG');
   img.src = '../images/plus14.gif';
   img.alt = 'Show help';
   button.appendChild(img);
   div.appendChild(button);
   return div;   
}
function s2_ihelpshowhide(el) {
   var par,x,cx;
   par = el.parentNode;
   if (par.className == 's2_iHelp') {
      par.className = 's2_iHelpV';
      el.firstChild.src = '../images/minus14.gif';
      el.firstChild.alt = 'Hide help';
   } else {
      par.className = 's2_iHelp';
      el.firstChild.src = '../images/plus14.gif';
      el.firstChild.alt = 'Show help';
   }
}
// the start function establishes the session / encryption settings / sets up 
// the screen and performs the first query to the server to populate the 
// interface.
window.onresize = function() {s2_resize();};
function s2_resize() {
   placescreen();
   setscreenres();
}
function s2_getusername() {
/* this needs changing */
   var uinfo;
   uinfo = s2_getcookie('u_info');
   uinfo = s2_decrypt(uinfo,'AB');
   alert(uinfo);
}
function s2_start() {
   var parameters,encrypt,drawn,jp,qs,par,pc;
   s2_loadstorage();
   parameters = s2_drawsettings();      
   s2_resize();
   encrypt = parseInt(s2_getstorage('encrypt'));
   if (encrypt) syncauth();
   drawn = s2_setupdomaininterface(); // FIX POSTGIS VERSION
   jp = s2_getstorage('S2Parameters');
   qs = s2_getqsparams();
   if (jp) {
      parameters = jp;
      s2_delstorage('S2Parameters'); 
   } else if (qs) parameters = qs;
   if (!drawn) s2_query(parameters);     
   else s2_query({"Action":"CheckIn"});
   completed();  
}
function s2_getqsparams(param) {
   var query,vars,pair,params;
   query = window.location.search.substring(1);
   vars = query.split("&");
   params = new Object();   
   for (var i=0;i<vars.length;i++) {
      pair = vars[i].split("=");
      if (pair[0] && pair[1]) params[pair[0]] = pair[1];      
   }
   var rval = (param && params[param])?params[param]:(!param && i>=1)?params:null;
   return rval;
}
// the query function is used to request data from the server. 
// the query can add / delete / update or list data. data for an item 
// is returned with the data for linked items. so the data for the current 
// partnership comes back with the associated meetings and site list. 
function s2_getcurrentpath() {
   var path = window.location.pathname;
   path = path.replace(/^\/+/,'');
   path = path.replace(/\/$/,'');
   var paths = path.split(/\//);
   var path = paths.join('/');
   path = (path.match(/^\/+/))?path:'/'+path; 
   //var url = 'http://'+window.location.host + path;
   var url = window.location.protocol + '//' + window.location.host + path;
//alert(window.location.protocol);
   return url; 
}
function s2_getcurrentdomain() {
   var path = window.location.pathname.split(/\//);
   var dom;
   var s2path = s2_getcookie('S2Path');
   while (dom != s2path) dom = path.shift(); // FIX THIS FOR INSTALLATIONS OUTSIDE /s2/
   dom = path.shift();
   return dom;
}
function s2_query(parameters) {
   inprogress();
   var req = new Object();
   var encrypt = s2_getstorage('encrypt');
   var i,obj,settings,user;

   //if(encrypt) req.target = 'http://'+unescape(s2_getcookie('cdm_server'))+'/sites/S2_query.php'; 
   //else req.target = './S2_query.php';
   req.target = s2_getcurrentpath()+'/S2_query.php';
   settings = s2_getcurrent();
   if (parameters) {
      var s2interim = s2_getinterim(parameters.IType);
      if (s2interim && (parameters.Action == 'Add' || parameters.Action == 'View' || parameters.Action == 'MSelect')) {
         s2_updateinterim(parameters.IType);
      } 
   } else if (settings) {
      parameters = new Object();
      for (i in settings) {
         obj = settings[i];
         if (obj.IType && obj.Domain == s2_getcurrentdomain() && obj.Current && obj.Name != null) {
            parameters.IType = obj.IType;
            parameters.Current = obj.Current;
         }
      }
      if (parameters.IType && parameters.Current!='Multiple') parameters.Action = 'View';
   }
   qs = s2_getqsparams();
   if (qs) for(i in qs) parameters[i] = qs[i];
   user = s2_getcookie('u_info');
   if (user) parameters.UInfo = user; 
   parameters.CurrentSettings = settings;
   
   req.request = parameters;
   req.responder = s2_draw;
   snc_send(req);   
}
var forms = 0;
var s2_surpressderive = false;
// the draw function creates the interface based on the data returned from the 
// server and (if present) the interim cookie. 
// the interim cookie allows you to fill out some fields in a form and then go 
// and enter or select linked data and return to the form. 
function s2_draw(json) {
//snc_report_json_obj(json);
//alert(s2storage['S2Settings'] + '\n' + JSON.stringify(s2storage['S2Settings']));
   var i,jobj,sobj,prop,iprop,nprop,link,add,id,name,obj,err,s2iset;
   s2_clrpopulated();
   try {
      jobj = JSON.parse(json);
   } catch(err) {
      alert('Action Failed:\nServer Communication Error');
      completed();
      return null;   
   }
   if (jobj.UInfo) {
//alert(JSON.stringify(jobj.UInfo));
      s2_setstorage(jobj.UInfo.Name,jobj.UInfo.Cookie,jobj.UInfo.Expiry);
      alert(jobj.Message);
   } 
   if (jobj.UserPermissions) {
      s2_updateusermenu(jobj.UserPermissions);
      s2_enableextender(jobj.UserPermissions); // FIX POSTGIS VERSION
   } //else s2_updateusermenu();
   s2_enableordering();
   if (jobj.Status == 0) {
      alert('Action Failed:\n'+jobj.Message);                                       
      completed();
      //s2_query(); 
   } else if (jobj.NoDraw) {
      completed();
      if (jobj.Refresh) s2_refresh();
   } else {
      snc_clearMenus('incontentdialog');
      forms = 0;
      if (s2_drawinterim(jobj.IType)) {
         var s2interim = s2_getinterim(jobj.IType);
         for (prop in s2interim.Data) {
            var type = s2interim.Data[prop];
            if (type && typeof type == 'object' && type.IsInterim) {
               var inttype = type.IType;
               if (inttype == jobj.IType) { 
                  sobj = new Object;
                  sobj.IType = jobj.IType;
                  sobj.DisplayName = jobj.DisplayName;
                  if (jobj.Current == 'Multiple' || type.DataType == 'LINKEDTO') {
                     sobj.Name = jobj.ItemName;
                     sobj.Id = jobj.Current;
                     sobj.ListData = jobj.ListData;
                  } else {
                     obj = new Object();
                     obj.Name = jobj.ItemName;
                     obj.Id = jobj.Current;
                     jobj.ListData = new Array();
                     jobj.ListData.push(obj);
                  }
                  if (type.DataType == 'LINKEDTOM') {
                     for (lprop in jobj.ListData) {
                        add = true;
                        obj = new Object();
                        obj.Id = (jobj.ListData[lprop].Id)?jobj.ListData[lprop].Id:jobj.ListData[lprop][jobj.Key];
                        obj.Name = jobj.ListData[lprop]['Name'];
                        for (iprop in type.IData) {
                           if (obj.Id == type.IData[iprop].Id) add = false;
                        }
                        
                        if (add) {
                           if (!type.IData) type.IData = new Array();
                           type.IData.push(obj);
                        }
                     }
                     sobj.ListData = type.IData;
                  } 
                  sobj.Key = jobj.Key;
                  s2interim.Data[prop] = sobj;
                  s2interim.Interim = true;
                  //s2_updateinterim(jobj.IType);
                  s2_unsetinterim(jobj.IType); 
//alert(JSON.stringify(jobj));
               }
            }
         }
         jobj = s2interim;                  
      } //else alert('Draw Original Interface');
      hidedialog();
      completed();
      if (jobj.Form) {
         s2_drawalternatives(jobj);
         s2_drawform(jobj);
         s2_drawsettings();
      } else {
         s2iset = s2_getcurrent(jobj.IType);
         if (jobj.Current) {
            sobj = new Object();
            sobj.IType = jobj.IType;
            sobj.DisplayName = jobj.DisplayName; 
            sobj.Name = jobj.ItemName;
            sobj.Current = jobj.Current;
            sobj.Domain = jobj.Domain;
            s2_setcurrent(sobj);
            s2_drawsettings();
            if (jobj.ManagePermissions) s2_drawpermissionslink(jobj.ManagePermissions);
            s2_drawalternatives(jobj);
            if (jobj.Current != 'Multiple') s2_drawheader(sobj);
            if (jobj.Views) s2_drawviews(jobj);
            
            if (!jobj.ViewName) s2_drawaddform(jobj);
            else s2_drawhiddenparent(jobj);
            s2_setstorage('S2ViewName',jobj.ViewName);
         } else if (jobj.ListData || (s2iset && s2iset.SearchBy)) {  // add search by filter so you don't draw the add form for 0 search results. 
            s2_drawalternatives(jobj);
            s2_drawlisttable(jobj); 
         } else {
            s2_drawalternatives(jobj);
            s2_drawaddform(jobj);
         } 
         if (jobj.LinkedData && jobj.LinkedData.length > 0) {
            for (i in jobj.LinkedData) {
               link = jobj.LinkedData[i];
               if (link.Data) {
                  s2_drawheader(link);
                  s2_drawaddform(link);
               } else if (link.Type == 'Links') {
                  s2_drawheader(link);
                  s2_drawlinks(link);
               }            
               else s2_drawlisttable(link,jobj.IType);   
            }
         }
         if (jobj.External) {
            s2_getexternaldata(jobj);
         }
         if (jobj.ProcessStatus && jobj.Current) {
            if ((jobj.ProcessStatus.Process.Percentage_Complete < 1) && !jobj.ViewName) s2_postprocess(jobj);
            s2_postprocesslink(jobj);
         } 
         //table_setupmanager();
         s2_intenablereturnbutton();         
      }  
   }
//snc_report_json_obj(json);
//snc_report_json_obj(JSON.stringify(s2storage));
}
function s2_postprocesslink(jobj) {
   var e,sid,node,a,btn,input;
   try {
      sid = 'Setting_IType-'+jobj.IType;
      node = document.getElementById(sid);
      btn = document.createElement('BUTTON');
      btn.name = 'Continue '+jobj.DisplayName.toLowerCase()+ ' processing';
      //btn.appendChild(document.createTextNode('\u03A6'));
      btn.appendChild(document.createTextNode('~'));
      btn.id = 'Setting_IType-'+jobj.IType+'_Action-Process_Current-'+jobj.Current;
      //a.href = '#';
      btn.onclick = function() {
         s2_postprocess(jobj);
         return false;
      };
      s2_addtooltip(btn);
      node.appendChild(btn);
   } catch (e) {}  
}
function s2_updateusermenu(perms) {
   var domain, label;
   var div, node, a, text, url, insert, ibutton;
   div = document.getElementById('s2menubar');
   node = div.lastChild;
   
   while (node.id != 's2menuheadings') {
      if (node.id == 's2_interfacebutton') ibutton = node;
      div.removeChild(div.lastChild);
      node = div.lastChild;
   }
   var u = s2_getcookie('u_info');
   if (u && perms && perms.UserName) {
      if (perms.users.list_users) {
         a = document.createElement('A');
         a.href = '../users' + window.location.search;//?a=List&i=USR_Users';
         a.onclick = function() {
            s2_setstorage('S2Parameters',{"Action":"List","IType":"USR_Users"});
         };
         a.className = 'buttonLink';
         a.appendChild(document.createTextNode('[ List Users ]'));
         div.appendChild(a);                    
      }
      if (perms.users.edit_self) {
         a = document.createElement('A');
         a.href = '../users' + window.location.search;//?a=View&i=USR_Users';
         a.onclick = function() {
            s2_setstorage('S2Parameters',{"Action":"View","IType":"USR_Users"});
         };
         a.className = 'buttonLink';
         a.appendChild(document.createTextNode('[ My Settings ]'));
         div.appendChild(a);                 
      }
      a = document.createElement('A');
      a.href = '../users';//?a=Logout&i=USR_Users';
      a.className = 'buttonLinkRight';
      a.onclick = function() {
         s2_delcookie('u_info');
         s2_setstorage('S2Parameters',{"Action":"Logout","IType":"USR_Users"});
         s2_savestorage();
         s2_unsetcurrent();
         return true;
      };
      a.appendChild(document.createTextNode('[ Logout ]'));
      div.appendChild(a);
      a = document.createElement('A');
      a.href = '../users';//?a=View&i=USR_Users';
      a.onclick = function() {
         s2_setstorage('S2Parameters',{"Action":"View","IType":"USR_Users"});
      };
      a.className = 'buttonLinkRight';
      a.appendChild(document.createTextNode('[ Login: '+ perms.UserName +' ]'));
      div.appendChild(a);
   } else {
      a = document.createElement('A');
      a.href = '../users';//?a=Register&i=USR_Users';
      a.onclick = function() {
         s2_setstorage('S2Parameters','{"Action":"GetForm","IType":"USR_Users","Form":"Register"}');
      };
      a.className = 'buttonLinkRight';
      a.appendChild(document.createTextNode('[ Register ]'));
      div.appendChild(a);  
      a = document.createElement('A');
      a.href = '../users';//?a=GetForm&i=USR_Users';
      a.onclick = function() {
         s2_setstorage('S2Parameters','{"Action":"GetForm","IType":"USR_Users"}');
      };
      a.className = 'buttonLinkRight';
      a.appendChild(document.createTextNode('[ Login ]'));
      div.appendChild(a);
      div.appendChild(document.createTextNode('...'));
   }
   if (ibutton) div.appendChild(ibutton);
   node = document.createElement('HR');
   node.style.clear = 'both';
   div.appendChild(node);    
}
function s2_drawpermissionslink(lobj) {
   var prop,value,div,node,li,h4,a,json,domain,denc;
   div = document.getElementById('left-menus');
   node = div.firstChild;
   var path = s2_getcurrentpath();
   path = path.replace(/\/[^\/]+$/,'/'+lobj.ToDomain);
   domain = new Object();
   for (prop in lobj.Parameters) {
      domain[prop] = lobj.Parameters[prop];
   }
   denc = getencrypted(JSON.stringify(domain),'AB');
   path += '?d='+denc;  
   while(node && node.nodeName != 'UL') node = node.nextSibling;
   if (node) {
      json = JSON.stringify(lobj);
      li = document.createElement('LI');
      h4 = document.createElement('H4');
      h4.appendChild(document.createTextNode('Manage Permissions'));                                                   
      li.appendChild(h4);
      a = document.createElement('A');
      a.href = path;
      a.className = 'ListEntry';
      a.appendChild(document.createTextNode(lobj.Parameters.alias));
      a.name = 'Manage user permissions';
      s2_addtooltip(a);
      li.appendChild(a);
      node.appendChild(li);
   }      
}
function s2_itypeselect(id) {
   var args = s2_disectid(id);
      //s2_removecrumbs(id); WHAT WAS THIS HERE FOR?
   s2_query(args);
   return true;
}
function s2_removecrumbs(id) {
   var args = s2_disectid(id);
   var settings = s2_getcurrent();
   var set,cur = false;
   var dom = s2_getcurrentdomain();
   //alert(JSON.stringify(settings));
   for(prop in settings) {
      set = settings[prop];
      if (args.IType == set.IType) cur = true;
      if (prop && cur && dom == set.Domain) s2_unsetcurrent(prop);
   }   
}
function s2_getcrumbs() {
   var dom = s2_getcurrentdomain();
   var settings = s2_getcurrent();
   //alert(JSON.stringify(settings));
   var crumbs = new Array();
   for(prop in settings) {
      set = settings[prop];
      if (dom == set.Domain) crumbs.push(set);
   }
   // the last crumb should be the current item
   crumbs.pop();
   return crumbs;   
}
function s2_itypeselectall(id) {
   var args,table,tbody,rownum,tr,td,tick,val,current,targs;
   args = s2_disectid(id);
   table = document.getElementById('Form-'+args.Form+'_IType-'+args.IType+'_Table');
   tbody = table.tBodies[0];
   args.Current = new Array();
   for (rownum=0;rownum<tbody.childNodes.length;rownum++) {
      tr = tbody.childNodes[rownum];
      td = tr.lastChild;
      tick = td.firstChild;
      while (tick && tick.className != 'tick') tick = tick.nextSibling;
      if (tick.checked) {
         targs = s2_disectid(tick.id);
         args.Current.push(targs.Current);
      }
   }
   if (args.Current.length == 1) {
      args.Action = 'View';
      args.Current = args.Current[0];
      s2_query(args);      
   } else if (args.Current.length > 0) {
      args.Action = 'MSelect';
      s2_query(args);      
   } else {
      alert('Nothing selected.\nNo boxes have been ticked.');
   }   
   return true;      
}
// form fields are given and id with key value pairs separated by underscores (_)
// with the key and value separated by a hyphens (-)
// forms are numbered sequencially on the page using the global forms variable
// eg Form-1_IType-Site_Action-Edit_Current-2
// the equivalent get method query string would be
// ?Form=1&IType=Site&Action=Edit&Current=2
// the get syntax has not been used as it contains characters which don't meet 
// the standard for HTML element ids   
function s2_disectid(id) {
   var args,keyvals,key,val; 
   args = new Object();
   if (id) {
      var pairs = id.split('_');
      var i,keyvals;
      for(i=0;i<pairs.length;i++) {
         if (!pairs[i].match('-')) {
            pairs[i] = pairs[i-1] + '_' + pairs[i];
            pairs = (pairs.slice(0,i-1)).concat(pairs.slice(i));
            i--;
         }
      }
      for(i in pairs) {
         keyvals = pairs[i].split('-');
         key = keyvals.shift();
         val = keyvals.join('-');
         args[key] = val;   
      }
   }
   return args; 
}
function s2_buildid(args) {
   var arg,id,col,val,pairs;
   pairs = new Array();
   for(col in args) {
      val = args[col];
      pairs.push(col+'-'+val); 
   }
   id = pairs.join('_');
   return id;
}


// the settings consist of objects with an IType, Name and Id. 
// these are stored as json in a settings cookie and are drawn to the left menu 
// giving the user clickable links to all the currently selected table entries.
function s2_removesetting(id) {
   var args = s2_disectid(id);
   s2_unsetcurrent(args.IType);
   s2_drawsettings();
   s2_hidetooltip();
   //args.Action = 'List';
   //s2_query(args);
   s2_query();
}

function s2_drawsettings() {
   var div,i,obj,cdom,cobj;
   //var table,thead,tbody,tr,th,td,node,a;
   var ul,li,a,button,h2,h3,h4,text,span,ipt;
   //forms++;
   snc_clearMenus('left-menus');
   div = document.getElementById('left-menus');
   h2 = document.createElement('H2');
   h2.appendChild(document.createTextNode('Settings'));
   div.appendChild(h2);
   ul = document.createElement('UL');
   var settings = s2_getcurrent();
   for (i in settings) {
      obj = settings[i];
      cdom = s2_getcurrentdomain();
      if (obj.Name && obj.Current) {
         if (!obj.Domain || obj.Domain == cdom) {
            li = document.createElement('LI');
            li.id = 'Setting_IType-'+obj.IType;
            h4 = document.createElement('H4');
            h4.appendChild(document.createTextNode(obj.DisplayName));  
            li.appendChild(h4);
            
            text = obj.Name;//.replace(/@/,' @ ');
            if (text.match(/@/)) {
               a = document.createElement('A');
               a.className = 'ListEntry';
               a.href = 'mailto:'+text;
               a.appendChild(document.createTextNode(text));
               li.appendChild(a);
               li.appendChild(document.createTextNode(' '));
            } else if (text.match(/http:\/+/)) {
               a = document.createElement('A');
               a.className = 'ListEntry';
               a.href = text;
               a.appendChild(document.createTextNode(text));
               li.appendChild(a);
               li.appendChild(document.createTextNode(' '));
            } else {
               span = document.createElement('SPAN');
               span.appendChild(document.createTextNode(text));
               li.appendChild(span);
            }
            li.appendChild(document.createElement('BR'));
            
            button = document.createElement('BUTTON');
            //button.appendChild(document.createTextNode('\xD7'));
            button.appendChild($('<span></span>').addClass('icon-cross')[0]);
            button.name = 'Remove '+obj.DisplayName.toLowerCase()+' setting';
            button.id = 'Form-'+forms+'_IType-'+obj.IType+'_Action-Unset_Current-'+obj.Current;
            button.onclick = function() {s2_removesetting(this.id);};
            s2_addtooltip(button);
            li.appendChild(button);
            
            button = document.createElement('BUTTON');
            button.name = 'View current '+obj.DisplayName.toLowerCase();
            //button.appendChild(document.createTextNode('\u2192'));
            button.appendChild($('<span></span>').addClass('icon-arrow-right')[0]);
            button.id = 'Form-'+forms+'_IType-'+obj.IType+'_Action-View_Current-'+obj.Current;
            button.onclick = function() {s2_itypeselect(this.id);return false;};
            s2_addtooltip(button);
            li.appendChild(button);
            /*
            if (jobj.ProcessStatus && jobj.Current && !jobj.ViewName) {
               button = document.createElement('BUTTON');
               button.name = 'View current '+obj.DisplayName.toLowerCase();
               button.appendChild(document.createTextNode('\u221E'));
               button.id = 'Form-'+forms+'_IType-'+obj.IType+'_Action-Process_Current-'+obj.Current;
               button.onclick = function() {
                  var jobj,json,args;
                  args = s2_disectid(this.id);
                  json = document.getElementById('Form-'+args.Form+'_IType-'+args.IType+'_Definition-Process');
                  jobj = JSON.parse(jobj);
                  s2_postprocess(jobj);
                  return false;
               };
               s2_addtooltip(button);
               li.appendChild(button);
               ipt = document.createElement('INPUT');
               ipt.type = 'hidden';
               ipt.value = JSON.stringify(jobj);
               ipt.id = 'Form-'+forms+'_IType-'+obj.IType+'_Definition-Process';
               li.appendChild(ipt);  
            }
            */
            ul.appendChild(li);
            cobj = obj;
         }
      }
   }
   div.appendChild(ul);
   var d = s2_getqsparams('d');
   if (d) {
      d = cdm_decrypt(d,'AB');
      d = JSON.parse(d);
      h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode('Permissions For'));
      div.appendChild(h2);
      ul = document.createElement('UL');
      li = document.createElement('LI');
      h4 = document.createElement('H4');
      h4.appendChild(document.createTextNode(d.alias));  
      button = document.createElement('BUTTON');
      //button.innerHTML = '&times;';
      button.appendChild(document.createTextNode('\xD7'));
      button.name = 'Remove effective domain setting';
      button.onclick = function() {s2_removeqs(this);};
      s2_addtooltip(button);
      h4.appendChild(button);
      li.appendChild(h4);
      ul.appendChild(li);
      div.appendChild(ul);
   }   
   if (cobj) cobj.Action = 'View';
   return cobj; 
}
function s2_removeqs(el) {
   window.location.search = null;
   // ul>li>h4>button
   var ul = el.parentNode.parentNode.parentNode;
   var h2 = ul.previousSibling;
   var div = ul.parentNode;
   div.removeChild(ul);
   div.removeChild(h2);   
}

function s2_addtooltip(el) {
   el.onmouseover = function(e) {s2_showtooltip(this,e);};
   el.onmouseout = function(e)  {s2_hidetooltip();};
}
function s2_showtooltip(el,e) {
   var x,y,div,node;
   var evt=window.event || e;
   x = (Math.floor(evt.clientX/10)-1) * 10;
   x = (x<10)?10:x;
   x += document.body.scrollLeft;
   y = (Math.floor(evt.clientY/10)+2) * 10;
   y = (y<10)?10:y;
   y += document.body.scrollTop;   
   div = document.createElement('DIV');
   div.style.position = 'absolute';
   div.style.left = x + 'px';
   div.style.top = y + 'px';
   div.className = 's2tooltip';
   div.id = 's2tooltip';
   div.appendChild(document.createTextNode(el.name)); 
   div.onmouseout = function(e) {s2_hidetooltip();};      
   document.getElementById('main').appendChild(div);
   return true;  
}
function s2_hidetooltip() {
   // only hide tooltip if there is one. 
   try {
      var node = document.getElementById('s2tooltip'); 
      if (node) node.parentNode.removeChild(node);
   } catch (e) {}
   return false;
}

// cookie alias functions
String.prototype.chunk = function(n) {
   if (typeof n=='undefined') n=2;
   return this.match(RegExp('.{1,'+n+'}','g'));
};

var csize = 2048;
function s2_setcookiechunks(name,value,expires) {
   var x,chunks,cname,rem,last;
   chunks = value.chunk(csize);
   for(x=0;x<chunks.length;x++) {
      cname = name + '_ck_' + (x+1);
      createCookie(cname,chunks[x],expires);
   }
   return chunks.length;    
}
function s2_getcookiechunks(name,chunks) {
   var ckx = parseInt(chunks.replace(/ck/,''));
   var val,cval,cname,x;
   for(x=1;x<=ckx;x++) {
      cname = name + '_ck_' + x;
      cval = readCookie(cname);
      if (val) val = val + cval;
      else val = cval;
   }
   return val;  
}
function s2_delcookiechunks(name,chunks) {
   var ckx = parseInt(chunks.replace(/ck/,''));
   var val,cval,cname,x;
   for(x=1;x<=ckx;x++) {
      cname = name + '_ck_' + x;
      cval = eraseCookie(cname);
      if (val) val = val + cval;
      else val = cval;
   }
   return val;  
}
function s2_setcookie(name,value,expires) {
   var ck,escval;
   escval = escape(value);
   if (escval.length > csize) {
      ck = s2_setcookiechunks(name,escval,expires);
      createCookie(name,'ck'+ck,expires);
   } else {
      createCookie(name,escval,expires);
   }    
   return true;
}
function s2_getcookie(name) {
   var c,chunks;
   c = readCookie(name);
   if (c && c.match(/ck\d+/)) c = s2_getcookiechunks(name,c); 
   if (c) c = unescape(c);
   return c;
}
function s2_delcookie(name) {
   var c; 
   c = readCookie(name);
   if (c && c.match(/ck\d+/)) c = s2_delcookiechunks(name,c);
   eraseCookie(name);
   return true;
}

function s2_inarray(needle, haystack) {
   var i,length;
   length = haystack.length;
   for (i = 0;i < length;i++) {
      if(haystack[i] == needle) return true;
   }
   return false;                     
}


// settings are stored in a cookie called S2Settings as json with the 
// itype, name field and id 
// for tables which only contain link data, the getname method is overridden on 
// the server end to calculate a name field for the entry. 
function s2_getcurrent(itype) {
   var s2settings = s2_getstorage('S2Settings');
   if (s2settings) {
      if (itype) return s2settings[itype];
      else return s2settings;
   } else return new Object(); 
//alert(s2storage['S2Settings']);
}
function s2_setcurrent(itypeobj) {
   var s2settings = s2_getcurrent();
   if (itypeobj.DisplayName && itypeobj.Current) {
      /* var obj = new Object();
      obj.IType = itypeobj.IType;
      obj.DisplayName = itypeobj.DisplayName;
      obj.Current = itypeobj.Current;
      obj.Name = itypeobj.Name;
      obj.Domain = itypeobj.Domain; */
      s2settings[itypeobj.IType] = itypeobj;
      s2_setstorage('S2Settings',s2settings);
//alert(s2storage['S2Settings']);
   }
   return true;   
}
function s2_unsetcurrent(itype) {
   if (itype) {
      var s2settings = s2_getcurrent();
      var prop,set = false;
      delete s2settings[itype];
      s2_setstorage('S2Settings',s2settings);
   } else s2_delstorage('S2Settings');
//alert(s2storage['S2Settings']);
   return true;   
}

function s2_setpage(itype,page) {
   var itypeobj,cdom;
   var s2settings = s2_getcurrent();
   cdom = s2_getcurrentdomain();
   if (s2settings[itype]) {
      itypeobj = s2settings[itype];
      itypeobj.Page = page;
   } else {
      itypeobj = new Object();
      itypeobj.Domain = cdom;
      itypeobj.IType = itype;
      itypeobj.Page = page;
   }
   s2settings[itype] = itypeobj;
   s2_setstorage('S2Settings',s2settings);
   s2_refresh();   
}   
function s2_refresh() {
   var vtype,vcurr,vview,vactn,args;
   var s2settings = s2_getcurrent();   
   vactn = (document.getElementById('Form-1_Action'))?document.getElementById('Form-1_Action').value:null;
   vtype = (document.getElementById('Form-1_ViewIType'))?document.getElementById('Form-1_ViewIType').value:null;
   vview = (document.getElementById('Form-1_ViewName'))?document.getElementById('Form-1_ViewName').value:'';
   vcurr = (s2settings[vtype] && s2settings[vtype]['Current'])?s2settings[vtype]['Current']:null;
   if (!vtype) s2_query();
   else {
      args = new Object();
      args.IType = vtype;
      if (vcurr) { 
         args.Current = vcurr;
         args.ViewName = vview;
      }
      switch(vactn) {
      case 'Edit':   args.Action = 'View';break;
      default:       args.Action = 'List';break; 
      }
      s2_query(args);
   }
   return true;
}
function s2_searchby(id) {
   var args,sby,sfor,itype,itypeobj,cdom;
   args = s2_disectid(id);
   id = id.replace(/\-Exec/,'');
   sby = document.getElementById(id+'-By').value;
   sfor = document.getElementById(id+'-For').value;
   cdom = s2_getcurrentdomain();
   var s2settings = s2_getcurrent();
   if (s2settings[args.IType]) {
      itypeobj = s2settings[args.IType];
   } else {
      itypeobj = new Object();
      itypeobj.Domain = cdom;
      itypeobj.IType = args.IType;
   }
   itypeobj.SearchBy = sby;
   itypeobj.SearchFor = sfor;   
   s2settings[args.IType] = itypeobj;
   s2_setstorage('S2Settings',s2settings);
   s2_refresh();
   return true;
}


// when add forms contain linked data these have to be set / added and then 
// the user gets returned to the form they were filling out.

// when the users clicks set on a linked data element in a form the current form
// data is saved to an "interim" cookie. when the server returns from adding or 
// setting the link data the form is re-populated with the contents from the
// cookie and the cookie is deleted.

// each time a server query returns, or an existing element is selected, the 
// draw function checks for an interim cookie and draws that interface if there 
// is one or the returned interface if there isn't 

function s2_drawinterim(target) {
   var s2interim = s2_getinterim(target);
   var drawinterim;
   drawinterim = (s2interim)?(s2interim.Status == 'Redraw'):false;
   //s2_updateinterim(target);
   return drawinterim;
}

function s2_saveinterim(id)  {
   var s2interim = s2_getinterim();
   var obj,node,id,name,cid,coldata;
   var args = s2_disectid(id);
   var target;
   var iface = JSON.parse(document.getElementById('Form-'+args.Form+'_Interface').value);
   var interfacetype,parameters,obj,id,name,iobj;
   parameters = new Object();
   for(prop in iface.Interface) {
      if (prop == args.Property) {
         if (typeof iface.Interface[prop] == 'object') itype = iface.Interface[prop].TargetType; 
         else itype = iface.Interface[prop].replace(/LINKEDTO[M]*:([^\.]+)\..+/,"$1");
         //parameters[prop] = '#INTERIM#'+itype;
         iface.Target = (itype)?itype:args.IType;
         id = 'Form-'+args.Form+'_IType-'+iface.IType+'_Property-'+prop;
         obj = new Object();
         obj.IsInterim = true;
         obj.IType = itype;
         obj.DataType = iface.Interface[prop].DataType;
         iobj = s2_getnode(id,prop,iface.Interface[prop],null,'Interim');
         if (iobj && iobj.ListData) obj.IData = iobj.ListData; 
         parameters[prop] = obj;
      } else {
         if (typeof iface.Interface[prop] == 'object') interfacetype = iface.Interface[prop].DataType;
         else interfacetype = iface.Interface[prop].replace(/([^\:]+).*/,'$1');
         switch (interfacetype) {
         case 'Unique Key': if(iface.Data) cid = iface.Data[prop]; break;
         default: {
            id = 'Form-'+args.Form+'_IType-'+iface.IType+'_Property-'+prop; 
            parameters[prop] = s2_getnode(id,prop,iface.Interface[prop],null,'Interim');
         }break;            
         }   
      }
   }
   if (cid) iface.Current = cid;
   iface.Data = parameters;
   iface.Status = 'Setting';
   delete iface.UserPermissions;
   var s2inum;
   s2inum = s2_deleteexpiredinterimdefs(); 
   s2interim[iface.Target] = s2inum;
   s2_saveinterim_x(iface,s2inum);
//alert(s2inum + '\n' + JSON.stringify(iface));
   s2_setstorage('S2Interim',s2interim);
   s2_savestorage();
//alert(JSON.stringify(s2interim));
   return true;   
}

function s2_saveinterim_x(iface,number) {
   var cname = 'S2Int'+number;
   s2_setstorage(cname,iface);
   return true;   
}
function s2_deleteexpiredinterimdefs() {
   var s2interim = s2_getinterim();
   var type,s2int,s2inum,s2imax=0;
   if (s2interim) {
      for (type in s2interim) {
         s2inum = s2interim[type];
         s2int = s2_getinterim(type);
         if (s2int && s2int.Status == 'Delete') {
            s2_delstorage('S2Int'+s2inum);
            delete s2interim[type];
            s2imax = Math.max(s2imax,s2inum);
         }
      }
   }
   return (s2imax+1);
}

function s2_updateinterim(target)  {
   var s2interim = s2_getinterim();
   var s2int;
   var drawinterim = false;
   if (s2interim && s2interim[target]) {
      var s2inum;
      s2inum = s2interim[target];
      s2int = s2_getinterim(target);
      if (s2int.Status) {
         switch (s2int.Status) { 
         case 'Setting': {
            s2int.Status = 'Redraw';
            drawinterim = false;
            s2_setstorage('S2Int'+s2inum,s2int);
         }break;
         case 'Redraw': {
            s2int.Status = 'Delete';
            drawinterim = true;
            s2_setstorage('S2Int'+s2inum,s2int);
         }break;
         case 'Delete': {
            s2_unsetinterim(target);
            drawinterim = false;         
         }break
         }
      }      
   }
   return drawinterim;
}
function s2_unsetinterim(target) {
   if (!target) s2_delstorage('S2Interim');
   else {
      var s2interim = s2_getinterim();
      var s2inum = s2interim[target];
      if (s2inum) { 
         delete s2interim[target];
         s2_delstorage('S2Int'+s2inum);
      }
      s2_setstorage('S2Interim',s2interim);
   }
   return true;   
}
function s2_getinterim(target) {
   var s2interim = s2_getstorage('S2Interim');
   var s2return,type,s2int,s2inum;
   if (s2interim) {
      if (target) {
         s2inum = s2interim[target];
         if (s2inum) {
            s2int = s2_getstorage('S2Int'+s2inum);
         }
         s2int = (s2int)?s2int:new Object();
      } else s2int = s2interim;
      return s2int;
   } else return new Object(); 
}


function s2_showaddinterface(id) {
   var args = s2_disectid(id);
   var id = 'Form-'+args.Form+'_Interface';
   var ifaceobj = JSON.parse(document.getElementById(id).value);
   //s2_drawaddform(ifaceobj);
   if (ifaceobj.ListData) delete ifaceobj.ListData;
   var iface = JSON.stringify(ifaceobj);
   s2_draw(iface);
   //snc_report_json_obj(iface);
}
function s2_passwordmatches(cid) {
   var bid,pid,cval,pval,encval,matches;
   bid = cid.replace(/\_Confirm.+$/,'');
   pid = bid+'_Confirm-1';
   cval = document.getElementById(cid).value;
   pval = document.getElementById(pid).value;
   matches = (pval == cval);
   if(!matches) alert('The passwords do not match');
   else s2_passwordencrypt(pval,pid);  
   return matches;   
}
function s2_passwordencrypt(val,pid) {
   var bid = pid.replace(/\_Confirm.+$/,'');
   encval = getencrypted(val,'AB');
   document.getElementById(bid).value = encval;
}
var s2ystart;
function s2_stretch() {}
var mapname,mappoly; 
var datetarget;
function s2_getdatetoday() {
   var date = new Date();
   var currentday,currentmonth,currentyear;
   currentday     = date.getUTCDate();
   currentmonth   = date.getUTCMonth();
   currentyear    = date.getUTCFullYear();
   var datetext = ((currentday<10)?'0'+currentday:currentday) + '/' + ((currentmonth<9)?'0'+(currentmonth+1):(currentmonth+1)) + '/' + currentyear;
   return datetext;   
}
function s2_today(target) {
   datetarget = target;
   var date = new Date();
   var currentday,currentmonth,currentyear;
   currentday     = date.getUTCDate();
   currentmonth   = date.getUTCMonth();
   currentyear    = date.getUTCFullYear();
   var datetext = ((currentday<10)?'0'+currentday:currentday) + '/' + ((currentmonth<9)?'0'+(currentmonth+1):(currentmonth+1)) + '/' + currentyear;   
   var dnode = document.getElementById(datetarget);  
   dnode.focus();
   dnode.value = datetext;
   dnode.blur();
   if (dnode.onchange) dnode.onchange();
}

function useDate() {
   var datetext = ((currentday<10)?'0'+currentday:currentday) + '/' + ((currentmonth<9)?'0'+(currentmonth+1):(currentmonth+1)) + '/' + currentyear;
   var dnode = document.getElementById(datetarget);  
   dnode.focus();
   dnode.value = datetext;
   dnode.blur();
   if (dnode.onchange) dnode.onchange();
   hidedialog(); 
}
function s2_showcalendardialog(target) {
   var dialog,cal,cont,cent;
   datetarget = target;
   cleardialog('dialog-liner');
   dialog = document.getElementById('dialog-liner');
   cent = document.createElement('CENTER');
   cal = document.createElement('DIV');
   cal.id = 'calendar';
   cal.className = 'calendar';
   cent.appendChild(cal);
   dialog.appendChild(cent);
   loadCalendar();
   //var close = document.createElement('BUTTON');
   //close.appendChild(document.createTextNode('close'));
   //close.onclick = hidedialog;
   //dialog.appendChild(close);
   showdialog();
}
function s2_drawheader(jobj) {
   var dialog,h2,text,button,input;
   dialog = document.getElementById('incontentdialog');
   h2 = document.createElement('H2');
   text = jobj.DisplayName;
   if (jobj.Name) text += ' : ' + jobj.Name 
   h2.appendChild(document.createTextNode(text));
   dialog.appendChild(h2);
   return true;
}
function s2_searchbylinks(jobj) {
   var node,a,id,span,prop,interfacetype,input,select,option,button,maxl;
   var parenttype;
   maxl = 20;
   var s2settings = s2_getcurrent(jobj.IType);
   node = document.createElement('DIV');
   node.className = 'searchbydiv';
   if (s2settings && s2settings.SearchBy) {
      span = document.createElement('SPAN');
      span.appendChild(document.createTextNode('Filter: '));
      node.appendChild(span);
      span = document.createElement('SPAN');
      span.appendChild(document.createTextNode(s2settings.SearchBy));
      span.style.marginLeft = '5px';
      span.style.marginRight = '5px';
      span.style.fontWeight = 'bold';
      node.appendChild(span);
      span = document.createElement('SPAN');
      span.appendChild(document.createTextNode(' = '));
      span.style.marginLeft = '5px';
      span.style.marginRight = '5px';
      node.appendChild(span);
      span = document.createElement('SPAN');
      prop = s2settings.SearchFor;
      prop = prop.replace(/\s*\,\s*/,' , ');
      if (prop.length > maxl) prop = prop.substring(0,(maxl-2))+'..'; 
      span.appendChild(document.createTextNode(prop));
      span.style.fontWeight = 'bold';
      node.appendChild(span);
      button = document.createElement('BUTTON');
      //button.innerHTML = '&times;';
      button.appendChild(document.createTextNode('\xD7'));
      button.className = 'lmtype';
      button.onclick = function() {s2_removefilter(jobj.IType);};
      node.appendChild(button);            
   } else {
      span = document.createElement('SPAN');
      span.appendChild(document.createTextNode('Search For'));
      span.style.marginRight = '5px';
      node.appendChild(span);
      input = document.createElement('INPUT');
      input.type = 'text';
      input.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Search-For'; 
      node.appendChild(input);
      
      select = document.createElement('SELECT');
      select.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Search-By';
      for(prop in jobj.Interface) {
         if (typeof jobj.Interface[prop] == 'object') interfacetype = jobj.Interface[prop].DataType;
         else interfacetype = jobj.Interface[prop].replace(/([^\:]+).*/,'$1');
         switch (interfacetype) {
         case 'Unique Key': break;
         //case 'LINKEDTO': break;
         //case 'LINKEDTOM': break;
         default: {
            text = prop.replace(/^[A-Z]+_/,'');
            text = text.replace(/_ID$/,'');
            text = text.replace(/_/,' ');
            option = document.createElement('OPTION');
            option.appendChild(document.createTextNode(text));
            option.value = prop;
            select.appendChild(option);
         }break;
         }
      }
      span = document.createElement('SPAN');
      span.appendChild(document.createTextNode('In'));
      span.style.marginLeft = '5px';
      span.style.marginRight = '5px';
      node.appendChild(span);
      node.appendChild(select);
      
      a = document.createElement('A');
      a.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Search-Exec';
      a.href = window.location.search + '#';
      a.appendChild(document.createTextNode('Go'));
      a.onclick = function() {s2_searchby(this.id);};
      a.className = 'listnav';
      node.appendChild(a);
   }
   return node;
}
function s2_pagelinks(jobj) {
   var node,a,id,span;
   node = document.createElement('DIV');
   node.className = 'pagelinksdiv';
   if (jobj.Page == 1) {
      span = document.createElement('SPAN');
      span.appendChild(document.createTextNode('\xAB'));
      span.style.color = '#999';
      span.className = 'listnav';
      node.appendChild(span);
   
      span = document.createElement('SPAN');
      span.appendChild(document.createTextNode('<'));
      span.style.color = '#999';
      span.className = 'listnav';
      node.appendChild(span);
   } else { 
      a = document.createElement('A');
      a.href = window.location.search + '#';
      a.appendChild(document.createTextNode('\xAB'));
      a.onclick = function() {s2_setpage(jobj.IType,(1));};
      a.className = 'listnav';
      node.appendChild(a);
   
      a = document.createElement('A');
      a.href = window.location.search + '#';
      a.appendChild(document.createTextNode('<'));
      a.onclick = function() {s2_setpage(jobj.IType,(jobj.Page-1));};
      a.className = 'listnav';
      node.appendChild(a);
   }
   span = document.createElement('SPAN');
   span.appendChild(document.createTextNode('Page '+jobj.Page+' of '+jobj.Pages));
   span.style.marginLeft = '5px';
   span.style.marginRight = '5px';
   node.appendChild(span);
   if (jobj.Page == jobj.Pages) {
      span = document.createElement('SPAN');
      span.appendChild(document.createTextNode('>'));
      span.style.color = '#999';
      span.className = 'listnav';
      node.appendChild(span);
   
      span = document.createElement('SPAN');
      span.appendChild(document.createTextNode('\xBB'));
      span.style.color = '#999';
      span.className = 'listnav';
      node.appendChild(span);
   } else {
      a = document.createElement('A');
      a.href = window.location.search + '#';
      a.appendChild(document.createTextNode('>'));
      a.onclick = function() {s2_setpage(jobj.IType,(jobj.Page+1));};
      a.className = 'listnav';
      node.appendChild(a);
   
      a = document.createElement('A');
      a.href = window.location.search + '#';
      a.appendChild(document.createTextNode('\xBB'));
      a.onclick = function() {s2_setpage(jobj.IType,(jobj.Pages));};
      a.className = 'listnav';
      node.appendChild(a);
   }
   return node;
}
function s2_removefilter(itype) {
   var s2settings = s2_getcurrent();
   if (s2settings[itype]) {
      var itypeobj = s2settings[itype]; 
      delete itypeobj.SearchBy;
      delete itypeobj.SearchFor
      s2settings[itype] = itypeobj; 
      s2_setstorage('S2Settings',s2settings);
      s2_refresh();
   }
}
function s2_drawlisttable(jobj,parent) {
   forms++;
   var dialog,i,j,indent,tabs,extratab,prop,index,type,button,title;
   var table,thead,tbody,tr,th,td,node,a,id,lid,span,h2,nodeid,prefix;
   var interfacetype;
   dialog = document.getElementById('incontentdialog');
   if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
   if (jobj.Page) {
      node = s2_pagelinks(jobj);
      dialog.appendChild(node);
   }
   node = s2_searchbylinks(jobj);
   dialog.appendChild(node);
   h2 = document.createElement('H2');
   //prefix = (jobj.IsAllowedTo.View)?'Select ':null;
   title = (jobj.IsAllowedTo.View)?'Select '+jobj.DisplayName:jobj.DisplayName;
   h2.appendChild(document.createTextNode(title));
   dialog.appendChild(h2);
   table = document.createElement('TABLE');
   table.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Table';
   table.className = 'listform';
   thead = document.createElement('THEAD');
   tr = document.createElement('TR');
   if (jobj.Versions && jobj.Versions.Column) {
      th = document.createElement('TH');
      th.appendChild(document.createTextNode('Current'));
      tr.appendChild(th);
   } 
   for(prop in jobj.Interface) {
      if (!jobj.Interface[prop].Hidden) {
         th = s2_getnode(null,prop,jobj.Interface[prop],null,'ListHeader');
         if (th) {
            if (s2_orderingenabled()) s2_addorder(th,jobj,prop);
            tr.appendChild(th);
         }
      }
   }
   if (jobj.IsAllowedTo.View) {
      th = document.createElement('TH');
      th.className = 's2_optionsnode';
      if (jobj.IsAllowedTo.Add) {
         button = document.createElement('BUTTON');
         button.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Action-AddInterface';
         button.onclick = function() {s2_showaddinterface(this.id);};
         button.appendChild(document.createTextNode('Add'));
         th.appendChild(button);
      }
      if (jobj.IsAllowedTo.Import) {
         button = document.createElement('BUTTON');
         button.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Action-ImportInterface';
         button.onclick = function() {s2_showimportinterface(this.id);};
         button.appendChild(document.createTextNode('Import'));
         th.appendChild(button);
      }
      tr.appendChild(th);
   }   
   thead.appendChild(tr);
   
   table.appendChild(thead);
   tbody = document.createElement('TBODY');
   for(index in jobj.ListData) {
      var listitem = jobj.ListData[index];
      tr = document.createElement('TR');
      if (jobj.Versions && jobj.Versions.Column) {
         td = gen_getlistversionnode(jobj,listitem);                   
         tr.appendChild(td);
      }
      var id=null,name=null,interfacetype;
      for(prop in jobj.Interface) {
         if (jobj.Interface[prop] == 'Unique Key') id = listitem[prop];
         if (!jobj.Interface[prop].Hidden) {
            nodeid = 'Form-'+forms+'_IType-'+jobj.IType+'_Property-'+prop+'_Current-'+((id)?id:index);          
            td = s2_getnode(nodeid,prop,jobj.Interface[prop],listitem[prop],'List');
            if (td) {
               if (td.className == 'currentversion' || td.className == 'autodata') tr.className = td.className;
               tr.appendChild(td);
            }
         }
      }
      td = gen_getlistoptionsnode(jobj,listitem);
      if (td) tr.appendChild(td);
      tbody.appendChild(tr);
   }
   table.appendChild(tbody);
   //table.onload = function() {table_split(this.id);};
   dialog.appendChild(table);
   s2_nbnifytab(table);
   s2_bgsifytab(table);
   
   if (jobj.IsAllowedTo.View) gen_appendchooseselected(jobj,dialog,forms);
   if (jobj.Versions && jobj.Versions.Current) { 
      var vid = 'Form-'+forms+'_IType-'+jobj.IType+'_Action-SetVersion_Version-'+jobj.Versions.Current;
      var vnode = document.getElementById(vid); 
      if (vnode) vnode.checked = 'checked';
   }
   /*
   // add extender button
   ext = s2_getextenderbutton(jobj,'edit');
   if (ext) dialog.appendChild(ext);
   */
   gen_appendhiddenfields(jobj,dialog,forms);
   
   return true;
}
function s2_drawlinks(jobj) {
   forms++;
   var dialog,i,j,indent,tabs,extratab,prop,index,type,button,title;
   var node,a,id,lid,lname,span,h2,nodeid,prefix,ol,ul,li;
   var interfacetype;
   dialog = document.getElementById('incontentdialog');
   if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
   //if (jobj.IsAllowedTo.View) {
      ol = document.createElement('OL');
      for(index in jobj.ListData) {
         var listitem = jobj.ListData[index];
         li = document.createElement('LI');
         a = document.createElement('A');
         lname = (listitem['Name'])?('View Report: '+listitem['Name'].replace(/\_/g,' ').replace(/\.html/,'')):'View Report';
         a.appendChild(document.createTextNode(lname));
         a.href = listitem.URL; 
         a.target = 'S2Reports';
         li.appendChild(a);
         
         if (jobj.IsAllowedTo.Delete) {
            a = document.createElement('A');
            a.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Action-Delete_Current-'+listitem.Current;
            a.href = window.location.search + '#';
            a.style.marginLeft = '20px';
            a.appendChild(document.createTextNode('Delete'));
            a.onclick = function() {
               if (s2_confirmdelete()) s2_itypeselect(this.id);
            };
            li.appendChild(a);
         }
         
         ol.appendChild(li);         
      }
      dialog.appendChild(ol);
   //}
   return true;
}
function s2_drawviews(jobj) {
   var ext;
   if (jobj.Current) {
      var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input;
      var div,node,text,id,lid,action,viewname;
      dialog = document.getElementById('incontentdialog');
      div = document.createElement('DIV');
      div.className = 'viewmenu';
      div.id = 'Menu_IType-'+jobj.IType;
      for(i in jobj.Views) {
         viewname = jobj.Views[i];
         if (jobj.ViewName == viewname) {
            node = document.createElement('SPAN');
            node.className = 'viewmenulink';
            node.appendChild(document.createTextNode(viewname));
         } else { 
            //alert(viewname);
            node = document.createElement('A');
            node.href = window.location.search + '#';
            node.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Action-View_Current-'+jobj.Current+'_ViewName-'+viewname;
            node.className = 'viewmenulink';
            node.style.marginRight = '5px';
            node.appendChild(document.createTextNode(viewname));
            node.onclick = function() {s2_itypeselect(this.id);};
         }
         div.appendChild(node);
         div.appendChild(document.createTextNode(' '));
      }
      ext = s2_getextenderbutton(jobj,'view');
      if (ext) div.appendChild(ext);            
      dialog.appendChild(div);
   } 
   return true;
}  

function s2_drawalternatives(jobj) {
   //if (!jobj.Current) {
      var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input;
      var div,node,text,id,lid,action,viewname,classname;
      dialog = document.getElementById('incontentdialog');
      div = document.createElement('DIV');
      div.className = 'viewmenu';
      if (jobj.DisplayName && (jobj.Current || !jobj.ListData)) {
         node = document.createElement('A');
         node.href = window.location.search + '#';
         node.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Action-List';
         node.style.marginRight = '10px';
         node.appendChild(document.createTextNode(jobj.DisplayName));
         node.onclick = function() {s2_itypeselect(this.id);};
         div.appendChild(node);
      }
      for(viewname in jobj.Alternatives) {
         classname = jobj.Alternatives[viewname];
         //alert(viewname);
         node = document.createElement('A');
         node.href = window.location.search + '#';
         node.id = 'Form-'+forms+'_IType-'+classname+'_Action-List';
         node.style.marginRight = '10px';
         node.appendChild(document.createTextNode(viewname));
         node.onclick = function() {s2_itypeselect(this.id);};
         div.appendChild(node);
      }
      dialog.appendChild(div);
   //} 
   return true;
}  

function s2_drawform(jobj) {
   //cleardialog('dialog-liner');
   forms++;
   var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input;
   var field,fieldname,fielddisplay,fieldtype,nodeid,link;
   var table,thead,tbody,tr,th,td,node,text,id,lid,action,ul,li;
   action = jobj.Form.Action;
   dialog = document.getElementById('incontentdialog');
   if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
   if (!jobj.Data || jobj.Interim == true) {
      var h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode(jobj.Form.Title));
      dialog.appendChild(h2);
   } 
   table = document.createElement('TABLE');
   table.id = action+'_'+jobj.Form.IType;
   table.className = 'addform';
   tbody = document.createElement('TBODY');
   for(prop in jobj.Form.Fields) {
      field = jobj.Form.Fields[prop];
      fieldname = field.Name;
      fielddisplay = field.DisplayName;
      params = field.Params;
      fieldtype = field.Params.DataType;
      
      tr = document.createElement('TR')
      th = document.createElement('TH');
      th.className = 'left';
      th.appendChild(document.createTextNode(fielddisplay));
      tr.appendChild(th);
      nodeid = 'Form-'+forms+'_IType-'+jobj.Form.IType+'_Property-'+fieldname;
      var data = s2_getqsparams(fieldname);
      td = s2_getnode(nodeid,fieldname,params,data,'View');
      if (td) {
         if (td.className == 'currentversion' || td.className == 'autodata') tr.className = td.className;
         tr.appendChild(td);
      }
      tbody.appendChild(tr);
   }
   table.appendChild(tbody);
   dialog.appendChild(table);
s2_nbnifytab(table);
s2_bgsifytab(table);
   button = document.createElement('BUTTON');
   button.onclick = function() {s2_submitform(this.id);};
   button.id = 'Form-'+forms+'_Action-'+action;
   button.appendChild(document.createTextNode(jobj.Form.Title));
   dialog.appendChild(button);
   if (jobj.Form.Links) {
      ul = document.createElement('UL');
      for (prop in jobj.Form.Links) {
         link = jobj.Form.Links[prop];
         li = document.createElement('LI');
         a = document.createElement('A');
         a.className = 'ListEntry';
         a.appendChild(document.createTextNode(link.DisplayName));
         a.href = (link.URL.match(/^http\:\/+/))?link.URL:'../'+link.URL;
         if (link.Target) a.target = link.Target;
         li.appendChild(a);         
         ul.appendChild(li);         
      }      
      dialog.appendChild(ul);
   }   
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_IType';
   node.type = 'hidden';
   node.value = jobj.Form.IType;
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_ViewIType';
   node.type = 'hidden';
   node.value = jobj.Form.IType;
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_Action';
   node.type = 'hidden';
   node.value = action;
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_FormInterface';
   node.type = 'hidden';
   node.value = JSON.stringify(jobj.Form);
   dialog.appendChild(node);
   //showdialog();
   return true;
}

function s2_drawaddform(jobj) {
   //cleardialog('dialog-liner');
   forms++;
   var dialog,container,i,j,indent,tabs,extratab,prop,propname,type,button,input,datatype;
   var table,thead,tbody,tr,th,td,node,text,id,lid,action,data,params,uploaders,cols,includecol;
   var interfacetype,
   action = (jobj.Current || (jobj.Data && !jobj.Interim))?'Edit':'Add';
   dialog = document.getElementById('incontentdialog');
   if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
   if (!jobj.Data || jobj.Interim == true) {
      var h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode(action+' '+jobj.DisplayName));
      dialog.appendChild(h2);
   } 
   table = document.createElement('TABLE');
   table.id = action+'_'+jobj.IType;
   table.className = 'addform';
   tbody = document.createElement('TBODY');
   
   for(prop in jobj.Interface) {
      id = 'Form-'+forms+'_IType-'+jobj.IType+'_Property-'+prop;
      params = jobj.Interface[prop];
      datatype = (typeof params == 'object')?params.DataType:params.replace(/:.+/,'');
      
      includecol = (!jobj.Columns || s2_inarray(prop,jobj.Columns));
      if (typeof params != 'object' || !(params.Hidden || params.NoView)) {
      
         if (!jobj.Data && jobj.Versions && prop == jobj.Versions.Column) data = (jobj.Versions.Maximum)?(Math.floor((jobj.Versions.Maximum + 0.1)*10)/10):1;
         else if (!jobj.Data && datatype == 'Count') data = (jobj.Reserved)?parseInt(jobj.Reserved):((jobj.Count)?parseInt(jobj.Count)+1:1);       
         else data = (jobj.Data && jobj.Data[prop])?jobj.Data[prop]:null;
         td = s2_getnode(id,prop,params,data,'View');
         
         if (td && includecol) {
            tr = document.createElement('TR')
            th = document.createElement('TH');
            th.className = 'left';
            if (prop.match(/_ID$/)) {
               prop = prop.replace(/^[^_]+_/,'');
               prop = prop.replace(/_ID$/,'');
            }
            th.appendChild(document.createTextNode(prop.replace(/\_/g,' ')));
            tr.appendChild(th);
            tr.appendChild(td);
            tbody.appendChild(tr);
         }
      }
   }
   table.appendChild(tbody);
   dialog.appendChild(table);
s2_nbnifytab(table);
s2_bgsifytab(table);
   
   if (jobj.Current && jobj.IsAllowedTo && jobj.IsAllowedTo.Edit) {
      button = document.createElement('BUTTON');
      button.onclick = function() {s2_submitdialog(this.id);};
      button.id = 'Form-'+forms+'_Action-Edit_Current-'+jobj.Current;
      button.appendChild(document.createTextNode('Save'));
      dialog.appendChild(button);   
   } else if (jobj.IsAllowedTo && jobj.IsAllowedTo.Add) {
      button = document.createElement('BUTTON');
      button.onclick = function() {s2_submitdialog(this.id);};
      button.id = 'Form-'+forms+'_Action-Add';
      button.appendChild(document.createTextNode('Save'));
      dialog.appendChild(button);   
   }
   if (jobj.Current && jobj.IsAllowedTo && jobj.IsAllowedTo.Delete) {
      button = document.createElement('BUTTON');
      button.onclick = function() {
         if (s2_confirmdelete()) s2_submitdialog(this.id);
      };
      button.id = 'Form-'+forms+'_Action-Delete_Current-'+jobj.Current;
      button.appendChild(document.createTextNode('Delete'));
      button.style.marginLeft = '5px';
      dialog.appendChild(button);   
   }
   /*
   // add extender button
   ext = s2_getextenderbutton(jobj,'edit');
   if (ext) dialog.appendChild(ext);
   */
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_ViewIType';
   node.type = 'hidden';
   node.value = jobj.IType;
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_ActionIType';
   node.type = 'hidden';
   node.value = jobj.IType;
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_Action';
   node.type = 'hidden';
   node.value = action;
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_Interface';
   node.type = 'hidden';
   node.value = JSON.stringify(jobj);
   dialog.appendChild(node);
   if (jobj.ViewName) {
      node = document.createElement('INPUT');
      node.id = 'Form-'+forms+'_ViewName';
      node.type = 'hidden';
      node.value = jobj.ViewName;
      dialog.appendChild(node);
   }
   //if (!jobj.Current) 
      s2_derivefromlinks();
   s2_onloadnode();
   return true;
}

function s2_drawhiddenparent(jobj) {
   //cleardialog('dialog-liner');
   forms++;
   var dialog,container,i,j,indent,tabs,extratab,prop,propname,type,button,input,datatype;
   var table,thead,tbody,tr,th,td,node,text,id,lid,action,data,params,uploaders;
   var interfacetype;
   action = (jobj.Current || (jobj.Data && !jobj.Interim))?'Edit':'Add';
   dialog = document.getElementById('incontentdialog');
   container = document.createElement('DIV');
   container.className = 's2hiddenparent';
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_ViewIType';
   node.type = 'hidden';
   node.value = jobj.IType;
   container.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_ActionIType';
   node.type = 'hidden';
   node.value = jobj.IType;
   container.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_Action';
   node.type = 'hidden';
   node.value = action;
   container.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_Interface';
   node.type = 'hidden';
   node.value = JSON.stringify(jobj);
   container.appendChild(node);
   if (jobj.ViewName) {
      node = document.createElement('INPUT');
      node.id = 'Form-'+forms+'_ViewName';
      node.type = 'hidden';
      node.value = jobj.ViewName;
      container.appendChild(node);
   }
   dialog.appendChild(container);
   return true;
}

function s2_showlist(id,target) {
   var args = s2_disectid(id);
   var pars = new Object();
   pars.IType = args.IType;
   pars.Action = 'List';
   s2_query(pars);
   return true;
}

function s2_submitform(id) {
   hidedialog();
   var args = s2_disectid(id);
   var prefix = 'Form-'+args.Form+'_';
   var jobj = JSON.parse(document.getElementById(prefix+'FormInterface').value);
   var action = jobj.Action;
   var itype = jobj.IType;
   var parameters = new Object();
   parameters.IType = itype;
   parameters.Action = action;
   var field;
   for(prop in jobj.Fields) {
      field = jobj.Fields[prop];
      switch (field.Type) {
      case 'Unique Key': break;
      default: parameters[field.Name] = document.getElementById(prefix+'IType-'+itype+'_Property-'+field.Name).value;break;
      }
   }
   s2_query(parameters);
}
function s2_submitdialog(id) {
   hidedialog();
   var node,id;
   var args = s2_disectid(id);
   var prefix = 'Form-'+args.Form+'_';
   var itype = document.getElementById(prefix+'ActionIType').value;
   var action = (args.Action)?args.Action:document.getElementById(prefix+'Action').value;
   var jobj = JSON.parse(document.getElementById(prefix+'Interface').value);
   var parameters = new Object();
   parameters.IType = itype;
   parameters.Action = action;
   if (action == 'Add') s2_surpressderive = true;
   if (jobj.Current && action == 'Delete') s2_unsetcurrent(itype); 
   if (jobj.Current && jobj.Current > 0) parameters.Current = jobj.Current;
   var datatype;
   var alldataisvalid,propisvalid,invalidprops; 
   alldataisvalid = true;
   invalidprops = new Array();
   
   for(prop in jobj.Interface) {
      if (typeof jobj.Interface[prop] == 'object') datatype = (jobj.Interface[prop].Interface)?jobj.Interface[prop].Interface:jobj.Interface[prop].DataType;
      else datatype = jobj.Interface[prop].replace(/([^\:]+).*/,'$1');
      switch (datatype) {
      case 'Unique Key': break;
      case 'File Path': {
         //node = document.getElementById(prefix+'IType-'+itype+'_Property-'+prop); 
         //if (node) parameters[prop] = escape(node.value);
         id = prefix+'IType-'+itype+'_Property-'+prop;
         node = s2_getnode(id,prop,jobj.Interface[prop],null,'Submit');
         parameters[prop] = escape(node);          
      }break;
      case 'URL': {
         //node = document.getElementById(prefix+'IType-'+itype+'_Property-'+prop); 
         //if (node) parameters[prop] = escape(node.value);
         id = prefix+'IType-'+itype+'_Property-'+prop;
         node = s2_getnode(id,prop,jobj.Interface[prop],null,'Submit'); 
         parameters[prop] = escape(node);
      }break;
      default: {
         //node = document.getElementById(prefix+'IType-'+itype+'_Property-'+prop); 
         //if (node) parameters[prop] = node.value;
         id = prefix+'IType-'+itype+'_Property-'+prop;
         node = s2_getnode(id,prop,jobj.Interface[prop],null,'Submit'); 
         parameters[prop] = node;
      }break;
      }
      // data validation
      
      propisvalid = s2_propertyvalueisvalid(prop,datatype,parameters[prop]);
      if (!propisvalid) {
         invalidprops.push(prop + ' = ' + parameters[prop]);
         //alert(prop + ' ' + parameters[prop]);
      } 
   }
   alldataisvalid = (invalidprops.length == 0);
   if (alldataisvalid) s2_query(parameters);
   else alert('Invalid entries:\n' + invalidprops.join('\n')); 
}

function s2_propertyvalueisvalid(property,datatype,value) {
   // assume it's fine. 
   var isvalid = true;
   // it's fine if it's null (add mandatory test)
   if (value) {
   // otherwise test based on datatype.
      switch(datatype) {
      case 'Short Text':      {isvalid = (value.length <= 100);}break;
      case 'Medium Text':     {isvalid = (value.length <= 200);}break;
      case 'Long Text':       {isvalid = (value.length <= Math.pow(2,16));}break;
      case 'Password':        {isvalid = (value.length <= 100);}break;
      case 'Encrypted Text':  {isvalid = (value.length <= Math.pow(2,16));}break;
      case 'Number':          {isvalid = (value.match(/^\d+$/));}break;
      case 'Percentage':      {isvalid = (value.match(/^[\d,\.]+%*$/));}break;
      case 'Count':           {isvalid = (value.match(/^\d+$/));}break;
      case 'Decimal':         {isvalid = (value.match(/^\-*[\d,\.]+$/));}break;
      case 'Version':         {isvalid = (value.match(/^[\d,\.]+$/));}break;
      case 'True or False':   {isvalid = ((value == 0)||(value == 1));}break;
      case 'Date':            {isvalud = (value.match(/^\d{2}\/\d{2}\/\d{4}$/));}break;
      case 'Time':            {isvalid = (value.match(/^[\d,\:]+$/));}break;
      //case 'Date and Time':   {isvalid = (value.match(/^\d{2}\/\d{2}\/\d{4}\s[\d,\:]+$/));}break;
      //case 'Map Data':        {isvalid = (value.match(/^POLYGON\(\([\d,\.\,]+\)\)$/));}break;
      //case 'Taxa':            {/* Taxa */}break;
      case 'URL':             {isvalid = (value.length <= 500);}break;
      case 'Email':           {isvalid = (value.length <= 100);}break;
      case 'File Path':       {isvalid = (value.length <= 500);}break;
      //case 'File':            {/* File */}break;
      }
   }
   return isvalid; 
}

function s2_setversion(id) {
   var args = s2_disectid(id);
   var prefix = 'Form-'+args.Form+'_';
   var jobj = JSON.parse(document.getElementById(prefix+'Interface').value);
   var parameters = new Object();
   parameters.Action = args.Action;
   if (jobj.Current && jobj.Current > 0) parameters.Current = jobj.Current;
   var bits,dtype,classname,column;
   for(prop in jobj.Interface) {
      dtype = jobj.Interface[prop].DataType;
      //bits = jobj.Interface[prop].split(/\:/);
      //dtype = bits[0];
      switch (dtype) {
      case 'Version': {
         //parameters.IType = bits[1];
         parameters.IType = jobj.Interface[prop].VersionOfType; 
         parameters.VersionOf = args.IType; 
         parameters.Version = args.Version;
      }break;
      }
   }
   s2_query(parameters);   
}
function cleardialog(dlinerid) {
   var dliner = document.getElementById(dlinerid);
   while(dliner.lastChild.id != 'dialog-title') {
      dliner.removeChild(dliner.lastChild);
   }
   return true;
}
function snc_send(r) {   
   inprogress();   
   r = cdm_wrapper(r);
   var ac = new AjaxClass();
   ac.url = r.target;
   ac.parameters = r.request;
   if (r.responder) ac.responder = r.responder;
   if (r.method) ac.Method = r.method;
   if (r.sync) ac.Async = !r.sync;
   ac.init();
   var resp = ac.send();
   if (r.sync) return resp;
}

function enableinterface() {
   var settings = s2_getstorage('Settings');
   //if (!settings) {
      var json = new Object();
      if (settings) json.Settings = settings;
      var action = new Object();
      action.Task = 'startsession';
      json.Action = action;
      snc_executeTask(json,true);
   //}             
}

function snc_executeTask(json) {
   var server = s2_getstorage('cdm_server');
   var r = new Object();
   r.target = 'http://'+server+'/snc_sqlitetasks2.php';
   var parameters = new Object();
   parameters['json'] = json;
   r.request = parameters;
   r.responder = snc_actionStatus;
   snc_send(r);    
}
function snc_actionStatus(json) {
   var json_obj = JSON.parse(json);
   if (json_obj.Settings) {
      var settings = json_obj.Settings;
      s2_setstorage('Settings',settings);
   }
   /*
   if (json_obj.Status) {
      alert(json_obj.Status.Message);
   }
   */
   if (json_obj.Choices) {
      var newchoices = json_obj.Choices;
      var choices = s2_getstorage('Choices');
      if (!choices) choices = new Object();
      for (prop in newchoices) choices[prop] = newchoices[prop];
      s2_setstorage('Choices',choices);
   }
   if (json_obj.Action) snc_actionDialog(json_obj.Action);
   if (json_obj.Options) snc_report_json_obj(json);
   s2_query();
   completed();
}
function snc_clearMenus(menuid) {
   var menudiv = document.getElementById(menuid);
   while(menudiv.childNodes.length > 0) {
      menudiv.removeChild(menudiv.lastChild);
   }
   return true;
}
function snc_get_json_html(json) {
   var pre = document.createElement('PRE');
   indent = 0;
   tabs = '';
   for (i=0;i<json.length;i++) {
      switch(json.substr(i,1)) {
      case ',': {
         tabs = '\n';
         for(j=0;j<indent;j++) tabs = tabs + '\t';          
         json = json.substr(0,i+1) + tabs + json.substr(i+1);
         i += tabs.length;      
      }break;
      case '{': {
         indent++;
         tabs = '\n';
         for(j=0;j<indent;j++) tabs = tabs + '\t';          
         json = json.substr(0,i+1) + tabs + json.substr(i+1);
         i += tabs.length;         
      }break;
      case '[': {
         indent++;
         tabs = '\n';
         for(j=0;j<indent;j++) tabs = tabs + '\t';          
         json = json.substr(0,i+1) + tabs + json.substr(i+1);
         i += tabs.length;
      }break;
      case '}': {
         if (json.substr(i+1,1) == ',') {
            indent--;
            tabs = '\n';
            for(j=0;j<indent;j++) tabs = tabs + '\t';          
            json = json.substr(0,i) + tabs + json.substr(i,2) + tabs + json.substr(i+2);
            i += tabs.length * 2;
         } else {
            indent-=2;
            tabs = '\n';
            extratab = (indent >= 0)?'\t':''; 
            for(j=0;j<indent;j++) tabs = tabs + '\t';
            json = json.substr(0,i) + tabs + extratab + json.substr(i,1) + tabs + json.substr(i+1);
            i += (tabs.length + 1) * 2; // extra 2 for the extra \t tab
         }      
      }break;
      case ']': {
         if (json.substr(i+1,1) == ',') {
            indent--;
            tabs = '\n';
            for(j=0;j<indent;j++) tabs = tabs + '\t';          
            json = json.substr(0,i) + tabs + json.substr(i,2) + tabs + json.substr(i+2);
            i += tabs.length * 2;
         } else {
            indent-=2;
            tabs = '\n';
            extratab = (indent >= 0)?'\t':''; 
            for(j=0;j<indent;j++) tabs = tabs + '\t';
            json = json.substr(0,i) + tabs + extratab + json.substr(i,1) + tabs + json.substr(i+1);
            i += (tabs.length + 1) * 2; // extra 2 for the extra \t tab
         }
      }break;
      }      
   }   
   pre.appendChild(document.createTextNode(json));
   pre.id = 'json_response';
   return pre;
}

function snc_report_json_obj(json) {
/*
   debugging code to print the returned json readably formatted to the screen     
   for some reason the last close bracket indent doesn't get removed.
*/
   var dialog,i,j,indent,tabs,extratab;
   //snc_clearMenus('incontentdialog');   
   dialog = document.getElementById('incontentdialog');
   var pre = snc_get_json_html(json);   
   dialog.appendChild(pre);      
   return true;
}

/*
   Interfaces provide the interaction between separate databases or separate 
   domains in S2. 
   
   To allow users to specify an existing contacts database rather than the 
   integrated S2 contacts database, S2 separates contacts into a separate 
   domain. 
   
   Data from an external source or from another domain is stored as the JSON 
   necessary to retreive the data from that domain with a mandatory name and 
   value element allowing the data to be rendered on the interface. 
   
   So if your site has an owner then you need to define an interface column 
   for the owner with the Domain "contacts" and the DataType "Contact" 
   when prompted to enter this information the user will be linked to the 
   contacts domain to retrieve the information which will be returned as JSON 
   and populated into the interface as the "Name" property.
   
   The big question is whether to post the request or to just store it in a 
   cookie. POST would be the obvious choice except that it isn't available to 
   javascript which would mean writing the post data back into the server 
   generated HTML which is ugly. GET is messy looking and old fashioned so 
   cookies it is.
   
   if you're using encryption you can encrypt the json into the cookie.
   can use get to set the datatype to be looked up from the cookie?        
*/
function s2_ifaceselect(json,noedit) {
   var params = JSON.parse(json);
   if (typeof params == 'object') {
      var s2int = new S2DomainInterface();
      var todom = s2_getcurrentdomain();
      if (!noedit) s2_updateinterim(params.IType);
      else s2_unsetinterim(params.IType);
      s2int.Init(params.From_Type,params.In_Domain,todom,params.ValidTypes,params.Display_Name);
      s2int.Edit = !noedit;
      if (params.Name) s2int.Set(params.Data_Type,params.Name,params.Value);
      s2int.Save();
      s2int.Send();
   }
}
function s2_ifaceclear(element) {
   var jsonnode,linknode;
   linknode = element.previousSibling;
   jsonnode = element.nextSibling;
   jsonnode.value = '';
   
   element.parentNode.removeChild(linknode);
   element.parentNode.removeChild(element);
}
function S2DomainInterface() {
// cookie name  = 'S2Interface';
   this.Display_Name = null;
   this.In_Domain = null;
   this.To_Domain = null
   this.Data_Type = null;
   this.ValidTypes = null;
   this.From_Type = null;
   this.Edit = true;
   this.Name = null;
   this.Value = null;
   this.Draw = false;
   this.Init = function(fromtype,indomain,todomain,types,display) {
      this.From_Type = fromtype;
      this.In_Domain = indomain;
      this.To_Domain = todomain;
      this.ValidTypes = types;
      this.Display_Name = display;
   }
   this.Set = function(type,name,value) {
      this.Data_Type = type;
      this.Name = name;
      this.Value = value;
   };
   this.Save = function() {
      var json = JSON.stringify(this);          
      s2_setstorage('S2Interface',json);
      s2_savestorage();
   }                
   this.Send = function() {
      var path = s2_getcurrentpath();
      path = path.replace(/\/[^\/]+$/,'/'+this.In_Domain);
      
      vtypes = this.ValidTypes;
      if (!this.Value) {
         var txt = 'Select one of: ';
         for(i in vtypes) {
            vtypes[i] = vtypes[i].replace(/^[^\_]+\_/,'');
            vtypes[i] = vtypes[i].replace(/\_/, ' ');
         }
         txt += vtypes.join(', ');
         txt += '\nThen press the "'+this.Display_Name+'" button in the menu bar';
         alert(txt);
      }
      window.location.href = path;      
   };
   this.AddButton = function () {
      var dom,menu,mnuid,btnid,button,img,txt;
      mnuid = 's2menubar';
      btnid = 's2_interfacebutton'; 
      dom = s2_getcurrentdomain();      
      if (this.In_Domain == dom) {
         menu = document.getElementById(mnuid);
         button = document.getElementById(btnid);
         if (!button) {
            button = document.createElement('BUTTON');
            button.id = btnid;
            txt = (this.Display_Name)?'Set '+this.Display_Name:'Use selected';
            button.name = 'Returns the current value into the previous form.';
            s2_addtooltip(button);
            button.appendChild(document.createTextNode(txt));
            button.onclick = function() {
               var s2int = new S2DomainInterface();
               s2int.Return();
            };
            menu.appendChild(button);
         } 
      }   
   };
   this.IsValidType = function(type) {
      var cobj,i,vtype,isvalid;
      isvalid = false;
      for(i in this.ValidTypes) {
         if (type == this.ValidTypes[i]) isvalid = true;         
      }
      return isvalid;
   }
   this.Return = function () {
      this.Read();
      var cobj,i,type;
      var types = this.ValidTypes;
      type = document.getElementById('Form-1_ActionIType').value;
      cobj = s2_getcurrent(type);
      if (cobj) {
         var path = s2_getcurrentpath();
         this.Data_Type = cobj.IType;
         this.Name = cobj.Name;
         this.Value = cobj.Current;
         this.Save();
         path = path.replace(/\/[^\/]+$/,'/'+this.To_Domain);
         window.location.href = path;
      } else {
         type = types.pop();
         type = types.join(", ") + " or " + type;         
         alert('No '+type+' is currently selected.');
      }
   };
   this.Delete = function() {
      s2_delstorage('S2Interface');
   };
   this.Read = function () {
      var thisdom,dom,prop,params;
      thisdom = false;
      var json = s2_getstorage('S2Interface');
      if (json) {
         s2int = JSON.parse(json);
         dom = s2_getcurrentdomain();
         thisdom = (s2int.In_Domain == dom);       
         for(prop in s2int) {
            this[prop] = s2int[prop];   
         }
      }
      return thisdom;             
   };
   this.Load = function () {
      var thisdom = this.Read();
      var drawn = false;
      if (this.From_Type) {
         var s2obj;
         var s2obj = s2_getinterim(this.From_Type);
         if (this.To_Domain == s2_getcurrentdomain() && s2obj) {
            //s2obj = s2obj[this.From_Type];
            for (prop in s2obj.Data) {
               val = s2obj.Data[prop];
               if (val && typeof val == 'object' && val.IsInterim) {
                  s2obj.Data[prop] = this;
                  s2obj.Interim = true;
               }
            }
            var json = JSON.stringify(s2obj); 
            s2_draw(json);
            s2_unsetinterim(s2obj.IType);
            drawn = true;
            this.Delete();         
         } else if (thisdom) {
            if(this.Edit) this.AddButton();
            if(this.Value && this.Data_Type && !this.Draw) {
               params = 'IType-'+this.Data_Type+'_Action-View_Current-'+this.Value;
               s2_itypeselect(params);
               drawn = true;
            } 
         }
      }
      return drawn;       
   };
}
function s2_intenablereturnbutton() {
   var i,vtypes,txt,buttontxt;
   var s2int = new S2DomainInterface();
   var thisdom = s2int.Read();
   if (thisdom) {
      var itype = document.getElementById('Form-1_ActionIType').value;
      var setting = s2_getcurrent(itype);
      var current = (setting)?setting.Current:false;
      var button = document.getElementById('s2_interfacebutton');
      var valid = s2int.IsValidType(itype);
      var enabled = false;
      if (button) {
         buttontxt = button.firstChild.nodeValue;
         enabled = (itype && current && valid);
         button.disabled = (enabled)?null:'disabled';
         button.className = (enabled)?'enabled':'disabled';   
      } 
   }     
}
function s2_setupdomaininterface() {
   var s2int = new S2DomainInterface();
   var drawn = s2int.Load();
   if (s2int.In_Domain != s2_getcurrentdomain() && !s2int.Edit) drawn = false;
   return drawn;   
}
function s2_redirect_with_post(path, params, method) {
   method = method || "post"; // Set method to post by default, if not specified.
   // The rest of this code assumes you are not using a library.
   // It can be made less wordy if you use one.
   var form = document.createElement("form");
   form.setAttribute("method", method);
   form.setAttribute("action", path);

   for(var key in params) {
      var hiddenField = document.createElement("input");
      hiddenField.setAttribute("type", "hidden");
      hiddenField.setAttribute("name", key);
      hiddenField.setAttribute("value", params[key]);
      form.appendChild(hiddenField);
   }
   //document.body.appendChild(form);    // Not entirely sure if this is necessary
   form.submit();
}

function s2_getextcredentials(auth,source) {
   var sid = s2_getcookie('PHPSESSID');
   var prop;
   var req = new Object();
   req.target = s2_getcurrentpath()+'/S2_EDS.php';
   var params = new Object();
   params.UInfo = s2_getcookie('u_info');
   params.Authority = auth;
   params.Source = source;
   req.request = params;
   req.etype = 'AB';
   req.sid = sid;
   req.sync = true;
   var res = snc_send(req);
   var dec = cdm_decrypt(res,'AB');
   //var dec = res;
   var obj = JSON.parse(dec); 
   completed();
   return obj;
}

function gen_getdialog() {
   var dialog = document.getElementById('incontentdialog');
   return dialog;
}

var s2derivefrom = new Array();
function s2_derivefromlinks() {
   var nodeid,derivedata;
   for(nodeid in s2derivefrom) {
      node = document.getElementById(nodeid);
      derivedata = JSON.parse(s2derivefrom[nodeid]);
      s2_derive(node,derivedata);
      delete s2derivefrom[nodeid];
   }
   return true;
}
var s2onload = new Array();
function s2_onloadnode() {
   var nodeid,dtype;
   for (nodeid in s2onload) {
      node = document.getElementById(nodeid);
      dtype = s2onload[nodeid];
      switch(dtype) {
      case 'Long Text': s2_resizelongtext(node,true);break
      }
      delete s2onload[nodeid];
   }
   return true;
}
function s2_getnode(nodeid,name,params,data,headerlistviewsubmit) {
   var itype,dtype,node,gcd;
   if (data && typeof data == 'string') data = htmlentities(data);
   if (typeof params == 'object') dtype = (params.Interface)?params.Interface:params.DataType;
   else dtype = params.replace(/\:.+/,'');
   switch(dtype) {
   case 'Long Text':       gcd = new Gen_LongTextColumnData(nodeid,name,params,data);break;
   case 'Password':        gcd = new Gen_PasswordColumnData(nodeid,name,params,data);break;
   case 'Encrypted Text':  gcd = new Gen_EncryptedColumnData(nodeid,name,params,data);break;
   case 'Number':          gcd = new Gen_NumberColumnData(nodeid,name,params,data);break;
   case 'Percentage':      gcd = new Gen_PercentageColumnData(nodeid,name,params,data);break;
   case 'Count':           gcd = new Gen_CountColumnData(nodeid,name,params,data);break;
   case 'Decimal':         gcd = new Gen_DecimalColumnData(nodeid,name,params,data);break;
   case 'Currency':        gcd = new Gen_CurrencyColumnData(nodeid,name,params,data);break;
   case 'Version':         gcd = new Gen_VersionColumnData(nodeid,name,params,data);break;
   case 'True or False':   gcd = new Gen_LogicalColumnData(nodeid,name,params,data);break;
   case 'Date':            gcd = new Gen_DateColumnData(nodeid,name,params,data);break;
   case 'Date From':       gcd = new Gen_DateColumnData(nodeid,name,params,data);break;
   case 'Date Until':      gcd = new Gen_DateColumnData(nodeid,name,params,data);break;
   case 'Time':            gcd = new Gen_TimeColumnData(nodeid,name,params,data);break;
   case 'Date and Time':   gcd = new Gen_TimestampColumnData(nodeid,name,params,data);break;
   case 'Map Data':        gcd = new Gen_MapColumnData(nodeid,name,params,data);break;
   case 'Taxa':            gcd = new Gen_TaxaColumnData(nodeid,name,params,data);break;
   case 'TVK':             gcd = new Gen_TVKColumnData(nodeid,name,params,data);break;
   case 'BGS':             gcd = new Gen_BGSColumnData(nodeid,name,params,data);break;
   case 'URL':             gcd = new Gen_URLColumnData(nodeid,name,params,data);break;
   case 'Email':           gcd = new Gen_EmailColumnData(nodeid,name,params,data);break;
   case 'File Path':       gcd = new Gen_FilePathColumnData(nodeid,name,params,data);break;
   case 'File':            gcd = new Gen_FileColumnData(nodeid,name,params,data);break;
   case 'LINKEDTO':        gcd = new Gen_LTColumnData(nodeid,name,params,data);break;
   case 'LINKEDTOM':       gcd = new Gen_LTMColumnData(nodeid,name,params,data);break;
   case 'INTERFACE':       gcd = new Gen_InterfaceColumnData(nodeid,name,params,data);break;
   default:                gcd = new Gen_ColumnData(nodeid,name,params,data);break;
   }
   switch(headerlistviewsubmit) {
   case 'ListHeader':   node = gcd.getListHeaderNode();break;
   case 'List':         node = gcd.getListDataNode();break;
   case 'View':         node = gcd.getDataNode();break;
   case 'Submit':       node = gcd.getSubmissionData();break;
   case 'Interim':      node = gcd.getInterim();break; 
   }
   if (node && !node.className) node.className = dtype.replace(/\s+/g,'_');
   if (nodeid && dtype == 'LINKEDTO' && params.Derives && headerlistviewsubmit == 'View') s2derivefrom[nodeid] = JSON.stringify(params.Derives);
   if (nodeid && dtype == 'Long Text') s2onload[nodeid] = dtype; 
   return node;
}
function gen_getlistversionnode(jobj,listitem) {
   var td,node,c;
   c = false;
   td = document.createElement('TD');
   node = document.createElement('INPUT');
   node.type = 'radio';
   node.name = 'V';
   node.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Action-SetVersion_Version-'+listitem[jobj.Versions.Column];
   node.value = listitem[jobj.Versions.Column]; 
   if (listitem[jobj.Versions.Column] == jobj.Versions.Current) {
      node.checked = 'checked';
      //c = true;
      td.className = 'currentversion';
   } else {
      td.className = 'autodata';
   }
   node.onclick = function() {s2_setversion(this.id);}; 
   td.appendChild(node);
   //if (c) node.checked = 'checked';
   return td;
}
function gen_getlistoptionsnode(jobj,listitem) {
   var td,input,button,form,prop,id,a,type;
   for (prop in jobj.Interface) {
      if (jobj.Interface[prop] == 'Unique Key') id = listitem[prop]; 
   }
   form = forms;
   td = null;
   if (jobj.IsAllowedTo.View) {
      td = document.createElement('TD');
      td.className = 's2_optionsnode';
      if (jobj.IsAllowedTo.View) {
         input = document.createElement('INPUT');
         input.id = 'Form-'+form+'_IType-'+jobj.IType+'_Action-Tick_Current-'+id;
         input.className = 'tick';
         input.type = 'checkbox';
         td.appendChild(input);
         a = document.createElement('A');
         a.id = 'Form-'+form+'_IType-'+jobj.IType+'_Action-View_Current-'+id;
         a.href = window.location.search + '#';
         a.appendChild(document.createTextNode('Select')); // View | Select
         a.onclick = function() {s2_itypeselect(this.id);};
         td.appendChild(a);
      }
      if (jobj.IsAllowedTo.Delete) {
         a = document.createElement('A');
         a.id = 'Form-'+form+'_IType-'+jobj.IType+'_Action-Delete_Current-'+id;
         a.href = window.location.search + '#';
         a.style.marginLeft = '5px';
         a.appendChild(document.createTextNode('Delete'));
         a.onclick = function() {
            if (s2_confirmdelete()) s2_itypeselect(this.id);
         };
         td.appendChild(document.createTextNode(' '));
         td.appendChild(a);
      }
   }
   return td;
}
function gen_appendhiddenfields(jobj) {
   var node,form,dialog;
   form = forms;
   dialog = gen_getdialog();
   node = document.createElement('INPUT');
   node.id = 'Form-'+form+'_ViewIType';
   node.type = 'hidden';
   node.value = jobj.IType;
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+form+'_ActionIType';
   node.type = 'hidden';
   node.value = jobj.IType;
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+form+'_Action';
   node.type = 'hidden';
   node.value = 'Add';
   dialog.appendChild(node);
   node = document.createElement('INPUT');
   node.id = 'Form-'+form+'_Interface';
   node.type = 'hidden';
   node.value = JSON.stringify(jobj);
   dialog.appendChild(node);
   return true;
}
function gen_appendchooseselected(jobj) {
   var button,form,dialog;
   form = forms;
   dialog = gen_getdialog();   
   button = document.createElement('BUTTON');
   button.id = 'Form-'+form+'_IType-'+jobj.IType+'_Action-SelectAll';
   button.appendChild(document.createTextNode('Ticked'));
   button.onclick = function() {s2_itypeselectall(this.id);};
   dialog.appendChild(button);
   return true;
}
function gen_def_getinterim() {
   var node,form;
   node = document.getElementById(this.NodeID); 
   if (node) return node.value;
   else return null;            
}
function gen_def_getlistheadernode() {
   var pk,th,text;
   if (typeof this.Params == 'object') pk = (this.Params.DataType == 'Unique Key');
   else pk = (this.Params.replace(/\:.+/,'') == 'Unique Key');
   if (!pk && !this.Params.NoList) {
      text = this.Name;
      if (text.match(/_ID$/)) {
         text = text.replace(/^[^_]+_/,'');         
         text = text.replace(/_ID$/,'');
      }         
      text = text.replace(/\_/g,' ')         
      th = document.createElement('TH');
      th.appendChild(document.createTextNode(text));
   }
   return th;
}
function gen_def_getlistdatanode() {
   var td,text,pk;
   // IE writes null to the screen if the input value is set to null
   if (this.Data == null) this.Data = ''; 
   if (typeof this.Params == 'object') pk = (this.Params.DataType == 'Unique Key');
   else pk = (this.Params.replace(/\:.+/,'') == 'Unique Key');
   if (!pk && !this.Params.NoList) {
      td = document.createElement('TD');
      text = this.Data;
      if (text && text.length > 50) {
         text = text.substring(0,46)+'...';
      }
      td.appendChild(document.createTextNode(text));
   }
   return td;
}
function gen_def_getdatanode() {
   var td,node,width,pk,opt,oi;
   // IE writes null to the screen if the input value is set to null
   if (this.Data == null) this.Data = ''; 
   if (typeof this.Params == 'object') pk = (this.Params.DataType == 'Unique Key');
   else pk = (this.Params.replace(/\:.+/,'') == 'Unique Key');
   if (!pk) {
      td = document.createElement('TD');
      if(!this.Params.ReadOnly) {
         if (this.Params.Options) { 
            node = document.createElement('SELECT');
            for (oi in this.Params.Options) {
               opt = document.createElement('OPTION');
               opt.value = this.Params.Options[oi];
               opt.appendChild(document.createTextNode(this.Params.Options[oi]));
               if (this.Data && this.Data == this.Params.Options[oi]) opt.selected = 'selected';
               node.appendChild(opt); 
            }
         } else {
            node = document.createElement('INPUT');
            node.type = 'text';
            if (this.Data) node.value = this.Data;
         }
         node.id = this.NodeID;
         node.name = this.NodeID;
         if (this.Params && this.Params.Derives) {
            var derives = this.Params.Derives;
            node.onchange = function() {s2_derive(this,derives);};
            node.onblur = function() {s2_derive(this,derives);};
         }
         td.appendChild(node);
      } else {
         //node = document.createTextNode(this.Data);
         td.className = 'autodata';
         //td.appendChild(node);
         node = document.createElement('INPUT');
         node.type = 'text';
         node.disabled = 'disabled';
         node.id = this.NodeID;
         node.name = this.NodeID;
         if (this.Data) node.value = this.Data;
         if (this.Params && this.Params.Derives) {
            var derives = this.Params.Derives;
            node.onchange = function() {s2_derive(this,derives);};
            node.onblur = function() {s2_derive(this,derives);};
         }
         td.appendChild(node);
      }
   }
   return td;
}
function gen_def_getsubmissiondata() {
   var node = document.getElementById(this.NodeID);
   if (node) return node.value;
   else return null;
}
function s2_confirmdelete() {
   var text,confirmed;
   text = "Are you sure?\n"+
   "Deleting will delete not only this entry\n"+
   "but all entries in other tables which depend upon it.";
   confirmed = confirm(text);
   return confirmed; 
}
function s2_editlink(button) {
   var node = button.previousSibling;
   var link = node.previousSibling;
   var parent = link.parentNode;
   var text = link.firstChild.nodeValue;
   node.type = 'text';
   node.onblur = function() {s2_createlink(this);};
   parent.removeChild(link);
   parent.removeChild(button);
   return true;   
}
function s2_createlink(node) {
   var text = node.value;
   var parent = node.parentNode;
   var link = document.createElement('A');
   var button = document.createElement('BUTTON');
   var href;
   if (text.match(/@/) && !text.match(/^mailto:/)) href = 'mailto:'+text;
   else if (!text.match(/^http:\/+/)) text = 'http://'+href;
   else href = text;
   link.href = href;
   link.appendChild(document.createTextNode(text));
   link.className = 's2_txtlink';
   link.style.marginRight = '10px';
   parent.insertBefore(link,node);
   button.onclick = function() {s2_editlink(this);};
   button.appendChild(document.createTextNode('Edit'));
   parent.appendChild(button);      
   node.type = 'hidden';
   node.value = text;
   return true;      
}
function s2_sendpassword(id) {
   var params = s2_disectid(id);
   var user = s2_getcurrent(params.IType);
   var ui;
   for (ui in user) if (ui != 'Action') params[ui] = user[ui];
   s2_query(params);
}                
function Gen_ColumnData(id,name,params,data) {
/*
   Type definitions
   INTERFACE:contacts:S2_Person,S2_Organisation
   LINKEDTO:Table.Column
   LINKEDTOM:Table.Column
   
   Appends 
      :Current
      :Mandatory
*/
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_ColumnData.prototype.getInterim = gen_def_getinterim;
Gen_ColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_ColumnData.prototype.getListDataNode = gen_def_getlistdatanode;
Gen_ColumnData.prototype.getDataNode = gen_def_getdatanode;
Gen_ColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_LongTextColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_LongTextColumnData.prototype.getInterim = gen_def_getinterim;
Gen_LongTextColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_LongTextColumnData.prototype.getListDataNode = gen_def_getlistdatanode;
Gen_LongTextColumnData.prototype.getDataNode = function() {
   var td,width,div,node;
   td = document.createElement('TD');
   // IE writes null to the screen if the input value is set to null
   if (this.Data == null) this.Data = ''; 
   if (!this.Params.ReadOnly) {
      node = document.createElement('TEXTAREA');
      node.onblur = function() {s2_resizelongtext(this,true);};
      node.onchange = function() {s2_resizelongtext(this,true);};
      node.onfocus = function() {s2_resizelongtext(this,true);}; 
      node.id = this.NodeID;
      node.name = this.NodeID;
      node.value = this.Data
      td.appendChild(node);
   } else {
      node = document.createTextNode(this.Data);
      td.className = 'autodata';
      td.appendChild(node);
      node = document.createElement('input');
      node.type = 'hidden';
      node.id = this.NodeID;
      node.name = this.NodeID;
      node.value = this.Data
      td.appendChild(node);
   }
   return td;
}
function s2_resizelongtext(textarea,auto) {
   if (textarea) {
      textarea.style.height = 'auto';
      textarea.style.height = (textarea.scrollHeight + 20) + 'px';
   } 
   return true;
}  

Gen_LongTextColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_PasswordColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_PasswordColumnData.prototype.getInterim = gen_def_getinterim;
Gen_PasswordColumnData.prototype.getListHeaderNode = function() {
   /* can user see passwords if not they shouldn't be sent */
   var th,text;
   if (!this.Params.NoList) {
      th = document.createElement('TH');
      text = this.Name;
      th.appendChild(document.createTextNode(text));
   }
   return th;
}
Gen_PasswordColumnData.prototype.getListDataNode = function() {
   var node,div,button,img,td;
   // IE writes null to the screen if the input value is set to null
   if (!this.Params.NoList) {
      if (this.Data == null) this.Data = ''; 
      var enc = this.Data;
      var clr = (enc)?cdm_decrypt(enc,'AB'):null;
      td = document.createElement('TD');
      div = document.createElement('DIV');
      var unconfirmed = !this.Params.Confirmed;
      node = document.createElement('INPUT');
      node.type = 'password';
      node.id = this.NodeID+'_Confirm-2';
   
      if (this.Data) node.value = clr;
      node.disabled = 'disabled';
      div.appendChild(node);
   
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.id = this.NodeID;
      node.name = this.NodeID;
      if (this.Data) node.value = enc;
      div.appendChild(node);
      td.appendChild(div);
   }
   return td;
}
Gen_PasswordColumnData.prototype.getDataNode = function() {
   var td,node,div,button,img;
   // IE writes null to the screen if the input value is set to null
   if (this.Data == null) this.Data = ''; 
   div = document.createElement('DIV');
   var unconfirmed = !this.Params.Confirmed;
   var enc = this.Data;
   var clr = (enc)?cdm_decrypt(enc,'AB'):null;
   if (unconfirmed) {
      node = document.createElement('INPUT');
      node.type = 'password';
      node.id = this.NodeID+'_Confirm-2';
      if (this.Data) node.value = clr;
      node.onchange = function() {s2_passwordencrypt(this.value,this.id);};
      div.appendChild(node);
   } else { 
      node = document.createElement('LABEL');
      node.appendChild(document.createTextNode('1:'));
      div.appendChild(node); 
      node = document.createElement('INPUT');
      node.type = 'password';
      node.id = this.NodeID+'_Confirm-1';
      node.value = clr;
      div.appendChild(node);
      div.appendChild(document.createElement('BR'));
      node = document.createElement('LABEL');
      node.appendChild(document.createTextNode('2:'));
      div.appendChild(node); 
      node = document.createElement('INPUT');
      node.type = 'password';
      node.id = this.NodeID+'_Confirm-2';
      if (this.Data) node.value = clr;
      node.onchange = function() {s2_passwordmatches(this.id);};
      div.appendChild(node); 
   }
   node = document.createElement('INPUT');
   node.type = 'hidden';
   node.id = this.NodeID;
   node.name = this.NodeID;
   if (this.Data) node.value = enc;
   div.appendChild(node);
   if (this.Data) {
      node = document.createElement('BUTTON');
      node.className = 's2iconbutton';
      node.appendChild(document.createTextNode('Send'));
      node.id = 'IType-'+this.Args.IType+'_Action-SendPassword';
      node.onclick = function() {s2_sendpassword(this.id);};
      div.appendChild(node);
   }
   td = document.createElement('TD');
   td.appendChild(div);
   return td;
}
Gen_PasswordColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_EncryptedColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_EncryptedColumnData.prototype.getInterim = gen_def_getinterim;
Gen_EncryptedColumnData.prototype.getListHeaderNode = function() {
   /* can user see passwords if not they shouldn't be sent */
   var th,text;
   if (!this.Params.NoList) {
      th = document.createElement('TH');
      text = this.Name;
      th.appendChild(document.createTextNode(text));
   }
   return th;
}
Gen_EncryptedColumnData.prototype.getListDataNode = function() {
   var node,div,button,img,td;
   // IE writes null to the screen if the input value is set to null
   if (!this.Params.NoList) {
      if (this.Data == null) this.Data = ''; 
      var enc = this.Data;
      var clr = (enc)?cdm_decrypt(enc,'AB'):null;
      td = document.createElement('TD');
      div = document.createElement('DIV');
      var unconfirmed = !this.Params.Confirmed;
      node = document.createElement('INPUT');
      node.type = 'password';
      node.id = this.NodeID+'_Confirm-2';
   
      if (this.Data) node.value = clr;
      node.disabled = 'disabled';
      div.appendChild(node);
   
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.id = this.NodeID;
      node.name = this.NodeID;
      if (this.Data) node.value = enc;
      div.appendChild(node);
      td.appendChild(div);
   }
   return td;
}
Gen_EncryptedColumnData.prototype.getDataNode = function() {
   var td,node,div,button,img;
   // IE writes null to the screen if the input value is set to null
   if (this.Data == null) this.Data = ''; 
   div = document.createElement('DIV');
   var unconfirmed = !this.Params.Confirmed;
   var enc = this.Data;
   var clr = (enc)?cdm_decrypt(enc,'AB'):null;
   if (unconfirmed) {
      node = document.createElement('INPUT');
      node.type = 'password';
      node.id = this.NodeID+'_Confirm-2';
      if (this.Data) node.value = clr;
      node.onchange = function() {s2_passwordencrypt(this.value,this.id);};
      div.appendChild(node);
   } else { 
      node = document.createElement('LABEL');
      node.appendChild(document.createTextNode('1:'));
      div.appendChild(node); 
      node = document.createElement('INPUT');
      node.type = 'password';
      node.id = this.NodeID+'_Confirm-1';
      node.value = clr;
      div.appendChild(node);
      div.appendChild(document.createElement('BR'));
      node = document.createElement('LABEL');
      node.appendChild(document.createTextNode('2:'));
      div.appendChild(node); 
      node = document.createElement('INPUT');
      node.type = 'password';
      node.id = this.NodeID+'_Confirm-2';
      if (this.Data) node.value = clr;
      node.onchange = function() {s2_passwordmatches(this.id);};
      div.appendChild(node); 
   }
   node = document.createElement('INPUT');
   node.type = 'hidden';
   node.id = this.NodeID;
   node.name = this.NodeID;
   if (this.Data) node.value = enc;
   div.appendChild(node);
   td = document.createElement('TD');
   td.appendChild(div);
   return td;
}
Gen_EncryptedColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_NumberColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_NumberColumnData.prototype.getInterim = gen_def_getinterim;
Gen_NumberColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_NumberColumnData.prototype.getListDataNode = gen_def_getlistdatanode;
Gen_NumberColumnData.prototype.getDataNode = gen_def_getdatanode;
Gen_NumberColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_PercentageColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_PercentageColumnData.prototype.getInterim = gen_def_getinterim;
Gen_PercentageColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_PercentageColumnData.prototype.getListDataNode = function() {
   var td,node,width;
   node = document.createElement('INPUT');
   node.type = 'text';
   node.id = this.NodeID;
   node.name = this.NodeID;
   node.value = Math.floor(this.Data*100)+'%';
   //node.disabled = 'disabled';
   td = document.createElement('TD');
   //td.className = 'autodata';
   td.appendChild(node);
   return td;
}

Gen_PercentageColumnData.prototype.getDataNode = function() {
   var td,node,width;
   node = document.createElement('INPUT');
   node.type = 'text';
   node.id = this.NodeID;
   node.name = this.NodeID;
   node.value = Math.floor(this.Data*100)+'%';
   //node.disabled = 'disabled';
   td = document.createElement('TD');
   //td.className = 'autodata';
   td.appendChild(node);
   return td;
}
//Gen_PercentageColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;
Gen_PercentageColumnData.prototype.getSubmissionData = function () {
   var node = document.getElementById(this.NodeID);
   var val;
   if (node.value.match(/\%/)) node.value = (parseFloat(node.value.replace(/\%/,''))/100); 
   if (node) return node.value;
   else return null;
}



function Gen_CountColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_CountColumnData.prototype.getInterim = gen_def_getinterim;
Gen_CountColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_CountColumnData.prototype.getListDataNode = function() {
   var td,node,width;
   node = document.createElement('INPUT');
   node.type = 'text';
   node.id = this.NodeID;
   node.name = this.NodeID;
   node.value = this.Data;
   node.disabled = 'disabled';
   td = document.createElement('TD');
   td.className = 'autodata';
   td.appendChild(node);
   return td;
}

Gen_CountColumnData.prototype.getDataNode = function() {
   var td,node,width;
   node = document.createElement('INPUT');
   node.type = 'text';
   node.id = this.NodeID;
   node.name = this.NodeID;
   node.value = this.Data;
   node.disabled = 'disabled';
   td = document.createElement('TD');
   td.className = 'autodata';
   td.appendChild(node);
   return td;
}
Gen_CountColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_DecimalColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_DecimalColumnData.prototype.getInterim = gen_def_getinterim;
Gen_DecimalColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_DecimalColumnData.prototype.getListDataNode = gen_def_getlistdatanode;
Gen_DecimalColumnData.prototype.getDataNode = gen_def_getdatanode;
Gen_DecimalColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

var currency_symbol = '\xA3';
var currency_decimals = 2;
function s2_formatcurrency(symbol,value) {
   value = Math.floor(value*100)/100;
   value = symbol + value;
   return value; 
}
function Gen_CurrencyColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_CurrencyColumnData.prototype.getInterim = gen_def_getinterim;
Gen_CurrencyColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_CurrencyColumnData.prototype.getListDataNode = function() {
   var td,node,width;
   node = document.createElement('INPUT');
   node.type = 'text';
   node.id = this.NodeID;
   node.name = this.NodeID;
   node.value = s2_formatcurrency(currency_symbol,this.Data);
   td = document.createElement('TD');
   td.appendChild(node);
   return td;
}
Gen_CurrencyColumnData.prototype.getDataNode = function() {
   var td,node,width;
   node = document.createElement('INPUT');
   node.type = 'text';
   node.id = this.NodeID;
   node.name = this.NodeID;
   node.value = s2_formatcurrency(currency_symbol,this.Data);
   td = document.createElement('TD');
   td.appendChild(node);
   return td;   
}
Gen_CurrencyColumnData.prototype.getSubmissionData = function() {
   var value,rx;
   var node = document.getElementById(this.NodeID);
   if (node) {
      rx = new RegExp(currency_symbol);
      value = node.value.replace(rx,'');
      value = parseFloat(value);
      return value;
   } else return null;   
}


function Gen_VersionColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_VersionColumnData.prototype.getInterim = gen_def_getinterim;
Gen_VersionColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_VersionColumnData.prototype.getListDataNode = function() {
   var td;
   td = document.createElement('TD');
   td.className = 'autodata';
   if (typeof this.Data == 'object') td.appendChild(document.createTextNode(this.Data.Name)); 
   else td.appendChild(document.createTextNode(this.Data));
   return td;
}
Gen_VersionColumnData.prototype.getDataNode = function() {
   var td,node,width,vval;
   node = document.createElement('INPUT');
   node.type = 'text';
   node.id = this.NodeID;
   node.name = this.NodeID;
   vval = (this.Data)?this.Data:1.0; 
   node.value = vval;
   node.disabled = 'disabled';
   td = document.createElement('TD');
   td.className = 'autodata';
   td.appendChild(node);
   return td;
}
Gen_VersionColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_LogicalColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_LogicalColumnData.prototype.getInterim = gen_def_getinterim;
Gen_LogicalColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_LogicalColumnData.prototype.getListDataNode = function () {
   var td,text,pk;
   if (!this.Params.NoList) {
      td = document.createElement('TD');
      switch(parseInt(this.Data)) {
      case 1: text='True';break;
      default: text='False';break;
      }
      td.appendChild(document.createTextNode(text));
   }
   return td;
};

Gen_LogicalColumnData.prototype.getDataNode = function () {
   var td,node,width,pk,option,options,io;
   var options = new Array('False','True');
   /*
   switch (typeof this.Data) {
   case 'string': this.Data = (this.Data == 'true')?1:0; break;
   case 'boolean': this.Data = (this.Data)?1:0; break;
   }
   */
   td = document.createElement('TD');
   if (!this.Params.ReadOnly) {
      node = document.createElement('SELECT');
      node.id = this.NodeID;
      node.name = this.NodeID;
      for(io in options) {
         option = document.createElement('OPTION');
         option.appendChild(document.createTextNode(options[io]));
         option.value = io;
         if (io == this.Data) option.selected = 'selected';
         node.appendChild(option);
      }
      td.appendChild(node);
   } else {
      node = document.createTextNode(options[((this.Data)?1:0)]);
      td.className = 'autodata';
      td.appendChild(node);
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.id = this.NodeID;
      node.name = this.NodeID;
      node.value = (this.Data)?1:0;
      td.appendChild(node);
   }
   return td;
};

Gen_LogicalColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_DateColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}                                 
Gen_DateColumnData.prototype.getInterim = gen_def_getinterim;
Gen_DateColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_DateColumnData.prototype.getListDataNode = function() {
   var node,div,button,img,td;
   // IE writes null to the screen if the input value is set to null
   if (this.Data == null) this.Data = '';
   if (!this.Params.NoList) { 
      td = document.createElement('TD');
      div = document.createElement('DIV');
      node = document.createElement('INPUT');
      node.type = 'text';
      node.id = this.NodeID;
      node.name = this.NodeID;
      node.value = this.Data;
      node.style.width = '100px';
      node.style.border = 'none';
      node.style.height = '38px'
      node.style.verticalAlign = 'top';
      node.disabled = 'disabled';
      div.appendChild(node);
      td.appendChild(div);
   }
   return td;
}
Gen_DateColumnData.prototype.getDataNode = function() {
   var td,node,div,button,img;
   // IE writes null to the screen if the input value is set to null
   if (this.Data == null) this.Data = ''; 
   div = document.createElement('DIV'); 
   node = document.createElement('INPUT');
   node.type = 'text';
   node.id = this.NodeID;
   node.name = this.Name;
   node.value = this.Data;
   if (this.Params && this.Params.Derives) {
      var derives = this.Params.Derives;
      node.onchange = function() {s2_derive(this,derives);};
      node.onblur = function() {s2_derive(this,derives);};
   } 
   node.style.width = '100px';
   node.style.border = 'none';
   node.style.height = '38px'
   node.style.verticalAlign = 'top';
   div.appendChild(node);
   button = document.createElement('BUTTON');
   button.onclick = function() { s2_showcalendardialog(this.previousSibling.id);};
   button.name = 'Calendar';
   s2_addtooltip(button);
   img = new Image;
   img.src = '../images/dateicon.php?dm=m';
   img.alt = 'cal';
   button.appendChild(img);
   button.className = 's2iconbutton';
   div.appendChild(button);
   button = document.createElement('BUTTON');
   button.onclick = function() {s2_today(this.previousSibling.previousSibling.id);};
   button.name = 'Today';
   s2_addtooltip(button);
   img = new Image;
   img.src = '../images/dateicon.php?dm=d';
   img.alt = 'tdy';
   button.appendChild(img);
   button.className = 's2iconbutton';
   div.appendChild(button);
   td = document.createElement('TD');
   td.appendChild(div);
   return td;
}
Gen_DateColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_TimeColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_TimeColumnData.prototype.getInterim = gen_def_getinterim;
Gen_TimeColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_TimeColumnData.prototype.getListDataNode = gen_def_getlistdatanode;
Gen_TimeColumnData.prototype.getDataNode = gen_def_getdatanode;
Gen_TimeColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_TimestampColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_TimestampColumnData.prototype.getInterim = gen_def_getinterim;
Gen_TimestampColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_TimestampColumnData.prototype.getListDataNode = gen_def_getlistdatanode;
Gen_TimestampColumnData.prototype.getDataNode = gen_def_getdatanode;
Gen_TimestampColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_MapColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_MapColumnData.prototype.getInterim = gen_def_getinterim;
Gen_MapColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_MapColumnData.prototype.getListDataNode = function() {
   var td,node;
   if (!this.Params.NoList) {
      td = document.createElement('TD');
      if (this.Data) {
         var poly = gm3_readwkt(this.Data);
         // if poly is not in WGS84 translate back into native WKT
         if (this.Data.match(/SRID=/)) this.Data = gm3_mpoly_to_wkt(poly);
         var name = gm3_getnamefromoverlay(poly);
         var propname = this.NodeID; 
         node = document.createElement('A');
         node.id = 'Link_'+propname;
         node.href = window.location.search + '#';
         node.onclick = function() {gm3_drawmap(this.id);return false;}; 
         node.appendChild(document.createTextNode(name));
         td.appendChild(node);
         node = document.createElement('INPUT');
         node.type = 'hidden';
         node.id = propname
         node.value = this.Data;       
         td.appendChild(node);
      }
   }
   return td;
}
Gen_MapColumnData.prototype.getDataNode = function() {
   var td,node,div,button,img;
   td = document.createElement('TD');
   if (this.Data) {
      var poly = gm3_readwkt(this.Data);
      // if poly is not in WGS84 translate back into native WKT
      if (this.Data.match(/SRID=/)) this.Data = gm3_mpoly_to_wkt(poly);
      var name = gm3_getnamefromoverlay(poly);
      var propname = this.NodeID;
      node = document.createElement('A');
      node.id = 'Link_'+propname
      node.style.marginLeft = '5px';
      node.href = window.location.search + '#';
      node.onclick = function() {gm3_drawmap(this.id);return false;}; 
      node.appendChild(document.createTextNode(name));
      td.appendChild(node);
      node = document.createElement('INPUT');
      // changed from hidden input to text input with display:none
      node.type = 'hidden';
      /*
      node.type = 'text';
      node.style.display = 'none';
      */
      node.id = propname
      node.value = this.Data;
      if (this.Params && this.Params.Derives) {
         var derives = this.Params.Derives;
         node.onchange = function() {s2_derive(this,derives);};
         node.onblur = function() {s2_derive(this,derives);};
      }
      td.appendChild(node);
   } else {
      div = document.createElement('DIV'); 
      node = document.createElement('INPUT');
      // changed from hidden input to text input with display:none
      node.type = 'hidden';
      /*
      node.type = 'text';
      node.style.display = 'none';
      */
      node.id = this.NodeID;
      node.name = this.Name;
      if (this.Params && this.Params.Derives) {
         var derives = this.Params.Derives;
         node.onchange = function() {s2_derive(this,derives);};
         node.onblur = function() {s2_derive(this,derives);};
      }
      div.appendChild(node);
      node = document.createElement('INPUT');
      node.type = 'text';
      node.id = this.NodeID+'_Name';
      node.name = this.Name+'_name';
      node.style.width = '170px';
      node.style.border = 'none';
      div.appendChild(node);
      button = document.createElement('BUTTON');
      button.onclick = function() {
         gm3_drawmap(this.id);
         return false;
      };
      button.appendChild(document.createTextNode('Map'));
      button.className = 's2iconbutton';
      button.id = 'MAP_'+this.NodeID;
      div.appendChild(button);
      td.appendChild(div);
   }
   return td;
}
Gen_MapColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_URLColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_URLColumnData.prototype.getInterim = gen_def_getinterim;
Gen_URLColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_URLColumnData.prototype.getListDataNode = function() {
   var td,node;
   td = document.createElement('TD');
   node = document.createElement('A');
   node.href = this.Data;
   node.target = 'S2ExternalLink';  
   node.appendChild(document.createTextNode(this.Data));
   //node.appendChild(document.createTextNode('[ link ]'));
   td.appendChild(node);
   return td;
}
Gen_URLColumnData.prototype.getDataNode = gen_def_getdatanode;
Gen_URLColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_EmailColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_EmailColumnData.prototype.getInterim = gen_def_getinterim;
Gen_EmailColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_EmailColumnData.prototype.getListDataNode = gen_def_getlistdatanode;
Gen_EmailColumnData.prototype.getDataNode = function() {
   var td,node,width,pk,a,div;
   if(this.Data) {                                         
      div = document.createElement('DIV');
      node = document.createElement('A');
      node.href = 'mailto:'+this.Data;
      node.className = 's2_txtlink';
      node.style.marginRight = '10px';
      node.appendChild(document.createTextNode(this.Data));
      div.appendChild(node);
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.id = this.NodeID;
      node.name = this.NodeID;
      node.value = this.Data;
      div.appendChild(node);
      node = document.createElement('BUTTON');
      node.className = 's2iconbutton';
      node.onclick = function() {s2_editlink(this);};
      node.appendChild(document.createTextNode('Edit'));
      div.appendChild(node);
      td = document.createElement('TD');
      td.appendChild(div);         
   } else {
      node = document.createElement('INPUT');
      node.type = 'text';
      node.id = this.NodeID;
      node.name = this.NodeID;
      node.value = this.Data;
      td = document.createElement('TD');
      td.appendChild(node);
   }
   return td;
}
Gen_EmailColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_FilePathColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_FilePathColumnData.prototype.getInterim = gen_def_getinterim;
Gen_FilePathColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_FilePathColumnData.prototype.getListDataNode = function() {
   var td,width,div,node,p,input,cont,button,img,fid,fdata,isimg;
   if (!this.Params.NoList) {
      td = document.createElement('TD');
      if (this.Data) {
         fid = this.Data.replace(/.+\=/,'');
         if (fid) fdata = s2_getfiledetails(fid);
         isimg = s2_isimageurl((fid)?fdata.Name:this.Data);
         a = document.createElement('A');
         a.target = 's2doc';
         a.href = unescape(this.Data);
         a.style.marginRight = '10px';
         if (!isimg) {
            a.appendChild(document.createTextNode((fdata.Name)?fdata.Name:'Download File'));
         } else {
            img = new Image();
            img.src = this.Data;
            img.className = 's2_thumbnail';
            img.alt = fdata.Name;
            a.appendChild(img);   
         }
         td.appendChild(a);
         button = document.createElement('BUTTON');
         button.className = 's2iconbutton';
         button.id = this.NodeID+'_ShowHide';
         button.appendChild(document.createTextNode('Show'));
         button.onclick = function () {s2_showhidelink(this.id);};
         td.appendChild(button);  
      }
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.id = this.NodeID;
      node.value = this.Data;
      td.appendChild(node);
   } 
   return td;         
}
Gen_FilePathColumnData.prototype.getDataNode = function() {
   var td,width,div,node,p,input,cont,a,button,img,fid,fdata,isimg;
   td = document.createElement('TD');
   if (this.Data && this.Data != 'null') {
      fid = this.Data.replace(/.+\=/,'');
      if (fid) fdata = s2_getfiledetails(fid);
      isimg = s2_isimageurl((fid)?fdata.Name:this.Data);
      a = document.createElement('A');
      a.target = 's2doc';
      a.href = unescape(this.Data);
      a.style.marginRight = '10px';
      if (!isimg) {
         a.appendChild(document.createTextNode(fdata.Name));
      } else {
         img = new Image();
         img.src = this.Data;
         img.className = 's2_medsize';
         img.alt = fdata.Name;
         a.appendChild(img);   
      }
      td.appendChild(a);
      button = document.createElement('BUTTON');
      button.className = 's2iconbutton';
      button.id = this.NodeID+'_ShowHide';
      button.appendChild(document.createTextNode('Show'));
      button.onclick = function () {s2_showhidelink(this.id);};
      td.appendChild(button);    
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.id = this.NodeID;
      node.value = this.Data;
      td.appendChild(node);
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.id = this.NodeID+'_FileName';
      node.value = fdata.Name;
      td.appendChild(node); 
   } else {
      var inline = this.Params.Inline; 
      button = document.createElement('BUTTON');
      button.className = 's2iconbutton';
      button.id = this.NodeID+'_Choose';
      button.appendChild(document.createTextNode('Find'));
      button.onclick = function () {s2_showfiledialog(this,inline);};
      td.appendChild(button);    
   }
   return td;         
}
Gen_FilePathColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_FileColumnData() {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_FileColumnData.prototype.getInterim = gen_def_getinterim;
Gen_FileColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_FileColumnData.prototype.getListDataNode = gen_def_getlistdatanode;
Gen_FileColumnData.prototype.getDataNode = gen_def_getdatanode;
Gen_FileColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_LTColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_LTColumnData.prototype.getInterim = function() {
   var itype,obj;
   itype = this.Params.TargetType;
   obj = new Object;
   obj.IType = itype;
   id = document.getElementById('Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name).value;
   if (id && id > 0) {
      node = document.getElementById('Form-'+this.Args.Form+'_IType-'+itype+'_Action-View_Current-'+id);
      obj = new Object;
      obj.IType = itype;
      obj.Id = id;
      if (node) obj.Name = node.firstChild.nodeValue;
      return obj;
   } else return null;          
}
Gen_LTColumnData.prototype.getListHeaderNode = function() {
   var itype,th,prop;
   
   itype = this.Params.TargetType;
   var pnode,pobj,parent;
   pnode = document.getElementById('Form-1_Interface');
   if (pnode) {
      pobj = JSON.parse(pnode.value);
      parent = pobj.IType;
   }  
   var ismandatory = this.Params.Mandatory;
   var usecurrent = this.Params.Current;
   var setting;
   
   if (!this.Params.NoList && (this.Params.Show || (itype != this.Args.IType && ((!usecurrent || !(itype == parent)))))) {
      th = document.createElement('TH');
      prop = this.Name;
      if (prop.match(/_ID$/)) {
         prop = prop.replace(/^[^_]+_/i,'');
         prop = prop.replace(/_ID$/i,'');
      }
      prop = prop.replace(/\_/g,' ');
      th.appendChild(document.createTextNode(prop));
      return th;            
   } else return null;
}
Gen_LTColumnData.prototype.getListDataNode = function() {
   var node,td,text,lid;
   var itype,rules;
   itype = this.Params.TargetType;
   var pnode,pobj,parent;
   pnode = document.getElementById('Form-1_Interface');
   if (pnode) {
      pobj = JSON.parse(pnode.value);
      parent = pobj.IType;  
   }
      
   var ismandatory = this.Params.Mandatory;
   var usecurrent = this.Params.Current;
   var setting;
   var dialog = gen_getdialog();
   
   if (this.Params.NoList || !(this.Params.Show || (itype != this.Args.IType && ((!usecurrent || !(itype == parent)))))) {
   //if (!this.Params.Show && itype == this.Args.IType) {
      node = document.createElement('INPUT');
      node.type = 'hidden';
      setting = s2_getcurrent(itype);      
      if (setting) node.value = setting.Current;
      node.id = this.NodeID;
      dialog.appendChild(node); 
   /*} else if (!this.Params.Show && (usecurrent ||(parent == itype))) {
      node = document.createElement('INPUT');
      node.type = 'hidden';
      setting = s2_getcurrent(itype);
      if (setting) node.value = setting.Current;
      node.id = this.NodeID;
      dialog.appendChild(node);*/
   } else {
      text = (typeof this.Data == 'object')?this.Data.Name:this.Data;
      text = s2_jssafetext(text);
      lid = (typeof this.Data == 'object')?this.Data.Id:this.Data;
      td = document.createElement('TD');
      if (lid>0) {
         node = document.createElement('A');
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Params.TargetType+'_Action-View_Current-'+lid;
         node.href = window.location.search + '#';
         node.appendChild(document.createTextNode(text));
         node.onclick = function() {s2_itypeselect(this.id);};
         td.appendChild(node);
      } else {
         td.appendChild(document.createTextNode(''));
      }
   }
   return td;
}
   
Gen_LTColumnData.prototype.getDataNode = function() {
   var td,width;
   var node = document.createElement('INPUT');
   node.type = 'text';
   node.id = this.NodeID;
   node.name = this.NodeID;
   node.value = this.Data;
   var dialog = gen_getdialog();
   var itype;
   itype = this.Params.TargetType;
   var ismandatory = this.Params.Mandatory;
   var usecurrent = this.Params.Current;
   var setting = s2_getcurrent(itype);
   if (itype == this.Args.IType) {
      setting = s2_getcurrent(this.Args.IType);
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.value = setting.Current;
      node.id = this.NodeID;
      dialog.appendChild(node); 
   } else if (usecurrent && setting && setting.Current) {
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.value = setting.Current;
      node.id = this.NodeID;
      dialog.appendChild(node);
   } else if (this.Data && typeof this.Data == 'object' && this.Data.Id != 0) {
      
      td = document.createElement('TD');
      switch (this.Data.Id) {
      case 'Multiple': {
         var l,listdata;
         var ids = new Array();
         for(l in this.Data.ListData) {
            listdata = this.Data.ListData[l];
            text = listdata['Name'];
            text = s2_jssafetext(text);
            lid = listdata[this.Data.Key];
            ids.push(lid);
            node = document.createElement('A');
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Params.TargetType+'_Action-View_Number-'+l+'_Current-'+lid;
            node.href = window.location.search + '#';
            node.style.marginRight = '10px';
            node.appendChild(document.createTextNode(text));
            node.onclick = function() {s2_itypeselect(this.id);};
            td.appendChild(node);
         }                  
         node = document.createElement('INPUT');
         node.type = 'hidden';
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name;
         node.value = JSON.stringify(ids);
         td.appendChild(node);
         
         node = document.createElement('BUTTON');
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Params.TargetType+'_Action-List_Property-'+this.Name;
         node.appendChild(document.createTextNode('Set'));
         node.className = 's2iconbutton';
         node.style.marginRight = '5px';
         node.onclick = function() {
            s2_saveinterim(this.id);
            s2_showlist(this.id);
         };
         td.appendChild(node);
      }break;
      default: {
         text = (typeof this.Data == 'object')?this.Data.Name:this.Data;
         text = s2_jssafetext(text);
         lid = (typeof this.Data == 'object')?this.Data.Id:this.Data;
         node = document.createElement('A');
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Params.TargetType+'_Action-View_Current-'+lid;
         node.href = window.location.search + '#';
         node.style.marginRight = '10px';
         node.style.marginLeft = '4px';
         //node.innerHTML = text;
         node.appendChild(document.createTextNode(text));
         node.onclick = function() {s2_itypeselect(this.id);};
         td.appendChild(node);
         node = document.createElement('INPUT');
         node.type = 'hidden';
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Args.Property;
         node.name = text;
         node.value = lid;
         /*
         //FIGURE OUT HOW TO CALL DERIVES METHODS 
         if (this.Params.Derives) {
            var derives = this.Params.Derives;
            //node.onchange = function() {s2_derive(this,derives);};
            derived[node] = derives;
         }
         //*/
         td.appendChild(node);
         
         node = document.createElement('BUTTON');
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Params.TargetType+'_Action-List_Property-'+this.Name;
         node.appendChild(document.createTextNode('Set'));
         node.className = 's2iconbutton';
         node.onclick = function() {
            s2_saveinterim(this.id);
            s2_showlist(this.id);
         };
         td.appendChild(node);
      }break;
      }     
   } else {
      td = document.createElement('TD');
      node = document.createElement('BUTTON');
      node.id = 'Form-'+this.Args.Form+'_IType-'+this.Params.TargetType+'_Action-List_Property-'+this.Name;
      node.appendChild(document.createTextNode('Set'));
      node.className = 's2iconbutton';
      node.onclick = function() {
         s2_saveinterim(this.id);
         s2_showlist(this.id);
      };
      td.appendChild(node);
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name;
      node.name = this.Name;
      td.appendChild(node);
      
   }
   return td;
}
Gen_LTColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;

function Gen_LTMColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_LTMColumnData.prototype.getInterim = function() {
   var node,form;
   node = document.getElementById(this.NodeID);
//alert(this.Name + '\n' + node.value);
   if (node && node.value) return JSON.parse(node.value);
   else return null;            
          
}
Gen_LTMColumnData.prototype.getListHeaderNode = function() {
   return null;
}
Gen_LTMColumnData.prototype.getListDataNode = function() {
   return null;
}
Gen_LTMColumnData.prototype.getDataNode = function() {
   var node,text,lid,mobj;
   var itype,rules,li,nodes;
   itype = this.Params.TargetType;
   var ismandatory = this.Params.Mandatory;
   var usecurrent = this.Params.Current;
   var setting = s2_getcurrent(itype);
   
   td = document.createElement('TD');
   if (typeof this.Data == 'object') {
//alert('getDataNode\n' + JSON.stringify(this.Data));
      var ldata,lobj,ul,li,ids,lind;
      mobj = this.Data;
      ldata = new Array();
      if (mobj && mobj.ListData && mobj.ListData.length > 0) {
         ldata = mobj.ListData
      } else if (mobj && mobj.Id) {
         text = mobj.Name;
         lid = mobj.Id;   
         lobj = new Object();
         lobj.Name = text;
         lobj.Id = lid;
         ldata = new Array();
         ldata.push(lobj); 
      }
      ids = new Array();
      ul = document.createElement('UL');
      ul.className = 's2ul_intable'; 
      for(lind in ldata) {
         if (ldata[lind].Id > 0) {
            li = document.createElement('LI');
            li.className = 's2li_intable';
            text = ldata[lind].Name;
            lid = (ldata[lind].Id)?ldata[lind].Id:ldata[lind][mobj.Key];
            //ids.push(lid);
            node = document.createElement('A');
            node.id = 'Form-'+this.Args.Form+'_IType-'+itype+'_Action-View_Current-'+lid;
            node.href = window.location.search + '#';
            node.style.marginRight = '5px';
            //node.innerHTML = text;
            node.appendChild(document.createTextNode(text));
            node.onclick = function() {s2_itypeselect(this.id);};
            li.appendChild(node);
            button = document.createElement('BUTTON');
            button.className = 's2delbutton';
            //button.innerHTML = '&times;';
            button.appendChild(document.createTextNode('\xD7'));
            button.name = 'Remove '+text;
            button.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name+'_Action-Unset_Current-'+lid;
            button.onclick = function() {s2_removeltmdata(this);};
            li.appendChild(button);
            ul.appendChild(li);
         }
      }
      button = document.createElement('BUTTON');
      //button.innerHTML = 'Add';
      button.appendChild(document.createTextNode('Add'));
      button.className = 's2addbutton';
      button.name = 'Add data';
      button.id = 'Form-'+this.Args.Form+'_IType-'+itype+'_Property-'+this.Name+'_Action-List';
      button.onclick = function() {
         s2_saveinterim(this.id);
         s2_showlist(this.id);
      };
      td.appendChild(button);td.appendChild(ul);      
      node = document.createElement('INPUT');
      node.type = 'hidden';
      node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name;
      node.name = this.Name;
      //node.value = JSON.stringify(ids);
      node.value = JSON.stringify(mobj);
      td.appendChild(node);
   }
   return td;
}
function s2_removeltmdata(node) {
   var id = node.id;
   var args = s2_disectid(id);
   var ul,li,ind,obj,ldata;
   li = node.parentNode;
   ul = li.parentNode;
   ul.removeChild(li);
   
   id = 'Form-'+args.Form+'_IType-'+args.IType+'_Property-'+args.Property;
   node = document.getElementById(id);
   
   obj = JSON.parse(node.value);
   ldata = obj.ListData;
   var newdata = new Array();
   for (ind in ldata) if (ldata[ind].Id!=parseInt(args.Current)) newdata.push(ldata[ind]);
   obj.ListData = newdata;
   node.value = JSON.stringify(obj);
   
   return true;    
}
Gen_LTMColumnData.prototype.getSubmissionData = function() {
   var node,obj,data,ind,val;
   node = document.getElementById(this.NodeID);
   obj = JSON.parse(node.value);

   if (obj) data = obj.ListData;
//alert('getSubmissionData\n' + JSON.stringify(data));
   if (data) {
      for (ind in data) {
         val = data[ind];
         data[ind] = (typeof val == 'object')?val.Id:val;    
      }
      data = JSON.stringify(data) 
   } else data = null
   return data;   
}

function Gen_InterfaceColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_InterfaceColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_InterfaceColumnData.prototype.getListDataNode = function() {
   var td,node;
   var text,json,obj;
   if (!this.Params.NoList) {
      td = document.createElement('TD');
      if (this.Data) {
         if(typeof this.Data == 'object') {
            text = (this.Data.Name)?this.Data.Name:null;
            json = this.Data.JSON;
            if (text && json) {
               node = document.createElement('A');
               node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Args.Property+'_Current-'+this.Data.Value;
               node.href = window.location.search + '#';
               node.appendChild(document.createTextNode(text));
               node.onclick = function() {
                  var args = s2_disectid(this.id);
                  delete args.Action;
                  var id = s2_buildid(args);
                  var json = document.getElementById(id).value;
                  s2_ifaceselect(json,true);
               };
               td.appendChild(node);
               node = document.createElement('INPUT');
               node.type = 'hidden';
               node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name+'_Current-'+this.Data.Value;
               node.value = json;
               td.appendChild(node);
            } /// ? should I add a set button here               
         } else if (typeof this.Data == 'string' && this.Data != '') {   
            obj = JSON.parse(this.Data);
            text = (obj.Name)?obj.Name:null;
            json = obj.JSON;
            if (text && json) {
               node = document.createElement('A');
               node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Args.Property+'_Current-'+obj.Value;
               node.href = window.location.search + '#';
               node.appendChild(document.createTextNode(text));
               node.onclick = function() {
                  var args = s2_disectid(this.id);
                  delete args.Action;
                  var id = s2_buildid(args);
                  var json = document.getElementById(id).value;
                  s2_ifaceselect(json,true);
               };
               td.appendChild(node);
               node = document.createElement('INPUT');
               node.type = 'hidden';
               node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name+'_Current-'+obj.Value;
               node.value = json;
               td.appendChild(node);
            } /// ? should I add a set button here                                 
         }
      }
   }
   return td;
}
Gen_InterfaceColumnData.prototype.getDataNode = function() {
   var td,node,div,button;
   var text,json,obj;
   td = document.createElement('TD');
   div = document.createElement('DIV');
   div.style.padding = '0 5px 0 5px';
   if (this.Data && this.Data != '0') {
      if (typeof this.Data == 'object') {
         text = (this.Data.Name)?this.Data.Name:null;
         json = JSON.stringify(this.Data);
         if (text && json) {
            node = document.createElement('A');
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Args.Property;
            node.href = window.location.search + '#';
            node.appendChild(document.createTextNode(text));
            node.onclick = function() {
               var args = s2_disectid(this.id);
               delete args.Action;
               var id = s2_buildid(args);
               var json = document.getElementById(id).value;
               s2_ifaceselect(json,true);
            };
            div.appendChild(node);
            button = document.createElement('BUTTON');
            button.className = 's2delbutton';
            //button.innerHTML = '&times;';
            button.appendChild(document.createTextNode('\xD7'));
            button.name = 'Remove '+text;
            button.onclick = function() {s2_ifaceclear(this);};
            div.appendChild(button);
            node = document.createElement('INPUT');
            node.type = 'hidden';
            //node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name;
            node.id = this.NodeID;
            node.value = json;
            div.appendChild(node);
         } /// ? should I add a set button here
         
         var s2int = new S2DomainInterface();
         s2int.Init(this.Args.IType,this.Params.Domain,s2_getcurrentdomain(),this.Params.ValidTypes,this.Name.replace(/_/,' '));
         json = JSON.stringify(s2int);
         node = document.createElement('BUTTON');
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-Set_Property-'+this.Name;
         node.appendChild(document.createTextNode('Set'));
         node.className = 's2iconbutton';
         node.onclick = function() {
            s2_saveinterim(this.id);
            s2_ifaceselect(json);
         };
         div.appendChild(node);
      } else if (typeof this.Data == 'string' && this.Data.match(/^\{/)) {   
         obj = JSON.parse(this.Data);
         text = (obj.Name)?obj.Name:null;
         json = this.Data;
         if (text && json) {
            node = document.createElement('A');
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Name;
            node.href = window.location.search + '#';
            node.appendChild(document.createTextNode(text));
            node.onclick = function() {
               var args = s2_disectid(this.id);
               delete args.Action;
               var id = s2_buildid(args);
               var json = document.getElementById(id).value;
               s2_ifaceselect(json,true);
            };
            div.appendChild(node);
            node = document.createElement('INPUT');
            node.type = 'hidden';
            //node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name;
            node.id = this.NodeID;
            node.value = json;
            div.appendChild(node);
         } /// ? should I add a set button here                                 
      }
   } else {
      var s2int = new S2DomainInterface();
      
      s2int.Init(this.Args.IType,this.Params.Domain,s2_getcurrentdomain(),this.Params.ValidTypes,this.Name.replace(/_/,' '));
      json = JSON.stringify(s2int);
      node = document.createElement('BUTTON');
      node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-Set_Property-'+this.Name;
      node.appendChild(document.createTextNode('Set'));
      node.className = 's2iconbutton';
      node.onclick = function() {
         s2_saveinterim(this.id);
         s2_ifaceselect(json);
      };
      div.appendChild(node);
   }
   td.appendChild(div);
   return td;
}
Gen_InterfaceColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;
Gen_InterfaceColumnData.prototype.getInterim = function() {
   var node,form;
   node = document.getElementById(this.NodeID);
   if (node && node.value) return JSON.parse(node.value);
   else return null;                      
}

function Gen_TaxaColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_TaxaColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_TaxaColumnData.prototype.getListDataNode = function() {
   var td,node;
   var text,json,obj;
   td = document.createElement('TD');
   if (this.Data) {
      if (typeof this.Data == 'string' && this.Data != '') this.Data = JSON.parse(this.Data);
      switch (typeof this.Data) {
      case 'object': {
         text = (this.Data.Name)?this.Data.Name:null;
         json = this.Data.JSON;
         if (text && json) {
            node = document.createElement('A');
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Args.Property;
            node.href = window.location.search + '#';
            node.appendChild(document.createTextNode(text));
            node.onclick = function() {
               s2_shownbntaxadialog(this);
            };
            td.appendChild(node);
         } /// ? should I add a set button here               
      } break;
      case 'array': {
         var ti,tobj,sel,opt;
         for(ti in this.Data) {
            tobj = this.Data[ti];
            text = (tobj.Name)?tobj.Name:null;
            json = tobj.JSON;
            sel = document.createElement('SELECT');
            sel.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Args.Property;
            if (text && json) {
               opt = document.createElement('OPTION');
               opt.appendChild(document.createTextNode(text));
               opt.value = json;
               sel.appendChild(opt);
            }
            td.appendChild(sel);
         } /// ? should I add a set button here               
      } break;
      /*
      case 'string': {
         if (this.Data != '') {   
            obj = JSON.parse(this.Data);
            text = (obj.Name)?obj.Name:null;
            json = this.Data.JSON;
            if (text && json) {
               node = document.createElement('A');
               node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Args.Property;
               node.href = window.location.search + '#';
               node.appendChild(document.createTextNode(text));
               node.onclick = function() {
                  s2_shownbntaxadialog(this);
               };
               td.appendChild(node);
            }
         } /// ? should I add a set button here                                 
      } break;
      */
      }                                             
   }
   return td;
}
Gen_TaxaColumnData.prototype.getDataNode = function() {
   var td,node,div;
   var text,json,obj,lid;
   td = document.createElement('TD');
   div = document.createElement('DIV');
   div.style.padding = '0 5px 0 5px';
   if (this.Data) {
      if (typeof this.Data == 'object') {
         text = (this.Data.Name)?this.Data.Name:null;
         json = JSON.stringify(this.Data);
   //alert(json);
         if (text && json) {
            lid = this.Data.Value;
            node = document.createElement('A');
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Args.Property;
            node.href = window.location.search + '#';
            node.appendChild(document.createTextNode(text));
            node.onclick = function() {
               s2_shownbntaxadialog(this,this.firstChild.nodeValue);
            };
            div.appendChild(node);
            button = document.createElement('BUTTON');
            button.className = 's2delbutton';
            //button.innerHTML = '&times;';
            button.appendChild(document.createTextNode('\xD7'));
            button.name = 'Remove '+text;
            button.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name+'_Action-Unset_Current-'+lid;
            button.onclick = function() {s2_ifaceclear(this);};
            div.appendChild(button);
            node = document.createElement('INPUT');
            node.type = 'hidden';
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name;
            node.value = json;
            div.appendChild(node);
         } /// ? should I add a set button here
         node = document.createElement('BUTTON');
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-Set_Property-'+this.Name;
         node.appendChild(document.createTextNode('Set'));
         node.className = 's2iconbutton';
         node.onclick = function() {
            s2_shownbntaxadialog(this);
         };
         div.appendChild(node);
      } else if (typeof this.Data == 'string' && this.Data != '') {   
         obj = JSON.parse(this.Data);
         text = (obj.Name)?obj.Name:null;
         json = this.Data;
         if (text && json) {
            lid = this.Data.Value
            node = document.createElement('A');
            node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-View_Property-'+this.Name;
            node.href = window.location.search + '#';
            node.appendChild(document.createTextNode(text));
            node.onclick = function() {
               s2_shownbntaxadialog(this,this.firstChild.nodeValue);
            };
            div.appendChild(node);
            node = document.createElement('INPUT');
            node.type = 'hidden';
            //node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Property-'+this.Name;
            node.id = this.NodeID;
            node.value = json;
            div.appendChild(node);
         } /// ? should I add a set button here
         node = document.createElement('BUTTON');
         node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-Set_Property-'+this.Name;
         node.appendChild(document.createTextNode('Set'));
         node.className = 's2iconbutton';
         node.onclick = function() {
            s2_shownbntaxadialog(this);
         };
         div.appendChild(node);                                 
      }
   } else {
      node = document.createElement('BUTTON');
      node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-Set_Property-'+this.Name;
      node.appendChild(document.createTextNode('Set'));
      node.className = 's2iconbutton';
      node.onclick = function() {
         s2_shownbntaxadialog(this);
      };
      div.appendChild(node);
   }
   td.appendChild(div);
   return td;
}
Gen_TaxaColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;
Gen_TaxaColumnData.prototype.getInterim = function() {
   var node,form;
   node = document.getElementById(this.NodeID);
   if (node) return JSON.parse(node.value);
   else return null;            
          
}
var tvkcache = [];
function Gen_TVKColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_TVKColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_TVKColumnData.prototype.getListDataNode = function() {
   var td,node;
   var text,json,obj;
   td = document.createElement('TD');
   td.className = (this.Params.Revise)?'TVK,Revise':'TVK,NoRevise';
   if (this.Data && this.Data != '') {
      node = document.createElement('INPUT');
      node.type='text';
      //node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-Set_Property-'+this.Name;
      node.id = this.NodeID;
      node.value = this.Data;
      td.appendChild(node);
   } 
   return td;
}
Gen_TVKColumnData.prototype.getDataNode = function() {
   var td,node,div;
   var text,json,obj,lid;
   td = document.createElement('TD');
   td.className = (this.Params.Revise)?'TVK,Revise':'TVK,NoRevise';
   if (this.Data && this.Data.match(/[A-Z]{6}\d{10}/)) {
      node = document.createElement('INPUT');
      node.type='text';
      //node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-Set_Property-'+this.Name;
      node.id = this.NodeID;
      node.value = this.Data;
      td.appendChild(node);
   } else {
      div = document.createElement('DIV');
      div.style.padding = '0 5px 0 0';
      node = document.createElement('BUTTON');
      node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-Set_Property-'+this.Name;
      node.appendChild(document.createTextNode('Set'));
      node.className = 's2iconbutton';
      node.onclick = function() {
         s2_shownbntaxadialog(this);
      };
      div.appendChild(node);
      td.appendChild(div);
   }
   return td;
}
Gen_TVKColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;
Gen_TVKColumnData.prototype.getInterim = function() {
   var node,form;
   node = document.getElementById(this.NodeID);
   if (node) return node.value;//JSON.parse(node.value);
   else return null;            
          
}      
// BGS LinkedData URI
function Gen_BGSColumnData(id,name,params,data) {
   this.NodeID = id;
   this.Args = s2_disectid(id);
   this.Params = params;
   this.Name = name;
   this.Data = data;      
}
Gen_BGSColumnData.prototype.getListHeaderNode = gen_def_getlistheadernode;
Gen_BGSColumnData.prototype.getListDataNode = function() {
   var td,node,namespace;
   var text,json,obj;
   namespace = this.Params.Namespace;
   td = document.createElement('TD');
   td.className = 'BGS';
   
   node = document.createElement('INPUT');
   node.type='hidden';
   node.id = this.NodeID + '_Namespace';
   node.value = namespace;
   td.appendChild(node);
   
   if (this.Data && this.Data != '') {
      node = document.createElement('INPUT');
      node.type='text';
      node.id = this.NodeID;
      node.value = this.Data;
      td.appendChild(node);
   } 
   return td;
}
Gen_BGSColumnData.prototype.getDataNode = function() {
   var td,node,div,namespace;
   var text,json,obj,lid;
   namespace = this.Params.Namespace;
   td = document.createElement('TD');
   td.className = 'BGS';
   
   node = document.createElement('INPUT');
   node.type='hidden';
   node.id = this.NodeID + '_Namespace';
   node.value = namespace;
   td.appendChild(node);
   
   if (this.Data) {
      node = document.createElement('INPUT');
      node.type='text';
      node.id = this.NodeID;
      node.value = this.Data;
      td.appendChild(node);
   } else {
      div = document.createElement('DIV');
      div.style.padding = '0 5px 0 0';
      node = document.createElement('BUTTON');
      node.id = 'Form-'+this.Args.Form+'_IType-'+this.Args.IType+'_Action-Set_Property-'+this.Name;
      node.appendChild(document.createTextNode('Set'));
      node.className = 's2iconbutton';
      node.onclick = function() {
         s2_showbgsdialog(this);
      };
      div.appendChild(node);
      td.appendChild(div);
   }
   return td;
}
Gen_BGSColumnData.prototype.getSubmissionData = gen_def_getsubmissiondata;
Gen_BGSColumnData.prototype.getInterim = function() {
   var node,form;
   node = document.getElementById(this.NodeID);
   if (node) return node.value;
   else return null;                      
}      


      
function s2_jssafetext(text) {
   var entities,ent,pat,rep;
   if (text) {
      entities = {
         '&amp;':'\x26',
         '&quot':'\x22',
         '&#39;':'\x27',
         '&#34;':'\x22',
         '&lt;' :'\x3C',
         '&gt;' :'\x3E'
      };
      for (ent in entities) {
         rep = entities[ent];
         pat = new RegExp(ent);
         text = text.replace(pat,rep);
      }
   }
   return text;
}  
;//Paul Tero, July 2001
//http://www.tero.co.uk/des/
//
//Optimised for performance with large blocks by Michael Hayworth, November 2001
//http://www.netdealing.com
//
//THIS SOFTWARE IS PROVIDED "AS IS" AND
//ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
//FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
//OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
//HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
//LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
//OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//SUCH DAMAGE.

//des
//this takes the key, the message, and whether to encrypt or decrypt
function des (key, message, encrypt, mode, iv, padding) {
  //declaring this locally speeds things up a bit
  var spfunction1 = new Array (0x1010400,0,0x10000,0x1010404,0x1010004,0x10404,0x4,0x10000,0x400,0x1010400,0x1010404,0x400,0x1000404,0x1010004,0x1000000,0x4,0x404,0x1000400,0x1000400,0x10400,0x10400,0x1010000,0x1010000,0x1000404,0x10004,0x1000004,0x1000004,0x10004,0,0x404,0x10404,0x1000000,0x10000,0x1010404,0x4,0x1010000,0x1010400,0x1000000,0x1000000,0x400,0x1010004,0x10000,0x10400,0x1000004,0x400,0x4,0x1000404,0x10404,0x1010404,0x10004,0x1010000,0x1000404,0x1000004,0x404,0x10404,0x1010400,0x404,0x1000400,0x1000400,0,0x10004,0x10400,0,0x1010004);
  var spfunction2 = new Array (-0x7fef7fe0,-0x7fff8000,0x8000,0x108020,0x100000,0x20,-0x7fefffe0,-0x7fff7fe0,-0x7fffffe0,-0x7fef7fe0,-0x7fef8000,-0x80000000,-0x7fff8000,0x100000,0x20,-0x7fefffe0,0x108000,0x100020,-0x7fff7fe0,0,-0x80000000,0x8000,0x108020,-0x7ff00000,0x100020,-0x7fffffe0,0,0x108000,0x8020,-0x7fef8000,-0x7ff00000,0x8020,0,0x108020,-0x7fefffe0,0x100000,-0x7fff7fe0,-0x7ff00000,-0x7fef8000,0x8000,-0x7ff00000,-0x7fff8000,0x20,-0x7fef7fe0,0x108020,0x20,0x8000,-0x80000000,0x8020,-0x7fef8000,0x100000,-0x7fffffe0,0x100020,-0x7fff7fe0,-0x7fffffe0,0x100020,0x108000,0,-0x7fff8000,0x8020,-0x80000000,-0x7fefffe0,-0x7fef7fe0,0x108000);
  var spfunction3 = new Array (0x208,0x8020200,0,0x8020008,0x8000200,0,0x20208,0x8000200,0x20008,0x8000008,0x8000008,0x20000,0x8020208,0x20008,0x8020000,0x208,0x8000000,0x8,0x8020200,0x200,0x20200,0x8020000,0x8020008,0x20208,0x8000208,0x20200,0x20000,0x8000208,0x8,0x8020208,0x200,0x8000000,0x8020200,0x8000000,0x20008,0x208,0x20000,0x8020200,0x8000200,0,0x200,0x20008,0x8020208,0x8000200,0x8000008,0x200,0,0x8020008,0x8000208,0x20000,0x8000000,0x8020208,0x8,0x20208,0x20200,0x8000008,0x8020000,0x8000208,0x208,0x8020000,0x20208,0x8,0x8020008,0x20200);
  var spfunction4 = new Array (0x802001,0x2081,0x2081,0x80,0x802080,0x800081,0x800001,0x2001,0,0x802000,0x802000,0x802081,0x81,0,0x800080,0x800001,0x1,0x2000,0x800000,0x802001,0x80,0x800000,0x2001,0x2080,0x800081,0x1,0x2080,0x800080,0x2000,0x802080,0x802081,0x81,0x800080,0x800001,0x802000,0x802081,0x81,0,0,0x802000,0x2080,0x800080,0x800081,0x1,0x802001,0x2081,0x2081,0x80,0x802081,0x81,0x1,0x2000,0x800001,0x2001,0x802080,0x800081,0x2001,0x2080,0x800000,0x802001,0x80,0x800000,0x2000,0x802080);
  var spfunction5 = new Array (0x100,0x2080100,0x2080000,0x42000100,0x80000,0x100,0x40000000,0x2080000,0x40080100,0x80000,0x2000100,0x40080100,0x42000100,0x42080000,0x80100,0x40000000,0x2000000,0x40080000,0x40080000,0,0x40000100,0x42080100,0x42080100,0x2000100,0x42080000,0x40000100,0,0x42000000,0x2080100,0x2000000,0x42000000,0x80100,0x80000,0x42000100,0x100,0x2000000,0x40000000,0x2080000,0x42000100,0x40080100,0x2000100,0x40000000,0x42080000,0x2080100,0x40080100,0x100,0x2000000,0x42080000,0x42080100,0x80100,0x42000000,0x42080100,0x2080000,0,0x40080000,0x42000000,0x80100,0x2000100,0x40000100,0x80000,0,0x40080000,0x2080100,0x40000100);
  var spfunction6 = new Array (0x20000010,0x20400000,0x4000,0x20404010,0x20400000,0x10,0x20404010,0x400000,0x20004000,0x404010,0x400000,0x20000010,0x400010,0x20004000,0x20000000,0x4010,0,0x400010,0x20004010,0x4000,0x404000,0x20004010,0x10,0x20400010,0x20400010,0,0x404010,0x20404000,0x4010,0x404000,0x20404000,0x20000000,0x20004000,0x10,0x20400010,0x404000,0x20404010,0x400000,0x4010,0x20000010,0x400000,0x20004000,0x20000000,0x4010,0x20000010,0x20404010,0x404000,0x20400000,0x404010,0x20404000,0,0x20400010,0x10,0x4000,0x20400000,0x404010,0x4000,0x400010,0x20004010,0,0x20404000,0x20000000,0x400010,0x20004010);
  var spfunction7 = new Array (0x200000,0x4200002,0x4000802,0,0x800,0x4000802,0x200802,0x4200800,0x4200802,0x200000,0,0x4000002,0x2,0x4000000,0x4200002,0x802,0x4000800,0x200802,0x200002,0x4000800,0x4000002,0x4200000,0x4200800,0x200002,0x4200000,0x800,0x802,0x4200802,0x200800,0x2,0x4000000,0x200800,0x4000000,0x200800,0x200000,0x4000802,0x4000802,0x4200002,0x4200002,0x2,0x200002,0x4000000,0x4000800,0x200000,0x4200800,0x802,0x200802,0x4200800,0x802,0x4000002,0x4200802,0x4200000,0x200800,0,0x2,0x4200802,0,0x200802,0x4200000,0x800,0x4000002,0x4000800,0x800,0x200002);
  var spfunction8 = new Array (0x10001040,0x1000,0x40000,0x10041040,0x10000000,0x10001040,0x40,0x10000000,0x40040,0x10040000,0x10041040,0x41000,0x10041000,0x41040,0x1000,0x40,0x10040000,0x10000040,0x10001000,0x1040,0x41000,0x40040,0x10040040,0x10041000,0x1040,0,0,0x10040040,0x10000040,0x10001000,0x41040,0x40000,0x41040,0x40000,0x10041000,0x1000,0x40,0x10040040,0x1000,0x41040,0x10001000,0x40,0x10000040,0x10040000,0x10040040,0x10000000,0x40000,0x10001040,0,0x10041040,0x40040,0x10000040,0x10040000,0x10001000,0x10001040,0,0x10041040,0x41000,0x41000,0x1040,0x1040,0x40040,0x10000000,0x10041000);

  //create the 16 or 48 subkeys we will need
  var keys = des_createKeys (key);
  var m=0, i, j, temp, temp2, right1, right2, left, right, looping;
  var cbcleft, cbcleft2, cbcright, cbcright2
  var endloop, loopinc;
  var len = message.length;
  var chunk = 0;
  //set up the loops for single and triple des
  var iterations = keys.length == 32 ? 3 : 9; //single or triple des
  if (iterations == 3) {looping = encrypt ? new Array (0, 32, 2) : new Array (30, -2, -2);}
  else {looping = encrypt ? new Array (0, 32, 2, 62, 30, -2, 64, 96, 2) : new Array (94, 62, -2, 32, 64, 2, 30, -2, -2);}

  //pad the message depending on the padding parameter
  if (padding == 2) message += "        "; //pad the message with spaces
  else if (padding == 1) {temp = 8-(len%8); message += String.fromCharCode (temp,temp,temp,temp,temp,temp,temp,temp); if (temp==8) len+=8;} //PKCS7 padding
  else if (!padding) message += "\0\0\0\0\0\0\0\0"; //pad the message out with null bytes

  //store the result here
  result = "";
  tempresult = "";

  if (mode == 1) { //CBC mode
    cbcleft = (iv.charCodeAt(m++) << 24) | (iv.charCodeAt(m++) << 16) | (iv.charCodeAt(m++) << 8) | iv.charCodeAt(m++);
    cbcright = (iv.charCodeAt(m++) << 24) | (iv.charCodeAt(m++) << 16) | (iv.charCodeAt(m++) << 8) | iv.charCodeAt(m++);
    m=0;
  }

  //loop through each 64 bit chunk of the message
  while (m < len) {
    left = (message.charCodeAt(m++) << 24) | (message.charCodeAt(m++) << 16) | (message.charCodeAt(m++) << 8) | message.charCodeAt(m++);
    right = (message.charCodeAt(m++) << 24) | (message.charCodeAt(m++) << 16) | (message.charCodeAt(m++) << 8) | message.charCodeAt(m++);

    //for Cipher Block Chaining mode, xor the message with the previous result
    if (mode == 1) {if (encrypt) {left ^= cbcleft; right ^= cbcright;} else {cbcleft2 = cbcleft; cbcright2 = cbcright; cbcleft = left; cbcright = right;}}

    //first each 64 but chunk of the message must be permuted according to IP
    temp = ((left >>> 4) ^ right) & 0x0f0f0f0f; right ^= temp; left ^= (temp << 4);
    temp = ((left >>> 16) ^ right) & 0x0000ffff; right ^= temp; left ^= (temp << 16);
    temp = ((right >>> 2) ^ left) & 0x33333333; left ^= temp; right ^= (temp << 2);
    temp = ((right >>> 8) ^ left) & 0x00ff00ff; left ^= temp; right ^= (temp << 8);
    temp = ((left >>> 1) ^ right) & 0x55555555; right ^= temp; left ^= (temp << 1);

    left = ((left << 1) | (left >>> 31)); 
    right = ((right << 1) | (right >>> 31)); 

    //do this either 1 or 3 times for each chunk of the message
    for (j=0; j<iterations; j+=3) {
      endloop = looping[j+1];
      loopinc = looping[j+2];
      //now go through and perform the encryption or decryption  
      for (i=looping[j]; i!=endloop; i+=loopinc) { //for efficiency
        right1 = right ^ keys[i]; 
        right2 = ((right >>> 4) | (right << 28)) ^ keys[i+1];
        //the result is attained by passing these bytes through the S selection functions
        temp = left;
        left = right;
        right = temp ^ (spfunction2[(right1 >>> 24) & 0x3f] | spfunction4[(right1 >>> 16) & 0x3f]
              | spfunction6[(right1 >>>  8) & 0x3f] | spfunction8[right1 & 0x3f]
              | spfunction1[(right2 >>> 24) & 0x3f] | spfunction3[(right2 >>> 16) & 0x3f]
              | spfunction5[(right2 >>>  8) & 0x3f] | spfunction7[right2 & 0x3f]);
      }
      temp = left; left = right; right = temp; //unreverse left and right
    } //for either 1 or 3 iterations

    //move then each one bit to the right
    left = ((left >>> 1) | (left << 31)); 
    right = ((right >>> 1) | (right << 31)); 

    //now perform IP-1, which is IP in the opposite direction
    temp = ((left >>> 1) ^ right) & 0x55555555; right ^= temp; left ^= (temp << 1);
    temp = ((right >>> 8) ^ left) & 0x00ff00ff; left ^= temp; right ^= (temp << 8);
    temp = ((right >>> 2) ^ left) & 0x33333333; left ^= temp; right ^= (temp << 2);
    temp = ((left >>> 16) ^ right) & 0x0000ffff; right ^= temp; left ^= (temp << 16);
    temp = ((left >>> 4) ^ right) & 0x0f0f0f0f; right ^= temp; left ^= (temp << 4);

    //for Cipher Block Chaining mode, xor the message with the previous result
    if (mode == 1) {if (encrypt) {cbcleft = left; cbcright = right;} else {left ^= cbcleft2; right ^= cbcright2;}}
    tempresult += String.fromCharCode ((left>>>24), ((left>>>16) & 0xff), ((left>>>8) & 0xff), (left & 0xff), (right>>>24), ((right>>>16) & 0xff), ((right>>>8) & 0xff), (right & 0xff));

    chunk += 8;
    if (chunk == 512) {result += tempresult; tempresult = ""; chunk = 0;}
  } //for every 8 characters, or 64 bits in the message

  //return the result as an array
  return result + tempresult;
} //end of des



//des_createKeys
//this takes as input a 64 bit key (even though only 56 bits are used)
//as an array of 2 integers, and returns 16 48 bit keys
function des_createKeys (key) {
  //declaring this locally speeds things up a bit
  pc2bytes0  = new Array (0,0x4,0x20000000,0x20000004,0x10000,0x10004,0x20010000,0x20010004,0x200,0x204,0x20000200,0x20000204,0x10200,0x10204,0x20010200,0x20010204);
  pc2bytes1  = new Array (0,0x1,0x100000,0x100001,0x4000000,0x4000001,0x4100000,0x4100001,0x100,0x101,0x100100,0x100101,0x4000100,0x4000101,0x4100100,0x4100101);
  pc2bytes2  = new Array (0,0x8,0x800,0x808,0x1000000,0x1000008,0x1000800,0x1000808,0,0x8,0x800,0x808,0x1000000,0x1000008,0x1000800,0x1000808);
  pc2bytes3  = new Array (0,0x200000,0x8000000,0x8200000,0x2000,0x202000,0x8002000,0x8202000,0x20000,0x220000,0x8020000,0x8220000,0x22000,0x222000,0x8022000,0x8222000);
  pc2bytes4  = new Array (0,0x40000,0x10,0x40010,0,0x40000,0x10,0x40010,0x1000,0x41000,0x1010,0x41010,0x1000,0x41000,0x1010,0x41010);
  pc2bytes5  = new Array (0,0x400,0x20,0x420,0,0x400,0x20,0x420,0x2000000,0x2000400,0x2000020,0x2000420,0x2000000,0x2000400,0x2000020,0x2000420);
  pc2bytes6  = new Array (0,0x10000000,0x80000,0x10080000,0x2,0x10000002,0x80002,0x10080002,0,0x10000000,0x80000,0x10080000,0x2,0x10000002,0x80002,0x10080002);
  pc2bytes7  = new Array (0,0x10000,0x800,0x10800,0x20000000,0x20010000,0x20000800,0x20010800,0x20000,0x30000,0x20800,0x30800,0x20020000,0x20030000,0x20020800,0x20030800);
  pc2bytes8  = new Array (0,0x40000,0,0x40000,0x2,0x40002,0x2,0x40002,0x2000000,0x2040000,0x2000000,0x2040000,0x2000002,0x2040002,0x2000002,0x2040002);
  pc2bytes9  = new Array (0,0x10000000,0x8,0x10000008,0,0x10000000,0x8,0x10000008,0x400,0x10000400,0x408,0x10000408,0x400,0x10000400,0x408,0x10000408);
  pc2bytes10 = new Array (0,0x20,0,0x20,0x100000,0x100020,0x100000,0x100020,0x2000,0x2020,0x2000,0x2020,0x102000,0x102020,0x102000,0x102020);
  pc2bytes11 = new Array (0,0x1000000,0x200,0x1000200,0x200000,0x1200000,0x200200,0x1200200,0x4000000,0x5000000,0x4000200,0x5000200,0x4200000,0x5200000,0x4200200,0x5200200);
  pc2bytes12 = new Array (0,0x1000,0x8000000,0x8001000,0x80000,0x81000,0x8080000,0x8081000,0x10,0x1010,0x8000010,0x8001010,0x80010,0x81010,0x8080010,0x8081010);
  pc2bytes13 = new Array (0,0x4,0x100,0x104,0,0x4,0x100,0x104,0x1,0x5,0x101,0x105,0x1,0x5,0x101,0x105);

  //how many iterations (1 for des, 3 for triple des)
  var iterations = key.length > 8 ? 3 : 1; //changed by Paul 16/6/2007 to use Triple DES for 9+ byte keys
  //stores the return keys
  var keys = new Array (32 * iterations);
  //now define the left shifts which need to be done
  var shifts = new Array (0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0);
  //other variables
  var lefttemp, righttemp, m=0, n=0, temp;

  for (var j=0; j<iterations; j++) { //either 1 or 3 iterations
    left = (key.charCodeAt(m++) << 24) | (key.charCodeAt(m++) << 16) | (key.charCodeAt(m++) << 8) | key.charCodeAt(m++);
    right = (key.charCodeAt(m++) << 24) | (key.charCodeAt(m++) << 16) | (key.charCodeAt(m++) << 8) | key.charCodeAt(m++);

    temp = ((left >>> 4) ^ right) & 0x0f0f0f0f; right ^= temp; left ^= (temp << 4);
    temp = ((right >>> -16) ^ left) & 0x0000ffff; left ^= temp; right ^= (temp << -16);
    temp = ((left >>> 2) ^ right) & 0x33333333; right ^= temp; left ^= (temp << 2);
    temp = ((right >>> -16) ^ left) & 0x0000ffff; left ^= temp; right ^= (temp << -16);
    temp = ((left >>> 1) ^ right) & 0x55555555; right ^= temp; left ^= (temp << 1);
    temp = ((right >>> 8) ^ left) & 0x00ff00ff; left ^= temp; right ^= (temp << 8);
    temp = ((left >>> 1) ^ right) & 0x55555555; right ^= temp; left ^= (temp << 1);

    //the right side needs to be shifted and to get the last four bits of the left side
    temp = (left << 8) | ((right >>> 20) & 0x000000f0);
    //left needs to be put upside down
    left = (right << 24) | ((right << 8) & 0xff0000) | ((right >>> 8) & 0xff00) | ((right >>> 24) & 0xf0);
    right = temp;

    //now go through and perform these shifts on the left and right keys
    for (i=0; i < shifts.length; i++) {
      //shift the keys either one or two bits to the left
      if (shifts[i]) {left = (left << 2) | (left >>> 26); right = (right << 2) | (right >>> 26);}
      else {left = (left << 1) | (left >>> 27); right = (right << 1) | (right >>> 27);}
      left &= -0xf; right &= -0xf;

      //now apply PC-2, in such a way that E is easier when encrypting or decrypting
      //this conversion will look like PC-2 except only the last 6 bits of each byte are used
      //rather than 48 consecutive bits and the order of lines will be according to 
      //how the S selection functions will be applied: S2, S4, S6, S8, S1, S3, S5, S7
      lefttemp = pc2bytes0[left >>> 28] | pc2bytes1[(left >>> 24) & 0xf]
              | pc2bytes2[(left >>> 20) & 0xf] | pc2bytes3[(left >>> 16) & 0xf]
              | pc2bytes4[(left >>> 12) & 0xf] | pc2bytes5[(left >>> 8) & 0xf]
              | pc2bytes6[(left >>> 4) & 0xf];
      righttemp = pc2bytes7[right >>> 28] | pc2bytes8[(right >>> 24) & 0xf]
                | pc2bytes9[(right >>> 20) & 0xf] | pc2bytes10[(right >>> 16) & 0xf]
                | pc2bytes11[(right >>> 12) & 0xf] | pc2bytes12[(right >>> 8) & 0xf]
                | pc2bytes13[(right >>> 4) & 0xf];
      temp = ((righttemp >>> 16) ^ lefttemp) & 0x0000ffff; 
      keys[n++] = lefttemp ^ temp; keys[n++] = righttemp ^ (temp << 16);
    }
  } //for each iterations
  //return the keys we've created
  return keys;
} //end of des_createKeys



////////////////////////////// TEST //////////////////////////////

function hexToString (h) {
  var r = "";
  for (var i= (h.substr(0, 2)=="0x")?2:0; i<h.length; i+=2) {r += String.fromCharCode (parseInt (h.substr (i, 2), 16));}
  return r;
}
function stringToHex (s) {
  var r = "";
  var hexes = new Array ("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");
  for (var i=0; i<s.length; i++) {r += hexes [s.charCodeAt(i) >> 4] + hexes [s.charCodeAt(i) & 0xf];}
  return r;
}
function d2h(d) {return d.toString(16);}
function h2d(h) {return parseInt(h,16);} 
/*
var key = "this is a 24 byte key !!";
var message = "This is a test message";
var ciphertext = des (key, message, 1, 0);
document.writeln ("DES Test: " + stringToHex (ciphertext));
*/
;// communication is handled via the cdm_orequest.js library.

var key;
var iv;
function getencrypted(clear,type) {
   var ko,key,iv;
   if (type && type == 'AB') {
      ko = cdm_getexternaldatakeys();
      key = ko.ab;
      iv =  ko.bc;
   } else {
      //key = readCookie('k1');
      //iv =  readCookie('k2');
      key = s2_getstorage('k1');
      iv =  s2_getstorage('k2');
   }
   var encoded = des(key,clear,1,1,iv,0);
   encoded = stringToHex(encoded);
   return encoded;
}
function decryptit(encoded,type) {
   var ko,key,iv;
   if (type && type == 'AB') {
      ko = cdm_getexternaldatakeys();
      key = ko.ab;
      iv =  ko.bc;
   } else {
      //key = readCookie('k1');
      //iv =  readCookie('k2');
      key = s2_getstorage('k1');
      iv =  s2_getstorage('k2');
   }
   var clear = des(key,hexToString(encoded),0,1,iv,0);
   /* if it is json remove any trailing characters */
   if (clear.match(/\}/)) clear = clear.substring(0,clear.lastIndexOf('}')+1);
   return clear;
}
function getAuthJSON() {
   var JSONObject = new Object();
   JSONObject.session = s2_getcookie('PHPSESSID');
   JSONObject.username = s2_getstorage('username');
   var JSONstring = JSON.stringify(JSONObject);
   return JSONstring;
}
function testJSON() {
   var JSONstring = getJSON();
   document.getElementById('plain').value = JSONstring;
   encryptit(JSONstring);
   return true;
}
var d = s2_getstorage('cdm_client');
//var d = 'www.humber-edc.org.uk/snc';
function auth() {
	inprogress();
   var h = s2_getstorage('handshake');
   if (!h || h != 'completed') authenticate();
   else checkin();
}
function authenticate() {
   var k1 = s2_getstorage('k1');
   var s = s2_getcookie('PHPSESSID');
   var ac = new AjaxClass();
	ac.url = 'cdm_json_proxy.php';
	var params = new Object();
   params.a = 'handshake';
   params.d = s2_getstorage('cdm_client');
   params.s = s;
   params.e = getencrypted(getAuthJSON());
   if (s2_getstorage('cdm_server')!=s2_getstorage('cdm_client')) params.k = k1;
   params.target = "http://"+s2_getstorage('cdm_server')+"/cdm_server_handshake2.php";
   ac.parameters = params;
   ac.responder = authresponse;
   ac.init();
   ac.send(); 
}
function authresponse(response) {
   completed();
   //var clear = decryptit(response);
//alert('response: ' + response + '\ndecoded: ' +clear);
   //var JSONobject = JSON.parse(clear);
   var JSONobject = JSON.parse(response);
//var c1 = ""+document.cookie;   
   s2_delstorage('k1');
   s2_delstorage('k2');
   s2_delstorage('cdm_session_id');
   s2_delstorage('handshake');
//var c2 = ""+document.cookie;   
	s2_setstorage('k1',JSONobject.k1,60,true);
	s2_setstorage('k2',JSONobject.k2,60,true);
	s2_setstorage('cdm_session_id',JSONobject.cdm_session_id,60,true);
	s2_setstorage('handshake','completed',60,true);
//var c3 = ""+document.cookie;   
//alert(c1+'\n'+c2+'\n'+c3);
//alert('Handshake completed.');
//   alert('Key1:return:'+JSONobject.k1+' cookie:'+readCookie('k1')+
//         '\nKey2:return:'+JSONobject.k2+' cookie:'+readCookie('k2'));
   //request('getMethods','methods');
   //enableinterface();
}
function syncauth() {
   var k1 = s2_getstorage('k1');
   var k2 = s2_getstorage('k2');
   var s = s2_getcookie('PHPSESSID');
   var ac = new AjaxClass();
	ac.url = 'cdm_json_proxy.php';
	var params = new Object();
   params.a = 'handshake';
   params.d = s2_getstorage('cdm_client');
   params.s = s;
   params.e = getencrypted(getAuthJSON());
   if (s2_getstorage('cdm_server')!=s2_getstorage('cdm_client')) params.k = k1;
   params.target = "http://"+s2_getstorage('cdm_server')+"/cdm_server_handshake2.php";
   ac.parameters = params;
   ac.Async = false;
   ac.init();
   var response = ac.send();
   response = cdm_decrypt(response);    
   var JSONobject = JSON.parse(response);
   s2_delstorage('k1');
   s2_delstorage('k2');
   s2_delstorage('cdm_session_id');
   s2_delstorage('handshake');
	s2_setstorage('k1',JSONobject.k1,60,true);
	s2_setstorage('k2',JSONobject.k2,60,true);
	s2_setstorage('cdm_session_id',JSONobject.cdm_session_id,60,true);
	s2_setstorage('handshake','completed',60,true);
   completed();
   //enableinterface();
}

function checkin() {
   var s = readCookie('cdm_session_id');
   //var d = 'localhost/dev';
   var ac = new AjaxClass();
	ac.url = 'cdm_json_proxy.php';
	//ac.url = 'jproxy.php';
	ac.responder = checkinresponse;
   var params = new Object();
   params.a = 'checkin';
   params.d = s2_getstorage('cdm_client');
   params.s = s;
   params.e = getencrypted(getAuthJSON());
   params.target = "http://"+s2_getstorage('cdm_server')+"/cdm_server_handshake2.php";
   ac.parameters = params;
   ac.init();
   ac.send();   
}
function checkinresponse(response) {
   completed();
   //var clear = decryptit(response);
   //var JSONobject = JSON.parse(clear);
   var JSONobject = JSON.parse(response);
	if (JSONobject.status) {
	   switch(JSONobject.status) {
	   case 'Session expired': {
	      authenticate();
	   }break;
	   default: {
         //alert(JSONobject.status+'\n'+JSONobject.message );
         s2_setstorage('handshake','completed',60,true);
         enableinterface();
      }break;
      }
	} else {
	   alert('Checkin failed.');
      s2_setstorage('handshake','required',60,true);
   }
}
function cdm_clientinit() {
   var req,par,res,sid,json,jobj;
   req = new Object();
   req.target = s2_getcurrentpath()+'/../cdm_client_handshake2.php';
   par = new Object();
   par.a = 'checkin';
   par.s = s2_getcookie('PHPSESSID');
   par.k = cdm_makekey();
   //s2_setcookie('k1',par.k,60,true);
   s2_setstorage('k1',par.k,60,true);
   req.request = par;
   req.sync = true;
   
   //req.responder = cdm_repairsession;
   //var json = snc_send(req);
   
   sid = s2_getcookie('PHPSESSID');
   req.etype = 'AB';
   req.sid = sid;
   res = snc_send(req);
   json = cdm_decrypt(res,'AB');
   jobj = JSON.parse(json);
   
   //s2_setcookie('k2',jobj.k,60,true);
   //s2_setcookie('encrypt',1,20160,true);
   //s2_setcookie('handshake','required',60,true);
   s2_setstorage('k2',jobj.k,60,true);
   s2_setstorage('encrypt',1,20160,true);
   s2_setstorage('handshake','required',60,true);
   return syncauth();
}
function cdm_repairssession(json) {
   var jobj = JSON.parse(json);
   //s2_setcookie('k2',jobj.k,60,true);
   // set encryption settings to last 2 weeks
   //s2_setcookie('encrypt',1,20160,true);
   //s2_setcookie('handshake','required',60,true);
   s2_setstorage('k2',jobj.k,60,true);
   // set encryption settings to last 2 weeks
   s2_setstorage('encrypt',1,20160,true);
   s2_setstorage('handshake','required',60,true);
   syncauth();
}

function cdm_makekey() {
   var len,k,seed,ran,characters; 
   characters = new Array( 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 
                           'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 
                           'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 
                           'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                           'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                           'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 
                           'w', 'x', 'y', 'z', '0', '1', '2', '3', 
                           '4', '5', '6', '7', '8', '9');
   k = '';
   while (k.length<8) {
      ran = (Math.floor(Math.random()*(characters.length+1)) % characters.length);
      k += characters[ran];
   }
   if (k.length > 8) k = k.substring(0,8);
   return k;  
} ;//var encrypt = readCookie('encrypt');
var encrypt = s2_getstorage('encrypt');
function cdm_send(request) {
//alert('testsend');
   inprogress();
   var ac = new AjaxClass();
   ac.url = request['target'];
   ac.parameters = request['request'];
   ac.responder = cdm_response;
   ac.init();
   ac.send(); 
}
function cdm_response(e_response) {
   var s_response = cdm_decrypt(e_response);
//alert(s_response);
   var response = JSON.parse(s_response);
//alert(response.name + '\n' + response.description);
   completed();       
}
function AjaxClass() {
   this.Method = "POST" //OR "GET"
   this.Async = true; //OR false (asynchronous or synchronous call)
   this.request = null;
   this.url = null;
   this.label = null;
   this.parameters = null;
   
   this.init = function() {
      if (window.XMLHttpRequest) // if Mozilla, Safari, IE8 etc
         this.request = new XMLHttpRequest();
         
      else if (window.ActiveXObject) { // if IE6 or previous 
         try {
            this.request = new    ActiveXObject("Msxml2.XMLHTTP");
         } catch (e) {
            try {
               this.request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e){/* think of something to do here */}
         }
      }
      // get shot of cookies loaded automatically into request.
      //try {
         //this.request.channel.loadFlags |= Components.interfaces.nsIRequest.LOAD_ANONYMOUS;
         //this.request.withCredentials = false;
         
      //} catch (e) {/* think of something to do here */}
   };
   this.encodeParameters = function () {
      var prop,pstring;
      pstring = '';
      for(prop in this.parameters) {
         // if the parameter is an object or an array then use JSON.stringify 
         // to encode it.
         // otherwise it can simply be appended as is whether it's a number, 
         // string or boolean.
         // when it's decoded you might have to be careful of boolean false 
         // decoded as string "false" which PHP evaluates to true
         if ((typeof this.parameters[prop] == 'object') 
            || (typeof this.parameters[prop] == 'array')) { 
            pstring += prop + '=' + escape(JSON.stringify(this.parameters[prop])) + '&';
         } else pstring += prop + '=' + this.parameters[prop] + '&'; 
      }
      // get rid of the last & symbol
      pstring = pstring.replace(/&$/,"");
      this.parameters = pstring;
   }
   this.send = function () {
      if (this.request && this.url) {
         if (this.Async) this.request.onreadystatechange = this.handleResponse;
         this.request.open(this.Method, this.url , this.Async);
         if (this.parameters && typeof this.parameters != 'string') this.encodeParameters();
         if (this.Method == 'POST') {
            if (this.parameters) {
               this.request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
               //this.request.setRequestHeader("Content-length", this.parameters.length);
               //this.request.setRequestHeader("Connection", "close");
            }
            this.request.send(this.parameters);
         } else {
            this.url = this.url + '?' + this.parameters;
            this.request.send(null);
         }
         
      }
      if (!this.Async) return (this.label)?'$$'+this.label+'$$'+this.request.responseText:this.request.responseText;
   };
   var self = this; // To handle lose of focus
   this.handleResponse = function() {
   // handleResponse role is to check for the completion of the request and 
   // to refer the output from the request to the responder function
   // responder can then be overridden when the class is instantiated to do 
   // different things with responses to different requests.   
      if (self.request.readyState == 4 && self.request.status == 200) {
         //var response = (parseInt(readCookie('encrypt')))?cdm_decrypt(self.request.responseText):self.request.responseText;
         var response = (s2_getstorage('encrypt'))?cdm_decrypt(self.request.responseText):self.request.responseText;
         if (self.label) response = '$$'+self.label+'$$'+response;
         self.responder(response);          
      }
   };
   this.responder = append;   
}
function append(text) {
   var p = document.createElement('P');
   p.appendChild(document.createTextNode(text));
   document.body.appendChild(p);
} 
function add_eenv(request,type,svr,sid) {
//alert('add_eenv');
   var eenv = new Object();
   //var server = (svr)?svr:unescape(readCookie('cdm_server'));
   var server = (svr)?svr:unescape(s2_getstorage('cdm_server'));
   var domain = 'localhost/dev';
   domain = window.location.host;
   var paths = window.location.pathname.split('/');
   paths.pop();
   domain = domain + paths.join('/');    
   eenv.target = "http://"+server+"/eproxy.php";
   var params = new Object();
   if (request.etype) params.etype = request.etype; 
   //params.session = (sid)?sid:readCookie('cdm_session_id');
   params.session = (sid)?sid:s2_getstorage('cdm_session_id');
   params.domain = domain; 
   params.request = getencrypted(JSON.stringify(request),type);
   eenv.request = params;
   /*
     if you override the default response, GET/POST method or send a synchronous 
     request these parameters have to be included in the envelope.
   */
   if (request.responder) eenv.responder = request.responder;
   if (request.method) eenv.method = request.method;
   if (request.sync) eenv.sync = request.sync;
   return eenv;      
}                                                              
function add_jenv(request) {
//alert('add_jenv');
   var domain = 'localhost/dev';    
   domain = window.location.host;
   var paths = window.location.pathname.split('/');
   paths.pop();paths.pop();
   domain = domain + paths.join('/');    
   var jenv = new Object();
   jenv['target'] = "http://"+domain+"/jproxy.php";   
   jenv['request'] = request;
   /*
     if you override the default response, GET/POST method or send a synchronous 
     request these parameters have to be included in the envelope.
   */
   if (request.responder) jenv.responder = request.responder;
   if (request.method) jenv.method = request.method;
   if (request.sync) jenv.sync = request.sync;
   return jenv;
}
function cdm_getexternaldatakeys() {
   var k = new S2AB();
   k.cd(s2_getcookie('PHPSESSID'));
   return JSON.parse(k.bd());
}
function cdm_wrapper(request,type,svr,sid) {
   /*
   var encrypt = parseInt(readCookie('encrypt'));
   var client = unescape(readCookie('cdm_client'));
   var server = (svr)?svr:(request.svr)?request.svr:unescape(readCookie('cdm_server'));
   */
   var encrypt = s2_getstorage('encrypt');
   var client = s2_getstorage('cdm_client');
   var server = (svr)?svr:(request.svr)?request.svr:s2_getstorage('cdm_server');
   type = (type)?type:(request.etype)?request.etype:null;
   sid = (sid)?sid:(request.sid)?request.sid:null;
   if (encrypt||(type=='AB')) {
      request.request.SID = (sid)?sid:s2_getcookie('PHPSESSID');
      request = add_eenv(request,type,server,sid);
   }
   var remote = (client != server);
   if (remote) request = add_jenv(request);
   return request;      
}  
function makeqs (params) {
   var strings = new Array();
   for(prop in params) {
      strings.push(prop + '=' + params[prop]);     
   }
   return strings.join('&');  
}  
/*                         
function cdm_send(request) {
//alert('testsend');
   inprogress();
   var http = getHTTPObject();
   var url = request['target'];
   http.open("POST", url, true);
   var params = 'request='+request['request'];//makeqs(request['request']);
   //Send the proper header information along with the request
   http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
   http.setRequestHeader("Content-length", params.length);
   http.setRequestHeader("Connection", "close");

   http.onreadystatechange = function() {//Call a function when the state changes.
	  if (http.readyState == 4 && http.status == 200) {
	     //var response = (readCookie('encrypt'))?cdm_decrypt(http.responseText):http.responseText;
	     var response = (s2_getstorage('encrypt'))?cdm_decrypt(http.responseText):http.responseText;
	     cdm_response(response);
	  }
	}
	http.send(params);
}
*/
function cdm_decrypt(e_response,type) {
   //var clear = (parseInt(readCookie('encrypt'))||(type=='AB'))?unescape(decryptit(e_response,type)):e_response;
   var clear = (parseInt(s2_getstorage('encrypt'))||(type=='AB'))?unescape(decryptit(e_response,type)):e_response;
   clear = clear.replace(/[\u0000]+$/,'');
   return clear;    
}
function cdm_encrypt(clear,type) {
   var encrypted = getencrypted(clear,type);
   return encrypted;    
}
;function getScrollXY() {
  var scrOfX = 0, scrOfY = 0;
  if( typeof( window.pageYOffset ) == 'number' ) {
    //Netscape compliant
    scrOfY = window.pageYOffset;
    scrOfX = window.pageXOffset;
  } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
    //DOM compliant
    scrOfY = document.body.scrollTop;
    scrOfX = document.body.scrollLeft;
  } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
    //IE6 standards compliant mode
    scrOfY = document.documentElement.scrollTop;
    scrOfX = document.documentElement.scrollLeft;
  }
  return [ scrOfX, scrOfY ];
}
function showdialog() {
   var dims,sTop,node;
   hidebackgroundselects();
   dims = getScrollXY();
   sTop = dims[1];
   document.getElementById('main').style.zIndex = 1;
   node = document.getElementById('screen');
   node.style.marginTop = sTop+'px';
   node.style.zIndex = 2;
   node.style.display = 'block';
   node.style.backgroundColor = '#ccf';
   node = document.getElementById('dialog-container');
   node.style.display = 'block';
   node.style.marginTop = sTop+'px';
   node.style.zIndex = 3;
   return true;
}
function hidedialog() {
   showhiddenselects();
   document.getElementById('dialog-container').style.zIndex = 1;
   document.getElementById('dialog-container').style.display = 'none';
   document.getElementById('screen').style.zIndex = 2;
   document.getElementById('screen').style.display = 'none';
   document.getElementById('screen').style.backgroundColor = 'transparent';
   document.getElementById('main').style.zIndex = 3;
   cleardialog('dialog-liner');
   s2_hidetooltip();
   return true;
}
/*
   In IE6 nothing can be on top of a select so when you create a dialog you have
   to first hide all the selects in the underlying content.  
*/
function nodeisindialog(child) {
   var node = child.parentNode;  
   var indialog = false;
   while (node && node.nodeName != 'BODY') {
      if (node.id == 'dialog-liner') indialog = true;
      node = node.parentNode; 
   }
   return indialog;   
} 
function hidebackgroundselects() {
   var selects,select,si,indialog,node;
   selects = document.getElementsByTagName('SELECT');
   for(si in selects) {
      select = selects[si];
      if (select && select.nodeName == 'SELECT') {
         indialog = nodeisindialog(select);
         if (!indialog) select.style.visibility = 'hidden';
      }    
   }
   return true;
}
function showhiddenselects() {
   var selects,select,si,indialog,node;
   selects = document.getElementsByTagName('SELECT');
   for(si in selects) {
      select = selects[si];
      if (select && select.nodeName == 'SELECT') {
         indialog = nodeisindialog(select);
         if (!indialog) select.style.visibility = 'visible';
      }    
   }
   return true;
}

function inprogress() {
   var size = 100;
   var screen = document.getElementById('screen');
   var img = document.createElement('IMG');
   img.src = '../images/progress.gif';
   img.style.position = 'absolute';
   img.style.width = size+'px';
   img.style.height = size+'px';
   img.style.top = Math.floor((parseInt(screen.style.height)/2) - (size/2));
   img.style.left = Math.floor((parseInt(screen.style.width)/2) - (size/2));
   img.alt = 'Communicating with server.';
   screen.appendChild(img);
   screen.textAlign = 'center'; 
   screen.style.zIndex = 4;
   screen.style.display = 'block';
   screen.style.backgroundColor = '#fff';
   //document.getElementById('dialog-container').style.zIndex = 1;
   //document.getElementById('main').style.zIndex = 2;
   return true;   
}
function completed() {
   var screen = document.getElementById('screen');
   screen.style.zIndex = 1;
   screen.style.backgroundColor = 'transparent';
   screen.style.display = 'none';
   while (screen.childNodes.length > 0) screen.removeChild(screen.lastChild);
   //document.getElementById('dialog-container').style.zIndex = 2;
   //document.getElementById('main').style.zIndex = 3;
   return true;   
}
function resolverequirements() {
   
}
function askforrequirement() {
   
}
var mobile = false;
function Coordinate (x, y, o) {
    this.x = x;
    this.y = y;
    this.orientation = o;
}
function getsize(element) {
   var dims = new Coordinate();
   dims.x = element.style.width;
   dims.y = element.style.height;
   if (dims.x && (dims.x.indexOf('%') == -1)) dims.x = stripPX(dims.x);
   else dims.x = element.width;
   if (dims.y && (dims.y.indexOf('%') == -1)) dims.y = stripPX(dims.y);
   else dims.y = element.height;
   return dims;
}

var mobile,startx,starty,correctx,correcty;

function follow(element,e) {

// This doesn't work
   var o,mx,my,dialog;
   if (mobile) mobile = false;
   else {
      mobile = true;
      if (typeof window.event != "undefined") {
         try {
            startx = truebody().scrollLeft + window.event.clientX - element.offsetLeft;
            starty = truebody().scrollTop + window.event.clientY - element.offsetTop;
         } catch (err) {}
      } else if (typeof e != "undefined") {
         try {
            startx = e.pageX - element.offsetLeft;
            starty = e.pageY - element.offsetTop;
         } catch (err) {}
      } 
      var isize = getsize(element);
      correctx = Math.floor(isize.x/2);
      correcty = Math.floor(isize.y/2);
      dialog = element;
   }
   return true;
}
;var tables;
var lheaderwidth = 200;
var buttonwidth = 20;
var tableids = new Array();
var tabledata = new Array();
var userotate = false;

// split at row = thead / tbody  
// split at col is where row data stops being th elements and starts being td elements
var splitatcol = new Array();
var showrows = new Array();
var showcols = new Array();

var rowcursor = new Array();
var colcursor = new Array();

function table_setupmanager() {
   var table,tnum,par,div,button;
   tables = document.getElementsByTagName('TABLE');
            
   for(tnum=0;tnum<tables.length;tnum++) {
      table = tables[tnum];
      if (!table.id) table.id = 'table_'+tnum;
      tableids.push(table.id);
      if (table.className) table.className = table.className + ',managed_table';
      else table.className = 'managed_table';
      tabledata[table.id] = table.cloneNode(true);
      par = table.parentNode;
      div = document.createElement('DIV');
      div.className = 'managed_table_div';
      div.appendChild(table);
      par.appendChild(div);
      table_split(table.id);      
   }      
}

function table_split(tableid) {
   var div,node,table,thead,tbody,tr,th,td,i,j,trh,trb,img,text,cols,rows,span;
   table = document.getElementById(tableid);
   if (table.tHead && table.tBodies[0].childNodes > 10) {
      var div = table.parentNode;
      var w = (div.style.width)?parseInt(div.style.width):div.offsetWidth;
      var h = window.innerHeight;
      var tdata = tabledata[tableid];
      thead = table.tHead;
      tbody = table.tBodies[0];
      trb = tbody.rows[0];
      var split = 0;
      node = trb.cells[0];
      trh = thead.rows[0]; 
      while(node.nodeName != 'TD') {   
         trh.cells[split].style.width = lheaderwidth+'px';
         node = node.nextSibling;
         split++;
      }
      splitatcol[tableid] = split;
      rows = tbody.rows.length;
      for (i=0;i<thead.rows.length;i++) {
         trh = thead.rows[i];
         cols = trh.cells.length;
         for (j=0+split;j<trh.cells.length;j++) {
            th = trh.cells[j];
            if (userotate) {
               text = th.childNodes[0].nodeValue;
               img = new Image();
               img.src = 'http://www.yhedn.org.uk/textrotate.php?t='+text;
               img.alt = text;
               th.replaceChild(img,th.childNodes[0]);
               th.style.verticalAlign = 'middle';
               th.style.textAlign = 'center';
            }
            /*
            text = th.childNodes[0];
            span = document.createElement('SPAN');
            //span.style.display = 'block';
            span.appendChild(text);
            th.appendChild(span);
            span.className = 'rotate90';
            */
         }   
      } 
      var twidth = (table.style.width)?parseInt(table.style.width):table.offsetWidth;
      var hheight = (thead.style.height)?parseInt(thead.style.height):thead.offsetHeight;
      var bheight = (tbody.style.height)?parseInt(tbody.style.height):tbody.offsetHeight;
      //alert (w + ' vs ' + twidth + '\n' + h + ' vs ' + theight);
      var rheight = 0;
      for (i=0;i<tbody.rows.length;i++) {  
         rheight = Math.max(rheight,((tbody.rows[i].style.height)?parseInt(tbody.rows[i].style.height):tbody.rows[i].offsetHeight));
      }
      showcols[tableid] = Math.floor(((w  - (split * lheaderwidth) - (2 * buttonwidth))/ twidth) * cols);
      showrows[tableid] = Math.floor((h - hheight - (2 * buttonwidth)) / rheight);
      /*
      for (i=0;i<tbody.rows.length;i++) {
         tbody.rows[i].cells[0].style.height = rheight+'px';
      }
      */
      if (showrows[tableid]<1)showrows[tableid] = 1;
      
      //alert(showcols[tableid] + ' ' + showrows[tableid]);
      rowcursor[tableid] = 0;
      colcursor[tableid] = 0;
      for(i=0;i<thead.rows.length;i++) {
         trh = thead.rows[i];
         while(trh.cells.length > (showcols[tableid] + split)) {
            trh.removeChild(trh.lastChild);
         }
      }
      while(tbody.rows.length > showrows[tableid]) {
         tbody.removeChild(tbody.lastChild);
      } 
      for(i=0;i<tbody.rows.length;i++) {
         trb = tbody.rows[i];
         trb.style.height = rheight+'px';
         while(trb.cells.length > (showcols[tableid] + split)) {
            trb.removeChild(trb.lastChild);
         }
      } 
      div = table.parentNode;
      node = div.lastChild;
      while(node.nodeName == 'BUTTON') {
         div.removeChild(node);
         node = div.lastChild;
      }
      button = document.createElement('BUTTON');
      button.onmousedown = function() {table_scroll(this.parentNode.firstChild.id,'up');};
      button.onmouseup = table_stopscrolling;
      img = new Image();
      img.src = 'http://www.yhedn.org.uk/images/up.gif';
      img.alt = 'scroll up'; 
      button.appendChild(img);
      button.className = 'scroller';
      div.appendChild(button);
      button = document.createElement('BUTTON');
      button.onmousedown = function() {table_scroll(this.parentNode.firstChild.id,'down');};
      button.onmouseup = table_stopscrolling;
      img = new Image();
      img.src = 'http://www.yhedn.org.uk/images/down.gif';
      img.alt = 'scroll down'; 
      button.appendChild(img);
      button.className = 'scroller';
      div.appendChild(button);
      button = document.createElement('BUTTON');
      button.onmousedown = function() {table_scroll(this.parentNode.firstChild.id,'left');};
      button.onmouseup = table_stopscrolling;
      img = new Image();
      img.src = 'http://www.yhedn.org.uk/images/left.gif';
      img.alt = 'scroll left'; 
      button.appendChild(img);
      button.className = 'scroller';
      div.appendChild(button);
      button = document.createElement('BUTTON');
      button.onmousedown = function() {table_scroll(this.parentNode.firstChild.id,'right');};
      button.onmouseup = table_stopscrolling;
      img = new Image();
      img.src = 'http://www.yhedn.org.uk/images/right.gif';
      img.alt = 'scroll right'; 
      button.appendChild(img);
      button.className = 'scroller';
      div.appendChild(button);
      
      button = document.createElement('BUTTON');
      button.onclick = function() {table_unsplit(this.parentNode.parentNode.parentNode.parentNode.id);};
      button.appendChild(document.createTextNode('unsplit'));
      thead.rows[0].cells[0].appendChild(document.createElement('BR'));
      thead.rows[0].cells[0].appendChild(button);
   }
   return true;         
}

function table_unsplit(tableid) {
   var table,tdata,div,i,node,button,cell1;
   table = document.getElementById(tableid);
   tdata = tabledata[tableid].cloneNode(true);
   div = table.parentNode;
   div.replaceChild(tdata,table);
   node = div.lastChild;
   while(node.nodeName == 'BUTTON') {
      div.removeChild(node);
      node = div.lastChild;
   }
   
   button = document.createElement('BUTTON');
   button.onclick = function() {table_split(this.parentNode.firstChild.id);};
   button.appendChild(document.createTextNode('split'));
   div.appendChild(button);
   return true;
}


var scrolling;

var interval = 200;
var startinterval = 200;
var step = 0;
var startstepchange = 5
var stepchange = 5;
var scrolltable;
var scrolldirection;
function table_scroll(tableid,direction) {
   scrolltable = tableid;
   scrolldirection = direction;
   table_scroll_now();
}
function table_scroll_now() {
   step++;
   switch (scrolldirection) {
   case 'up': {
      if (rowcursor[scrolltable] > 0) {
         rowcursor[scrolltable]--;
         table_updatedata(scrolltable,false,true);
      }
   }break;
   case 'down': {
      var rows = tabledata[scrolltable].tBodies[0].rows.length;
      var show = showrows[scrolltable];
      if (rowcursor[scrolltable] < (rows-show)) {
         rowcursor[scrolltable]++;
         table_updatedata(scrolltable,false,true);
      }
   }break;
   case 'left': {
      if (colcursor[scrolltable] > 0) {
         colcursor[scrolltable]--;
         table_updatedata(scrolltable,true,false);
      }
   }break;
   case 'right': {
      var cols = tabledata[scrolltable].tHead.rows[0].cells.length;
      var show = showcols[scrolltable];
      if ((colcursor[scrolltable]+splitatcol[scrolltable]) < (cols-show)) {
         colcursor[scrolltable]++;
         table_updatedata(scrolltable,true,false);
      }
   }break;
   }
   scrolling = window.setTimeout('table_scroll_now();',interval);
   if (step >= stepchange) {
      stepchange *= 3;
      interval /= 2;
      step = 0;   
   } 
}
function table_updatedata(tableid,movex,movey) {
   var table = document.getElementById(tableid);
   var i,j,thead,tbody,trh,trb,trd,th,td,img,text,td2;
   var tdata = tabledata[tableid];
   var cursr = rowcursor[tableid];
   var cursc = colcursor[tableid];
   var showr = showrows[tableid];
   var showc = showcols[tableid];
   var split = splitatcol[tableid];
   if (movex) {
      thead = table.tHead;
      for(i=0;i<thead.rows.length;i++) {
         trh = thead.rows[i];
         trd = tdata.tHead.rows[i];
         for(j=0;j<showc;j++) {
            th = trh.cells[j+split];
            text = trd.cells[j+cursc+split].firstChild.nodeValue;
            if (userotate) {
               img = new Image();
               img.src = 'http://www.yhedn.org.uk/textrotate.php?t='+text;
               th.replaceChild(img,th.firstChild);
            } else {
               th.replaceChild(document.createTextNode(text),th.firstChild);
            }             
         }
      }   
   }
   tbody = table.tBodies[0];
   for(i=0;i<tbody.rows.length;i++) {
      trb = tbody.rows[i];
      trd = tdata.tBodies[0].rows[i+cursr];
      if (movey) {
         for(j=0;j<split;j++) {
            th = trb.cells[j];
            text = trd.cells[j].firstChild.cloneNode(true);
            th.replaceChild(text,th.firstChild);             
         }
      }
      for(j=0;j<showc;j++) {
         td = trb.cells[j+split];
         td2 = trd.cells[j+cursc+split].cloneNode(true);
         trb.replaceChild(td2,td);             
      }
   }
   return true;
}

function table_stopscrolling() {
   window.clearTimeout(scrolling);
   step = 0;
   interval = startinterval;
   stepchange = startstepchange;
   return true;
}
window.onmouseup = table_stopscrolling;;function s2_targetdate(idate,interval) {
   var mats,odate,y,m,d,dobj,p,n,t;
   mats = idate.match(/(\d{2})\/(\d{2})\/(\d{4})/);
   d = mats[1]*1;                  
   m = mats[2]*1;
   m -= 1;
   y = mats[3]*1;
   mats = interval.match(/([+,-])(\d+)(\w+)/);
   p = mats[1];
   n = mats[2]*1;
   t = mats[3].toLowerCase();
   if (p != '+') n = -n;
   switch(t) {
   case 'd': {
      d += n;
      dobj = new Date(y,m,d);
   }break;
   case 'wd': {
      dobj = new Date(y,m,d);
      dobj.addBusDays(n);   
   }break;
   case 'w': {
      d += (n*7);
      dobj = new Date(y,m,d);
   }break;
   case 'm': {
      m += n;
      dobj = new Date(y,m,d);
   }break;
   case 'y': {
      y += n;            
      dobj = new Date(y,m,d);
   }break;
   }
   odate = s2_padnum(dobj.getDate(),2)+"/"+s2_padnum(dobj.getMonth()+1,2)+"/"+s2_padnum(dobj.getFullYear(),4);
   return odate;          
}

//alert(s2_targetdate('17/08/2011','+5D'));

function s2_columnisnull(col) {
   return (col == null);
}

function s2_getcompletionstatus(tdate,cdate) {
   var status,tobj,dobj,cobj;
   dobj = new Date();            // Today's Date
   tobj = s2_readdate(tdate);    // Target Date
   if (cdate == null || cdate == "") {
      status = (tobj.getTime() >= dobj.getTime())?'In Progress':'Overdue';
   } else {
      cobj = s2_readdate(cdate); // Completion Date
      status = (cobj.getTime() <= tobj.getTime())?'Completed On Time':'Completed Late';
   }
   return status;
}
function s2_isvalid(sdate,edate) {
   var status,sobj,dobj,eobj;
   dobj = new Date();            // Today's Date
   sobj = s2_readdate(sdate);    // Target Date
   if (edate == null || edate == "") {
      status = (sobj.getTime() >= dobj.getTime())?'Future':'Valid';
   } else {
      eobj = s2_readdate(edate); // Completion Date
      status = (eobj.getTime() < dobj.getTime())?'Expired':'Valid';
   }
   return status;
}

function s2_getclosestresolution(area) {
   // areas are measured against equivalent square side length so the first
   // thing is to square root the area to get the equivalent square size.
   area = Math.sqrt(area);   
   var res = new Array(1,10,100,1000,2000,5000,10000,20000,50000,100000);
   var closest,smaller,bigger;
   var i = 0;
   while((i < res.length) && (res[i] < area)) i++;
   if (i >= res.length -1) closest = res[(res.length-1)];
   else {
      smaller = res[i-1];
      bigger = res[i];
      closest = (Math.abs(smaller - area) > Math.abs(bigger - area))?bigger:smaller;
   }   
   return closest;
}
function s2_getgr(wkt) {
//alert('GetGR: '+ wkt);
   var polygon,p,ddc,gr,ngr,ptext,pname,parea,path,zoom;
   try {
      polygon = s2_wkt_to_gmappoly(wkt);
      path = polygon.getPath().getArray();
      parea = s2gmapv3getarea(path);
      p = s2_getpolycenter(polygon);
      ddc = new DegreesDecimalCoordinate(p.lat(),p.lng());
      gr = wgs84_to_osgb36(ddc);
      gr.accuracy = s2_getclosestresolution(parea);
      gr.easting = Math.floor(gr.easting/gr.accuracy)*gr.accuracy;
      gr.northing = Math.floor(gr.northing/gr.accuracy)*gr.accuracy;
      ngr = find_gridsquare(gr);
      ptext = ngr.asString();      
   } catch(err) {
      ptext = 'Google Maps not available in offline mode'; 
   }
   return ptext;
}
function s2_getgrsquare(ngrs) {
   var polygon,p,ngr,gr,ddc,pname,parea,zoom,pobj;
   try {
      if (ngrs.match(/[a-z,A-Z]{2}\d+/)) {
         ngr = new NationalGridReference();
         ngr.fromString(ngrs);
         gr = s2_ngr_to_gr(ngr);
         points = s2_getgridsquarefromgr(gr,ngr.accuracy);
         wkt = 'POLYGON((';
         for (pcount=0;pcount<points.length;pcount++) {
            p = points[pcount];
            wkt += p.lng() + ' ' + p.lat() + ',';
         }
         p = points[0];
         wkt += p.lng() + ' ' + p.lat() + '))';
         //p = new GPolygon(points, "#FF0000", 0.8, 0.5, "#FF0000", 0.35,{"clickable":true});
         p = new google.maps.Polygon({
            paths: points,
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 0.5,
            fillColor: "#FF0000",
            fillOpacity: 0.35,
            clickable: true
         });
         //p.click = function(pt) {s2_clickmap(this,pt);};
         ptext = s2_poly_getname(p);
      }
   } catch (err) {
      ptext = 'Google Maps not available in offline mode';
      wkt = null; 
   }
   pobj = new Object();
   pobj.Name = ptext;
   pobj.Value = wkt;
   return pobj;
}
function s2_getyear(gap,from) {
   if (!gap) gap = 0;
   if (from) {
      from = parseInt(from);
   } else { 
      var d = new Date();
      from = d.getFullYear();
   }
   var y = from + parseInt(gap);
   return y;
}

function s2_getcolumnvalue(type,id,col) {
   var req,res,dec,params,param,i,current,q,rval,jobj;
   if (id) {
      req = new Object();
      req.target = s2_getcurrentpath()+'/S2_query.php';
      params = new Object();
      params.Action = 'ColumnValue';
      params.IType = type;
      params.Current = id;
      params.Column = col;
      params.CurrentSettings = s2_getcurrent();
      req.request = params;
      req.sync = true;
      res = snc_send(req);    
      res = cdm_decrypt(res);
      completed();      
      jobj = JSON.parse(res);
      rval = jobj.Value;
   }
   return rval;
}

var s2pop = new Array();
function s2_ispopulated(col) {
   var ispop; 
   ispop = (typeof s2pop == 'object' && s2pop[col])?s2pop[col]:false; 
   return ispop;
}
function s2_setpopulated(col,val) {
   if (typeof s2pop != 'object') s2pop = new Array();
   s2pop[col] = val;
} 
function s2_clrpopulated() {
   s2pop = new Array();
}

function s2_derive(formel,derives) {
   var elid,args,id,ip,d,ps,p,pc,dcol,val,method,pars,cid,colnode,icolnode,rcol,rid,rtype,rval,from;
   var cname,update=true,ispop=false,prop;
   elid = formel.id; 
   args = s2_disectid(elid);
   prop = args.Property;
   ispop = s2_ispopulated(prop);
   if (formel && ! ispop) {
      s2_setpopulated(prop,true);
      for(id in derives) {
         val = null;
         d = derives[id];
         dcol = d.Column;
         ispop = s2_ispopulated(dcol);
         cname = dcol.replace(/\_/,' ');
         args.Property = d.Column;
         cid = s2_buildid(args);
         colnode = document.getElementById(cid);
         if (ispop) update = false;
         else if (colnode.value && colnode.value != "") update = false;//confirm('Do you want to replace existing '+cname+' value?');
         if (colnode && update) {   
            method = d.Method;
            ps = d.Params;
            pc = ps.length;
            pars = new Array();
            for(ip in ps) {
               p = ps[ip];
               switch(p.Type) {
               case 'Value': {
                  if (p.For) pars.push(formel.value.substring(((p.From)?p.From:0),p.For));
                  else pars.push(formel.value);
               }break;
               case 'Static': pars.push(p.Value);break;
               case 'Column': {
                  args.Property = p.Value;
                  cid = s2_buildid(args);
                  icolnode = document.getElementById(cid);
                  if (colnode) {
                     if (p.IType) {
                        rval = s2_getcolumnvalue(p.IType,icolnode.value,p.Property);
                        rval = (rval && typeof rval == 'object')?rval.Name:rval;
                        if (rval && p.For) rval = rval.substring(((p.From)?p.From:0),p.For);  
                        pars.push(rval);            
                     } else {   
                        rval = icolnode.value;
                        if (p.For) rval = rval.substring(((p.From)?p.From:0),p.For);
                        pars.push(rval);
                     }
                  } 
               }break;
               }                              
            }
            if (pars.length == pc) {
               var expr = "val = "+method+"('"+pars.join("','")+"')"; 
               eval(expr);
            }
            if (val) {
//alert(JSON.stringify(val));
               switch(typeof val) {
               case 'object': {
                  var namenode,parent;
                  namenode = document.getElementById(colnode.id + '_Name');
                  if (namenode) namenode.value = val.Name;
                  else {
                     namenode = document.createElement('INPUT');
                     namenode.type = 'text';
                     namenode.value = val.Name;
                     namenode.style.width = '200px';
                     colnode.parentNode.insertBefore(namenode,colnode);
                  }
                  colnode.value = val.Value;
               }break;
               default: colnode.value = val; break;
               }
               if (colnode.onchange) colnode.onchange();
            }
         }                     
      }
   }        
}

function s2_getcolumnvalue(type,id,col) {
   var req,res,dec,params,param,i,current,q;
   if (id) {
      req = new Object();
      req.target = s2_getcurrentpath()+'/S2_query.php';
      params = new Object();
      params.Action = 'ColumnValue';
      params.IType = type;
      params.Current = id;
      params.Column = col;
      params.CurrentSettings = s2_getcurrent();
      req.request = params;
      req.sync = true;
      res = snc_send(req);
      res = cdm_decrypt(res);
      completed();      
      var jobj = JSON.parse(res);
      return jobj.Value;
   } else return null;
}
function s2_getdatecolumnvalue(type,id,col) {
   if (id) {
      var ymd = s2_getcolumnvalue(type,id,col);
      var dmy = ymd.replace(/(\d{4})\-(\d{2})\-(\d{2})/,'$3/$2/$1');
      return dmy;
   } else return null; 
}

function s2_buffergeom(wkt,rad,proj) {
   var poly,ptxt,wktout,jobj=null,pobj=null;
   if (wkt) {
      req = new Object();
      req.target = s2_getcurrentpath()+'/S2_query.php';
      params = new Object();
      params.Action = 'Calculate';
      params.Calculation = 'Buffer';
      params.Polygon = wkt;
      params.Buffer = rad;
      params.Native = proj;
      params.CurrentSettings = s2_getcurrent();
      req.request = params;
      req.sync = true;
      res = snc_send(req);
      res = cdm_decrypt(res);
      completed();      
      jobj = JSON.parse(res);
      if (jobj.Result) {
         wktout = jobj.Result;
         poly = s2_wkt_to_gmappoly(wktout);
         ptxt = s2_poly_getname(poly);
         pobj = new Object();
         pobj.Name = ptxt;
         pobj.Value = wktout;
      }
   }
   return pobj;
}
function s2_nextsitecode(type,col,zone) {
   var sitecode;
   if (zone) {
      req = new Object();
      req.target = s2_getcurrentpath()+'/S2_query.php';
      params = new Object();
      params.Action = 'Calculate';
      params.Calculation = 'Site_Code';
      params.IType = type;
      params.Column = col;
      params.Zone = zone;
      params.CurrentSettings = s2_getcurrent();
      req.request = params;
      req.sync = true;
      res = snc_send(req);
      res = cdm_decrypt(res);
      completed();      
      jobj = JSON.parse(res);
      if (jobj.Result) sitecode = jobj.Result;
   }
   return sitecode;   
}
function s2_nextfilecode(type,col,prefix,prefix2) {
   var filecode;
   if (prefix) {
      req = new Object();
      req.target = s2_getcurrentpath()+'/S2_query.php';
      params = new Object();
      params.Action = 'Calculate';
      params.Calculation = 'File_Code';
      params.IType = type;
      params.Column = col;
      params.Prefix = prefix;
      if (prefix2) params.Prefix += '-' + prefix2;
      params.CurrentSettings = s2_getcurrent();
      req.request = params;
      req.sync = true;
      res = snc_send(req);
      res = cdm_decrypt(res);
      completed();      
      jobj = JSON.parse(res);
      if (jobj.Result) filecode = jobj.Result;
   }
   return filecode;   
}
function s2_getcurrentversion(versionoftype,id,method,versionedtype) {
//alert(versionoftype + ' ' + id + ' ' + method + ' ' + versionedtype);
   var cver;
   if (id) {
      req = new Object();
      req.target = s2_getcurrentpath()+'/S2_query.php';
      params = new Object();
      params.Action = 'One';
      params.ParentType = versionoftype;
      params.ParentID = id;
      params.Method = method;
      params.IType = versionedtype;
      params.CurrentSettings = s2_getcurrent();
      req.request = params;
      req.sync = true;
      res = snc_send(req);
      res = cdm_decrypt(res);
      completed();      
      jobj = JSON.parse(res);
      if (jobj.ItemName && jobj.Current) cver = {"Name":jobj.ItemName,"Value":jobj.Current};
   }
   return cver;
}
;function s2_getparamlist(params) {
   var p,param,props,val;
   props = new Object();
   for(p in params) {
      param = params[p];
      val = param.Value;
      val = (param.Encrypt)?cdm_decrypt(val,"AB"):val;      
      props[param.Name] = val;
   }
   return props;   
}

function s2_getexternaldata(jobj) {
   var text = 'Retrieving data from an external source.\nThis may take a little more time.';
   alert(text);
   var exts = jobj.External;
   var sid = s2_getcookie('PHPSESSID');
   var exti,i,j,props,svr,url,req,authority,source,p,param,value;
   for(i in exts) {
      exti = exts[i];
      req = new Object();
      svr = exti.Server;
      url = exti.Target;
      req.target = 'http://'+svr+url;
      req.request = s2_getparamlist(exti.Parameters);
      if (exti.Encrypt) req.etype = "AB";
      req.sync = true;
      req.svr = svr;
      req.sid = sid;
      //req.responder = s2_drawexternaldata;
      var res = snc_send(req);
      var dec = (req.etype)?cdm_decrypt(res,req.etype):res;
      if (dec) {
         var ed = JSON.parse(dec);
         delete jobj.LinkedData;
         delete jobj.External;
         //s2_draw(JSON.stringify(jobj));
         s2_drawexternaldata(ed,exti.Name);
      }
      completed();
   }
}
function s2_drawexternaldata(jobj,section) {
   forms++
   var dialog,i,j,prop,tabname,colname,coldefs,text,ftext,input;
   var table,thead,tbody,tr,th,td,node,a,id,lid,span,h3,listitem;
   dialog = document.getElementById('incontentdialog');
   if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
   /*
   h2 = document.createElement('H2');
   text = (jobj.Title)?jobj.Title:section;
   h2.appendChild(document.createTextNode(text));
   dialog.appendChild(h2);
   */
//alert(JSON.stringify(jobj.Tables));
   shrinknodes = new Array();
   if (jobj.Status == 0) alert(unescape(jobj.Message));
   else {
      for (tabname in jobj.Tables) {
      
         text = tabname.replace(/\_/g,' ');
         h2 = document.createElement('H2');
         h2.appendChild(document.createTextNode(text));
         dialog.appendChild(h2);
         
         if (jobj.Columns[tabname]) coldefs = jobj.Columns[tabname];
         else {
            coldefs = new Array();
            for(prop in jobj.Tables[tabname][0]) coldefs[prop] = prop;
         }
         table = document.createElement('TABLE');
         table.id = 'Form-'+forms+'_IType-R6_Table-'+tabname;
         table.className = 'extform';
         thead = document.createElement('THEAD');
         tr = document.createElement('TR');
         for (prop in coldefs) {
            colname = coldefs[prop];
            th = document.createElement('TH');
            th.appendChild(document.createTextNode(colname));
            tr.appendChild(th);      
         }
         thead.appendChild(tr);
         table.appendChild(thead);
         tbody = document.createElement('TBODY');
         for (i in jobj.Tables[tabname]) {
            listitem = jobj.Tables[tabname][i];
            tr = document.createElement('TR');
            for (prop in coldefs) {
               td = document.createElement('TD');
               //td.appendChild(document.createTextNode(listitem[prop]));
               switch (typeof listitem[prop]) {
               case 'object': {
                  text = null;
                  for (j in listitem[prop]) {
                     if (!text) text = listitem[prop][j];
                     else text = text + '<br/>' + listitem[prop][j];
                  }
                  td.innerHTML = text;
               }break;
               case 'array': {
                  text = listitem[prop].join('<br/>');
                  td.innerHTML = text;                           
               }break;
               default: {
                  if (listitem[prop]) {
                     if (typeof listitem[prop] == 'string') {
                        Encoder.EncodeType = "entity";
                        text = Encoder.htmlDecode(listitem[prop]);
                     } else text = listitem[prop];
                     td.innerHTML = text;
                  }
               }break;
               }
               tr.appendChild(td);      
            }   
            tbody.appendChild(tr);  
         }
         table.appendChild(tbody);
         dialog.appendChild(table);
      }
   }
};function s2_enableextender(perms) {
   var dom = s2_getcurrentdomain();
   var perm = (perms[dom]&&perms[dom]['System Administrator'])?perms[dom]['System Administrator']:0;
   //var extendable = s2_getcookie('s2_extender');
   var extendable = s2_getstorage('s2_extender');
   var encbutton,extbutton,img;
   extbutton = document.getElementById('flip_extender');
   if (!extbutton && perm) { 
      encbutton = document.getElementById('flip_encrypt');
      extbutton = document.createElement('BUTTON');   
      extbutton.className = 'flipextend';
      extbutton.id = 'flip_extender';
      extbutton.onclick = function() {s2_flip_extender(this.id);};
      img = document.createElement('IMG');
      if (extendable>0) {
         img.src = '../images/leaf.gif' 
         img.alt = 'extendable';
      } else {
         img.src = '../images/leaf-gs.gif' 
         img.alt = 'fixed';
      }
      extbutton.appendChild(img);
      encbutton.parentNode.insertBefore(extbutton,encbutton);
   }      
}
function s2_flip_extender(buttonid) {            
   var txt,img,onoff,button;
   if (!buttonid) {
      //onoff = (!s2_getcookie('s2_extender'))?'extendable':'fixed';
      onoff = (!s2_getstorage('s2_extender'))?'extendable':'fixed';
      button = document.getElementById('flip_extender');   
   } else {
      button = document.getElementById(buttonid);
      onoff = button.firstChild.alt;
   }
   switch (onoff) {
   case 'extendable': {
      txt = 'fixed';
      img = new Image();
      img.src = '../images/leaf-gs.gif'
      img.alt = txt;
      button.replaceChild(img,button.firstChild);
      //s2_setcookie('s2_extender',0);
      s2_setstorage('s2_extender',0);
   }break;
   case 'fixed': {
      txt = 'extendable';
      img = new Image();
      img.src = '../images/leaf.gif';
      img.alt = txt;
      button.replaceChild(img,button.firstChild); 
      //s2_setcookie('s2_extender',1);
      s2_setstorage('s2_extender',1);
   }break;                    
   }
   s2_refresh();
   return true;   
}

function s2_getextenderbutton(jobj,part) {
//alert('extender button');
   var dom = s2_getcurrentdomain();
//alert(JSON.stringify(jobj.UserPermissions));
//alert(perm);
   var button,img;
   //if (s2_getcookie('s2_extender')>0) {
   if (s2_getstorage('s2_extender')>0) {
      button = document.createElement('BUTTON');
      button.className = 'inlineextender';
      button.onclick = function() {s2_getclassdefinition(jobj.IType,part);};
      img = document.createElement('IMG');
      img.src = '../images/leaf.gif';
      img.alt = 'Extend ' + jobj.IType + ' ' + part;
      button.appendChild(img);
   }
   return button;
}
function s2_callextender(req) {
   req.target = '../custom/S2_Extender.php';
   snc_send(req);
}
function s2_getclassdefinition(itype,part) {
   var req,par;
   req = new Object();
   par = new Object();
   par.Action = 'Define';
   par.IType = itype;
   par.Part = part;
   req.request = par;
   req.responder = s2_drawclassdefinition;
   s2_callextender(req);
}
function s2_drawclassdefinition(json) {
   //alert(json);
   var jobj = JSON.parse(json);
   
   var dialog,node,h2;
   snc_clearMenus('incontentdialog');   
   dialog = document.getElementById('incontentdialog');
   h2 = s2_extendheader('S2 Extender Customisation');
   dialog.appendChild(h2);
   
   node = s2_drawclasscontext(jobj);
   if(node) dialog.appendChild(node);
   
   node = s2_drawclasscolumndefs(jobj);
   if(node) dialog.appendChild(node);
   
   node = s2_drawclasspermissions(jobj);
   if(node) dialog.appendChild(node);
   
   node = s2_drawclassviewmenu(jobj);
   if(node) dialog.appendChild(node);
   else {
      node = s2_drawclassshowlist(jobj);
      if(node) dialog.appendChild(node);
   }
   
   
//snc_report_json_obj(json);
   completed();
}
function s2_extendheader(name) {
   var h2 = document.createElement('H2');
   h2.appendChild(document.createTextNode(name));
   return h2;
}
function s2_getvtable(headings) {
   var table,thead,tr,th,i;
   table = document.createElement('TABLE');
   thead = document.createElement('THEAD');
   tr = document.createElement('TR');
   for (i in headings) {
      th = document.createElement('TH');
      th.appendChild(document.createTextNode(headings[i]));
      tr.appendChild(th);
   }
   thead.appendChild(tr);
   table.appendChild(thead);
   return table;   
}
function s2_getselect(options,current) {
   var select,option,opt,val,oname;
   select = document.createElement('SELECT');
   for (opt in options) {
      val = options[opt];
      option = document.createElement('OPTION');
      option.appendChild(document.createTextNode(val));
      option.value = val;
      if (current == val) option.selected = 'selected';
      select.appendChild(option);
   }   
   return select;                         
}
function s2_drawclasscontext(jobj) {
   var rnode,h2,div,node,table,tbody,tr,th,td,input,tnode,prop;
   var props = new Array();
   props['IType'] = 'Type Name';
   props['DisplayName'] = 'Display As';
   //props['TableName'] = 'Database Table Name';
   props['Domain'] = 'Permissions Domain';
   props['InstanceOf'] = 'Instance Of';
   rnode = document.createElement('DIV');
   h2 = s2_extendheader('Context');
   rnode.appendChild(h2);
   table = document.createElement('TABLE');
   tbody = document.createElement('TBODY');
   for (prop in props) {
      tr = document.createElement('TR');
      th = document.createElement('TH');
      th.appendChild(document.createTextNode(props[prop]));
      tr.appendChild(th);
      td = document.createElement('TD');
      input = document.createElement('INPUT');
      input.type = 'text';
      input.value = jobj.Current[prop];
      td.appendChild(input);
      tr.appendChild(td);
      tbody.appendChild(tr);
   }
   table.appendChild(tbody);
   rnode.appendChild(table);
   node = document.createElement('HR');
   node.style.clear = 'both';
   rnode.appendChild(node);            
   return rnode;                                                                                                                        
}

function s2_drawclasscolumndefs(jobj) {
   var rnode,h2,table,tbody,tr,td,i,input,select,col,def,dtype,dtypeval;
   var coldefs = (jobj.Current && jobj.Current.Columns)?jobj.Current.Columns:null;
   var datatypes = jobj.DataTypes;
   rnode = document.createElement('DIV');
   h2 = s2_extendheader('Columns');
   rnode.appendChild(h2);
   table = s2_getvtable(new Array('Column Name','Data Type','Options'));
   tbody = document.createElement('TBODY'); 
   for (col in coldefs) {
      def = coldefs[col];
      dtype = (typeof def == 'object')?def.DataType:def.replace(/\:+/,'');
      switch(dtype) {
      case 'Unique Key': dtypeval = null;break;
      case 'LINKEDTO': dtypeval = 'Linked Key';break;
      case 'LINKEDTOM': dtypeval = 'Linked Key';break;
      case 'INTERFACE': dtypeval = 'Interface';break;
      default: dtypeval = dtype;break;
      }
      if (dtypeval) {
         tr = document.createElement('TR');
         td = document.createElement('TD');
         input = document.createElement('INPUT');
         input.value = col;
         td.appendChild(input);
         tr.appendChild(td);
         td = document.createElement('TD');
         select = s2_getselect(datatypes,dtypeval);
         td.appendChild(select);
         tr.appendChild(td);
         
         td = document.createElement('TD');
         btn = document.createElement('BUTTON');
         btn.className = 'inlineextender';
         btn.innerHTML = '&times;';
         td.appendChild(btn);
         
         btn = document.createElement('BUTTON');
         btn.className = 'inlineextender';
         img = document.createElement('IMG');
         img.src = '../images/arrow_u.gif';
         img.alt = 'Move Up';
         btn.appendChild(img)
         td.appendChild(btn);
         
         btn = document.createElement('BUTTON');
         btn.className = 'inlineextender';
         img = document.createElement('IMG');
         img.src = '../images/arrow_d.gif';
         img.alt = 'Move Down';
         btn.appendChild(img)
         td.appendChild(btn);
   
         tr.appendChild(td);
         tbody.appendChild(tr);
      }                
   }
   table.appendChild(tbody);
   rnode.appendChild(table);                   
   node = document.createElement('HR');
   node.style.clear = 'both';
   rnode.appendChild(node);            
   return rnode;
}

function s2_drawclasspermissions(jobj) {
   var rnode,h2,table,tbody,tr,td,i,input,select,action,perm;
   var perms = (jobj.Current && jobj.Current.Permissions)?jobj.Current.Permissions:null;
   var defperms = jobj.DefaultPermissions;
   var actions = jobj.Actions;
   rnode = document.createElement('DIV');
   h2 = s2_extendheader('Permission Requirements');
   rnode.appendChild(h2);
   table = s2_getvtable(new Array('Action','Permission','Options'));
   tbody = document.createElement('TBODY'); 
   for (action in perms) {
      perm = perms[action];
      tr = document.createElement('TR');
      td = document.createElement('TD');
      select = s2_getselect(actions,action);
      td.appendChild(select);
      tr.appendChild(td);
      td = document.createElement('TD');
      select = s2_getselect(defperms,perm);
      td.appendChild(select);
      tr.appendChild(td);
      
      td = document.createElement('TD');
      btn = document.createElement('BUTTON');
      btn.className = 'inlineextender';
      btn.innerHTML = '&times;';
      td.appendChild(btn);
      
      btn = document.createElement('BUTTON');
      btn.className = 'inlineextender';
      img = document.createElement('IMG');
      img.src = '../images/arrow_u.gif';
      img.alt = 'Move Up';
      btn.appendChild(img)
      td.appendChild(btn);
      
      btn = document.createElement('BUTTON');
      btn.className = 'inlineextender';
      img = document.createElement('IMG');
      img.src = '../images/arrow_d.gif';
      img.alt = 'Move Down';
      btn.appendChild(img)
      td.appendChild(btn);

      tr.appendChild(td);

      
      tbody.appendChild(tr);                
   }
   table.appendChild(tbody);
   rnode.appendChild(table);                   
   node = document.createElement('HR');
   node.style.clear = 'both';
   rnode.appendChild(node);            
   return rnode;
}

function s2_drawclassviewmenu(jobj) {
   var rnode,h2,table,tbody,tr,td,i,input,select,ind,view,vname,sval,btn,img,div;
   var views = (jobj.Current && jobj.Current.Views)?jobj.Current.Views:null;
   var showopts = jobj.ShowOpts;
   rnode = document.createElement('DIV');
   
   for (ind in views) {
      view = views[ind];
      vname = view.Name;
      div = document.createElement('DIV');
      div.id = 'View_Number_'+ind;
      h2 = s2_extendheader('View: '+vname);
      h2.style.paddingRight = '11px';
      btn = document.createElement('BUTTON');
      btn.className = 'inlineextender';
      btn.innerHTML = '&times;';
      h2.appendChild(btn);
      
      btn = document.createElement('BUTTON');
      btn.className = 'inlineextender';
      img = document.createElement('IMG');
      img.src = '../images/arrow_u.gif';
      img.alt = 'Move Up';
      btn.appendChild(img)
      h2.appendChild(btn);
      btn = document.createElement('BUTTON');
      btn.className = 'inlineextender';
      img = document.createElement('IMG');
      img.src = '../images/arrow_d.gif';
      img.alt = 'Move Down';
      btn.appendChild(img)
      h2.appendChild(btn);
      
      div.appendChild(h2);
   
      table = s2_getvtable(new Array('Type','Name','Show','Options'));
      tbody = document.createElement('TBODY');
       
      for (i in view.Data) {
         
         tr = document.createElement('TR');
         td = document.createElement('TD');
         input = document.createElement('INPUT');
         input.value = view.Data[i].Table;
         td.appendChild(input);
         tr.appendChild(td);
         
         td = document.createElement('TD');
         input = document.createElement('INPUT');
         input.value = (view.Data[i].Name)?view.Data[i].Name:'-- default --';
         td.appendChild(input);
         tr.appendChild(td);
         
         td = document.createElement('TD');
         sval = (view.Data[i].Show)?view.Data[i].Show:'All';
         select = s2_getselect(showopts,sval);
         td.appendChild(select);
         tr.appendChild(td);
         
         td = document.createElement('TD');
         btn = document.createElement('BUTTON');
         btn.className = 'inlineextender';
         btn.innerHTML = '&times;';
         td.appendChild(btn);
         
         btn = document.createElement('BUTTON');
         btn.className = 'inlineextender';
         img = document.createElement('IMG');
         img.src = '../images/arrow_u.gif';
         img.alt = 'Move Up';
         btn.appendChild(img)
         td.appendChild(btn);
         
         btn = document.createElement('BUTTON');
         btn.className = 'inlineextender';
         img = document.createElement('IMG');
         img.src = '../images/arrow_d.gif';
         img.alt = 'Move Down';
         btn.appendChild(img)
         td.appendChild(btn);
         
         tr.appendChild(td);
         tbody.appendChild(tr);                
      }
   
      table.appendChild(tbody);
      div.appendChild(table);
      node = document.createElement('HR');
      node.style.clear = 'both';
      div.appendChild(node);            
      rnode.appendChild(div);                      
   }
   return rnode;
}
function s2_drawclassshowlist(jobj) {
   var rnode,h2,table,tbody,tr,td,i,input,select,tab;
   var show = (jobj.Current && jobj.Current.Show)?jobj.Current.Show:null;
   
   rnode = document.createElement('DIV');
   h2 = s2_extendheader('Show Types');
   rnode.appendChild(h2);
   table = s2_getvtable(new Array('Type'));
   tbody = document.createElement('TBODY'); 
   for (tab in show) {
      tr = document.createElement('TR');
      td = document.createElement('TD');
      input = document.createElement('INPUT');
      input.value = show[tab];
      td.appendChild(input);
      tr.appendChild(td);
      tbody.appendChild(tr);                
   }
   table.appendChild(tbody);
   rnode.appendChild(table);                   
   node = document.createElement('HR');
   node.style.clear = 'both';
   rnode.appendChild(node);            
   
   return rnode;
}
 ;function s2_openclose(bt) {
   var bid = bt.id;
   var uid = bid.replace(/bt_/,'ul_');
   var ul = document.getElementById(uid);
   var li = bt.parentNode;
   switch(ul.className) {
   case 's2open': {
      ul.className = 's2closed';
      li.className = 's2closed';
   }break;
   default: {                           
      ul.className = 's2open';
      li.className = 's2open';
   }break;
   }
   return true;      
}


/*
 * Files are listed in descending date order. This is done by 
 * prepending to the containers with the h4 header prepended last 
 */
function s2_drawfilelist(files) {
   var div,a,node,y,m,d,i,yfiles,mfiles,dfiles,ifile,button,link;
   div = $('<div></div>').addClass('s2filecontainer');
   for(y in files) {
      var yli,yul;
      yfiles = files[y];
      for(m in yfiles) {
         var mli,mul;
         mfiles = yfiles[m];
         for(d in mfiles) {
            var dli,dul,dateDiv,h4;
            dateDiv = $('<div></div>').addClass('s2-upload-date');
            h4 = $('<h4></h4>').text(d+'/'+m+'/'+y);
            dfiles = mfiles[d];
            for(i in dfiles) {
               var ili,iul,url,name,ext,fileDiv;
               ifile = dfiles[i];
               url = ifile['Link'];
               fname = ifile['Name'];
               ext = fname.replace(/^.+\./,'');
               ext = ext.toLowerCase();
               /*
               switch(ext) {
               case 'csv': {fname = 'T:' +fname;}break;
               case 'xls': {fname = 'X:' +fname;}break;
               case 'mif': {fname = 'MI:'+fname;}break;
               case 'mid': {fname = 'MI:'+fname;}break;
               }
               */
               fileDiv = $('<div></div>').addClass('s2-upload-file');
               fileDiv.className = 's2file';
               a = $('<a></a>');
               a.attr('href', url);
               a.text(fname);
               fileDiv.append(a);
               button = $('<button></button>');
               button.attr('name', 'Use this file.');
               //button.innerHTML = '&rarr;';
               //button.appendChild(document.createTextNode('\u2192'));
               button.text('Use');
               button.click(function() {s2_returnfile($(this).next().val());return false});
               s2_addtooltip(button[0]);
               fileDiv.append(button);
               input = $('<input></input>');
               input.attr('type', 'hidden');
               input.val(JSON.stringify(ifile));
               fileDiv.append(input); 
               dateDiv.prepend(fileDiv);
            }
            dateDiv.prepend(h4);
            div.prepend(dateDiv);
         }
      }
   }
   return div[0];
}

var ftd;
var fbid;
function s2_getfilelist() {
   var req,par,sid,res,dec,job;
   sid = s2_getcookie('PHPSESSID');
   req = new Object();
   par = {"a":"list"};
   req.target = s2_getcurrentpath()+'/../echofile.php';
   req.request = par;
   req.etype = 'AB';
   req.sync = true;
   req.sid = sid;
   res = snc_send(req);
   dec = cdm_decrypt(res,'AB');
   //dec = res;
   try {
      job = JSON.parse(dec);
   } catch (e) {
      alert(dec);
   }
   completed();
   return job;         
}
function s2_getfiledetails(id) {
   var req,par;
   var sid = s2_getcookie('PHPSESSID');
   req = new Object();
   par = {"a":"details","i":id};
   req.target = s2_getcurrentpath()+'/../echofile.php';
   req.request = par;
   req.etype = 'AB';
   req.sync = true;
   req.sid = sid;
   var res = snc_send(req);
   var dec = cdm_decrypt(res,'AB');
   var job = JSON.parse(dec);
   completed();
   return job;         
}

function s2_showfiledialog(el,inline)  {
   var h2,node,e,divl,divr,divw;
   ftd = el.parentNode;
   fbid = el.id;
   // get the files 
   var files = s2_getfilelist(); 
   var div = s2_drawfilelist(files);
   
   var dc = document.getElementById('dialog-container');
   var dialog = document.getElementById('dialog-liner');
   
   if (inline) {
      try {
         node = document.getElementById('s2inlinefiledialog');
         while (node.childNodes.length > 0) node.removeChild(node.lastChild);
      } catch (e) {
         node = document.createElement('DIV');
         node.id = 's2inlinefiledialog';
         dialog.appendChild(node);
      }
      dialog = node;
      dialog.appendChild(document.createElement('BR'));
   } else cleardialog('dialog-liner');
   divw = document.createElement('DIV');
   divw.style.textAlign = 'center';
   divw.style.clear = 'both';
   
   divl = document.createElement('DIV');
   divl.style.display = 'inline-block';
   //divl.style.width = '500px';
   divl.style.width = '55%';
   divl.style.height = '300px';
   divl.style.overflow = 'auto';
   divl.style.border = '1px solid #ccc';
   divl.style.backgroundColor = '#eee';
   divl.style.padding = '5px';
   divl.style.margin = '2px';
      h4 = document.createElement('H4');
      h4.appendChild(document.createTextNode('Uploaded files by date'));
      divl.appendChild(h4);
      divl.appendChild(div);
   divw.appendChild(divl);
   divr = document.createElement('DIV');
   divr.style.display = 'inline-block';
   //divr.style.width = '200px';
   divr.style.width = '35%';
   divr.style.height = '300px';
   divr.style.overflow = 'auto';
   divr.style.border = '1px solid #ccc';
   divr.style.backgroundColor = '#eee';
   divr.style.padding = '5px';
   divr.style.margin = '2px';
      h4 = document.createElement('H4');
      h4.appendChild(document.createTextNode('Upload a new file'));
      divr.appendChild(h4);
      div = document.createElement('DIV');
      id = fbid.replace(/\_Choose/,'_DialogUploader');
      div.id = 'file-uploader-'+id;
      divr.appendChild(div);
   divw.appendChild(divr);
   dialog.appendChild(divw);
   s2_inituploader(id);
   showdialog();   
} 
function s2_returnfile(json) {
   var a,button,node,x,cx;
   
   var args = s2_disectid(fbid);
   while(ftd.childNodes.length > 0) ftd.removeChild(ftd.lastChild);
   var job = JSON.parse(json);
   var inline = false;
   var dc = document.getElementById('dialog-container');
   var dialog = document.getElementById('dialog-liner');
   try {
      var ifd = document.getElementById('s2inlinefiledialog');
      dialog.removeChild(ifd);
      inline = true;
   } catch (e) {
      hidedialog('dialog-liner');
   }
         
   a = document.createElement('A');
   a.target = 's2doc';
   a.href = unescape(job.Link);
   a.appendChild(document.createTextNode(job.Name));
   a.style.marginRight = '10px';
   ftd.appendChild(a);
   button = document.createElement('BUTTON');
   button.className = 's2iconbutton';
   button.id = fbid.replace(/\_Choose/,'_ShowHide');
   button.appendChild(document.createTextNode('Show'));
   button.onclick = function () {s2_showhidelink(this.id);};
   ftd.appendChild(button);    
   button = document.createElement('BUTTON');
   button.className = 's2iconbutton';
   button.id = fbid;
   button.appendChild(document.createTextNode('Find'));
   button.onclick = function () {s2_showfiledialog(this,inline);};
   ftd.appendChild(button);    
   node = document.createElement('INPUT');
   node.type = 'hidden';
   node.id = fbid.replace(/\_Choose/,'');
   node.name = args.Property; 
   node.value = job.Link;
   ftd.appendChild(node);
   node = document.createElement('INPUT');
   node.type = 'hidden';
   node.id = fbid.replace(/\_Choose/,'_FileName');
   node.value = job.Name;
   ftd.appendChild(node);
   node = document.createElement('INPUT');
   node.type = 'hidden';
   node.id = fbid.replace(/\_Choose/,'_Id');
   node.value = job.S2_File_ID;
   ftd.appendChild(node);
   s2_hidetooltip();   
}

var links = new Array();
function s2_setlink(href,node) {
   links[href] = node;
}
function s2_getlink(href) {
   var node;
   node = links[href];
   links[href] = null;
   return node;
}
function s2_showhidelink(id) {
   var button = document.getElementById(id);
   var a = button.previousSibling;
   if (button.firstChild.nodeValue == 'Show') {
      s2_setlink(a.href,a.firstChild);
      a.replaceChild(document.createTextNode(a.href),a.firstChild);
      button.replaceChild(document.createTextNode('Hide'),button.firstChild);
   } else {
      //var fnid = id.replace(/\_ShowHide/,'_FileName');
      //var fnode = document.getElementById(fnid);
      //var fname = (fnode)?fnode.value:'Download';  
      //a.replaceChild(document.createTextNode(fname),a.firstChild);
      a.replaceChild(s2_getlink(a.href),a.firstChild);
      button.replaceChild(document.createTextNode('Show'),button.firstChild);
   } 
   return true;
}
function s2_isimageurl(url) {
   var isimg = getQueryVariable('isimg',url);
   var ext,i;
   var imgtypes = ["jpg","jpeg","gif","png"];
   if (!isimg) {
      ext = url.replace(/.+\.(.+$)/,"$1");
      for (i in imgtypes) if (ext.toLowerCase() == imgtypes[i]) isimg = 1;    
   }
   return isimg;
}


function s2_inituploader(upload) {
   var uploader,node;
   node = document.getElementById('file-uploader-'+upload);
   if (node) {
      uploader = new qq.FileUploader({
         element: node,
         action: '../echofile.php',
         allowedExtensions: ["jpg","gif","png","doc","docx","csv","xls","xlsx","ppt","txt","pdf","eml","mid","mif","kmz","kml"],
         debug: false,
         onComplete: function(id, fileName, responseJSON){s2_linkuploader(this.element.id,fileName,responseJSON);}
      });           
   }  
}
function s2_linkuploader(id,file,jsonobj) {
   var div,a,img,node,url,p,input,name,button;
   node = document.getElementById(id);
   var qqu = node.lastChild;
   var qqul = qqu.lastChild;
   var qqi = 0;
   var qqli,qqname;
   while (qqi < qqul.childNodes.length) {
      qqli = qqul.childNodes[qqi];
      qqi++;
      qqname = qqli.firstChild.firstChild.nodeValue;
      if (jsonobj.data.Name == qqname) {
         if (jsonobj.image && jsonobj.image != 'false') {
            a = document.createElement('A');
            a.className = 's2_imglink';
            a.target = 's2doc';
            a.href = jsonobj.data.Link;
            img = new Image();
            img.src = jsonobj.data.Link;
            img.style.width = '120px';
            a.appendChild(img);
            qqli.appendChild(a);
            p = document.createElement('P');
            p.appendChild(document.createTextNode(jsonobj.data.Link));
            qqli.appendChild(p);
         } else {
            a = document.createElement('A');
            a.className = 's2_txtlink';
            a.target = 's2doc';
            a.href = jsonobj.data.Link;
            a.appendChild(document.createTextNode(jsonobj.data.Link));
            a.style.marginRight = '10px';
            qqli.appendChild(a);
         }
         button = document.createElement('BUTTON');
         button.name = 'Use this file.';
         //button.innerHTML = '&rarr;';
         //button.appendChild(document.createTextNode('\u2192'));
         button.appendChild(document.createTextNode('Use'));
         button.onclick = function() {s2_returnfile(this.nextSibling.value);return false};
         s2_addtooltip(button);
         qqli.appendChild(button);
         input = document.createElement('INPUT');
         input.type = 'hidden';
         input.value = JSON.stringify(jsonobj.data);
         qqli.appendChild(input);
      }      
   }
   return true;
}
   
;/*
   IMPORT PROCESS
   
      Choose file type - Return file type
      Choose file(s) - Return file id(s)
      Match columns -
         Choose type of match for each column
            Column in input matches column in table
            Column in input matches a column in linked table
            Same value select from available
            Static value enter a value for all  
         Set match for each column  
         Returns interface with columns matched
         
     The server interactions are to collate the data from the import file(s),
     execute the import and review the success of the process.            
*/
function s2_showimportinterface(id) {
   var args = s2_disectid(id);
   var id = 'Form-'+args.Form+'_Interface';
   var ifaceobj = JSON.parse(document.getElementById(id).value);
   s2_drawimportform_choosetype(ifaceobj);
}

function s2_getimporttypenode(id,prop) {
   var td, select, option, oi;
   var options = new Array('CSV','XLS','MIF');
   td = document.createElement('TD');
   select = document.createElement('SELECT');
   select.id = id;
   select.name = prop;
   for (oi=0;oi<options.length;oi++) {
      option = document.createElement('OPTION');
      option.value = options[oi];
      option.appendChild(document.createTextNode(options[oi]));
      select.appendChild(option);        
   }
   td.appendChild(select);
   return td;   
}

function s2_initimportuploader(upload) {
   var uploader,node;
   node = document.getElementById('file-uploader-'+upload);
   if (node) {
      uploader = new qq.FileUploader({
         element: node,
         action: '../echofile_v1_1.php',
         allowedExtensions: ["csv","xls","mid","mif"],
         debug: false,
         onComplete: function(id, fileName, responseJSON){s2_linkimportuploader(node.id,fileName,responseJSON);}
      });           
   }  
}

function s2_linkimportuploader(id,file,jsonobj) {
   var div,a,img,node,url,p,input,name,button;
   node = document.getElementById(id);
   div = document.createElement('DIV');
   name = id.replace(/file\-uploader\-/,'');
   div.id = id;
   url = unescape(jsonobj.url);
   var args = s2_disectid(name);
   a = document.createElement('A');
   a.className = 's2_txtlink';
   a.target = jsonobj.file;
   a.href = url;
   a.appendChild(document.createTextNode('Download File'));
   a.style.marginRight = '10px';
   div.appendChild(a);
   button = document.createElement('BUTTON');
   button.className = 's2iconbutton';
   button.id = id+'_ShowHide';
   button.appendChild(document.createTextNode('Show'));
   button.onclick = function () {s2_showhidelink(this.id);};
   div.appendChild(button);

   input = document.createElement('INPUT');
   input.id = name;
   input.name = name;
   input.value = url;
   input.type = 'hidden';
   div.appendChild(input); 
   node.parentNode.replaceChild(div,node);

   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);     
   var iset = JSON.parse(iidnode.value);
   if (!iset.Files) iset.Files = new Object();
   iset.Files[args.Type] = jsonobj.id;
   iidnode.value = JSON.stringify(iset);
   //alert(JSON.stringify(jsonobj));
   // this should create the url and replace the uploader with a url parameter 
   // for the database. 
   return true;
}
function s2_drawimportform_linkfiles(args) {
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);     
   var iset = JSON.parse(iidnode.value);
   if (!iset.Files) iset.Files = new Object();
   var type = iset.ImportType.toLowerCase();
   var node,url,nid,fid,qs;
   switch(type) {
   case 'csv': {
      nid = 'Form-'+args.Form+'_IType-'+args.IType+'_Property-CSV_File_Type-csv_Id';
      fid = document.getElementById(nid).value;
      iset.Files['csv'] = fid;
   }break;
   case 'xls': {
      nid = 'Form-'+args.Form+'_IType-'+args.IType+'_Property-Excel_File_Type-xls_Id';
      fid = document.getElementById(nid).value;
      iset.Files['xls'] = fid;
   }break;
   case 'mif': {
      nid = 'Form-'+args.Form+'_IType-'+args.IType+'_Property-MIF_File_Type-mif_Id';
      fid = document.getElementById(nid).value;
      iset.Files['mif'] = fid;
      nid = 'Form-'+args.Form+'_IType-'+args.IType+'_Property-MID_File_Type-mid_Id';
      fid = document.getElementById(nid).value;
      iset.Files['mid'] = fid;
   }break;
   }
   iidnode.value = JSON.stringify(iset);
   return true;
}

function s2_drawimportform_choosetype(jobj) {
   forms++;
   var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input,datatype;
   var table,thead,tbody,tr,th,td,node,text,id,lid,action,data,params,uploaders;
   var interfacetype,
   action = 'Import';
   dialog = document.getElementById('dialog-liner');
   if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
   if (!jobj.Data || jobj.Interim == true) {
      var h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode(action+' '+jobj.DisplayName));
      dialog.appendChild(h2);
   } 
   table = document.createElement('TABLE');
   table.id = action+'_'+jobj.IType;
   table.className = 'addform';
   tbody = document.createElement('TBODY');
   uploaders = new Array();
   
   prop = 'Import_Type';
   id = 'Form-'+forms+'_IType-'+jobj.IType+'_Property-'+prop;
   td = s2_getimporttypenode(id,prop)
   tr = document.createElement('TR')
   th = document.createElement('TH');
   th.className = 'left';
   th.appendChild(document.createTextNode(prop.replace(/\_/g,' ')));
   tr.appendChild(th);
   tr.appendChild(td);
   tbody.appendChild(tr);
   
   table.appendChild(tbody);                  
   dialog.appendChild(table);

   delete jobj.ListData;
   delete jobj.Page;
   delete jobj.Pages;
   delete jobj.Versions;
   delete jobj.Count;
   delete jobj.IsAllowedTo;
   for (i in jobj.Interface) {
      if (jobj.Interface[i].Current) delete jobj.Interface[i].Current;
   }
         
   node = document.createElement('INPUT');
   node.id = 'Form-'+forms+'_ImportSettings';
   node.type = 'hidden';
   node.value = JSON.stringify(jobj);
   dialog.appendChild(node);
   
   button = document.createElement('BUTTON');
   button.onclick = function() {s2_drawimportform_loaddata(this.id);};
   button.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Action-Import_Stage-Type';
   button.appendChild(document.createTextNode('Choose Files'));
   dialog.appendChild(button);  
    
   showdialog();
   for (var i in uploaders) s2_initimportuploader(uploaders[i]);
   
   //s2_debugjson(node.value);   
   return true;
}                      

function s2_drawimportform_loaddata(buttonid) {
   //forms++;
   var args = s2_disectid(buttonid);
   var typeid = 'Form-'+args.Form+'_IType-'+args.IType+'_Property-Import_Type';
   var type = document.getElementById(typeid).value.toLowerCase();
   
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);     
   var iset = JSON.parse(iidnode.value);
   iset.ImportType = type;
   iidnode.value = JSON.stringify(iset);
   
   var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input,datatype,div,p,text;
   var table,thead,tbody,tr,th,td,node,text,id,lid,action,data,params,uploaders;
   var interfacetype,
   action = 'Import';
   dialog = document.getElementById('dialog-liner');
   dialog.removeChild(document.getElementById(buttonid));
   var h2 = document.createElement('H2');
   h2.appendChild(document.createTextNode('Choose Import Files'));
   dialog.appendChild(h2);
   table = document.createElement('TABLE');
   table.id = action+'_'+args.IType;
   table.className = 'addform';
   tbody = document.createElement('TBODY');
   uploaders = new Array();
   switch (type) {
   case 'csv': {
      prop = 'CSV_File';
      id = 'Form-'+forms+'_IType-'+args.IType+'_Property-'+prop+'_Type-csv';
      params = JSON.parse('{"DataType":"File Path","Mandatory":1,"Inline":1}');
      datatype = 'File Path';
      td = s2_getnode(id,prop,params,data,'View')
      tr = document.createElement('TR')
      th = document.createElement('TH');
      th.className = 'left';
      th.appendChild(document.createTextNode(prop.replace(/\_/g,' ')));
      tr.appendChild(th);
      tr.appendChild(td);
      tbody.appendChild(tr);
      //uploaders.push(id);
   }break;
   case 'xls': {
      prop = 'Excel_File';
      id = 'Form-'+forms+'_IType-'+args.IType+'_Property-'+prop+'_Type-xls';
      params = JSON.parse('{"DataType":"File Path","Mandatory":1,"Inline":1}');
      datatype = 'File Path';
      td = s2_getnode(id,prop,params,data,'View')
      tr = document.createElement('TR')
      th = document.createElement('TH');
      th.className = 'left';
      th.appendChild(document.createTextNode(prop.replace(/\_/g,' ')));
      tr.appendChild(th);
      tr.appendChild(td);
      tbody.appendChild(tr);
      //uploaders.push(id);
   }break;
   case 'mif': {
      div = s2_ihelpdiv();
      text = new Array();
      text.push('Please ensure that your MID MIF file projection is Latitude Longitude WGS84 before importing.');
      text.push('To do this open the table and choose File - Save Copy As');
      text.push('Then choose a different name eg <Table Name>_WGS84.TAB and click projection to change the projection.');
      text.push('Choose Latitude Longitude and then scroll to the bottom of the list and choose WGS84.');
      text.push('Finally open the new table and choose Table Export to Export to MID MIF.');
      for(i in text) {
         p = document.createElement('P');
         p.appendChild(document.createTextNode(text[i]));
         div.appendChild(p);
      }
      dialog.appendChild(div);
      
      prop = 'MIF_File';
      id = 'Form-'+forms+'_IType-'+args.IType+'_Property-'+prop+'_Type-mif';
      params = JSON.parse('{"DataType":"File Path","Mandatory":1,"Inline":1}');
      datatype = 'File Path';
      td = s2_getnode(id,prop,params,data,'View')
      tr = document.createElement('TR')
      th = document.createElement('TH');
      th.className = 'left';
      th.appendChild(document.createTextNode(prop.replace(/\_/g,' ')));
      tr.appendChild(th);
      tr.appendChild(td);
      tbody.appendChild(tr);
      uploaders.push(id);
   
      prop = 'MID_File';
      id = 'Form-'+forms+'_IType-'+args.IType+'_Property-'+prop+'_Type-mid';
      params = JSON.parse('{"DataType":"File Path","Mandatory":1,"Inline":1}');
      datatype = 'File Path';
      td = s2_getnode(id,prop,params,data,'View')
      tr = document.createElement('TR')
      th = document.createElement('TH');
      th.className = 'left';
      th.appendChild(document.createTextNode(prop.replace(/\_/g,' ')));
      tr.appendChild(th);
      tr.appendChild(td);
      tbody.appendChild(tr);
      uploaders.push(id);
   }break;   
   }
   table.appendChild(tbody);
   dialog.appendChild(table);
   button = document.createElement('BUTTON');
   button.onclick = function() {s2_drawimportform_getheadings(this.id);};
   button.id = 'Form-'+args.Form+'_IType-'+args.IType+'_Action-Import_Stage-Match';
   button.appendChild(document.createTextNode('Match Columns'));
   dialog.appendChild(button);   
      
   //s2_debugjson(iidnode.value);   
   //showdialog();
   for (var i in uploaders) s2_initimportuploader(uploaders[i]);
   return true;
}
function s2_importcheckfiles(jobj) {
   
}

function s2_imatchselect(id,headings,params) {
   var td,select,option,value,ih,hx;
   td = document.createElement('TD');
   var dtype = (params.DataType)?params.DataType:params;
   var args = s2_disectid(id);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);
   var jobj = JSON.parse(iidnode.value);
   var match;
   switch(dtype) {
   case 'Unique Key':   {td = null;} break;
   case 'LINKEDTOM':    {td = null;} break;
   case 'LINKEDTO': {
      select = document.createElement('SELECT');
      select.id = id + '_TType-' + params.TargetType;
      select.className = 'MatchType_IColumn';
      select.onchange = function() {s2_isetmatchoption(this)};
      if (!jobj.Matches) jobj.Matches = new Object();
      if (!jobj.Matches[args.Property]) match = new Object();
      else match = jobj.Matches[args.Property];
      match.Type = 'Link';       
      var first = true;
      for(ih in headings) {
         hx = headings[ih];
         option = document.createElement('OPTION');
         option.value = hx;
         option.appendChild(document.createTextNode(hx));
         select.appendChild(option);
         if (first) {
            option.selected = 'selected';
            first = false;
            //s2_isetmatchoption(select);
            match.IColumn = hx;
         }
               
      }
      option = document.createElement('OPTION');
      option.value = 'Choose';
      option.appendChild(document.createTextNode('* Choose Existing'));
      select.appendChild(option);
      option = document.createElement('OPTION');
      option.value = 'Derive';
      option.appendChild(document.createTextNode('* Derive'));
      select.appendChild(option);
      option = document.createElement('OPTION');
      option.value = 'None';
      option.appendChild(document.createTextNode('* None'));
      select.appendChild(option);
      td.appendChild(select);
      // get columns from linked table to match
      
      var req = new Object();
      var par = new Object();
      par.Action = 'List';
      par.IType = params.TargetType;
      par.CurrentSettings = s2_getcurrent();
      req.target = 'S2_query.php';
      req.sync = true;
      req.request = par;
      var resp = snc_send(req);
      completed();
      showdialog();
      var tint = JSON.parse(resp);
      delete tint.Alternatives;
      delete tint.Views;
      delete tint.ViewName;
      delete tint.Current;
      delete tint.IsAllowedTo;
      delete tint.UserPermissions;
      
      if (!jobj.Links) jobj.Links = new Object();
      jobj.Links[params.TargetType] = tint;
      
      select = document.createElement('SELECT');
      select.id = id;
      select.className = 'MatchType_TColumn';
      select.onchange = function() {s2_isetmatchoption(this)};
      var first = true;
      for (ih in tint.Interface) {
         hx = tint.ListData[ih];
         option = document.createElement('OPTION');
         option.value = ih;
         option.appendChild(document.createTextNode(ih));
         select.appendChild(option);
         if (first) {
            option.selected = 'selected';
            first = false;
            //s2_isetmatchoption(select);
            match.TColumn = ih;
         }      
      }
      td.appendChild(select);  
      jobj.Matches[args.Property] = match;
      iidnode.value = JSON.stringify(jobj);
   }break;
   default: {
      select = document.createElement('SELECT');
      select.id = id;
      select.className = 'MatchType_Column';
      select.onchange = function() {s2_isetmatchoption(this)};
      var first = true;
      if (dtype == 'Version' || dtype == 'Count') {
         first = false;
         option = document.createElement('OPTION');
         option.value = 'Auto';
         option.appendChild(document.createTextNode('* Auto'));
         select.appendChild(option);
         option.selected = 'selected';
         s2_isetmatchoption(select);
      }
      for(ih in headings) {
         hx = headings[ih];
         option = document.createElement('OPTION');
         option.value = hx;
         option.appendChild(document.createTextNode(hx));
         select.appendChild(option);
         if (first) {
            option.selected = 'selected';
            first = false;
            s2_isetmatchoption(select);
         }      
      }
      option = document.createElement('OPTION');
      option.value = 'Static';
      option.appendChild(document.createTextNode('* Enter Text'));
      select.appendChild(option);
      td.appendChild(select);   
   }break;
   }
   return td;
}
function s2_isetmatchoption(node) {
   var id = node.id;
   var value = node.value;
   var args = s2_disectid(id);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);
   var jobj = JSON.parse(iidnode.value);
   if (!jobj.Matches) jobj.Matches = new Object();
   
   switch(value) {
   case 'Choose': {
      var td = node.parentNode;
      var tint = jobj.Links[args.TType];
      while (td.childNodes.length > 0) {
         td.removeChild(td.lastChild);
      }
      var select,option,ih,hx;
      select = document.createElement('SELECT');
      select.id = id;
      select.className = 'MatchType_SameForAll';
      select.onchange = function() {s2_isetmatchoption(this);};
      var tidcol;
      for (ih in tint.Interface) {
         if (tint.Interface[ih] == 'Unique Key') {
            tidcol = ih;            
         } 
      }
      var first = true;
      for (ih in tint.ListData) {
         hx = tint.ListData[ih];
         option = document.createElement('OPTION');
         option.value = hx[tidcol];
         option.appendChild(document.createTextNode(hx.Name));
         select.appendChild(option);
         if (first) {
            option.selected = 'selected';
            first = false;
            s2_isetmatchoption(select);
         }      
      }
      td.appendChild(select);                                            
   }break;
   case 'Derive': {
      var td = node.parentNode;
      var tr = td.parentNode;
      var tb = tr.parentNode;
      var req = new Object();
      var par = new Object();
      par.Action = 'GetDeriveReqs';
      par.IType = args.TType;
      req.target = 'S2_query.php';
      req.sync = true;
      req.request = par;
      var resp = snc_send(req);
      completed();
      showdialog();
      var tint = JSON.parse(resp);
//s2_debugjson(resp);
      delete tint.IsAllowedTo;
      delete tint.UserPermissions;
      var crumbs = s2_getcrumbs();
      var tname,ii,ic,ct,crumb,cset,tdo,tro,tho,select,option;
      for (ii in tint) {
         cset = false;
         tdo = null;
         tname = tint[ii].DataType;
         id = 'Form-'+forms+'_IType-'+jobj.IType+'_DataType-LINKEDTO_TargetType-'+tname+'_Property-'+tname+'_IsCrumb-1';
         for (ic in crumbs) {
            crumb = crumbs[ic];
            ct = crumb.IType;
            if (tname == ct) {
               tdo = document.createElement('TD');
               select = document.createElement('SELECT');
               select.className = 'MatchType_SameForAll';
               select.id = id;
               option = document.createElement('OPTION');
               option.value = crumb.Current;
               option.appendChild(document.createTextNode(crumb.Name));
               select.appendChild(option); 
               option = document.createElement('OPTION');
               option.value = 'Match';
               option.appendChild(document.createTextNode('* Match Data'));
               select.appendChild(option);
               option = document.createElement('OPTION');
               option.value = 'Choose';
               option.appendChild(document.createTextNode('* Choose Existing'));
               select.appendChild(option);
               select.onchange = function() {s2_isetmatchoption(this);};
               tdo.appendChild(select);
            } 
         }
         if (!tdo) {
            var headings = (jobj.Worksheet)?jobj.Headings[jobj.Worksheet]:jobj.Headings;
            var params = s2_disectid(id);
            tdo = document.createElement('TD');
            select = document.createElement('SELECT');
            select.className = 'MatchType_SameForAll';
            select.id = id;
            option = document.createElement('OPTION');
            option.value = '';
            option.appendChild(document.createTextNode('-- choose --'));
            select.appendChild(option); 
            option = document.createElement('OPTION');
            option.value = 'None';
            option.appendChild(document.createTextNode('* None'));
            select.appendChild(option); 
            option = document.createElement('OPTION');
            option.value = 'Match';
            option.appendChild(document.createTextNode('* Match Data'));
            select.appendChild(option);
            select.onchange = function() {s2_isetmatchoption(this);};
            tdo.appendChild(select);
            
         }
         tro = document.createElement('TR')
         tho = document.createElement('TH');
         tho.className = 'left';
         if (tname.match(/_ID$/)) {
            tname = tname.replace(/^[^_]+_/,'');
            tname = tname.replace(/_ID$/,'');
         }
         tho.appendChild(document.createTextNode(tname.replace(/\_/g,' ')));
         tro.appendChild(tho);
         tro.appendChild(tdo);
         tb.insertBefore(tro,tr);
         if (select) {
            s2_isetmatchoption(select);
            jobj = JSON.parse(iidnode.value);
         }
      }
      var td1,match;
      td1 = document.createElement('TD');
      td1.className = 'autodata';
      td1.appendChild(document.createTextNode('Derived Data'));
      tr.replaceChild(td1,td);
      match = new Object();
      match.Type = 'Derived';
      jobj.Matches[args.Property] = match;
      iidnode.value = JSON.stringify(jobj);
      //if (!jobj.Links) jobj.Links = new Object();
      //jobj.Links[args.TType] = tint;      
   }break;
   case 'None': {
      var td = node.parentNode;
      var tr = td.parentNode;
      var tb = tr.parentNode;
      tb.removeChild(tr);
      delete jobj.Matches[args.Property];
      iidnode.value = JSON.stringify(jobj);
   }break;
   case 'Match': {
      var headings = (jobj.Worksheet)?jobj.Headings[jobj.Worksheet]:jobj.Headings;
      var params = s2_disectid(node.id);
      var td = node.parentNode;
      var td2 = s2_imatchselect(node.id,headings,params); 
      var tr = td.parentNode;
      tr.replaceChild(td2,td);
   }break;
   case 'Static': {
      var nnode = document.createElement('INPUT');
      nnode.id = id;
      nnode.className = 'MatchType_Static';
      nnode.onblur = function() {s2_isetmatchoption(this);};
      node.parentNode.replaceChild(nnode,node);
      nnode.focus();            
      s2_isetmatchoption(nnode);
   }break;
   case '': {
      delete jobj.Matches[args.Property];
      iidnode.value = JSON.stringify(jobj);
   }break;
   default: {
      var match = (jobj.Matches[args.Property]!=null)?jobj.Matches[args.Property]:new Object();
      var bits = node.className.split(/\_/);
      var type = bits.pop();
      switch(type) {
      case 'IColumn': {
         match.Type = 'Link'; 
         match.IColumn = value;      
      }break;
      case 'TColumn': {
         match.Type = 'Link'; 
         match.TColumn = value;      
      }break;
      default: {
         if (match.IColumn) delete match.IColumn;
         if (match.TColumn) delete match.TColumn;
         match.Type = type; 
         match.Value = value;      
      }break;
      }
      if (args.IsCrumb != null) {
         match.IsCrumb = true;
         //match.Type = 'Crumb';
      } 
      jobj.Matches[args.Property] = match;
      iidnode.value = JSON.stringify(jobj);   
   }break;
   } 
//s2_debugjson(iidnode.value);
   return true;  
}

function s2_imatchworksheetselect(id,headings) {
   var td,select,option,value,ih,hx;
   td = document.createElement('TD');
   select = document.createElement('SELECT');
   select.id = id;
   select.className = 'MatchType_Worksheet';
   select.onchange = function() {s2_isetmatchworksheetoption(this)};
   var first = true;
   for(ih in headings) {
      option = document.createElement('OPTION');
      option.value = ih;
      option.appendChild(document.createTextNode(ih));
      select.appendChild(option);
      if (first) {
         option.selected = 'selected';
         first = false;
         s2_isetmatchworksheetoption(select);
      }      
   }
   td.appendChild(select);
   return td;
}
function s2_isetmatchworksheetoption(node) {
   var id = node.id
   var value = node.value;
   var args = s2_disectid(id);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);
   var jobj = JSON.parse(iidnode.value);
   jobj.Worksheet = value;
   //if (jobj.Headings[value]) jobj.Headings = jobj.Headings[value];
   iidnode.value = JSON.stringify(jobj);    
   //s2_debugjson(iidnode.value);  
}

function s2_drawimportform_getheadings(id) {
   var args = s2_disectid(id);
   s2_drawimportform_linkfiles(args);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);     
   var jobj = JSON.parse(iidnode.value);
   // add file check
   // get import file headings
   var fi;
   var req = new Object;
   req.sync = true;
   req.target = '../echofile_v1_1.php?a=getcolumnheadings&i=' + jobj.Files[jobj.ImportType];
   var iheadings = JSON.parse(snc_send(req));
   completed();
   showdialog();
   jobj.Headings = iheadings.Headings;
   iidnode.value = JSON.stringify(jobj);
   if (jobj.ImportType == 'xls') {
      s2_ichooseworksheet(id,jobj.Headings);
   } else {
      s2_drawimportform_matchdata(id);
   }  
}

function s2_ichooseworksheet(id,headings) {
   var args = s2_disectid(id);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);     
   var jobj = JSON.parse(iidnode.value);
   var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input,datatype;
   var table,thead,tbody,tr,th,td,node,text,id,lid,action,data,params,uploaders;
   var interfacetype,
   action = 'Import';
   dialog = document.getElementById('dialog-liner');
   dialog.removeChild(document.getElementById(id));
   
   if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
   if (!jobj.Data || jobj.Interim == true) {
      var h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode(action+' '+jobj.DisplayName));
      dialog.appendChild(h2);
   } 
   table = document.createElement('TABLE');
   table.id = action+'_'+jobj.IType;
   table.className = 'addform';
   tbody = document.createElement('TBODY');
   
   prop = 'Worksheet';
   id = 'Form-'+forms+'_IType-'+jobj.IType+'_Property-'+prop;
   td = s2_imatchworksheetselect(id,headings);
   tr = document.createElement('TR')
   th = document.createElement('TH');
   th.className = 'left';
   th.appendChild(document.createTextNode(prop.replace(/\_/g,' ')));
   tr.appendChild(th);
   tr.appendChild(td);
   tbody.appendChild(tr);
   
   table.appendChild(tbody);
   dialog.appendChild(table);
   button = document.createElement('BUTTON');
   button.onclick = function() {s2_drawimportform_matchdata(this.id);};
   button.id = 'Form-'+args.Form+'_IType-'+args.IType+'Action-Import_Stage-Review';
   button.appendChild(document.createTextNode('Match Columns'));
   dialog.appendChild(button); 
   
   //s2_debugjson(iidnode.value);     
}

function s2_drawimportform_crumbs() {
   var crumbs = s2_getcrumbs();
   var prop,crumb,select,option,ctype;
   if (!jobj.Matches) jobj.Matches = new Array();
   for (prop in crumbs) {
      crumb = crumbs[prop]; 
      id = 'Form-'+forms+'_IType-'+jobj.IType+'_DataType-LINKEDTO_TargetType-'+crumb.IType+'_Property-'+crumb.IType+'_IsCrumb-1';
      td = document.createElement('TD');
      select = document.createElement('SELECT');
      select.className = 'MatchType_SameForAll';
      select.id = id;
      option = document.createElement('OPTION');
      option.value = crumb.Current;
      option.appendChild(document.createTextNode(crumb.Name));
      select.appendChild(option); 
      option = document.createElement('OPTION');
      option.value = 'Match';
      option.appendChild(document.createTextNode('* Match Data'));
      select.appendChild(option);
      select.onchange = function() {s2_isetmatchoption(this);};
      td.appendChild(select); 
      tr = document.createElement('TR')
      th = document.createElement('TH');
      th.className = 'left';
      ctype = crumb.IType;
      if (ctype.match(/_ID$/)) {
         ctype = ctype.replace(/^[^_]+_/,'');
         ctype = ctype.replace(/_ID$/,'');
      }
      th.appendChild(document.createTextNode(ctype.replace(/\_/g,' ')));
      tr.appendChild(th);
      tr.appendChild(td);
      tbody.appendChild(tr);
      s2_isetmatchoption(select);      
   }            
}

function s2_drawimportform_matchdata(id) {
   var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input,datatype;
   var div,table,thead,tbody,tr,th,td,node,text,id,lid,action,data,params,uploaders;
   var interfacetype;
   
   var args = s2_disectid(id);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);     
   var jobj = JSON.parse(iidnode.value);
   jobj.Form = args.Form;
   action = 'Import';
   dialog = document.getElementById('dialog-liner');
   dialog.removeChild(document.getElementById(id));
   
   if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
   if (!jobj.Data || jobj.Interim == true) {
      var h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode(action+' '+jobj.DisplayName));
      dialog.appendChild(h2);
   }
   table = document.createElement('TABLE');
   table.id = action+'_'+jobj.IType;
   table.className = 'addform';
   tbody = document.createElement('TBODY');
   uploaders = new Array();
   
   var headings = (jobj.Worksheet)?jobj.Headings[jobj.Worksheet]:jobj.Headings;
   for(prop in jobj.Interface) {
      id = 'Form-'+forms+'_IType-'+jobj.IType+'_Property-'+prop;
      params = jobj.Interface[prop];
      datatype = (typeof params == 'object')?params.DataType:params.replace(/:.+/,'');
      if (!jobj.Data && datatype == 'File Path') uploaders.push(id);
      if (typeof params != 'object' || !params.Hidden) {
      
         if (!jobj.Data && jobj.Versions && prop == jobj.Versions.Column) data = (jobj.Versions.Maximum)?(Math.floor((jobj.Versions.Maximum + 0.1)*10)/10):1;
         else if (!jobj.Data && datatype == 'Count') data = (jobj.Count)?parseInt(jobj.Count)+1:1;       
         else data = (jobj.Data && jobj.Data[prop])?jobj.Data[prop]:null;
         td = s2_imatchselect(id,headings,params);
         if (td) {
            tr = document.createElement('TR')
            th = document.createElement('TH');
            th.className = 'left';
            if (prop.match(/_ID$/)) {
               prop = prop.replace(/^[^_]+_/,'');
               prop = prop.replace(/_ID$/,'');
            }
            th.appendChild(document.createTextNode(prop.replace(/\_/g,' ')));
            tr.appendChild(th);
            tr.appendChild(td);
            tbody.appendChild(tr);
         }
      }
   }
   table.appendChild(tbody);
   dialog.appendChild(table);
   button = document.createElement('BUTTON');
   button.onclick = function() {s2_drawimportform_collatedata(this.id);};
   button.id = 'Form-'+args.Form+'_IType-'+args.IType+'Action-Import_Stage-Collate';
   button.appendChild(document.createTextNode('Collate Data'));
   dialog.appendChild(button);   
   
   //s2_debugjson(iidnode.value);
   //showdialog();
   for (var i in uploaders) s2_inituploader(uploaders[i]);
   return true;
}
function s2_drawimportform_sendidata(id) {
   var args = s2_disectid(id);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);     
   var jobj = JSON.parse(iidnode.value);
   delete jobj.UserPermissions;
   delete jobj.Views;
   delete jobj.Alternatives;
   delete jobj.Domain;   
   var dialog,user,settings;
   dialog = document.getElementById('dialog-liner');
   dialog.removeChild(document.getElementById(id));   
   //alert(iidnode.value);
   //s2_debugjson(iidnode.value);
   var req = new Object();
   req.target = 'S2_query.php';
   var par = new Object();
   par.IType = jobj.IType;
   par.Action = 'Import';
   par.ImportSettings = jobj;
   //user = s2_getcookie('u_info');
   user = s2_getstorage('u_info');
   settings = s2_getcurrent();
   if (user) par.UInfo = user;
   if (settings) par.CurrentSettings = settings;
   req.request = par;
   switch(jobj.Stage) {
   case 'Collate': req.responder = s2_drawimportform_previewdata;break;
   case 'Import': req.responder = s2_drawimportform_reviewdata;break; 
   }    
   snc_send(req);    
   return true;   
}
function s2_drawimportform_collatedata(id) {
   var args = s2_disectid(id);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);
   jobj = JSON.parse(iidnode.value);
   jobj.Stage = 'Collate';
   jobj.Form = args.Form;   
   iidnode.value = JSON.stringify(jobj);
   s2_drawimportform_sendidata(id);   
}
function s2_drawimportform_previewdata(json) {
   var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input,datatype;
   var table,thead,tbody,tr,th,td,node,text,id,lid,action,data,params,uploaders;
   var interfacetype,jobj;
   if (json) {
      jobj = JSON.parse(json);
      cleardialog('dialog-liner');      
      dialog = document.getElementById('dialog-liner');
      
      node = document.createElement('INPUT');
      node.id = 'Form-'+jobj.Form+'_ImportSettings';
      node.type = 'hidden';
      node.value = JSON.stringify(jobj);
      dialog.appendChild(node);     
      action = 'Import';
         
      if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
      
      node = s2_searchbylinks(jobj);
      dialog.appendChild(node);
      h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode('Preview Data'));
      dialog.appendChild(h2);
      div = document.createElement('DIV');
      div.className = 's2_iImportTable'; 
      table = document.createElement('TABLE');
      table.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Table';
      table.className = 'listform';
      thead = document.createElement('THEAD');
      tr = document.createElement('TR');
      for(prop in jobj.Interface) {
         if (!jobj.Interface[prop].Hidden) {
            params = jobj.Interface[prop];
            params.Show = true;
            th = s2_getnode(null,prop,params,null,'ListHeader');
            if (th) tr.appendChild(th);
         }
      }
      thead.appendChild(tr);
      
      table.appendChild(thead);
      tbody = document.createElement('TBODY');
      for(index in jobj.ListData) {
         var listitem = jobj.ListData[index];
         tr = document.createElement('TR');
         var id,name=null,interfacetype;
         for(prop in jobj.Interface) {
            if (jobj.Interface[prop] == 'Unique Key') id = listitem[prop];
            if (!jobj.Interface[prop].Hidden) {
               nodeid = 'Form-'+jobj.Form+'_IType-'+jobj.IType+'_Property-'+prop+'_Current-'+id;
               params = jobj.Interface[prop];
               params.Show = true;
               td = s2_getnode(nodeid,prop,params,listitem[prop],'List');
               if (td) {
                  if (td.className == 'currentversion' || td.className == 'autodata') tr.className = td.className;
                  tr.appendChild(td);
               }
            }
         }
         //td = gen_getlistoptionsnode(jobj,listitem);
         //if (td) tr.appendChild(td);
         tbody.appendChild(tr);
      }
      table.appendChild(tbody);
      //table.onload = function() {table_split(this.id);};
      div.appendChild(table);
      dialog.appendChild(div);
      
      button = document.createElement('BUTTON');
      button.id = 'Form-'+jobj.Form+'_IType-'+jobj.IType+'_Action-ImportInterface';
      button.onclick = function() {s2_drawimportform_importdata(this.id);};
      button.appendChild(document.createTextNode('Import'));
      dialog.appendChild(button);
      
      gen_appendhiddenfields(jobj,dialog,forms);
   }
   
   //s2_debugjson(json);
   completed();
   showdialog();
   return true;   
}
function s2_drawimportform_importdata(id) {
   var args = s2_disectid(id);
   var iid = 'Form-'+args.Form+'_ImportSettings';
   var iidnode = document.getElementById(iid);
   jobj = JSON.parse(iidnode.value);
   jobj.Stage = 'Import';   
   jobj.Form = args.Form;   
   iidnode.value = JSON.stringify(jobj);
   s2_drawimportform_sendidata(id);   
}
function s2_drawimportform_reviewdata(json) {
   var dialog,i,j,indent,tabs,extratab,prop,propname,type,button,input,datatype;
   var table,thead,tbody,tr,th,td,node,text,id,lid,action,data,params,uploaders;
   var interfacetype;
   if (json) {
   
      cleardialog('dialog-liner');
      jobj = JSON.parse(json);
      action = 'Import';
      dialog = document.getElementById('dialog-liner');
         
      if (dialog.childNodes.length > 1) dialog.appendChild(document.createElement('HR'));
      
      node = s2_searchbylinks(jobj);
      dialog.appendChild(node);
      h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode('Review Import'));
      dialog.appendChild(h2);
      div = document.createElement('DIV');
      div.className = 's2_iImportTable'; 
      table = document.createElement('TABLE');
      table.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Table';
      table.className = 'listform';
      thead = document.createElement('THEAD');
      tr = document.createElement('TR');
      for(prop in jobj.Interface) {
         if (!jobj.Interface[prop].Hidden) {
            th = s2_getnode(null,prop,jobj.Interface[prop],null,'ListHeader');
            if (th) tr.appendChild(th);
         }
      }
      th = document.createElement('TH');
      th.appendChild(document.createTextNode('Status'));
      tr.appendChild(th);
      thead.appendChild(tr);
      
      table.appendChild(thead);
      tbody = document.createElement('TBODY');
      for(index in jobj.ListData) {
         var listitem = jobj.ListData[index];
         tr = document.createElement('TR');
         tr.className = 's2_i'+listitem.Status;
         var id,name=null,interfacetype;
         for(prop in jobj.Interface) {
            if (jobj.Interface[prop] == 'Unique Key') id = listitem[prop];
            if (!jobj.Interface[prop].Hidden) {
               nodeid = 'Form-'+forms+'_IType-'+jobj.IType+'_Property-'+prop+'_Current-'+id;          
               td = s2_getnode(nodeid,prop,jobj.Interface[prop],listitem[prop],'List');
               if (td) {
                  if (td.className == 'currentversion' || td.className == 'autodata') tr.className = td.className;
                  tr.appendChild(td);
               }
            }
         }
         td = document.createElement('TD');
         td.appendChild(document.createTextNode(listitem.Status));
         tr.appendChild(td);
         tbody.appendChild(tr);
      }
      table.appendChild(tbody);
      div.appendChild(table);
      dialog.appendChild(div);      
   }   
   //s2_debugjson(json);
   completed();
   showdialog();
   return true;   
}

function s2_debugjson(json) {
   if (json) {
      var dialog;
      dialog = document.getElementById('dialog-liner');
      var pre = snc_get_json_html(json);
      pre.style.height = '200px';
      pre.style.overflow = 'auto';
      var n = document.getElementById(pre.id);
      if (n) dialog.removeChild(n);
      dialog.appendChild(pre);
   }
   return true;
}
;
var pp_type;
var pp_id;
var pp_stattimeout;
var pp_steptimeout;
var pp_update = true;
var pp_statgap = 5;
var pp_nextgap = 5;

function s2_showprocess(jobj) {
   var thisstep,nextstep,step,stepnum,btn,p,ipt,msg,a,hr;
   var statusnodeid,dialog,table,thead,tbody,trh,trb,th,td,tn,cname,div,h2;
   cleardialog('dialog-liner');
   dialog = document.getElementById('dialog-liner');
   statusnodeid = 's2pp_statusbar';
   
   div = document.createElement('DIV');
   div.id = statusnodeid;
   div.appendChild(document.createElement('HR'));
   h2 = document.createElement('H2');
   h2.appendChild(document.createTextNode('Process Status'));
   div.appendChild(h2);
   table = document.createElement('TABLE');
   trh = document.createElement('TR');
   trb = document.createElement('TR');
   thisstep = (jobj.Data && jobj.Data.PP_Step)?jobj.Data.PP_Step:0;
//alert(JSON.stringify(jobj.PostProcess));
   for(stepnum in jobj.PostProcess) {
      step = jobj.PostProcess[stepnum];
      cname = (stepnum <= thisstep)?'s2pp_completed':'s2pp_notrun'; 
      th = document.createElement('TH');
      th.appendChild(document.createTextNode('Step:'+(parseInt(stepnum)+1)));
      trh.appendChild(th);
      td = document.createElement('TD');
      td.className = cname;
      //td.appendChild(document.createTextNode(step.Name));
      a = document.createElement('A');
      a.href = '#'
      a.id = 's2ppstep-'+stepnum;
      a.appendChild(document.createTextNode(step.Name));
      a.onclick = function() {
         var json,jobj,step;
         step = parseInt(this.id.replace(/[^\-]+\-/,''));
         json = document.getElementById('s2_postprocess').value;
         jobj = JSON.parse(json);
         jobj.Data.PP_Step = (step-1);
         s2_postprocess_nextstep(jobj);
         return false;
      }
      td.appendChild(a);
      trb.appendChild(td);
   }
   thead = document.createElement('THEAD');
   tbody = document.createElement('TBODY');
   thead.appendChild(trh);
   tbody.appendChild(trb);
   table.appendChild(thead);
   table.appendChild(tbody);
   div.appendChild(table);
   dialog.appendChild(div);
   
   s2_postprocess_addprogressbars(div);
   
   thisstep = (jobj.Data && jobj.Data.PP_Step>0)?jobj.Data.PP_Step:0;
   nextstep = thisstep + 1;
   
   /*
   if (thisstep >= 0) {
      step = jobj.PostProcess[thisstep];
      h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode('Step ' + thisstep + ': ' + step.Name));
      dialog.appendChild(h2);
      p = document.createElement('P');
      msg = (jobj.Message)?jobj.Message:'Last step completed successfully.';
      p.appendChild(document.createTextNode(msg));
      dialog.appendChild(p);      
   }
   
   if (nextstep < jobj.PostProcess.length) {
      step = jobj.PostProcess[nextstep];
      h2 = document.createElement('H2');
      h2.appendChild(document.createTextNode('Step ' + nextstep + ': ' + step.Name));
      dialog.appendChild(h2);
      p = document.createElement('P');
      p.appendChild(document.createTextNode('Continue to next step?'));      
      dialog.appendChild(p);
   }
   */
   btn = document.createElement('BUTTON');
   btn.appendChild(document.createTextNode('Cancel'));
   btn.onclick = function() {
      cleardialog('dialog-liner');
      hidedialog();
      if (pp_steptimeout) window.clearTimeout(pp_steptimeout);
   };
   dialog.appendChild(btn);
   if (nextstep < jobj.PostProcess.length) {
      btn = document.createElement('BUTTON');
      btn.appendChild(document.createTextNode('Continue'));
      btn.onclick = function() {
         var json,jobj;
         json = document.getElementById('s2_postprocess').value;
         jobj = JSON.parse(json);
         s2_postprocess_nextstep(jobj);
         return false;
      };
      dialog.appendChild(btn);
   }
   dialog.appendChild(document.createElement('HR'));   
   ipt = document.createElement('INPUT');
   ipt.type = 'hidden';
   ipt.id = 's2_postprocess';
   ipt.value = JSON.stringify(jobj);
   dialog.appendChild(ipt);
   pp_update = false;
   s2_postprocess_getstatus();
   completed();
   showdialog();
}

function s2_postprocess(jobj) {
   //completed();
   s2_showprocess(jobj);
   pp_update = false;
   if (pp_steptimeout) window.clearTimeout(pp_steptimeout);
   pp_steptimeout = window.setTimeout('s2_postprocess_autonext();',pp_nextgap*1000);
}   
function s2_postprocess_autonext() {
   var json,jobj;
   json = document.getElementById('s2_postprocess').value;
   jobj = JSON.parse(json);
   s2_postprocess_nextstep(jobj);         
}
function s2_postprocess_nextstep(jobj) {
   var req,params,step,thisstep,nextstep,user; 
   if (pp_steptimeout) window.clearTimeout(pp_steptimeout);
   thisstep = (jobj.Data && jobj.Data.PP_Step>=0)?jobj.Data.PP_Step:-1;
   nextstep = thisstep + 1;
   params = new Object();
   params.IType = jobj.IType;
   params.Current = jobj.Current;
   params.Action = 'PostProcess';
   params.Step = nextstep;
//   s2_query(params);
   pp_update = true; 
   //inprogress(); 
   user = s2_getcookie('u_info');
   if (user) params.UInfo = user; 
   req = new Object();
   req.target = s2_getcurrentpath()+'/S2_query.php';
   req.request = params;
   req.responder = s2_draw;
   snc_send(req);
   s2_postprocess_getstatus();
   completed();   
   showdialog();
}
function s2_postprocess_addprogressbars(parent) {
   var cont,pbar,h2,p,text,bars,bar,b,hr;
   bars = ['Step','Process'];
   for (b in bars) { 
      bar = bars[b];
      hr = document.createElement('HR');
      parent.appendChild(hr);
      cont = document.createElement('DIV');
      cont.id = 'pp-'+bar+'-container';
      h2 = document.createElement('H2');
      h2.style.display = 'inline';
      h2.style.marginRight = '10px';
      h2.id = 'pp-'+bar+'-name';
      h2.appendChild(document.createTextNode(bar + ' Status..'));
      cont.appendChild(h2);
      p = document.createElement('P');
      p.style.display = 'inline';
      p.id = 'pp-'+bar+'-status';
      cont.appendChild(p);
      pbar = document.createElement('DIV');
      pbar.id = 'pp-'+bar+'-progresscontainer';
      cont.appendChild(pbar);
      parent.appendChild(cont);
      s2_makeprogressbar('pp-'+bar+'-progresscontainer','pp-'+bar+'-progressbar',0);
   }
}
function s2_postprocess_getstatus() {
   var req,params,step,thisstep,nextstep,json,jobj,pnode;
   pnode = document.getElementById('s2_postprocess');
   if (pnode) { 
      json = pnode.value;
      jobj = JSON.parse(json);
      params = new Object();
      params.IType = jobj.IType;
      params.Current = jobj.Current;
      params.Action = 'ProcessStatus';
      user = s2_getcookie('u_info');
      if (user) params.UInfo = user; 
      req = new Object();
      req.target = s2_getcurrentpath()+'/S2_query.php';
      req.request = params;
      req.responder = s2_postprocess_updatestatus;
      snc_send(req);
   }   
}
function s2_postprocess_updatestatus(json) {
   var div,statusnodeid,jobj,bars,b,bar,node,text,tnode;
   try {
      jobj = JSON.parse(json);
      bars = ['Step','Process'];
      for (b in bars) { 
         bar = bars[b];
         if (jobj.Process) {
            text = jobj.Process.Status;
            node = document.getElementById('pp-Process-status');
            tnode = document.createTextNode(text);
            if (node.childNodes.length>0) node.replaceChild(tnode,node.firstChild);
            else node.appendChild(tnode);
            s2_updateprogressbar('pp-Process-progressbar',jobj.Process.Percentage_Complete);
         }
         if (jobj.Step) {
            text = 'Step ' + jobj.Step.Number + ': ' + jobj.Step.Name + '..'; 
            node = document.getElementById('pp-Step-name');
            tnode = document.createTextNode(text);
            if (node.childNodes.length>0) node.replaceChild(tnode,node.firstChild);
            else node.appendChild(tnode);
            text = jobj.Step.Status;
            node = document.getElementById('pp-Step-status');
            tnode = document.createTextNode(text);
            if (node.childNodes.length>0) node.replaceChild(tnode,node.firstChild);
            else node.appendChild(tnode);
            s2_updateprogressbar('pp-Step-progressbar',jobj.Step.Percentage_Complete)
         }
      }
      if (pp_update) {
         if (pp_stattimeout) window.clearTimeout(pp_stattimeout);
         pp_stattimeout = window.setTimeout('s2_postprocess_getstatus();',pp_statgap*1000);
      }
   } catch(e) {
      pp_update = false;
   }
   completed();
   showdialog();
}
;function s2_updateprogressbar(id,complete) {
   var pc,pb,ps,pt,percent;
   percent = Math.floor(complete * 100);
   pc = document.getElementById(id);
   pb = document.getElementById(id + '-bar');
   ps = document.getElementById(id + '-status');
   pt = document.getElementById(id + '-statustext');
   stext = '\u00a0' + percent + '%\u00a0~\u00a0';
   ps.style.width = percent + '%';
   pt.replaceChild(document.createTextNode(stext),pt.firstChild);
   if (percent >= 50) ps.appendChild(pt);
   else pb.insertBefore(pt,pb.firstChild);
}
function s2_completeprogressbar(id) {
   var pc,pnode;
   pc = document.getElementById(id);
   pnode = pc.parentNode;
   pnode.removeChild(pc);
}
function s2_makeprogressbar(parent,id,complete,width) {
   var pnode,c,b,s,sp,percent,stext;
   pnode = document.getElementById(parent);
   percent = Math.floor(complete * 100);
   c = document.createElement('DIV');
   c.id = id;
   c.className = 'pbar-container';
   if (width) c.style.width = width;
   b = document.createElement('DIV');
   b.id = id + '-bar';
   b.className = 'pbar-bar';        
   s = document.createElement('DIV');
   s.id = id + '-status';
   s.className = 'pbar-status';
   s.style.width = percent + '%';
   stext = percent + '%\u00a0~\u00a0';
   sp = document.createElement('SPAN');
   sp.id = id + '-statustext';
   sp.className = 'pbar-statustext';
   sp.appendChild(document.createTextNode(stext));
   if (percent >= 50) s.appendChild(sp);
   else b.appendChild(sp);
   b.appendChild(s);
   c.appendChild(b);
   pnode.appendChild(c);
   return true;
}
;function s2_makecollapsible() {
   var nodes,nx,node,collapser,btn,img,expand; 
   nodes = document.getElementsByTagName('DIV');
    
   for (nx in nodes) {                              
      node = nodes[nx];
      if (node.className.match(/s2_collapsible/)) {
         expand = (node.className.match(/expand/));
         collapser = node.previousSibling;
         btn = document.createElement('BUTTON');
         btn.className = 's2_expander';
         img = new Image();
         img.src = (expand)?'../images/arrow_d.gif':'../images/arrow_r.gif';
         //img.src = (expand)?'../images/eye-open-20.gif':'../images/eye-closed-20.gif';
         img.className = 's2_expander';
         img.alt = (expand)?'Collapse':'Expand';
         btn.appendChild(img);
         btn.name = img.alt;
         s2_addtooltip(btn);
         btn.onclick = function () {s2_clickcollapser(this);};
         collapser.insertBefore(btn,collapser.firstChild);
         node.className = (expand)?'s2_visiblesection':'s2_hiddensection';
      } 
   }
}
function s2_clickcollapser(btn) {
   var collapser,node,img;
   collapser = btn.parentNode;
   node = collapser.nextSibling;
   img = btn.firstChild;    
   switch (node.className) {
   case 's2_hiddensection': {
      node.className = 's2_visiblesection';
      img.src = '../images/arrow_d.gif';
      //img.src = '../images/eye-open-20.gif';
      img.alt = 'Collapse';
      btn.name = 'Collapse';
   }break;
   default: {
      node.className = 's2_hiddensection';
      img.src = '../images/arrow_r.gif';
      //img.src = '../images/eye-closed-20.gif';
      img.alt = 'Expand';
      btn.name = 'Expand';
   }break;
   }
   return true;
};function s2_enableordering() {
   var orderable = s2_getstorage('s2_orderer');
   var encbutton,ordbutton,img;
   ordbutton = document.getElementById('flip_orderer');
   if (!ordbutton) { 
      encbutton = document.getElementById('flip_encrypt');
      ordbutton = document.createElement('BUTTON');   
      ordbutton.className = 'fliporder';
      ordbutton.id = 'flip_orderer';
      ordbutton.onclick = function() {s2_flip_orderer(this.id);};
      img = document.createElement('IMG');
      if (orderable>0) {
         img.src = '../images/sorticon-on.png'; 
         img.alt = 'ordering: on';
      } else {
         img.src = '../images/sorticon-off.png'; 
         img.alt = 'ordering: off';
      }
      ordbutton.appendChild(img);
      encbutton.parentNode.insertBefore(ordbutton,encbutton);
   }      
}
function s2_orderingenabled() {
   var orderable = s2_getstorage('s2_orderer');
   return orderable;
}
function s2_flip_orderer(buttonid) {            
   var txt,img,onoff,button;
   if (!buttonid) {
      onoff = (!s2_getstorage('s2_orderer'))?'ordering: on':'ordering: off';
      button = document.getElementById('flip_orderer');   
   } else {
      button = document.getElementById(buttonid);
      onoff = button.firstChild.alt;
   }
   switch (onoff) {
   case 'ordering: on': {
      txt = 'ordering: off';
      img = new Image();
      img.src = '../images/sorticon-off.png';
      img.alt = txt;
      button.replaceChild(img,button.firstChild);
      s2_setstorage('s2_orderer',0);
   }break;
   case 'ordering: off': {
      txt = 'ordering: on';
      img = new Image();
      img.src = '../images/sorticon-on.png';
      img.alt = txt;
      button.replaceChild(img,button.firstChild); 
      s2_setstorage('s2_orderer',1);
   }break;                    
   }
   s2_refresh();
   return true;   
}
function s2_getorders(type) {
   var orders,vals,order,text;
   orders = {'None':'\xD7','Up':'\u2192','Down':'\u2190'};
   if (type) {
      vals = [];
      for (order in orders) {
         switch(type) {
         case 'text':   {vals.push(orders[order]);}break;
         case 'sql':    {vals.push(order);}break;
         }
      }
   } else vals = orders;
   return vals;
}
function s2_orderby(id) {
   var args,button,itypeobj,cdom,text,oi,orders,order,oicons,oicon,cn,colm,coli,ordr,oobj;
   args = s2_disectid(id);
   button = document.getElementById(id);
   text = button.firstChild.nodeValue;
   orders = s2_getorders('sql');
   oicons = s2_getorders('text');
   colm = args.Column; 
   cdom = s2_getcurrentdomain();
     
   for (oi in oicons) {
      oicon = oicons[oi];
      if (oicon == text) {
         cn = ((oi + 1) % oicons.length);
         button.firstChild.nodeValue = oicons[cn];
      }     
   }
   var s2settings = s2_getcurrent();
   if (s2settings[args.IType]) {
      itypeobj = s2settings[args.IType];
   } else {
      itypeobj = new Object();
      itypeobj.Domain = cdom;
      itypeobj.IType = args.IType;
   }
   ordr = orders[cn];
   // Orders should be array of objects 
   if (order != 'None' && ((!itypeobj.Orders)||(typeof itypeobj.Orders != 'array'))) itypeobj.Orders = [];
   // Orders should be applied in the order they're selected so if you've 
   // already got a setting for the given column it needs to be removed and 
   // re-added at the end.
   for (oi in itypeobj.Orders) {
      oobj = itypeobj.Orders[oi];
      if (oobj.Column == colm) itype.Orders.splice(oi,1); 
   }
   if (ordr != 'None') itypeobj.Orders.push({Column:colm,Order:ordr});
   if (itypeobj.Orders.length == 0) delete itypeobj.Orders;  
   s2settings[args.IType] = itypeobj;
   s2_setstorage('S2Settings',s2settings);
   s2_refresh();
   return true;  
}
function s2_isorderable(jobj,prop) {
   var isorderable = true;     
   if (jobj.Interface[prop].DataType) {
      switch(jobj.Interface[prop].DataType) {
      case 'LINKEDTO':  isorderable = false;
      case 'LINKEDTOM': isorderable = false;
      case 'Map Data':  isorderable = false;
      }
   } else isorderable = false;
   return isorderable;
}
function s2_addorder(th,jobj,prop) {
   var sobj,orders,oicons,oi,text,order;
   if (s2_isorderable(jobj,prop)) {
      sobj = s2_getcurrent(jobj.IType);
      // get current orders // get order values and set column icons to correct order
      orders = s2_getorders();
      
      // add order by link 
      text = null;
      if (sobj && sobj.Orders) {for (oi in sobj.Orders) {if (sobj.Orders[oi].Column == prop) text = orders[sobj.Orders[oi].Order];}} 
      if (!text) text = orders.None; 
      button = document.createElement('BUTTON');
      button.className = 's2orderer';
      button.id = 'Form-'+forms+'_IType-'+jobj.IType+'_Column-'+prop+'_Action-OrderBy';
      button.onclick = function() {s2_orderby(this.id);};
      button.appendChild(document.createTextNode(text));
      th.insertBefore(button,th.firstChild);
   }
}
;var svr = 'http://localhost/s2';
function s2_enableexporter() {
   var encbutton,expbutton,img,icn,div,i,id,ids;
   ids = ['page','main','report-page'];
   expbutton = document.getElementById('flip_export');
   if (!expbutton) { 
      expbutton = document.createElement('BUTTON');   
      expbutton.className = 'fliporder';
      expbutton.id = 'flip_export';
      expbutton.onclick = function() {s2_excelmedoc();};
      img = document.createElement('IMG');
      //img.src = svr+'/images/expicon-on.png'; 
      img.src = '../images/expicon-on.png'; 
      img.alt = 'Export data';
      expbutton.appendChild(img);
      encbutton = document.getElementById('flip_encrypt');
      if (encbutton) encbutton.parentNode.insertBefore(expbutton,encbutton);
      else {
         for(i=0;i<ids.length;i++) {
            div = document.getElementById(ids[i]);
            if (!id && div) {
               id = ids[i];
               div.insertBefore(expbutton,div.firstChild);
            } 
         }
      }
   }   
   s2_addtabexporters();      
}
function s2_excelmedoc() {
   var dat;
   dat = s2_getalltabledata();
   s2_sendexcelmedata(dat);
}
function s2_excelmetab(id) {
   var dat;
   dat = new Array(s2_gettabledatabyid(id));
   s2_sendexcelmedata(dat);   
}
function s2_sendexcelmedata(dat) {
   var hdn,frm,ta,json;
   json = JSON.stringify(dat);
   
   hdn = document.createElement('DIV');
   hdn.style.display = 'none';
   hdn.style.visibility = 'hidden';
   frm = document.createElement('FORM');
   frm.method = 'POST';
   frm.target = 's2excelme';
   frm.action = svr+'/htmlexcel.php';
   
   ta = document.createElement('TEXTAREA');
   ta.name = 'data'
   ta.value = json;
   ta.style.width = '100%';
   ta.style.height = '300px';
   frm.appendChild(ta);
   hdn.appendChild(frm);
   document.body.appendChild(hdn);
   frm.submit();
   document.body.removeChild(hdn);
}
function s2_getalltabledata() {
   var tabs,tab,t,dats,dat;
   tabs = document.getElementsByTagName('TABLE');
   dats = new Array();
   for (t=0;t<tabs.length;t++) {
      tab = tabs[t];
      if (!tab.className.match(/s2noexport/)) {
         dat = s2_gettabledata(tab);
         dats.push(dat);
      }
   }
   return dats;
}  
function s2_addtabexporters() {
   var tabs,tab,t;
   tabs = document.getElementsByTagName('TABLE');
   for (t=0;t<tabs.length;t++) {
      tab = tabs[t];
      if (!tab.className.match(/s2noexport/))
         s2_addtabexportbutton(tab,t);
   }
}
function s2_addtabexportbutton(tab,t) {
   var expbutton,img;
   if (!tab.id) tab.id = 's2table_'+t;
   if (!tab.className.match(/noexport/)&&(!tab.previousSibling||tab.previousSibling.className != 'inlineexport')) {
      expbutton = document.createElement('BUTTON');   
      expbutton.className = 'inlineexport';
      expbutton.id = 'flip_export_'+tab.id;
      expbutton.onclick = function() {s2_excelmetab(tab.id);};
      img = document.createElement('IMG');
      //img.src = svr+'/images/expicon-on.png'; 
      img.src = '../images/expicon-on.png'; 
      img.alt = 'Export data';
      expbutton.appendChild(img);
      tab.parentNode.insertBefore(expbutton,tab);
   }   
}
function s2_gettabledatabyid(id) {
   var tab,dat;
   tab = document.getElementById(id);
   dat = s2_gettabledata(tab);
   return dat;
}
function s2_gettabledata(tab) {
   var dat,dor,ttl,bx,bds,rx,rows,row,cx,cel,nde;
   dat = {};
   dat.rows = new Array();
   //dat.dim = tab.rows.length;
   ttl = s2_gettabletitle(tab);
   if (ttl) dat.title = ttl;
   //rows = tab.rows;
   if (tab.tHead) {
      rows = tab.tHead.rows;
      for (rx=0;rx<rows.length;rx++) {
         row = rows[rx];
         dor = s2_getrowdata(row); 
         dat.rows.push(dor);
      }
   }
   for (bx=0;bx<tab.tBodies.length;bx++) {
      rows = tab.tBodies[bx].rows;
      for (rx=0;rx<rows.length;rx++) {
         row = rows[rx];
         dor = s2_getrowdata(row); 
         dat.rows.push(dor);
      }
   }
   return dat;
}
function s2_getrowdata(row) {
   var bx,bds,cx,cel,nde;
   dor = new Array();
   for (cx=0;cx<row.cells.length;cx++) {
      cel = row.cells[cx];
      nde = s2_getcel(cel);
      dor.push(nde); 
   }
   return dor;
}
function s2_gettabletitle(tab) {
   var ttl;
   ttl = s2_getattval(tab,'title');
   // if the table doesn't have a title attribute then try to find a hx element 
   // associated with successive parent elements.
   if (!ttl) ttl = s2_getsibhx(tab);
   if (!ttl) ttl = s2_getparhx(tab);
   return ttl;
}
function s2_getattval(nde,nme) {
   var ttl,ax,att,attnme;
   for (ax in nde.attributes) {
      att = nde.attributes[ax];
      attnme = att.nodeName;
      if (attnme == nme) tll = att.nodeValue;
   }
   return ttl;
}
function s2_getcel(cel) {
   var nde;
   nde = {};
   nde.t = (cel.nodeName == 'TH')?'h':'d';
   nde.v = s2_getndeval(cel);
   return nde;
}
function s2_getndeval(nde) {
   var val,cld,cx,ccld,ccx,frms;
   val = '';
   for(cx=0;cx<nde.childNodes.length;cx++) {
      cld = nde.childNodes[cx];
      switch(cld.nodeName) {
      case 'INPUT':     if (cld.type != 'hidden') val += cld.value; break;
      case 'TEXTAREA':  val += cld.value; break;
      default:          val += ((cld.nodeValue)?cld.nodeValue:''); break;
      }
      if (cld.childNodes) val += s2_getndeval(cld);
   }
   val = val.replace(/[^\x00-\x7F]/g,'');
   val = val.replace(/[\x60,\x27]/g,''); // replace backticks
   return val;
}
function s2_getsibhx(nde) {
   var ttl;
   while (!ttl && (nde = nde.previousSibling)) {
      if (nde.nodeName && nde.nodeName.match(/H\d/) && nde.childNodes) ttl = s2_getndeval(nde);//nde.firstChild.nodeValue;
      else if (nde.childNodes) ttl = s2_getcldhx(nde);
   }
   return ttl;
}
function s2_getcldhx(nde) {
   var ttl,cld,cx;
   for (cx in nde.childNodes) {
      cld = nde.childNodes[cx];
      if (!ttl && cld.nodeName && cld.nodeName.match(/H\d/) && cld.childNodes) ttl = s2_getndeval(cld);//cld.firstChild.nodeValue;
   }
   return ttl;
}
function s2_getparhx(nde) {
   var ttl,cx,cld;
   while (!ttl && (nde = nde.parentNode)) {
      ttl = s2_getsibhx(nde);
      if (!ttl) {
      for (cx in nde.childNodes) {
         cld = nde.childNodes[cx];
         if (!ttl && cld.nodeName && cld.nodeName.match(/H\d/) && cld.childNodes) ttl = s2_getndeval(cld);//cld.firstChild.nodeValue;
      }}
   }   
   return ttl;
};/**
 * http://github.com/valums/file-uploader
 * 
 * Multiple file upload component with progress-bar, drag-and-drop. 
 * © 2010 Andrew Valums ( andrew(at)valums.com ) 
 * 
 * Licensed under GNU GPL 2 or later, see license.txt.
 */    

//
// Helper functions
//

var qq = qq || {};

/**
 * Adds all missing properties from second obj to first obj
 */ 
qq.extend = function(first, second){
    for (var prop in second){
        first[prop] = second[prop];
    }
};  

/**
 * Searches for a given element in the array, returns -1 if it is not present.
 * @param {Number} [from] The index at which to begin the search
 */
qq.indexOf = function(arr, elt, from){
    if (arr.indexOf) return arr.indexOf(elt, from);
    
    from = from || 0;
    var len = arr.length;    
    
    if (from < 0) from += len;  

    for (; from < len; from++){  
        if (from in arr && arr[from] === elt){  
            return from;
        }
    }  
    return -1;  
}; 
    
qq.getUniqueId = (function(){
    var id = 0;
    return function(){ return id++; };
})();

//
// Events

qq.attach = function(element, type, fn){
    if (element.addEventListener){
        element.addEventListener(type, fn, false);
    } else if (element.attachEvent){
        element.attachEvent('on' + type, fn);
    }
};
qq.detach = function(element, type, fn){
    if (element.removeEventListener){
        element.removeEventListener(type, fn, false);
    } else if (element.attachEvent){
        element.detachEvent('on' + type, fn);
    }
};

qq.preventDefault = function(e){
    if (e.preventDefault){
        e.preventDefault();
    } else{
        e.returnValue = false;
    }
};

//
// Node manipulations

/**
 * Insert node a before node b.
 */
qq.insertBefore = function(a, b){
    b.parentNode.insertBefore(a, b);
};
qq.remove = function(element){
    element.parentNode.removeChild(element);
};

qq.contains = function(parent, descendant){       
    // compareposition returns false in this case
    if (parent == descendant) return true;
    
    if (parent.contains){
        return parent.contains(descendant);
    } else {
        return !!(descendant.compareDocumentPosition(parent) & 8);
    }
};

/**
 * Creates and returns element from html string
 * Uses innerHTML to create an element
 */
qq.toElement = (function(){
    var div = document.createElement('div');
    return function(html){
        div.innerHTML = html;
        var element = div.firstChild;
        div.removeChild(element);
        return element;
    };
})();

//
// Node properties and attributes

/**
 * Sets styles for an element.
 * Fixes opacity in IE6-8.
 */
qq.css = function(element, styles){
    if (styles.opacity != null){
        if (typeof element.style.opacity != 'string' && typeof(element.filters) != 'undefined'){
            styles.filter = 'alpha(opacity=' + Math.round(100 * styles.opacity) + ')';
        }
    }
    qq.extend(element.style, styles);
};
qq.hasClass = function(element, name){
    var re = new RegExp('(^| )' + name + '( |$)');
    return re.test(element.className);
};
qq.addClass = function(element, name){
    if (!qq.hasClass(element, name)){
        element.className += ' ' + name;
    }
};
qq.removeClass = function(element, name){
    var re = new RegExp('(^| )' + name + '( |$)');
    element.className = element.className.replace(re, ' ').replace(/^\s+|\s+$/g, "");
};
qq.setText = function(element, text){
    element.innerText = text;
    element.textContent = text;
};

//
// Selecting elements

qq.children = function(element){
    var children = [],
    child = element.firstChild;

    while (child){
        if (child.nodeType == 1){
            children.push(child);
        }
        child = child.nextSibling;
    }

    return children;
};

qq.getByClass = function(element, className){
    if (element.querySelectorAll){
        return element.querySelectorAll('.' + className);
    }

    var result = [];
    var candidates = element.getElementsByTagName("*");
    var len = candidates.length;

    for (var i = 0; i < len; i++){
        if (qq.hasClass(candidates[i], className)){
            result.push(candidates[i]);
        }
    }
    return result;
};

/**
 * obj2url() takes a json-object as argument and generates
 * a querystring. pretty much like jQuery.param()
 * 
 * how to use:
 *
 *    `qq.obj2url({a:'b',c:'d'},'http://any.url/upload?otherParam=value');`
 *
 * will result in:
 *
 *    `http://any.url/upload?otherParam=value&a=b&c=d`
 *
 * @param  Object JSON-Object
 * @param  String current querystring-part
 * @return String encoded querystring
 */
qq.obj2url = function(obj, temp, prefixDone){
    var uristrings = [],
        prefix = '&',
        add = function(nextObj, i){
            var nextTemp = temp 
                ? (/\[\]$/.test(temp)) // prevent double-encoding
                   ? temp
                   : temp+'['+i+']'
                : i;
            if ((nextTemp != 'undefined') && (i != 'undefined')) {  
                uristrings.push(
                    (typeof nextObj === 'object') 
                        ? qq.obj2url(nextObj, nextTemp, true)
                        : (Object.prototype.toString.call(nextObj) === '[object Function]')
                            ? encodeURIComponent(nextTemp) + '=' + encodeURIComponent(nextObj())
                            : encodeURIComponent(nextTemp) + '=' + encodeURIComponent(nextObj)                                                          
                );
            }
        }; 

    if (!prefixDone && temp) {
      prefix = (/\?/.test(temp)) ? (/\?$/.test(temp)) ? '' : '&' : '?';
      uristrings.push(temp);
      uristrings.push(qq.obj2url(obj));
    } else if ((Object.prototype.toString.call(obj) === '[object Array]') && (typeof obj != 'undefined') ) {
        // we wont use a for-in-loop on an array (performance)
        for (var i = 0, len = obj.length; i < len; ++i){
            add(obj[i], i);
        }
    } else if ((typeof obj != 'undefined') && (obj !== null) && (typeof obj === "object")){
        // for anything else but a scalar, we will use for-in-loop
        for (var i in obj){
            add(obj[i], i);
        }
    } else {
        uristrings.push(encodeURIComponent(temp) + '=' + encodeURIComponent(obj));
    }

    return uristrings.join(prefix)
                     .replace(/^&/, '')
                     .replace(/%20/g, '+'); 
};

//
//
// Uploader Classes
//
//

var qq = qq || {};
    
/**
 * Creates upload button, validates upload, but doesn't create file list or dd. 
 */
qq.FileUploaderBasic = function(o){
    this._options = {
        // set to true to see the server response
        debug: false,
        action: '/server/upload',
        params: {},
        button: null,
        multiple: true,
        maxConnections: 3,
        // validation        
        allowedExtensions: [],               
        sizeLimit: 0,   
        minSizeLimit: 0,                             
        // events
        // return false to cancel submit
        onSubmit: function(id, fileName){},
        onProgress: function(id, fileName, loaded, total){},
        onComplete: function(id, fileName, responseJSON){},
        onCancel: function(id, fileName){},
        // messages                
        messages: {
            typeError: "{file} has invalid extension. Only {extensions} are allowed.",
            sizeError: "{file} is too large, maximum file size is {sizeLimit}.",
            minSizeError: "{file} is too small, minimum file size is {minSizeLimit}.",
            emptyError: "{file} is empty, please select files again without it.",
            onLeave: "The files are being uploaded, if you leave now the upload will be cancelled."            
        },
        showMessage: function(message){
            alert(message);
        }               
    };
    qq.extend(this._options, o);
        
    // number of files being uploaded
    this._filesInProgress = 0;
    this._handler = this._createUploadHandler(); 
    
    if (this._options.button){ 
        this._button = this._createUploadButton(this._options.button);
    }
                        
    this._preventLeaveInProgress();         
};
   
qq.FileUploaderBasic.prototype = {
    setParams: function(params){
        this._options.params = params;
    },
    getInProgress: function(){
        return this._filesInProgress;         
    },
    _createUploadButton: function(element){
        var self = this;
        
        return new qq.UploadButton({
            element: element,
            multiple: this._options.multiple && qq.UploadHandlerXhr.isSupported(),
            onChange: function(input){
                self._onInputChange(input);
            }        
        });           
    },    
    _createUploadHandler: function(){
        var self = this,
            handlerClass;        
        
        if(qq.UploadHandlerXhr.isSupported()){           
            handlerClass = 'UploadHandlerXhr';                        
        } else {
            handlerClass = 'UploadHandlerForm';
        }

        var handler = new qq[handlerClass]({
            debug: this._options.debug,
            action: this._options.action,         
            maxConnections: this._options.maxConnections,   
            onProgress: function(id, fileName, loaded, total){                
                self._onProgress(id, fileName, loaded, total);
                self._options.onProgress(id, fileName, loaded, total);                    
            },            
            onComplete: function(id, fileName, result){
                self._onComplete(id, fileName, result);
                self._options.onComplete(id, fileName, result);
            },
            onCancel: function(id, fileName){
                self._onCancel(id, fileName);
                self._options.onCancel(id, fileName);
            }
        });

        return handler;
    },    
    _preventLeaveInProgress: function(){
        var self = this;
        
        qq.attach(window, 'beforeunload', function(e){
            if (!self._filesInProgress){return;}
            
            var e = e || window.event;
            // for ie, ff
            e.returnValue = self._options.messages.onLeave;
            // for webkit
            return self._options.messages.onLeave;             
        });        
    },    
    _onSubmit: function(id, fileName){
        this._filesInProgress++;  
    },
    _onProgress: function(id, fileName, loaded, total){        
    },
    _onComplete: function(id, fileName, result){
        this._filesInProgress--;                 
        if (result.error){
            this._options.showMessage(result.error);
        }             
    },
    _onCancel: function(id, fileName){
        this._filesInProgress--;        
    },
    _onInputChange: function(input){
        if (this._handler instanceof qq.UploadHandlerXhr){                
            this._uploadFileList(input.files);                   
        } else {             
            if (this._validateFile(input)){                
                this._uploadFile(input);                                    
            }                      
        }               
        this._button.reset();   
    },  
    _uploadFileList: function(files){
        for (var i=0; i<files.length; i++){
            if ( !this._validateFile(files[i])){
                return;
            }            
        }
        
        for (var i=0; i<files.length; i++){
            this._uploadFile(files[i]);        
        }        
    },       
    _uploadFile: function(fileContainer){      
        var id = this._handler.add(fileContainer);
        var fileName = this._handler.getName(id);
        
        if (this._options.onSubmit(id, fileName) !== false){
            this._onSubmit(id, fileName);
            this._handler.upload(id, this._options.params);
        }
    },      
    _validateFile: function(file){
        var name, size;
        
        if (file.value){
            // it is a file input            
            // get input value and remove path to normalize
            name = file.value.replace(/.*(\/|\\)/, "");
        } else {
            // fix missing properties in Safari
            name = file.fileName != null ? file.fileName : file.name;
            size = file.fileSize != null ? file.fileSize : file.size;
        }
                    
        if (! this._isAllowedExtension(name)){            
            this._error('typeError', name);
            return false;
            
        } else if (size === 0){            
            this._error('emptyError', name);
            return false;
                                                     
        } else if (size && this._options.sizeLimit && size > this._options.sizeLimit){            
            this._error('sizeError', name);
            return false;
                        
        } else if (size && size < this._options.minSizeLimit){
            this._error('minSizeError', name);
            return false;            
        }
        
        return true;                
    },
    _error: function(code, fileName){
        var message = this._options.messages[code];        
        function r(name, replacement){ message = message.replace(name, replacement); }
        
        r('{file}', this._formatFileName(fileName));        
        r('{extensions}', this._options.allowedExtensions.join(', '));
        r('{sizeLimit}', this._formatSize(this._options.sizeLimit));
        r('{minSizeLimit}', this._formatSize(this._options.minSizeLimit));
        
        this._options.showMessage(message);                
    },
    _formatFileName: function(name){
        if (name.length > 33){
            name = name.slice(0, 19) + '...' + name.slice(-13);    
        }
        return name;
    },
    _isAllowedExtension: function(fileName){
        var ext = (-1 !== fileName.indexOf('.')) ? fileName.replace(/.*[.]/, '').toLowerCase() : '';
        var allowed = this._options.allowedExtensions;
        
        if (!allowed.length){return true;}        
        
        for (var i=0; i<allowed.length; i++){
            if (allowed[i].toLowerCase() == ext){ return true;}    
        }
        
        return false;
    },    
    _formatSize: function(bytes){
        var i = -1;                                    
        do {
            bytes = bytes / 1024;
            i++;  
        } while (bytes > 99);
        
        return Math.max(bytes, 0.1).toFixed(1) + ['kB', 'MB', 'GB', 'TB', 'PB', 'EB'][i];          
    }
};
    
       
/**
 * Class that creates upload widget with drag-and-drop and file list
 * @inherits qq.FileUploaderBasic
 */
qq.FileUploader = function(o){
    // call parent constructor
    qq.FileUploaderBasic.apply(this, arguments);
    
    // additional options    
    qq.extend(this._options, {
        element: null,
        // if set, will be used instead of qq-upload-list in template
        listElement: null,
                
        template: '<div class="qq-uploader">' + 
                '<div class="qq-upload-drop-area"><span>Drop files here to upload</span></div>' +
                '<div class="qq-upload-button">Upload</div>' +
                '<ul class="qq-upload-list"></ul>' + 
             '</div>',

        // template for one item in file list
        fileTemplate: '<li>' +
                '<span class="qq-upload-file"></span>' +
                '<span class="qq-upload-spinner"></span>' +
                '<span class="qq-upload-size"></span>' +
                '<a class="qq-upload-cancel" href="#">Cancel</a>' +
                '<span class="qq-upload-failed-text">Failed</span>' +
            '</li>',        
        
        classes: {
            // used to get elements from templates
            button: 'qq-upload-button',
            drop: 'qq-upload-drop-area',
            dropActive: 'qq-upload-drop-area-active',
            list: 'qq-upload-list',
                        
            file: 'qq-upload-file',
            spinner: 'qq-upload-spinner',
            size: 'qq-upload-size',
            cancel: 'qq-upload-cancel',

            // added to list item when upload completes
            // used in css to hide progress spinner
            success: 'qq-upload-success',
            fail: 'qq-upload-fail'
        }
    });
    // overwrite options with user supplied    
    qq.extend(this._options, o);       

    this._element = this._options.element;
    this._element.innerHTML = this._options.template;        
    this._listElement = this._options.listElement || this._find(this._element, 'list');
    
    this._classes = this._options.classes;
        
    this._button = this._createUploadButton(this._find(this._element, 'button'));        
    
    this._bindCancelEvent();
    this._setupDragDrop();
};

// inherit from Basic Uploader
qq.extend(qq.FileUploader.prototype, qq.FileUploaderBasic.prototype);

qq.extend(qq.FileUploader.prototype, {
    /**
     * Gets one of the elements listed in this._options.classes
     **/
    _find: function(parent, type){                                
        var element = qq.getByClass(parent, this._options.classes[type])[0];        
        if (!element){
            throw new Error('element not found ' + type);
        }
        
        return element;
    },
    _setupDragDrop: function(){
        var self = this,
            dropArea = this._find(this._element, 'drop');                        

        var dz = new qq.UploadDropZone({
            element: dropArea,
            onEnter: function(e){
                qq.addClass(dropArea, self._classes.dropActive);
                e.stopPropagation();
            },
            onLeave: function(e){
                e.stopPropagation();
            },
            onLeaveNotDescendants: function(e){
                qq.removeClass(dropArea, self._classes.dropActive);  
            },
            onDrop: function(e){
                dropArea.style.display = 'none';
                qq.removeClass(dropArea, self._classes.dropActive);
                self._uploadFileList(e.dataTransfer.files);    
            }
        });
                
        dropArea.style.display = 'none';

        qq.attach(document, 'dragenter', function(e){     
            if (!dz._isValidFileDrag(e)) return; 
            
            dropArea.style.display = 'block';            
        });                 
        qq.attach(document, 'dragleave', function(e){
            if (!dz._isValidFileDrag(e)) return;            
            
            var relatedTarget = document.elementFromPoint(e.clientX, e.clientY);
            // only fire when leaving document out
            if ( ! relatedTarget || relatedTarget.nodeName == "HTML"){               
                dropArea.style.display = 'none';                                            
            }
        });                
    },
    _onSubmit: function(id, fileName){
        qq.FileUploaderBasic.prototype._onSubmit.apply(this, arguments);
        this._addToList(id, fileName);  
    },
    _onProgress: function(id, fileName, loaded, total){
        qq.FileUploaderBasic.prototype._onProgress.apply(this, arguments);

        var item = this._getItemByFileId(id);
        var size = this._find(item, 'size');
        size.style.display = 'inline';
        
        var text; 
        if (loaded != total){
            text = Math.round(loaded / total * 100) + '% from ' + this._formatSize(total);
        } else {                                   
            text = this._formatSize(total);
        }          
        
        qq.setText(size, text);         
    },
    _onComplete: function(id, fileName, result){
        qq.FileUploaderBasic.prototype._onComplete.apply(this, arguments);

        // mark completed
        var item = this._getItemByFileId(id);                
        qq.remove(this._find(item, 'cancel'));
        qq.remove(this._find(item, 'spinner'));
        
        if (result.success){
            qq.addClass(item, this._classes.success);    
        } else {
            qq.addClass(item, this._classes.fail);
        }         
    },
    _addToList: function(id, fileName){
        var item = qq.toElement(this._options.fileTemplate);                
        item.qqFileId = id;

        var fileElement = this._find(item, 'file');        
        qq.setText(fileElement, this._formatFileName(fileName));
        this._find(item, 'size').style.display = 'none';        

        this._listElement.appendChild(item);
    },
    _getItemByFileId: function(id){
        var item = this._listElement.firstChild;        
        
        // there can't be txt nodes in dynamically created list
        // and we can  use nextSibling
        while (item){            
            if (item.qqFileId == id) return item;            
            item = item.nextSibling;
        }          
    },
    /**
     * delegate click event for cancel link 
     **/
    _bindCancelEvent: function(){
        var self = this,
            list = this._listElement;            
        
        qq.attach(list, 'click', function(e){            
            e = e || window.event;
            var target = e.target || e.srcElement;
            
            if (qq.hasClass(target, self._classes.cancel)){                
                qq.preventDefault(e);
               
                var item = target.parentNode;
                self._handler.cancel(item.qqFileId);
                qq.remove(item);
            }
        });
    }    
});
    
qq.UploadDropZone = function(o){
    this._options = {
        element: null,  
        onEnter: function(e){},
        onLeave: function(e){},  
        // is not fired when leaving element by hovering descendants   
        onLeaveNotDescendants: function(e){},   
        onDrop: function(e){}                       
    };
    qq.extend(this._options, o); 
    
    this._element = this._options.element;
    
    this._disableDropOutside();
    this._attachEvents();   
};

qq.UploadDropZone.prototype = {
    _disableDropOutside: function(e){
        // run only once for all instances
        if (!qq.UploadDropZone.dropOutsideDisabled ){

            qq.attach(document, 'dragover', function(e){
                if (e.dataTransfer){
                    e.dataTransfer.dropEffect = 'none';
                    e.preventDefault(); 
                }           
            });
            
            qq.UploadDropZone.dropOutsideDisabled = true; 
        }        
    },
    _attachEvents: function(){
        var self = this;              
                  
        qq.attach(self._element, 'dragover', function(e){
            if (!self._isValidFileDrag(e)) return;
            
            var effect = e.dataTransfer.effectAllowed;
            if (effect == 'move' || effect == 'linkMove'){
                e.dataTransfer.dropEffect = 'move'; // for FF (only move allowed)    
            } else {                    
                e.dataTransfer.dropEffect = 'copy'; // for Chrome
            }
                                                     
            e.stopPropagation();
            e.preventDefault();                                                                    
        });
        
        qq.attach(self._element, 'dragenter', function(e){
            if (!self._isValidFileDrag(e)) return;
                        
            self._options.onEnter(e);
        });
        
        qq.attach(self._element, 'dragleave', function(e){
            if (!self._isValidFileDrag(e)) return;
            
            self._options.onLeave(e);
            
            var relatedTarget = document.elementFromPoint(e.clientX, e.clientY);                      
            // do not fire when moving a mouse over a descendant
            if (qq.contains(this, relatedTarget)) return;
                        
            self._options.onLeaveNotDescendants(e); 
        });
                
        qq.attach(self._element, 'drop', function(e){
            if (!self._isValidFileDrag(e)) return;
            
            e.preventDefault();
            self._options.onDrop(e);
        });          
    },
    _isValidFileDrag: function(e){
        var dt = e.dataTransfer,
            // do not check dt.types.contains in webkit, because it crashes safari 4            
            isWebkit = navigator.userAgent.indexOf("AppleWebKit") > -1;                        

        // dt.effectAllowed is none in Safari 5
        // dt.types.contains check is for firefox            
        return dt && dt.effectAllowed != 'none' && 
            (dt.files || (!isWebkit && dt.types.contains && dt.types.contains('Files')));
        
    }        
}; 

qq.UploadButton = function(o){
    this._options = {
        element: null,  
        // if set to true adds multiple attribute to file input      
        multiple: false,
        // name attribute of file input
        name: 'file',
        onChange: function(input){},
        hoverClass: 'qq-upload-button-hover',
        focusClass: 'qq-upload-button-focus'                       
    };
    
    qq.extend(this._options, o);
        
    this._element = this._options.element;
    
    // make button suitable container for input
    qq.css(this._element, {
        position: 'relative',
        overflow: 'hidden',
        // Make sure browse button is in the right side
        // in Internet Explorer
        direction: 'ltr'
    });   
    
    this._input = this._createInput();
};

qq.UploadButton.prototype = {
    /* returns file input element */    
    getInput: function(){
        return this._input;
    },
    /* cleans/recreates the file input */
    reset: function(){
        if (this._input.parentNode){
            qq.remove(this._input);    
        }                
        
        qq.removeClass(this._element, this._options.focusClass);
        this._input = this._createInput();
    },    
    _createInput: function(){                
        var input = document.createElement("input");
        
        if (this._options.multiple){
            input.setAttribute("multiple", "multiple");
        }
                
        input.setAttribute("type", "file");
        input.setAttribute("name", this._options.name);
        
        qq.css(input, {
            position: 'absolute',
            // in Opera only 'browse' button
            // is clickable and it is located at
            // the right side of the input
            right: 0,
            top: 0,
            fontFamily: 'Arial',
            // 4 persons reported this, the max values that worked for them were 243, 236, 236, 118
            fontSize: '118px',
            margin: 0,
            padding: 0,
            cursor: 'pointer',
            opacity: 0
        });
        
        this._element.appendChild(input);

        var self = this;
        qq.attach(input, 'change', function(){
            self._options.onChange(input);
        });
                
        qq.attach(input, 'mouseover', function(){
            qq.addClass(self._element, self._options.hoverClass);
        });
        qq.attach(input, 'mouseout', function(){
            qq.removeClass(self._element, self._options.hoverClass);
        });
        qq.attach(input, 'focus', function(){
            qq.addClass(self._element, self._options.focusClass);
        });
        qq.attach(input, 'blur', function(){
            qq.removeClass(self._element, self._options.focusClass);
        });

        // IE and Opera, unfortunately have 2 tab stops on file input
        // which is unacceptable in our case, disable keyboard access
        if (window.attachEvent){
            // it is IE or Opera
            input.setAttribute('tabIndex', "-1");
        }

        return input;            
    }        
};

/**
 * Class for uploading files, uploading itself is handled by child classes
 */
qq.UploadHandlerAbstract = function(o){
    this._options = {
        debug: false,
        action: '/upload.php',
        // maximum number of concurrent uploads        
        maxConnections: 999,
        onProgress: function(id, fileName, loaded, total){},
        onComplete: function(id, fileName, response){},
        onCancel: function(id, fileName){}
    };
    qq.extend(this._options, o);    
    
    this._queue = [];
    // params for files in queue
    this._params = [];
};
qq.UploadHandlerAbstract.prototype = {
    log: function(str){
        if (this._options.debug && window.console) console.log('[uploader] ' + str);        
    },
    /**
     * Adds file or file input to the queue
     * @returns id
     **/    
    add: function(file){},
    /**
     * Sends the file identified by id and additional query params to the server
     */
    upload: function(id, params){
        var len = this._queue.push(id);

        var copy = {};        
        qq.extend(copy, params);
        this._params[id] = copy;        
                
        // if too many active uploads, wait...
        if (len <= this._options.maxConnections){               
            this._upload(id, this._params[id]);
        }
    },
    /**
     * Cancels file upload by id
     */
    cancel: function(id){
        this._cancel(id);
        this._dequeue(id);
    },
    /**
     * Cancells all uploads
     */
    cancelAll: function(){
        for (var i=0; i<this._queue.length; i++){
            this._cancel(this._queue[i]);
        }
        this._queue = [];
    },
    /**
     * Returns name of the file identified by id
     */
    getName: function(id){},
    /**
     * Returns size of the file identified by id
     */          
    getSize: function(id){},
    /**
     * Returns id of files being uploaded or
     * waiting for their turn
     */
    getQueue: function(){
        return this._queue;
    },
    /**
     * Actual upload method
     */
    _upload: function(id){},
    /**
     * Actual cancel method
     */
    _cancel: function(id){},     
    /**
     * Removes element from queue, starts upload of next
     */
    _dequeue: function(id){
        var i = qq.indexOf(this._queue, id);
        this._queue.splice(i, 1);
                
        var max = this._options.maxConnections;
        
        if (this._queue.length >= max){
            var nextId = this._queue[max-1];
            this._upload(nextId, this._params[nextId]);
        }
    }        
};

/**
 * Class for uploading files using form and iframe
 * @inherits qq.UploadHandlerAbstract
 */
qq.UploadHandlerForm = function(o){
    qq.UploadHandlerAbstract.apply(this, arguments);
       
    this._inputs = {};
};
// @inherits qq.UploadHandlerAbstract
qq.extend(qq.UploadHandlerForm.prototype, qq.UploadHandlerAbstract.prototype);

qq.extend(qq.UploadHandlerForm.prototype, {
    add: function(fileInput){
        fileInput.setAttribute('name', 'qqfile');
        var id = 'qq-upload-handler-iframe' + qq.getUniqueId();       
        
        this._inputs[id] = fileInput;
        
        // remove file input from DOM
        if (fileInput.parentNode){
            qq.remove(fileInput);
        }
                
        return id;
    },
    getName: function(id){
        // get input value and remove path to normalize
        return this._inputs[id].value.replace(/.*(\/|\\)/, "");
    },    
    _cancel: function(id){
        this._options.onCancel(id, this.getName(id));
        
        delete this._inputs[id];        

        var iframe = document.getElementById(id);
        if (iframe){
            // to cancel request set src to something else
            // we use src="javascript:false;" because it doesn't
            // trigger ie6 prompt on https
            iframe.setAttribute('src', 'javascript:false;');

            qq.remove(iframe);
        }
    },     
    _upload: function(id, params){                        
        var input = this._inputs[id];
        
        if (!input){
            throw new Error('file with passed id was not added, or already uploaded or cancelled');
        }                

        var fileName = this.getName(id);
                
        var iframe = this._createIframe(id);
        var form = this._createForm(iframe, params);
        form.appendChild(input);

        var self = this;
        this._attachLoadEvent(iframe, function(){                                 
            self.log('iframe loaded');
            
            var response = self._getIframeContentJSON(iframe);

            self._options.onComplete(id, fileName, response);
            self._dequeue(id);
            
            delete self._inputs[id];
            // timeout added to fix busy state in FF3.6
            setTimeout(function(){
                qq.remove(iframe);
            }, 1);
        });

        form.submit();        
        qq.remove(form);        
        
        return id;
    }, 
    _attachLoadEvent: function(iframe, callback){
        qq.attach(iframe, 'load', function(){
            // when we remove iframe from dom
            // the request stops, but in IE load
            // event fires
            if (!iframe.parentNode){
                return;
            }

            // fixing Opera 10.53
            if (iframe.contentDocument &&
                iframe.contentDocument.body &&
                iframe.contentDocument.body.innerHTML == "false"){
                // In Opera event is fired second time
                // when body.innerHTML changed from false
                // to server response approx. after 1 sec
                // when we upload file with iframe
                return;
            }

            callback();
        });
    },
    /**
     * Returns json object received by iframe from server.
     */
    _getIframeContentJSON: function(iframe){
        // iframe.contentWindow.document - for IE<7
        var doc = iframe.contentDocument ? iframe.contentDocument: iframe.contentWindow.document,
            response;
        
        this.log("converting iframe's innerHTML to JSON");
        this.log("innerHTML = " + doc.body.innerHTML);
                        
        try {
            response = eval("(" + doc.body.innerHTML + ")");
        } catch(err){
            response = {};
        }        

        return response;
    },
    /**
     * Creates iframe with unique name
     */
    _createIframe: function(id){
        // We can't use following code as the name attribute
        // won't be properly registered in IE6, and new window
        // on form submit will open
        // var iframe = document.createElement('iframe');
        // iframe.setAttribute('name', id);

        var iframe = qq.toElement('<iframe src="javascript:false;" name="' + id + '" />');
        // src="javascript:false;" removes ie6 prompt on https

        iframe.setAttribute('id', id);

        iframe.style.display = 'none';
        document.body.appendChild(iframe);

        return iframe;
    },
    /**
     * Creates form, that will be submitted to iframe
     */
    _createForm: function(iframe, params){
        // We can't use the following code in IE6
        // var form = document.createElement('form');
        // form.setAttribute('method', 'post');
        // form.setAttribute('enctype', 'multipart/form-data');
        // Because in this case file won't be attached to request
        var form = qq.toElement('<form method="post" enctype="multipart/form-data"></form>');

        var queryString = qq.obj2url(params, this._options.action);

        form.setAttribute('action', queryString);
        form.setAttribute('target', iframe.name);
        form.style.display = 'none';
        document.body.appendChild(form);

        return form;
    }
});

/**
 * Class for uploading files using xhr
 * @inherits qq.UploadHandlerAbstract
 */
qq.UploadHandlerXhr = function(o){
    qq.UploadHandlerAbstract.apply(this, arguments);

    this._files = [];
    this._xhrs = [];
    
    // current loaded size in bytes for each file 
    this._loaded = [];
};

// static method
qq.UploadHandlerXhr.isSupported = function(){
    var input = document.createElement('input');
    input.type = 'file';        
    
    return (
        'multiple' in input &&
        typeof File != "undefined" &&
        typeof (new XMLHttpRequest()).upload != "undefined" );       
};

// @inherits qq.UploadHandlerAbstract
qq.extend(qq.UploadHandlerXhr.prototype, qq.UploadHandlerAbstract.prototype)

qq.extend(qq.UploadHandlerXhr.prototype, {
    /**
     * Adds file to the queue
     * Returns id to use with upload, cancel
     **/    
    add: function(file){
        if (!(file instanceof File)){
            throw new Error('Passed obj in not a File (in qq.UploadHandlerXhr)');
        }
                
        return this._files.push(file) - 1;        
    },
    getName: function(id){        
        var file = this._files[id];
        // fix missing name in Safari 4
        return file.fileName != null ? file.fileName : file.name;       
    },
    getSize: function(id){
        var file = this._files[id];
        return file.fileSize != null ? file.fileSize : file.size;
    },    
    /**
     * Returns uploaded bytes for file identified by id 
     */    
    getLoaded: function(id){
        return this._loaded[id] || 0; 
    },
    /**
     * Sends the file identified by id and additional query params to the server
     * @param {Object} params name-value string pairs
     */    
    _upload: function(id, params){
        var file = this._files[id],
            name = this.getName(id),
            size = this.getSize(id);
                
        this._loaded[id] = 0;
                                
        var xhr = this._xhrs[id] = new XMLHttpRequest();
        var self = this;
                                        
        xhr.upload.onprogress = function(e){
            if (e.lengthComputable){
                self._loaded[id] = e.loaded;
                self._options.onProgress(id, name, e.loaded, e.total);
            }
        };

        xhr.onreadystatechange = function(){            
            if (xhr.readyState == 4){
                self._onComplete(id, xhr);                    
            }
        };

        // build query string
        params = params || {};
        params['qqfile'] = name;
        var queryString = qq.obj2url(params, this._options.action);

        xhr.open("POST", queryString, true);
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhr.setRequestHeader("X-File-Name", encodeURIComponent(name));
        xhr.setRequestHeader("Content-Type", "application/octet-stream");
        xhr.send(file);
    },
    _onComplete: function(id, xhr){
        // the request was aborted/cancelled
        if (!this._files[id]) return;
        
        var name = this.getName(id);
        var size = this.getSize(id);
        
        this._options.onProgress(id, name, size, size);
                
        if (xhr.status == 200){
            this.log("xhr - server response received");
            this.log("responseText = " + xhr.responseText);
                        
            var response;
                    
            try {
                response = eval("(" + xhr.responseText + ")");
            } catch(err){
                response = {};
            }
            
            this._options.onComplete(id, name, response);
                        
        } else {                   
            this._options.onComplete(id, name, {});
        }
                
        this._files[id] = null;
        this._xhrs[id] = null;    
        this._dequeue(id);                    
    },
    _cancel: function(id){
        this._options.onCancel(id, this.getName(id));
        
        this._files[id] = null;
        
        if (this._xhrs[id]){
            this._xhrs[id].abort();
            this._xhrs[id] = null;                                   
        }
    }
});;var map;
var mapbutton;
var infoWindow;
var overlays = new Array();
var mapdiv = 'gmap3';
var tool = 'recentre';
var points = new Array();
var vertices = 35;
var paths = new Array();

function s2_drawmap(el) 		{ gm3_drawmap(el); }  
function s2_creategmap(parent){ gm3_creategmap(parent); }
function s2_initgmap() 			{ gm3_initgmap(); }
function s2_assignmap() 		{ gm3_assignmap(); }

function S2GM3Polygon(opts) {
   this.setOptions(opts);
   this.s2name = null;
   this.s2center = null;
   this.s2area = null;
   this.s2paths = null;
   this.s2wkt = null;
   this.s2box = null;
   this.s2bbarea = null;
   this.s2issimple = null;
   this.s2parents = null;
   this.gets2name = function() {
      if (this.s2name == null) this.s2name = gm3_mpoly_getname(this);
	   return this.s2name;
   }
   this.gets2center = function() {
      if (this.s2center == null) this.s2center = gm3_getmultipolycenter(this);
	   return this.s2center;
   }  
   this.gets2area = function() {
	   if (this.s2area == null) this.s2area = gm3_mpolygetarea(this);
	   return this.s2area;
   }
   this.gets2paths = function() {
      if (this.s2paths == null) this.s2paths = gm3_mpoly_to_arrays(this);
	   return this.s2paths;
   }
   this.gets2wkt = function() {
      if (this.s2wkt == null) this.s2wkt = gm3_mpwkt_from_arrays(this.gets2paths(),4326);
	   return this.s2wkt;
   }
   this.gets2box = function() {
      if (this.s2box == null) this.s2box = gm3_getmpolybounds(this);
	   return this.s2box;
   }
   this.gets2bbarea = function() {
	   if (this.s2area == null) this.s2bbarea = gm3_mpolygetbbarea(this);
	   return this.s2bbarea;
   }   
   this.gets2issimple = function() {
      if (this.s2issimple == null) this.s2issimple = gm3_mpolyissimple(this);
	   return this.s2issimple;
   }
   this.gets2parents = function() {
      if (this.s2parents == null) this.s2parents = gm3_get_parents(this);
	   return this.s2parents;
   }
}
S2GM3Polygon.prototype = new google.maps.Polygon();


function gm3_drawmap(el) {
   var wktnode,polygon;
   mapbutton = el.replace(/^[^_]+_/,'');
   wktnode = document.getElementById(mapbutton);
   polygon;
   if (wktnode && wktnode.value) polygon = gm3_readwkt(wktnode.value);
   cleardialog('dialog-liner');
   var dialog = document.getElementById('dialog-liner');
   var mapcont = document.createElement('DIV');
   mapcont.id = 'mapcontainer';
   dialog.appendChild(mapcont)
   showdialog();
   gm3_creategmap('mapcontainer');
}  
function gm3_creategmap(parent) {
   var pnode = document.getElementById(parent);
   var d = pnode.parentNode;
   var tools,map,div,toolw,mapw,button;
   pnode.className = 'mapcontainer';
   var w = s2_eldims(pnode,'width');
   toolw = 180;
   mapw = w - toolw - 30;
   
   tools = s2_getmaptoolstable();
   tools.style.width = toolw+'px';
   div = document.createElement('DIV');
   div.style.width = mapw+'px';
   div.className = 'mapdiv';
   div.id = mapdiv;
   pnode.appendChild(tools);
   pnode.appendChild(div);
   
   gm3_initgmap();
   s2_setmaptool('recentre');
   s2_showlocation();

   d.appendChild(document.createElement('HR'));
   button = document.createElement('BUTTON'); 
   button.className = 'dbutton';
   button.onclick = function() {hidedialog();};
   button.appendChild(document.createTextNode('Cancel'));
   d.appendChild(button);             

   button = document.createElement('BUTTON');
   button.className = 'dbutton';
   button.onclick = function() {gm3_assignmap();};
   button.appendChild(document.createTextNode('OK'));
   d.appendChild(button);
   d.appendChild(document.createElement('HR'));             
}
function gm3_initgmap() {
   var md = document.getElementById(mapdiv); 
   try {
      var myLatLng = new google.maps.LatLng(53.38, -1.46);
      var myOptions = {
         zoom: 12,
         center: myLatLng,
         panControl: false,
         zoomControl: true,
         mapTypeControl: true,
         scaleControl: true,
         streetViewControl: true,
         overviewMapControl: true,
         mapTypeId: google.maps.MapTypeId.SATELLITE
      };
      map = new google.maps.Map(md,myOptions);
      google.maps.event.addListener(map, 'click', function(e) {s2_clickmap(e);});
      if (overlays['boundary']) {
         overlays['boundary'].setMap(map);
         s2_fittomap();
      }
   } catch (e) {
      md.appendChild(document.createTextNode('Google Maps: Initialise failed.'));
   }
}
function gm3_readwkt(wkt) {
   var b;
   if (wkt.match(/POINT\(/)) b = gm3_wkt_to_point(wkt);
   else b = gm3_wkt_to_mpoly(wkt);
   overlays['boundary'] = b;
   return b; 
}
function gm3_getwktfromoverlay(overlay) {
   var wkt,mps,p;
   if (overlay instanceof S2GM3Polygon) wkt = overlay.gets2wkt();
   else if (overlay instanceof google.maps.Polygon) {
      mps = gm3_mpoly_to_arrays(overlay);
	   wkt = gm3_mpwkt_from_arrays(mps,4326);
   } else if (overlay instanceof google.maps.Circle) {
	   path = s2_getcirclevertices(overlay);
      overlay = new S2GM3Polygon({paths: path});
	   mps = gm3_mpoly_to_arrays(overlay);
	   wkt = gm3_mpwkt_from_arrays(mps,4326);
   } else if (overlay instanceof google.maps.Marker) {
	   p = overlay.getPosition();
	   wkt = 'POINT('+ p.lng() + ' ' + p.lat() + ')';	  
   }
   return wkt;
}
function gm3_getnamefromoverlay(overlay) {
   var name;
   if (overlay instanceof S2GM3Polygon) name = overlay.gets2name();
   else if (overlay instanceof google.maps.Polygon) name = gm3_mpoly_getname(overlay);
   else if (overlay instanceof google.maps.Circle) name = gm3_mpoly_getname(overlay);
   else if (overlay instanceof google.maps.Marker) name = overlay.getTitle();
   return name;
}
function gm3_mpoly_getname(polygon) {
   var wkt,p,ddc,gr,ngr,ptext,pname,parea,path,zoom,issimple;
   try {
      if (polygon instanceof google.maps.Circle) {
         path = s2_getcirclevertices(polygon);
         //polygon = new google.maps.Polygon({paths: path});
         polygon = new S2GM3Polygon({paths: path});
         polygon.setMap(map);
      }
      p = gm3_getmultipolycenter(polygon);
      ddc = new DegreesDecimalCoordinate(p.lat(),p.lng());
      gr = s2_ll_to_gr(ddc);
      gr.accuracy = 10;
      gr.easting = Math.floor(gr.easting/gr.accuracy)*gr.accuracy;
      gr.northing = Math.floor(gr.northing/gr.accuracy)*gr.accuracy;
      ngr = s2_gr_to_ngr(gr);
      issimple = (polygon instanceof S2GM3Polygon)?polygon.gets2issimple():gm3_mpolyissimple(polygon);
      if (issimple) parea = (polygon instanceof S2GM3Polygon)?polygon.gets2area():gm3_mpolygetarea(polygon);
      else parea = (polygon instanceof S2GM3Polygon)?polygon.gets2bbarea():gm3_mpolygetbbarea(polygon);
      if (parea > 900000) ptext = Math.round(parea/100000)/10+' km�';
      else if (parea > 9000) ptext = Math.round(parea/1000)/10+' ha';
      else ptext = Math.round(parea*10)/10+' m�';
      if (!issimple) ptext = '[BB:' + ptext + ']';
      ptext += ' @ '+ngr.asString();  
   } catch(err) {
      ptext = 'Google Maps not available in offline mode'; 
   }
   return ptext;
}
function gm3_mpolyissimple(p) {
	var mps,cp,vs,issimple,maxsimple;
	issimple = true;
	maxsimple = 100;
	mps = (p instanceof S2GM3Polygon)?p.gets2paths():gm3_mpoly_to_arrays(p);
	if ((mps > 1)) {issimple = false;}
	else {
		vs = 0;
		for(cp in mps) {vs += mps[cp].length;}
		if (vs > maxsimple) issimple = false;
	}
	return issimple;
}
function gm3_mpolygetbbarea(p) {
// split polygon into paths,
// get area for each path 
// add to total area or remove if path is a hole.
   var bb,sw,ne,dy,dx;
   bb = (p instanceof S2GM3Polygon)?p.gets2box():gm3_getmpolybounds(p);
   sw = bb.getSouthWest();
   sw = s2_gmp_to_ll(sw);
   sw = s2_ll_to_gr(sw);
   ne = bb.getNorthEast();
   ne = s2_gmp_to_ll(ne);
   ne = s2_ll_to_gr(ne);
   a = (ne.easting - sw.easting) * (ne.northing - sw.northing);
   return a;
}

function gm3_mpolygetarea(p) {
// split polygon into paths,
// get area for each path 
// add to total area or remove if path is a hole.
   var mps,par,i,a,ax,px;
   mps = (p instanceof S2GM3Polygon)?p.gets2paths():gm3_mpoly_to_arrays(p);
   if (mps.length > 1) par = (p instanceof S2GM3Polygon)?p.gets2parents():gm3_get_parents(mps);
   else par = new Array(new Array());
   a = 0;
   for (i in mps) {
      path = mps[i];
      px = gm3_poly_from_ring(path);
      //ax = s2gmapv3getarea(px.getPath().getArray());
      ax = google.maps.geometry.spherical.computeArea(px.getPath());
      // if it has an odd number of parents, it's a hole
      is_hole = ((par[i].length % 2) == 1);
      a = (is_hole)?a-ax:a+ax;
   }
   return a;
}
function gm3_mpoly_to_arrays(p) {
   var p,c,cx,cy,maxx,maxy,minx,miny,sw,ne,mps,mpp; 
   var path,paths,cxs,px,r,e,rs,rx,m;
   mps = [];
   try {
      paths = p.getPaths();
      cxs = [];
      rs = paths.getLength();
      for(rx=0;rx<rs;rx++) {
         path = paths.getAt(rx).getArray();
         mpp = [];
         for (p in path) {
            c = path[p];
            mpp.push([c.lng(),c.lat()]);
         }
         mps.push(mpp);
      }
      
   } catch (e) { /* add handler */ }
   return mps; 
}


function gm3_reorder_paths(mps) {
   var i,is_hole,is_clockwise,path,rpath,parents;
   parents = gm3_get_parents(mps);
   for (i in parents) {
      path = mps[i];
      // if it has an odd number of parents, it's a hole
      is_hole = ((parents[i].length % 2) == 1);
      is_clockwise = gm3_path_is_clockwise(path);
      if ((is_hole && is_clockwise)||(!is_hole && !is_clockwise)) path.reverse();
      mps[i] = path;
   }
   return mps;
}
function gm3_get_parents(mps) {
   var parents,polys,i,pi,j,pj;
   parents = new Array();
   polys = new Array();
   for(i in mps) {
      polys[i] = gm3_poly_from_ring(mps[i]);
   }
   for(i in polys) {
      parents[i] = new Array();
      pi = polys[i];
      for (j in polys) {
         pj = polys[j];
         if (gm3_p1_contains_p2(pj,pi)) parents[i].push(j);
      }
   }
   return parents;
}
function gm3_show_path_directions(mps) {
   var px,pathx,dir;
   for(px in mps) {
      pathx = mps[px];
      dir = (gm3_path_is_clockwise(pathx))?'clockwise':'anti-clockwise';
      alert('Path ' + px + ': ' + dir);
   }
}
function gm3_poly_from_ring(ring) {
   var p,path,i,xy,grp,llp,ll;
   path = new Array();
   for (i in ring) {
      xy = ring[i];
      ll = new google.maps.LatLng(xy[1],xy[0]);
      path.push(ll);
   }
   //p = new google.maps.Polygon({
   p = new S2GM3Polygon({
      paths: path,
      clickable: false
   });
   return p;
}
function gm3_p1_contains_p2(p1,p2) {
   // For a p1 to contain p2 
   //       all p2 vertices fall within p1 
   // and   all p1 vertices fall outside p2
   var ppath,i,pi,is_inside;
   ppath = p2.getPath().getArray();
   is_inside = true;
   for (i in ppath) {
      pi = ppath[i];
      is_inside &= (google.maps.geometry.poly.containsLocation(pi,p1));
	   if (!is_inside) break;
   }
   if (is_inside) {
	   ppath = p1.getPath().getArray();
      for (i in ppath) {
         pi = ppath[i];
         is_inside &= (!google.maps.geometry.poly.containsLocation(pi,p2));
		   if (!is_inside) break;
      }
   }
   return is_inside;
}
function gm3_path_is_clockwise(path) {
   var i,ptc,ptm,ptn,vx,theta,thetam,thetan,thetat;
   ptc = gm3_get_path_center(path);
   vx = path.length-1;
   thetat = 0;
   for (i=0;i<vx;i++) {
      ptm = path[i];
      ptn = path[((i+1)%vx)];
      if(ptm != ptn) {
         thetam = gm3_get_line_gradient(ptc,ptm);
         thetan = gm3_get_line_gradient(ptc,ptn);
         theta = thetan - thetam;
         // each pair of vertices is joined by a straight line to form a polygon
         // so if you have an angle > 180 degrees you need the other side of the 
         // circle.
         if (Math.abs(theta)>180) theta = (theta>0)?(theta-360):(theta+360);
         thetat += theta;
      }
   }
   return (thetat > 0); 
}
function gm3_get_line_gradient(ptc,ptn) {  
   var cx,cy,nx,ny,dx,dy,theta,r2d,gp1,gp2;
   gp1 = new google.maps.LatLng(ptc[1],ptc[0]);
	gp2 = new google.maps.LatLng(ptn[1],ptn[0]);
   /*               
   r2d = 180/Math.PI;
   cx = ptc[0];cy = ptc[1];
   nx = ptn[0];ny = ptn[1];
   dx = cx - nx;
   dy = ny - cy;
   if (dx == 0 && dy == 0) theta = 0; 
   else if (dy == 0) theta = (dx > 0)?180:0;
   else if (dx == 0) theta = (dy > 0)?90:-90;
   else {
      theta = Math.atan(dy/dx) * r2d;
      if (dx<0) theta += 180;
   }
   */
   theta = google.maps.geometry.spherical.computeHeading(gp1,gp2);
   return theta;
}
function gm3_get_path_center(path) {
   var i,cx,cy,ax,ay,tx,ty,ptx,vx;
   vx = path.length-1;
   tx = 0;ty = 0;
   for (i=0;i<vx;i++) {
      ptx = path[i];
      cx = ptx[0];
      cy = ptx[1];
      tx+= cx;
      ty+= cy;
   }
   ax = tx/vx;
   ay = ty/vx;
   return [ax,ay];
}
function gm3_arrays_from_mpwkt(wkt) {
   var pairs,p,pt,xy,ll,polygon,rings,r,rg,grp,llp,epsg,err,h1,h2,p0,p1,p2,simp,dis;
   try {epsg = parseInt(wkt.replace(/SRID\=(\d+)\;.*/,'$1'));} 
   catch (err) {epsg = 4326;}
   try {
      ll = new google.maps.LatLng(0,0);  
      if (wkt) {
         paths = new Array();
         points = new Array();
         wkt = wkt.replace(/^[^\(]+\(+/,'');
         wkt = wkt.replace(/\)+$/,'');
         rings = wkt.split(/\)+\,\(+/);
         for(r in rings) {
            rg = rings[r];
            pairs = rg.split(/\,/);
			   simp = (pairs.length>200);
            for(p in pairs) { 
               pt = pairs[p];
               xy = pt.split(/\s+/);
               switch(epsg) {
               case 27700: {
                  llp = conv_uk_ings_to_ll(new GridReference(xy[0],xy[1]));
                  xy = new Array(llp.lng(),llp.lat());  
               }break;
               }
			      /* to simplify the polygon remove overwrite points where 
				      the next point is on roughly the same heading */
			      if (simp && p1) {
				      gp1 = new google.maps.LatLng(p1[1],p1[0]);
				      gp2 = new google.maps.LatLng(xy[1],xy[0]);
                  dis = google.maps.geometry.spherical.computeDistanceBetween(gp1,gp2);
                  if (dis < 10) p2 = points.pop(); 
                  else {
                     if (h2) h1 = h2;
   			         //h2 = gm3_get_line_gradient(p1,xy);
                     h2 = google.maps.geometry.spherical.computeHeading(gp1,gp2);
                     if (h1 && h2) if (Math.abs(h1-h2)<=10) p2 = points.pop();
                  }
               }
			      points.push(xy);
			      p1 = xy;					
            }
     // alert('o: ' + pairs.length + ' s: ' + points.length);
		      /* if not closed, close polygon by adding first point */
            if (JSON.stringify(points[0]) != JSON.stringify(points[points.length-1])) {
               points.push(points[0]);
            }
            paths.push(points);
            points = new Array();
         }
      }
   } catch(err) {/* add something here. */}
   return paths;         
}
function gm3_mpwkt_from_arrays(rings,epsg) {
   var r,vertices,v,coords,c,x,y,wkt,scords,svertices,srings,sv,sr;
   srings = new Array();
   for(r in rings) {
      vertices = rings[r];
      scoords = new Array();
      for (v in vertices) {
         coords = vertices[v];
         switch(epsg) {
         case 27700: {
            llp = conv_uk_ings_to_ll(new GridReference(coords[0],coords[1]));
            coords = new Array(llp.lng(),llp.lat());   
         }break;
         }
         scoords.push(coords.join(' '));
      }
      if (scoords[0] != scoords[scoords.length-1]) scoords.push(scoords[0]);
      srings.push('(' + scoords.join(',') + ')');
   } 
   wkt = 'POLYGON(' + srings.join(',') + ')';
   return wkt;  
}
function gm3_wkt_to_mpoly(wkt) {
   var pairs,p,pt,xy,ll,polygon,rings,r,rg,grp,llp,epsg,err,mps;
   mps = gm3_arrays_from_mpwkt(wkt);
   if (mps.length > 1) mps = gm3_reorder_paths(mps);
   //wkt = gm3_mpwkt_from_arrays(mps);
   polygon = null;
   try {epsg = parseInt(wkt.replace(/SRID\=(\d+)\;.*/,'$1'));} 
   catch (err) {epsg = 4326;}
   try {
      ll = new google.maps.LatLng(0,0);  
      if(mps) {
         paths = new Array();
         points = new Array();
         for (r in mps) {
            ring = mps[r];
            for (p in ring) {
               xy = ring[p];
               ll = new google.maps.LatLng(xy[1],xy[0]);
               points.push(ll);
            }
            paths.push(points);
            points = new Array();
         }
		 //google.maps.Polygon({
         polygon = new S2GM3Polygon({ 
            paths: paths,
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 0.5,
            fillColor: "#FF0000",
            fillOpacity: 0.35,
            editable: false
         });
      }
   } catch(err) { /* add something here. */ }
   return polygon;   
}
function gm3_getmpolybounds(p) {
   var p,c,cx,cy,maxx,maxy,minx,miny,sw,ne; 
   var path,paths,cxs,px,r,e,rs,rx,m;
   try {
      paths = p.getPaths();
      cxs = new Array();
      rs = paths.getLength();
      for(rx=0;rx<rs;rx++) {
         path = paths.getAt(rx).getArray();
         for (p in path) {
            c = path[p];
            maxx = (maxx)?Math.max(maxx,c.lng()):c.lng();
            minx = (minx)?Math.min(minx,c.lng()):c.lng();
            maxy = (maxy)?Math.max(maxy,c.lat()):c.lat();
            miny = (miny)?Math.min(miny,c.lat()):c.lat();
         }
      }
      sw = new google.maps.LatLng(miny,minx);
      ne = new google.maps.LatLng(maxy,maxx);
      c = new google.maps.LatLngBounds(sw,ne);
   } catch (e) { /* add handler */ }
   return c; 
}
function gm3_getmultipolycenter(p) {
   var p,path,paths,cxs,px,r,c,e,rs,rx,m;
   try {
      paths = p.getPaths();
      cxs = new Array();
      rs = paths.getLength();
      for(rx=0;rx<rs;rx++) {
         path = paths.getAt(rx).getArray();
         r = new google.maps.Polygon({paths: path});
         cxs.push(s2_getpolycenter(r));
      } 
      r = new google.maps.Polygon({paths: cxs});
      c = s2_getpolycenter(r);
   } catch (e)  { /* what to do here */ }
   return c;
}
function s2_drawmultipoly(paths) {
   var p,c;
   if (overlays['boundary']) {
      p = overlays['boundary'];
      p.setMap(null);
   }
   //p = new google.maps.Polygon({
   p = new S2GM3Polygon({
      paths: paths,
      strokeColor: "#FF0000",
      strokeOpacity: 0.8,
      strokeWeight: 0.5,
      fillColor: "#FF0000",
      fillOpacity: 0.35,
      clickable: true
   });
   google.maps.event.addListener(p, 'click', function(e) {s2_clickmap(e);});
   p.setMap(map);
   overlays['boundary'] = p;
   c = gm3_getmultipolycenter(p);
   s2_showlocation(c);
   s2_fittomap();
}

function s2_fittomap() {
   var polygon;
   if (map && overlays['boundary']) {
      polygon = overlays['boundary'];
      var bnd = gm3_getmpolybounds(polygon);
      var ctr = bnd.getCenter();
      map.setCenter(ctr);
      map.fitBounds(bnd);  
   }
}

function s2_eldims(el,dim) {
   var r,x,y;
   x = (el.style && parseInt(el.style.width)>0)?parseInt(el.style.width):(el.offsetWidth>0)?el.offsetWidth:el.clientWidth;   
   y = (el.style && parseInt(el.style.height)>0)?parseInt(el.style.height):(el.offsetHeight>0)?el.offsetHeight:el.clientHeight;
   switch(dim) {
   case 'width':  r = x; break;
   case 'height': r = y; break;
   default: r = new Array(x,y); break; 
   }
   return r;   
}
function s2_setmaptool(t) {
   var x;
   switch(t) {
   case 'recentre': break;
   default: {
      var b = overlays['boundary'];
      if (b) {
         //map.removeOverlay(b);
         b.setMap(null);
         delete overlays['boundary'];
         points = new Array();
      }
   }break;
   } 
   x = document.getElementById(tool);
   if (x) x.className = 'tool';
   tool = t;
   if (x) {
      x = document.getElementById(tool);
      x.className = 'toolx';
   }
   s2_showtooloptions(t);
   s2_showlocation();
   return true;
}
function s2_showtooloptions(t) {
   var o = document.getElementById('tooloptions');
   var mt = document.getElementById('maptools');
   if (mt) {
      var div,h4,input,table,tbody,tr,th,td;
      var div = document.createElement('DIV');
      div.id = 'tooloptions';
      h4 = document.createElement('H4');
      h4.appendChild(document.createTextNode('Options'));
      div.appendChild(h4);
      table = document.createElement('TABLE');
      tbody = document.createElement('TBODY');
      
      switch(t) {
      case 'circle': {
         tr = document.createElement('TR');
         td = document.createElement('TD');
         td.appendChild(document.createTextNode('Radius (m)'));
         tr.appendChild(td);
         td = document.createElement('TD');
         input = document.createElement('INPUT');
         input.id = 'circle_radius';
         input.type = 'text';
         input.value = 1000;
         td.appendChild(input);
         tr.appendChild(td);
         tbody.appendChild(tr);
      }break;
      case 'square': {
         tr = document.createElement('TR');
         td = document.createElement('TD');
         td.appendChild(document.createTextNode('Grid size (m)'));
         tr.appendChild(td);
         td = document.createElement('TD');
         input = document.createElement('INPUT');
         input.id = 'square_side';
         input.type = 'text';
         input.value = 1000;
         td.appendChild(input);
         tr.appendChild(td);
         tbody.appendChild(tr);
      }break;
      }
   
      tr = document.createElement('TR');
      td = document.createElement('TD');
      td.appendChild(document.createTextNode('NGR'));
      tr.appendChild(td);
      td = document.createElement('TD');
      input = document.createElement('INPUT');
      input.id = 'ngr';
      input.type = 'text';
      input.onchange = function() {s2_movetongr(this.value);};
      td.appendChild(input);
      tr.appendChild(td);
      tbody.appendChild(tr);
   
      tr = document.createElement('TR');
      td = document.createElement('TD');
      td.appendChild(document.createTextNode('LL'));
      tr.appendChild(td);
      td = document.createElement('TD');
      input = document.createElement('INPUT');
      input.id = 'latlng';
      input.type = 'text';
      input.onchange = function() {s2_movetoll(this.value);};
      td.appendChild(input);
      tr.appendChild(td);
      tbody.appendChild(tr);
   
      table.appendChild(tbody);
      div.appendChild(table);
      if (o) mt.replaceChild(div,o);
      else mt.appendChild(div);
   }
}
function s2_getmaptoolstable() {
   var div,h4,table,tbody,div;
   div = document.createElement('DIV');
   div.className = 'maptools';
   div.id = 'maptools';
   h4 = document.createElement('H4');
   h4.appendChild(document.createTextNode('Tools'));
   div.appendChild(h4);
   table = document.createElement('TABLE');
   tbody = document.createElement('TBODY');
   tbody.appendChild(s2_getrecentre_tooltr());
   tbody.appendChild(s2_getcircle_tooltr());
   tbody.appendChild(s2_getgridsquare_tooltr());
   tbody.appendChild(s2_getpolygon_tooltr());
   tbody.appendChild(s2_geteditvertices_tooltr());
   table.appendChild(tbody);
   div.appendChild(table);
   return div; 
}
function s2_getrecentre_tooltr() {
   var tr,th,node,text,input,img,button;
   tr = document.createElement('TR');
   td = document.createElement('TD');
   button = document.createElement('BUTTON');
   button.className = 'toolx';
   img = document.createElement('IMG');
   img.src = '../images/recentre.gif';
   img.alt = 'Re-centre';
   button.appendChild(img);
   button.id = 'recentre'; 
   button.onclick = function() {s2_setmaptool(this.id);};
   td.appendChild(button);
   tr.appendChild(td);
   td = document.createElement('TD');
   td.appendChild(document.createTextNode('Recentre'));
   tr.appendChild(td);
   return tr;
}
function s2_getcircle_tooltr() {
   var tr,th,node,text,input,img,button;
   tr = document.createElement('TR');
   td = document.createElement('TD');
   button = document.createElement('BUTTON');
   button.className = 'tool';
   img = document.createElement('IMG');
   img.src = '../images/circle.gif';
   img.alt = 'Point and radius';
   button.appendChild(img);
   button.id = 'circle'; 
   button.onclick = function() {s2_setmaptool(this.id);};
   td.appendChild(button);
   tr.appendChild(td);
   td = document.createElement('TD');
   td.appendChild(document.createTextNode('Radius'));
   tr.appendChild(td);
   return tr;
}
function s2_getgridsquare_tooltr() {
   var tr,th,node,text,input,img,button;
   tr = document.createElement('TR');
   td = document.createElement('TD');
   button = document.createElement('BUTTON');
   button.className = 'tool';
   img = document.createElement('IMG');
   img.src = '../images/square.gif';
   img.alt = 'Grid Square';
   button.appendChild(img);
   button.id = 'square'; 
   button.onclick = function() {s2_setmaptool(this.id);};
   td.appendChild(button);
   tr.appendChild(td);
   td = document.createElement('TD');
   td.appendChild(document.createTextNode('Gridsquare'));
   tr.appendChild(td);
   return tr;
}
function s2_getpolygon_tooltr() {
   var tr,th,node,text,input,img,button;
   tr = document.createElement('TR');
   td = document.createElement('TD');
   button = document.createElement('BUTTON');
   button.className = 'tool';
   img = document.createElement('IMG');
   img.src = '../images/polygon.gif';
   img.alt = 'Polygon';
   button.appendChild(img);
   button.id = 'polygon'; 
   button.onclick = function() {s2_setmaptool(this.id);};
   td.appendChild(button);
   tr.appendChild(td);
   td = document.createElement('TD');
   td.appendChild(document.createTextNode('Polygon'));
   tr.appendChild(td);
   return tr;
}
function s2_geteditvertices_tooltr() {
   var tr,th,node,text,input;
   tr = document.createElement('TR');
   td = document.createElement('TD');
   td.className = 's2gmaptdbutton';
   input = document.createElement('INPUT');
   input.type = 'checkbox';
   input.id = 's2gmapcheck';
   input.className = 's2gmapcheck';
   input.onclick = function() {s2_editpoly(this.checked);};
   
   td.appendChild(input);
   tr.appendChild(td);
   td = document.createElement('TD');
   td.appendChild(document.createTextNode('Edit Poly'));
   tr.appendChild(td);
   return tr;
}
function s2_editpoly(makeeditable) {
   var p,vc,v1,vn;
   if (overlays['boundary']) {
      p = overlays['boundary'];
      if (makeeditable) {
         //vc = p.getVertexCount();
         //v1 = p.getVertex(0);
         //vn = p.getVertex(vc-1);
         //if (v1 != vn) p.insertVertex(vc,v1); 
         p.setEditable(true);
      }
      else p.setEditable(false);
   }
}

function s2_moveto(point) {
   map.setCenter(point);
   return true;
}
function s2_movetopolycenter() {
   var c = s2_getpolycenter();
   s2_moveto(c);
   return c;
}
function s2_movetongr(gridref) {
   var gr,ll,ctr;
   if (gridref) {
      gr = s2_gr_from_ngrstring(gridref);
      ll = s2_gr_to_ll(gr);
      ctr = s2_ll_to_gmp(ll);
      s2_moveto(ctr);
   }
   return ctr;
}
function s2_gr_from_ngrstring(gridref) {
   var bits,sq,en,e,n,sx,res,ngr,gr,ll,ctr;
   if (gridref) {
      gridref = gridref.toUpperCase();
      gridref = gridref.replace(/(\w{2})(\d+)(\w*)/,'$1-$2-$3');
      bits = gridref.split(/\-/);
      sq = bits[0];
      en = bits[1];
      e = parseInt(en.substr(0,Math.floor(en.length/2)));
      n = parseInt(en.substr(Math.floor(en.length/2),Math.floor(en.length/2)));
      sx = bits[2];
      res = Math.pow(10,(5 - Math.floor(en.length/2)));
      e *= res;
      n *= res;
      switch(sx.length) {
      case 1: {
         // DINTY tetrad notation 2K
         var tn,tx,ty;
         tn = sx.charCodeAt(0) - 65;
         tn = (tn > 14)?(tn-1):tn;
         ty = (tn%5) * 2 * (res/10);
         tx = Math.floor(tn/5) * 2 * (res/10);
         res /= 5;
         e += tx;
         n += ty;
      }break;
      case 2: {
         // NE,SE,SW,NW notation 5K
         var tx = (sx.substring(1,1) == 'E')?5:0;
         tx *= (res/10);
         var ty = (sx.substring(0,1) == 'N')?5:0;
         ty *= (res/10);
         res /= 2;
         e += tx;
         n += ty;
      }break;
      }
      e += (res/2);
      n += (res/2);
      ngr = new NationalGridReference(sq,e,n,res);
      gr = s2_ngr_to_gr(ngr);      
   }
   return gr; 
}
   
function s2_movetoll(llstring) {
   var bits,lat,lng,ctr;
   if (llstring && llstring.match(/\-*\d+\s*\,\s*\-*\d+/)) {
      bits = llstring.split(/\s*\,\s*/);
      lng = parseInt(bits[0]);
      lat = parseInt(bits[1]);
      ctr = new google.maps.LatLng(lng,lat);
      s2_moveto(ctr);                                                  
   }
}


function s2_getpolybounds(poly) {
   var px = (poly)?poly.getPath().getArray():points;
   var p,c,cx,cy,maxx,maxy,minx,miny,sw,ne; 
   for (p in px) {
      c = px[p];
      maxx = (maxx)?Math.max(maxx,c.lng()):c.lng();
      minx = (minx)?Math.min(minx,c.lng()):c.lng();
      maxy = (maxy)?Math.max(maxy,c.lat()):c.lat();
      miny = (miny)?Math.min(miny,c.lat()):c.lat();
   }
   sw = new google.maps.LatLng(miny,minx);
   ne = new google.maps.LatLng(maxy,maxx);
   c = new google.maps.LatLngBounds(sw,ne);
   return c; 
}
function s2_getpolycenter(poly) {
   var px = (poly)?poly.getPath().getArray():points;
   var p,c,cx,cy,maxx,maxy,minx,miny; 
   for (p in px) {
      c = px[p];
      maxx = (maxx)?Math.max(maxx,c.lng()):c.lng();
      minx = (minx)?Math.min(minx,c.lng()):c.lng();
      maxy = (maxy)?Math.max(maxy,c.lat()):c.lat();
      miny = (miny)?Math.min(miny,c.lat()):c.lat();
   }
   cx = minx + ((maxx-minx)/2);
   cy = miny + ((maxy-miny)/2);
   c = new google.maps.LatLng(cy,cx);
   return c; 
}

function s2_zoomto(znum) {
   try {
      map.setOptions({"zoom":znum});                  
   } catch (e) {}
}
function s2_maptype(type) {
   try {
      map.setOptions({"mapTypeId":google.maps.MapTypeId.SATELLITE});
   } catch (e){}
}  
function s2_getres() {
   var z,r;
   z = map.getZoom();
   r = (z>=18)?10:((z>=16)?100:((z>=12)?1000:((z>=8)?10000:100000)));
   return r;
}   
function s2_clickmap(e) {
   var rad;
   var pt = e.latLng;
   if (pt) {
      switch(tool) {
      case 'recentre': {
         s2_moveto(pt);
         s2_showlocation();
      }break;
      case 'circle': {
         rad = parseInt(document.getElementById('circle_radius').value);
         s2_drawcircle(pt,rad);
      }break;
      case 'square': {
         rad = parseInt(document.getElementById('square_side').value);
         s2_drawgridsquare(pt,rad);
      }break;
      case 'polygon': {
         points.push(pt);
         s2_drawpoly(points);
      }break;
      case 'point': {
         var gr,ngr,a;
         a = s2_getres();
         points = new Array(pt);
         gr = s2_pointtogrstring(pt,a);
         s2_drawpointmarker(pt,gr,null)
         ngr = document.getElementById('ngr');
         if (ngr) ngr.value = gr;
      }break;
      }         
   }
   return true;
}  
function s2_showlocation(ctr) {
   var ll,gr,ngr,res,z;
   if (!ctr) ctr = map.getCenter();
   switch(tool) {
   case 'circle': {
      res = parseInt(document.getElementById('circle_radius').value);
      res *= 2;
   }break;
   case 'square': {
      res = parseInt(document.getElementById('square_side').value);
   }break;
   default: {
      res = s2_getres();
   }break;
   }
   ll = s2_gmp_to_ll(ctr);
   gr = s2_ll_to_gr(ll);
   ngr = s2_gr_to_ngr(gr);
   ngr.accuracy = res;
   ll.accuracy = res;
   
   try { document.getElementById('ngr').value = ngr.asString(); } catch(e) {}
   try { document.getElementById('latlng').value = ll.asString(); } catch(e) {}
}
function s2_pointtogrstring(p,a) {
   var ll,gr,ngr,res,z,grs;
   try {
      ll = s2_gmp_to_ll(p);
      gr = s2_ll_to_gr(ll);
      gr.accuracy = a;
      ngr = s2_gr_to_ngr(gr);
      grs = ngr.asString();
   } catch (e) {}
   return grs;
}
function s2_drawpoly(points) {
   var p,c;
   if (overlays['boundary']) {
      p = overlays['boundary'];
      p.setMap(null);
   }
   //p = new google.maps.Polygon({
   p = new S2GM3Polygon({
      paths: points,
      strokeColor: "#FF0000",
      strokeOpacity: 0.8,
      strokeWeight: 0.5,
      fillColor: "#FF0000",
      fillOpacity: 0.35,
      clickable: true
   });
   google.maps.event.addListener(p, 'click', function(e) {s2_clickmap(e);});
   p.setMap(map);
   overlays['boundary'] = p;
   c = s2_getpolycenter(p);
   s2_showlocation(c);
}
function s2_drawcircle(ctr,rad) {
   var c,p;
   if (overlays['boundary']) {
      c = overlays['boundary'];
      c.setMap(null);
   }
   c = new google.maps.Circle({
      strokeColor: "#FF0000",
      strokeOpacity: 0.8,
      strokeWeight: 0.5,
      fillColor: "#FF0000",
      fillOpacity: 0.35,
      center: ctr,
      radius: rad,
      clickable: true
   });
   c.setMap(map);
   points = s2_getcirclevertices(c);
   google.maps.event.addListener(c, 'click', function(e) {s2_clickmap(e);});
   c.setMap(map);
   overlays['boundary'] = c;
   s2_showlocation(ctr);
}

function s2_drawgridsquare(clicked,res) {
   var p,ll,gr,ngr;
   if (overlays['boundary']) {
      p = overlays['boundary'];
      p.setMap(null);
   }
   ll = s2_gmp_to_ll(clicked);
   gr = s2_ll_to_gr(ll);
   ngr = s2_gr_to_ngr(gr);
   ngr.easting = ngr.easting - (ngr.easting % res) + (0.5 * res);
   ngr.northing = ngr.northing - (ngr.northing % res) + (0.5 * res);
   ngr.accuracy = res;
   points = s2_getgridsquarefromgr(gr,res);
   
   //p = new google.maps.Polygon({
   p = new S2GM3Polygon({
      paths: points,
      strokeColor: "#FF0000",
      strokeOpacity: 0.8,
      strokeWeight: 0.5,
      fillColor: "#FF0000",
      fillOpacity: 0.35,
      clickable: true
   });
   google.maps.event.addListener(p, 'click', function(e) {s2_clickmap(e);});

   p.setMap(map);
   s2_showlocation(clicked);
   overlays['boundary'] = p;
}

var ztime;
function s2_showzoom() {
   google.maps.event.addListener(map, 'zoom_changed', function() {
      // Get the current bounds, which reflect the bounds before the zoom.
      var zrect = new google.maps.Rectangle();
      zrect.setOptions({
         strokeColor: "#0000FF",
         strokeOpacity: 0.8,
         strokeWeight: 0.5,
         fillColor: "#0000FF",
         fillOpacity: 0.35,
         bounds: map.getBounds()
      });
      zrect.setMap(map);
      overlays['zoom'] = zrect;
      ztime = window.setTimeout("s2_hidezoom();",1000);   
   });
}
function s2_hidezoom() {
   var zrect = overlays['zoom'];
   if (zrect) { 
      zrect.setMap(null);
      zrect = null;
      ztime = null;
   }
   delete overlays['zoom'];
}
                                  
function s2_drawpointmarker(point,title,info) {
   var o;
   while(overlays.length > 0) {
      o = overlays.shift();
      o.setMap(null);
   }
   var infowindow = new google.maps.InfoWindow({
      content: info
   });

   var marker = new google.maps.Marker({
      position: point,
      title: title
   });
   
   google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
   });
   marker.setMap(map);
   overlays.push(marker);
}

function s2_showoverlays() {
   if (overlays) {
      for (i in overlays) {
         overlays[i].setMap(map);
      }
   }
}

// Deletes all markers in the array by removing references to them
function s2_deleteoverlays() {
   if (overlays) {
      for (i in overlays) {
         overlays[i].setMap(null);
      }
      overlays.length = 0;
   }
}

function s2_gmp_to_ll(gmp) {
   var ddc = new DegreesDecimalCoordinate(gmp.lat(),gmp.lng());
   return ddc;
}
function s2_ll_to_gr(ll) {
   var gr = wgs84_to_osgb36(ll);
   return gr;
}
function s2_gr_to_ngr(gr) {
   var ngr = find_gridsquare(gr);
   return ngr;
}
function s2_ngr_to_gr(ngr) {
   var gr = conv_ngr_to_ings(ngr);
   return gr; 
}
function s2_gr_to_ll(gr) {
   var ddc = conv_uk_ings_to_ll(gr);
   return ddc;
}
function s2_ll_to_gmp(ll) {
   var gmp = new google.maps.LatLng(ll.lat(),ll.lng());
   return gmp;
}

function s2_getgridsquarefromgr(gr,acc) {
   var x,y,xi,yi,ll;
   x = gr.easting - (gr.easting % acc);
   y = gr.northing - (gr.northing % acc);
   var points = new Array();   
   for (xi=0;xi<=1;xi++) {
      for (yi=0;yi<=1;yi++) {
         gr = new GridReference((x + (xi*acc)),y + (((xi+yi)%2)*acc));
         gr.accuracy = acc;
         ll = s2_gr_to_ll(gr);                  
         points.push(s2_ll_to_gmp(ll));                  
      }
   }
   if (points[0] != points[points.length-1]) points.push(points[0]);
   return points;            
}
function s2_getgridsquarecenter(gr,acc) {
   var x,y,xi,yi,ll;
   x = gr.easting - (gr.easting % acc);
   y = gr.northing - (gr.northing % acc);
   gr = new GridReference((x + (0.5*acc)),(y + (0.5*acc)));
   gr.accuracy = acc;
   ll = s2_gr_to_ll(gr);                  
   return s2_ll_to_gmp(ll);            
}
function gm3_convertcircle(o) {
   path = s2_getcirclevertices(o);
   o = new S2GM3Polygon({paths: path});
   return o;
}
function s2_getcirclevertices(c) {
   var ctr,rad,seg,ll,gr,ngr,points,vertex;
   ctr = c.getCenter();
   rad = c.getRadius();
   seg = Math.floor(360/vertices) * (Math.PI/180);
   points = new Array();
   ll = s2_gmp_to_ll(ctr);
   gr = s2_ll_to_gr(ll);
   cgr = new GridReference(gr.easting,gr.northing);
   for (vertex=0;vertex<=vertices;vertex++) {
      gr.easting = cgr.easting + (rad * Math.sin(seg*vertex));
      gr.northing = cgr.northing + (rad * Math.cos(seg*vertex));
      ll = s2_gr_to_ll(gr);
      points[vertex] = s2_ll_to_gmp(ll);
   }
   if (points[0] != points[points.length-1]) points.push(points[0]);
   return points;      
} 

function s2_wkt_to_gmappoly(wkt) {
   var pairs,p,pt,xy,ll,polygon;
   polygon = null;
   try {
      ll = new google.maps.LatLng(0,0);  
      if (wkt) {
         points = new Array();
         wkt = wkt.replace(/^[^\(]+\(+/,'');
         wkt = wkt.replace(/\)+$/,'');
         pairs = wkt.split(/\,/);
         for(p in pairs) {
            pt = pairs[p];
            xy = pt.split(/\s+/);
            ll = new google.maps.LatLng(xy[1],xy[0]);
            points.push(ll);
         }
         //polygon = new google.maps.Polygon({
         polygon = new S2GM3Polygon({
            paths: points,
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 0.5,
            fillColor: "#FF0000",
            fillOpacity: 0.35,
            clickable: true
         });
      }
   } catch(err) {
      // add something here.   
   }
   return polygon;
}
function s2_poly_getname(polygon) {
   var wkt,p,ddc,gr,ngr,ptext,pname,parea,path,zoom;
   try {
      if (polygon instanceof google.maps.Circle) {
         path = s2_getcirclevertices(polygon);
         //polygon = new google.maps.Polygon({
         polygon = new S2GM3Polygon({
            paths: path,
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 0.5,
            fillColor: "#FF0000",
            fillOpacity: 0.35,
            clickable: true
         });
         polygon.setMap(map);
      }
      p = s2_getpolycenter(polygon);
      ddc = new DegreesDecimalCoordinate(p.lat(),p.lng());
      gr = s2_ll_to_gr(ddc);
      gr.accuracy = 10;
      gr.easting = Math.floor(gr.easting/gr.accuracy)*gr.accuracy;
      gr.northing = Math.floor(gr.northing/gr.accuracy)*gr.accuracy;
      ngr = s2_gr_to_ngr(gr);
      //path = polygon.getPath().getArray();
      //parea = s2gmapv3getarea(path);
	  parea = google.maps.geometry.spherical.computeArea(polygon.getPath());
      if (parea > 900000) ptext = Math.round(parea/100000)/10+' km� @ '+ngr.asString();
      else if (parea > 9000) ptext = Math.round(parea/1000)/10+' ha @ '+ngr.asString();  
      else ptext = Math.round(parea*10)/10+' m� @ '+ngr.asString();
   } catch(err) {
      ptext = 'Google Maps not available in offline mode'; 
   }
   return ptext;
}
function gm3_assignmap() {
   var wkt,p,ddc,gr,ngr,ptext,pname,parea,zoom,polygon,o;
   var mappoly = document.getElementById(mapbutton);        
   var mapname = document.getElementById(mapbutton+'_Name');
   if (!mapname) mapname = document.getElementById('Link_'+mapbutton); 
   zoom = map.getZoom();
   if (overlays['boundary']) {
 	   o = overlays['boundary'];
	   if (o instanceof google.maps.Circle) o = gm3_convertcircle(o);
	   ptext = gm3_getnamefromoverlay(o);
	   wkt = gm3_getwktfromoverlay(o);
   } else if (points && points.length > 1) {
      wkt = 'POLYGON((';
      for (pcount=0;pcount<points.length;pcount++) {
         p = points[pcount];
         wkt += p.lng() + ' ' + p.lat() + ',';
      }
      p = points[0];
      wkt += p.lng() + ' ' + p.lat() + '))';
      //polygon = new google.maps.Polygon({
      polygon = new S2GM3Polygon({
         paths: points,
         strokeColor: "#FF0000",
         strokeOpacity: 0.8,
         strokeWeight: 0.5,
         fillColor: "#FF0000",
         fillOpacity: 0.35,
         clickable: true
      });
      ptext = s2_poly_getname(polygon);
   } else if (points && points.length > 0) {
      p = points[0];
      ddc = s2_gmp_to_ll(p);
      gr = s2_ll_to_gr(ddc);
      gr.accuracy = (zoom >= 12)?100:(zoom>=8)?1000:(zoom>=5)?10000:100000;
      gr.easting = Math.floor(gr.easting/gr.accuracy)*gr.accuracy;
      gr.northing = Math.floor(gr.northing/gr.accuracy)*gr.accuracy;
      ngr = find_gridsquare(gr);
      wkt = 'POINT('+ p.lng() + ' ' + p.lat() + ')';
      ptext = ngr.asString(); 
   }
   mappoly.value = wkt;
   // execute onchange if the function is defined. 
   // no catch action because it just means the onchange function is not defined
   try {mappoly.onchange();} catch (e) {}
   if (mapname) {
      if (mapname.nodeName == 'INPUT') mapname.value = ptext;
      else mapname.replaceChild(document.createTextNode(ptext),mapname.firstChild);
   }
   //points = new Array();
   hidedialog();
   return true;
}         ;var polyPoints = [];
var Points = [];
var PolyShape;
var map;
var startMarker;
var earthRadiusMeters=6367460.0;
var metersPerDegree=2.0*Math.PI*earthRadiusMeters/360.0;
var degreesPerRadian=180.0/Math.PI;
var radiansPerDegree=Math.PI/180.0;
var metersPerKm=1000.0;
var meters2PerHectare=10000.0;
var feetPerMeter=3.2808399;
var feetPerMile=5280.0;
var acresPerMile2=640;

function s2gmapv3getarea(points) {
   var areaMeters2;
   if(points.length>2) {
      areaMeters2=PlanarPolygonAreaMeters2(points);
      if(areaMeters2>1000000.0) areaMeters2=SphericalPolygonAreaMeters2(points);
   } else areaMeters2 = 0;
   return areaMeters2;    
}
function PlanarPolygonAreaMeters2(points) {
    var a=0.0;
    for(var i=0;i<points.length;++i)
        {var j=(i+1)%points.length;
        var xi=points[i].lng()*metersPerDegree*Math.cos(points[i].lat()*radiansPerDegree);
        var yi=points[i].lat()*metersPerDegree;
        var xj=points[j].lng()*metersPerDegree*Math.cos(points[j].lat()*radiansPerDegree);
        var yj=points[j].lat()*metersPerDegree;
        a+=xi*yj-xj*yi;}
    return Math.abs(a/2.0);
}
function SphericalPolygonAreaMeters2(points) {
    var totalAngle=0.0;
    //alert(points[0]);
    for(i=0;i<points.length;++i)
        {var j=(i+1)%points.length;
        var k=(i+2)%points.length;
        totalAngle+=Angle(points[i],points[j],points[k]);}
    var planarTotalAngle=(points.length-2)*180.0;
    var sphericalExcess=totalAngle-planarTotalAngle;
    if(sphericalExcess>420.0)
        {totalAngle=points.length*360.0-totalAngle;
        sphericalExcess=totalAngle-planarTotalAngle;}
    else if(sphericalExcess>300.0&&sphericalExcess<420.0)
        {sphericalExcess=Math.abs(360.0-sphericalExcess);}
    return sphericalExcess*radiansPerDegree*earthRadiusMeters*earthRadiusMeters;
}
function Angle(p1,p2,p3) {
    var bearing21=Bearing(p2,p1);
    var bearing23=Bearing(p2,p3);
    var angle=bearing21-bearing23;
    if(angle<0.0) angle+=360.0;
    return angle;
}
function Bearing(from,to) {
    var lat1=from.lat()*radiansPerDegree;
    var lon1=from.lng()*radiansPerDegree;
    var lat2=to.lat()*radiansPerDegree;
    var lon2=to.lng()*radiansPerDegree;
    var angle=-Math.atan2(Math.sin(lon1-lon2)*Math.cos(lat2),Math.cos(lat1)*Math.sin(lat2)-Math.sin(lat1)*Math.cos(lat2)*Math.cos(lon1-lon2));
    if(angle<0.0) angle+=Math.PI*2.0;
    angle=angle*degreesPerRadian;
    return angle;
}

function Areas(areaMeters2) {
    var areaHectares=areaMeters2/meters2PerHectare;
    var areaKm2=areaMeters2/metersPerKm/metersPerKm;
    var areaFeet2=areaMeters2*feetPerMeter*feetPerMeter;
    var areaMiles2=areaFeet2/feetPerMile/feetPerMile;
    var areaAcres=areaMiles2*acresPerMile2;
    //return areaMeters2.toPrecision(4)+' m&sup2; / '+areaHectares.toPrecision(4)+' hectares / '+areaKm2.toPrecision(4)+' km&sup2; / '+areaFeet2.toPrecision(4)+' ft&sup2; / '+areaAcres.toPrecision(4)+' acres / '+areaMiles2.toPrecision(4)+' mile&sup2;';}
    var area = areaMeters2+' m&sup2; / '+areaHectares.toFixed(4)+' hectares / '+areaKm2.toFixed(4)+' km&sup2;<br />'
        +areaFeet2.toFixed(2)+' ft&sup2; / '+areaAcres.toFixed(4)+' acres / '+areaMiles2.toFixed(4)+' mile&sup2;';
    return area;
}
;var geocoder = new google.maps.Geocoder();
var infowindow = new google.maps.InfoWindow();
var marker;
function gm3_isCoordinate(val) {
   return (val.match(/^[\d,\.]+\s*\,\s*[\d,\.]+$/));
}
function gm3_isEasting(val) {
   return (val.length >= 6 && val.match(/^[\d,\.]+$/));
}
function gm3_codeAddress() {
   var lat,lng,latlng,coords,e,n,gr,ll;
   var address = document.getElementById('address').value;
   if (gm3_isCoordinate(address)) {
      coords = address.split(/\s*\,\s*/);
      e = parseFloat(coords.shift());
      n = parseFloat(coords.shift());
      ll = s2_gr_to_ll(new GridReference(e,n));
      latlng = ll.latitude + ',' + ll.longitude; 
      gm3_updateNGR(latlng);
   } else if (gm3_isEasting(address)) {
      document.getElementById('address').value = address + ', ';
      alert('Now paste Northing');
   } else {
      geocoder.geocode( { 'address': address}, function(results, status) {
         if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            if (marker) marker.setMap(null);
            marker = new google.maps.Marker({
               map: map,
               position: results[0].geometry.location
            });
            lat = marker.position.lat();
            lng = marker.position.lng();
            latlng = (Math.round(lat*100000)/100000) + ',' + (Math.round(lng*100000)/100000);
            gridref = gm3_updateNGR(latlng);
            gm3_updateLL(latlng);
            infowindow.setContent('GR: '+gridref+'<br/>LL: '+latlng);
            infowindow.open(map, marker);
         } else {
            alert('Geocode was not successful: ' + status);
         }
      });
   }
}

function gm3_codeLatLng() {
   var addnode;
   var input = document.getElementById('latlng').value;
   var latlngStr = input.split(',', 2);
   var lat = parseFloat(latlngStr[0]);
   var lng = parseFloat(latlngStr[1]);
   var latlng = new google.maps.LatLng(lat, lng);
   geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
         if (results[1]) {
            //map.setZoom(11);
            map.setCenter(latlng);
            if (marker) marker.setMap(null);
            marker = new google.maps.Marker({
               position: latlng,
               map: map
            });
            infowindow.setContent(results[1].formatted_address);
            infowindow.open(map, marker);
            addnode = document.getElementById('address');
            addnode.value = results[1].formatted_address;
         } else {
           alert('No results found');
         }
      } else {
         alert('Geocoder failed due to: ' + status);
      }
   });
}
function gm3_codeNGR(gridref) {
   var gr,ll,input;
   gr = s2_gr_from_ngrstring(gridref);
   ll = s2_gr_to_ll(gr);
   input = document.getElementById('latlng');
   input.value = ll.lat() + ',' + ll.lng();
   //codeLatLng();
   return true;  
}
function gm3_updateLL(llstr) {
   input = document.getElementById('latlng');
   input.value = llstr;
}
function gm3_updateNGR(latlng) {
   var gm,ll,gr,ngr,gridref,grnode,latlngstr,llnode;
   var latlngStr = latlng.split(',', 2);
   var lat = parseFloat(latlngStr[0]);
   var lng = parseFloat(latlngStr[1]);
   var gm = new google.maps.LatLng(lat, lng);
   ll = s2_gmp_to_ll(gm);
   gr = s2_ll_to_gr(ll);
   ngr = s2_gr_to_ngr(gr);
   gridref = ngr.asString();
   grnode = document.getElementById('ngr');
   grnode.value = gridref;
   s2_clickmap({latLng:gm});
   return gridref;
}
;function S2AB() {this.ab;this.bc;this.cd=function(ef) {var fg=ef.split('');var gh='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';var hi=gh.split('');var ij=array_flip(hi);var jk=0;var op;for(op=0;op<fg.length;op++) {jk +=parseInt(ij[fg[op]]);}var pq=new Date();var qr=pq.getUTCFullYear();var rs=pq.getUTCMonth()+1;rs=(rs<10)?new String('0'+rs):rs;var st=pq.getUTCDate();st=(st<10)?new String('0'+st):st;var kl=Math.floor(parseInt(new String(qr)+new String(rs)+new String(st),10)/parseInt(new String(st)+new String(rs),10))%hi.length;hi=hi.slice(kl,hi.length).concat(hi.slice(0,kl));var lm=0;var mn=0;var no;for (op=0;op<hi.length;op++) {mn=(jk*ij[hi[op]])%hi.length;no=hi[mn];hi[mn]=hi[lm];hi[lm]=no;lm=(mn+1)%hi.length;}ij=array_flip(hi);kl=kl%fg.length;fg=fg.slice(kl,fg.length).concat(fg.slice(0,kl));tu=0;ab='';bc='';wx=null;xy=null;while(tu<8) {uv=new S2BC(ij[fg[(tu*2)]],ij[fg[((tu*2)+1)]]);if (wx) uv.hj(wx);vw=new S2BC(ij[fg[(fg.length-((tu*2)+1))]],ij[fg[(fg.length-((tu*2)+2))]]);if (xy) vw.hj(xy);ab +=hi[(Math.floor(uv.ln())%(hi.length))];bc +=hi[(Math.floor(vw.ln())%(hi.length))];var yz=((ij[fg[(tu*2)]]%3)+1);var ac=(ij[fg[(tu*2+1)]]%2);if (wx) {switch(ac) {case 0: wx=uv.hj(wx);break;case 1: wx=uv.ik(wx);break;}} else {switch(yz) {case 1: wx=uv;break;case 2: wx=uv.fh();break;case 3: wx=uv.gi();break;}}yz=(((ij[fg[(fg.length-((tu*2)+1))]])%3)+1);ac=((ij[fg[(fg.length-((tu*2)+2))]])%2);if (xy) {switch(ac) {case 0: xy=vw.hj(xy);break;case 1: xy=vw.ik(xy);break;}} else {switch(yz) {case 1: xy=vw;break;case 2: xy=vw.fh();break;case 3: xy=vw.gi();break;}}tu++;}this.ab=ab;this.bc=bc;};this.bd=function() {ce=new Object();ce.ab=this.ab;ce.bc=this.bc;return JSON.stringify(ce);};}function S2BC(df,eg) {this.df=df;this.eg=eg;this.get=function(pr) {switch(pr) {case 'r': return this.df;break;case 'i': return this.eg;break;default: return new Array(this.df,this.eg);break;}};this.fh=function() {var df=this.df*1;this.df=((Math.pow(df,2)) - (Math.pow(this.eg,2)));this.eg=(2 * df * this.eg);};this.gi=function() {var df=this.df*1;var eg=this.eg*1;this.df=(Math.pow(df,3) - (df * Math.pow(eg,2)) - (2 * df * Math.pow(eg,2)));this.eg=((Math.pow(df,2)*eg) - Math.pow(eg,3) + (2 * Math.pow(df,2) * eg));};this.hj=function(uv) {this.df=(this.df*1 + uv.df*1);this.eg=(this.eg*1 + uv.eg*1);};this.ik=function(uv) {this.df=(this.df*1 - uv.df*1);this.eg=(this.eg*1 - uv.eg*1);};this.jl=function(uv) {this.df=(this.df * uv.df)-(this.eg * uv.eg);this.eg=(this.df * uv.eg)+(this.eg * uv.df);};this.km=function(uv) {return Math.sqrt(Math.pow((this.df*1 - uv.df*1),2) + Math.pow((this.eg*1 - uv.eg*1),2));};this.ln=function() {return Math.sqrt(Math.pow(this.df*1,2)+Math.pow(this.eg*1,2));};}function array_flip(mo) {var np=new Array();var op;for (op in mo) {np[mo[op]]=op;}return np;};<!--
var wtimeoutid;
/*
<button class='lmtype' id='s2nbnbackbutton' onclick='nbngetprevsearch();'>&larr;</button>
<input type='text' class='text' id='searchfor' onkeypress='nbntaxasearch(this);' style='width:300px;'/>
<div id='speciescontainer'></div>
<input type='hidden' id='nbnsearchhistory'/>
*/
var taxatarget;
function s2_shownbntaxadialog(target,species) {
   var button,input,div,table,tbody,tr,th,td,span,h2;
   taxatarget = target;
   cleardialog('dialog-liner');
   var dialog = document.getElementById('dialog-liner');
   h2 = document.createElement('H2');
   h2.appendChild(document.createTextNode('Species browser'));
   dialog.appendChild(h2);
   span = document.createElement('SPAN');
   span.style.fontWeight = 'bold';
   span.appendChild(document.createTextNode('Search For: '));
   dialog.appendChild(span);
   input = document.createElement('INPUT');
   input.type = 'text';
   input.className = 'text';
   input.style.border = '1px solid #999';
   input.style.padding = '5px';
   input.id = 'searchfor';
   if (species) input.value = species;
   //input.onkeypress = function() {nbntaxasearch(this);};
   input.onkeyup = function() {nbntaxasearch(this);};
   input.style.width = '250px';
   dialog.appendChild(input);
   button = document.createElement('BUTTON');
   button.innerHTML = '&larr;';
   button.className = 'lmtype';
   button.id = 's2nbnbackbutton';
   button.onclick = function() {nbngetprevsearch();};
   button.name = 'Previous search';
   s2_addtooltip(button);
   dialog.appendChild(button);
   input = document.createElement('INPUT');
   input.type = 'hidden';
   input.id = 'nbnsearchhistory';
   if (species) input.value = species
   dialog.appendChild(input)
   div = document.createElement('DIV');
   div.id = 'speciescontainer';
   dialog.appendChild(div)
   /*   
   var close = document.createElement('BUTTON');
   close.appendChild(document.createTextNode('close'));
   close.onclick = hidedialog;
   dialog.appendChild(close);
   */
   showdialog();
   if (species) nbnsendsearch(species);
}
function nbnusetaxa(elid) {
//alert(elid);
   var args,targs,obj,node,input,a,text,button;
   if (taxatarget) {
      node = taxatarget.parentNode;
      
      args = s2_disectid(elid);
      targs = s2_disectid(taxatarget.id);
      /*
      obj = new Object();
      obj.Display_Name = targs.Property;
      obj.To_Domain = 'Inline';
      obj.Data_Type = 'NBNTaxa';
      obj.Name = args.NBNSpecies;
      obj.Value = args.NBNCode;
      */
      taxatarget = node.lastChild;
      while (node.childNodes.length > 1) node.removeChild(node.firstChild);
      a = document.createElement('A');
      a.appendChild(document.createTextNode(args.NBNSpecies));
      a.href = '#';
      a.id = 'Form-'+targs.Form+'_IType-'+targs.IType+'_Action-View_Property-'+targs.Property; 
      a.onclick = function() {s2_shownbntaxadialog(this,this.firstChild.nodeValue)};       
      node.insertBefore(a,taxatarget);
      button = document.createElement('BUTTON');
      button.className = 's2delbutton';
      button.innerHTML = '&times;';
      button.name = 'Remove '+text;
      button.id = 'Form-'+targs.Form+'_IType-'+targs.IType+'_Property-'+targs.Property+'_Action-Unset_Current-'+args.NBNCode;
      button.onclick = function() {s2_ifaceclear(this);};
      node.appendChild(button);
         
      input = document.createElement('INPUT');
      input.type = 'hidden';
      input.id = 'Form-'+targs.Form+'_IType-'+targs.IType+'_Property-'+targs.Property;
      input.value = args.NBNCode;//JSON.stringify(obj);
      node.insertBefore(input,taxatarget); 

      cleardialog('dialog-liner');
      hidedialog();
   }
//alert(JSON.stringify(args));
}
 
function nbnload() {
   placescreen();
   completed();     
   var el = document.getElementById('searchfor');
   nbntaxasearch(el);
   nbnaddtosearchhistory(el.value); 
}
function nbntaxasearch(searchfor) {
   window.clearTimeout(wtimeoutid);
   if (searchfor.value.length >= 3) {
      wtimeoutid = window.setTimeout(nbnsendsearch,500,searchfor.value);
   } 
}

function nbnsendsearch(searchfor) { 
   window.clearTimeout(wtimeoutid);
   var req,par;
   req = new Object();
   req.method = 'GET';
   //req.target = '../taxa/nbnapi/search/taxa?q='+searchfor;
   req.target = nbnapiurl+'/search/taxa?q='+searchfor;
   //par = new Object();
   //par.q = searchfor;
   //req.request = par;
   req.responder = nbnshowtaxa;
   snc_send(req);
}
function nbnshowtaxa(response) {
   var div = document.getElementById('speciescontainer');
   while (div.childNodes.length > 0) div.removeChild(div.lastChild);
   var taxonlist = JSON.parse(response);
   var ul;
   if (taxonlist.header.numFound > 0) {
      ul = document.createElement('UL');
      for(var i in taxonlist.results) {
         li = nbncreatetaxalistitem(taxonlist.results[i]);
         ul.appendChild(li);
      }
      div.appendChild(ul);
   } 
   completed();
}
function nbncreatetaxalistitem(taxa) {
   var ul,li,li2,a,button,span,text,b;
   li = document.createElement('LI');
   li.appendChild(document.createTextNode(taxa.rank + ': '));
   a = document.createElement('A');
   a.appendChild(document.createTextNode(taxa.name));
   a.href = '#';
   a.onclick = function() {nbnusetaxa(this.id);};
   a.id = 'NBNSpecies-'+taxa.name+'_NBNCode-'+taxa.ptaxonVersionKey;
   a.name = 'Use this species';
   s2_addtooltip(a);
   li.appendChild(a);
   
   button = document.createElement('BUTTON');
   button.className = 'lmtype';
   button.innerHTML = '&rarr;';
   button.id = 'NBNSearchBy-'+taxa.name;
   button.name = 'Search by this';
   button.onclick = function() {nbnsearchby(this.id);};
   s2_addtooltip(button);
   li.appendChild(button);
   return li;
}
function nbnsearchby(elid) {
   var args = s2_disectid(elid);
   var node = document.getElementById('searchfor');
   node.value = args.NBNSearchBy;
   nbnsendsearch(args.NBNSearchBy);
   nbnaddtosearchhistory(args.NBNSearchBy)
   return true;
}
function nbnaddtosearchhistory(term) {
   if (term && term != '') {
      var histnode = document.getElementById('nbnsearchhistory');
      if (histnode.value && histnode.value !='') hist = JSON.parse(histnode.value);
      else hist = new Array();
      hist.push(term);
      histnode.value = JSON.stringify(hist);
   }   
}
function nbngetprevsearch() {
   var sf = document.getElementById('searchfor'); 
   var current = sf.value;
   var histnode = document.getElementById('nbnsearchhistory');
   var retval;
   if (histnode.value && histnode.value !='') {
      hist = JSON.parse(histnode.value);
      for (var i=1;i<=hist.length;i++) {
         if (hist[i] == current) retval = hist[i-1];  
      }
   }
   if (retval) {
      sf.value = retval;
      nbnsendsearch(retval);
      hist.pop();
      histnode.value = JSON.stringify(hist);
   }    
}
function nbngetnextsearch() {
   var sf = document.getElementById('searchfor'); 
   var current = sf.value;
   var histnode = document.getElementById('nbnsearchhistory');
   var retval;
   if (histnode.value && histnode.value !='') {
      hist = JSON.parse(histnode.value);
      for (var i=0;i<hist.length;i++) {
         if (hist[i] == current) retval = hist[i+1];   
      }
   }
   if (retval) {
      sf.value = retval;
      nbnsendsearch(retval);
   }   
}

function s2_browsefrom() {
   var searchfor = document.getElementById('s2speciessearch').value;
   if (searchfor.length > 3) {
      s2_browsespecies('getbylatinname',searchfor);
   } else {
      s2_browsespecies();
   }
}
function s2_searchspecies(value) {
   window.clearTimeout(wtimeoutid);
   if (value.length >= 3) {
      wtimeoutid = window.setTimeout(s2_browsespecies,500,'getbyname',value);
   }
}
function s2_browsespecies(action,value) {
//alert(action + ' ' + value);
   var req,par;
   req = new Object();
   req.target = './dictionary.php';
   par = new Object();
   par.a = action;
   par.v = value;
   req.request = par;
   req.responder = s2_updatespecieslist;
   snc_send(req);      
}
function s2_updatespecieslist(json) {
   var jobj = JSON.parse(json);
   var list = s2_parsespecies(jobj)
   var node = document.getElementById('s2specieslist');
   while(node.childNodes.length > 0) {
      node.removeChild(node.lastChild);
   }
   node.appendChild(list);
   completed();
}
function s2_parsespecies(item,inner) {
   var citem,ul,li,pli,cul,pul,button,node,text,names,span,i;
   if (typeof item == 'object' && item.TAXON_LIST_ITEM_KEY) {
      names = new Array();
      li = document.createElement('LI');
      li.className = 's2taxondictionary';
      if (item.COMMON_NAME) names.push(document.createTextNode(item.COMMON_NAME));
      if (item.ACTUAL_NAME) {
         span = document.createElement('SPAN');
         span.className = 'latin';
         span.appendChild(document.createTextNode(' ('+item.ACTUAL_NAME+')'));
         names.push(span);
      }
      for (i in names) li.appendChild(names[i]);
      button = document.createElement('BUTTON');
      button.className = 'lmtype';
      button.innerHTML = '&rarr;';
      button.id = item.TAXON_LIST_ITEM_KEY;
      button.onclick = function() {s2_browsespecies('getself',this.id);};
      li.appendChild(button);
      if (item.CHILDREN) {
         cul = s2_parsespecies(item.CHILDREN,true);
         li.appendChild(cul);
      } 
      if (!inner) {
         ul = document.createElement('UL');
         ul.className = 's2taxondictionary';
         ul.appendChild(li);
         if (item.PARENT && item.PARENT != 'NULL') {
            pul = document.createElement('UL');
            pul.className = 's2taxondictionary';
            pli = s2_parsespecies(item.PARENT,true);
            pli.appendChild(ul);                  
            pul.appendChild(pli);
            node = pul;
         } else node = ul;
      } else node = li;
   } else if (typeof item == 'object') {
      ul = document.createElement('UL');
      ul.className = 's2taxondictionary';
      var i;
      for (i in item) {
         citem = item[i];
         li = s2_parsespecies(citem,true);
         ul.appendChild(li); 
      }
      node = ul;           
   } else {
      alert(JSON.stringify(item));
   }
   return node;
}
var nbnresults = [];



/* 
   s2_nbntaxa_send(url,responder,label)
   url = nbnapiurl+'/taxa/'+tvk
*/
//-->
;var baseurl   = 'http://dan.lunarfish.co.uk/s2/';
var nbnapiurl = baseurl+'taxa/nbnapi.php';
var taxacache = [];
var nbn_cells = [];
var nbn_tcols = [];
var nbn_ctab;
var nbn_show;
 
function s2_nbnify() {
   var tabs,t,tab; 
   tabs = document.getElementsByTagName('TABLE');
   for(t=0;t<tabs.length;t++) {
      tab = tabs[t];
      if (!tab.id) tab.id = 'tab_'+t; 
      s2_nbnifytab(tab);
   }
}
function s2_nbntaxa_gettvkcols(tab) {
   var tr,th,tvkcols;
   tvkcols = [];
   tr = tab.tHead.rows[0];
   for(cx=0;cx<tr.cells.length;cx++) {
      th = tr.cells[cx];
      if (th.className && th.className.match(/TVK/)) tvkcols.push(cx); 
   }
   return tvkcols; 
}
function s2_nbnifytab(tab) {
   var o,tvkcols,addcols,tbody,thead,tr,th,td,nde,tvk,tx,rx,cx,row,col,tid,cid,addCN,addTG;
   tbody = tab.tBodies[0];
   o = (tab.tHead)?'h':'v'; // list tables have headings across the top 
   switch(o) {
   case 'h': {
      tvkcols = s2_nbntaxa_gettvkcols(tab);
      for(rx=0;rx<tbody.rows.length;rx++) {
         tr = tbody.rows[rx];
         for(tx=0;tx<tvkcols.length;tx++) {
            cx = tvkcols[tx];
            td = tr.cells[cx];
            if (!td.id) td.id = tab.id+'r'+rx+'c'+cx;
            s2_nbntaxa_loadcell(td);
         }
      }
   }break;
   case 'v': {
      for(rx=0;rx<tbody.rows.length;rx++) {
         tr = tbody.rows[rx];
         for(cx=0;cx<tr.cells.length;cx++) {
            td = tr.cells[cx];
            if (td.nodeName == 'TD' && td.className.match(/TVK/)) {
               if (!td.id) td.id = tab.id+'r'+rx+'c'+cx;
               s2_nbntaxa_loadcell(td);
            }
         }
      }     
   }break;
   }
}
function s2_nbntaxa_loadcell(cel) {
   var tvk;
   tvk = s2_getndeval(cel);
   if (!nbn_cells[cel.id]) nbn_cells[cel.id] = tvk;
   s2_nbntaxa_gettvkjson(tvk);      
}
function s2_nbntaxa_send(url,responder,label) {
   var ac,resp;
   ac = new AjaxClass();
   ac.url = url;
   ac.responder = responder;
   ac.Method = 'GET';
   ac.Async = true;
   ac.label = label;
   ac.init();
   resp = ac.send();            
}
function s2_nbntaxa_gettvkjson(tvk) {
   var url;
   if (tvk.match(/[A-Z]{6}\d{10}/)) {
      if (!taxacache[tvk] || taxacache[tvk] == 'null') { 
         url = nbnapiurl+'/taxa/'+tvk;
         s2_nbntaxa_send(url,s2_nbntaxa_loadtvkjson);
      } else {
         s2_nbntaxa_loadtvkjson(taxacache[tvk]);
      }            
   }
}
function s2_nbntaxa_loadtvkjson(json) {
   var taxa,tvk,ctvk,nx,cx,cid,cel;
   taxa = JSON.parse(json);
   tvk = taxa.taxonVersionKey;
   if (!taxacache[tvk] || taxacache[tvk] == 'null') taxacache[tvk] = json;
   // if the commmon name is not populated get the synonyms straight away
   if (!taxa.commonName) s2_nbntaxa_getsynonyms(tvk);
   if (!taxa.designations) s2_nbntaxa_getdesignations(tvk);
   
   for(cid in nbn_cells) {
      ctvk = nbn_cells[cid];
      if (tvk == ctvk) {
         cel = document.getElementById(cid);
         s2_nbntaxa_loadtaxa(cel,taxa);
      }   
   }
}
function s2_nbntaxa_settab(el) {
   var tab,tabid;
   // use elemment to get 
   tab = el;
   while(tab && tab.nodeName != 'TABLE') tab = tab.parentNode;
   tabid = tab.id;
   nbn_ctab = tabid;
}
function s2_nbntaxa_gettab() {
   return nbn_ctab;
}
function s2_nbntaxa_loadtaxa(cel,taxa) {
   var lnk,txt,act,tab,addcols;
   lnk = document.createElement('A');
   lnk.style.paddingLeft = '5px';
   lnk.appendChild(document.createTextNode(taxa.name)); 
   if (!cel.className.match(/NoRevise/) && s2_nbntaxa_toberevised(taxa)) {
      lnk.appendChild(document.createTextNode(' [updating to:'+taxa.ptaxonVersionKey + ']'));
      s2_nbntaxa_revisetaxa(cel,taxa.ptaxonVersionKey);
      s2_nbntaxa_postrevision(taxa.taxonVersionKey,taxa.ptaxonVersionKey);
   } else {
      delete nbn_cells[cel.id];
   }  
   lnk.alt = taxa.taxonVersionKey;
   lnk.href = '#';
   lnk.onclick = function() {s2_nbntaxa_settab(this);s2_nbntaxa_showtaxa(this);};
   switch(cel.firstChild.nodeName) {
   case 'INPUT': {
      cel.firstChild.type = 'hidden'; 
      cel.firstChild.value = taxa.taxonVersionKey;
      cel.insertBefore(lnk,cel.firstChild);
   }break;
   default: cel.replaceChild(lnk,cel.firstChild); break;
   }
}
function s2_nbntaxa_toberevised(taxa) {
   return (taxa.taxonVersionKey != taxa.ptaxonVersionKey); 
}
function s2_nbntaxa_revisetaxa(cel,tvk) {
   nbn_cells[cel.id] = tvk;
   s2_nbntaxa_gettvkjson(tvk);
}
function s2_nbntaxa_resusenode(id) {
   var node;
   node = document.getElementById(id);
   if (node) {
      while(node.childNodes.length > 0) node.removeChild(node.lastChild);
   } else {
      node = document.createElement('DIV');
      node.id = id;
   }                                                                      
   return node;
}
function s2_nbntaxa_gettabparent(el) {
   while (el && el.nodeName != 'TABLE') el = el.parentNode;
   return el; 
}
function s2_nbntaxa_showtaxa(lnk) {
   var id,taxa,ti,node,text,name,tvk,btn,p,prop,val,suppress;
   var tab,tabid,tab,tr,th,td,btn,img,o;
   tvk = lnk.alt;
   tab = s2_nbntaxa_gettabparent(lnk);
   o = (tab&&tab.tHead)?'h':'v'; // list tables have headings across the top 
     
   taxa = JSON.parse(taxacache[tvk]);
   nbn_show = tvk;
   if (!taxa.synonyms) s2_nbntaxa_getsynonyms(tvk);
   if (!taxa.designations) s2_nbntaxa_getdesignations(tvk);
   cleardialog('dialog-liner');
   dialog = document.getElementById('dialog-liner');
   id = 's2taxa'; 
   node = s2_nbntaxa_resusenode(id);
   tabid = s2_nbntaxa_gettab();
   tab = document.createElement('TABLE');
   suppress = [
      'commonNameTaxonVersionKey',
      'gatewayRecordCount',
      'href',
      'languageKey',
      'nameStatus',
      'ptaxonVersionKey',
      'taxonVersionKey',
      'taxonOutputGroupKey',
      'organismKey',
      'versionForm'
   ];
   for(p in taxa) {
      val = taxa[p];
      if (suppress.indexOf(p)<0
         && val != '' 
         && val != []
         && val != {}) {
         prop = s2_nbntaxa_colnamefromjsonprop(p);
         tr = document.createElement('TR');
         th = document.createElement('TH');
         th.appendChild(document.createTextNode(prop));
         tr.appendChild(th);
         td = document.createElement('TD');
         val = s2_objecttonode(val); 
         td.appendChild(val);
         if (o=='h'&&val.nodeName == '#text') {
            btn = document.createElement('BUTTON');
            btn.id = 't-'+tabid+'_p-'+p; 
            btn.className = 'inline';
            img = new Image();
            if (nbn_tcols[tabid] && nbn_tcols[tabid][p]) {
               img.src = baseurl+'images/colicon-rem.png';
               img.alt = 'Remove column';
               btn.onclick = function() {s2_nbntaxa_remascolumn(this.id);};
            } else {
               img.src = baseurl+'images/colicon-add.png';
               img.alt = 'Add column';
               btn.onclick = function() {s2_nbntaxa_addascolumn(this.id);};
            } 
            btn.appendChild(img);
            td.appendChild(btn);
         }
         tr.appendChild(td); 
         tab.appendChild(tr);
      }
   }
   node.appendChild(tab);
   dialog.appendChild(node);
   showdialog();    
}
function s2_objecttonode(obj) {
   var node,ul,li,x,span,vals,text;
   switch (typeof obj) {
   case 'number': node = document.createTextNode(obj); break;
   case 'string': node = document.createTextNode(obj); break;
   case 'array': {
      node = document.createElement('UL');
      node.style.display = 'inline-block';
      for(x in obj) {
         li = document.createElement('LI');
         li.appendChild(s2_objecttonode(obj[x]));
         node.appendChild(li);
      }
   }break;
   case 'object': {
      node = document.createElement('UL');
      node.style.display = 'inline-block';
      for(x in obj) {
         li = document.createElement('LI');
         if (!x.match(/^\d+$/)) {
            span = document.createElement('SPAN');
            span.style.fontWeight = 'bold';
            span.appendChild(document.createTextNode(x + ': '));
            li.appendChild(span);
         }
         li.appendChild(s2_objecttonode(obj[x]));
         node.appendChild(li);
      }
   }break;
   default: node = document.createTextNode(typeof obj); break;
   }
   return node;
}
function s2_nbntaxa_postrevision(oldtvk,newtvk) {
   var args = new Object();
   args.IType     = 'RCD_Taxon_Revision';
   args.Action    = 'Add';
   args.Old_Taxa  = oldtvk;
   args.New_Taxa  = newtvk;
   args.Reason    = 'Taxonomic Revision';
   args.Date      = s2_getdatetoday();
   args.NoDraw    = 1;
   args.Refresh   = 1;
   s2_query(args);                                                                            
}
function s2_nbntaxa_getsynonyms(tvk) {
   var url;
   url = nbnapiurl+'/taxa/'+tvk+'/synonyms';
   s2_nbntaxa_send(url,s2_nbntaxa_loadsynjson);            
}
function s2_nbntaxa_loadsynjson(json) {
   var synlist,syn,taxa,ptvk,err;
   synlist = JSON.parse(json);
   taxa = null
   for(sid in synlist) {
      syn = synlist[sid];
      ptvk = syn.ptaxonVersionKey;
      try {
         if (!taxa && taxacache[ptvk]) taxa = JSON.parse(taxacache[ptvk]);
         // even if there are no synonyms create an empty array so that request 
         // is not repeatedly re-sent
         if (taxa) {
            if (!taxa.synonyms) taxa.synonyms = [];            
            if (ptvk != syn.taxonVersionKey) {
               switch(syn.languageKey) {
               case 'en': {// common name
                  if (!taxa.commonName && syn.nameStatus == 'Recommended') taxa.commonName = syn.name;
                  else if (!taxa.alternateNames) taxa.alternateNames = []; 
                  if (syn.name != taxa.commonName 
                     && (taxa.alternateNames.indexOf(syn.name) < 0)) taxa.alternateNames.push(syn.name); 
               }break;
               case 'la': {// latin name
                  if ((taxa.name != syn.name) 
                     && (syn.nameStatus == 'Synonym') 
                     && (taxa.synonyms.indexOf(syn.name) < 0)) taxa.synonyms.push(syn.name);
               }break;
               }
            }
         }
      } catch(err) {
         //alert(taxacache[ptvk]);
      }
   }
   taxacache[ptvk] = JSON.stringify(taxa);
   s2_nbntaxa_edittaxa(ptvk);
}
function s2_nbntaxa_edittaxa(tvk) {
   if (s2_livedialog()) s2_nbntaxa_showtaxa(tvk);
}
function s2_nbntaxa_getdesignations(tvk) {
   var url,lab;
   url = nbnapiurl+'/taxa/'+tvk+'/designations';
   s2_nbntaxa_send(url,s2_nbntaxa_loaddesjson,tvk);            
}
function s2_nbntaxa_loaddesjson(json) {
   var deslist,syn,taxa,ptvk,d,des,obj;
   ptvk = json.replace(/\${2}([^\$]+)\${2}.*/,"$1");
   json = json.replace(/\${2}[^\$]+\${2}(.*)/,"$1");
   deslist = JSON.parse(json);
   if (taxacache[ptvk]) {
      taxa = JSON.parse(taxacache[ptvk]);
      if (taxa) {
         taxa.designations = [];
         for(d in deslist) {
            des = deslist[d];
            taxa.designations.push(des.designation.label);
         }
         taxacache[ptvk] = JSON.stringify(taxa);
         s2_nbntaxa_edittaxa(ptvk);
      }
   }
}

var element;
function s2_nbntaxa_eloffsets(el) {
	var x = 0;
   var y = 0;
	if (el.offsetParent) {
	  do {
	  	  x += el.offsetLeft;
		  y += el.offsetTop;
	  } while (el = el.offsetParent);
	}
	return {"x":x,"y":y};
}

function s2_nbntaxa_colnamefromjsonprop(prop) {
   prop = prop.replace(/([A-Z])/g," $1");
   prop = prop.replace(/^([a-z])/,function(v) {return v.toUpperCase();});
   return prop;
}


function s2_nbntaxa_remascolumn(id) {
   var args,tvk,taxa,tab,tabid,tbody,tr,th,td,p,prop,x,c,node;
   args = s2_disectid(id);
   tabid = args.t;
   p = args.p;
   prop = s2_nbntaxa_colnamefromjsonprop(p);
   
   tab = document.getElementById(tabid);
   s2_tabremcols(tab,[prop]);
   delete nbn_tcols[tabid][p];
   hidedialog();
}
function s2_nbntaxa_addascolumn(id) {
   var args,tvk,taxa,tab,tabid,tbody,tr,th,td,p,prop,x,c,node;
   args = s2_disectid(id);
   tabid = args.t;
   p = args.p;
   prop = s2_nbntaxa_colnamefromjsonprop(p);
      
   tab = document.getElementById(tabid);
   tvkcols = s2_nbntaxa_gettvkcols(tab);
   hidedialog();
   for(x in tvkcols) {
      c = tvkcols[x];
      s2_tabaddcols(tab,(c+1),[prop]);
      // populate column
      tbody = tab.tBodies[0];
      for(rx=0;rx<tbody.rows.length;rx++) {
         tr = tbody.rows[rx];
         // get link alt from tvk col
         tvk = tr.cells[c].firstChild.alt;
         taxa = JSON.parse(taxacache[tvk]);
         node = null;
         if (taxa[p]) node = s2_objecttonode(taxa[p]);
         if (node) tr.cells[c+1].appendChild(node);
         
      }
   } 
   if (!nbn_tcols[tabid]) nbn_tcols[tabid] = [];
   nbn_tcols[tabid][p] = prop;     
}
function s2_renamecol(th,name) {
   th.replaceChild(document.createTextNode(name),th.firstChild);
}
function s2_tabremcols(tab,cols) {
   var tbody,tr,th,td,cx,rx,col,clx,cel,remcols;
   tr = tab.tHead.rows[0];
   remcols = [];
   for(cx in cols) {
      col = cols[cx];
      for(clx=tr.cells.length-1;clx>=0;clx--) {
         cel = tr.cells[clx];
         if (cel.firstChild.nodeValue == col) {
            remcols.push(clx);
            tr.removeChild(cel);
         }
      } 
   } 
   tbody = tab.tBodies[0];
   for(rx=0;rx<tbody.rows.length;rx++) {
      tr = tbody.rows[rx];
      for(clx=tr.cells.length-1;clx>=0;clx--) {
         cel = tr.cells[clx];
         if (remcols.indexOf(clx)>=0) {
            tr.removeChild(cel);
         }
      }
   }  
}
function s2_tabaddcols(tab,beforeindex,cols) {
   var tbody,tr,th,td,cx,rx,col;
   tr = tab.tHead.rows[0];
   for(cx in cols) {
      col = cols[cx];
      th = document.createElement('TH');
      th.appendChild(document.createTextNode(col));
      if ((beforeindex) >= tr.childNodes.length) tr.appendChild(th); 
      else tr.insertBefore(th,tr.childNodes[beforeindex+1]); 
   }
   tbody = tab.tBodies[0];
   for(rx=0;rx<tbody.rows.length;rx++) {
      tr = tbody.rows[rx];
      for(cx in cols) {
         td = document.createElement('TD');
         if ((beforeindex) >= tr.childNodes.length) tr.appendChild(td); 
         else tr.insertBefore(td,tr.childNodes[beforeindex+1]);
      }
   }   
}

// move to other file
function s2_livedialog() {
   return (document.getElementById('dialog-container').style.display == 'block');
}  



/* 
   NBN REST API search dialog
*/ 
var taxasearchcache = [];
function s2_nbntaxa_search(term) {
   var url;
   if (!taxasearchcache[term] || taxasearchcache[term] == 'null') { 
      url = nbnapiurl+'/search/taxa?q='+term;
      s2_nbntaxa_send(url,s2_nbntaxa_loadsearchjson,term);
   } else {
      s2_nbntaxa_loadsearchjson(taxasearchcache[tvk]);
   }            
}
function s2_nbntaxa_loadsearchjson(json) {
   var taxa,tvk,ctvk,nx,cx,cid,cel;
   results = JSON.parse(json);
   if (!taxasearchcache[term] || taxasearchcache[term] == 'null') taxasearchcache[term] = json;
   // if the commmon name is not populated get the synonyms straight away
   
   //if (!taxa.commonName) s2_nbntaxa_getsynonyms(tvk);
   //if (!taxa.designations) s2_nbntaxa_getdesignations(tvk);
   
   
}
function s2_nbntaxa_loadsearchtaxa(cel,taxa) {

}
