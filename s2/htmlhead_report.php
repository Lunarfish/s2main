<?php //error_reporting(E_ALL); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Second Site @ YHEDN</title>
<link rel="shortcut icon" href="../favicon.ico"/>
<link rel='stylesheet' href='../styles/usb.css' media='screen'/>
<link rel='stylesheet' href='../styles/usb2.css' media='screen'/>
<link rel='stylesheet' href='../styles/cdm.css' media='screen' />
<link rel='stylesheet' href='../styles/snc.css' media='screen' />
<link rel='stylesheet' href='../styles/s2_gmap.css' media='screen' />
<link rel='stylesheet' href='../styles/fileuploader.css' media='screen' />
<link rel='stylesheet' href='../styles/s2_color_border.css' media='screen' />
<link rel='stylesheet' href='../styles/user_styles.css' media='screen'/>
<link rel='stylesheet' href='../styles/s2_print.css' media='print' />
<script type='text/javascript' src='../lib/htmlentities.js'></script>
<script type='text/javascript' src='../lib/encoder.js'></script>
<script type='text/javascript' src='../lib/msword.js'></script>
<script type='text/javascript' src='../lib/json/json2.js'></script>
<script type='text/javascript' src='../lib/calendarButtons.js'></script>

<script type='text/javascript' src='../lib/proj4js/proj4js-combined.js'></script>
<!-- British National Grid --><script type='text/javascript' src="../lib/proj4js/defs/EPSG27700.js"></script><!-- Bounds: -7.5600, 49.9600, 1.7800, 60.8400 -->
<!-- OSNI 1952 Northern Irish Grid --><script type='text/javascript' src="../lib/proj4js/defs/EPSG29901.js"></script><!-- Bounds: -8.2000, 54.0100, -5.4200, 55.3100 -->
<!-- TM65 Irish National Grid --><script type='text/javascript' src="../lib/proj4js/defs/EPSG29902.js"></script><!-- Bounds: -13.4200, 48.1300, -5.2000, 56.7200 -->
<!-- WGS84 --><script type='text/javascript' src="../lib/proj4js/defs/EPSG4326.js"></script>

<script type='text/javascript' src='../lib/coordinateConversions.js'></script>
<script type='text/javascript' src='../lib/genericInterface.js'></script>
<script type='text/javascript' src='../lib/screen.js'></script>
<script type='text/javascript' src='../lib/s2_v1_1.js'></script>
<script type='text/javascript' src='../lib/cdm/cdm_encrypt.js'></script>
<script type='text/javascript' src='../lib/cdm/cdm_authenticate2_v1_1.js'></script>
<script type='text/javascript' src='../lib/cdm/cdm_orequest_v1_1.js'></script>
<script type='text/javascript' src='../lib/cdm/cdm_dialog.js'></script>
<script type='text/javascript' src='../lib/tablemanager.js'></script>
<script type='text/javascript' src='../lib/s2_deriveddata.js'></script>
<script type='text/javascript' src='../lib/s2_externaldata.js'></script>
<script type='text/javascript' src='../lib/s2_extend_v1_1.js'></script>
<script type='text/javascript' src='../lib/s2_files.js'></script>
<script type='text/javascript' src='../lib/s2_importer_v1_1.js'></script>
<script type='text/javascript' src='../lib/s2_postprocess.js'></script>
<script type='text/javascript' src='../lib/fileuploader.js'></script>

<!--
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>
<script type='text/javascript' src='../lib/s2_gmapv3.js'></script>
-->

<script type='text/javascript' src='http://maps.google.com/maps?file=api&v=2&key=<?php print $gmapapikey;?>'></script>
<script type='text/javascript' src='../lib/s2_gmapv2.js'></script>

<script type='text/javascript' src='../lib/G_imanipulator_o.js'></script>
<script type='text/javascript' src='../lib/nbn/nbntaxa.js'></script>
<!--[if gte IE 5]>
<style type='text/css' media='screen'>
div#page {
   width: 90%;
}
div#dialog {
   width: 80%;
}
</style>  
<![endif]-->
</head>
