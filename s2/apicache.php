<?php
session_start();
error_reporting(E_ERROR);
$paths = array(
   './',
   './lib/',
   './interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_DBT.php');       
include_once('api/S2_Cache.php');
include_once('httpgetheader.php');
include_once('post.php');
include_once('put.php');

$s2api_baseurl = "http://${server}/${folder}/api.php";

$iurl = stripslashes($_SERVER['REQUEST_URI']);
$iscr = preg_replace('/\//','.',stripslashes($_SERVER['SCRIPT_NAME']));
$endpoint = preg_replace("/$iscr/","",$iurl);
$exp = (preg_match('/login/',$iurl))?-1:null;

$url = $s2api_baseurl . $endpoint;

// It's sensible to cache PUT and POST requests 
$api = new S2_API_Cache();            
$json = (isset($_POST) && count($_POST)>0)? post($url,$_POST): ((isset($_PUT) && count($_PUT)>0)? put($url,$_PUT): $api->get($url,$exp));
//$json = $nbn->get($url);
echo $json;
?>