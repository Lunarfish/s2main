<?php                                                  
error_reporting(E_ALL);
$cdmpath = "./lib/";
ini_set('include_path', $cdmpath);
include_once('jsonprofiles/jsonprofile_functions.php');
include_once('Zend/Db.php');
include_once('snc/SnCDatabaseConnect2.php');
//$json = readJSONFile('./template/GENERA_V0.2.json',false);
$json = readJSONFile('./template/Contacts_V0.1.json',false);
$action = $_REQUEST['a'];
//echo $json;
//*
$template = json_decode($json);
if ($template) {
   $db = SnC_getDatabaseConnection();
   print "<h1>$template->Template $template->Version</h1>";
   $dbscript = '';
   //foreach ($template->Actions as $a) {
   $a = $template->Actions[$action];
      print "<h2>$a->Name</h2>";
      switch ($a->Method) {
      case 'Define': {
         foreach ($template->Tables as $t) {
            //print "<pre>";print_r($t);print "</pre>";
            $tablename = "$t->Prefix$t->Name"; 
            print "<h3>$tablename</h3>";
            $createtablestatement = "CREATE TABLE $tablename (\n";
            foreach ($t->Columns as $c) {
               foreach ($template->DataTypes as $dt) {
                  if ($dt->Name == $c->DataType) {
                     $createtablestatement .= "\t$c->Name $dt->Definition,\n";                     
                  }
               }
               if ($c->DataType == 'Unique Key') {
                  $primarykeystatement = "\tPRIMARY KEY ($c->Name)\n";
               }            
            }
            $createtablestatement .= $primarykeystatement.");\n";
            print "<pre>$createtablestatement</pre>";
            $dbscript .= $createtablestatement;            
         }
         $db->exec($dbscript);      
      }break;
      case 'Insert': {
         $type = $a->Type;
         foreach ($template->Tables as $kt => $t) {
            $isdata = false;
            eval("\$isdata = isset(\$t->$type);");
            if ($isdata) {
               $data = null;
               eval("\$data = \$t->$type;");
               $tablename = "$t->Prefix$t->Name";
               foreach($data as $kd => $d) {
                  $insertdata = (array) $d;
                  $db->insert($tablename,$insertdata);
                  $id = $db->lastInsertId();
                  $hardcoded = false;
                  $eval = "\$hardcoded = isset(\$insertdata[\"${tablename}_Id\"]);";
                  eval($eval);
                  if (!$hardcoded) {
                     $eval = "\$template->Tables[$kt]->${type}[$kd]->${tablename}_Id = $id;"; 
                     eval($eval);
                  }
               } 
               eval("print_r(\$template->Tables[\$kt]->$type);"); 
            }         
         }
      }break;
      case 'UpdateGenera': {
         // foreach table 
         //    input ADM_Table
         //    foreach column 
         //       input ADM_Column
         //       input ADM_ForeignKey
         
         // get applications for reference to ids
         $query = $db->select();
         $query->from('ADM_Application');
         $apps = $db->fetchAll($query);

         // get datatypes for reference to ids          
         $query = $db->select();
         $query->from('DEV_DataType');
         $dts = $db->fetchAll($query);
         
         // create empty arrays and fill with insert data for use in fkeys.         
         $tables = array();
         $columns = array();
         $fks = array();
         foreach ($template->Tables as $kt => $t) {
            // 3 loops through the tables 
            // put tables in first 
            // then primary keys and other columns 
            // then foreign keys 
            $tablename = "$t->Prefix$t->Name";
            
            $dname = (isset($t->DisplayName))?$t->DisplayName:$t->Name; 
            $tdata = array (
               'Name' => $tablename,
               'DisplayName' => $dname,
               'IsSearchable' => (isset($t->IsSearchable))?(is_bool($t->IsSearchable))?$t->IsSearchable:($t->IsSearchable == 'TRUE'):true,
               'IsEditable' => (isset($t->IsEditable))?(is_bool($t->IsEditable))?$t->IsEditable:($t->IsEditable == 'TRUE'):true,
               'CreatedBy' => 1,
               'CreatedOn' => new Zend_Db_Expr('CURDATE()')
            );
            $ttablename = "ADM_Table";
            $db->insert($ttablename,$tdata);
            $tableid = $db->lastInsertId();
            $tables[$tablename] = $tableid;
            
            $sdata = array (
               'InTable' => $tablename,
               'FromTable' => null,
               'Name' => $dname,
               'CreatedBy' => 1,
               'CreatedOn' => new Zend_Db_Expr('CURDATE()')
            );
            $stablename = "ADM_Setting";
            $db->insert($stablename,$sdata);            
            // On the first pass you just add the table definitions and 
            // the ADM_Column table definitions 
            
            // On the 2nd pass you add the ForeignKey table
            // display name is more for application templates where you want to 
            // control how that column is rendered to the user
            // if no display name is supplied in the JSON column definition then 
            // the name field for that column is used. 
            foreach($t->Columns as $kc => $c) {
               $dtid = null;
               foreach ($dts as $dt) {
                  if ($dt['Name'] == $c->DataType) {
                     $dtid = $dt['DEV_DataType_Id'];
                  }                
               }
               $cdata = array (
                  'ADM_Table_Id' => $tableid,
                  'DEV_DataType_Id' => $dtid,
                  'Name' => $c->Name,
                  'DisplayName' => (isset($c->DisplayName))?$c->DisplayName:$c->Name,                  
                  'Mandatory' => (isset($c->Mandatory))?(is_bool($c->Mandatory))?$c->Mandatory:($c->Mandatory == 'TRUE'):true,
                  'IsUnique' => (isset($c->IsUnique))?(is_bool($c->IsUnique))?$c->IsUnique:($c->IsUnique == 'TRUE'):true,
                  'IsSearchable' => (isset($c->IsSearchable))?(is_bool($c->IsSearchable))?$c->IsSearchable:($c->IsSearchable == 'TRUE'):true,
                  'IsEditable' => (isset($c->IsEditable))?(is_bool($c->IsEditable))?$c->IsEditable:($c->IsEditable == 'TRUE'):true,
                  'CreatedBy' => 1,
                  'CreatedOn' => new Zend_Db_Expr('CURDATE()')
               );
               $ctablename = "ADM_Column";
               $db->insert($ctablename,$cdata);
               $colid = $db->lastInsertId();
               if (!isset($columns[$tablename])) $columns[$tablename] = array();
               $columns[$tablename][$c->Name] = $colid;                
            }
         }
         foreach ($template->Tables as $kt => $t) {
            $tablename = "$t->Prefix$t->Name";
            $tdname = (isset($t->DisplayName))?$t->DisplayName:$t->Name;                            
            foreach($t->Columns as $kc => $c) {
               switch($c->DataType) {
               case 'Linked Data': {
                  $name = $c->Name;
                  $dname = (isset($c->DisplayName))?$c->DisplayName:$c->Name;
                  $sname = (isset($c->SelectActionName))?$c->SelectActionName:"Set $dname";
                  $uname = (isset($c->UnselectActionName))?$c->UnselectActionName:"Unset $dname";
                  $aname = (isset($c->InsertActionName))?$c->InsertActionName:"Add $dname";
                  $fkdata = array(
                     'LinkFromTable' => $tablename,
                     'LinkFromColumn' => $c->Name,  
                     'LinkToTable' => $c->ToTable,
                     'Name' => $name,
                     'DisplayName' => $dname,
                     'SelectActionName' => $sname,
                     'UnselectActionName' => $uname,
                     'InsertActionName' => $aname,
                     'CreatedBy' => 1,
                     'CreatedOn' => new Zend_Db_Expr('CURDATE()')
                  );
                  $ftablename = "ADM_TableForeignKey";
                  $db->insert($ftablename,$fkdata);
                  $fkid = $db->lastInsertId();
                  
                  if (!($name == 'CreatedBy')) {
                     if(!isset($stablename)) $stablename = "$stable->Prefix$stable->Name";
                     $dname = ($sname == $dname)?"$tdname $dname":$sname; 
                     $sdata = array (
                        'InTable' => $c->ToTable,
                        'FromTable' => $tablename,
                        'Name' => $dname,
                        'CreatedBy' => 1,
                        'CreatedOn' => new Zend_Db_Expr('CURDATE()')
                     );
                     $db->insert($stablename,$sdata);
                  }
                  
                  if (!isset($fks[$tablename])) $fks[$tablename] = array();
                  $fks[$tablename][$c->Name] = $fkid;                                  
               }break;
               }
            }       
         }
      }break;
      case 'UpdatePermissions': {
         $default_permissions_table = 'USR_Default_Permissions';
         $table_permissions_table = 'PER_TablePermission';
         foreach ($template->DefaultPermissions as $kp => $p) {
            $query = $db->select();
            $query->from($default_permissions_table);
            $query->where('Domain = ?',$p->Domain);
            $query->where('Label = ?',$p->Label);
            $exists = $db->fetchAll($query);
            if (!count($exists)) {
               $tdata = array (
                  'Domain'       => $p->Domain,
                  'Label'        => $p->Label,
                  'Description'  => $p->Description,
                  'Value'        => $p->Value
               );
               $db->insert($default_permissions_table,$tdata);
               $tableid = $db->lastInsertId();
            }
         }
         foreach ($template->Tables as $kt => $t) {
            if (isset($t->Permissions)) {
               $table = "$t->Prefix$t->Name";
               $query = $db->select();
               $query->from('ADM_Table',array('ADM_Table_Id'));
               $query->where('Name = ?',$table);
               $qtables = $db->fetchAll($query);
               $tableid = 0;
               foreach($qtables as $qt => $tt) $tableid = $tt['ADM_Table_Id'];
               foreach($t->Permissions as $kp => $p) {
                  $tdata = array (
                     'ADM_Table_Id' => $tableid,
                     'Domain'       => $p->Domain,
                     'Permission'   => $p->Permission,
                     'LimitTo'      => (isset($p->LimitTo))?$p->LimitTo:null,
                     'CanSearch'    => in_array('Search',$p->Actions),
                     'CanSelect'    => in_array('Select',$p->Actions),
                     'CanAdd'       => in_array('Add',$p->Actions),
                     'CanEdit'      => in_array('Edit',$p->Actions),
                     'CanDelete'    => in_array('Delete',$p->Actions),
                     'CreatedBy'    => 1,
                     'CreatedOn'    => new Zend_Db_Expr("NOW()")
                  );
                  $db->insert($table_permissions_table,$tdata);
                  $tableid = $db->lastInsertId();
               }
            }
         }   
      }break;
      }   
   //}
   /*
   print "<pre>";
   print_r($template);
   print "</pre>";
   */
} else { 
   switch(json_last_error()) {
   case JSON_ERROR_DEPTH:
      echo ' - Maximum stack depth exceeded';
   break;
   case JSON_ERROR_CTRL_CHAR:
      echo ' - Unexpected control character found';
   break;
   case JSON_ERROR_SYNTAX:
      echo ' - Syntax error, malformed JSON';
   break;
   case JSON_ERROR_NONE:
      echo ' - No errors';
   break;
   }
}

//*/
?>