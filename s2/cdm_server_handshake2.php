<?php
error_reporting(E_ALL);
$action = $_REQUEST['a'];
$server = $_SERVER['PHP_SELF'];
$serverpaths = preg_split('/\//',$server);
array_pop($serverpaths);
$paths = array(
   './',
   './lib/',
   './interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('Zend/Db.php');
include('cdm/CDMServer_class2.php');
$server = $_SERVER['SERVER_NAME'].join('/',$serverpaths);
//print $server;
switch($action) {
case 'handshake': {
   $session = $_REQUEST['s'];
   $domain = $_REQUEST['d'];
   $e_client = $_REQUEST['e'];
   $cdm = new CDMServer();
   $cdm->populate($session);
   $cdm->setClient($domain);
   $cdm->setDomain($server);
   if ($domain != $server) {
      $k = substr($session,0,$cdm->blocksize);
      $cdm->setKey($k,1);
   }
   $cdmid = $cdm->getCDMSessionID();
//print "<pre>"; print_r($cdm); print "</pre>"; exit;
   $ejson = $cdm->handshake($domain,$e_client);
   echo $ejson;
}break;
case 'checkin': {
   $cdm_session_id = $_REQUEST['s'];
   $domain = $_REQUEST['d'];
   $e_client = $_REQUEST['e'];
   $cdm = new CDMServer();
   $cdm->populate(null,$cdm_session_id);
   $cdm->setClient($domain);
   $cdm->setDomain($server);
   $ejson = $cdm->checkin($domain,$e_client);
   //echo $cdm->decrypt($ejson);
   echo $ejson;
}break;
case 'request': {
   /* 
      how does the request process work 
      needs to be able to refer an external request
      this page constructs the response json and manhandles the php response 
      into JavaScript HTML. 
      
      meaning that this file needs to know the available requests and where to 
      send them.
   */
   $cdm_session_id = $_REQUEST['s'];
   $domain = $_REQUEST['d'];
   $e_client = $_REQUEST['e'];
   $cdm = new CDMServer();
   $cdm->setClient($domain);
   $cdm->populate(null,$cdm_session_id);
   $ejson = $cdm->checkin($domain,$e_client);
   $json = json_decode($cdm->decrypt($ejson));
   $client = json_decode($cdm->decrypt($e_client));
   if (isset($client->method))   $method = $client->method;
   if (isset($client->tnode))    $json->tnode = $client->tnode;
   $args = (isset($client->args))?$client->args:null;
   $json->response = json_decode($cap->callProjectMethod($cdm->getProject(),$method,$args));
   $ejson = $cdm->encrypt(json_encode($json));
   echo $ejson;
}break;
}
?>
