<?php error_reporting(E_ERROR); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Second Site @ YHEDN</title>
<link rel="shortcut icon" href="../favicon.ico"/>
<link rel='stylesheet' href='../styles/s2screen.css' media='screen' />
<link rel='stylesheet' href='../styles/s2print.css' media='print' />

<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="http://d3js.org/d3.v3.min.js"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places,geometry"></script>
<script type='text/javascript' src='../js/s2.js'></script>

<!--[if gte IE 5]>
<style type='text/css' media='screen'>                                           
div#page {
   width: 90%;
}
div#dialog {
   width: 80%;
}
</style>  
<![endif]-->
</head>
