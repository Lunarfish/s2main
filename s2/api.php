<?php
session_start();
error_reporting(E_ERROR);
$paths = array('./','./lib/','./interfaces');
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_DBT.php');       
include_once('nbn/NBN_Cache.php');
include_once('post.php');
include_once('put.php');

$iurl = stripslashes($_SERVER['REQUEST_URI']);
$iscr = preg_replace('/\//','.',stripslashes($_SERVER['SCRIPT_NAME']));
$endpoint = preg_replace("/$iscr/","",$iurl);
$endpoint = preg_replace("/\?.+/","",$endpoint);
// url format 
// <domain>/<tablename>
// <domain>/<tablename>/<id>
// <domain>/<tablename>/<id>/<viewname>
$bits = preg_split('/\//',$endpoint);
//print_r($bits);
array_shift($bits);
if (count($bits)>0) $domain =    array_shift($bits);
if (count($bits)>0) $itype =     array_shift($bits);
if (count($bits)>0) $current =   array_shift($bits);
if (count($bits)>0) $viewname =  array_shift($bits);
if (count($bits)>0) $ltype =     array_shift($bits);

$burl = "http://${server}/${folder}/";
$url = "${burl}${domain}/S2_query.php";
   


$req = array();
$req['IType'] = $itype;
$req['Action'] = (isset($current))?'View':'List';
if (isset($_REQUEST['q'])) {
   list($prop,$val) = preg_split('/\:/',$_REQUEST['q']);
   $set = array();
   $set[$itype] = array('SearchBy' => $prop,'SearchFor' => $val);
   $req['CurrentSettings'] = $set;
}

if (isset($current)) $req['Current'] = $current; 
if (isset($viewname)) $req['ViewName'] = urldecode($viewname);
$req['NoPaging'] = 1;
$req['UInfo'] = ($_REQUEST['UInfo'])?$_REQUEST['UInfo']:null;

//print $url;print_r($req);exit;
$json = s2refer($url,$req);
//echo($json);exit;
$resp = json_decode($json);
if (isset($resp->Status)) {
   $json = '{"status":0,"message":"Permission denied"}';
} else if (isset($ltype)) {
   foreach($resp->LinkedData as $link) if ($link->IType == $ltype) $json = json_encode($link->ListData);  
} else if (!isset($current)) {
   $json = json_encode($resp->ListData);
} else if (isset($viewname)) {
   $json = json_encode($resp->LinkedData);
} else {
   $json = json_encode($resp->Data);
}
echo $json;



function s2refer($url,$request) {
   $curl = curl_init($url);
   if (isset($_POST)) {
      $post = (array)$request;
      foreach ($post as $key => $val)  {
         switch (gettype($val)) {
         case 'object': $post[$key] = json_encode($val);break;
         case 'array': $post[$key] = json_encode($val);break;
         }
      }
      curl_setopt ($curl, CURLOPT_POST, true);
   	curl_setopt ($curl, CURLOPT_POSTFIELDS, $post);
   }
   // Don't return HTTP headers. Do return the contents of the call
   
   $sid = $_COOKIE['PHPSESSID'];
   $cookie_jar = tempnam('/tmp','cookie');
   curl_setopt($curl, CURLOPT_COOKIEJAR, $cookie_jar);
   curl_setopt($curl, CURLOPT_COOKIE, "PHPSESSID=$sid;");
   curl_setopt($curl, CURLOPT_COOKIEFILE, $cookie_jar);
         
   curl_setopt($curl, CURLOPT_HEADER, false);
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
   // Make the call
   $response = curl_exec($curl);
   curl_close($curl);
   return $response;
}

function enc_unravel($k3,$s=null) {
   if (!isset($s)) $s = session_id();
   $k = new KLib($s);
   $ks = json_decode($k->getkeys());
   $i = 0;$k1='';$k2='';
   while($i < strlen($k3)) {
      $k1 .= substr($k3,($i*3),1);
      $k2 .= substr($k3,(($i*3)+1),2);
      $i++; 
   }
   $k2d = decrypt($k2,$ks->k1,$ks->k2);
   return ($k1 == $k2d);
}
?>