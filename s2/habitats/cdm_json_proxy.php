<?php
// PHP Proxy example for Yahoo! Web services. 
// Responds to both HTTP GET and POST requests
//
// Author: Jason Levitt, modified by Tim Tully
// December 7th, 2005
// Oct 23, 2006
//

// Get the REST call path from the AJAX application
// Is it a POST or a GET?
$path = (isset($_POST['target'])) ? $_POST['target'] : $_GET['target'];
$url = $path;
// Open the Curl session
$session = curl_init($url);

// If it's a POST, put the POST data in the body
if ($_POST['target']) {
	$postvars = '';
	while ($element = current($_POST)) {
		$postvars .= key($_POST).'='.$element.'&';
		next($_POST);
	}
	curl_setopt ($session, CURLOPT_POST, true);
	curl_setopt ($session, CURLOPT_POSTFIELDS, $postvars);
}

// Don't return HTTP headers. Do return the contents of the call
curl_setopt($session, CURLOPT_HEADER, false);
curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

// Make the call
$rv = curl_exec($session);

echo $rv;
curl_close($session);
?>

