<?php
/*
   eproxy.php uses an envelope similar to soap to redirect a request to 
   a different target. 
   
   eproxy is an encryption layer so the encoded request is received, decrypted 
   and executed, then the response is encrypted and sent back. 
   
   the eenv request contains the request to be forwarded as an object property
   and the target for where the request should be forwarded to.
   
   the session and domain have to be passed outside the encrypted parameters to 
   allow the message to be successfully decoded.
   
*/
error_reporting(E_ERROR);
$cdmpath = "./lib/";
set_include_path(get_include_path() . PATH_SEPARATOR . $cdmpath);
include_once('Zend/Db.php');
$etype = (isset($_REQUEST['etype']))?$_REQUEST['etype']:null;

switch($etype) {
case 'AB': {
   include_once('cdm/cdm_access_functions.php');
   include_once('klib.php');
   $sid = $_REQUEST['session'];
   $e_request = $_REQUEST['request'];
   $k = new KLib($sid);
   $ky = json_decode($k->getkeys());
   $decode = decrypt($e_request,$ky->k1,$ky->k2);
   $request = json_decode($decode);

   $url = $request->target;
   // Open the Curl session
   $curl = curl_init($url);
   // get request parameters are contained in the url
   if (isset($_POST)&&isset($request->request)) {
      $post = (array)$request->request;
      if (is_object($post)) $post = (array) $post; 
      if (is_array($post)) {
         foreach ($post as $key => $val)  {
            switch (gettype($val)) {
            case 'object': $post[$key] = json_encode($val);break;
            case 'array': $post[$key] = json_encode($val);break;
            }
         }
      }
      curl_setopt ($curl, CURLOPT_POST, true);
   	  curl_setopt ($curl, CURLOPT_POSTFIELDS, $post);
   }
   // Don't return HTTP headers. Do return the contents of the call
   curl_setopt($curl, CURLOPT_HTTPHEADER,array("Expect:  "));
   curl_setopt($curl, CURLOPT_HEADER, false);
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
   // Make the call
   $response = curl_exec($curl);
   curl_close($curl);
   $e_response = encrypt($response,$ky->k1,$ky->k2);
   echo $e_response;
}break;
default: {
   include('cdm/CDMServer_class2.php');
   
   $cdm_session_id = $_REQUEST['session'];
   $domain = $_REQUEST['domain'];
   $e_request = $_REQUEST['request'];
   //print_r($_REQUEST);
   //exit;
   
   $cdm = new CDMServer();
   $cdm->populate(null,$cdm_session_id);
   //print "<p>".$cdm->getKey(1)."<br/>".$cdm->getKey(2)."</p>";
   $ejson = $cdm->checkin($domain,$e_request);
   $json = json_decode($cdm->decrypt($ejson));
   // do something with checkin response
   $request = json_decode($cdm->decrypt($e_request));
   
   //print_r($request);
   //exit;
   $url = $request->target;
   // Open the Curl session
   $curl = curl_init($url);
   // get request parameters are contained in the url
   if (isset($_POST)&&isset($request->request)) {
      $post = $request->request;
      if (is_object($post)) $post = (array) $post; 
      if (is_array($post)) {
         foreach ($post as $key => $val)  {
            switch (gettype($val)) {
            case 'object': $post[$key] = json_encode($val);break;
            case 'array': $post[$key] = json_encode($val);break;
            }
         }
      }
      curl_setopt ($curl, CURLOPT_POST, true);
   	curl_setopt ($curl, CURLOPT_POSTFIELDS, $post);
   }
   // Don't return HTTP headers. Do return the contents of the call
   curl_setopt($curl, CURLOPT_HEADER, false);
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
   // Make the call
   $response = curl_exec($curl);
   //print $response; exit;
   curl_close($curl);
   $e_response = $cdm->encrypt($response);
   echo $e_response;
   //echo $cdm->decrypt($e_response);
}break;
}
?>
