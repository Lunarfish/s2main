<?php
session_start();
error_reporting(E_ERROR);
$paths = array(
   './',
   './lib/',
   './interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_DBT.php');       
include_once('api/S2_Cache.php');


$domain = $_REQUEST['domain'];
$itype = $_REQUEST['itype'];
$bycol = $_REQUEST['col'];

$searchstring = "${domain}/${itype}?q=${bycol}";
//print $searchstring;   
 
$nbn = new S2_API_Cache(); 
$settings = json_decode('{
   "S2_API_Cache": {
         "SearchBy":"Endpoint",
         "SearchFor":"'.$searchstring.'"
   }  
}');           


$taxa = $nbn->selectAll($settings);

//print "<pre>"; print_r($taxa); print "</pre>"; exit;

if (count($taxa)>0) {
	$djson = '{';
	while($taxon = array_shift($taxa)) {
		$url = $taxon['Endpoint'];
		$exp = preg_replace('/(\d{2})\/(\d{2})\/(\d{4})/','$3-$2-$1',$taxon['Expiry']);
		$tdy = date('Y-m-d',mktime());
		$isval = ($exp > $tdy);
		if ($isval) {
			$tvk = substr($url,-16);
			$djson .= "\"$tvk\":" . $taxon['JSON'] . ",";            
		}
	}
	$djson = preg_replace('/\,$/','}',$djson);
} else $djson = '{}';

$djson = preg_replace('/\,$/','}',$djson);
$json = "{\"local_lists\":$djson}";
echo $json;
?>