<?php
session_start();
error_reporting(E_ALL);
$paths = array('../','../lib/','../interfaces');
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_DBT.php');       
include_once('nbn/NBN_Cache.php');
include_once('post.php');
include_once('put.php');


$taxa = array(
"Botaurus stellaris",
"Caprimulgus europaeus",
"Circus aeruginosus",
"Panurus biarmicus",
"Grus grus",
"Arvicola amphibius",
"Bembidion humerale",
"Curimopsis nigrita",
"Phaonia jaroschewskii",
"Carduelis chloris",
"Corvus monedula",
"Falco tinnunculus",
"Emberiza schoeniclus",
"Corvus frugilegus",
"Columba palumbus",
"Motacilla flava",
"Emberiza calandra",
"Carduelis carduelis",
"Perdix perdix",
"Vanellus vanellus",
"Carduelis cannabina",
"Sturnus vulgaris",
"Columba oenas",
"Alauda arvensis",
"Passer montanus",
"Streptopelia turtur",
"Sylvia communis",
"Emberiza citrinella",
"Numenius arquata",
"Cygnus olor",
"Anas crecca",
"Tringa totanus",
"Egretta garzetta",
"Gallinago gallinago",
"Acrocephalus scirpaceus",
"Cettia cetti",
"Acrocephalus schoenobaenus",
"Ardea cinerea",
"Alcedo atthis",
"Haematopus ostralegus",
"Riparia riparia",
"Rana temporaria",
"Bufo bufo",
"Vipera berus",
"Natrix natrix",
"Zootoca vivipara",
"Lutra lutra",
"Coenonympha tullia",
"Triturus cristatus",
"Lissotriton helveticus",
"Lissotriton vulgaris",
"Calopteryx splendens",
"Hottonia palustris",
"Lathyrus palustris",
"Potentilla palustris",
"Metrioptera brachyptera",
"Potamogeton",
"Callitriche truncata",
"Panagaeus cruxmajor",
"Laccophilus poecilus",
"Agabus striolatus",
"Philorhizus sigma",
"Paradromius longiceps",
"Chiroptera"
);

$nbn = new NBN_Cache();
$burl = nbn_getapibaseurl();
$tvklist = array();
foreach ($taxa as $ti => $taxon) {
   $url = $burl . 'search/taxa?q=' . urlencode($taxon);
   $sjson = $nbn->get($url);
   if (isset($sjson)) {
      $sjobj = json_decode($sjson);
      //print "<h2>$taxon</h2>";
      //print_r($sjobj);
      if (isset($sjobj)) {
         $matched = false;
         foreach($sjobj->results as $ri => $rtaxon) {
            $tvk = null;
            $lat = null;
            if (!$matched && $rtaxon->name == $taxon) {
               $matched = true; 
               $tvk = $rtaxon->ptaxonVersionKey;
               if ($rtaxon->taxonVersionKey != $rtaxon->ptaxonVersionKey) {
                  $purl = $burl . 'taxa/' . $rtaxon->ptaxonVersionKey;
                  $pjson = $nbn->get($purl);
                  $pjobj = json_decode($pjson);
                  $lat = $pjobj->name;
               } else $lat = $taxon;
               $entry = (object)array('lat'=>$lat,'tvk'=>$tvk);
               $entry->children = getchildren($rtaxon);
               $tvklist[] = $entry;
            }
         }
         if (!$matched) {
            $entry = (object)array('lat'=>$taxon,'tvk'=>'NOT MATCHED');
            $entry->results = $sjobj;
            $tvklist[] = $entry;
         } 
      }
   }     
}

//print_r($tvklist);
print "<pre>";
print "Latin,TVK\n";
foreach($tvklist as $ei => $entry) {
   printtaxa($entry);
}
print "</pre>";
function printtaxa($taxa) {
   print "$taxa->lat,$taxa->tvk\n";
   if (isset($taxa->children)&&count($taxa->children) > 0) {
      foreach($taxa->children as $ci => $child) {
         printtaxa($child);
      }
   }
   if ($taxa->tvk == 'NOT MATCHED') {
      print_r($taxa->results);
   }
}


function getchildren($taxon) {
   global $burl,$nbn;
   $children = array();
   if ($taxon->rank != 'Species') {
      $tvk = $taxon->ptaxonVersionKey;
      $suburl = $burl . 'taxa/' . $tvk . '/children';
      $cjson = $nbn->get($suburl);
      $cjobj = json_decode($cjson);
      foreach($cjobj as $ci => $ctaxon) {
         $entry = (object)array('lat'=>$ctaxon->name,'tvk'=>$ctaxon->ptaxonVersionKey);
         $entry->children = getchildren($ctaxon);
         $children[] = $entry;
      }
   }
   return $children;
}
?>