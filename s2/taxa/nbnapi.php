<?php
session_start();
error_reporting(E_ALL);
$paths = array('../','../lib/','../interfaces');
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_DBT.php');       
include_once('nbn/NBN_Cache.php');
include_once('post.php');
include_once('put.php');

//$nbnapi_baseurl = 'https://staging-data.nbn.org.uk/api';
$nbnapi_baseurl = 'https://data.nbn.org.uk/api';
$iurl = stripslashes($_SERVER['REQUEST_URI']);
$iscr = preg_replace('/\//','.',stripslashes($_SERVER['SCRIPT_NAME']));
$endpoint = preg_replace("/$iscr/","",$iurl);

$url = $nbnapi_baseurl . $endpoint;
//print $url;exit;
// You can't (and there's no sense trying to) cache PUT and POST requests 
$nbn = new NBN_Cache();
$json = (isset($_POST) && count($_POST)>0)? post($url,$_POST): ((isset($_PUT) && count($_PUT)>0)? put($url,$_PUT): $nbn->get($url));
echo $json;
?>