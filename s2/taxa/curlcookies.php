<?php

// set a cookie in a curl request
curl_setopt($ch, CURLOPT_COOKIE, "loopermanlooperman=$sessid")
/*
CURLOPT_COOKIE
The contents of the "Cookie: " header to be used in the HTTP request. Note that 
multiple cookies are separated with a semicolon followed by a space 
(e.g., "fruit=apple; colour=red")

http://www.php.net/manual/en/function.curl-setopt.php/
*/


// retrieve a cookie from a curl response 
$ch = curl_init('http://www.google.com/');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// get headers too with this line
curl_setopt($ch, CURLOPT_HEADER, 1);
$result = curl_exec($ch);
// get cookie
preg_match('/^Set-Cookie:\s*([^;]*)/mi', $result, $m);

parse_str($m[1], $cookies);
var_dump($cookies);


?>