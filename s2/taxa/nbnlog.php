<?php
session_start();
error_reporting(E_ERROR);
$paths = array(
   '../',
   '../lib/',
   '../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_DBT.php');       
include_once('nbn/NBN_Cache.php');
include_once('cdm/cdm_access_functions.php');
include_once('klib.php');
include_once('httpgetheader.php');
include_once('post.php');
include_once('put.php');



$nbnapi_baseurl = 'https://data.nbn.org.uk/api';

//$nbnapi_baseurl = 'http://localhost/s2/taxa/reflect.php';
$iurl = stripslashes($_SERVER['REQUEST_URI']);
$iscr = preg_replace('/\//','.',stripslashes($_SERVER['SCRIPT_NAME']));

$endpoint = preg_replace("/$iscr/","",$iurl);
$exp = (preg_match('/login/',$iurl))?-1:null;
if (isset($_REQUEST['enc'])) {
   $e_request = $_REQUEST['enc'];
   $sid = session_id();
   $k = new KLib($sid);
   $ky = json_decode($k->getkeys());
   $decode = decrypt($e_request,$ky->k1,$ky->k2);
   $endpoint = preg_replace('/\?enc\=.+/',"?$decode",$endpoint);
} 

$url = $nbnapi_baseurl . $endpoint;

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_HEADER, true);
$resp = curl_exec($ch);
$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
$header = substr($resp, 0, $header_size);
$cookie = httpgetheader($header,'Set-Cookie');
//$_SESSION['nbn.token_key'] = $cookie;
nbn_saveheadercookie($cookie);
$json = substr($resp, $header_size);


echo $json;

function nbn_saveheadercookie($cookie) {
   // nbn.token_key=3y4eQ8PtTWlzrKyhB2bqtYzzwMxJ5fM3MoFBBBDzdTgYYJPzBacRAoipC82XiTxn_8-WbXu5rq3drRvSAiNyUg;Version=1;Comment="authentication token";Domain=.nbn.org.uk;Path=/
   $cparts = preg_split('/\;/',$cookie);
   $cargs = array();
   while(count($cparts)>0) {
      $cpart = array_shift($cparts);
      list($key,$val) = preg_split('/\=/',$cpart);
      if (!isset($cargs['Name'])) {
         $cargs['Name'] = $key;
         $cargs['Value'] = $val;
      } else {
         $cargs[$key] = $val;   
      } 
   }
   return setcookie ($cargs['Name'],$cargs['Value'],0,'/');//$cargs['Path'],$cargs['Domain']);
}                                                                   
?>