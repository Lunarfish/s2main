<?php
set_time_limit(0);
$paths = array(
   '../',
   '../../'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once 'Zend/Db.php';
$dbConfig = array(
   'dbname'	      => 'taxondict',
   'host'         => 'localhost',
   'username'     => 'taxon',
   'password'     => 'taxon',
   'persistent'   => true
);
$db = Zend_Db::factory('Pdo_MySQL', $dbConfig);
function gethierarchy() {
   $hierarchy = array(
      'Kingdom',
      'Phylum',
      'Superclass',
      'Class',
      'Subclass',
      'Superorder',
      'Order',
      'Suborder',
      'Superfamily',
      'Family',
      'Subfamily',
      'Genus',
      'Subgenus',
      'Species aggregate',
      'Species',
      'Species hybrid',
      'Subspecies',
      'Subspecies hybrid'
   );
   return $hierarchy;
}

function getrootquery() {
   global $db;
   $query = $db->select()
               ->distinct()
               ->from(array('ITN'   => 'INDEX_TAXON_NAME'),array());
   $query->join(array('TLI'   => 'TAXON_LIST_ITEM'),  "ITN.TAXON_LIST_ITEM_KEY = TLI.TAXON_LIST_ITEM_KEY",array());
   $query->join(array('NS'    => 'NAMESERVER'),       "NS.INPUT_TAXON_VERSION_KEY = TLI.TAXON_VERSION_KEY",array());
   $query->join(array('TLI2'  => 'TAXON_LIST_ITEM'),  "TLI2.TAXON_LIST_ITEM_KEY = NS.RECOMMENDED_TAXON_LIST_ITEM_KEY", array("PARENT","TAXON_LIST_ITEM_KEY"));
   $query->join(array('TV2'   => 'TAXON_VERSION'),    "TV2.TAXON_VERSION_KEY = NS.RECOMMENDED_TAXON_VERSION_KEY", array("TAXON_VERSION_KEY"));
   $query->join(array('ITN2'  => 'INDEX_TAXON_NAME'), "ITN2.TAXON_LIST_ITEM_KEY = TLI2.TAXON_LIST_ITEM_KEY", array("ACTUAL_NAME","COMMON_NAME"));
   $query->join(array('TR'    => 'TAXON_RANK'),       "TR.TAXON_RANK_KEY = TLI2.TAXON_RANK_KEY", array("RANK" => "LONG_NAME"));
//print $query->__toString();exit;   
   return $query;
}

function gettaxa($action,$val,$inc_hierarchy=true) {
   global $db;
   $r = null;
   $f_one = false;
   $get_hierarchy = false;
   $q = getrootquery();
   switch ($action) {
   case 'getself': {
      $q->where('TLI.TAXON_LIST_ITEM_KEY = ?',$val);
      $get_hierarchy = $inc_hierarchy;
      $f_one = true;
   }break;
   /*
   case 'getchildrenof': {
      $q->where('TLI.PARENT = ?',$val);
   }break;
   */
   case 'getchildrenof': {
      $t = gettaxa('getself',$val,false);
      $r = $t['RANK'];
      $h = gethierarchy(); 
      $hf = array_flip($h);
      $hi = $hf[$r];
      $c_ranks = array();
      $c_ranks[] = $h[$hi+1];
      $c_ranks[] = $h[$hi+2];
      $q = $db->select()
            ->distinct()
            ->from(array('NS'   => 'NAMESERVER'),array());
      $q->join(array('TLIP'   => 'TAXON_LIST_ITEM'),"TLIP.TAXON_VERSION_KEY = NS.INPUT_TAXON_VERSION_KEY",array());
      $q->join(array('TLIC'   => 'TAXON_LIST_ITEM'),"TLIC.PARENT = TLIP.TAXON_LIST_ITEM_KEY",array());
      $q->join(array('NS2'    => 'NAMESERVER'),"TLIC.TAXON_VERSION_KEY = NS2.INPUT_TAXON_VERSION_KEY",array());
      $q->join(array('TLI2'   => 'TAXON_LIST_ITEM'),  "TLI2.TAXON_LIST_ITEM_KEY = NS2.RECOMMENDED_TAXON_LIST_ITEM_KEY", array("PARENT","TAXON_LIST_ITEM_KEY"));
      $q->join(array('TV2'    => 'TAXON_VERSION'),    "TV2.TAXON_VERSION_KEY = NS2.RECOMMENDED_TAXON_VERSION_KEY", array("TAXON_VERSION_KEY"));
      $q->join(array('ITN2'   => 'INDEX_TAXON_NAME'), "ITN2.TAXON_LIST_ITEM_KEY = TLI2.TAXON_LIST_ITEM_KEY", array("ACTUAL_NAME","COMMON_NAME"));
      $q->join(array('TR'     => 'TAXON_RANK'),       "TR.TAXON_RANK_KEY = TLI2.TAXON_RANK_KEY", array("RANK" => "LONG_NAME"));
      $q->where('NS.RECOMMENDED_TAXON_LIST_ITEM_KEY = ?',$val);
      $q->where('TR.LONG_NAME IN(?)',$c_ranks);
      $q->order('TLI2.SORT_CODE');
//print $q->__toString();exit;
   
   }break;
   /*
   case 'getparentof': {
      $q->join(array('TLIC' => 'TAXON_LIST_ITEM'),"TLIC.PARENT = TLI.TAXON_LIST_ITEM_KEY");
      $q->where('TLIC.TAXON_LIST_ITEM_KEY = ?',$val);
   }break;
   */
   case 'getparentof': {
      $t = gettaxa('getself',$val,false);
      $r = $t['RANK'];
      $h = gethierarchy(); 
      $hf = array_flip($h);
      $hi = $hf[$r];
      $p_ranks = array();
      $p_ranks[] = $h[$hi-1];
      $p_ranks[] = $h[$hi-2];
      $q = $db->select()
            ->distinct()
            ->from(array('NS'   => 'NAMESERVER'),array());
      $q->join(array('TLIC'   => 'TAXON_LIST_ITEM'),"TLIC.TAXON_VERSION_KEY = NS.INPUT_TAXON_VERSION_KEY",array());
      $q->join(array('TLIP'   => 'TAXON_LIST_ITEM'),"TLIC.PARENT = TLIP.TAXON_LIST_ITEM_KEY",array());
      $q->join(array('NS2'    => 'NAMESERVER'),"TLIP.TAXON_VERSION_KEY = NS2.INPUT_TAXON_VERSION_KEY",array());
      $q->join(array('TLI2'   => 'TAXON_LIST_ITEM'),  "TLI2.TAXON_LIST_ITEM_KEY = NS2.RECOMMENDED_TAXON_LIST_ITEM_KEY", array("PARENT","TAXON_LIST_ITEM_KEY"));
      $q->join(array('TV2'    => 'TAXON_VERSION'),    "TV2.TAXON_VERSION_KEY = NS2.RECOMMENDED_TAXON_VERSION_KEY", array("TAXON_VERSION_KEY"));
      $q->join(array('ITN2'   => 'INDEX_TAXON_NAME'), "ITN2.TAXON_LIST_ITEM_KEY = TLI2.TAXON_LIST_ITEM_KEY", array("ACTUAL_NAME","COMMON_NAME"));
      $q->join(array('TR'     => 'TAXON_RANK'),       "TR.TAXON_RANK_KEY = TLI2.TAXON_RANK_KEY", array("RANK" => "LONG_NAME"));
      $q->where('NS.RECOMMENDED_TAXON_LIST_ITEM_KEY = ?',$val);
      $q->where('TR.LONG_NAME IN(?)',$p_ranks);
      $q->order('TLI2.SORT_CODE');
   }break;
   //*/
   case 'getbyrank': {
   
      $q = $db->select()
            ->distinct()
            ->from(array('TR' => 'TAXON_RANK'), array("RANK" => "LONG_NAME"));
      $q->join(array('TLI1'   => 'TAXON_LIST_ITEM'),"TLI1.TAXON_RANK_KEY = TR.TAXON_RANK_KEY",array());
      $q->join(array('NS2'    => 'NAMESERVER'),"TLI1.TAXON_VERSION_KEY = NS2.INPUT_TAXON_VERSION_KEY",array());
      $q->join(array('TLI2'   => 'TAXON_LIST_ITEM'),  "TLI2.TAXON_LIST_ITEM_KEY = NS2.RECOMMENDED_TAXON_LIST_ITEM_KEY", array("PARENT","TAXON_LIST_ITEM_KEY"));
      $q->join(array('TV2'    => 'TAXON_VERSION'),    "TV2.TAXON_VERSION_KEY = NS2.RECOMMENDED_TAXON_VERSION_KEY", array("TAXON_VERSION_KEY"));
      $q->join(array('ITN2'   => 'INDEX_TAXON_NAME'), "ITN2.TAXON_LIST_ITEM_KEY = TLI2.TAXON_LIST_ITEM_KEY", array("ACTUAL_NAME","COMMON_NAME"));
      $q->where('TR.LONG_NAME = ?',$val);
      $q->order('TLI2.SORT_CODE');
   }break;
   case 'getbylatinname': {
      $val = "%$val%";
      $q->where('ITN.ACTUAL_NAME LIKE ?',$val);
   }break;
   case 'getbycommonname': {
      $val = "%$val%";
      $q->where('ITN.COMMON_NAME LIKE ?',$val);   
   }break;
   default: {
      $q->where('TR.LONG_NAME = ?','Kingdom');
//print $q->__toString();exit;
   }break;
   }
//print $q->__toString();exit;
   if ($f_one) $r = $db->fetchRow($q);
   else $r = $db->fetchAll($q);
   if ($get_hierarchy) {
      if (isset($r['PARENT']) && ($r['PARENT'] != 'NULL')) 
         $r['PARENT'] = gettaxa('getself',$r['PARENT'],false);
      $r['CHILDREN'] = gettaxa('getchildrenof',$r['TAXON_LIST_ITEM_KEY']);
   }
   return $r;
}

/*
$action = 'getbyrank';
$val = 'Kingdom';

$action = 'getself';
$val = 'NHMSYS0000332479';
 
$action = 'getbylatinname';
$val = 'Chiroptera';

*/

$action = (isset($_REQUEST['a']))?$_REQUEST['a']:null;
$val = (isset($_REQUEST['v']))?$_REQUEST['v']:null;
//$action = 'getself';
//$val = 'NHMSYS0000332479';

if ($action == 'getbyname') {
   $tl = gettaxa('getbylatinname',$val);
   if (count($tl)==0) $tl = gettaxa('getbycommonname',$val);
} else $tl = gettaxa($action,$val);
 
echo json_encode($tl);


?>