<?php
session_start();
error_reporting(E_ERROR);
$paths = array(
   '../',
   '../lib/',
   '../interfaces'
);
ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.join(PATH_SEPARATOR,$paths).PATH_SEPARATOR);
include_once('settings.conf');
include_once('snc/SnCDatabaseConnect2.php');
include_once('snc/S2_DBT.php');       
include_once('nbn/NBN_Cache.php');

$nbn = new NBN_Cache(); 
$settings = json_decode('{
   "NBN_Cache": {
         "SearchBy":"Endpoint",
         "SearchFor":"taxa"
   }  
}');           

$taxa = $nbn->selectAll($settings);
$tjson = '{';
$djson = '{';
while($taxon = array_shift($taxa)) {
   $url = $taxon['Endpoint'];
   $exp = preg_replace('/(\d{2})\/(\d{2})\/(\d{4})/','$3-$2-$1',$taxon['Expiry']);
   $tdy = date('Y-m-d',mktime());
   $isval = ($exp > $tdy);
   if ($isval) {  
      if (preg_match('/designations/',$url)) {
         preg_match('/([A-Z]{6}\d{10})/',$url,$mat);
         $tvk = $mat[1];
         if (isset($taxon['JSON']) && $taxon['JSON'] != '') {
            $djson .= "\"$tvk\":" . $taxon['JSON'] . ","; 
         }
      } else {
         $tvk = substr($url,-16);
         $istvk = preg_match('/[A-Z]{6}\d{10}/',$tvk);
         if ($istvk && isset($taxon['JSON']) && $taxon['JSON'] != '') {
            $tjson .= "\"$tvk\":" . $taxon['JSON'] . ",";            
         }
      }
   }
}
$tjson = preg_replace('/\,$/','}',$tjson);
$djson = preg_replace('/\,$/','}',$djson);
$json = "{\"taxa\":$tjson,\"designations\":$djson}";
echo $json;
?>