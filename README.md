# README #

Second Site or S2 is a web based data management tool like an 
[ORM]( https://en.wikipedia.org/wiki/Object-relational_mapping)
which allows you to quickly define a relational database with a default interface which presents various APIs to allow data to be built into custom applications including defining layers in a GeoServer instance to present the data as WFS/WMS/GeoJSon/KML for online mapping and GIS.

### What is this repository for? ###

* At present S2 is only really available on request. 
* Download and install instructions are being worked on. 
 
### How do I get set up? ###

* If you'd like a copy of S2 at present you will need to express your interest 
to NEYEDC [neyedc.org.uk](http://www.neyedc.org.uk).

#### Upgrading ####

The install and update process for S2 is brand new. If you have an existing S2 
install I would recommend taking a copy of the __/s2__ folder before running this 
process so you can recover your working installation if something goes wrong.

If you have a copy of Second Site already installed you should at least take a 
copy of your __settings.conf__ file before running the installer.

#### Install ####

To install, download a copy of the [s2installer.php](https://bitbucket.org/Lunarfish/s2main/raw/master/s2installer.php) file.

This file needs to be placed in the root of your website. If your website is 
hosted you'll probably need an ftp client like [FileZilla](https://filezilla-project.org/) to do this. 

Your web hosts should have instructions for setting up FTP client software. 

Then you need to visit the file eg __http://your-domain.com/s2installer.php__

The installer should tell you about the latest available version of Second Site 
and offer you the option to install it. 

#### Setup ####

Having run the install script you should have a copy of the S2 code on your 
website but you won't have a working database or config file.

Create a database and run the database setup script using MySQL Workbench, 
PHPMyAdmin or whatever tool your web hosts provide for managing databases: 

* __s2/sql/s2my_minimal.sql__ for MySQL 
* __s2/sql/s2pg_minimal.sql__ for PostgreSQL

You can install the S2 database tables in your main website database but if your
host lets you have multiple databases it's probably best to give it its own 
database. 

All the configuration settings are stored in __config.ini__ in the s2 directory. 
If you don't have a config.ini file you can copy the __config_empty.ini__ file.

(If you are upgrading an existing Second Site installation then you can find all 
your existing config settings in __s2/settings.conf__)

You then need to update all the settings in __config.ini__ with your settings.

For different functions in Second Site you need different settings configured. 

The absolute minimum requirements are that you complete the following sections:
* __core__
* __database:default__

Other settings sections are as follows: 
* database:[other] Allows you to use different database settings for a section of Second site eg database:users will connect to a different database for the users 
* api Allows you to configure your Google Maps API key and NBN API key.
* cms Allows you to delegate the Second Site login to your parent site if your site is based on drupal or wordpress 
* server:[serverName] Allows you to configure settings for logged in 3rd party services like NBN, Recorder or a WFS/WMS server.

##### [core] #####

* folder s2 The installer installs Second Site to http://your-domain.com/s2. If you want you can change this but you will need to make the same change in the folder setting in config.ini. 
* server your-domain.com (no http and no trailing slash)
* client your-domain.com (no http and no trailing slash)
* encrypt true or false 

##### [api] #####
Initially you can ignore the api settings. 

##### [database:default] #####
You must at least set default database settings. You can choose MySQL or PGSQL 
for the platform and then provide the connection hostname, database name, user
and password. 

* platform MySQL or PGSQL
* name s2 Set this to the name of the database you have created for Second Site 
* host localhost This should be the same as the hostname in your parent site settings. 
* username This should be the same as the database username in your parent site settings.
* password This should be the same as the database password in your parent site settings.

##### [server:[type]] #####
You can set up connections to remote services like a Recorder 6 database or a 
WMS/WFS server like [GeoServer](http://geoserver.org).

If you are logging in to NBN or Recorder I would recommend creating a user account 
specifically for that purpose and granting it the relevant permissions.  

#### Logging in for the first time ####
Having run the database script and setup the basic config file you should be 
able to login to Second Site by visiting http://your-domain.com/s2 

The default username and login are 
 
* user: __root@localhost__
* pass: __changeme__ 

Once you're logged in you should change these settings. 

If you want Second Site to use the login from your parent domain CMS (drupal or wordpress) 
you can set the __cms__ settings in config.ini. 

You will need to create accounts in Second Site corresponding to the CMS users 
which are identified by their email addresses and grant the S2 accounts the 
relevant permissions.

### Who do I talk to? ###

* [Dan Jones](mailto:dan.jones@lunarfish.co.uk)
* [Simon Pickles](mailto:simon.pickles@neyedc.co.uk)
