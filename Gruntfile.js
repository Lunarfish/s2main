module.exports = function(grunt) {

  grunt.initConfig({
    mkdir: {
		all: {
			options: {
				create: ['s2/js']
			}
		}
    },
    /*
    symlink: {
	  options: {
		overwrite: true
	  },
	  expanded: {
		files: [
		  {
			expand: true,
			overwrite: true,
			cwd: '/vagrant/build/bower_components/wordpress/',
			src: ['*'],
			dest: '/vagrant/web'
		  },
		  {
			expand: true,
			overwrite: true,
			cwd: '/vagrant/build/bower_components/s2main',
			src: ['s2'],
			dest: '/vagrant/web'
		  }
		]
	  },
	},
	copy: {
		wordpress: {
			expand: true,
			cwd: '/vagrant/build/bower_components/wordpress',
			src: ['**'],
			dest: '/vagrant/dist/'
		}
	}*/
	sass: {
		dist: {
		  files: {
			's2/styles/s2screen.css': 's2/styles/scss/main.scss',
			's2/styles/s2print.css':  's2/styles/scss/print.scss'
		  }
		}
	},
	concat: {
		options: {
		  separator: ';',
		},
		dist: {
		  src: [
			's2/lib/bgs/bgsldapi.js',
			's2/lib/htmlentities.js',
			's2/lib/encoder.js',
			's2/lib/msword.js',
			's2/lib/json/json2.js',
			's2/lib/calendarButtons.js',
			's2/lib/proj4js/proj4js-combined.js',
			's2/lib/proj4js/defs/EPSG27700.js',
			's2/lib/proj4js/defs/EPSG29901.js',
			's2/lib/proj4js/defs/EPSG29902.js',
			's2/lib/proj4js/defs/EPSG4326.js',
			's2/lib/coordinateConversions.js',
			's2/lib/genericInterface.js',
			's2/lib/screen.js',
			's2/lib/s2_v1_7.js',
			's2/lib/cdm/cdm_encrypt.js',
			's2/lib/cdm/cdm_authenticate2_v1_1.js',
			's2/lib/cdm/cdm_orequest_v1_2.js',
			's2/lib/cdm/cdm_dialog.js',
			's2/lib/tablemanager.js',
			's2/lib/s2_deriveddata_v1_3.js',
			's2/lib/s2_externaldata.js',
			's2/lib/s2_extend_v1_1.js',
			's2/lib/s2_files_v1_3.js',
			's2/lib/s2_importer_v1_3.js',
			's2/lib/s2_postprocess.js',
			's2/lib/s2_progress.js',
			's2/lib/s2_reports.js',
			's2/lib/s2_ordering.js',
			's2/lib/s2_exporter.js',
			's2/lib/fileuploader.js',
			's2/lib/s2_gmapv3_v1_5.js',
			's2/lib/s2_gmapv3areas_v1_5.js', 
			's2/lib/s2_gmapv3_geocoder.js',
			's2/lib/G_imanipulator_o.js',
			's2/lib/nbn/nbntaxa_v1_3.js',
			's2/lib/s2_nbnapi_v1_1.js'
		  ],
		  dest: 's2/js/s2.js',
		}
	},
	uglify: {
		options: {
		  mangle: false
		},
		dist: {
		  files: {
			's2/js/s2.min.js': ['s2/js/s2.js']
		  }
		}
	}
  });
  
  /*
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-symlink');
  grunt.loadNpmTasks('grunt-contrib-copy');
  */
  grunt.loadNpmTasks('grunt-mkdir');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask('default', ['mkdir:all','sass:dist', 'concat:dist', 'uglify:dist']);

};
